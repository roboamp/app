<?php

namespace App\Http\Middleware;

use Closure;
use App\SubDomain;

class ServerHeaders{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
    /*
        $subdomain_exist= SubDomain::where('subdomain',$_SERVER['HTTP_HOST'])->first();
        $subdomains=SubDomain::all();
        //for debugging purposes.
        //$subdomain_exist=1;
        if($subdomain_exist){
            dd('Connection has been established');
        }else{
            //dd('nope',$subdomain_exist,$_SERVER['SERVER_HOST'],$subdomains,$_SERVER);
        }

        //dd($request);
    */
        return $next($request);

    }
}
