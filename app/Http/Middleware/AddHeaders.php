<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AddHeaders
{
    public function handle(Request $request, Closure $next)
    {
        $domain_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
        //$http_origin = $_SERVER['HTTP_REFERER'];

        $response = $next($request);
        //header("Access-Control-Allow-Origin: *.ampproject.org");
        $response->header("Access-Control-Allow-Origin", "https://demo-roboamp-com.cdn.ampproject.org");
        //$response->header("Access-Control-Allow-Origin", "postmaster@amp.roboamp.com");
        $response->header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, FETCH");
        $response->header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token");
        $response->header("Access-Control-Expose-Headers", "AMP-Access-Control-Allow-Source-Origin");
        $response->header("Access-Control-Allow-Credentials", "true");
        $response->header("AMP-Same-Origin", "true");
        $response->header("Access-Control-Expose-Headers", "AMP-Access-Control-Allow-Source-Origin");
        $response->header("Vary", "Origin");
        $response->header("Vary", "X-Origin");
        $response->header("Vary", "Referer");
        $response->header("AMP-Access-Control-Allow-Source-Origin","https://ampy.roboamp.com");

        //header("Cache-Control:private");


        return $response;
    }
}