<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\PIC;

class PicController extends Controller
{
    public function index()
    {
        $data['user'] = Auth::User();

        return view('admin.pic.index', $data);
    }

    public function public()
    {
        $data['user'] = Auth::guest();
        return view('admin.pic.newDemo', $data);
    }

    public function store()
    {

        if ($_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR']){
            $this->output->set_status_header(400, 'No Remote Access Allowed');
            exit;
        }

        $res = PIC::where('average_monthly_visitor', request('average_monthly_visitors'))
            ->where('conversion_rate', request('conversion_rate'))
            ->where('average_order_value', request('average_order_value'))
            ->where('starting_speed', request('loading_time_speed'))
            ->where('loading_time', request('potential_time_speed'))
            ->pluck('total');

        return response()->json(['result' => $res], 200);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
