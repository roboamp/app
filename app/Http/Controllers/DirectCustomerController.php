<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Plan;
use App\User;
use App\Customer;
use App\Property;
use Webpatser\Uuid\Uuid;
use Auth;
use Faker;
use Carbon\Carbon;

class DirectCustomerController extends Controller
{

	public function __construct(){
        $this->testing=true;

    }

    public function show(){

        return view('customers.show', $data);
    }
}
