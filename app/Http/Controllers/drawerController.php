<?php

namespace App\Http\Controllers;

use App\MyClasses\Footer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class drawerController extends Controller
{
    private $template_name;


    public function light($child){

        $data['active_page']=$child;

        return  view('templates.drawer.light.'.$child)
            ->withHeaders([
                'AMP-Same-Origin' => 'true',
                'Access-Control-Allow-Origin' => '*',
            ],$data);
    }

}
