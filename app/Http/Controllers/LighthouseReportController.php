<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\LighthouseReport;
use App\LighthouseCategory;
use App\LighthouseFlag;
use App\LighthouseFlagReport;
use Illuminate\Support\Facades\Auth;
use App\Property;
use App\SelectorType;
use App\MyClasses\URL;
use App\Jobs\KnobGeneration;
use App\Jobs\ExecuteLighthouseTest;
use App\Jobs\ProcessUrlsToLighthouseReports;
use App\Jobs\SendLighthouseReportEmail;
use PDF;
use App\Jobs\TestingRoberto;

class LighthouseReportController extends Controller
{
    public $dismissed_urls;
    public $invalid_urls = array();
    
    public $limit;

    public function index(){
        //render a list of a resource
        $data['user']=Auth::User();
        $data['reports']=LighthouseReport::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Reports List';
        return view('admin.lighthouseReports.index',$data);
    }

    public function create (){
        //shows a view to create a new resource
        $data['properties']=Property::all();
        $data['selectors']=SelectorType::all();
        $data['categories']=LighthouseCategory::all();


        $data['user']=Auth::User();

        $data['messages']=0;
        $data['comments']=0;

        $data['form_centered']=true;
        $data['form_title']='Lighthouse Test';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.speedy.store');

        return view('admin.lighthouseReports.create',$data);
    }

    public function store(Request $request){



        if (Auth::user()) {   // Check is user logged in
            $this->limit = 20;
        }else{ 
            $this->limit = 1;
        }

        request()->validate([
            'url' => 'required'
        ]);
            
        $pattern = "/[\s,]+/";
        //separate the strings and create an array with the info
        $urls = preg_split($pattern, $request->url);
        
        //validate the URLs
        $valid_urls = $this->validate_urls_list($urls);
        
        //splice the valid urls to the given limit
        $accepted_urls = $this->limit_urls_array_list($valid_urls);

        if(count($valid_urls) <= 0){
            return back()->withErrors($this->invalid_urls);
        }


       for ($i=0;$i<20;$i++){
           TestingRoberto::dispatch();
       }


    //    ProcessUrlsToLighthouseReports::dispatch($accepted_urls, $request->category);
        
        //$result = $this->report_creation_cicle($accepted_urls, $request);
        
        return redirect(route('admin.speedy.reports'));

    }

    public function show($id){
        $data['user']=Auth::User();
        $data['report']=LighthouseReport::firstWhere('id', $id);
        $data['messages']=0;
        $data['comments']=0;

        return view('admin.lighthouseReports.show',$data);
    }



    public function validate_urls_list($urls){
        $valid_urls = array();
        foreach ($urls as $url) {
            if(URL::is_valid_url($url)){
                $valid_urls[] = $url;
            }else{
                $this->invalid_urls[] = $url;
            }
        }

        return $valid_urls;
    }

    //Check the number of URLs
    public function limit_urls_array_list($urls)
    {

        $urlscount = count($urls);
        $splice = $urlscount;
        //check if it's more than the limit (20)
        if($urlscount > $this->limit){
            $splice = $this->limit;
        }
        $this->dismissed_urls =  array_splice($urls, $splice);
        return $urls;

    }


    public function report_creation_cicle($urls, $request){
        (Auth::user()) ? $type = 'User' : $type = 'Admin';

        foreach ($urls as $url) {


            $report = LighthouseReport::firstWhere('url', $url);
            if(!$report){
                $this->store_report_to_database($url, $request);
            }else{
                if (!Auth::user()) {
                    return $report->id;
                }
            }
        }
    }

    Public function store_report_to_database($url, $request){
        if (Auth::user()) {   // Check is user logged in
            $type = 'User';
        }else{ 
            $type = 'Client';
        }
        $report= LighthouseReport::create([
            'url' => $url,
            'lighthouse_category_id' => $request->category,
            'type' => $type,
            'added_on' => now()
        ]);

        ExecuteLighthouseTest::dispatch($report);
    }

    
    public function send_mail_to(Request $request){
        SendLighthouseReportEmail::dispatch($request->report_id, $request->email, $request->status);
        return redirect()->route('admin.speedy.reports');
    }

    public function change_report_status($id){

        $report = LighthouseReport::firstWhere('id', $id);
        $report->lighthouse_status_id = 4;
        $report->readed_on = now();
        $report->save();

        return redirect()->away('https://www.roboamp.com');

    }


    public function preparing_knobs_images($report){
        $report = LighthouseReport::firstWhere('id', $report->id);

        foreach($report->flags as $flag){
            //generating With ROBOAMP knob charts
            $this->generate_knob($flag, 'amp', $flag->pivot->lighthouse_report_id, $flag->pivot->score);
            //generating Without ROBOAMP knob charts
            $this->generate_knob($flag, 'normal', $flag->pivot->lighthouse_report_id, $flag->pivot->score_normal);
        }
    }

    public function generate_knob($flag, $type, $report, $score){
        $root = URL::to('/');
        $color = ($type == 'amp') ?  '01c0c8' : 'f36e92'; 
        $url = $root.'/knob/'.$color.'/1.0/0/0.01/200/'.$score;
        KnobGeneration::dispatch($url, $flag, $type, $report);
    }

    
    public function preview_email($id){
        $data['user']=Auth::User();
        $data['report']=LighthouseReport::firstWhere('id', $id);
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Preview Report';
        
        return view('admin.lighthouseReports.preview.email',$data);
    }
}
