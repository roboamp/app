<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


use Illuminate\Http\Request;
use App\MyClasses\Server;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLoginForm(){

        $local_server= new Server();
        if($local_server->local_server){

            $data['user_testing_email']='roberto@roboamp.com';
            $data['user_testing_password']='roboamp';

        }
        if(!isset($data))dd("this is not a local server",$local_server);

        return view('admin.auth.login',$data);
    }
    public function logout(Request $request){
        Auth::logout();
        return $this->showLoginForm();


    }
}
