<?php

namespace App\Http\Controllers;

use App\MasterStatus;
use App\Pivot_timeline_status;
use App\Timeline;
use Illuminate\Support\Facades\Auth;
use Session;

class TimelineController extends Controller
{
    public function index(Timeline $id)
    {
        $data['user'] = Auth::User();
        $data['timeline'] = $id;

        return view('admin.timeline.index', $data);
    }

    public function list()
    {
        $data['user'] = Auth::User();
        $data['timelines'] = Timeline::all();

        return view('admin.timeline.list', $data);
    }

    public function create()
    {
        $data['user'] = Auth::User();
        $data['statuses'] = MasterStatus::all();
        $data['form_submit_button_title'] = 'Create Timeline';
        $data['form_action'] = route('admin.timeline.store');

        return view('admin.timeline.create', $data);
    }

    public function store()
    {
        $request = request()->validate([
            'name' => 'required',
            'description' => 'required',
            'statuses' => 'exists:status_id,id',
        ]);

        $descriptions = request()->get('dd_status_description');
        $status_ids = request()->get('dd_status_id');

        if (count(array_unique($status_ids)) < count($status_ids)) {
            return redirect()->back()->withInput()->withErrors(['statuses' => 'Duplicate Statuses.']);
        } else {

            $timeline_id = Timeline::create($request);

            $i = 0;
            foreach ($descriptions as $description) {
                if (!$description == '') {
                    $timeline_id->statuses_seeder()->attach($status_ids[$i], ['description' => $description]);
                    $i++;
                }
            }

        }
        return redirect(route('admin.timeline.index', $timeline_id));
    }

    public function show($id)
    {
        $data['timeline'] = Timeline::findOrFail($id);
        $data['user'] = Auth::user();
        $data['statuses'] = MasterStatus::all();
        $data['form_action'] = route('admin.timeline.update', $id);
        $data['form_submit_button_title'] = 'Edit Timeline';

        return view('admin.timeline.edit', $data);
    }

    //you receive the active_id wich is the ID for pivot table status_timeline
    public function status_preview(Pivot_timeline_status $id)
    {
        $data['user'] = Auth::user();
        $data['active_checkpoint_id'] = $id->status_id;
        $data['timeline'] = Timeline::where('id', '=', $id->timeline_id)->first();

        return view('admin.timeline.index', $data);
    }

    public function update(Timeline $id)
    {

        $id->update(request()->validate([
            'name' => 'required',
            'description' => 'required',
            'statuses' => 'sometimes|exists:status_id,id'
        ]));

        $status_id_add = request('dd_status_id_add');
        $status_description_add = request('dd_status_description_add');
        $status_id = request('dd_status_id');
        $status_description = request('dd_status_description');

        $check_duplicates = array_merge($status_id_add, $status_id);

        $i = 0;
        foreach ($status_description as $description) {
            $id->statuses_seeder()->updateExistingPivot($status_id[$i], ['description' => $description]);
            $i++;
        }

        $i = 0;
        foreach ($status_description_add as $description) {
            if (!$description == '') {
                if (count(array_unique($check_duplicates)) < count($check_duplicates)) {
                    return redirect()->back()->withInput()->withErrors(['statuses' => 'Duplicate Statuses.']);
                } else {
                    $id->statuses_seeder()->attach($status_id_add[$i], ['description' => $description]);
                }
            }
        }

        Session::flash('active_id', $id->id);

        return redirect(route('admin.timeline.index', $id));
    }

    public function destroy_status_timeline(Pivot_timeline_status $id)
    {
        if ($id) {
            $id->delete();
            return response()->json(['success' => 'Status Deleted.'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong status could not be deleted.'], 422);
        }
    }

    public function destroy(Timeline $id)
    {
        if ($id) {
            $id->delete();
            return response()->json(['success' => 'Timeline Deleted.'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong timeline could not be deleted.'], 422);
        }
    }
}
