<?php

/*
 *
 */
namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Auth;
use App\Template;
use App\Property;
use App\SelectorType;
use App\MyClasses\URL;
use App\MyClasses\MyArray;




class TemplateController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        //render a list of a resource
        $data['user']=Auth::User();
        $data['templates']=Template::all();
        $data['messages']=0;
        $data['comments']=0;
        return view('admin.templates.demo',$data);
    }

    public function create (){
        //shows a view to create a new resource
        $data['properties']=Property::all();
        $data['selectors']=SelectorType::all();


        $data['user']=Auth::User();

        $data['messages']=0;
        $data['comments']=0;

        $data['form_centered']=false;
        $data['form_title']='Create Template';
        $data['form_submit_button_title']=$data['form_title'];

        $data['selector_content_type_options']=null;
        $data['dev_mode']=false;
        $data['selector_type_options']=null;



        $data['form_action']=route('admin.properties.templates.store');

        return view('admin.templates.create',$data);


        //$response = new Response($view);
        //return $response->withCookie('something','value',60);
    }
    public function show(Template $template){
        //show a single resource
    }

    public function store(){

        dd(request()->all());
        //persist the new resource
        $request=request()->validate([
            'txt_name'=>'required',
            'property_id'=>'required',
            'txt_url'=>[
                'required',
                function ($attribute, $value, $fail) {
                    $input_domain=URL::get_domain(request('txt_url'));
                    $property=Property::where('id',request('property_id'))->first();
                    $match=$property->domain_match($input_domain);

                    if(!$match) $fail("This URL doesn't match the domain or subdomain for this property");

                }],
            'selector_id'=>'required',
            'txt_signature'=>'required'
        ]);

        $request=MyArray::remove_prefix($request,"txt_");

        Template::create($request);

        return redirect(route('properties.templates.list'));
    }

    public function edit(Template $template){
        //show a view to edit am existing resource
    }
    public function update(){
        //persist the edited resource
    }

    public function destroy(Template $template){
        //deletes an element
    }




}
