<?php

namespace App\Http\Controllers;

use App\Console\Commands\IDs;
use App\Console\Commands\ROBOAMP;
use App\MyClasses\URL;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

class UrlController extends Controller
{
    public function index()
    {
        $data['user'] = Auth::user();
        $data['properties'] = Property::with('customers')->get();

        return view('admin.url.index', $data);
    }

    public function show($id)
    {
        $data['user'] = Auth::user();

        $amp_url = [];
        $insight_amp_original = [];
        $insight_amp_version = [];

        $url = new URL();
        $property = Property::findOrFail($id);

        $data['urls'] = [
//            $property->url,
            "https://demo.roboamp.com/buyr/home",
            "https://demo.roboamp.com/buyr/category",
            "https://demo.roboamp.com/buyr/product",
            "https://demo.roboamp.com/buyr/search",
        ];

//        dd($property->url);

        foreach ($data['urls'] as $item) {
//            dd($item);
            $tmp_amp_url = "https://" . $url->make_amp_url($item, 1);

            $insight_amp_original[] = $this->get_amp_urls('original', $tmp_amp_url);
            $insight_amp_version[] = $this->get_amp_urls('demo', $tmp_amp_url);

            $amp_url[] = $tmp_amp_url;
        }

        $data['amp_cache_demo_env'] = $amp_url;
        $data['insight_amp_original'] = $insight_amp_original;
        $data['insight_amp_version'] = $insight_amp_version;

        return view('admin.url.show', $data);
    }

    public function get_amp_urls($type, $amp_url)
    {
        $base_url = "https://developers.google.com/speed/pagespeed/insights/?url=";
        switch ($type) {
            case 'original':
                return $base_url.urlencode($this->get_original_url($amp_url));
                break;
            case 'demo':
                return $base_url.urlencode($amp_url);
                break;
        };
    }

    public function get_original_url($url)
    {
        $original_url = explode(".com/", $url);
//        dd($original_url);
        return "https://" . str_replace("/", ".com/", $original_url[1]);
    }
}
