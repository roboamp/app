<?php

namespace App\Http\Controllers;

use App\Category;
use App\FAQ;
use App\Mail\Response;
use App\Query;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Session;

class AdminFAQController extends Controller
{
    //list
    public function index()
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['questions'] = FAQ::paginate(20);

            return view('admin.faq.admin_list', $data);
        } else {
            return redirect()->back();
        }
    }

    public function list_preview($privacy = "all")
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['categories'] = Category::all();
            $data['questions'] = FAQ::list_questions($privacy);

            return view('admin.faq.list', $data);

        } else {
            return redirect()->back();
        }
    }

    public function answer_show(FAQ $id)
    {
        if (Auth::user()->is_admin()) {

            if (!$id) {
                abort(404);
            }
            $data['user'] = Auth::User();
            $data['privacy'] = FAQ::privacy_array_filter($id);
            $data['categories'] = Category::all();

            return view('admin.faq.show', ['question' => $id], $data);
        } else {
            return redirect()->back();
        }
    }

    //answer-store the changes
    public function answer_store(FAQ $id)
    {
        $email = $id->user->email;
        $answer = request('answer');
        $data = FAQ::edit_validation($id);

        Mail::to($email)
            ->send(new Response($answer));

        Session::flash('active_id', $id->id);

        return redirect(route('admin.faq.admin_list'))
            ->with('message', 'Answer Sent.');
    }

    public function pending()
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['questions'] = FAQ::pending_question();
            return view('admin.faq.pending', $data);
        } else {
            return redirect()->back();
        }
    }

    //answer-delete
    public function destroy(FAQ $id)
    {
        if (Auth::user()->is_admin()) {
            if ($id) {
                $id->delete();
                return response()->json(['success' => 'Question Deleted.'], 200);
            } else {
                return response()->json(['error' => 'Something went wrong.'], 422);
            }
        } else {
            return redirect()->back();
        }
    }

    // query log
    public function reports()
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['search_logs'] = Query::get_query();
            return view('admin.faq.reports', $data);
        } else {
            return redirect()->back();
        }
    }
}
