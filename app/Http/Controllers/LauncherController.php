<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Report;
use App\MyClasses\Templates\Partials\Listing;

class LauncherController extends Controller
{
    public function __construct(){



    }
    public function report($url){
        if($user = Auth::user())
        {
            $now = Carbon::now();
            if($user->user_role_id==3){
                $report = new Report();
                $report->url=$url;
                $report->user_id=$user->id;
                $report->created_at=$now;
                $report->updated_at=$now;
                $report->save();
                return redirect()->route('users.reports.amp.errors');
            }
            return redirect()->route('login');

        }else{
            return redirect()->route('login');
        }


    }

    public function robolink_main(){
        if (!Auth::check()){
            dd('nono');
            return redirect()->route('users.login');
        }else{
            dd(Auth::check());
        }

        dd('end');


        return redirect()->route('users.reports.amp.errors');

    }
    public function list_errors(){

        //generic class for listings

       /* $block=new Listing('amp_error');

        dd($block);

        $data['show_coupons']=0;
        $data['show_customers']=0;
       */
        $data['errors']=Report::all();

        return view('reports.main', $data);
    }

}
