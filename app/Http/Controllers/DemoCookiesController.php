<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DemoCookiesController extends Controller{

    public function consent(){
        return View('demo/consent');
    }
    public function amp_consent(){
        return View('demo/amp-consent');
    }
}
