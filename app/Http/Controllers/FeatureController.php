<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feature;
use Auth;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //render a list of a resource
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['features']=Feature::all();

        return view('admin.features.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //render a list of a resource
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
    
        $data['form_centered']=true;
        $data['form_title']='Create Feature';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action'] = route('admin.features.store');

        return view('admin.features.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $feature = Feature::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price
        ]);

        return redirect()->route('admin.features.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $feature = Feature::find($request->id);
        
        $feature->name = $request->name;
        $feature->price = $request->price;
        $feature->save();

        if($feature->id == $request->id && $feature->name == $request->name && $feature->price == $request->price){
            $data["success"] = true;
            $data["message"] = "Feature ".$feature->name." Succesfully Edited.";
            $data["target"] = $feature->id;
            $data["name"] = $feature->name;
            $data["price"] = $feature->price;
        }else{
            $data["success"] = false;
            $data["message"] = "Feature ".$feature->name." Not Edited.";
            $data["target"] = $feature->id;
        }

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $feature = Feature::find($request->feature);
        $feature->delete();

        $feature2 = Feature::find($feature->id);

        if(!$feature2){
            $data["success"] = true;
            $data["message"] = "Feature Deleted Successfully.";
        }else{
            $data["success"] = false;
            $data["message"] = "Feature Not Deleted.";
        }

        $data["target"] = $feature->id;

        return response()->json($data);
    }
}
