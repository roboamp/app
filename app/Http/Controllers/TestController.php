<?php
/*
 * This controller is the one called from ***EVERY*** client
 *
 */
namespace App\Http\Controllers;

use App\MyClasses\Output;
use App\MyClasses\URL;
use App\Page;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MyClasses;
use Illuminate\Support\Facades\File;
use Config;
use App\MyClasses\Parser;


class TestController extends Controller
{

    //only to detect if has been rendered already
    protected $rendered=false;
    //page I'm going to render
    protected  $render;


    public function page(){

    /*    $content=view('page')->render();
        $new_content=str_replace('<meta name="generator" content="https://html2amp.mobilizetoday.ru">','<meta name="generator" content="https://roboamp.com">',$content);
        $content=str_replace('Initiator: MobilizeToday AMP Generator','Initiator: ROBOAMP AMP Generator',$new_content);

        */

        return view('page');
    }
    //replaces pre content with html entities



    public function main($property_id,$page=""){
        $output=new Output();
        //first detect if it is a bot


//"19cceb8e-4702-4ef    b-bb64-a8880958669c"



        /* @var $property \App\Property */

        $property=Property::find($property_id);


        if($property!=null){

            if($property->status->status=="Active"){

                /*
                 * if($property->created_via_coupon()){
                //do something here for properties created using coupons
                $this->render=$page_exist;
                return $output->render($this->render);
            }
                 */

               //remove trailing slash
                $url = rtrim($page, '/');

                $parser=new Parser();

                //check if the page exist
                $page_exist= $parser->validate_page($url);

                if($page_exist){

                    $this->render=$page_exist;
                    $this->render->canonical=$url;
                    //pass 1 so we know we are loafing the
                    //trigger option
                    return $output->render($this->render,$property,1);

                }else{

                    return abort(404,'Property exists but Page Doesn\'t');
                }


                //call the render function
                //check if less than 24 hours to re-render



            }else{
                return abort(404,'Property not active');
            }

        }else{
            return abort(404,'There is no property with that ID');
        }
        dd("something else");
    }


    public function test($property_id,$page=""){

        $output=new Output();
        //first detect if it is a bot
        if($page=="standish"){
            $referer="index";
            if (isset($_SERVER['HTTP_REFERER'])) {$referer=$_SERVER['HTTP_REFERER'];}
            if($referer!=""){$page=$referer;}

        }




        /* @var $property \App\Property */

        $property=Property::find($property_id);

        if($property!=null){

            if($property->status->status=="Active"){


                //remove trailing slash
                $url = rtrim($page, '/');

                $parser=new Parser();

                //check if the page exist
                $page_exist= $parser->validate_page($url);

                if($page_exist){

                    $this->render=$page_exist;
                    $this->render->canonical=$url;
                    //folder is in dev folder
                    return $output->render($this->render,$property,0,1);

                }else{
                    return abort(404,'Property exists but Page Doesn\'t');
                }


                //call the render function
                //check if less than 24 hours to re-render



            }else{
                return abort(404,'Property not active');
            }

        }else{
            return abort(404,'There is no property with that ID');
        }
        dd("something else");
    }

    private function _bot_detected() {

        return (
            isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT'])
        );
    }


    //returns if the page exists on the DB or not;



}
