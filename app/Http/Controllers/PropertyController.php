<?php

namespace App\Http\Controllers;

use App\Mail\Messenger;
use App\Customer;
use App\MyClasses\Stripe\RoboStripe;
use App\Platform;
use App\Step;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Property;
use DB;


class PropertyController extends Controller
{
    private $testing;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(){
        return redirect (route('admin.properties.templates.create'));
    }

    public function update_platform(Request $request){

        $user= Auth::guard('customers')->user();

        //check if user can update that property
        $steps=new Step();
        $data=$request->all();
        $keys=(array_keys($data));
        $property_id=$keys[1];
        $platform_id=$keys[3];
        $platform_code=$keys[2];
        $change_from_div=$keys[4];


        if($data[$change_from_div]=='yes'){
            $steps_id=null;
            $update_array=['platform_id'=>$data[$platform_id]];
        }else{
            $steps_id=2;
            $update_array=['platform_id'=>$data[$platform_id],'steps_id'=>$steps_id];
        }



        Property::where('id',$property_id)->where('customer_id',$user->id)
            ->update($update_array);


        return $steps->render_steps($steps_id, $data[$platform_id], $property_id);


    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */


    public function activate_property($property_id){

        $property=Property::find($property_id);


        return redirect(route('users.add.customer',['customer_id'=>$property->customer_id,'property_id'=>$property->id]));
    }
    public function change_property_status($property_id,$status_id){


        $property=new Property();

        $user_id=Auth::user()->id;
        $property->update_status($property_id,$status_id,$user_id);

        $robostripe = new RoboStripe();
        switch ($status_id){
            case 1:
            break;
            case 2:

            break;
            case "3":
                $res = $robostripe->cancel_subscription_by_property_id($property_id);
                $message['error']=0;
                $message['success']="Subscription has been canceled.";
            break;

        }
        return json_encode($message);

    }
    public function confirm(Request $request){
        $propertyModel=new Property();
        $customer=new Customer();
        $steps=new Step();
        $user= Auth::guard('customers')->user();

        $property_id=$request->get('property_id');

        $property=$propertyModel::where('id',$property_id)->first();
        $email=$property->customers->email;
        $property->email=$email;

        $steps_id=3;

        $update_array=['platform_id'=>$property->platform_id,'steps_id'=>$steps_id];

        Property::where('id',$property->id)->where('customer_id',$user->id)
            ->update($update_array);

        $notification= $property->send_notification("<@UC2RQLBC7> A new Property has been updated");

        //email notification to user and admin

        $res=Property::where('id',$property_id)->first();

        $customer=Customer::where('id',$res->customer_id)->first();

        //delegate to a queue
        $data['type']='waiting_on_google';

        $data['username']=$customer->name;

        \Mail::to($customer->email)->send(new Messenger($data));

        $data['type']='admin_waiting_on_google';
        \Mail::to('roberto@roboamp.com')->send(new Messenger($data));


        return $steps->render_steps($steps_id, $property->platform_id, $property_id);

    }

}
