<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\MyClasses\URL;
use App\Plan;
use App\Customer;
use App\Property;
use Webpatser\Uuid\Uuid;
use Auth;
use Faker;
use App\Platform;
use Validator;
use App\Counter;
use App\MyClasses\Counters;
use App\MyClasses\Server;
use App\Notifications\PropertyNotification;



class UserController extends Controller
{

    private $testing;

    //the construct always runs first;
    public function __construct(){


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //shows all the websites for a particular customer
    public function index($id){

        $data['testing'] = $this->testing;
        if($data['testing'] == true){
            $data['testing'] = $this->setup_testing_array();
        }
        $customer=Customer::find($id);
        if(is_null($customer)){
            return response()->view('errors.404');

        }
        $data['customer'] = $customer;
        $data['sites'] = Property::where('customer_id', '=', $id)->paginate(10);


        return view('users.index', $data);
    }

    public function show_code($property_id){
        $data['property']=Property::where('id',$property_id)->first();
        $data['platforms'] = Platform::get();
        $data['add_website_url']=route('users.add.property',$data['property']->customer_id);

        //HTML is set to default platform
        $data['default_platform'] = Platform::where('name', 'html')->first();

        return view('customer-user-select-code',$data);
    }


    public function add_property_submit(Request $request){

        $validatedData = $request->validate([
            'url' => 'required|url',
        ]);

        //before creating a subscription: Check if that domain exist
        $url=new URL();
        $domain_exist=$url->check_if_domain_exist_in_db($request->input('url'));
        if(is_array($domain_exist)) return json_encode($domain_exist);

        $customer=Auth::guard('customers')->user();

        // dd($customer);
        $property = new Property;
        $property->id = Uuid::generate(4);
        $property->customer_id = $customer->id;
        $property->status_id = 2;
        $property->url = $request->input('url');
        $property->save();


        $data['error'] = 0;
        $data['success'] = "Website has been added";

            dd('its this one');


        return json_encode($data);

    }



    /**
     * Show the form for creating a new website.
     *
     * @return \Illuminate\Http\Response
     */
    //post from the plan selection
    public function create(Request $request)

    {


        $validatedData = $request->validate([
            'url' => 'required|url',
        ]);
        $renew_subscription=($request->input('txt_renew_subscription')!=null)?true:false;


        if($request->input('name') == '' || $request->input('plan_stripe_id') == '' || $request->input('url') == ''){
            $data['error'] = 1;
            return json_encode($data);
        }
        else {
            //check if the $edit_customer_edit has been set
            //if not that means this is a new user,
            // otherwise we are adding a website to existent user

            $user=Auth::user();

            //before creating a subscription: Check if that domain exist
            $url=new URL();
            //if we are renewing a subscription, do not check for domain
            if(!$renew_subscription){
                $domain_exist=$url->check_if_domain_exist_in_db($request->input('url'));
                if(is_array($domain_exist)) return json_encode($domain_exist);
            }


            //create the subscription
            $message= $user->create_subscription($request);


            $subscription_id=$message['subscription_id'];

            if(is_array($message) && $message['error']==0){

                //Creates a customer if no customer_id is passed
                if(is_null($request->edit_customer_id) && $renew_subscription==false) {
                    $customer_id = $this->create_customer($request, $user->id);
                    $success_message="Customer has been created";
                }else{
                    if($renew_subscription==false) {
                        $customer_id = $request->edit_customer_id;
                        $success_message = "Property has been added to customer " . $request->name;
                    }
                }
                //creates the property if we are not renewing a subscription
                if($renew_subscription==false) {
                    $message = $this->create_property($request, $customer_id, $subscription_id);
                    if (!is_array($message)) {
                        $message = ["error" => 0, "success" => $success_message];

                    }
                }
            }

            return json_encode($message);
        }



    }
    public function add_customer_website($customer_id){

        //check if this customer belongs to current logged user
        //if it does, returns the customer's record to add_customer.
        $user=Auth::user();
        //check if this customer belongs to this user
        $valid_customer=$user->validate_customer($customer_id);
        if(!is_null($valid_customer)){
            return $this->add_customer($valid_customer);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function display_dashboard()
    {

        $user = Auth::user()->id;
        $data['testing'] = $this->testing;
        $data['customers'] = Customer::where('user_id', '=', $user)->paginate(10);

        $user_obj=Auth::user();

                //LOOPS THROUGH ALL COUNTERS COMING BACK FROM WIDGET CLASS AND PLACES THEM
        //INTO AN ARRAY TO BE LOOPED THROUGH IN COUNTER.BLADE
        $counters = Counter::where('user_id', $user)->get();

        // dd($counters);
        $data['counters_array'] = [];
        foreach($counters as $obj){
            $counter = new Counters();
            $counter->set_name($obj['name']);
            $counter->set_owner_id($obj['user_id']);
            $counter->set_table($obj['table']);
            $counter->set_type($obj['type']);
            $counter->set_label($obj['label']);
            $counter->set_color($obj['color']);
            $counter->set_icon($obj['icon']);

            array_push($data['counters_array'], $counter);
            $data['counters'][]= $counter->render();
        }

        //--------------------


        
        //LOOPS THROUGH ALL COUNTERS COMING BACK FROM COUNTER CLASS AND PLACES
        //ATTRIBUTES INTO AN ARRAY SO WE CAN PASS POST DATA BACK TO THIS CONTROLLER
        // $data['counters_name'] = [];
        // foreach($data['counters_array'] as $obj){
        //     $array = [];
        //     array_push($array, $obj->get_name(), $obj->get_type(), $obj->get_table());
        //     array_push($data['counters_name'], $array);
        // }

        // $data['counters_name']=json_encode($data['counters_name']);
        //--------------------



        if($data['testing'] == true){
            $data['testing'] = $this->setup_testing_array();
        }

        $data['coupons']=Coupon::all();

        return view('home', $data);
    }

    private function setup_testing_array(){
        $faker = Faker\Factory::create();

        $data['url'] = $faker->url();
        $data['name'] = $faker->name();
        
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $status)
    {
        $properties = Property::where('customer_id', '=', $id)->get();

        foreach($properties as $property){
            $property->status_id = $status;
            $property->save();
        }

        $data['error'] = 0;
        

        return json_encode($data);

    }

    public function edit_show($id){
        $customer = Customer::find($id);
        $data['customer'] = $customer;
        $data['plans'] = Plan::all();


        return view('users.edit', $data);
    }

    public function edit_customer(Request $request, $id){

        $customer = Customer::find($id);
        $customer->name = $request->input('name');
        $customer->plan_id = $request->input('plan_id');
        $customer->save();
        

        return redirect('/');

    }

    //displays the plan selection before creating a new website
    public function add_customer($customer_id=null,$property_id=null){

        $server=new Server();

        $user=Auth::user();
        $data['stripe_email']=$user->email;
        //used for the buttons on plan selection
        $data['plans'] = Plan::all();
        //image displayed on Stripe Form
        $data['stripe_form_image']=asset('img/ROBO_stripe.png');

        $data['stripe_key'] = $server->get_public_stripe_key();


        $data['customer']=(!is_null($customer_id)?Customer::find($customer_id):null);


        $data['property']=(!is_null($property_id)?Property::find($property_id):null);
        $data['callback']='users.process_stripe_url';
        $data['default_plan']='';
//        dd($data['plans']);

        return view('users.add', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->plan_id = 3;
        $customer->save();
        $data['error'] = 0;

        return json_encode($data);
    }

    public function add_site(Request $request, $id)
    {

        $uuid= new Uuid();

        $customer = Customer::find($id);
        $property = new Property;
        $property->id = $uuid::generate(4);
        $property->customer_id = $id;
        $property->status_id = 2;
        $property->url = $request->input('url');
        $property->name = $customer->name;
        $property->save();


        $data['url'] = $property->url;
        $data['error'] = 0;
        $data['testing'] = $this->setup_testing_array();


        return json_encode($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function create_customer($request,$user_id){
        $customer = new Customer;
        $customer->id=Uuid::generate();
        $customer->name = $request->input('name');
        $customer->user_id = $user_id;
        $customer->password=bcrypt(Carbon::now());
        $customer->save();
        return $customer->id;
    }
    private function create_property($request,$customer_id,$subscription_id){


        $domain=$request->input('url');

        if(is_array($domain)) return $domain;

        $plan=new Plan();
        $property = new Property;
        $property_id=Uuid::generate(4);
        $property->id = $property_id;
        $property->customer_id = $customer_id;
        $property->plan_id =$plan->get_plan_id_from_stripeID($request->input('plan_stripe_id'));
        $property->status_id = 2;
        $property->url =$domain;
        $property->main_website = 1;
        $property->save();

        //update the subscription with the right property_id
        $subscription=Subscription::where('id',$subscription_id)
            ->update(['property_id'=>$property_id]);

        $event='property_added';
        $property->notify(new PropertyNotification($event));

        return $property->id;
    }

    public function indexReferredCustomer(Request $request, $id){

        $customer = Customer::find($request->cookie('loggedin'));


        if($request->cookie('loggedin')){
            if($request->cookie('loggedin') == $id){
                $data['customer'] = $customer;
                $data['loggedin'] = true;
                return view('users.index', $data);
            }else{
                return redirect('/customers/' . $request->cookie('loggedin'));
            }
        }

        return redirect(route('users.login'));
    }


}
