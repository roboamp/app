<?php

namespace App\Http\Controllers;

use App\MyClasses\HTML;
use App\MyClasses\Seeders;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Mail\Mailer;
use App\Mail\Demo;
use App\Mail\Contact;
use Validator;
use Newsletter;
use App\MyClasses\LandingPageData;

class LandingController extends Controller{
    
    public function index (){

        //$exclude=array('team_members');
        $exclude=null;
        //LandingPageData is an array with all the info needed for the landing page
        $data=new LandingPageData($exclude);
        $data=$data->getData();

        //dd($data);
        //generate HTML content
        $html= new HTML();
        return $html->view('landing',$data);

    }

    public function demo_email(Request $request){
        $validatedData = $request->validate([
            'email' => 'required|email',
        ]);

        $email=$request->email;
        $newsletter= new Newsletter();
        $newsletter::subscribe($email);


        $data = ['email' => $email];
        \Mail::to($email)->send(new Demo($data));

    }

    public function contact_email(Request $request){

        $validatedData = $request->validate([
            'email' => 'required|email',
        ]);
        $email=$request->email;
        $newsletter= new Newsletter();
        $newsletter::subscribe($email);
        $data = ['name' => $request->name, 'email' => $email, 'contact_message' => $request->message];
        \Mail::to('justin@roboamp.com')->send(new Contact($data));
    }

    public function subscription_email(Request $request){

        $validatedData = $request->validate([
            'email' => 'required|email',
        ]);
        
        $email=$request->email;
        $newsletter= new Newsletter();
        $newsletter::subscribe($email);
        $data = ['name' => $request->name, 'email' => $email, 'contact_message' => $request->message];
        \Mail::to('roberto@roboamp.com')->send(new Contact($data));
    }

    public function csrf(){
        return json_encode(csrf_token());
    }

}
