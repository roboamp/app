<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ROBOAMP\Strings;
use Auth;
use App\Ampcomponent;
use App\AttributeType;
use App\Attribute;
use App\Amptemplate;
use App\AmptemplateAttribute;

class AmpTemplateController extends Controller
{
    public function index()
    {
        $data['user']=Auth::User();
        $data['templates']=Amptemplate::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Amp Templates List';
        

        return view('admin.amptemplates.index', $data);
    }

    public function create()
    {
        $data['user']=Auth::User();
        $data['components']=Ampcomponent::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_centered']=true;
        $data['form_title']='New Amp Template';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.amptemplates.store');

        return view('admin.amptemplates.create', $data);
    }


    public function store(Request $request)
    {
        $template = Amptemplate::create([
            'name' => $request->name,
            'description' => $request->description,
            'component_id' => $request->component
        ]);

        $attr_numbers = count($request->attr_id);
        for($i=0; $i<$attr_numbers; $i++){
            AmptemplateAttribute::create([
            'template_id' => $template->id,
            'attribute_id' => $request->attr_id[$i],
            'value' => $request->value[$i]
            ]);
        }

        return redirect()->route('admin.amptemplates.index');
    }


    public function show()
    {
        $attributes = AmptemplateAttribute::select('id')->where('template_id', 4)->get();
        foreach($attributes as $attribute){
            dump($attribute->id);
        }
    }


    public function edit(Request $request)
    {
        $data['user']=Auth::User();
        $data['template'] = Amptemplate::findOrFail($request->template);
        $data['components']=Ampcomponent::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_centered']=true;
        $data['form_title']='Edit Template';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.amptemplates.store');

        return view('admin.amptemplates.edit', $data);
    }


    public function update(Request $request)
    {
        $template = Amptemplate::findOrFail($request->template);      
        $template->name = $request->name;
        $template->description = $request->description;
        $template->component_id = $request->component;
        $template->save();

        $attr_numbers = count($request->attr_id);
        for($i=0; $i<$attr_numbers; $i++){
            $attribute = AmptemplateAttribute::find($request->attr_id[$i]);
            
            if($attribute === null){
                $attribute = AmptemplateAttribute::Create([
                'template_id' => $request->template,
                'attribute_id' => $request->attr_id[$i],
                'value' => $request->value[$i]
                ]);
            }else{
                $attribute->value = $request->value[$i];
                $attribute->save();
            }
        }

        return redirect()->route('admin.amptemplates.index');
    }


    public function destroy(Request $request)
    {
        $template = Amptemplate::find($request->template);
        $template->delete();

        $template2 = Amptemplate::find($request->template);

        if(!$template2){
            $attributes = AmptemplateAttribute::select('id')->where('template_id', $request->template)->get();
            foreach($attributes as $attribute){
                $attribute->delete();
            }

            $data["success"] = true;
            $data["message"] = "Template Deleted Succesfully";
        }else{
            $data["success"] = false;
            $data["message"] = "Template Not Deleted";
        }

        $data["target"] = $request->template;

        return response()->json($data);
    }


    public function generate_code(Request $request){
        $template = Amptemplate::where('id', $request->template)->where('component_id', $request->component)->first();
        $component = Ampcomponent::firstWhere('id', $request->component);
        $codebase = $component->codebase;
        
        foreach($template->attributes as $attributes){
            $nail = '||'.$attributes->attribute->name.'=""';
            $attr = $attributes->attribute->name.'="'.$attributes->value.'"';
            $haystack = Strings::find_string_in_string($codebase, $nail);
            if($haystack){
                $codebase = str_replace($nail, $attr, $codebase);
            }else{
                $warnings[] = $attributes->attribute->name;
            }
        }

        $data["code"] = $codebase;
        $data['success'] = true;
        $data['message'] = '';

        return response()->json($data);
    }

    public function get_attributes(Request $request){
        $attributes = Attribute::where('component_id', $request->component)->get();
        $temp_attr = AmptemplateAttribute::where('template_id', $request->template)->get();
        if(count($temp_attr)>0){
            $has_attr = true;
        }else{
            $has_attr = false;
        }
        
        if(count($attributes) > 0){
            $data['status'] = 'success';
            $data['attributes'] = $attributes;
            $data['templates_attr'] = $temp_attr;
            $data['template'] = $request->template;
            $data['has_attr'] = $has_attr;
        }else{
            $data['status'] = 'fail';
        }
        return response()->json($data);
    }

    public function upgrade_template_attributes(Request $request){

        foreach($request->attributes_inactive as $attribute){
            $attr = Attribute::find($attribute);
            $value = json_decode($attr->value, true);
            $new_attr = new AmptemplateAttribute;
            $new_attr->template_id = $request->template;
            $new_attr->attribute_id = $attr->id;
            if($attr->type_id == 1){
                $new_attr->value = $value["options"][0];
            }else{
                $new_attr->value = $value["default"];
            }
            $new_attr->save();
        }
        $data['success'] = true;
        return response()->json($data);

    }
}
