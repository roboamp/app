<?php

namespace App\Http\Controllers;

use App\Status;
use App\MasterStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatusController extends Controller
{
    public function index()
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['statuses'] = MasterStatus::withCount('timelines')->get();

            return view('admin.timeline.status', $data);
        } else
            return redirect()->back();
    }

    public function show()
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['statuses'] = MasterStatus::all();
            return view('admin.timeline.status', $data);
        } else {
            return redirect()->back();
        }
    }

    public function store()
    {
        $res = request()->validate([
            'name' => 'required|unique:statuses|min:3'
        ]);

        if(!$res) {
            return response()->json('error', 422);
        } else {
            $status = MasterStatus::create($res);
            $data['success'] = 'Status Saved.';
            $data['id'] = $status->id;
            $data['name'] = $status->name;
            return response()->json($data, 200);
        }
    }

    public function update(MasterStatus $id)
    {
        $res = $id->update(request()->validate([
            'name' => ['required', 'min: 3']
        ]));

        if (!$res) {
            return response()->json(['error' => 'Something went wrong status could not be updated.'], 422);
        } else {
            return response()->json(['success' => 'Status Updated.'], 200);
        }
    }

    public function destroy(MasterStatus $id)
    {
        if ($id) {
            $id->delete();
            return response()->json(['success' => 'Status Deleted.'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong status could not be deleted.'], 422);
        }
    }
}
