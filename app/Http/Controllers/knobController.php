<?php

namespace App\Http\Controllers;

use App\Jobs\KnobGeneration;
use App\Jobs\MoveKnobFile;
use App\MyClasses\URL;

class knobController extends Controller
{
    public function show($color, $max, $min, $step, $size, $value){
        $data["color"] = "#".$color;
        $data["max"] = $max;
        $data["min"] = $min;
        $data["step"] = $step;
        $data["size"] = $size;
        $data["value"] = $value;
        
        return view('knob', $data);       
    }

    public function knobs(){
        for($i=0.00; $i<0.03; $i += 0.01){
            $root = URL::to('/');
            $success = '01c0c8'; 
            $fail = 'f36e92';
            $type = 'amp';
            $url = $root.'/knob/'.$success.'/1.0/0/0.01/200/'.$i;
            KnobGeneration::dispatch($url);
            MoveKnobFile::dispatch($type, ($i*100));
        }
    }
}