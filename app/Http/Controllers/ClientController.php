<?php

namespace App\Http\Controllers;

use App\PriceBucket;
use Illuminate\Http\Request;
use Auth;
use App\TmpClient;


class ClientController extends Controller
{
    public function index()
    {
        $data['user']=Auth::User();
        $data['clients']=TmpClient::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Clients List';

        return view('admin.clients.index', $data);
    }

    public function show($id)
    {   
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['client'] = TmpClient::firstWhere('id', $id);
        $data['buckets'] = PriceBucket::all();
        $data['form_title']='Client Info';
        
        return view('admin.clients.show', $data);
    }
}
