<?php

namespace App\Http\Controllers;

use App\Mail\CustomerParserTool;
use App\MyClasses\Server;
use App\MyClasses\URL;
use Illuminate\Http\Request;
use Mail;
use Mailgun\Mailgun;
use Auth;


class CustomerParserController extends Controller{

    private $testing_server=false;

    public function __construct(){
        $server=new Server();
        $this->testing_server=$server->testing_server();

    }

    public function main(){

        $data['form_title']='PARSER TOOL';
        $data['text_area_placeholder']="URLs list";



        return view('Tools/CustomersParser',$data);
    }

    public function redraider($parsed_url=null){


        $data['form_title']= 'URL ENCODER TOOL';
        $data['post_form_url']='form.raider.post';
        $data['parsed_url']=$parsed_url;
        $data['text_area_placeholder']="Insert Product URL";
       // if(!is_null($parsed_url))dd($data);
        return view('Tools/CustomersParser',$data);

    }
    public function redraider_post_form(Request $request){
        $property_id='3a256d94-c27c-46a3-b466-ff90709c0277';
        $url_prefix=config('app.url').'/'.$property_id."/";
        $url=$request->get('urls');
        $url_encoded=$url_prefix.urlencode($url);
        $parsed_url="<a href=\"".$url_encoded."\" target=\"_blank\">".$url_encoded."</a>";

       return $this->redraider($parsed_url);
    }

    public function post_form(Request $request){
       //THIS SENDS ME AN EMAIL WITH ALL THE URLS I NEED TO PARSE
        /* $credentials = [
            'email' => $request['email'],
            'password' => $request['password'],
        ];

        if (!Auth::attempt($credentials)) {

            flash('Invalid Credentials')->error();
            return redirect()->back();

        }*/


        $valid_urls=[];
        $urls=explode("\n",$request->get('urls'));
        $j=0;
        $values = preg_split('/[\n\r\\" \\"]+/', $request->get('urls'));

        foreach ($urls as $item){
            $item=str_replace("\r","",$item);
            $urls[$j]=$item;
            $j++;
        }
        $urls=array_filter($urls);

        foreach ($urls as $item){
           if(URL::is_valid_url($item))$valid_urls[]=$item;
        }

        $invalid_urls=array_diff($urls,$valid_urls);

        if(count($valid_urls)>0){
            $data['valid_urls']=implode("<br>",$valid_urls);
            $mg = Mailgun::create('ca835dccae7a0470be2c8e7600927fd5-87cdd773-2d606455');

            $mg->messages()->send('tools.roboamp.com', [
                'from'    => 'Roberto Inetti <Robby@tools.roboamp.com>',
                'to'      => 'roberto@roboamp.com',
                'subject' => 'URLs to Parse',
                'html'=>view('emails.CustomerParserTool',$data)->render()
            ]);
            flash($data['valid_urls']."<br> Has been added to the queue");

        }
        if(count($invalid_urls)>0){
            $data['invalid_urls']=implode("<br>",$invalid_urls);
            $message="Is not a valid URL";
            if(count($invalid_urls)>1)$message="Are not valid URLs";

            flash($data['invalid_urls']."<br>".$message)->error();
        }


        $data['form_title']='PARSER TOOL';
        $data['text_area_placeholder']="URLs list";


        return view('Tools/CustomersParser',$data);

    }

}
