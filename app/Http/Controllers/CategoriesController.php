<?php

namespace App\Http\Controllers;

use App\Category;
use App\FAQ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{

    public function index()
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['categories'] = Category::withCount('question')->get();
            return view('admin.faq.category', $data);
        } else
            return redirect()->back();
    }

    public function show()
    {
        if (Auth::user()->is_admin()) {
            $data['user'] = Auth::User();
            $data['categories'] = Category::all();
            return view('admin.faq.category', $data);
        } else {
            return redirect()->back();
        }
    }

    public function store()
    {
        $res = request()->validate([
            'name' => ['required', 'min: 3']
        ]);

        //check for res
        if (!$res) {
            return response()->json(['error' => 'Something went wrong'], 422);
        } else {
            $category = Category::create($res);
            $data['success'] = 'Category saved.';
            $data['id'] = $category->id;
            $data['name'] = $category->name;
            return response()->json($data, 200);
        }
    }

    public function update(Category $id)
    {
        $res = $id->update(request()->validate([
            'name' => ['required', 'min: 3']
        ]));

        if (!$res) {
            return response()->json(['error' => 'Something went wrong'], 422);
        } else {
            return response()->json(['success' => 'Category Saved.'], 200);
        }
    }

    public function destroy(Category $id)
    {
        if ($id) {
            $id->delete();
            return response()->json(['success' => 'Category Deleted.'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong category could not be deleted.'], 422);
        }
    }
}

