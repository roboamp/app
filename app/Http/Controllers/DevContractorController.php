<?php

namespace App\Http\Controllers;

use App\DevContractor;
use App\DevProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class DevContractorController extends Controller
{
    public function index()
    {
        $data['user'] = Auth::User();
        $data['dev_contractors'] = DevContractor::all();

        return view('admin.contractors.index', $data);
    }

    public function create()
    {
        $data['user'] = Auth::User();
        $data['form_submit_button_title'] = 'Create Contractor';
        $data['form_action'] = route('admin.contractors.store');

        return view('admin.contractors.create', $data);
    }

    public function store()
    {
        $request = request()->validate([
            'name' => 'required',
            'email' => 'required'
        ]);

        DevContractor::create($request);

        return redirect(route('admin.contractors.index'));
    }

    public function show($id)
    {
        $data['dev_contractor'] = DevContractor::findOrFail($id);
        $data['user'] = Auth::user();
        $data['form_action'] = route('admin.contractors.update', $id);
        $data['form_submit_button_title'] = 'Edit Contractor';

        return view('admin.contractors.show', $data);
    }

    public function update(DevContractor $id)
    {
        $id->update(request()->validate([
            'name' => 'required',
            'email' => 'required',
        ]));

        Session::flash('active_id', $id->id);

        return redirect(route('admin.contractors.index'));
    }

    public function destroy($id)
    {
        $contractor_exist = DevContractor::findOrFail($id);

        if ($contractor_exist) {
            DevProject::where('dev_contractor_id', $contractor_exist->id)
                ->update(['status_id' => 1]);

            $contractor_exist->delete();
            return response()->json(['success' => 'Contractor Deleted.'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong contractor could not be deleted.'], 422);
        }
    }
}
