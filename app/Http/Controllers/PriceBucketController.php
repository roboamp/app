<?php

namespace App\Http\Controllers;

use App\PriceBucket;
use Illuminate\Http\Request;
use Auth;

class PriceBucketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['buckets'] = PriceBucket::all();
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_centered']=false;
        $data['form_title']='Price Buckets List';

        return view('admin.priceBuckets.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_centered']=true;
        $data['form_title']='Create Price Bucket';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.buckets.store');

        return view('admin.priceBuckets.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bucket = PriceBucket::create($request->validate([
            'name' => 'required',
            'pricing' => 'required|numeric',
            'max' => 'required|numeric'
        ]));

        return redirect()->route('admin.buckets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PriceBucket  $priceBucket
     * @return \Illuminate\Http\Response
     */
    public function show(PriceBucket $priceBucket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PriceBucket  $priceBucket
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceBucket $priceBucket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PriceBucket  $priceBucket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $bucket = PriceBucket::find($request->id);
        $bucket->name = $request->name;
        $bucket->pricing = $request->pricing;
        $bucket->max = $request->max;
        $bucket->save();

        if($bucket->id == $request->id && $bucket->name == $request->name && $bucket->pricing == $request->pricing && $bucket->max == $request->max){
            $data["success"] = true;
            $data["message"] = 'Bucket '.$bucket->name.' Succesfully edited.';
        }else{
            $data["success"] = false;
            $data["message"] = 'Bucket '.$bucket->name.' not edited.';
        }
        $data["target"] = $bucket->id;
        $data["name"] = $bucket->name;
        $data["pricing"] = $bucket->pricing;
        $data["max"] = $bucket->max;

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PriceBucket  $priceBucket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $bucket = PriceBucket::find($request->bucket);
        $bucket->delete();

        $bucket =  PriceBucket::find($request->bucket);
        if($bucket){
            $data["success"] = true;
            $data["message"] = 'Bucket Succesfuly Delete.';
        }else{
            $data["success"] = true;
            $data["message"] = 'Bucket not Deleted.';
        }
        $data["target"] = $request->bucket;

        return response()->json($data);
    }
}
