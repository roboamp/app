<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\{Charge,Customer,Stripe};

class PaymentsController extends Controller
{

    public function main(){


        Stripe::setApiKey(env('STRIPE_SECRET'));


       $customer=Customer::create([
            'email'=> request('stripeEmail'),
            'source'=>request('stripeToken')
        ]);



        Charge::create([
            'customer'=>$customer->id,
            'amount'=>25000,
            'currency'=>'usd'
        ]);

        return 'all done';


    }
    public function createStripeCustomer($token){
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $customer = \Stripe\Customer::create(array(
            "description" => Auth::user()->email,
            "source" => $token
        ));

        Auth::user()->stripe_id = $customer->id;
        Auth::user()->save();

        return $customer;
    }
    public function chargeCustomer($product_id, $product_price, $product_name, $token)
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        if (!$this->isStripeCustomer())
        {
            $customer = $this->createStripeCustomer($token);
        }
        else
        {
            $customer = \Stripe\Customer::retrieve(Auth::user()->stripe_id);
        }

        return $this->createStripeCharge($product_id, $product_price, $product_name, $customer);
    }

    public function createStripeCharge($product_id, $product_price, $product_name, $customer)
    {
        try {
            $charge = \Stripe\Charge::create(array(
                "amount" => $product_price,
                "currency" => "brl",
                "customer" => $customer->id,
                "description" => $product_name
            ));
        } catch (\Stripe\Error\Card $e) {
            return redirect()
                ->route('index')
                ->with('error', 'Your credit card was been declined. Please try again or contact us.');
        }
        return $this->postStoreOrder($product_name);

    }
    public function postStoreOrder($product_name){
        Order::create([
            'email' => Auth::user()->email,
            'product' => $product_name
        ]);

        return redirect()
            ->route('index')
            ->with('msg', 'Thanks for your purchase!');
    }
    public function isStripeCustomer(){
        return Auth::user() && \App\User::where('id', Auth::user()->id)->whereNotNull('stripe_id')->first();
    }

}
