<?php

namespace App\Http\Controllers;

use ROBOAMP\Files;
use App\MyClasses\Strings;
use Auth;
use App\Task;
use App\Activity;
use App\PagePullSchedule;
use App\Timeframe;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title'] = 'Schedule Task';
        $data['activities'] = Activity::all();
        $data['timeframes'] = Timeframe::all();
        $data['tasks']=Task::all();

        return view('admin.schedule.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title'] = 'Create Scheduled Task';
        $data['activities'] = Activity::all();
        $data['timeframes'] = Timeframe::all();
        $data['tasks']=Task::all();
        $data['form_centered']=true;
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.schedule.store');

        return view('admin.schedule.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = Task::create([
            'activity_id' => $request->activity,
            'timeframe_id' => $request->timeframe,
            'user_id' => $request->user,
            'parameters' => $request->parameters
        ]);

        $file = new Files();
        $arr_placeholders = ["//begin-child-node"];
        $subs = $this->get_template_content($task, $arr_placeholders[0]);
        $arr_subs = [$subs];
        $route_path = base_path("/app/Console/Kernel.php");
        $file->replace_all_placeholders($arr_placeholders,$arr_subs,$route_path);


        return redirect()->route('admin.schedule.index');

    }

    private function get_template_content($task, $placeholder){
        return '$schedule->command("'.$task->activity->command.' '.$task->parameters.'")->'.$task->timeframe->value.'();'."\n".$placeholder."\n";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title'] = 'Edit Scheduled Task';
        $data['activities'] = Activity::all();
        $data['timeframes'] = Timeframe::all();
        $data['task']=Task::firstWhere('id', $id);
        $data['form_centered']=true;
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=url('/admin/schedule/'.$id);

        return view('admin.schedule.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $task->activity_id = $request->activity;
        $task->timeframe_id = $request->timeframe;
        $task->parameters = $request->parameters;
        $task->save();

        $timeframe = Timeframe::find($request->old_timeframe);
        $old_task = ['$schedule->command("'.$task->activity->command.' '.$task->parameters.'")->'.$timeframe->value.'();'];
        $this->edit_kernel("", $old_task);
            
        $arr_placeholders = ["//begin-child-node"];
        $subs = $this->get_template_content($task, $arr_placeholders[0]);

        $this->edit_kernel($subs, $arr_placeholders);


        return redirect()->route('admin.schedule.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $task = Task::find($request->task);

        $old_task = ['$schedule->command("'.$task->activity->command.' '.$task->parameters.'")->'.$task->timeframe->value.'();'];
        //$arr_placeholders = [$this->get_template_content($task, "")];
        $subs = "";
        $this->edit_kernel($subs, $old_task);

        $task->delete();
        $task2 = Task::find($request->task);
        if($task2 && $this->find_schedule($old_task[0])){
            $data["success"] = false;
            $data["message"] = "Schedule Not Deleted";
        }else{
            $data["success"] = true;
            $data["message"] = "Schedule Succesfully Deleted";
        }

        $data["target"] = $task->id;

        return response()->json($data);
    }


    public function get_command(Request $request){

        $activity = Activity::firstWhere('id', $request->activity);

        $data['success'] = true;
        $data['command'] = $activity->command;

        return response()->json($data);
    }

    public function page_schedule(Request $request){

        $task = PagePullSchedule::updateOrCreate(
            [
                'page_id' => $request->page
            ],
            [
                'active' => $request->active,
                'activity_id' => 1,
                'timeframe_id' => $request->timeframe,
                'user_id' => $request->user,
            ]
        );

        $timeframe = Timeframe::find($request->old_timeframe);
        $old_task = ['$schedule->command("'.$task->activity->command.' '.$task->page->url.'")->'.$timeframe->value.'();'];

        
        if($request->active == 0){
            $data['message'] = 'Pull Schedule for page '.$task->page->name.' disabled';
            $arr_placeholders = [$this->get_template_content_page($task, "")];
            $subs = "";
            $task->timeframe_id = "1";
            $task->save();

            $this->edit_kernel($subs, $arr_placeholders);

            if($this->find_schedule($old_task[0])){
                $data['success'] = false;
                $data['debug'] = 'Old task found in file.';
            }else{
                $data['success'] = true;
                $data['debug'] = 'Old task not found. Correctly erased from file';
            }
            
        }else{
            $this->edit_kernel("", $old_task);
            
            $arr_placeholders = ["//begin-child-node"];
            $subs = $this->get_template_content_page($task, $arr_placeholders[0]);

            if($this->find_schedule($old_task[0])){
                $data['success'] = false;
                $data['debug'] = 'Old task found in file';
            }

            $this->edit_kernel($subs, $arr_placeholders);
            
            if($this->find_schedule($subs[0])){
                $data['success'] = true;
                $data['debug'] = 'Old task not found. New task found in file.';
            }

            $data['message'] = 'Pull Schedule for page '.$task->page->name.' Saved';
        }
                
        $data["task"] = $task;
        return response()->json($data);
    }

    private function get_template_content_page($task, $placeholder){
        return '$schedule->command("'.$task->activity->command.' '.$task->page->url.'")->'.$task->timeframe->value.'();'."\n".$placeholder;
    }

    public function edit_kernel($string, $placeholder){
        $file = new Files();
        $arr_placeholders = $placeholder;
        $subs = $string;
        $arr_subs = [$subs];
        $route_path = base_path("/app/Console/Kernel.php");
        $file->replace_all_placeholders($arr_placeholders,$arr_subs,$route_path);
    }

    public function find_schedule($string){
        $route_path = base_path("/app/Console/Kernel.php");
        $result = Strings::find_string_in_file($string, $route_path);
        return $result->status;
    }
}