<?php

namespace App\Http\Controllers\Amp4Email;

use App\Http\Controllers\Controller;
use App\MyClasses\Server;
use App\MyClasses\Strings;
use Carbon\Carbon;
use Illuminate\Http\Request;
use View;
use Mailgun\Mailgun;

class Examples extends Controller{

    public function method_post(){
        header("AMP-Access-Control-Allow-Source-Origin: https://".$_SERVER['HTTP_HOST']);
        if(isset($_SERVER["HTTP_ORIGIN"])) {

            switch ($_SERVER["HTTP_ORIGIN"]) {
                case "https://parser.roboamp.com":
                    //https://parser.roboamp.com
                    header("Poronga:  TAYTUS--|" . $_SERVER["HTTP_ORIGIN"]);

                    header("Access-Control-Allow-Origin: https://" . $_SERVER['SERVER_NAME']);
                    break;
                case "https://parser-roboamp-com.cdn.ampproject.org":
                    //https://parser-roboamp-com.cdn.ampproject.org
                    header("Poronga: " . $_SERVER["HTTP_ORIGIN"] . "| TAYTUS");

                    header("Access-Control-Allow-Origin: https://parser-roboamp-com.cdn.ampproject.org");
                    break;
                default:
                    header("Poronga:  TAYTUS");

                    header("Access-Control-Allow-Origin: https://mail.google.com");
                    header("AMP-Access-Control-Allow-Source-Origin: postmaster@amp.roboamp.com");

            };
        }else{

        }




        return response()->json($this->get_items(),200);

    }

    public function asr_post2(Request $request){
        header("AMP-Access-Control-Allow-Source-Origin: https://".$_SERVER['HTTP_HOST']);
        header("Access-Control-Allow-Origin: https://mail.google.com");
        header("AMP-Access-Control-Allow-Source-Origin: postmaster@amp.roboamp.com");


        $animal=$request->input('animal');
        if($animal!="") {
            //$options = ['cat' => 'miaw', 'dog' => 'guau guau', 'parrot' => 'parroting', '' => ''];
            //$data['animal'] = $options[$animal];
            //$filter="low-high|bikes.json";

            switch ($animal){
                case "cat":
                    return response()->json($this->get_items2(),200);
                case "parrot":
                    return response()->json($this->get_items3(),200);
                case "dog":
                    return response()->json($this->get_items(),200);
            }

        }else{
            return response()->json($this->get_items2(),200);

        }


        return response()->json($this->get_items2(),200);

    }


    public function send_email(){


        $mg = Mailgun::create('ca835dccae7a0470be2c8e7600927fd5-87cdd773-2d606455');
        $data['time']=new Carbon();
        $mg->messages()->send('amp.roboamp.com', [
            'from'    => 'Roberto Inetti <postmaster@amp.roboamp.com>',
            //'to'      => 'copertus@gmail.com,mkane@mailgun.com',
            'to'      => 'copertus@gmail.com',
            'subject' => 'Advanced Server Request!',
            'text'=>'hello',
            'html'=>'<h1>THIS IS AN AMAZING DEMO</h1>',
            'amp-html'    => view('amp4email.examples.send',$data)->render(),
        ]);
        dd($data['time'],"EMAIL SENT");
    }
    public function asr(){
        return view('amp4email.examples.asr');
    }
    public function asr_post(Request $request){
        header("AMP-Access-Control-Allow-Source-Origin: https://".$_SERVER['HTTP_HOST']);
        if(isset($_SERVER["HTTP_ORIGIN"])) {

            switch ($_SERVER["HTTP_ORIGIN"]) {
                case "https://parser.roboamp.com":
                    //https://parser.roboamp.com
                    header("Poronga:  TAYTUS--|" . $_SERVER["HTTP_ORIGIN"]);

                    header("Access-Control-Allow-Origin: https://" . $_SERVER['SERVER_NAME']);
                    break;
                case "https://parser-roboamp-com.cdn.ampproject.org":
                    //https://parser-roboamp-com.cdn.ampproject.org
                    header("Poronga: " . $_SERVER["HTTP_ORIGIN"] . "| TAYTUS");

                    header("Access-Control-Allow-Origin: https://parser-roboamp-com.cdn.ampproject.org");
                    break;
                default:
                    header("Poronga:  TAYTUS");

                    header("Access-Control-Allow-Origin: https://mail.google.com");
                    header("AMP-Access-Control-Allow-Source-Origin: postmaster@amp.roboamp.com");

            };
        }else{

        }



        $animal=$request->input('animal');
        if($animal!="") {
            $options = ['cat' => 'miaw', 'dog' => 'guau guau', 'parrot' => 'parroting', '' => ''];
            $data['animal'] = $options[$animal];
            $filter="low-high|bikes.json";

        }else{
            $filter = "high-low|all-products.json";

        }



        $filter=$this->setup_filter($filter);
        $view_name=$filter['direction']."-".$filter['product'];

        $path_to_view=base_path('public/amp4email/ecommerce/api');

        $my_view=view::addNamespace('json',$path_to_view);


        $data['items']=[
            ["name"=> "Chain set","description"=> "Silver alloy construction for durability.","price"=> 867,"image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-3.jpg"],
            ["name"=> "Something","description"=> "BOOM!","price"=> 867,"image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-4.jpg"]
        ];
        return response()->json($this->get_items(),200);

    }
    public function setup_filter($filter){
        $arr=explode("|",$filter);

        if(count($arr)==2) {
            $product = explode(".json", $arr[1]);
            $arr['product'] = $product[0];
            $arr['direction'] = $arr[0];
        }else{

        }
        return $arr;

    }



    public function amplist(){
        $server=new Server();
        //if($server->local_server){
        //the path will be the local folder
        //I need to generate a JSON file and return that file
        $source_path="'/email/ecommerce/api/'";
        $source_path=$source_path." + products.filter + '-' + products.category +'.json'";
        //}
        $data['source_path']=$source_path;
        return view ('amp4email/components/amplist',$data);
    }
    public function get_items(){
        return array("items"=>[
            [
                "name"=> "Horn Handles",
                "description"=> "Grippingly durable and stylish.",
                "price"=> 838,
                "image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-7.jpg",
                "category"=> "components"
            ],
            [
                "name"=> "Caliper Brakes",
                "description"=> "Fits most wheel sizes and designed to last long.",
                "price"=> 776,
                "image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-8.jpg",
                "category"=> "components"
            ],
            [
                "name"=> "Red Cruiser",
                "description"=> "Smooth ride for enjoyable cruising.",
                "price"=> 688,
                "image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-6.jpg",
                "category"=> "accessories"
            ]

        ]);

    }

    public function get_items2(){
        return array("items"=>[
            [
                "name"=> "Red Cruiser",
                "description"=> "Smooth ride for enjoyable cruising.",
                "price"=> 688,
                "image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-6.jpg",
                "category"=> "accessories"
            ]


        ]);

    }
    public function get_items3(){
        return array("items"=>[
            [
                "name"=> "Caliper Brakes",
                "description"=> "Fits most wheel sizes and designed to last long.",
                "price"=> 776,
                "image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-8.jpg",
                "category"=> "components"
            ],
            [
                "name"=> "Red Cruiser",
                "description"=> "Smooth ride for enjoyable cruising.",
                "price"=> 688,
                "image"=> "https://parser.roboamp.com/img/amp4email/templates/e-commerce/product/product-6.jpg",
                "category"=> "accessories"
            ]


        ]);

    }


}


