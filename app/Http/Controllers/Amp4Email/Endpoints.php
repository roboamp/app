<?php

namespace App\Http\Controllers\Amp4Email;

use App\Http\Controllers\Controller;
use View;

class Endpoints extends Controller{

    public function get_ecommerce($filter=null){
        if(!is_null($filter)){

            $filter=$this->setup_filter($filter);
            $view_name=$filter['direction']."-".$filter['product'];

            $path_to_view=base_path('public/amp4email/ecommerce/api');

            $my_view=view::addNamespace('json',$path_to_view);
           // dd($view_name);
            $view=view('json::'.$view_name)->render();
            return response()->json(json_decode($view));

        }else{
          //  $view=$filter."-".$category;
            $view=view('amp4email.components.amplist')->render();
            return $view;
        }

    }
    public function setup_filter($filter){
        $arr=explode("|",$filter);

        if(count($arr)==2) {
            $product = explode(".json", $arr[1]);
            $arr['product'] = $product[0];
            $arr['direction'] = $arr[0];
        }else{

        }
        return $arr;

    }

    public function get_hotels(){

        $res= json_decode('{"items": [{
    "id": 1,
    "img": "/static/samples/img/product1_640x426.jpg",
    "name": "Apple",
    "price": "1.99",
    "stars": "&#9733;&#9733;&#9733;&#9733;&#9733;",
    "attribution": "visualhunt",
    "url": "#",
    "color": "green"
  }, {
    "id": 2,
    "img": "/static/samples/img/product2_640x426.jpg",
    "name": "Orange",
    "attribution": "visualhunt",
    "price": "0.99",
    "stars": "&#9733;&#9733;&#9733;&#9733;&#9734;",
    "url": "#",
    "color": "orange"
  }, {
    "id": 3,
    "img": "/static/samples/img/product3_640x426.jpg",
    "name": "Pear",
    "attribution": "visualhunt",
    "price": "1.50",
    "stars": "&#9733;&#9733;&#9733;&#9734;&#9734;",
    "url": "#",
    "color": "green"
  }, {
    "id": 4,
    "img": "/static/samples/img/product4_640x426.jpg",
    "name": "Banana",
    "attribution": "pixabay",
    "price": "1.50",
    "stars": "&#9733;&#9733;&#9733;&#9733;&#9733;",
    "url": "#",
    "color": "yellow"
  }, {
    "id": 5,
    "img": "/static/samples/img/product5_640x408.jpg",
    "name": "Watermelon",
    "attribution": "pixabay",
    "price": "4.50",
    "stars": "&#9733;&#9733;&#9733;&#9733;&#9733;",
    "url": "#",
    "color": "red"
  }, {
    "id": 6,
    "img": "/static/samples/img/product6_640x424.jpg",
    "name": "Melon",
    "attribution": "pixabay",
    "price": "3.50",
    "stars": "&#9733;&#9733;&#9733;&#9733;&#9733;",
    "url": "#",
    "color": "yellow"
  }]}');

        return response()->json($res);
    }
}


