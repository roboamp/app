<?php

/*
 *
 */
namespace App\Http\Controllers;
use Illuminate\Http\Response;

use Illuminate\Http\Request;
use App\Demo;
use Carbon\Carbon;
use Auth;
use App\MyClasses\Debug;
use App\User;
use ROBOAMP\MyArray;



class AdminController extends Controller{

    private $view_section='admin.demos.';

    public function __construct(){

        $this->middleware('auth');
    }

    public function index(){

        $data=$this->setup_basic_data_info();

        $data['user']=Auth::User();

        $data['demos']=$data['user']->demos()->get();

        //if(count($data['demos'])==0)dd("redirect to demos creation form");

        $data['table_headers']=['Name','URL','Status','ETA'];

        $view=$this->get_view_section();
//       dd($view);
        return view($view,$data);
    }

    public function show(Request $request){
        //by default displays a list with all the available demos
        dd(__METHOD__,"BATMAN");
        $data=$this->setup_basic_data_info($request);

        return view('admin.demo_pages',$data);




        $last_visited_section=$request->session()->get('last_visited_section');

        if(is_null($last_visited_section)){
            //load the list of demos
            $res=session(['last_visited_section'=>"some_section"]);
            dd('it was null, but is not null anymore :) ',$last_visited_section,$res);
        }else{
            dd('now there is something',$last_visited_section);
        }

        $view= view('admin/dashboard',$data);
        $response = new Response($view);
        return $response->withCookie('something','value',60);

    }

    public function generate_data($model=null){
        $faker_js = new Faker_js();

        if($faker_js->get_testing() == true){
            $data['faker_js']= $faker_js->create($model, 1);
        // $data['faker_js']= $faker_js->create_x_random($model, 1);
            return;
        }

    }

    public function get_records_created_during_x_months($table=null,$months=1){

        $now=Carbon::now();
        $past=Carbon::now()->subMonth($months);

        $obj=$model::whereBetween('created_at', [$past,$now])->get();
            
        return $obj;

    }

    public function get_records_created_during_this_month($table=null){

        $model="App\\".$table;
        $now=Carbon::now();
        $start_of_month=Carbon::now()->startOfMonth();
        $obj=$model::whereBetween('created_at', [$start_of_month,$now])->get();
            
        return $obj;

    }


    public function get_data(Request $request){

        foreach ($request->counters as $obj) {
            $arr=explode(',', $obj);
            $data['response_'.$arr[0]]['units'] = $this->get_records_created_during_this_month($arr[2])->count();
        }

        foreach ($request->widgets as $obj) {
            $arr=explode(',', $obj);
            $data['response_'.$arr[0]]['new_properties'] = $this->get_records_created_during_this_month($arr[2])->count();
            $data['response_'.$arr[0]]['number_of_properties'] = floor(($data['response_'.$arr[0]]['new_properties']/ $arr[3]) * 100);
        }

        return json_encode($data);
    }


    public function test(){
        $data = $this->get_records_created_during_x_months('property', 4);
        dd($data);
    }

    //these vars are commonly used on the dashboard views
    //exposed here as a simple way to debug
    private function setup_basic_data_info(){
        $data['user']=Auth::User();
        $data['messages']=null;
        $data['comments']=null;
       // $data['section_name']=ucfirst($request->route()->getName());
        $data['card_name']="Radisson Hotels - AMP Pages";

        //for tables

        $data['hide_table_label']=true;

        return $data;
    }

    private function get_view_section(){
        $slug=Debug::called_from_controller_method();
        return $this->view_section.$slug;
    }



}
