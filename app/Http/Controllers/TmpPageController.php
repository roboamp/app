<?php

namespace App\Http\Controllers;

use App\TmpPage;
use Illuminate\Http\Request;

class TmpPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TmpPage  $tmpPage
     * @return \Illuminate\Http\Response
     */
    public function show(TmpPage $tmpPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TmpPage  $tmpPage
     * @return \Illuminate\Http\Response
     */
    public function edit(TmpPage $tmpPage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TmpPage  $tmpPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TmpPage $tmpPage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TmpPage  $tmpPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(TmpPage $tmpPage)
    {
        //
    }
}
