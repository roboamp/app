<?php

namespace App\Http\Controllers;

use App\TmpClient;
use App\TmpProperty;
use App\Timeframe;
use Auth;
use Illuminate\Http\Request;

class PullScheduleController extends Controller
{
    public function index(){
        $data['user']=Auth::User();
        //$data['clients']=TmpClient::where('user_id', Auth::User()->id)->get();
        $data['clients']=TmpClient::all();
        $data['timeframes'] = Timeframe::all();
        $data['form_title']='Property List - Pull Schedule';
        
        return view('admin.pullSchedule.index', $data);
    }

    public function show($id){
        $property = TmpProperty::findOrFail($id);

        $data['user']=Auth::User();
        $data["property"] = $property;
        $data['timeframes'] = Timeframe::all();
        $data['form_title']='Pull Schedule Setup';

        return view('admin.pullSchedule.show', $data);
    }

    public function get_properties($id){
        $client = TmpClient::findOrFail($id);
        
        $data['success'] = true;
        $i = 0;
        foreach($client->properties as $property){
            $data["properties"][$i]["property"] = $property;
            $data["properties"][$i]["pages"] = $property->pages->count();
            $data["properties"][$i]["bucket"] = $property->bucket;
            $i++;
        }

        return response()->json($data);
    }


    public function get_pages(Request $request){
        $property = TmpProperty::find($request->property);
        
        $data['success'] = true;
        $data['pages'] = $property->pages;
        $data['property'] = $property->id;

        return response()->json($data);
    }
}
