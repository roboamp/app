<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use Auth;


class PartnerController extends Controller{

    public function show(){
        $user = Auth::user();

        $data['user'] = $user;

        return view('partners.index', $data);

    }
    
}
