<?php

namespace App\Http\Controllers;

use App\MyClasses\MyArray;
use App\MyClasses\Server;
use Illuminate\Http\Request;
use App\MyClasses\URL;
use App\Plan;
use App\Customer;
use App\Property;
use Webpatser\Uuid\Uuid;
use Auth;
use Faker;
use App\Platform;
use App\User;
use App\MyClasses\Seeders;
use App\Subscription;
use App\Notifications\PropertyNotification;
use App\Step;

class CustomerController extends Controller
{

    private $testing;

    //the construct always runs first;
    public function __construct(){
        $this->testing=true;
    }

    public function redraider(){

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data['testing'] = $this->testing;
        if($data['testing'] == true){
            $data['testing'] = $this->setup_testing_array();
        }

        $data['customer'] = Customer::find($id);
        $data['sites'] = Property::where('customer_id', '=', $id)->paginate(10);

        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


        if($request->input('name') == '' || $request->input('plan_stripe_id') == '' || $request->input('url') == ''){
            $data['error'] = 1;
            return json_encode($data);
        }
        else {
            //check if the $edit_customer_edit has been set
            //if not that means this is a new user,
            // otherwise we are adding a website to existent user

            $user=Auth::user();

            //before creating a subscription: Check if that domain exist

            $domain_exist=$this->check_if_domain_exist($request->input('url'));

            if(is_array($domain_exist)) return json_encode($domain_exist);

            //save data into stripe

            $message= $user->create_subscription($request);
            if(is_array($message) && $message['error']==0){

                //saves data into the DB
                if(is_null($request->edit_customer_id)) {
                    $customer_id = $this->create_customer($request, $user->id);
                    $success_message="Customer has been created";
                }else{
                    $customer_id=$request->edit_customer_id;
                    $success_message="Property has been added to customer ".$request->name;
                }
                //creates the property
                $message=$this->create_property($request,$customer_id);
                if(!is_array($message)) {
                    $message=["error"=>0,"success"=>$success_message];

                }
            }

            return json_encode($message);

        }



    }
    //display the plan selection view
    public function add_property(){
        $server = new Server();
        $MyArray=new MyArray();
        $plan=new Plan();
        $faker= Faker\Factory::create();

        $user=Auth::guard('customers')->user();
        $data['stripe_email']=$user->email;
        //used for the buttons on plan selection
        //hide or display specific plans depending on coupons
        $data['plans']=$plan->get_active_plans_and_coupon_X($user->coupon_id);

        //image displayed on Stripe Form
        $data['stripe_form_image']=asset('img/ROBO_stripe.png');
        $data['stripe_key']=$server->get_public_stripe_key();
        $data['customer']=$user;
        $data['callback']='customers.process_stripe_url';
        $data['default_plan']=$data['plans'][0]->stripe_id;
        $data['url']=($server->testing_server()?'https://'.$faker->domainName:"https://");
        $data['testing']=$server->testing_server();
        $data['plans']= $MyArray->setup_default_option($data['plans'],$data['default_plan']);


        return view('customers.add', $data);
    }



    public function add_property_submit(Request $request,$beta=0){

        $user = new User();
        $validatedData = $request->validate([
            'url' => 'required|url',
        ]);

        //before creating a subscription: Check if that domain exist
        $url=new URL();
        $domain_exist=$url->check_if_domain_exist_in_db($request->input('url'));
        if(is_array($domain_exist)) return json_encode($domain_exist);

        $customer=Auth::guard('customers')->user();
        $message= $user->create_subscription($request,true,$customer->email);

        $subscription_id=$message['subscription_id'];
        if(is_array($message) && $message['error']==0){

            //Creates a customer if no customer_id is passed
            // if(is_null($request->edit_customer_id) && $renew_subscription==false) {
            //     $customer_id = $this->create_customer($request, $user->id);
            //     $success_message="Customer has been created";
            // }else{
            //     if($renew_subscription==false) {
            //         $customer_id = $request->edit_customer_id;
            //         $success_message = "Property has been added to customer " . $request->name;
            //     }
            // }
            //creates the property if we are not renewing a subscription
            $renew_subscription = false;

            if($renew_subscription==false) {
                $message = $this->create_property($request, $customer->id, $subscription_id);
                if (!is_array($message)) {
                    $message = ["error" => 0, "success" => 'This website has been added to your account'];

                }
            }
        }

        return json_encode($message);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_dashboard(){
        $steps=new Step();
        $platforms=new Platform();
        $data['customer'] = Auth::guard('customers')->user();

        $data['platforms']= $platforms->show_platforms($data['customer']->properties);


        $data['steps']=$steps->get_steps_for_properties($data['customer']->properties);

        //HTML is set to default platform
        $data['default_platform'] = Platform::first();
        $data['login_type']='customers';
        $data['add_website_url']=route('customers.add.property',$data['customer']->id);
        $data['property']=null;

        return view('customers.dashboard.show', $data);
    }

    private function setup_testing_array(){
        $faker = Faker\Factory::create();

        $data['url'] = $faker->url();
        $data['name'] = $faker->name();
        
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $status)
    {
        $properties = Property::where('customer_id', '=', $id)->get();

        foreach($properties as $property){
            $property->status_id = $status;
            $property->save();
        }

        $data['error'] = 0;
        

        return json_encode($data);

    }

    public function edit_show($id){
        $customer = Customer::find($id);
        $data['customer'] = $customer;
        $data['plans'] = Plan::all();


        return view('users.edit', $data);
    }

    public function edit_customer(Request $request, $id){
        $customer = Customer::find($id);
        $customer->name = $request->input('name');
        $customer->plan_id = $request->input('plan_id');
        $customer->save();
        

        return redirect('/');

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->plan_id = 3;
        $customer->save();
        $data['error'] = 0;

        return json_encode($data);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function create_customer($request,$user_id){
        $customer = new Customer;
        $customer->name = $request->input('name');
        $customer->user_id = $user_id;
        $customer->save();
        return $customer->id;
    }
    private function create_property($request,$customer_id, $subscription_id=null,$coupon_id=null){




        $domain=$request->input('url');

        if(is_array($domain)) return $domain;

        $plan=new Plan();
        $property = new Property;
        $property->id = Uuid::generate(4);
        $property->customer_id = $customer_id;
        $property->plan_id =$plan->get_plan_id_from_stripeID($request->input('plan_stripe_id'));
        $property->status_id = 2;
        $property->url =$domain;
        $property->main_website = 1;
        $property->steps_id=99;
        $property->steps_id=1;
        $property->platform_id=1;
        $property->coupon_id=$coupon_id;
        $property->save();

        //update the subscription

        $notification= $property->send_notification("New Property has been created");

        //notifications are being triggered

        $subscription = Subscription::where('id', $subscription_id)->update(['property_id'=> $property->id]);
        return $property->id;
    }
    //check if this domain has already been added to the database
    private function check_if_domain_exist($domain){

        $url=new URL();
        $domain=$url::get_domain($domain);

        $count_record=Property::where('url',$domain)->count();

        if($count_record){
            return ['error'=>7,'error_message'=>'This domain already exist'];
        }else{
            return $domain;
        }
    }
    public function indexReferredCustomer(Request $request, $id){

        $customer = Customer::find($request->cookie('loggedin'));


        if($request->cookie('loggedin')){
            if($request->cookie('loggedin') == $id){
                $data['customer'] = $customer;
                $data['loggedin'] = true;
                return view('users.index', $data);
            }else{
                return redirect('/customers/' . $request->cookie('loggedin'));
            }
        }

        return redirect('/login/customers');
    }
    public function add_property_beta(Request $request){
        $property=new Property();


        $user = new User();
        $validatedData = $request->validate([
            'url' => 'required|url',
        ]);


        //before creating a subscription: Check if that domain exist
        $url=new URL();
        $domain_exist=$url->check_if_domain_exist_in_db($request->input('url'));

        if(is_array($domain_exist)) return json_encode($domain_exist);

        $customer=Auth::guard('customers')->user();

        if(!$customer->at_least_one_property()){
            $coupon_id=$customer->coupon_id;

            $message= $user->create_beta_subscription($request,$customer->email);

            $subscription_id=$message['subscription_id'];

            if(is_array($message) && $message['error']==0){
                $message = $this->create_property($request, $customer->id, $subscription_id,$coupon_id);
                if (!is_array($message)) {
                    $message = ["error" => 0, "success" => 'This website has been added to your account'];

                }

            }
        }else{
            $message = ["error" => 0, "error" => 'You can only add ONE website on the Beta Plan'];
        }

        return json_encode($message);

    }



}
