<?php
/*
 * This controller is the one called from ***EVERY*** client
 *
 */
namespace App\Http\Controllers;

use App\MyClasses\Output;
use App\MyClasses\URL;
use App\Page;
use App\Property;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Config;
use App\MyClasses\Parser;
use App\MyClasses\Errors as e;
use App\Template;
use App\MyClasses\Curl;
use App\MyClasses\Strings;
use App\Includes;
use View;
use App\MyClasses\MyClasses as MyClass;
use App\MyClasses;
use KubAT\PhpSimple\HtmlDomParser;
use App\Generated;
use App\MyClasses\Files;
use App\MyClasses\Errors;
class RenderController extends Controller
{

    //only to detect if has been rendered already
    protected $rendered=false;
    //page I'm going to render
    protected  $render;
    protected $dev=false;
    protected $property_error_page;
    protected $testing_server;
    private $view_name;
    private $my_view;


    public function page(){

    /*    $content=view('page')->render();
        $new_content=str_replace('<meta name="generator" content="https://html2amp.mobilizetoday.ru">','<meta name="generator" content="https://roboamp.com">',$content);
        $content=str_replace('Initiator: MobilizeToday AMP Generator','Initiator: ROBOAMP AMP Generator',$new_content);

        */

        return view(
            'page');
    }
    //replaces pre content with html entities
    public function pre_entities($matches=null) {
        return str_replace($matches[1],htmlentities($matches[1]),$matches[0]);
    }
//to html entities;  assume content is in the "content" variable


    public function main($property_id,$page="", Request $request){

        if($request->dev===env('DEV_KEY')){
            $this->dev=true;
        }
        $this->dev=false;

        $variant=null;
        if($property_id ==1 && $page==1){
            return abort(404,'There is no property with that ID');
        }
        //forced for coravana
        //TODO apply similiar logic for all the properties
        if($property_id=='1262c4a8-0441-462b-a036-fe596051898b')$variant = $request->variant;
        if($variant)$page.="?variant=".$variant;

        $server=new MyClasses\Server();
        $this->testing_server=$server->testing_server();
        $output=new Output($this->testing_server);
        //first detect if it is a bot



        /* @var $property \App\Property */
        $property=Property::find($property_id);


        if($property!=null){
            $this->property_error_page=$property->error_page;
            if($property->status->status=="Active" || $property->status->status=="Beta"){


                //property is valid, now validate that that the domain or subdomains are valids
                //remove trailing slash
                $url = rtrim($page, '/');
                $page_url=URL::get_domain($url);
                if($page_url!=$property->url && $page_url!=$property->subdomain){
                    return abort(404,'Property Exist but Page\'s domain and subdomain do not match the Property');
                }






                $slugs=URL::get_slugs();

                if(!$property->cleared_slugs($slugs))return abort(404,'Invalid Slug');



                $parser=new Parser();

                //check if the page exist
                $page_exist= $parser->validate_page($url,$property_id);

                if($page_exist){

                    $this->render=$page_exist;
                    $this->render->canonical=$url;
                    return $output->render($this->render,$property,$this->dev);

                }else{
                    if($property->status_id!=5) {
                        //the page doesn't exist, analyze it and check for fingerprinting
                            $finger_print = $this->finger_print($property);
                            if ($finger_print) {return $finger_print;}

                        $this->exit_code();
                    }else{
                        return abort(404,'Property Exist but Page has not been Found');
                    }
                }


                //call the render function
                //check if less than 24 hours to re-render



            }else{
                return abort(404,'Property not active');
            }

        }else{
            $error_message='There is no property with that ID';
            //if($this->dev)e::dd($error_message,$property_id,$request);
            return abort(404,$error_message);
        }
    }
    private function _bot_detected() {

        return (
            isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT'])
        );
    }
    ///PROCESS


    //receives a property Object.
    public function finger_print($property){
        $error_code="404";
        $url=new URL();

        $customer_page=URL::decode_current_url($property->id);

        if(!URL::is_valid_url($customer_page)) return false;


        $customer_page_domain=URL::get_domain($customer_page);


        $property_url=URL::get_domain($property->url);
        $property_subdomain=URL::get_domain($property->subdomain);

        //before doing anything, I check if the page has been cached before
        //if so, serve the cached version

        $data['property_url']=$customer_page_domain;
        $folder_to_property=base_path('parser/properties/'.$customer_page_domain);

        $cached=$this->check_for_cached_page($customer_page,$folder_to_property,$property);

        if($cached!=null) return $cached;

        $res=new Curl($customer_page);
        $html=$res->get_content();
        $dom=HtmlDomParser::str_get_html($html);

        $dom=$this->update_links($dom,$customer_page_domain);

        //$dom=$this->remove_amp_errors($dom);

        $vars_array=[];
        //I have a response from the server, //get all the possible templates
        //each template has a signature that we need to verify,
        //it could be NULL for index pages
        $templates=Template::where('property_id',$property->id)->with('ampscripts')->get();

        if(count($templates)==0){
            if($this->testing_server){
                dd('There is no template for this property',
                    "Property ID = ".$property->id,
                    "Template URL = ".urldecode("https%3A%2F%2Fblog.ioogo.com"),
                    $html);
            }else{
                $this->exit_code('703', 'No template available for this page');
            }
        }


        //parse the template


        foreach ($templates as $item){
            $vars_array['ampscripts']=$item->ampscripts->toArray();

            if($item->signature!="") {
                if (Strings::find_string_in_string($html, $item->signature)) {
                    //we know the template, now I need to extract the variables
                    //each template will need different variables,
                    $includes=Includes::where('template_id','=',$item->id)->get();
                    foreach ($includes as $obj){
                        $code=json_decode($obj->node);
                        $code->obj=(isset($code->obj)?$code->obj:false);
                        $code->process=(isset($code->process)?$code->process:false);
                        switch ($code->type){
                            case 'tag':
                                $res=$dom->find($code->tag_name,0)->text();
                                break;
                            case 'header_link':
                                $res=$dom->find('head link[rel='.$code->header_link.']', 0)->href;
                                break;
                            case 'class':


                                if($code->obj==true){
                                    $res=$dom->find("[class={$code->class_name}]",0)->innertext();
                                }else{
                                    $res=$dom->find("[class={$code->class_name}]",0)->{$code->data};
                                }

                                if($code->process==true){
                                    $class=$code->callback->class;
                                    $method=$code->callback->method;
                                    $res=MyClass::call_method($class,$method,$res);
                                }


                                break;
                            case 'div':
                                //this means to grab all the childs
                                if($code->obj==true){

                                    $target=$dom->find('div[id='.$code->div_id.']',0);

                                    if(!is_null($target)) {
                                        $res =$target->innertext();
                                        if($code->div_id=='searchspring-options'){
                                            //dd($code->div_id,'batman');

                                        }
                                    }else{
                                        e::dd('target is null for div '. $code->div_id,$res);
                                    }

                                }
                                if($code->process==true){
                                    $class=$code->callback->class;
                                    $method=$code->callback->method;
                                    $res=MyClass::call_method($class,$method,$res);
                                }
                                break;
                        }
                        $vars_array[$obj->name]=Strings::delete_tabs($res);
                    }
                    // e::dd($vars_array);
                    //call the view with all the variables
                    //add the namespace
                    $data['property_url']='redraideroutfitter.com';

                    $folder_to_property=base_path('parser/properties/'.$data['property_url']);
                    $my_view=View::addNamespace($data['property_url'],$folder_to_property);
                    View::addNamespace('includes',$folder_to_property."/includes/");
                    $hints=$my_view->getFinder()->getHints();
                    return view("{$data['property_url']}::{$item->name}",$vars_array);

                }else{
                    return abort(404,'Template exist but Signature has not been found for this URL');

                }

                // echo $item->name."     ".$item->code."<br>";

                //$res=$dom->find('title');
                //   $res=$dom->find($item->node);

                // e::dd($res[0]->text());


                /*   if (Strings::find_string_in_string($html, $item->signature)) {

                       foreach ($includes as $obj){
                           $code=json_decode($obj->code);
                          // if($obj==$includes[5])e::dd($code,$includes,$code->start,$html);
                           $var=Strings::get_string_between($html,$code->start,$code->end);
                           $vars_array[$obj->name]=htmlspecialchars_decode($var);
                       }

                       e::dd($vars_array);

                       //call the view with all the variables
                       //add the namespace
                       $data['property_url']='redraideroutfitter.com';

                       $folder_to_property=base_path('parser/properties/'.$data['property_url']);
                       $my_view=View::addNamespace($data['property_url'],$folder_to_property);
                       View::addNamespace('includes',$folder_to_property."/includes/");
                       $hints=$my_view->getFinder()->getHints();
                       return view("{$data['property_url']}::{$item->name}",$vars_array);


                       e::dd('done');

                       $data['page_name']='product';
                       $view="{$data['property_url']}::{$data['page_name']}";

                       e::dd($view,$hints);

                       return $view($view,$vars_array);

                       e::dd($vars_array);
                       e::dd('found it!', $item->id, $item->signature);
                   };
              */
            }
            //echo $item->signature."<br>";
        }



       //this goes in a live server
        $res=URL::curl($customer_page);

        return $res;


        $current_url=URL::current();
        $res=$url->check_for_roboamp_code($current_url);
        dd($res);
        return $res;

    }

    private function remove_amp_errors($dom){

        $res=$dom->find();

    }
    private function update_links($dom,$customer_page_domain){
        //validate all the links
        foreach($dom->find('a') as $element) {

            if($element->href[0]=="/"){
                $element->href="https://".$customer_page_domain.$element->href;
            }
        }
        return $dom;
    }
    private function exit_code($error_code="404",$error_message_index=0){
        $error=['404'=>[
                'Property exists but Page Doesn\'t',
                'Page doesn\'t belong to this Property',

            ]
        ];

        $error_code_array=$error["{$error_code}"];
        $error_message=$error_code_array[$error_message_index];


        return Errors::abort($error_code,$error_message,$this->property_error_page);
    }
    private function check_for_cached_page($customer_page,$folder_to_property,$property){

        $record = DB::table('r_pages_generated')
            ->where('created_at', '>',
                Carbon::now()->subHours(24)->toDateTimeString()
            )->where('url',$customer_page)->first();


        if($record){
            //call the view with all the variables
            //add the namespace
            $this->my_view=View::addNamespace("generated",$folder_to_property."/generated");
            $view_name=str_replace(".blade.php","",$record->view_name);
            $this->view_name="generated::{$view_name}";
            //before returning the view, check if the view exist!
            if(view()->exists($this->view_name)){
                return view($this->view_name);
                // return $view;
            }else{
                $this->reset_record($property,$customer_page,$record);
            }
        }else{
            $this->create_new_record($property,$customer_page);
        }
        return null;
    }

    private function reset_record($property,$customer_page,$record){
        Generated::where('id',$record->id)->delete();
        $this->create_new_record($property,$customer_page);
    }
    private function create_new_record($property,$customer_page){
        //it should enter here only if the view Exist
        $customer_page_domain=URL::get_domain($customer_page);
        $view_name=$this->save_new_generated_page($property,$customer_page);
        $folder_to_property=base_path('parser/properties/'.$customer_page_domain);
        $my_view=View::addNamespace($customer_page_domain,$folder_to_property);

        return View::addNamespace('includes',$folder_to_property."/includes/");

    }


    private function save_new_generated_page($property,$customer_page){
        $now=\Carbon\Carbon::now();
        $now=$now->micro;
        $view_name=$property->id."_".$now.".blade.php";
        $generated=new Generated();
        $generated->property_id=$property->id;
        $generated->created_at=now();
        $generated->updated_at=now();
        $generated->url=$customer_page;
        $generated->view_name=$view_name;
        $generated->save();
        return $view_name;
    }





    //returns if the page exists on the DB or not;



}
