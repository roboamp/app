<?php

namespace App\Http\Controllers;

use App\DevContractor;
use App\DevPage;
use App\DevProject;
use App\MasterStatus;
use App\Pivot_timeline_status;
use App\Timeline;
use Illuminate\Support\Facades\Auth;
use Session;

class DevProjectController extends Controller
{
    public function index()
    {
        $data['user'] = Auth::User();
        $data['dev_projects'] = DevProject::all();
        $data['dev_contractors'] = DevContractor::all();

        return view('admin.devQA.index', $data);
    }

    public function create()
    {
        $data['user'] = Auth::User();
        $data['dev_contractors'] = DevContractor::all();
        $data['form_submit_button_title'] = 'Create Project';
        $data['form_action'] = route('admin.devQA.store');

        return view('admin.devQA.create', $data);
    }

    public function store()
    {
        $request = request()->validate([
            'name' => 'required',
            'dev_contractor_id' => 'nullable'
        ]);

        $contractor_assigned = request('dev_contractor_id');

        // when project is created it's status is set up to created, but if it has a contractor
        //associated with it, it's status will be assigned
        $contractor_assigned ? $request['status_id'] = 2 : $request['status_id'] = 1;

        $project = DevProject::create($request);

        $page_names = request()->get('page_name');
        $page_urls = request()->get('page_url');

        $i = 0;
        foreach ($page_names as $name) {
            $project->dev_pages()->create(['name' => $name, 'url' => $page_urls[$i]]);
            $i++;
        }

        return redirect(route('admin.devQA.index'));
    }

    public function show($id)
    {
        $data['dev_project'] = DevProject::findOrFail($id);
        $data['user'] = Auth::user();
        $data['dev_contractors'] = DevContractor::all();
        $data['dev_pages'] = DevPage::where('dev_project_id', '=', $id)->get();
        $data['form_action'] = route('admin.devQA.update', $id);
        $data['form_submit_button_title'] = 'Edit Project';

        return view('admin.devQA.show', $data);
    }

    public function update(DevProject $id)
    {
        $contractor_assigned = request('dev_contractor_id');
        $page_name_add = request('page_name');
        $page_url_add = request('page_url');
        $page_id = request('page_id');
        $page_names_update = request('page_name_update');
        $page_urls_update = request('page_url_update');

        $request = request()->validate([
            'name' => 'required',
            'dev_contractor_id' => 'nullable'
        ]);

        $contractor_assigned == null ? $request['status_id'] = 1 : $request['status_id'] = 2;

        $id->update($request);

        $i = 0;
        foreach ($page_name_add as $name) {
            if (!$name == '' || !$page_url_add[$i] == '') {
                $id->dev_pages()->create(['name' => $name, 'url' => $page_url_add[$i]]);
                $i++;
            }
        }

        if ($page_id != null) {
            $i = 0;
            foreach ($page_id as $page) {
                $page = DevPage::find($page);
                $page->update(['name' => $page_names_update[$i], 'url' => $page_urls_update[$i]]);
                $i++;
            }
        }

        Session::flash('active_id', $id->id);

        return redirect(route('admin.devQA.index'));
    }

    public function destroy_project_pages($id)
    {
        $page_exist = DevPage::findOrFail($id);

        if ($page_exist) {
            $page_exist->delete();
            return response()->json(['success' => 'Page Deleted.'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong page could not be deleted.'], 422);
        }
    }

    public function destroy($id)
    {
        $project_exist = DevProject::findOrFail($id);

        if ($project_exist) {
            $project_exist->delete();
            return response()->json(['success' => 'Project Deleted.'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong project could not be deleted.'], 422);
        }
    }
}
