<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Auth;
use App\Feature;

class EngagementController extends Controller
{
    public function index(){
        $data['features'] = Feature::all();
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Engagement Calculator';

        return view('admin.calculator.index', $data);
    }


    public function downloadpdf(Request $request){
        //dd($request);
        $features = array();
        foreach($request->features as $item){
            $features[] = Feature::firstWhere('id', (int)$item);
        }
        $data ["description"] = $request->add_description;
        $data ["customer"] = $request->customer;
        $data ["features"] = $features;
        $data ["total"] = $request->total;

        $pdf_view = PDF::loadView('admin.calculator.pdf', $data);
        return $pdf_view->download('engagement_estimation.pdf');
    }
}
