<?php

namespace App\Http\Controllers;

use App\MyClasses\Footer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class mainController extends Controller
{
    private $template_name;


    public function amp(){



        return  view('amp.countdown')
            ->withHeaders([
                'AMP-Same-Origin' => 'true',
                'Access-Control-Allow-Origin' => '*',
            ]);
    }

    public function countdown(){
        return view('countdown');
    }

    public function scenic(){
        return view('google');

        $date="2018-10-07T12:02:41Z";
        $headline="Taytus is AMAZING!!";
        $path="http://this.is.an.url/";
        $this->temlate_name="scenic";

        $data['date']=$date;
        $data['headline']=$headline;
        $data['path']=$path;
        $data['theme']=$this->template_name;
        $data['footer_id']=$this->get_active_template("footers");
        $data['social_id']=$this->get_active_template("social_follow");



        return view($this->theme,$data);
    }

    public function beck(){
        $date="2018-10-07T12:02:41Z";
        $headline="Taytus is AMAZING!!";
        $path="http://this.is.an.url/";
        $this->theme="beckandgalo";

        $data['date']=$date;
        $data['headline']=$headline;
        $data['path']=$path;
        $data['theme']=$this->theme;



        return view($this->theme,$data);
    }
    public function article(){
        $date="2018-10-07T12:02:41Z";
        $headline="Taytus is AMAZING!!";
        $path="http://this.is.an.url/";
        $this->theme="article";

        $data['date']=$date;
        $data['headline']=$headline;
        $data['path']=$path;
        $data['theme']=$this->theme;



        return view($this->theme,$data);
    }
    /*
     * for testing purposes returns a valid id for each section for X template
     */
    private function get_active_template($section){

        //return  call_user_func(array($this,$section),$template_name);
        $table='partials_'.$section;

        $res=DB::table($table)
            ->select($table.'.id as item_id')
            ->join('templates','templates.id','=',$table.'.template_id')
            ->where('templates.name',strtolower($this->template_name))->first();

        return $res->item_id;
    }

    private static function footer($template_name){

    }
}
