<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\NewFAQ;

use App\SearchLog;

use App\FAQ;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class FAQController extends Controller
{
    public function index()
    {
        $data['user'] = Auth::User();
        $data['questions'] = FAQ::list_questions('public');
        $data['categories'] = Category::all();

        return view('admin.faq.list', $data);
    }

    public function store()
    {
        $request = FAQ::store_validation();
        $request['user_id'] = Auth::id();

        $newFAQ = FAQ::create($request);

        $question = request('question');
        $url = url('/admin/question/' . $newFAQ->id);
        $email = 'admin@example.com';

        Mail::to($email)
            ->send(new NewFAQ($question, $userEmail['email'] = Auth::user()->email, $url));

        if(Auth::user()->is_admin()) {
            return redirect()->back()
                ->with('message', 'Question Posted');
        } else {
            return redirect(route('admin.faq.index'))
                ->with('message', 'Question Sent.');
        }

    }

    public function search()
    {
        $data['user'] = Auth::User();
        $request['term'] = request('search');
        $term = $request['term'];

        SearchLog::create($request);

        $data['questions'] = FAQ::search_by_title($term);

        return view('admin.faq.search', $data);
    }

    public function public_list() {
        $data['user'] = Auth::User();
        $data['questions'] = FAQ::list_questions('public');
        $data['categories'] = Category::all();

        return view('admin.faq.public', $data);
    }

    public function public_search() {
        $data['user'] = Auth::guest();
        $request['term'] = request('search');
        $term = $request['term'];

        SearchLog::create($request);

        $data['questions'] = FAQ::search_by_title($term);

        return view('admin.faq.public_search', $data);
    }

}
