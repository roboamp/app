<?php

namespace App\Http\Controllers;

use App\TmpProperty;
use Auth;
use Illuminate\Http\Request;

class TmpPropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TmpProperty  $tmpProperty
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = TmpProperty::find($id);
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['property']= $property;
        $data['form_title']='Property';

        return view('admin.tmpProperties.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TmpProperty  $tmpProperty
     * @return \Illuminate\Http\Response
     */
    public function edit(TmpProperty $tmpProperty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TmpProperty  $tmpProperty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $property = TmpProperty::find($request->property);

        if($property){
            $property->price_bucket_id = $request->bucket;
            $property->save();
            $data["success"] = true;
            $data["message"] = 'Price Bucket Succesfully Updated for the Property '.$property->name;
        }else{
            $data["success"] = false;
            $data["message"] = 'Price Bucket not Updated. Contact Support.';
        }               
        $data["max"] = $property->bucket->max;
        $data["property"] = $property->id;
        $data["name"] = $property->name;
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TmpProperty  $tmpProperty
     * @return \Illuminate\Http\Response
     */
    public function destroy(TmpProperty $tmpProperty)
    {
        //
    }
}
