<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpClient\HttpClient;
use Illuminate\Support\Arr;
use Goutte\Client;
use App\MyClasses\Strings;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\PotentialCustomer;
use App\PotentialCustomerEmail;
use App\PotentialCustomerPhone;
use App\MyClasses\URL;
use App\Jobs\GettingClientContactInfo;

class WebScrapperController extends Controller
{

    public function get_contact_info(){

        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;

        $data['form_centered']=true;
        $data['form_title']='Get Contact Info from URLs';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.web_scrapper.scrapping_contact_info');

        return view('admin.webScrapper.getContactInfo.index', $data);

    }

    public function scrapping_contact_info(Request $request){
        request()->validate([
            'url' => 'required'
        ]);

        $pattern = "/[\s,]+/";

        //separate the strings and create an array with the info
        $urls = preg_split($pattern, $request->url);
        //validate the URLs
        $validated_urls = $this->validate_urls_list($urls);
        $valid_urls = $validated_urls['valids'];
        $invalid_urls = $validated_urls['invalids'];

        foreach($valid_urls as $v_url){
            $customer = PotentialCustomer::updateOrCreate(
                ['url' => $v_url]
            );
            
            GettingClientContactInfo::dispatch($customer->url, $customer->id);
        }

        return redirect()->route('admin.web_scrapper.potential_client_list');
    }

    public function validate_urls_list($urls){
        $valid_urls = array();
        $invalid_urls = array();
        $validated_urls = array();
        foreach ($urls as $url) {
            if(URL::is_valid_url($url)){
                $valid_urls[] = $url;
            }else{
                $invalid_urls[] = $url;
            }
        }

        $validated_urls['valids'] = $valid_urls;
        $validated_urls['invalids'] = $invalid_urls;

        return $validated_urls;
    }

    public function potential_client_list(){
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['customers']=PotentialCustomer::all();
        $data['form_title']='Potential Client List';

        return view('admin.webScrapper.getContactInfo.list', $data);
    }

    public function getting_emails($html){
        $email_regexp = "/(?:[a-z!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
        preg_match_all($email_regexp, $html,  $emails);
        $emails_flatten = Arr::flatten($emails);

        return $emails;
    }

    public function getting_phones($html){
        $partial_phones = stristr($html, '"tel:');
        $phones = str_replace('">', '', substr($partial_phones,5,14));

        return $phones;
    }


}
