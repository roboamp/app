<?php

namespace App\Http\Controllers;

use App\PageSection;
use Auth;
use App\TmpClient;
use App\TmpProperty;
use Illuminate\Http\Request;

class UsageReportController extends Controller
{
    public function index(){
        $data['user']=Auth::User();
        $data['clients']=TmpClient::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Usage Report';

        return view('admin.usageReports.index', $data);
    }

    public function show(Request $request){
        $client = TmpClient::find($request->client);

        //dd();
        $data['success'] = true;
        $data['properties_count'] = $client->properties->count();
        $data['properties'] = $client->properties;
        $data['sections_count'] = $client->properties->map->sections->flatten()->count();
        $data['pages_count'] = $client->properties->map->total_pages()->sum();

        return response()->json($data);
    }

    public function getSections(Request $request){
        $property = TmpProperty::find($request->property);
        $sections = $property->sections;

        $data['property'] = $request->property;
        $data["sections"] = array();
        foreach($sections as $section){
            $data["sections"]["names"][] = $section->name;
            $data["sections"]["pages"][] = $section->pages->count();
        }
        return response()->json($data);
    }

    public function getPages(Request $request){
        $section = PageSection::find($request->section);
        $pages = $section->pages;

        $data["pages"] = $pages;

        return response()->json($data);
    }
}
