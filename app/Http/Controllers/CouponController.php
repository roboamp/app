<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\MyClasses\Stripe\RoboStripe;
use Illuminate\Support\Facades\Notification;
use App\MyClasses\Stripe\RoboStripeCouponsCli;

class CouponController extends Controller
{
    private $err_invalid_code='Invalid Coupon Code';
    private $RoboStripe;

    public function __construct(){
        $this->RoboStripe=new RoboStripe();
    }


    public function show_create_form(){
        return view('admin.users.coupons.create_coupon_form');
    }
    public function post_create_form(Request $request){


        $now=Carbon::now('US/Central');
        $redeem_by=$now->addDays(30);
        $coupon=new Coupon();

        $RoboStripeCouponsCli=new RoboStripeCouponsCli();
        $coupon->id= $RoboStripeCouponsCli->clean_coupon_id($request->get('txt_coupon_id'));

        //checks if coupon's ID is already in use

        $res=$coupon->find($coupon->id);
        if(!is_null($res)){
            return redirect('home')->with('error', 'This Coupon ID is already in use');
        }


        $coupon->redeem_by=$redeem_by;
        $coupon->percent_off=$request->get('txt_percent_off');
        $coupon->max_redemptions=$request->get('txt_max_redemptions');
        $coupon->duration=$request->get('cmb_duration');
        $coupon->save();

        //save coupon on stripe
        $coupon->redeem_by=$redeem_by->timestamp;
        $res=$this->RoboStripe->create('coupon',$coupon);

        $coupon->send_notification("New Coupon has been created");


        return redirect('home')->with('success',"New Coupon has been created");


        //trigger notification everything went well

    }
    public function show_edit_coupon_form($id=null){
        if(!is_null($id)) {
            $coupon = Coupon::find($id);
            if (is_null($coupon)) abort(404, $this->err_invalid_code);
            $data['edit'] = true;
            $data['coupon']['id'] = $coupon->id;
            $data['coupon']['percent_off'] = $coupon->percent_off;
            $data['coupon']['max_redemptions'] = $coupon->max_redemptions;
            $data['coupon']['redeem_by'] = $coupon->redeem_by;

            return view('admin.users.coupons.create_coupon_form', $data);
        }else{
            abort(404,$this->err_invalid_code);
        }
    }


    public function post_edit_coupon_form(Request $request){

        $coupon=Coupon::find($request->get('txt_coupon_old_id'));
        $coupon->id=$request->get('txt_coupon_id');
        $coupon->percent_off=$request->get('txt_percent_off');
        $coupon->max_redemptions=$request->get('txt_max_redemptions');
        $coupon->save();

        return view('home');

    }
    public function delete_coupon($id=null){

        if(is_null($id))abort(404,$this->err_invalid_code);

        $coupon=Coupon::find($id);

        if(is_null($coupon)) abort(404,$this->err_invalid_code);

        $coupon->delete();

        //find and delete the coupon on stripe
        $res=$this->RoboStripe->delete_by_id('coupon',$id);

        if($res)$data['error']=0;

        return json_encode($data);


    }
}
