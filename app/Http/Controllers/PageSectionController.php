<?php

namespace App\Http\Controllers;

use App\PageSection;
use Auth;
use Illuminate\Http\Request;

class PageSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['user']=Auth::User();
        $data['sections']=PageSection::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Pages Sections';

        return view('admin.pagesSections.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageSection  $pageSection
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $section = PageSection::find($id);
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Pages Sections';

        return view('admin.pagesSections.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageSection  $pageSection
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageSection  $pageSection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $section = PageSection::find($request->section);

        if($section){
            $section->name = $request->name;
            $section->description = $request->description;
            $section->url = $request->url;
            $section->limit = $request->limit;
            $section->save();
        }else{
            $section = PageSection::create([
                'name' => $request->name,
                'description' => $request->description,
                'property_id' => $request->property,
                'url' => $request->url,
                'limit' => $request->limit
            ]);
        }

        $data["success"] = true;
        $data["section"] = $request->section;
        $data["name"] = $section->name;
        $data["description"] = $section->description;
        $data["url"] = $section->url;
        $data["limit"] = $section->limit;

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageSection  $pageSection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $section = PageSection::find($request->item);
        $section->delete();

        $data["success"] = true;
        $data["target"] = $request->item;
        $data["action"] = "section";

        return response()->json($data);
    }
}
