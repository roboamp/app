<?php

/*
 * This controller is the one called from ***EVERY*** client
 *
 */
namespace App\Http\Controllers\Reports;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MyClasses\CliFormat;

use App\Http\Controllers\Controller;


class UsersReportsController extends Controller
{

   /*
    * Get a general overview of all the properties per user,
    *  and their statuses and their pages
    */
    public function Properties(){
        $users=User::all();

        foreach ($users as $user){
            echo $user->id." | ". $user->name."<br><br>";// $user->property."<br>";
            foreach ($user->properties as $property){

                echo "<div style='".CliFormat::style($property->status->status)."'>";
                echo CliFormat::tabs(3). $property->id."  |  ".$property->status->status."<br>";

                foreach ($property->page as $page){
                    echo CliFormat::tabs(6).$page->name."<br>";
                }
                echo "</div>";

            }

            echo "<br><br>";
        }

   }






}
