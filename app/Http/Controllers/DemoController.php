<?php

namespace App\Http\Controllers;

use App\DemoPage;
use App\Http\Controllers\Admin\AdminDemoController;
use Illuminate\Http\Request;
use View;
use Auth;
use App\Template;
use Illuminate\Http\Client\Response;
use App\MyClasses\URL;
use App\Property;

class DemoController extends Controller{

    public function demo(){

        return $this->starter_page('partners.demo.index');
        //shows a view to create a new resource
        
        $data['user']=Auth::User();

        $data['messages']=0;
        $data['comments']=0;

        $data['form_centered']=false;
        $data['form_title']='Test Template';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.templates.demo.post');

        $view= view('admin.templates.demo',$data);
        $response = new Response($view);
        return $response->withCookie('something','value',60);
    }

    public function demo_post(Request $request){
        //
        return $this->finger_print($request->get('txt_url'));


        dd("this has been received");
    }


    public function index($property,$page){
        //check if property exist in the demo Table
      /*  dd('check if property exist in demo table');

        dd('check if demo/property folder is available');

        dd('check if view is available');*/

        $property_name="radisson";


        View::addNamespace($property_name, base_path('parser/demo/'.$property_name));

        if(view()->exists("{$property_name}::{$page}") )return view("{$property_name}::{$page}");

        return abort(404,"View doesn't exist");


    }

    //get the dynamic info fromt the host
    public function radisson_info($page){
        $property_name="radisson";

        View::addNamespace($property_name, base_path('parser/demo/'.$property_name));

        dd($page);
        //if(view()->exists("{$property_name}::index") )return view("{$property_name}::{$page}");

//        return abort(404,"View doesn't exist");
    }

    //only displays the HTML files
    public function radisson($page){
        $property_name="radisson";


        View::addNamespace($property_name, base_path('parser/demo/'.$property_name));

        if(view()->exists("{$property_name}::{$page}") )return view("{$property_name}::{$page}");

        return abort(404,"View doesn't exist");
    }


    public function show($property_url){

        View::addNamespace("demo", base_path('parser/properties/'.$property_url));

        if(view()->exists("demo::index") )return view("demo::index");

        return abort(404,"View doesn't exist");

    }
    public function info(DemoPage $id){
        dd($id);
    }

    public function finger_print($url){
        $error_code="404";
        $property_class=new Property();

        $property=$property_class->get_property_from_url($url);
        dd($property);

        $customer_page=URL::decode_current_url($property->id);

        dd($customer_page);

        if(!URL::is_valid_url($customer_page)) return false;


        $customer_page_domain=URL::get_domain($customer_page);


        $property_url=URL::get_domain($property->url);
        $property_subdomain=URL::get_domain($property->subdomain);

        //before doing anything, I check if the page has been cached before
        //if so, serve the cached version

        $data['property_url']=$customer_page_domain;
        $folder_to_property=base_path('parser/properties/'.$customer_page_domain);

        $cached=$this->check_for_cached_page($customer_page,$folder_to_property,$property);

        if($cached!=null) return $cached;

        $res=new Curl($customer_page);
        $html=$res->get_content();
        $dom=HtmlDomParser::str_get_html($html);

        $dom=$this->update_links($dom,$customer_page_domain);

        //$dom=$this->remove_amp_errors($dom);

        $vars_array=[];
        //I have a response from the server, //get all the possible templates
        //each template has a signature that we need to verify,
        //it could be NULL for index pages
        $templates=Template::where('property_id',$property->id)->with('ampscripts')->get();

        if(count($templates)==0){
            if($this->testing_server){
                dd('There is no template for this property',
                    "Property ID = ".$property->id,
                    "Template URL = ".urldecode("https%3A%2F%2Fblog.ioogo.com"),
                    $html);
            }else{
                $this->exit_code('703', 'No template available for this page');
            }
        }


        //parse the template


        foreach ($templates as $item){
            $vars_array['ampscripts']=$item->ampscripts->toArray();

            if($item->signature!="") {
                if (Strings::find_string_in_string($html, $item->signature)) {
                    //we know the template, now I need to extract the variables
                    //each template will need different variables,
                    $includes=Includes::where('template_id','=',$item->id)->get();
                    foreach ($includes as $obj){
                        $code=json_decode($obj->node);
                        $code->obj=(isset($code->obj)?$code->obj:false);
                        $code->process=(isset($code->process)?$code->process:false);
                        switch ($code->type){
                            case 'tag':
                                $res=$dom->find($code->tag_name,0)->text();
                                break;
                            case 'header_link':
                                $res=$dom->find('head link[rel='.$code->header_link.']', 0)->href;
                                break;
                            case 'class':


                                if($code->obj==true){
                                    $res=$dom->find("[class={$code->class_name}]",0)->innertext();
                                }else{
                                    $res=$dom->find("[class={$code->class_name}]",0)->{$code->data};
                                }

                                if($code->process==true){
                                    $class=$code->callback->class;
                                    $method=$code->callback->method;
                                    $res=MyClass::call_method($class,$method,$res);
                                }


                                break;
                            case 'div':
                                //this means to grab all the childs
                                if($code->obj==true){

                                    $target=$dom->find('div[id='.$code->div_id.']',0);

                                    if(!is_null($target)) {
                                        $res =$target->innertext();
                                        if($code->div_id=='searchspring-options'){
                                            //dd($code->div_id,'batman');

                                        }
                                    }else{
                                        e::dd('target is null for div '. $code->div_id,$res);
                                    }

                                }
                                if($code->process==true){
                                    $class=$code->callback->class;
                                    $method=$code->callback->method;
                                    $res=MyClass::call_method($class,$method,$res);
                                }
                                break;
                        }
                        $vars_array[$obj->name]=Strings::delete_tabs($res);
                    }
                    // e::dd($vars_array);
                    //call the view with all the variables
                    //add the namespace
                    $data['property_url']='redraideroutfitter.com';

                    $folder_to_property=base_path('parser/properties/'.$data['property_url']);
                    $my_view=View::addNamespace($data['property_url'],$folder_to_property);
                    View::addNamespace('includes',$folder_to_property."/includes/");
                    $hints=$my_view->getFinder()->getHints();
                    return view("{$data['property_url']}::{$item->name}",$vars_array);

                }else{
                    return abort(404,'Template exist but Signature has not been found for this URL');

                }

                // echo $item->name."     ".$item->code."<br>";

                //$res=$dom->find('title');
                //   $res=$dom->find($item->node);

                // e::dd($res[0]->text());


                /*   if (Strings::find_string_in_string($html, $item->signature)) {

                       foreach ($includes as $obj){
                           $code=json_decode($obj->code);
                          // if($obj==$includes[5])e::dd($code,$includes,$code->start,$html);
                           $var=Strings::get_string_between($html,$code->start,$code->end);
                           $vars_array[$obj->name]=htmlspecialchars_decode($var);
                       }

                       e::dd($vars_array);

                       //call the view with all the variables
                       //add the namespace
                       $data['property_url']='redraideroutfitter.com';

                       $folder_to_property=base_path('parser/properties/'.$data['property_url']);
                       $my_view=View::addNamespace($data['property_url'],$folder_to_property);
                       View::addNamespace('includes',$folder_to_property."/includes/");
                       $hints=$my_view->getFinder()->getHints();
                       return view("{$data['property_url']}::{$item->name}",$vars_array);


                       e::dd('done');

                       $data['page_name']='product';
                       $view="{$data['property_url']}::{$data['page_name']}";

                       e::dd($view,$hints);

                       return $view($view,$vars_array);

                       e::dd($vars_array);
                       e::dd('found it!', $item->id, $item->signature);
                   };
              */
            }
            //echo $item->signature."<br>";
        }



        //this goes in a live server
        $res=URL::curl($customer_page);

        return $res;


        $current_url=URL::current();
        $res=$url->check_for_roboamp_code($current_url);
        dd($res);
        return $res;

    }

    private function starter_page($view){
        switch ($view){
            case 'partners.demo.index':
                $admin_demo_controller=new AdminDemoController();
                return $admin_demo_controller->index();
            break;
        }

    }
}
