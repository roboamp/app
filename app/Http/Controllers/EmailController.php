<?php

namespace App\Http\Controllers;

use App\Mail\TestEmail;
use App\Mail\AMP;
use App\Notify;
use Carbon\Carbon;
use Mail;

use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Page;
use ROBOAMP\Strings;
use Mailgun\Mailgun;


class EmailController extends Controller{

    private $sender_email;
    public function sender(){


        $data = ['message' => 'This is a very cool test!'];

        Mail::to('hello@parse.mortalwar.com')->send(new TestEmail($data));

        dd('email sent',new Carbon());

        /*$mg = Mailgun::create('ca835dccae7a0470be2c8e7600927fd5-87cdd773-2d606455');

        $mg->messages()->send('amp.roboamp.com', [
            'from'    => 'postmaster@amp.roboamp.com',
            'to'      => 'copertus@gmail.com',
            'cc'=>'mkane@mailgun.com',
            'subject' => 'ROBOAMP IS AWESOME!',
            'text'=>'hello',
            'html'=>'<h1>THIS IS AN AMAZING DEMO</h1>',
            'amp-html'    => view('emails.mailing_list')->render(),
        ]);
        dd('email new email',new Carbon());
*/


    }
    public function parser(Request $request){


        /*$now= new Carbon();
        $this->parse_email($request->input('from'));
        $page_id= Uuid::generate(4);
        $page_class=new Page();
        $page_class->id=$page_id;
        $page_class->property_id="1262c4a8-0441-462b-a036-fe596051898b";
        $page_class->name="EMAIL TEST_".$now;
        $page_class->created_at=$now;
        $page_class->updated_at=$now;
        $page_class->email=$this->sender_email;
        $page_class->save();*/

        //Mail::to($this->sender_email)->send(new AMP($this->sender_email));

        $mg = Mailgun::create('ca835dccae7a0470be2c8e7600927fd5-87cdd773-2d606455');

        $mg->messages()->send('amp.roboamp.com', [
            'from'    => 'Roberto Inetti <postmaster@amp.roboamp.com>',
            'to'      => 'copertus@gmail.com',
            'subject' => 'ROBOAMP IS SUPER AWESOME!',
            'text'=>'hello',
            'html'=>'<h1>THIS IS AN AMAZING DEMO</h1>',
            'amp-html'    => view('emails.mailing_list')->render(),
        ]);

        $this->save_notify();

        dd('email sent');


    }

    //triggered when the form in the mailing list example is sent
    public function post_form(Request $request){

        header("AMP-Access-Control-Allow-Source-Origin: https://".$_SERVER['HTTP_HOST']);

        if(!isset($_SERVER["HTTP_ORIGIN"])){
            header("Poronga:  TAYTUS");
            header("Access-Control-Allow-Origin: https://mail.google.com");
            header("AMP-Access-Control-Allow-Source-Origin: postmaster@amp.roboamp.com");
        }else {

            switch ($_SERVER["HTTP_ORIGIN"]) {
                case "https://parser.roboamp.com":
                    header("Poronga:  TAYTUS--|" . $_SERVER["HTTP_ORIGIN"]);

                    header("Access-Control-Allow-Origin: https://" . $_SERVER['SERVER_NAME']);
                    break;
                case "https://parser-roboamp-com.cdn.ampproject.org":
                    //https://parser-roboamp-com.cdn.ampproject.org
                    header("Poronga: " . $_SERVER["HTTP_ORIGIN"] . "| TAYTUS");

                    header("Access-Control-Allow-Origin: https://parser-roboamp-com.cdn.ampproject.org");
                    break;
                default:
                    header("Poronga:  TAYTUS");

                    header("Access-Control-Allow-Origin: https://mail.google.com");
                    header("AMP-Access-Control-Allow-Source-Origin: postmaster@amp.roboamp.com");

            };
        }


       //for email: Access-Control-Allow-Origin

        //"https://".$_SERVER['HTTP_HOST']
        $name = isset($_POST['name']) ? $_POST['name'] : 'Error';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        //return $_SERVER;

        if($name==='error'){
            return response()->json([
                'name'=>$name,

            ],500);
        }

        $now= new Carbon();
        $page_id= Uuid::generate(4);
        $page_class=new Page();
        $page_class->id=$page_id;
        $page_class->property_id="1262c4a8-0441-462b-a036-fe596051898b";
        $page_class->name="THIS IS COMING FROM GMAIL's FORM_".$now;
        $page_class->created_at=$now;
        $page_class->updated_at=$now;
        $page_class->url=$email;
        $page_class->save();


        return response()->json([
            'name' => $name
        ],200);

    }
    private function parse_email($email=null){
        $myString=new Strings();
        $email=$myString::get_string_between($email,"<",">");
        $this->sender_email= 'copertus@gmail.com';

    }
    private function save_notify(){
        $notify=new Notify();
        $max_id=$notify->max('id')+1;
        $notify->id=$max_id;
        $notify->name="AMP email demo";
        $notify->copy="";
        $notify->interval="1 day";
        $notify->notifications_type_id=1;
        $notify->created_at=now();
        $notify->updated_at=now();
        $notify->save();
    }


}
