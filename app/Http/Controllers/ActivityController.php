<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ROBOAMP\Strings;
use Auth;
Use App\Activity;
use App\Task;
Use App\Timeframe;

class ActivityController extends Controller
{
    public function index()
    {
        $data['user']=Auth::User();
        $data['activities']=Activity::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Tasks List';

        return view('admin.activities.index', $data);
    }


    public function create()
    {
        //render a list of a resource
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Create Task';
        $data['form_centered']=true;
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.activities.store');

        return view('admin.activities.create', $data);
    }

    public function store(Request $request)
    {

        $activity = Activity::create([
            'name' => $request->name,
            'label' => $request->label,
            'command' => $request->command,
            'description' => $request->description
        ]);

        return redirect()->route('admin.activities.index');
    }

    public function edit(Request $request)
    {
        $data['activity'] = Activity::firstWhere('id', $request->id);
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Edit Task';
        $data['form_centered']=true;
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.activities.update');

        return view('admin.activities.edit', $data);
    }

    public function update(Request $request)
    {
        $activity = Activity::find($request->id);
        $activity->name = $request->name;
        $activity->label = $request->label;
        $activity->description = $request->description;
        $activity->command = $request->command;
        $activity->save();

        $data['user']=Auth::User();
        $data['activities']=Activity::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Tasks List';

        return redirect()->route('admin.activities.index');
    }

    public function destroy(Request $request)
    {
        $activity = Activity::find($request->activity);
        $activity->delete();

        $activity2 = Activity::find($activity->id);

        if(!$activity2){
            $tasks = Task::select('id')->where('activity_id', $request->activity)->get();
            foreach($tasks as $task){
                $task->delete();
            }
            $data["success"] = true;
            $data["message"] = "Task Succesfully Deleted.";
        }else{
            $data["success"] = false;
            $data["message"] = "Task Not Deleted.";
        }

        
        $data["target"] = $activity->id;

        return response()->json($data);
    }
}
