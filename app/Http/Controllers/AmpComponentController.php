<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ROBOAMP\Strings;
use Auth;
use App\Ampcomponent;
use App\Amptemplate;
use App\AmptemplateAttribute;
use App\AttributeType;
use App\Attribute;
use App\Template;

class AmpComponentController extends Controller
{
    public function index()
    {
        $data['user']=Auth::User();
        $data['components']=Ampcomponent::all();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_title']='Amp Components List';

        return view('admin.ampComponents.index', $data);
    }


    public function create(Request $request)
    {
        $data['attr_types'] = AttributeType::all();
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_centered']=true;
        $data['form_title']='Create Component';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.ampcomponents.store');
        return view('admin.ampComponents.create', $data);
    }


    public function store(Request $request)
    {
        $component = Ampcomponent::create([
            'name' => $request->name,
            'description' => $request->description,
            'codebase' => $request->codebase
        ]);
        
        $attr_numbers = count($request->attr_name);
        for($i=0; $i<$attr_numbers; $i++){
            $attribute = Attribute::create([
            'component_id' => $component->id,
            'name' => $request->attr_name[$i],
            'description' => $request->attr_description[$i],
            'type_id' => $request->attr_type[$i],
            'value' => $request->value[$i]
            ]);
        }

        return redirect()->route('admin.ampcomponents.index');
    }

    public function show($id)
    {
        $component = Ampcomponent::findOrFail($id);
        $warnings = $this->validate_attributes_in_codebase($component);

        $data['component'] = $component;
        $data['warnings'] = $warnings;
        
        return view('components.show', $data);
    }

    public function edit(Request $request)
    {
        $data['component'] = Ampcomponent::findOrFail($request->id);
        $data['attr_types'] = AttributeType::all();
        $data['user']=Auth::User();
        $data['messages']=0;
        $data['comments']=0;
        $data['form_centered']=true;
        $data['form_title']='Edit Component';
        $data['form_submit_button_title']=$data['form_title'];
        $data['form_action']=route('admin.ampcomponents.update');

        return view('admin.ampComponents.edit', $data);
    }


    public function update(Request $request)
    {
        $component = Ampcomponent::findOrFail($request->id);
        $component->name = $request->name;
        $component->description = $request->description;
        $component->codebase = $request->codebase;
        $component->save();

        $this->add_Attributes($request);

        return redirect()->route('admin.ampcomponents.index');
    }


    public function destroy(Request $request)
    {   
        $component = Ampcomponent::find($request->component);
        $component->delete();

        $component2 = Ampcomponent::find($component->id);

        if(!$component2){
            $attributes = Attribute::select('id')->where('component_id', $request->component)->get();
            foreach($attributes as $attribute){
                $attribute->delete();
            }

            $templates = Amptemplate::select('id')->where('component_id', $request->component)->get();
            foreach($templates as $template){
                $attributes = AmptemplateAttribute::select('id')->where('template_id', $template->id)->get();
                foreach($attributes as $attribute){
                    $attribute->delete();
                }
                $template->delete();
            }

            $data["success"] = true;
            $data['message'] = 'Component Deleted Succesfully.';
        }else{
            $data["success"] = false;
            $data['message'] = 'Component Not Deleted.';
        }
        
        $data["target"] = $request->component;

        return response()->json($data);
    }


    Public function add_Attributes($request){
        
        $attr_numbers = count($request->attr_name);

        for($i=0; $i<$attr_numbers; $i++){
            $attribute = Attribute::find($request->attr_id[$i]);
            
            if($attribute === null){
                $attribute = Attribute::Create([
                'component_id' => $request->id,
                'name' => $request->attr_name[$i],
                'description' => $request->attr_description[$i],
                'type_id' => $request->attr_type[$i],
                'value' => $request->value[$i]
                ]);
            }else{
                $attribute->component_id = $request->id;
                $attribute->name = $request->attr_name[$i];
                $attribute->description = $request->attr_description[$i];
                $attribute->type_id = $request->attr_type[$i];
                $attribute->value = $request->value[$i];
                $attribute->save();
            }
        }

    }


    public function validate_attributes_in_codebase($component){
        $codebase = $component->codebase;
        $warnings = array();
        foreach($component->attributes as $attribute){
            $nail = '||'.$attribute->name;
            $haystack = Strings::find_string_in_string($codebase, $nail);
            if(!$haystack){
                $warnings[] = $attribute->name;
            }
        }

        return $warnings;
    }
}
