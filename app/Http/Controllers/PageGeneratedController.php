<?php

namespace App\Http\Controllers;

use App\PageGenerated;
use Illuminate\Http\Request;

class PageGeneratedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageGenerated  $pageGenerated
     * @return \Illuminate\Http\Response
     */
    public function show(PageGenerated $pageGenerated)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageGenerated  $pageGenerated
     * @return \Illuminate\Http\Response
     */
    public function edit(PageGenerated $pageGenerated)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageGenerated  $pageGenerated
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageGenerated $pageGenerated)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageGenerated  $pageGenerated
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageGenerated $pageGenerated)
    {
        //
    }
}
