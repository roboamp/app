<?php

namespace App\Http\Controllers\ignore;

use Illuminate\Http\Request;

class DemoController extends Controller
{
    public function create()
    {
        return view('demo_old.create');
    }

    public function store()
    {
        flash('Your message has been sent!')->success();

        return redirect()->route('contact.create');

    }
}
