<?php

/*
 * This controller is used by Partners to Create Demos
 *
 */
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\DemoPage;
use Auth;
use App\MyClasses\MyArray;





class AdminDemoController extends Controller{
    public function __construct(){}

    public function upload(){
        return view('admin.qa.upload');
    }

    public function show(){
        return "this is the control panel";
        return view('qa.upload');

    }





    public function index(){
        $data=$this->setup_basic_data_info();

        $data['user']=Auth::User();

        $data['demos']=$data['user']->demos()->get();

        //dd($data['demos']);
        $data['card_title']="List of active Demos";
        $data['card_subtitle']="This is only a demo";

        if(count($data['demos'])==0) return redirect()->route('admin.properties.templates.create');

        $data['table_headers']=['Name','URL','Status','ETA'];

        return view('admin.partners.demos.index',$data);

    }

    public function create(){
        $data=$this->setup_basic_data_info();

        $data['user']=Auth::User();
        $data['card_title']="Create New Demo";
        $data['form_action']=route('admin.demo.create.post');
        $data['form_submit_button_title']="Create Demo";


        return view('admin.partners.demos.create',$data);

    }
    private function setup_basic_data_info(){
        $data['user']=Auth::User();
        $data['messages']=null;
        $data['comments']=null;
        // $data['section_name']=ucfirst($request->route()->getName());
        $data['card_name']="Radisson Hotels - AMP Pages";

        //for tables

        $data['hide_table_label']=true;

        return $data;
    }

    public function store(){
        //persist the new resource
       /* $request=request()->validate([
            'txt_name'=>'required',
            'property_id'=>'required',
            'txt_url'=>[
                'required',
                function ($attribute, $value, $fail) {
                    $input_domain=URL::get_domain(request('txt_url'));
                    $property=Property::where('id',request('property_id'))->first();
                    $match=$property->domain_match($input_domain);

                    if(!$match) $fail("This URL doesn't match the domain or subdomain for this property");

                }],
            'selector_id'=>'required',
            'txt_signature'=>'required'
        ]);*/
        $request=request()->validate(
            ['name'=>'required','txt_url_1'=>'required','txt_url_2'=>'string','txt_url_3'=>'string','txt_url_4'=>'string'],
            ['name.required'   =>" "]
            );


        $request=MyArray::remove_prefix($request,"txt_");

        $urls=[];
        if($request['url_1']!="")$urls[]=$request['url_1'];
        if($request['url_2']!="")$urls[]=$request['url_2'];
        if($request['url_3']!="")$urls[]=$request['url_3'];
        if($request['url_4']!="")$urls[]=$request['url_4'];
        $id=rand (1,1000000);


        foreach ($urls as $item){
            $demoPage=new DemoPage();
            $demoPage->id=$id;
            $demoPage->name="demo";
            $demoPage->local_url="demo_local_url";
            $demoPage->testing_url="demo_testing_url";
            $demoPage->demo_id=2;
            $demoPage->status_id=1;
            $demoPage->eta="2020-07-15 01:57:54";
            $demoPage->created_at=now();
            $demoPage->updated_at=now();
            $demoPage->save();
            dd("done");
        }

        DemoPage::create($request);

        return redirect(route('admin.demo.index'));
    }



}
