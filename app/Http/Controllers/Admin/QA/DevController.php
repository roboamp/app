<?php

/*
 *
 */
namespace App\Http\Controllers\Admin\QA;
use App\Http\Controllers\Controller;
use App\MyClasses\Files;
use Illuminate\Http\Response;

use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MyClasses\Faker_js;
use App\MyClasses\Counters;
use App\MyClasses\Widgets;
use App\Counter;
use App\Widget;
use Carbon\Carbon;
use Auth;
use Webpatser\Uuid\Uuid;
//use Madnest\Madzipper\Madzipper;






class DevController extends Controller
{
    public function __construct(){
    }



    public function show(){
        $data['user']=Auth::user();
        return view('admin.qa.form-upload',$data);
        //return view('admin.qa.upload',$data);

    }
    public function store(Request $request){

        if($request->hasFile('dev')){

            $project=$request->input('project');
            //get latest version and setup the new version
            $id= Uuid::generate(4);

            //ID is used for the route path;

            $fileNameToStore=$this->get_file_name($request,$id);

            // upload
            $project="taytus";
            $save_file_path=base_path('dev/'.$project."/zip/");
            $request->file('dev')->move($save_file_path, $fileNameToStore);
            $zip_file_path=$save_file_path. $fileNameToStore;

            $this->unzip_files($zip_file_path,$id);





        }
        else{
            dd("ERROR 356");
        }

    }

    private function unzip_files($zip_file_path,$id){
        $mad= new Madzipper();
        $destination=dirname($zip_file_path,2)."/".$id;
        $mad->make($zip_file_path)->extractTo($destination);
        $mad->close();
    }

    private function get_file_name($request,$id){
        $fileNameWithExt = $request->file('dev')->getClientOriginalName();
        // get file name
        $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        // get extension
        $extension = $request->file('dev')->getClientOriginalExtension();
        //add the time to the name
        $fileNameToStore = $id.'.'.$extension;

        return $fileNameToStore;
    }

    public function generate_data($model=null){
        $faker_js = new Faker_js();

        if($faker_js->get_testing() == true){
            $data['faker_js']= $faker_js->create($model, 1);
        // $data['faker_js']= $faker_js->create_x_random($model, 1);
            return;
        }

    }

    public function get_records_created_during_x_months($table=null,$months=1){

        $now=Carbon::now();
        $past=Carbon::now()->subMonth($months);

        $obj=$model::whereBetween('created_at', [$past,$now])->get();
            
        return $obj;

    }

    public function get_records_created_during_this_month($table=null){

        $model="App\\".$table;
        $now=Carbon::now();
        $start_of_month=Carbon::now()->startOfMonth();
        $obj=$model::whereBetween('created_at', [$start_of_month,$now])->get();
            
        return $obj;

    }


    public function index(){

        //THIS WILL DELETE RECORDS ON PAGE LOAD FOR TESTING
        $faker_js = new Faker_js();
        if($faker_js->get_testing() == true){
        $data['faker_js']= $faker_js->delete_all_until_x_records('property',5);
        // $data['faker_js']= $faker_js->delete_all_until_x_records('user',5);
        // $data['faker_js']= $faker_js->delete_all_until_x_records('customer',10);
        // $data['faker_js']= $faker_js->delete_x_records_from_table('property', 10);
        //--------------------
    }

        //LOOPS THROUGH ALL COUNTERS COMING BACK FROM WIDGET CLASS AND PLACES THEM
        //INTO AN ARRAY TO BE LOOPED THROUGH IN COUNTER.BLADE
        $counters = Counter::where('user_id', 1)->get();

        $data['counters_array'] = [];
        foreach($counters as $obj){
            $counter = new Counters();

            $counter->set_name($obj['name']);
            $counter->set_table($obj['table']);
            $counter->set_type($obj['type']);
            $counter->set_label($obj['label']);
            $counter->set_color($obj['color']);
            $counter->set_icon($obj['icon']);

            array_push($data['counters_array'], $counter);
            $data['counters'][]= $counter->render();
        }

        //--------------------


        
        //LOOPS THROUGH ALL COUNTERS COMING BACK FROM COUNTER CLASS AND PLACES
        //ATTRIBUTES INTO AN ARRAY SO WE CAN PASS POST DATA BACK TO THIS CONTROLLER
        $data['counters_name'] = [];
        foreach($data['counters_array'] as $obj){
            $array = [];
            array_push($array, $obj->get_name(), $obj->get_type(), $obj->get_table());
            array_push($data['counters_name'], $array);
        }

        $data['counters_name']=json_encode($data['counters_name']);
        //--------------------


        //LOOPS THROUGH ALL WIDGETS COMING BACK FROM WIDGET CLASS AND PLACES THEM
        //INTO AN ARRAY TO BE LOOPED THROUGH IN WIDGET.BLADE
        $widgets = Widget::all();
        $data['widgets_array'] = [];
        foreach($widgets as $obj){
            $widget = new Widgets();

            $widget->set_type($obj['type']);
            $widget->set_goal($obj['goal']);
            $widget->set_label($obj['label']);
            $widget->set_name($obj['name']);
            $widget->set_table($obj['table']);
            $widget->set_color($obj['color']);

            array_push($data['widgets_array'], $widget);
            $data['widgets'][]= $widget->render();

        }
        //--------------------



        //LOOPS THROUGH ALL WIDGETS COMING BACK FROM WIDGET CLASS AND PLACES
        //ATTRIBUTES INTO AN ARRAY SO WE CAN PASS POST DATA BACK TO THIS CONTROLLER
        $data['widgets_name'] = [];
        foreach($data['widgets_array'] as $obj){
            $array = [];
            array_push($array, $obj->get_name(), $obj->get_type(), $obj->get_table(), $obj->get_goal());
            array_push($data['widgets_name'], $array);
        }

        $data['widgets_name']=json_encode($data['widgets_name']);
        //--------------------------------

        return view('testing/dashboard.widgets', $data);
    }

    public function get_data(Request $request){

        foreach ($request->counters as $obj) {
            $arr=explode(',', $obj);
            $data['response_'.$arr[0]]['units'] = $this->get_records_created_during_this_month($arr[2])->count();
        }

        foreach ($request->widgets as $obj) {
            $arr=explode(',', $obj);
            $data['response_'.$arr[0]]['new_properties'] = $this->get_records_created_during_this_month($arr[2])->count();
            $data['response_'.$arr[0]]['number_of_properties'] = floor(($data['response_'.$arr[0]]['new_properties']/ $arr[3]) * 100);
        }

        return json_encode($data);
    }


    public function test(){
        $data = $this->get_records_created_during_x_months('property', 4);
        dd($data);
    }



}
