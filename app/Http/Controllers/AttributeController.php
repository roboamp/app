<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AttributeType;
use App\Attribute;
use App\AmptemplateAttribute;
use App\Ampcomponent;
use Auth;

class AttributeController extends Controller
{
    public function index()
    {
        $attributes = Attribute::all();

        $data["attributes"] = $attributes;

        return view('attributes.index', $data);
    }


    public function create(Request $request)
    {
        $component = Ampcomponent::firstWhere('id', $request->component_id);
        $attr_types = AttributeType::all();

        $data["component"] = $component;
        $data["attributes_types"] = $attr_types;

        return view('attributes.create', $data);
    }


    public function store(Request $request)
    {
        
        $attribute = Attribute::create([
            'component_id' => $request->component_id,
            'name' => $request->name,
            'description' => $request->description,
            'type_id' => $request->type_id,
            'value' => $request->value
        ]);

        return redirect('/components/'.$request->component_id.'/edit');
    }

    public function destroy(Request $request)
    {
        $attribute = Attribute::find($request->attribute);
        $attr_is_active = AmptemplateAttribute::where('attribute_id', $request->attribute)->exists();

        $data['success'] = true;
        $data['target'] = $attribute->id;
        
        if($attr_is_active){
            $data['success'] = false;
            $data['message'] = 'The attribute <strong>'.$attribute->name.'</strong> is active in a template and can not be deleted.';
            return response()->json($data);
        }else{
            
            return response()->json($data);
            $attribute->delete();
        }
        return response()->json($data);
    }

    public function get_Attributes(Request $request){
        $attributes = Attribute::where('component_id', $request->component)->get();
        
        if(count($attributes) > 0){
            $data['status'] = 'success';
            $data['attributes'] = $attributes;
        }else{
            $data['status'] = 'fail';
        }
        return response()->json($data);
    }
}
