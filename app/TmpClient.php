<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpClient extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function properties(){
        return $this->hasMany('App\TmpProperty', 'client_id');
    }
}
