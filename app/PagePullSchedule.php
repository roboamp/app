<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PagePullSchedule extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function page(){
        return $this->belongsTo('App\TmpPage', 'page_id');
    }

    public function activity(){
        return $this->belongsTo('App\Activity');
    }

    public function timeframe(){
        return $this->belongsTo('App\Timeframe');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
