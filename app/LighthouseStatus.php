<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LighthouseStatus extends Model
{
    protected $guarded = [];
    public function report()
    {
        return $this->hasMany('App\LighthouseReport');
    }
}
