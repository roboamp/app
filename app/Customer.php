<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use App\Notifications\CustomerResetPasswordNotification;

use Illuminate\Notifications\Notifiable;


trait Uuids{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();


    }
}



class Customer extends Model{

    use HasFactory;
    use Notifiable;
    use Uuids;
    protected $guard='customers';
    public $incrementing = false;

    public function properties(){
        return $this->hasMany('App\Property');
    }
    public function coupon(){
        return $this->hasOne('App\Coupon');
    }
    public function notification(){
        return $this->hasOne('App\Notify');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function main_website(){
        $site = Property::where('customer_id', '=', $this->id)
            ->where('main_website', '=', 1)->pluck('url');

        return (count($site)>0)?$site[0]:"Not Available";
    }

    public function all_accounts_suspended(){
        $flag = true;
        $properties = $this->properties()->get();

        foreach($properties as $property){
            if ($property->status_id != 3){
                $flag = false;
            }
        }

        return ($flag);
    }
    public function more_than_one_property(){

       return (count($this->properties)>1?true:false);

    }
    public function at_least_one_property(){
        return (count($this->properties)>=1?true:false);
    }
    public function sendPasswordResetNotification($token){
        $this->notify(new CustomerResetPasswordNotification($token));
    }

    public function routeNotificationForSlack($notification){
        return env('SLACK_WEBHOOK');
    }

    public function customers_without_properties($time_interval=null,$notify_id=null){


        $fields=['id','name','email','notify_id','latest_notification_date','created_at'];
        $properties=new Property();
        $customers_id=$properties->pluck('customer_id')->all();

        if(is_null($time_interval))return $this::select($fields)->whereNotin('id',$customers_id)->get()->toArray();

        return $this::select($fields)->whereNotin('id',$customers_id)
            ->where('created_at','>',$time_interval)
            ->where('notify_id',$notify_id)
            ->get()->toArray();

    }
    public function with_properties($testing=0){
        $properties=new Property();
        $customers_id=$properties->pluck('customer_id')->all();
        return $this::select('id')->whereIn('id',$customers_id)
            ->where('testing',$testing)->get()->toArray();
    }
    public function demo_customer($id,$coupon="beta"){

        $me=$this->where('id',$id)->first();

        if(!$me->more_than_one_property()){
            if($me->coupon_id==$coupon){
                return true;
            }
        }
        return false;
    }




}
