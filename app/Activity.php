<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function tasks(){
        return $this->hasMany('App\Task');
    }

    public function pullSchedule(){
        return $this->hasOne('App\PagePullSchedule', 'activity_id');
    }
}
