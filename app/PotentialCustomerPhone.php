<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PotentialCustomerPhone extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function potentialCustomer(){
        return $this->belongsTo('App\PotentialCustomer');
    }
}
