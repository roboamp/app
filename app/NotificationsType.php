<?php

namespace App;

use App\Partials\PartialTemplate;
use Illuminate\Database\Eloquent\Model;

class NotificationsType extends Model
{
    //
    protected $table = "notifications_type";
    public $timestamps = false;

    public function status(){
        return $this->belongsTo('App\Notify');
    }



}

