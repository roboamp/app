<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpProperty extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function client(){
        return $this->belongsTo('App\TmpClient', 'client_id');
    }

    public function bucket(){
        return $this->belongsTo('App\PriceBucket', 'price_bucket_id');
    }

    public function sections(){
        return $this->hasMany('App\PageSection', 'property_id');
    }

    public function pages(){
        return $this->hasMany('App\TmpPage', 'property_id');
    }

    public function pages_generated(){
        return $this->hasMany('App\PageGenerated', 'property_id');
    }

    public function total_pages(){
        return $this->sections->map->pages->flatten()->count();
    }
}
