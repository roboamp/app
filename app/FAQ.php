<?php

namespace App;

use App\MyClasses\MyArray;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FAQ extends Model
{
    use HasFactory;

    protected $table = "questions";
    protected $guarded = [];

    // question -> user
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    // question -> category
    public function category()
    {
        return $this->hasOne(Category::class, 'category_id');
    }

    public static function get_public_group_by_category($category_id)
    {
        return self::orderBy('category_id')->where('privacy', '=', 0)->where('category_id', '=', $category_id)->get();
    }

    public static function list_questions($privacy){

        if($privacy=="all"){
            $FAQ= FAQ::where('privacy', '=', 'private')
                ->orWhere('privacy', '=', 'public')
                ->orWhere('privacy', '=', 'secret')
                ->orderBy('category_id','ASC')
                ->get();
        }else{

            $FAQ=FAQ::where('privacy', '=', $privacy)->orderBy('category_id','ASC')->get();
        }

        return $FAQ->groupBy('category_id');

    }

    public static function search_by_title($search)
    {
        return FAQ::where('question', 'like', '%' . $search . '%')->get();
    }

    public static function pending_question()
    {
        return FAQ::orderBy('question')->where('answer', '=', null)->get();
    }

    public static function edit_validation($id)
    {
        return $id->update(request()->validate([
            'question' => ['required', 'min: 5', 'max: 255'],
            'answer' => 'required',
            'category_id' => 'required',
            'privacy' => 'nullable'
        ]));
    }

    public static function store_validation()
    {
        return request()->validate([
            'question' => ['required', 'min: 5', 'max: 255'],
        ]);
    }

    public static function privacy_array_filter($id) {
        $my_arr = new MyArray();
        $privacyOptions = ['public', 'private', 'secret'];
        return $privacyOptions = $my_arr->search_for_string_and_remove($id->privacy, $privacyOptions);
    }
}
