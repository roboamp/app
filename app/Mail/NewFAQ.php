<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewFAQ extends Mailable
{
    use Queueable, SerializesModels;

    public $answer;
    public $email;
    public $question;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($question, $email, $answer)
    {
        $this->question = $question;
        $this->answer = $answer;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.newFAQ')
            ->subject('New Question Submission');
    }
}
