<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = "copertus@gmail.com";
        $subject = "This is a very cool demo!";
        $name = 'Roberto Inetti';

        return $this->view('emails.sender')
            ->from($address, $name)
           // ->cc('mkane@mailgun.com', "Michael Kane")
           // ->bcc($address, $name)
           // ->replyTo($address, $name)
            ->subject($subject)
            ->with([ 'message' => $this->data['message'] ]);

    }
}
