<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendReport extends Mailable
{
    use Queueable, SerializesModels;

    public $report;

    public $url;
    public $asset;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($report, $url_status, $asset)
    {
        $this->report = $report;

        $this->url = $url_status;

        $this->asset = $asset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.lighthouseReport')
            ->subject('RoboAmp Report');
    }
}
