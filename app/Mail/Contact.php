<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public  $data=[];

    public function __construct($data)
    {
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $address = 'info@roboamp.com';
        $subject = $data['email'] . ' has contacted ROBOAMP';
        $name = 'ROBOAMP';

        // dd($data['contact_message']);

        return $this->view('emails.contact', $data)
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->subject($subject);
    }
}
