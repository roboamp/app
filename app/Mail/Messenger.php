<?php

namespace App\Mail;

use App\MyClasses\NameParser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MyClasses\Emojis;

class Messenger extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public  $data=[];

    public function __construct($data)
    {
        $this->data=$data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emoji=new Emojis();
        $from = 'info@roboamp.com';
        $name_parser=new NameParser();
        $name = 'ROBOAMP';
        $info['header']='';
        if(isset($this->data['username'])) {
            $username = $name_parser->parse_name($this->data['username']);
            $subject = 'Welcome to ROBOAMP!';
            $hello = "!";//"  " . $emoji->get_emoji_by_shortcut(":wave-hand:", "php");
            $info['username']=$username['salutation']." ".$username['fname'];
            $info['signature']="<hr>We make the Web faster!";
            $info['action']='';
        }


        switch($this->data['type']){
            case "welcome":
                $info['title']="Welcome to ROBOAMP";
                $info['username'].= $hello;
                $message="Now that you've created an account, there's just a couple more steps before you get a lightning-fast website:<br>";
                $message.="<ol><li>Add your website URL in your ". $this->dashboard('dashboard')."</li>";
                $message.="<li>Copy and Paste the line of code that ROBOAMP generates into your website</li>";
                $message.="</ol><br>Check out your customer dashboard here: ".$this->dashboard()."<br><br>Thank you!<br>";
                $info['header']=" Hi, ".$info['username']."<br>Welcome to ROBOAMP <br>";
                $info['action']="<a href=\"".route('customers.dashboard')."\" style=\"color: #ffffff; text-decoration: none;\">AMP YOUR PAGE!</a>";
                break;
            case 'admin_welcome':
                $info['title']="New User Signup";
                $info['header']=" We have a new user! <br>";
                $message=$info['username']." has created a new account ";
                $message.="her/his email is ".$this->data['email'];

                break;
            case 'waiting_on_google':

                $subject="Almost there!";
                $info['title']=$subject;
                $message="Hi ".$info['username'].",<br>";
                $message.="<br>We wanted to let you know that we’ve finished everything on our end and submitted your new page to Google to be cached!";
                $message.="<br><br>We’re waiting for Google now and we’ll let you know as soon as everything is up and running.<br>Thanks!";
                break;
            case 'admin_waiting_on_google':

                $subject="Property confirmed!";
                $info['title']=$subject;
                $message="User ".$info['username']." has confirmed the property<br>";

                break;





            case 'all_is_done':
                $subject="You're really flying now!";
                $info['title']=$subject;
                $message="Hi ".$info['username'].",<br>";
                $message.= "Your new page has been cached by Google and everything is up and running.<br>";
                $message.="<br>Before ROBOAMP your loading time was [[value]]. Now it’s [[VALUE]]. That’s [[value]] faster!";
                $message.="Try it out on mobile and see for yourself [URL]";

                break;


        }


        $info['content']=$message;

        return $this->view('emails.welcome',$info)
                    ->from($from, $name)
                    ->replyTo($from, $name)
                    ->subject($subject);
    }
    private function dashboard($copy=null){

        if(is_null($copy))$copy=route('customers.dashboard');

        return "<a href=\"".route('customers.dashboard')."\">".$copy."</a>";

    }
}
