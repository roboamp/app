<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ampcomponent extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function template(){
        return $this->hasMany('App\Amptemplate');
    }

    public function attributes(){
        return $this->hasMany('App\Attribute', 'component_id', 'id' );
    }
}
