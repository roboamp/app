<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DemoStatus extends Model{

    protected $table="demo_status";
    public $timestamps=false;

    public function demo(){
        return $this->belongsTo('App\Demo');
    }
    public function demo_page(){
        return $this->belongsTo('App\DemoPage');
    }
}
