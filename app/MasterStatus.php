<?php

//this model is going to be the only model refering to any status moving forward
namespace App;
use App\MyClasses\MyArray;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterStatus extends Model
{
    use HasFactory;

    protected $table = 'statuses';
    protected $guarded = [];

    public function timelines() {
        return $this->belongsToMany(Timeline::class, 'status_timeline', 'status_id', 'timeline_id')
            ->withTimestamps()
            ->withPivot(['description', 'timeline_id', 'id']);
    }
}
