<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubDomain extends Model
{
    protected $table = "subdomains";

}
