<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function activity(){
        return $this->belongsTo('App\Activity');
    }

    public function timeframe(){
        return $this->belongsTo('App\Timeframe');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
