<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\MyClasses\Notifications;
use App\Notifications\CouponNotification;
use App\MyClasses\Dates;

class Coupon extends Model
{
    use Notifiable;

    public $incrementing = false;

    protected $fillable=['id'];
    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public static function is_valid_coupon($coupon){

        $now=Carbon::now('US/Central');
        $error_message=null;
        if(count($coupon)>0) {
            $coupon=$coupon[0];
            if ($coupon->usage < $coupon->max_redemptions) {
                if ($now < $coupon->redeem_by) {
                    return true;

                } else {
                    $error_message = "Coupon has expired.";
                }
            } else {
                $error_message = "Max Redemptions limit reached.";
            }
        }


        return false;
    }
    public function routeNotificationForSlack($notification=NULL){
        return Notifications::Hook($notification);
    }
    public function send_notification($message,$notification_type='product',$status='success'){

        $this['channel']='#'.$notification_type."-alerts";
        $this['content']=$message;
        $this['status']=$status;
        $this['url']=route('users.coupons.edit',$this->id);
        $this['notification_type']=$notification_type;
        $this['redeem_by_in_date_format']=Dates::timestamp_to_date($this->redeem_by);

        $this->notify(new CouponNotification($this));
    }


}
