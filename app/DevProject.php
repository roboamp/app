<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DevProject extends Model
{
    use HasFactory;

    protected $table = 'dev_projects';
    protected $guarded = [];

    public function dev_contractor() {
        return $this->belongsTo(DevContractor::class);
    }

    public function dev_pages(){
        return $this->hasMany(DevPage::class, 'dev_project_id');
    }

    public function timeline(){
        return $this->hasOne(Timeline::class);
    }

}
