<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceBucket extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function property(){
        return $this->hasMany('App\TmpProperty');
    }
}
