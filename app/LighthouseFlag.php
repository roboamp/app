<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LighthouseFlag extends Model
{
    protected $guarded = [];
    public function reports()
    {
        return $this->belongsToMany('App\LighthouseReport');
    }
}
