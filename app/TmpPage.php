<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TmpPage extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function property(){
        return $this->belongsTo('App\TmpProperty');
    }

    public function section(){
        return $this->belongsTo('App\PageSection');
    }

    public function pullSchedule(){
        return $this->hasOne('App\PagePullSchedule', 'page_id');
    }


}
