<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AmptemplateAttribute extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function template(){
        return $this->belongsTo('App\Amptemplate', 'component_id', 'id');
    }

    public function attribute(){
        return $this->belongsTo('App\Attribute');
    }
}
