<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Query extends Model
{
    use HasFactory;

    protected $table="search_logs";
    protected $guarded = [];

    public static function get_query_group_by_date(){
        return self::orderBy('created_at', 'DESC')->get();
    }

    public static function get_query() {
        return DB::table('search_logs')
            ->select('term', DB::raw('COUNT(*) as `count`'))
            ->groupBy('term')
            ->havingRaw('COUNT(*) > 0')
            ->orderBy('count', 'DESC')
            ->get();
    }
}
