<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generated extends Model
{
    protected $table = "r_pages_generated";

    public function property(){
        return $this->hasOne('App\Property','uuid','property_id');
    }

}
