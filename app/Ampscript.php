<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ampscript extends Model{

    protected $table='ampscripts';


    public function templates(){
        return $this->belongsToMany('App\Template', 'ampscript_template');
    }


}
