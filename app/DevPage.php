<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DevPage extends Model
{
    use HasFactory;

    protected $table = 'dev_pages';
    protected $guarded = [];

    public function dev_projects() {
        return $this->hasMany(DevProject::class);
    }
}
