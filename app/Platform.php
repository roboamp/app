<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    //this is an array of platforms for each property
    //the idea is to decide if we show the "select your platform option or not
    public function show_platforms($customer){
        $data=[];
        foreach($customer as $property){
            if($property->steps_id==1){
                $data[$property->id]=$this::all();
            }else{
                $data[$property->id]=$this::where('id','>',1)->get();
            }
            $data[$property->id]->default_code=$this->setup_property_default_code($data[$property->id],$property->platform_id,$property->id);
        }
        return $data;
    }
    private function setup_property_default_code($platforms,$platform_id,$property_id){
        foreach($platforms as $item){
            if($item->id==$platform_id){
                $item->selected="selected";
                $default_code=$this->platform_link($item->name,$property_id);
                break;

            }else{
                $item->selected="";
                $default_code="Select your platform";
            }
        }
        return $default_code;
    }
    public function sexy(){
        return 'sexy';
    }
    public function platform_link($platform_name,$property_id=null){

        $url="";
        $base_url=env('APP_URL','https://roboamp.com')."/amp";
        $full_url=$base_url."/".$this->id. "/".$this->url;
        $full_url=substr($full_url,0,-1);
        if(!is_null($property_id)) {
            $property = Property::where('id', $property_id)->select('id', 'url')->first();
            $url = $property->id . "/" . $property->url;
        }
        $url=(is_null($url)?"":$url);
        $str="Select your platform";

        switch($platform_name){
            case "HTML":
                $str = "<link rel='amphtml' href='".$full_url.$url."'>";
                break;
            case "Wordpress":
                $str="<link rel='amphtml' href='".$full_url.$property_id."/<?php echo urlencode(get_the_permalink()); ?>'>";
                break;
            case "Squarespace":

                $str = "<link rel='amphtml' href='".$full_url.">";
                break;
            case "Tumblr":
                $str = "{block:PermalinkPage}".
                    "<link rel='amphtml' href='".$full_url.$property_id."/{Permalink}'>{/block:PermalinkPage}";
                break;
            case "Blogger":
                $str = "<link rel='amphtml' expr:href='".$full_url.$property_id."/ + data:blog.url' />";
                break;
        }
        return $str;


    }
}
