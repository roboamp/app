<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function component(){
        return $this->belongsTo('App\Ampcomponent', 'component_id', 'id');
    }

    public function type(){
        return $this->belongsTo('App\AttributeType');
    }

    public function template_attribute(){
        return $this->hasMany('App\AmptemplateAttribute');
    }
}
