<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "products";

    public function plan(){
        return $this->belongsTo('App\Plan');
    }
}

