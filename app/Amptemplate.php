<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amptemplate extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function component(){
        return $this->belongsTo('App\Ampcomponent');
    }

    public function attributes(){
        return $this->hasMany('App\AmptemplateAttribute', 'template_id', 'id');
    }

    public function attr_active(){
        return $this->roles->map->abilities->flatten()->pluck('name')->unique();
    }
}
