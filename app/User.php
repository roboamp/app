<?php

namespace App;

use App\MyClasses\Notifications;
use App\MyClasses\Stripe\RoboStripe;
use App\Subscription as sub_model;
use App\Customer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Webpatser\Uuid\Uuid;
use Illuminate\Notifications\Notifiable;
use App\Coupon;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;



class User extends Eloquent implements Authenticatable
{
    use HasFactory;
    use AuthenticableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use Notifiable;
    protected $fillable = [
        'name',
        'email',
        'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'datetime',
        'uses_two_factor_auth' => 'boolean',
    ];
    public $slack_webhook_url = 'https://hooks.slack.com/services/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';


    //LOCAL SCOPES

    public function scopeAdmins($query){
        $query->where('user_role_id',3);
    }

    public function is_admin(){
        return $this->has_role('admin');
    }

    public function has_role($roleName) {
        if($roleName==$this->user_role->name){
            return 1;
        }
    }

    public function demos(){
        return $this->hasMany('App\Demo');
    }




    public function user_role(){
        return $this->hasOne('App\UserRole','id','user_role_id');
    }
    public function add_stripe_customer($token){
        $robostripe = new RoboStripe();
        $customer['email'] = $this->email;
        $customer['source'] = $token;
        $stripe_id = $robostripe->create('customer', $customer);

        self::where('email', $this->email)->update(['stripe_id' => $stripe_id]);
        return $stripe_id;
    }

    public function create_subscription($request,$customer=false,$email=false,$beta_subscription=0)
    {

        $robostripe = new RoboStripe();
        $plan = new Plan();

        $subscription_model = new sub_model();
        $plan_stripe_id = $request->input('plan_stripe_id');
        $property_id=$request->input('txt_property_id');




        if($customer){
            $user_stripe_id = $this->add_stripe_customer_from_customer($request->stripeToken,$email);
            $customer = Customer::where('email', $email)->first();
            $user_id = null;
            $customer_id = $customer->id;
            $email=$customer->email;
        }else{
            $user_stripe_id = $this->add_stripe_customer($request->stripeToken);
            $user_id= $this->id;
            $customer_id = null;
            $email=$this->email;


        }


        //we assume that the coupon is valid (locally) but we need to check on the server
        //and update local info is not valid
        $coupon =null;
        if($customer) {
            if (!is_null($customer->coupon_id)) {
                $coupon = $robostripe->retrieve('Coupon', $customer->coupon_id);
                if (!is_array($coupon)) {
                    if ($coupon->valid) {
                        $stripe_coupon = $coupon;
                        $coupon = $coupon->id;
                    }
                } else {
                    //send notification to sync with stripe
                    dd($customer->coupon_id, $coupon);

                    $coupon = null;
                    // $customer->c
                }
            }
        }


        //apply coupon to roberto|info@roboamp.com

        if($email=='roberto@roboamp.com' || $email=='info@roboamp.com'){
            $subscription = ['customer' => $user_stripe_id,'coupon'=>'launch', 'items' => [['plan' => $plan_stripe_id]]];

        }else{
            $subscription = ['customer' => $user_stripe_id, 'coupon'=>$coupon,'items' => [['plan' => $plan_stripe_id]]];

        }


        //update local coupon
        //send notification coupon has been redeemed


        $subscription = $robostripe->create('Subscription', $subscription);


        if(isset($subscription['error'])){return $subscription;}


        //save the record in the DB;
        $plan_id = $plan->get_plan_id_from_stripeID($plan_stripe_id);
        $uuid= Uuid::generate(4);
        $subscription_model->id=$uuid;
        $subscription_model->user_id = $user_id;
        $subscription_model->customer_id = $customer_id;
        $subscription_model->plan_id = $plan_id;
        $subscription_model->stripe_id = $subscription;
        $subscription_model->property_id=$property_id;
        $subscription_model->save();

        //update local coupon
        if(!is_null($coupon)) {
            $local_coupon = Coupon::find($stripe_coupon->id);
            $local_coupon->percent_off = $stripe_coupon->percent_off;
            $local_coupon->duration = $stripe_coupon->duration;
            $local_coupon->max_redemptions = $stripe_coupon->max_redemptions;
            $local_coupon->redeem_by = Carbon::createFromTimestamp($stripe_coupon->redeem_by)->toDateTimeString();
            $local_coupon->save();
        }else{
            //delete
        }







        Property::where('id',$property_id)->update(['status_id'=>2]);

        return ['error'=>0,
            'success'=>'Subscription has been completed',
            'subscription_id'=>$uuid->string
        ];


    }
    ///////
    ///
    //being called here but relates to customers creating new account
    //we need to refactor this
    public function add_stripe_customer_from_customer($token,$email)
    {
        $customer_instance= new Customer();
        $robostripe = new RoboStripe();
        $customer['email'] = $email;
        $customer['source'] = $token;
        $stripe_id = $robostripe->create('customer', $customer);

        //self::where('email', $this->email)->update(['stripe_id' => $stripe_id]);
        return $stripe_id;
    }


    //returns true if the customer belongs to the current logged User
    public function validate_customer($customer_id){

        $customer=Customer::where('id',$customer_id)->where('user_id',$this->id)->first();



        return (!is_null($customer)?$customer:null);
    }

    public function routeNotificationFor($driver,$notification=NULL){
        return Notifications::Hook($notification);
    }



    public function get_admins_by_type($user_type='business'){
        switch ($user_type){
            case 'business':
                return $this->where('email','luke@roboamp.com')
                    ->orWhere('email','tik@roboamp.com')
                    ->get();
                break;
            case 'product':
                return $this->where('email','roberto@roboamp.com')->get();
                break;

        }
    }


    public function create_beta_subscription($request,$email){
        $plan_stripe_id = $request->input('plan_stripe_id');
        $property_id=$request->input('txt_property_id');


        $subscription_model = new sub_model();
        $plan = new Plan();
        $customer = Customer::where('email', $email)->first();
        $customer_id = $customer->id;
        $email=$customer->email;


        //save the record in the DB;
        $plan_id = $plan->get_plan_id_from_stripeID($plan_stripe_id);
        $uuid= Uuid::generate(4);
        $subscription_model->id=$uuid;
        $subscription_model->user_id = null;
        $subscription_model->customer_id = $customer_id;
        $subscription_model->plan_id = $plan_id;
        $subscription_model->stripe_id = "BETA";
        $subscription_model->property_id=$property_id;
        $subscription_model->save();

        return ['error'=>0,
            'error'=>'Subscription has been completed',
            'subscription_id'=>$uuid->string
        ];

    }

}
