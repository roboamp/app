<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{

    //for each property, attach all the steps needed depending ont he selected platform
    public function get_steps_for_properties($properties,$flatten=1){
        $data=$tmp_data=[];
        if($properties->isEmpty()){return $this->render_steps(0,2,"null");}

        $obj=$properties->first();
        if($obj->steps_id==1 && $obj->platform_id==1){
            return $this->render_steps($obj->steps_id, 2, $obj->id);
        }else {
          return $this->render_steps($obj->steps_id, $obj->platform_id, $obj->id);
        }



    }


    //shortcut for HTML websites
    public function steps_for_html($step){
        return $this->render_steps($step,2);
    }

    //receives current step and format the HTML for the output

    //this is going to change depending on the selected property
    //for the beta the user has only one property


    public function render_steps($current_step,$platform=1,$row_id){
        if($platform==1)$platform=2;
        $steps=$this::where('platform_id',$platform)->get();
        if(is_null($current_step)){
            $property=Property::where('id',$row_id)->first();
            $current_step=$property->steps_id;
        }
        $j=1;
        $str="<tbody id=\"tbody_".$row_id."\">";


        foreach($steps as $step){
            if($current_step==2){
                $step->copy=str_replace("disabled"," value=\"".$row_id."\"",$step->copy);
            }
            $check_id='step'.$j.'_'.$row_id;
            if($current_step<$j){
                $class="alert-info'";
                $check_mark="";
            }else{
                $class="alert-success'";
                $check_mark="<i class='far fa-2x fa-check' aria-hidden='true' id='".$check_id."'></i>";
            }
            $str.="<tr>";
            $str.="<td width='100%' class='text-center " .$class.">";
            $str.=$step->copy."&nbsp;&nbsp;	&nbsp;".$check_mark;
            $str.="</td></tr>";
            $j++;
        }
        return $str."</tbody>";
    }




}
