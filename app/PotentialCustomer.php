<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PotentialCustomer extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function emails(){
        return $this->hasMany('App\PotentialCustomerEmail');
    }

    public function phones(){
        return $this->hasMany('App\PotentialCustomerPhone');
    }
}
