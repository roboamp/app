<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlugFilter extends Model
{
    protected $table='slug_filters';
    public $timestamps = false;

}
