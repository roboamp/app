<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use App\User;

class ReferralLink extends Model
{
    protected $fillable = ['user_id', 'referral_program_id'];

    protected static function boot(){
        static::creating(function (ReferralLink $model) {
            $model->generateCode();
        });
    }

    private function generateCode(){
        $this->code = (string)Uuid::uuid1();
    }

    public static function getReferral($user, $program){
        return static::firstOrCreate([
            'user_id' => $user->id,
            'referral_program_id' => $program->id
        ]);
    }

    public function getLinkAttribute(){
        return url($this->program->uri) . '/' . $this->code;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function program(){
        return $this->belongsTo(ReferralProgram::class, 'referral_program_id');
    }

    public function relationships($id){

        $referral_link = ReferralLink::find($id);
        $referral_program = ReferralProgram::find($referral_link->referral_program_id);

        if($referral_program->name == 'partners'){
            return @count(User::where('referral_link_id', '=', $id)->get());   
        }else{
            return @count(Customer::where('referral_link_id', '=', $id)->get());   
        }
    }
}
