<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use Webpatser\Uuid\Uuid;

class American extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'american';

    public $my_output;
    public $input;



    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Outputs a bunch of URLs for the demo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        dd('https://americanreceivable.com/','https://amp.americanreceivable.com/','https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Famp-americanreceivable-com.cdn.ampproject.org%2Fc%2Fs%2Famp.americanreceivable.com','https://developers.google.com/speed/pagespeed/insights/?url=americanreceivable.com%2F');


    }



}
