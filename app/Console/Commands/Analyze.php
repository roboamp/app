<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MyClasses\Server;
use App\MyClasses\Cli\CliSetup;
use App\MyClasses\Cli\Cli;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\MyClasses\URL;

class Analyze extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:analyze';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $live;
    public $input;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $server=new Server();
        $this->live=!$server->testing_server();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $url=new URL();

        $cliSetup=new CliSetup($this);
        $data=$cliSetup->data;
        $cli=new Cli($data);

        $file_names=['assets.html','detailed.html','domains.html','help.html', 'index.html', 'pages.html','toplist.html'];

        $domain=$cli->ask('Enter domain name: ');
        $domain=$url->get_domain_info($domain);


        $process = new Process(array('sitespeed.io', $domain['full_domain_with_schema'],'--outputFolder=public/reports/speed/'.$domain['full_domain']));
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();

        foreach($file_names as $files){
            //search for a string on those files
        }

        //after everything is done, go and replace images









    }
}
