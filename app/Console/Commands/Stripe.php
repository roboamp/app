<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\MyClasses\Stripe\RoboStripeCli;
use App\MyClasses\Server;
use App\MyClasses\Cli\Cli;
use App\MyClasses\Cli\CliSetup;
use Symfony\Component\Console\Helper\SymfonyQuestionHelper;


class Stripe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:Stripe';


    //arrays for the submenus


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes and creates records on Stripe';

    private $live;

    public $input;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($skip=null){

        if(!is_null($skip))dd($skip,"Spyderman");
        parent::__construct();
        $server= new Server();
        $this->live=!$server->testing_server();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        $cliSetup=new CliSetup($this);
        $data=$cliSetup->data;
        $cli=new Cli($data);
        $this->menu()->setForegroundColour($cli->colors['menuForegroundColour']);
        $this->menu()->setBackgroundColour($cli->colors['menuBackgroundColour']);



        //$cli=new Cli($this->setup_cli());

        $option = $this->menu('Stripe Options', ['Refresh',
            'Delete All Records','Create Base Data','Create 50 users','Coupons'

        ])->open();
        $arr_functions=['refresh','delete_all_testing_data','create_base_data','create_50_users_for_testing','coupons_menu'];


        $RoboStripeCli=new RoboStripeCli($cli);

        call_user_func(array($RoboStripeCli,$arr_functions[$option]));


    }






}
