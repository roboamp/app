<?php

namespace App\Console\Commands;


use App\MyClasses\Controllers;
use App\MyClasses\Curl;
use App\MyClasses\Files;
use App\MyClasses\Paths;
use App\MyClasses\Routes;
use Illuminate\Console\Command;
use App\MyClasses\Emojis;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use App\MyClasses\Cli\CliStyle;
class demo_prep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demo_prep';

    protected $url;
    private $demo_url="http://127.0.0.1:8000/8fc33172-1b47-4eb3-95b9-92e3d4007912/mansfield-dentalcare.com";


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepares everything to run a demo';
    private $urls=[
            "http://127.0.0.1:8000/8fc33172-1b47-4eb3-95b9-92e3d4007912/mansfield-dentalcare.com",
            "https://mansfield-dentalcare.com",
            "https://www.dropbox.com/s/oqojuz5fssexryh/Gamestop.jpg?dl=0",
            "https://www.dropbox.com/s/nql4w4mrr9bf3da/mansfield-dental.jpg?dl=0",
            "https://www.dropbox.com/s/166dhq55ah7eq28/coravana.jpg?dl=0",
            "https://americanreceivable.com",
            "https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Famericanreceivable.com%2F",
            "https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Famp-americanreceivable-com.cdn.ampproject.org%2Fc%2Fs%2Famp.americanreceivable.com%2F%3Fid%3Db095d582-1417-45b8-9f96-d761776e9d6a%26page%3Dhttps%3A%2F%2Famericanreceivable.com"
        ];

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(){
        if(Curl::page_exist($this->demo_url)){
            //rename index page;
            $file_path_origin=base_path('parser/properties/mansfield-dentalcare.com/index.blade.php');
            $file_path_destination=base_path('parser/properties/mansfield-dentalcare.com/index.blade.php.'.time());

            $files=new Files();
            $files->copy_file($file_path_origin,$file_path_destination);
            $files->delete_file($file_path_origin);
            echo "Index File has been deleted\n";
            $this->handle();
        }else{
            $this->print_urls();
            //open the URLs using dusk



        }


    }
    private function print_urls(){
        foreach ($this->urls as $item){
            echo $item."\n\n";
        }
    }
    private function setup_demo(){
        system('clear');

        $this->create_demo_route();
        $this->create_demo_controller(1);
        $this->create_demo_folder(0);
        $this->create_progress_bars();




    }
    private function create_progress_bars(){
        $cli=new CliStyle();
        $cli->signature();

        $emojis=new Emojis();

        //$emojis_list=$emojis->get_emojis_list();

        $robot=$emojis->get_emoji_by_name('robot face','php');
        $operations=array(
            ['message'=>array("Pinging Website"),'start_label'=>'something','end_label'=>'ending_point','delay'=>2],
            ['message'=>array("Fetching HTML"),'end_label'=>'ending_point','delay'=>3],
            ['message'=>array("Writting AMP Code"),'end_label'=>'ending_point','delay'=>10],
            ['message'=>array("Validating AMP Code"),'start_label'=>'Batman','end_label'=>'ending_point','delay'=>5]


        );

        echo "\n\n\n";
        $j=0;

        foreach ($operations as $task) {
            if($j==3){
                $this->copy_template(1);

            }
            $total_delay=1;
            $total=100;
            $progressBar = $this->output->createProgressBar($total);
            $progressBar=$this->setup_bar_chars($progressBar,"=",$robot);
            $progressBar->setFormat("%status%\n%current%/%max% [%bar%] %percent:3s%%");
            //$progressBar->setProgressCharacter("\xF0\x9F\x8D\xBA");
            for ($i = 0; $i < 100; $i++) {
                if(isset($task['message'])) {
                    if ($i < 30) {
                        $progressBar->setMessage($task['message'][0], 'status');
                    } elseif ($i < 70) {
                        if(isset($task['message'][1])) {
                            $progressBar->setMessage($task['message'][1], 'status');
                        }
                    } else {
                        if(isset($task['message'][2])) {
                            $progressBar->setMessage($task['message'][2], 'status');
                        }
                    }
                }else{
                    $progressBar->setMessage("Activity started", 'status');
                }

                $progressBar->advance();



                usleep(5000 * $task['delay']);
            }
            $progressBar->finish();
            $j++;
            echo "\n\n";
        }
    }

    private function create_demo_route(){
        $routes=new Routes();
        if(($routes->has('demo'))==false){
            $routes->add_route('demo','/demo','index','DemoController');
        };
    }
    private function create_demo_controller($delete_if_exist=false){
        $controllers=new Controllers();
        if($delete_if_exist){
            $controllers->delete_controller('demo');
        }
        $controllers->create_controller('demo');
        $controllers->create_controller('demo');
        $controllers->add_method_to_controller('demo');
        $code="return view('demo.minor');";
        $controllers->insert_code_into_method($code);

    }
    private function create_demo_folder($delete_if_exist){
        $files=new Files();
        $files->create_folder('demo','view',$delete_if_exist);
    }

    private function setup_bar_chars($progressBar,$char,$cursor,$before_color="red",$after_color="green"){
        $progressBar->setBarCharacter("<fg=".$before_color.">".$char.'</>');
        $progressBar->setEmptyBarCharacter("<fg=".$after_color.">".$char."</>");
        $progressBar->setProgressCharacter("<fg=".$before_color.">".$cursor."</>");
        return $progressBar;
    }
    private function copy_template($delete_duplicates){
        $files=new Files();
        $base_path=base_path();
        //$origin_path=$base_path."/app/MyClasses/AMP/Includes/Demo/werd.blade.php";
        //$destination_path=$base_path."/resources/views/demo/werd.blade.php";

        //$origin_path=$base_path."/app/MyClasses/AMP/Includes/Demo/minor2.blade.php";
        //$destination_path=$base_path."/resources/views/demo/minor.blade.php";

        $origin_path=$base_path."/parser/properties/mansfield-dentalcare.com/index2.blade.php";
        $destination_path=$base_path."/parser/properties/mansfield-dentalcare.com/index.blade.php";
        $files->copy_file($origin_path,$destination_path,"//TimeGoesHere",Carbon::now('US/Central'),$delete_duplicates);

        $origin_path=$base_path."/parser/properties/mansfield-dentalcare.com/index.blade.php";
        $destination_path=$base_path."/resources/views/demo/index.blade.php";
        $files->copy_file($origin_path,$destination_path,"//TimeGoesHere",Carbon::now('US/Central'),$delete_duplicates);

    }

}
