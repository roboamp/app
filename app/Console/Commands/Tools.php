<?php

namespace App\Console\Commands;

use App\MyClasses\Cli\Cli;
use App\MyClasses\Cli\CliSetup;
use App\MyClasses\Directory;
use App\MyClasses\Files;
use App\MyClasses\Templates;
use App\MyClasses\URL;
use App\Page;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\MyClasses\Validate;
use App\MyClasses\Server;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use PhpSchool\CliMenu\Builder\CliMenuBuilder;
use App\MyClasses;
use App\MyClasses\MyArray;
use PhpSchool\CliMenu\CliMenu as class_menu;


use App\MyClasses\Cli\CliMenu;

//models
use App\Property;
//classes

use App\MyClasses\Cli\Properties\PropertiesURL;

class Tools extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:tools';

    protected $selected_option=null;
    protected $active_class=null;
    protected $top_menu=0;
    protected $server_url="127.0.0.1:8000/amp/";
    protected $users;
    //var used to print different stuff from other classes
    public $my_output;
    public $output;
    public $input;
    public $helper;
    /** @var  'App\MyClasses\Cli\Cli */
    public $cli;
    public $master_menu;
    public $sub_menus;


    //arrays for the submenus


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($cli=null){

       



        $this->cli=$cli;
        parent::__construct();


        $server= new Server();
        $this->live=!$server->testing_server();



    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $helpers['question']=$this->getHelper('question');
        $helpers['formatter']=$this->getHelper('formatter');
        $data=new CliSetup($this->input,$this->output,$helpers);
        $this->cli=new Cli($data);



        //CLI is ready to be passed to the child classes


        $local_method='App\Console\Commands\Tools';
        //start debugginge here

        $menu=new CliMenu($this->cli);
        $menu->source=$menu->get_main_menu_options();
        $menu->render();


        //$menu->display_main_menu();

        dd('yeah!!');
        $this->validate_options();
        dd();



        dd('main thread');
        dd();

       // $this->HTML_to_Blade();
        $this->create_url_reports();
        dd();
        $this->HTML_to_Blade();



        $this->menu_options=[
            ['Add Google Analytics Code',$local_method,'add_analytics_code_to_property'],

            ['HTML to Blade',$local_method,'HTML_to_Blade'],
            ['ENV',$local_method,'show_env_vars'],
            ['Reports',$reports_main_menu_class,'show_main_menu'],
            ['Properties',$main_menu_properties,'show_main_menu'],
            ['Stripe',$local_method,'Stripe_menu'],
            ['Database',$local_method,'show_database_menu'],
            ['break']
        ];


        $this->menu = new CliMenuBuilder();
        $this->menu->setTitle('Basic CLI Menu');

        foreach($this->menu_options as $item){
        //    echo count($item)."\n";
            if(count($item)>=3) {
                $this->menu->addItem($item[0], $this->create_callable($item[1], $item[2]));
            }else{
                $this->menu->addLineBreak('-o', 2);

            }
        }
        $master=$this->menu->build();

        $master->open();

        dd('done');


        //see /myClasses/CLI/Menu/Debug to see how to pass and unpack parameters

        $this->call('config:cache');
        $this->my_output=$this->output;



        $data= new \stdClass();







        $master_menu=new CliMenu($this->cli);

        $res=$master_menu->create_menu('Main Menu',$this->menu_options);

        if(!$this->live){
            $this->debug($res);

        }

        $res->open();
        //  $master_menu->add_separator();

        // $master_menu


    }
    private function debug($res){

        $res->addLineBreak('=',1);

        $arr=[
            ['label'=>'Debug','class'=>'App\MyClasses\Cli\Menu\Debug','method'=>'show_main_menu'],
            ['label'=>'Export Chrome Extension','class'=>'App\MyClasses\Cli\Menu\Chrome','method'=>'export_chrome_extension'],
            ['label'=>'Import Dev Property','class'=>'App\MyClasses\Cli\DevImport','method'=>'main']

        ];


        foreach($arr as $item){
            $class=$item['class'];
            $method=$item['method'];
            $res->addItem($item['label'],function() use ($class,$method){
                $menu= new $class;
                $menu->$method($this->cli);
            });
        }

        $res->addLineBreak('=',1);
    }

    public static function Stripe_menu(){
        \Artisan::call("ROBOAMP:Stripe");
    }
    public function show_env_vars($output){
        $command=new ENV();
        $command->handle(1,$output->output);
    }
    public function show_database_menu($output){
        \Artisan::call("ROBOAMP:Database");
    }
    public function create_callable($class,$method){
       // dd($class,$method);
        if($class==__CLASS__){
            if(is_array($method)){dd($method);};

            if(method_exists($this,$method)) {
           // $callable=array("class"=>$this,"method"=> $method);
                   return function() use ($method){
                       call_user_func(array($this, $method));
                       };



           }else{
                dd('method '.$method." doesn't exists\n");
            }
        }else{
            $myClass= new $class;

            return function(){
                return true;
            };
        }
    }




    public function list_all_pages($property=""){

        if($property!=""){
            $pages=Page::where('property_id','=','property_id')->get();
        }else {
            $pages = Page::where('label', '!=', '')->orderBy('property_id')->get();
        }

        foreach ($pages as $page){

            echo  $page->name."\t".$this->server_url.$page->url."\n";
        }
    }






    /////users OPTIONS
    ///
    public function new_user_options(){

        $now= Carbon::now();
        $name=$this->ask("Type a Full Name for the new User");
        $email=$this->ask("Type an email for the new User");
        $password = bcrypt('secret');
        $remember_token=str_random(10);

        DB::table('users')->insert([

            'name'=>$name,
            'email'=>$email,
            'password'=>$password,
            'test'=>false,
            'remember_token'=>$remember_token,
            'created_at'=>$now,
            'updated_at'=>$now

        ]);

    }


    public function validate_options(){

        //e88d8d9a-77fc-4bef-9568-4a610bd1debf
        //get a list of available properties

        Validate::validate_local_amp_property('e3bc51a6-2923-4b5f-a4da-30cb5385b232');
        dd("done!");

    }



    public function add_analytics_code_to_property(){
       echo "This is a new code";
        return true;

        return function(){
            return true;
        };
        dd();

        $cliSetup=new CliSetup($data);
        $data=$cliSetup->data;
        $this->cli=new Cli($data);
        $this->cli->ask("batman");
        dd($this->cli);



        $data=$cliSetup->data;
        $data['input']=$this->input;

        $this->cli=new Cli($data);

        $this->cli->output=$this->output;
        $this->cli->input=$this->input;


        $this->ask('nini');

        $menu = $this->menu('Pizza menu')
            ->addOption('mozzarella', 'Mozzarella')
            ->addOption('chicken_parm', 'Chicken Parm')
            ->addOption('sausage', 'Sausage')
            ->addQuestion('Make your own', 'Describe your pizza...');

        $itemCallable = function (CliMenu $cliMenu) use ($menu) {
            $cliMenu->askPassword()
                ->setValidator(function ($password) {
                    return $password === 'secret';
                })
                ->setPromptText('Secret password?')
                ->ask();

            $menu->setResult('Free spice!');

        };

        $menu->open();

        $q=$this->getHelper('question');
        $res=$q->ask($this->input,$this->output,new Question("nana\n"));


        dd($res);



        dd($this->cli->ask('tssss'));
        $data=$data[0];

        $helper=$data->getHelper('question');

        $question = new ConfirmationQuestion('Batman?', false);

        $res=$helper->ask($data->input, $data->output, $question);

        dd($res);

        $data['input']=$data->input;
        $data['output']=$data->output;
        $cli=new Cli($data);


        $helper = $data->cli->getHelper('question');
        $question = new ConfirmationQuestion('Batman?', false);




        if (!$helper->ask($data->input, $data->output, $question)) {
            return;
        }


        $this->execute($data->input,$data->input);

        $helper=$data->question_helper;

        $str= new Question('Banana');
        $val=$helper->ask($this->input, $this->output, $str);

        dd($val);
        $files=new Files();
        $res=$cli->ask('Enter Directory');
        dd($res);
        //$ask->ask($cli->input,$cli->output,$cli->getHelper('question'), 'something');
    }
    public function askme(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action?', false);

        if (!$helper->ask($input, $output, $question)) {
            return;
        }
    }
    ////// PROPERTIES FUNCTIONS
    public function create_url_reports(){
        $property_folder='northarlingtondentalcare.com';
        $url= new URL();

        $property=new Property();
        $property_id=$property->where('url','https://'.$property_folder)->pluck('id')->first();
        $template= new Templates();
        $property_domain=$url::get_domain_from_property_id($property_id);

        $white_label=$property->white_label($property_id);
        $pages['white_label']=$white_label;

        $property_class=new PropertiesURL();
        $pages['pages']=Page::where('property_id',$property_id)->get()->toArray();
        $pages['property_id']=$property_id;
        $property_url="https://amp.".$property_domain;

        $str="";
        foreach($pages['pages'] as $item){
            $url=$property_url."?id=".$property_id."&page=".$item['url'];
            $str.="<a href=\"".$url."\">".$url."</a><br>";
        }

        $res=$template->create_urls_report_from_tools($str,$property_domain);
        dd("done creating the report");
    }






}
