<?php

namespace App\Console\Commands;

use App\MyClasses\MyClasses;
use App\Property;
use Carbon\Carbon;
use Faker;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\MyClasses\CliFormat;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\MyClasses\Cli\CliDebug;
use App\MyClasses\Stripe\CliRoboStripe;
use App\MyClasses\Cli\CliCommon;
/*use PhpSchool\CliMenu\Builder\CliMenuBuilder;
use PhpSchool\CliMenu\Action\GoBackAction;
use PhpSchool\CliMenu\CliMenu;
use PhpSchool\CliMenu\MenuItem\SelectableItem;*/
use App\MyClasses\CliMenu;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Debug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:debug';

    protected $url;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $menu_options;

    private $menu;

    private $selectedOption;

    //private $menus_classes=['CliLandingPage','CliRoboStripe'];
    private $menus_classes=['CliRoboStripe'];
    //private $menus_classes=['CliDatabase'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function testing($menu){
        //echo $menu->getSelectedItem()->getText()."\n";
       // echo $menu->getSelectedItem()->getIndex()."\n";
        //dd($menu->getSelectedItem());
         $a=$menu->getSelectedItem();
        dd($a);
    }

    public function select()
    {
        $this->menu->open();
        return $this->selectedOption;
    }

    public function handle()
    {

        $url=$this->ask("URL");

        /*$properties = new Property();
        $urls = $properties->get_all_active_urls();*/
        $errors = [];
       // foreach ($urls as $item) {
            $process = new Process(array('amphtml-validator', $url));
            $process->run();
            if (!$process->isSuccessful()) {
                $error['errors'] = $process->getErrorOutput();
                $error['url'] = $url;
                $errors[] = $error;
            } else {
                echo $url . "\t\t*****PASS*****  \n\n";
            }
       // }

        dd($errors);

    }

}
