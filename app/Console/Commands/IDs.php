<?php
namespace App\Console\Commands;
use App\MyClasses\Analytics;
use App\MyClasses\Cli\Reports\Queries\Properties;
use App\MyClasses\Directory;
use App\MyClasses\Files;
use App\MyClasses\Paths;
use App\MyClasses\Templates;
use App\MyClasses\URL;
use FontLib\Table\Type\loca;
use Illuminate\Console\Command;
use App\MyClasses\Server;
use Webpatser\Uuid\Uuid;
use App\MyClasses\Strings;
use App\Property;
class IDs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:ids {url?}';
    public $my_output;
    public $input;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Random Ids, used in seeders';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
        $server= new Server();
        $this->live=!$server->testing_server();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $url= new URL();
        // $this->generate_ids();
        $urls=[
            "https://demo.roboamp.com/buyr/home",
            "https://demo.roboamp.com/buyr/category",
            "https://demo.roboamp.com/buyr/product",
            "https://demo.roboamp.com/buyr/search",
            //"https://demo.roboamp.com/amp/demos/consent",
            //"https://demo.roboamp.com/amp/demos/cookies",
        ];
        $amp_urls=[];
        foreach ($urls as $item){
            $amp_urls[]="https://".$url->make_amp_url_from_url($item,1);
            echo "\n". end($amp_urls)."\n";
        }
        $base_url="https://developers.google.com/speed/pagespeed/insights/?url=";
        $i=0;
        foreach ($amp_urls as $item){
            echo "\n".$base_url.urlencode($item) ."\n";
            echo "\n".$base_url.urlencode($this->get_original_url($urls[$i])) ."\n";
            echo "---------------------------------------\n";
            echo "---------------------------------------\n";
            $i++;
        }
        //https://developers.google.com/speed/pagespeed/insights/?url=
        dd();
        $amp_form="https://parser.roboamp.com/f638e0e0-fee6-4a5e-b917-3b4deed70268/index";
        $res=$url->make_amp_url_from_url($amp_form,0);
        $amp_form="https://parser.roboamp.com/f638e0e0-fee6-4a5e-b917-3b4deed70268/accessories";
        $res1=$url->make_amp_url_from_url($amp_form,0);
        $amp_form="https://parser.roboamp.com/f638e0e0-fee6-4a5e-b917-3b4deed70268/product";
        $res2=$url->make_amp_url_from_url($amp_form,0);
        $amp_form="https://parser.roboamp.com/f638e0e0-fee6-4a5e-b917-3b4deed70268/news";
        $res3=$url->make_amp_url_from_url($amp_form,0);
        $amp_form="https://parser.roboamp.com/f638e0e0-fee6-4a5e-b917-3b4deed70268/news_article";
        $res4=$url->make_amp_url_from_url($amp_form,0);
        dd($res,$res1,$res2,$res3.$res4);
        dd();
        // $this->generate_ids();
        // $amp_form='https://amp.dev/documentation/examples/interactivity-dynamic-content/favorite_button/?format=websites';
        $amp_form="https://demo.roboamp.com/cookies";
        $res=$url->make_amp_url_from_url($amp_form,0);
        dd($res);
        //////////////////////////////////////
        ///
        ///
        ///
        ///
        $this->encode_url($this->argument('url'));
        dd();
        ///         $res=$url->make_amp_url_from_url('https://parser.roboamp.com/b03d9340-b144-4e38-888d-38901a6b6a3a/gamestop.com');
        $american_receivable="https://amp.americanreceivable.com/?id=b095d582-1417-45b8-9f96-d761776e9d6a&page=https%3A%2F%2Famericanreceivable.com%2F";
        dd($res);
        $this->generate_ids();
        dd();
        $property_domain='demo_analytics.com';
        $this->insert_analytics($property_domain);
        dd();
        $res=$url->make_amp_url_from_url('https://parser.roboamp.com/075c9bb1-678c-4986-9c64-05b43fa1c00d/https%3A%2F%2Fsignup.com%2F%2FIdea-Center');
        dd($res);
        $this->show_main_urls();
        dd();
        $this->auto_replace_amp_sidebars();
        dd("done");
        $url= new URL();
        $res=$url->make_amp_url_from_url('https://parser.roboamp.com/b03d9340-b144-4e38-888d-38901a6b6a3a/gamestop.com');
        $res1=$url->make_amp_url_from_url('https://parser.roboamp.com/7042e415-e19b-4511-8486-8de569b51ecd/https%3A%2F%2Fshockwaveinnovations.com%2Fblog');
        dd($res,$res1);
        //dd(str_replace($))
        $res=$url->make_amp_url_from_url('https://parser.roboamp.com/7042e415-e19b-4511-8486-8de569b51ecd/https%3A%2F%2Fshockwaveinnovations.com');
        $res1=$url->make_amp_url_from_url('https://parser.roboamp.com/7042e415-e19b-4511-8486-8de569b51ecd/https%3A%2F%2Fshockwaveinnovations.com%2Fblog');
        dd($res,$res1);
    }
    public function generate_ids(){
        for($j=0;$j<15;$j++){
            $id= Uuid::generate(4);
            echo "\n".$id."\n";
        }
        dd();
    }
    public function show_main_urls(){
        $properties= new Property();
        $res=$properties::all();
        foreach($res as $item){
            $url='127.0.0.1:8000/'.urlencode($item->id."/".$item->url)."\n";
            echo $url;
        }
    }
    //first check if there is an analytics include file - DONE
    //if it doesn't exist, copy it from the templates folder -DONE
    //then check if the google plugin is being added, if not, add it
    //finally check if the analytics @include exist, if not add it
    public function insert_analytics($property_domain){
        $analytics=(new Analytics())->run($property_domain);
    }
    public function encode_url($url){
        $base_url='http://127.0.0.1:8000/3a256d94-c27c-46a3-b466-ff90709c0277/';
        echo $base_url.urlencode($url);
    }
    public function get_original_url($url){
        $original_url=explode(".com/",$url);
        return "https://".str_replace("/",".com/",$original_url[1]);
    }
}