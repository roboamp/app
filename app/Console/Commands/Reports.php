<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MyClasses\Server;
use App\MyClasses\Cli\Cli;
use App\MyClasses\Cli\CliSetup;
use App\MyClasses\Cli\CliMenu;


class Reports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:Reports {option=""}';

    public $my_output;
    public $input;
    public $menu;
    private $cli;
    private $callables;
    private $active_menu='show_menu_';
    private $menu_options;



    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $server= new Server();
        $this->live=!$server->testing_server();


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        system('clear');

        $cliSetup=new CliSetup($this);
        $data=$cliSetup->data;
        $this->cli=new Cli($data);
        $this->cli->output=$this->output;
        $this->menu_options=[
            ['Properties','App\MyClasses\Cli\Reports\PropertiesCliMenu','show_main_menu'],
            ['properties','properties_function'],
            ['pizza','some_function']];

        $master_menu=new CliMenu($this->cli);
        $master_menu->create_menu('Reports',$this->menu_options)->open();
        //$option=$this->create_menu('Reports',$this->menu_options)->open();

    }

    public static function properties_function(){
        dd('buuuuu');
    }











}
