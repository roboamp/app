<?php

namespace App\Console\Commands;

use App\MyClasses\cli\CliProperties;
use App\Page;
use App\Property;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use File;
use App\MyClasses\Cli\CliStyle;
use App\MyClasses\Validate;
use App\MyClasses\Server;
use App\MyClasses\Cli\CliSetup;
use App\MyClasses\Cli\Cli;
use App\MyClasses\Maker\Maker;

class Make extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:make {option : What to Make}{--name= : Name of the tool}';

    protected $selected_option=null;
    protected $active_class=null;
    protected $top_menu=0;
    protected $server_url="127.0.0.1:8000/amp/";
    protected $users;
    //var used to print different stuff from other classes
    public $my_output;

    public $input;


    //arrays for the submenus


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $server= new Server();
        $this->live=!$server->testing_server();




    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        system('clear');
       // dd($this->argument('option'), $this->option('name'), "commands/Make.php");
        $maker = Maker::make($this->argument('option'), $this->option('name'));
    }

}
