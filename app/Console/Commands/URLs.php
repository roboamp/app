<?php


namespace App\Console\Commands;

use App\MyClasses\URL;
use Illuminate\Console\Command;
use App\MyClasses\Server;
use Webpatser\Uuid\Uuid;

class URLs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:URLs';

    public $my_output;
    public $input;



    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Random Ids, used in seeders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $server= new Server();
        $this->live=!$server->testing_server();


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){


        for($j=0;$j<15;$j++){
            $id= Uuid::generate(4);
            echo "\n".$id."\n";
        }

        $url= new URL();
        $res=$url->make_amp_url_from_url('https://amp.mansfield-dentalcare.com');
        $res1=$url->make_amp_url_from_url('https://amp.americanreceivable.com');
        dd($res,$res1);
    }



}
