<?php


namespace App\Console\Commands;

use App\Property;
use Illuminate\Console\Command;
use App\MyClasses\Server;
use App\MyClasses\Cli\Cli;
use App\MyClasses\Cli\CliSetup;
use DB;
use App\MyClasses\DB as myDB;
use Illuminate\Database\QueryException;
use Symfony\Component\Process\Process;

class Database extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:Database';

    public $my_output;
    public $input;



    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $server= new Server();
        $this->live=!$server->testing_server();


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        system('clear');

        $cliSetup=new CliSetup($this);
        $data=$cliSetup->data;
        $cli=new Cli($data);
        $this->menu()->setForegroundColour($cli->colors['menuForegroundColour']);
        $this->menu()->setBackgroundColour($cli->colors['menuBackgroundColour']);



        $option = $this->menu('Database Options', ['Import Customers from Prod to QA',
            'Create SHH Tunnel','Kill SHH Tunnels'])->open();

        $arr_functions=['import_customers','create_shh_tunnel','kill_shh_tunnels'];


        call_user_func(array($this,$arr_functions[$option]));




    }
    public function import_customers(){



        //delete local stuff
        $myDB=new MyDb();
        $myDB->truncate('pages');
        $myDB->truncate('properties');
        $myDB->truncate('customers');

       // $this->rename_customers_folders();

        // Connect to production database
        $live_database = DB::connection('mysql_prod');

        try {
            $res=$live_database->table('customers')->where('testing',0)->get();
        }catch (QueryException $e){

            if($e->getCode()==2002){
                echo "Connection Failed\n\n";
                echo "Trying to automatically open the Tunnel\n\n";
                $live_database=$this->retry_connection();
                $res=$live_database->table('customers')->where('testing',0)->get();

            }else {
                dd($e->getMessage(), $e);
            }
        }

        // Get table data from production



        foreach($res as $data){
            // Save data to staging database - default db connection
            DB::table('customers')->insert((array) $data);
        }

        $res=$live_database->table('properties')->get();

        foreach($res as $data){
            DB::table('properties')->insert((array) $data);
        }
        $res=$live_database->table('pages')->get();

        foreach($res as $data){
            DB::table('pages')->insert((array) $data);
        }


        echo "Finished importing Customers\n";
    }



    function create_shh_tunnel(){
        $server = "159.65.73.143";
        $username = "forge";
        $str = "ssh -f " .$username. "@" .$server. " -L 3307:127.0.0.1:3306 -N";

        $process = new Process($str);
        $process->disableOutput();
        $process->run();
        echo "\nTunnel has been created\n";


    }
    function kill_shh_tunnels(){
        exec('sudo killall ssh');
        echo "\nAll SHH tunnels have been closed\n";
    }
    private function retry_connection(){
        $this->create_shh_tunnel();
        return DB::connection('mysql_prod');

    }




}
