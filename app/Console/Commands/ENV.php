<?php

namespace App\Console\Commands;

use App\MyClasses\MyArray;
use Illuminate\Console\Command;
use App\MyClasses\Server;
use App\MyClasses\Cli\Cli;
use App\MyClasses\Cli\CliSetup;
use App\Customer;
use App\MyClasses\Emojis;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Helper\addRow;
use DB;

class ENV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:ENV {menu=""}';

    public $my_output;
    public $input;
    protected $symfonyStyle;
    protected $table;
    protected $headers;




    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display Critical ENV variables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($output=null)
    {
        parent::__construct();
        $server= new Server();
        $this->live=!$server->testing_server();
        //$this->my_output=(is_null($output)?$this->output():$output);


        //dd($this->my_output);

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle($menu=false,$output){

        if($menu)$this->output=$output;

        $this->table = new Table($this->output);


        // Create a new TableSeparator instance.
        $separator = new TableSeparator;

        // Set the table headers.

        $server=new Server();

        $server_info=$server->server_info();
        $myArray=new MyArray();
        $headers=$myArray->get_key_names_from_array($server_info);


        $tableStyle = new TableStyle();
        $tableStyle->setPadType(STR_PAD_BOTH);

        $this->headers=$this->set_table_header('SERVER INFO',$headers);

        $table_rows=$myArray->replace_booleans_with_x($server_info,0);
        $this->table->setHeaders($this->headers);
        $this->table->setRows([
            $myArray->get_values_from_array($table_rows),
            new TableSeparator(),
        ]);

        $this->show_db_info();





        //dd($myArray->get_keys_from_array($table_rows),$myArray->get_values_from_array($table_rows));

        $this->table->setStyle($tableStyle)->render();


    }

    public function add_table_header($caption,$cols=null){
        $total_columns=(is_null($cols)?count($this->headers[1]):$cols);
        $this->table->addRow(
            [new TableCell($caption, ['colspan' => $total_columns])]
        );
    }


    public function set_table_header($caption,$header=null){

        $total_columns=(is_null($header)?count($this->headers):count($header));

        return array(
            array(new TableCell($caption, array('colspan' => $total_columns))),
            $header,
        );
    }
    public function add_separator(){
        return new TableSeparator();

    }
    public function add_row($array,$separator=1){
        $separator=($separator==1?$this->add_separator():[]);
        return $this->table->addRows([$array,$separator]);

    }
    public function show_db_info(){
        $this->add_table_header('Database');
        $env=strtoupper(env('APP_ENV'));

        $connection='mysql';
        $message="<bg=red;fg=WHITE;options=bold> DATABASE CONNECTION FAILED</>";
        try {
            DB::connection($connection)->getDatabaseName();

            $message= "<bg=green;fg=black;options=bold>".DB::connection($connection)->getDatabaseName()."</>";
        }catch(\Exception $e){
            $e->getMessage();
        }

        $this->add_table_header($message);

    }







}
