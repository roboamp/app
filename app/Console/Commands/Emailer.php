<?php

namespace App\Console\Commands;

use App\Customer;
use App\MyClasses\MyArray;
use Carbon\Carbon;
use Illuminate\Console\Command;
//use App\Notify;
use App\MyClasses\Cli\CliMenu;
use App\MyClasses\Cli\CliSetup;
use App\MyClasses\Cli\Cli;

class Emailer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:Emailer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $notifications=[];
    public $my_output;
    public $input;
    public $menu;
    private $cli;



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$notications= new Notify();
        //$this->notifications=$notications->get()->toArray();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        $cliSetup=new CliSetup($this);
        $data=$cliSetup->data;
        $this->cli=new Cli($data);
        $this->cli->output=$this->output;

        //dd($this->notifications);
        $this->customers_withuot_properties();
    }


    private function customers_withuot_properties(){
        $customer=new Customer();
        $myArray=new MyArray();
        $customers=$customer->customers_without_properties(null,null);
        $date=  Carbon::now();
        $date->setTimezone('US/Central');


        foreach($customers as $target){

            if(is_null($target['latest_notification_date']) && is_null($target['notify_id'])){
                $date_difference=$date->diffInDays($target['created_at']);
                $tmp_data['name']=$target['name'];
                $tmp_data['email']=$target['email'];
                $tmp_data['created_at']=$target['created_at'];
                $tmp_data['date_difference']=$date_difference;


            }
            $data[]=$tmp_data;

        }


        $data2=$data;
        $data3=$myArray->arrayOrderBy($data2,'date_difference desc');
        $data3['headers']=['Name','Email','Created at','Difference in Days'];


        $cliMenu=new CliMenu($this->cli);
        return $cliMenu->output_to_table($data3);
        //$this->properties_level1();

       // foreach ($this->notifications as )
    }



    private function properties_level1(){
     /*   $customer=new Customer();
        $customers=$customer->customers_without_properties(null,null);


        dd($customers,$this->notifications);

*/

    }


}
