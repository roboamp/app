<?php

namespace App\Console\Commands;

use App\MyClasses\Cli\CliCommon;
use App\MyClasses\Dates;
use App\MyClasses\Paths;
use App\MyClasses\URL;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\MyClasses\Files;
use App\MyClasses\CliFormat;
use Laravel\Dusk\Console\DuskCommand;
use App\Property;
use Tests\Browser\ExampleTest;
use Carbon\Carbon;

class Pinger extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:Pinger';

    protected $url;

    private $properties;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $cli;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()

    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        $pending_pings=$this->get_pending_pings();
        foreach ($pending_pings as $item) {
            echo "\n".$item->url."\n";
        }




    }
    public function get_pending_pings(){
        $now=Carbon::create();
        $res=Property::all()->filter(function($item)use ($now){

            $last_ping=Dates::mysql_date_to_carbon($item->last_ping);
            $tmp_buffer=new Carbon($last_ping);
            $next_ping=$tmp_buffer->addHours($item->pinger);
          if($now->greaterThan($next_ping)){
              return $item;
          }

        });
        return $res;

    }



}
