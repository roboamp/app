<?php

namespace App\Console\Commands;

use App\MyClasses\Paths;
use Illuminate\Console\Command;
use App\MyClasses\Files;
use App\MyClasses\Templates;

class ROBOAMP_make extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP_make:controller {--name=: Name of the Controller}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a custom Controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        $files=new Files();
        $template_path=Paths::path_to_template('controller','Controllers',false);
        $template_path_destination=base_path()."/app/Http/Controllers/delete_me.php";
        //get the replacements
        $class=ucfirst($this->option('name'));


        $placeholders=['--php','--class'];
        $replacements=['<?php',$class];

        //get the template
        $tmp_file=$files->copy_file($template_path,$template_path_destination);
        //make the changes

        $res=$files->replace_all_placeholders($placeholders,$replacements,$template_path_destination);

        //save the file




    }
}
