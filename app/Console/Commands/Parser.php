<?php

namespace App\Console\Commands;

use App\MyClasses\Cli\CliCommon;
use App\MyClasses\Paths;
use App\MyClasses\URL;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\MyClasses\Files;
use App\MyClasses\CliFormat;
use Laravel\Dusk\Console\DuskCommand;
use App\Property;
use Tests\Browser\ExampleTest;
use App\MyClasses\Directory;
use App\MyClasses\Templates;

class Parser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:Parser {url=""}';

    protected $url;

    private $properties;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $cli;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()

    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $template=new Templates();
        $url_class=new URL();

        $url=$this->ask('Enter an URL');

        //it's working for domains only not full urls
        //schema is not being contemplated yet
        $url_info=$url_class->check_if_domain_exist_in_db($url);
        $folder=$this->get_folder($url_info);

        Directory::create($folder);



        $placeholders=['{{URL}}','{{FOLDER}}'];
        $substitutions=[$url,$folder];

        $template->create_test_from_template('parser',$url_info,$placeholders,$substitutions);





        dd('done');


        \Artisan::call('dusk');


    }
    private function get_folder($url_info){
        $paths=new Paths();

        if(is_null($url_info->customer_id)){
            return $paths->path_to_folder('parser').'/'.$url_info->domain;
        }
        return $url_info->folder_name;
    }



}
