<?php

namespace App\Console\Commands;

use App\MyClasses\Paths;
use Illuminate\Console\Command;
use App\MyClasses\Templates;
use App\MyClasses\Files;

class admin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ROBOAMP:Admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an Endpoint for Admin Control Panel';
    private $controller="";
    private $controller_name="";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){

        $this->call('ROBOAMP_make:controller',['--name'=>"bobobo"]);

        dd("controller has been created");
        $template_content=$this->get_template_content();


        $this->update_route_file($template_content);



        $this->call('make:model',['name'=>ucfirst($this->controller)]);
        $this->call('ROBOAMP_make:controller',['--name'=>$this->controller_name]);



        dd('backup done');



    }



    private function update_route_file($template_content){

        $file=new Files();
        //anchor
        $arr_placeholders=["//begin-child-node"];

        //template_content is the new value (DB records)
        //adds the anchor again

        $subs=$template_content."\n".$arr_placeholders[0]."\n";
        $arr_subs=[$subs];

        //kernel
        $route_path=Paths::path_to_folder('routes')."/web.php";

        //$file->backup_file_with_timestamp($route_path);

        $file->replace_all_placeholders($arr_placeholders,$arr_subs,$route_path);

        //$file->delete_all_backups($route_path);

        echo "\n Routes File has been Updated\n";
    }


    private function get_template_content(){
        $file=new Files();

        $prefix=strtolower($this->ask("What's the prefix?"));
        $this->controller=strtolower($this->ask("What's the subgroup?"));

        $this->controller_name=ucfirst($prefix).ucfirst($this->controller)."Controller";

        $arr_placeholders=['--prefix','--subgroup','--controller'];
        $arr_subs=[$prefix,$this->controller,$this->controller_name];


        $tmp_path=$this->get_template_path($prefix);

        $template_path=$file->backup_file_with_timestamp($tmp_path);

        //hola.txt
        //--username=Roberto
        //--username=Viktor

        $arr_placeholders=['--username'];
        $arr_subs=['user'];
        $file->replace_all_placeholders($arr_placeholders,$arr_subs,'hola.txt');



        $file->replace_all_placeholders($arr_placeholders,$arr_subs,$template_path);

        $content=$file->get_file_content($template_path);
        $file->delete_all_backups($template_path);

        return $content;


    }

    private function get_template_path($prefix){

        $template_name=(is_null($prefix)?'admin_without_prefix':'admin');

        return Paths::path_to_template($template_name,'routes',false);

    }
}
