<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\UpdateFooter::class,
        Commands\ROBOAMP::class,
        Commands\Tools::class,
        Commands\Pinger::class,
        Commands\Parser::class,
        Commands\Migrate::class,
        Commands\Debug::class,
        Commands\Stripe::class,
        Commands\Demo::class,
        Commands\demo_prep::class,

        Commands\Components::class,
        Commands\Analyze::class,
        Commands\Make::class,
		Commands\Reports::class,
		Commands\Database::class,
		Commands\ENV::class,
        Commands\Emailer::class,

        Commands\Signature::class,
        Commands\Debug::class,
        Commands\IDs::class,
        Commands\American::class,
        Commands\Bubba::class,
        Commands\Admin::class,
        Commands\ROBOAMP_make::class



        //{{new_command}}
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('ROBOAMP:parse')->everyMinute();




$schedule->command("php artisan command:usageReportEmail --allCustomers")->monthly();








$schedule->command("php artisan command:consequuntur https://bode.com/provident-quia-ut-ipsum-et-id-soluta-aut-ut")->hourly();
$schedule->command("php artisan command:consequuntur https://bode.com/aut-aut-qui-porro-aut-voluptatem")->monthly();
$schedule->command("php artisan command:consequuntur https://bode.com/et-libero-quia-aut-rem")->everyTwoHours();

$schedule->command("php artisan command:quia https://graham.com/corrupti-dolor-odit-corporis-ad")->hourly();

$schedule->command("php artisan command:quia https://graham.com/nesciunt-necessitatibus-alias-eos")->everyFourHours();
//begin-child-node


    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
