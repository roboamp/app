<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Timeline;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Pivot_timeline_status extends Model
{
    use HasFactory;

    protected $table = 'status_timeline';
    protected $guarded = [];

//    public function Timeline()
//    {
//        return Timeline::where('id', '=', $this->timeline_id)->with('statuses_seeder')->first();
//    }

    public static function project_status($id)
    {
        if($id != 0) {
            $project_status = Pivot_timeline_status::where('id', $id)->get();
            $status = MasterStatus::where('id', $project_status[0]->status_id)->get();
            return $status[0]->name;
        }
    }
}
