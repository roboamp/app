<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageGenerated extends Model
{
    protected $guarded = [];
    use HasFactory;

    protected $table = 'page_generated';

    public function section(){
        return $this->belongsTo('App\PageSection');
    }

    public function property(){
        return $this->belongsTo('App\TmpProperty');
    }
}
