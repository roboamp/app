<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;

class MoveKnobFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $flag;
    protected $type;
    protected $report;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($flag, $type, $report)
    {
        $this->flag = $flag;
        $this->type = $type;
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $from = base_path("/tests/Browser/screenshots/knob.png");
        $to = public_path()."/knobs/images/";
        $imageName = 'knob-'.$this->type.'-'.$this->flag->slug.'-'.$this->report.'.png';

        File::copy($from, $to.$imageName);
        return $imageName;
    }
}
