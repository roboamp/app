<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Goutte\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpClient\HttpClient;
use App\Events\PotentialCustomerInfoUpdated;

use App\PotentialCustomer;
use App\PotentialCustomerEmail;
use App\PotentialCustomerPhone;

class GettingClientContactInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $url;

    protected $customer;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $customer)
    {
        $this->url = $url;

        $this->customer = $customer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();

        $crawler = $client->request('GET', $this->url);

        $html = $crawler->html();
        $email_regexp = "/(?:[a-z!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";
        preg_match_all($email_regexp, $html,  $emails_obtained);        
        $emails_obtained_flatten = Arr::flatten($emails_obtained);

        $phone_obtained = stristr($html, '"tel:');
        $phone = str_replace('">', '', substr($phone_obtained,5,14));
   
        $emails = array();
        $phones = array();
        

        foreach($emails_obtained_flatten as $email){
            if($email){
                $pcemail = PotentialCustomerEmail::where('email', $email)->first();
                if($pcemail){
                    $pcemail->potential_customer_id = $this->customer;
                }else{
                    $pcemail = PotentialCustomerEmail::create([
                        'email' => $email,
                        'potential_customer_id' => $this->customer
                    ]);
                }
                $emails[$pcemail->id] = $pcemail;
            }
            
        }

        if($phone){
            $pcphone = PotentialCustomerPhone::where('phone', $phone)->first();
            if($pcphone){
                $pcphone->potential_customer_id = $this->customer;
            }else{
                $pcphone = PotentialCustomerPhone::create([
                    'phone' => $phone,
                    'potential_customer_id' => $this->customer
                ]);
            }
            $phones[] = $pcphone;
        }
            

        $data['customer'] = $this->customer;
        $data['mails'] = $emails;
        $data['phones'] = $phones;
        $data['message'] = 'Contact Info Obtained from '.$this->url;

        PotentialCustomerInfoUpdated::dispatch($data);;
    }
}
