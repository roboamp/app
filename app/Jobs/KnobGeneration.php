<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use App\MyClasses\Templates;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Jobs\MoveKnobFile;

class KnobGeneration implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $flag;
    protected $type;
    protected $report;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $flag, $type, $report)
    {
        $this->url = $url;
        $this->flag = $flag;
        $this->type = $type;
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template=new Templates();
        $url=new \stdClass();
        $url->full_url='https://roboamp.com';
        $url_str=$this->url;
        $arr_placeholders = ["--url"];
        $arr_subs=[$url_str];
        $template->create_test_from_template('dusk_screenshots',$url,$arr_placeholders,$arr_subs);

        Artisan::call('dusk --filter=ExampleTest');

        //MoveKnobFile::dispatchNow($this->flag, $this->type, $this->report);
    }
}
