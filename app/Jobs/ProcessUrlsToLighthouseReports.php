<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\LighthouseReport;
use App\Jobs\ExecuteLighthouseTest;

class ProcessUrlsToLighthouseReports implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $urls;

    protected $category;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($urls, $category)
    {
        $this->urls = $urls;
        $this->category = $category;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->urls as $url) {
            $report = LighthouseReport::firstWhere('url', $url);
            if(!$report){
                //$result = $this->generate_report($url, $request);
                $this->store_report_to_database($url);
            }
        }
    }

    Public function store_report_to_database($url){   // Check is user logged in
        $type = 'User';
        $report= LighthouseReport::create([
            'url' => $url,
            'lighthouse_category_id' => $this->category,
            'type' => $type,
            'added_on' => now()
        ]);

        ExecuteLighthouseTest::dispatch($report);
    }
}
