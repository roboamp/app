<?php

namespace App\Jobs;

use App\Events\LighthouseReportUpdated;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Auth;
use Illuminate\Support\Facades\Http;
use App\LighthouseReport;
use App\LighthouseFlag;
use App\LighthouseFlagReport;
use App\LighthouseStatus;
use App\MyClasses\URL;
use Illuminate\Support\Facades\Log;

class ExecuteLighthouseTest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $report;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($report)
    {
        $this->report = $report;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $report = LighthouseReport::find($this->report->id);

        $report_normal = $this->execute_lighthouse_test($this->report->url);
        $report_amp = $this->execute_lighthouse_test('https://mercury.postlight.com/amp?url='.$this->report->url);

        if(is_array($report_amp) && is_array($report_normal)){
            $this->report->audits = $report_amp;
            $this->report->save();
            $result =  $this->compare_reports($report_normal, $report_amp, $this->report);
            $data['success'] = true;
            $data['message'] = 'Info updated for '.$this->report->url;
        }else{
            $this->report->lighthouse_status_id = 5;
            $this->report->save();
            $data['success'] = false;
            $data['message'] = 'An error ocurred while executing test. Please contact Support team to solve the issue.';
        }

        $status = LighthouseStatus::find($this->report->lighthouse_status_id);

        $data['report'] = $report;
        $data['status'] = $status;
        
        LighthouseReportUpdated::dispatch($data);
    }

    public function execute_lighthouse_test($url){

        $test = Http::post('https://lighthouse-dot-webdotdevsite.appspot.com//lh/newaudit', [
            'url' => $url,
            'replace' => true,
            'save' => false
        ]);

        if($test->successful()){
            
            $response = $test->json();
        
            return $response["lhr"]["audits"];
        }else{
            return false;
        }

    }


    public function compare_reports($report_normal, $report_amp, $report){

        $flags = LighthouseFlag::all();
        foreach($flags as $flag){
            if($report_amp[$flag->slug]['score'] > $report_normal[$flag->slug]['score'] ){
                LighthouseFlagReport::create([
                    'lighthouse_report_id' => $report->id,
                    'lighthouse_flag_id' => $flag->id,
                    'score' => $report_amp[$flag->slug]['score'],
                    'score_normal' => $report_normal[$flag->slug]['score']
                ]);
            }

        }

        $report->lighthouse_status_id = 2;
        $report->save();

        //$this->preparing_knobs_images($report);

        return $report;

    }

    /*public function preparing_knobs_images($report){
        $report = LighthouseReport::firstWhere('id', $report->id);

        foreach($report->flags as $flag){
            //generating With ROBOAMP knob charts
            $this->generate_knob($flag, 'amp', $flag->pivot->lighthouse_report_id, $flag->pivot->score);
            //generating Without ROBOAMP knob charts
            $this->generate_knob($flag, 'normal', $flag->pivot->lighthouse_report_id, $flag->pivot->score_normal);
        }
    }

    public function generate_knob($flag, $type, $report, $score){
        $root = URL::to('/');
        $color = ($type == 'amp') ?  '01c0c8' : 'f36e92'; 
        $url = $root.'/knob/'.$color.'/1.0/0/0.01/200/'.$score;
        KnobGeneration::dispatch($url, $flag, $type, $report);
        MoveKnobFile::dispatch($flag, $type, $report);
    }*/
}
