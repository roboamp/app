<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\LighthouseReport;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendReport;
use App\MyClasses\URL;
use Illuminate\Support\Facades\Log;

class SendLighthouseReportEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $report;
    protected $email;
    protected $status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($report, $email, $status)
    {
        $this->report = $report;
        $this->email = $email;
        $this->status = $status;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = asset('');
        $report = LighthouseReport::firstWhere('id', $this->report);
        $report->lighthouse_status_id = $this->status;
        $report->save();

        $url_status = URL::to('admin/speedy/status/change/'.$report->id);

        Mail::to($this->email)->send(new SendReport($report, $url_status, $asset));
    }
}
