<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageSection extends Model
{
    protected $guarded = [];
    use HasFactory;

    public function property(){
        return $this->belongsTo('App\TmpProperty');
    }

    public function pages(){
        return $this->hasMany('App\TmpPage', 'section_id');
    }

    public function pages_generated(){
        return $this->hasMany('App\PageGenerated', 'section_id');
    }
}
