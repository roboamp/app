<?php
/**
 * Created by PhpStorm.
 * User: taytus
 * Date: 5/16/18
 * Time: 9:01 AM
 */

namespace App\MyClasses;


use ROBOAMP\myarray as Robo_Array;

class MyArray extends Robo_Array
{

    public function search_for_string_and_remove($string, $array){

        $res = $this->check_for_string_in_array($string, $array, true);
        if (!is_null($res)) {
            $array = MyArray::unset_x_position_and_reset_index($res, $array);
        }
        return $array;
    }

    public static function remove_empty_string_elements($array){
        return array_filter($array,'strlen');
    }
    public static function recursively_remove_empty_string_elements($array){
        foreach ($array as $item){
            if(is_array($item))$new_array[]=self::remove_empty_string_elements($item);
        }
        return $new_array;

    }
    public static function remove_prefix(array $array, string $prefix=""){

        $new_arr = array();
        foreach($array as $key => $value) {

            $newkey = explode($prefix, $key);

            if(count($newkey)>=2){
                $new_arr[$newkey[1]] = $value ;
            }else{
                $new_arr[$key] = $value ;
            }

        }
         return $new_arr;
    }

    public static function array_element_in_other_array($needle,$hay){
        //dd($needle,$hay);
        return (count(array_intersect($needle, $hay))) ? true : false;
    }

//one dimension arrays only
    public function remove_tabs_from_array($array){
        foreach ($array as $item){
            $item=trim(preg_replace('/\t+/', '', $item));

        }
        return $array;
    }
    public function check_for_string_in_array($string,$array,$boolean=false){
        $i=0;
        foreach ($array as $element) {

            if ($string=== $element)  {

                $this->cursor=$i;
                if($boolean)return $i;
                return true;
            }
            $i++;
        }

        return null;
    }

    private static function insert_items_from_array($model,$item){
        $class =  ucfirst($model);
        $model = new $class();
        foreach ($item as $obj => $val) {
            $model->$obj = $val;
        }
        $model->save();
    }



}

