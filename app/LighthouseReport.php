<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class LighthouseReport extends Model
{
    use Uuid;
    use HasFactory;
    protected $keyType = 'string';
    public $incrementing = false;

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\LighthouseCategory', 'lighthouse_category_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo('App\LighthouseStatus', 'lighthouse_status_id', 'id');
    }

    public function flags()
    {
        return $this->belongsToMany('App\LighthouseFlag', 'lighthouse_flag_reports',  'lighthouse_report_id', 'lighthouse_flag_id')->withPivot('id', 'lighthouse_report_id', 'score', 'score_normal');
    }
}
