<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LighthouseCategory extends Model
{
    protected $guarded = [];
    public function reports()
    {
        return $this->hasMany('App\LighthouseReport');
    }
}
