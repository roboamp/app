<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DemoPage extends Model{

    public $fillable=['name'];

    public function demo(){
        return $this->belongsTo('App\Demo','id','demo_id');
    }
    public function status(){
        return $this->hasOne('App\DemoStatus','id','status_id');
    }
    public function getEtaAttribute($value){

       // dd($this->status_id);
        //if($value < Carbon::now()) return false;
        return "it will be ready in ".Carbon::create($value)->diffForHumans(Carbon::now(),true);


    }
}
