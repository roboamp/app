<?php

namespace App;

use App\MyClasses\MyArray;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Pivot_timeline_status;


class Timeline extends Model
{
    use HasFactory;

    protected $table = 'timelines';
    protected $guarded = [];

    public function statuses() {
        return $this->belongsToMany(MasterStatus::class, 'status_timeline', 'timeline_id', 'status_id')
            ->withTimestamps()
            ->withPivot(['description', 'timeline_id', 'id'])->get();
    }

    public function statuses_seeder() {
        return $this->belongsToMany(MasterStatus::class, 'status_timeline', 'timeline_id', 'status_id')
            ->withTimestamps()
            ->withPivot(['description', 'timeline_id', 'id']);
    }

    // test push
}
