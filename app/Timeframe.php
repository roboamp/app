<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timeframe extends Model
{
    protected $guarded = [];

    public function tasks(){
        return $this->hasMany('App\Task');
    }

    public function pullSchedule(){
        return $this->hasOne('App\PagePullSchedule', 'timeframe_id');
    }
}
