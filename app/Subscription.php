<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public function plan(){
        return $this->hasOne('App\Plan');
    }
    public function user(){
        return $this->hasOne('App\User');
    }
    public function property(){
        return $this->hasOne('App\Property');
    }
}
