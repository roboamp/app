<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;


class Template extends Model{

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->id = (string) Uuid::generate(4);
        });
    }
    protected $primaryKey='id';
    protected $table = "templates";
    public $incrementing = false;
    protected $fillable=["property_id","name","signature","url",'selector_id'];


    public function ampscripts(){
        return $this->belongsToMany('App\Ampscript', 'ampscript_template');
    }



}

