<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model{

    protected $table = "notify";

    public function notifications_type(){
        $this->hasOne('App\NotificationsType');
    }

}
