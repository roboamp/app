<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model{

    protected $with = ['pages'];

    public function status(){
        return $this->hasOne('App\DemoStatus','id','status_id');
    }

    public function pages(){
        return $this->hasMany('App\DemoPage');
    }



}
