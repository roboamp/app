<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{

    protected $table = "report_amp_errors";

    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
    public function created_at_for_humans(){
        $date=new \Carbon\Carbon($this->created_at);
        return $date->toDayDateTimeString();
    }
}
