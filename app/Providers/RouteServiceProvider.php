<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */

    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });

        $this->routes(function () {
            Route::middleware('web')
                ->namespace('App\Http\Controllers')
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60);
        });
    }
    public function map(Router $router){
        /* Usage Report Routes */
        $this->mapUsageReport($router);
        /* AmpGenerator Routes */
        $this->mapAmpGenerator($router);
        /* Engagement Calculator Routes */
        $this->mapEngagementCalculator($router);
        /* Pull Schedule Routes */
        $this->mapPullSchedule($router);
        /* Speedy (Lighthouse Reports) Routes */
        $this->mapSpeedy($router);
        /* Timeline Routes */
        $this->mapTimelineRoutes($router);
        /* FAQs Routes */
        $this->mapFAQRoutes($router);

        $this->mapPreviewRoutes($router);

        $this->mapWebRoutes($router);

        $this->mapApiRoutes($router);

        //custom route groups

        //$this->mapDemoRoutes($router);
    }


    protected function mapDemoRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/demo.php');
        });
    }

    protected function mapPreviewRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/preview.php');
        });
    }

    protected function mapFAQRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/features/FAQ.php');
        });

    }
    
    protected function mapTimelineRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/features/Timeline.php');
        });
    }

    protected function mapSpeedy(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/features/Speedy.php');
        });
    }

    protected function mapPullSchedule(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/features/PullSchedule.php');
        });

    }

    protected function mapEngagementCalculator(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/features/EngagementCalculator.php');
        });

    }

    protected function mapAmpGenerator(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/features/AmpGenerator.php');
        });

    }

    protected function mapUsageReport(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => ['web'],
        ], function ($router) {
            require base_path('routes/features/UsageReport.php');
        });

    }

}
