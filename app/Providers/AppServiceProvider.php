<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;


use Illuminate\Support\Facades\Blade;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::directive('rand',function(){
            $str="<?php";
            $str.=" echo rand(1,800);?>";
            //dd($str);
            return $str;
        });


        Blade::directive('center_form',function($params){
            $str= "<?php";
            $str.=" if(isset({$params}) && {$params}==true){";
            $str.=" \$class='col-6 offset-lg-3';";
            $str.=" }else{\$class='col-12';};";

            $str.="  echo \"<div class='{\$class}'>\" ?>";
            return $str;


        });
        Blade::directive('endcenter_form',function(){
            return  '</div><!-- TAYTUS-->';
        });

        Paginator::useBootstrap();
    }
}
