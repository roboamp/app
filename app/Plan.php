<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public function product(){
        return $this->hasOne('App\Product','stripe_id','product_stripe_id');
    }

    public function get_plan_id_from_stripeID($stripeID){
        return self::where('stripe_id',$stripeID)->pluck('id')->first();
    }
    public function get_active_plans(){
        return $this::where('active','=',1)
            ->where('coupon_id','=',null)->get();
    }

    public function get_active_plans_and_coupon_X($coupon){

        if(is_null($coupon)) return $this->get_active_plans();

        return $this::where('active','=',1)
            ->whereNull('coupon_id')
            ->orWhere('coupon_id','=',$coupon)
            ->get();
    }
}
