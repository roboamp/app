<?php
/**
 * Created by PhpStorm.
 * User: taytus
 * Date: 2/10/20
 * Time: 3:35 PM
 */

namespace App\Traits;

trait Uuids
{
    /**
     *
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->{$model->uuid} =
                str_replace('-', '', \Uuid::generate(4)->string);
        });
    }
}