<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DevContractor extends Model
{
    use HasFactory;

    protected $table = 'dev_contractors';
    protected $guarded = [];


    public function dev_projects() {
        return $this->hasMany(DevProject::class, 'dev_contractor_id');
    }
}
