var room = 1;

function education_fields() {

    room++;
    var objTo = document.getElementById('code_sections');
    var divtest = document.createElement("div");
    var sel_options=document.getElementById('txth_selector').value;
    var sel_options_type=document.getElementById('txth_content').value;

    divtest.setAttribute("class", "form-group removeclass" + room);
    var rdiv = 'removeclass' + room;
    divtest.innerHTML = '<div class="row">' +
        '<div class="col-sm-3 nopadding">' +
        '<div class="form-group">' +
        '<select class="custom-select"  id="dd_code_block_selector_type" name="dd_code_block_selector_type[]">'+sel_options+'</select></div>' +
        '</div>' +
        '' +
        '<div class="col-sm-3 nopadding">' +
        '<div class="form-group">' +
        '<select class="custom-select"  id="dd_content_type" name="dd_content_type[]">'+sel_options_type+'</select></div>' +
        '</div>' +
        '' +
        '<div class="col-sm-3 nopadding"><div class="form-group">' +
        '<input type="text" class="form-control" id="txt_block_class" name="txt_block_class[]" value="" placeholder="Block Class"></div></div><div class="col-sm-3 nopadding"><div class="form-group"><div class="input-group">' +
        '<input type="text" class="form-control" id="txt_block_value" name="txt_block_value[]" value="" placeholder="Block Value"><div class="input-group-append"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <i class="fa fa-minus"></i> </button></div></div></div></div><div class="clear"></div></row>';

    objTo.appendChild(divtest)
}

function remove_education_fields(rid) {
    $('.removeclass' + rid).remove();
}