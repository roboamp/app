var room = 1;

function education_fields() {

    room++;
    var objTo = document.getElementById('multiple_timeline_statuses');
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "col-sm-12 form-group removeclass" + room);
    var rdiv = 'removeclass' + room;
    divtest.innerHTML = '<div class="row m-3">\n' +
        '            <div class="col-sm-6 nopadding">\n' +
        '                <div class="input-group">\n' +
        '                    <input type="text" class="form-control" id="Degree"\n' +
        '                           id="page_name"\n' +
        '                           name="page_name[]"\n' +
        '                           placeholder="name" required>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            <div class="col-sm-6 nopadding">\n' +
        '                <div class="input-group">\n' +
        '                    <input type="text" class="form-control" id="Degree"\n' +
        '                           id="page_url"\n' +
        '                           name="page_url[]"\n' +
        '                           placeholder="URL" required>\n' +
        '                    <div class="input-group-append">\n' +
        '                        <button class="btn btn-danger" type="button"\n' +
        '                                onclick="remove_education_fields(' + room + ');">\n' +
        '                            <i class="fa fa-minus"></i>\n' +
        '                        </button>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '            <div class="clear"></div>\n' +
        '            </row>';

    objTo.appendChild(divtest)
}

function remove_education_fields(rid) {
    $('.removeclass' + rid).remove();
}