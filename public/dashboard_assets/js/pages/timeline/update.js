var room = 1;

function education_fields() {

    room++;
    var objTo = document.getElementById('multiple_timeline_statuses');
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "col-sm-12 form-group removeclass" + room);
    var rdiv = 'removeclass' + room;
    divtest.innerHTML = '<div class="row m-3">\n' +
        '        <div class="col-sm-6 nopadding">\n' +
        '            <div class="form-group">\n' +
        '                <div class="input-group"><select class="form-control" id="dd_status_id" name="dd_status_id_add[]">\n' +
        '                        <option value="1">created</option>\n' +
        '                        <option value="2">assigned</option>\n' +
        '                        <option value="3">qa</option>\n' +
        '                        <option value="4">delivered</option>\n' +
        '                        <option value="5">closed</option>\n' +
        '                    </select>\n' +
        '\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="col-sm-6 nopadding">\n' +
        '            <div class="input-group">\n' +
        '                <input type="text" class="form-control" id="dd_status_description" name="dd_status_description_add[]"\n' +
        '                       placeholder="Description" required>\n' +
        '                <div class="input-group-append">\n' +
        '                    <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"><i\n' +
        '                                class="fa fa-minus"></i></button>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="clear"></div>\n' +
        '        </row>';

    objTo.appendChild(divtest)
}

function remove_education_fields(rid) {
    $('.removeclass' + rid).remove();
}
