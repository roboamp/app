/*global jQuery, window, document*/
jQuery(document).ready(function ($) {
    'use strict';

    /*-------------------------------------
    Screenshot carousel
    -------------------------------------*/
    if ($('#screenshot-carousel-1').length > 0) {
        $('#screenshot-carousel-1').owlCarousel({
            items: 3,
            itemsDesktop: [1199, 2],
            itemsDesktopSmall: [991, 1],
            itemsTablet: [767, 1],
            itemsMobile: [479, 1],
            slideSpeed: 200,
            navigation: true,
            navigationText: ['<i class=\"fa fa-angle-left\"></i>', '<i class=\"fa fa-angle-right\"></i>'],
            pagination: false
        });
    }
    
    /*-----------------------------------------
    testimonial carousel 1
    -----------------------------------------*/
    if ($('#testimonial-carousel').length > 0) {
        $('#testimonial-carousel').owlCarousel({
            singleItem:true,
            slideSpeed: 200,
            autoPlay: 3000,
            stopOnHover: true,
            navigation: false,
            pagination: true
        });
    }


    /*-----------------------------------
       Contact Form
       -----------------------------------*/
    // Function for email address validation
    function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

        return pattern.test(emailAddress);

    }

    $("#contactForm").on('submit', function (e) {
        e.preventDefault();
        var data = {
            name: $("#name").val(),
            email: $("#email").val(),
            message: $("#message").val()
        };

        console.log(data);

        if (isValidEmail(data['email']) && (data['message'].length > 1) && (data['name'].length > 1)) {
            $.ajax({
                type: "POST",
                url: "/contact_email",
                data: $('#contactForm').serialize(),
                success: function () {
                    $('#contactForm')[0].reset();
                    $('#contactForm .input-success').delay(500).fadeIn(1000).delay(3000).fadeOut();
                    $('#contactForm .input-error').fadeOut(500);
                }
            });
        } else {
            $('#contactForm .input-error').delay(500).fadeIn(1000);
            $('#contactForm .input-success').fadeOut(500);
        }



        return false;
    });

    /*-----------------------------------
       Demo Form
       -----------------------------------*/
    // Function for email address validation
    
    $("#demoForm").on('submit', function (e) {
        e.preventDefault();
        var data = {
            email: $("#demoemail").val(),
        };

        console.log(data);

        if (isValidEmail(data['email'])) {
            $.ajax({
                type: "POST",
                url: "/demo_email",
                data: $('#demoForm').serialize(),
                success: function () {
                    $('#demoForm')[0].reset();
                    $('#demoForm .input-success').delay(500).fadeIn(1000).delay(3000).fadeOut();
                    $('#demoForm .input-error').fadeOut(500);
                }
            });
        } else {
            $('#demoForm .input-error').delay(500).fadeIn(1000);
            $('#demoForm .input-success').fadeOut(500);
        }



        return false;
    });

    /*-----------------------------------
       Subscribe Form
       -----------------------------------*/
    // Function for email address validation
    
    $("#subscriptionForm").on('submit', function (e) {
        e.preventDefault();
        var data = {
            email: $("#subscription_email").val(),
        };

        console.log(data);

        if (isValidEmail(data['email'])) {
            $.ajax({
                type: "POST",
                url: "/subscription_email",
                data: $('#subscriptionForm').serialize(),
                success: function () {
                    $('#subscriptionForm')[0].reset();
                    $('#subscriptionForm .input-success').delay(500).fadeIn(1000).delay(3000).fadeOut();
                    $('#subscriptionForm .input-error').fadeOut(500);
                }
            });
        } else {
            $('#subscriptionForm .input-error').delay(500).fadeIn(1000);
            $('#subscriptionForm .input-success').fadeOut(500);
        }


        return false;
    });

});
