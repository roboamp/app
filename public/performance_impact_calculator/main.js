
function sendRequest() {

    $average_monthly_visitors = $("#average_monthly_visitors").val();
    $conversion_rate = $("#conversion_rate").val();
    $average_order_value = $("#average_order_value").val();
    $loading_time_speed = $("#loading_time_speed").val();
    $potential_time_speed = $("#potential_time_speed").val();

    $.ajax({
        url: "/admin/pic/store",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method: 'POST',
        data: {
            average_monthly_visitors: $average_monthly_visitors,
            conversion_rate: $conversion_rate,
            average_order_value: $average_order_value,
            loading_time_speed: $loading_time_speed,
            potential_time_speed: $potential_time_speed
        }
    })
        .done(function (data) {
            result = Number(data.result).toLocaleString('en')
            $(".result span").text(result);
        })
        .fail(function (data) {
            $(".result span").text('failed')
        });
}


$("#average_monthly_visitors").ionRangeSlider({

    skin: "big",
    prettify_enabled: true,
    prettify_separator: ",",
    min: 50000,
    max: 500000,
    step: 50000,
    grid: false,
    onChange: function (data) {
        sendRequest();
    }
});

$("#conversion_rate").ionRangeSlider({

    skin: "big",
    min: 5,
    max: 30,
    grid: false,
    step: 5,
    postfix: "%",
    onChange: function (data) {
        sendRequest();
    }

});

$("#average_order_value").ionRangeSlider({

    skin: "big",
    min: 50,
    max: 200,
    grid: false,
    step: 50,
    prefix: "$",
    onChange: function (data) {
        sendRequest();
    }

});

$("#loading_time_speed").ionRangeSlider({

    skin: "big",
    min: 2,
    max: 8,
    grid: false,
    onChange: function (data) {
        sendRequest();
        range_obj = $("#potential_time_speed");
        range = range_obj.data("ionRangeSlider");
        var o = {
            max: $loading_time_speed,
            from: $loading_time_speed /2
        };
        range.update(o);
    }
});

$loading_time_speed = $("#loading_time_speed").val();

$("#potential_time_speed").ionRangeSlider({

    skin: "big",
    min: 0.6,
    max: $loading_time_speed,
    from: 2,
    grid: false,
    step: 0.1,
    onChange: function (data) {
        sendRequest();
    }
});


// var ctx = document.getElementById("myChart").getContext('2d');
//
// var myChart = new Chart(ctx, {
//     type: 'line',
//     data: {
//         labels: ["Tokyo",	"Mumbai",	"Mexico City",	"Shanghai",	"Sao Paulo",	"New York",	"Karachi","Buenos Aires",	"Delhi","Moscow"],
//         datasets: [{
//             label: 'Series 1', // Name the series
//             data: [0,	50,	2424,	14040,	14141,	4111,	4544,	47,	5555, 6811], // Specify the data values array
//             fill: false,
//             borderColor: '#2196f3', // Add custom color border (Line)
//             backgroundColor: '#2196f3', // Add custom color background (Points and Fill)
//             borderWidth: 1 // Specify bar border width
//         }]},
//     options: {
//         responsive: true, // Instruct chart js to respond nicely.
//         maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height
//     }
// });

