
// ************** Compatibility Check *******************
function svgMissing() 
{
    // SVG test taken from Modernizr 2.0
    if (!!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect)
    {
      return false;
    }
    else
    {
      return true;
    }        
}

if (svgMissing()) { window.location = 'incompatible.html';}

// ************** Initialize *******************

// Global Variables
var fixedVar = "payment";
var periods = 12; //26.089
var canadian = false;
var maxInt = 40; // maximum interest rate of 40%
var principalData = {outputID:"principal", minx:50000,maxx:500000,incr:50000,position:52,value:"50,000"}
var paymentData = {outputID:"payment", minx:100,maxx:10000,incr:100,position:31,value:"$1505.51"}
var intRateData = {outputID:"intRate", minx:0.05,maxx:0.30,incr:0.05,position:44,value:"5%"}
var termData = {outputID:"term", minx:50,maxx:200,incr:50,position:31,value:"$50"}

// Number Formatting
var fmtCommaNum = d3.format("0,f");
var fmtCommaNum2 = d3.format("0,.2f");
var fmtGeneralNum = d3.format("g");
var fmtPercent = d3.format("0.2%");



window.onresize = updateWindow;

// Set click and change events
d3.select("#linkPay").on("click",switchToPayment);
d3.select("#linkPrin").on("click",switchToPrincipal);
d3.select("#linkTerm").on("click",switchToTerm);

d3.select("#principal").on("change",inputChange);
d3.select("#payment").on("change",inputChange);
d3.select("#intRate").on("change",inputChange);
d3.select("#term").on("change",inputChange);

d3.select("#ctrlPayFreq").on("click",toggleFreq);
d3.select("#displayPayFreq").on("click",toggleFreq);
d3.select("#canTogLink").on("click",toggleCanadian);

d3.select("#principalSlide")
  .data([principalData])
  .on("touchmove",SlideTouch)
  .on("touchstart",SlideTouch)
  .on("mouseover",SlideOver)
  .on("mousemove",SlideMove)
  .on("mouseout",SlideOut)
  .on("click",SlideClick)
  .selectAll("img").style("left",principalData.position + "px");

d3.select("#paymentSlide")
  .data([paymentData])
  .on("touchmove",SlideTouch)
  .on("touchstart",SlideTouch)
  .on("mouseover",SlideOver)
  .on("mousemove",SlideMove)
  .on("mouseout",SlideOut)
  .on("click",SlideClick)
  .selectAll("img").style("left",paymentData.position + "px");

d3.select("#intRateSlide")
  .data([intRateData])
  .on("touchmove",SlideTouch)
  .on("touchstart",SlideTouch)
  .on("mouseover",SlideOver)
  .on("mousemove",SlideMove)
  .on("mouseout",SlideOut)
  .on("click",SlideClick)
  .selectAll("img").style("left",intRateData.position + "px");

d3.select("#termSlide")
  .data([termData])
  .on("touchmove",SlideTouch)
  .on("touchstart",SlideTouch)
  .on("mouseover",SlideOver)
  .on("mousemove",SlideMove)
  .on("mouseout",SlideOut)
  .on("click",SlideClick)
  .selectAll("img").style("left",termData.position + "px");




// Principal Chart
var princW = 400;
var princH = 250;
var princMarg = 1;
var princBMarg = 20;
var princTMarg = 40;


var princDataset = new Array(360);
princDataset[0]= 250000;
princDataset[1]= 0;
var princSvg = d3.select("#principalChart")
            .append("svg")
            .attr("id", "princBars")
            .attr("width", princW)   
            .attr("height", princH)
            .on("mousemove", princTooltip)
            .on("mouseout", tooltipRemove); 

        
var princXScale = d3.scale.linear()
             .domain([0, 30*12])
             .range([0 + princMarg, princW - princMarg*2]);

var princYScale = d3.scale.linear()
             .domain([0, d3.max(princDataset, function(d) { return d; })])
             .range([princH - princBMarg ,0+princTMarg])
             .nice()
             .clamp(true);
                
var princXAxis = d3.svg.axis()
                  .scale(princXScale)
                  .orient("bottom")
                  .tickValues([0,60,120,180,240,300,360])
                  .tickSubdivide(4)
                  .tickFormat(function(d,i){if(d==30*12){return "30_";} else if (d==15*12){return "15 YEARS";} else {return fmtGeneralNum(d/12);}});  

princSvg.append("g")
          .attr("class","axis")
          .attr("transform", "translate(0," + (princH - princBMarg) + ")")
          .call(princXAxis);
                    
var princBars = princSvg.append("g").selectAll("rect")
                .data(princDataset)
                .enter()
                .append("rect")
                .attr("class", function(d,i){if(i % 120==0){return "barOn";} else {return "";}});;
            
princSvg.append("g")
          .attr("id","svgText")
          .append("text")
            .attr("id", "val1");
d3.select("#svgText").append("text")
          .attr("id", "val2");
d3.select("#svgText").append("text")
          .attr("id", "val3");
d3.select("#svgText").append("text")
          .attr("id", "tooltip");

// var totalSvg = d3.select("#totalChart")
//         .append("svg")
//         .attr("id","totalStacked")
//         .attr("width", princW)  
//         .attr("height", 50);  
d3.select("#principalBar").style("width","40%");
d3.select("#interestBar").style("width","60%");
  





// Initialize to calculate payments

switchToPayment();
updateWindow();


// ************** Functions *******************
function calcAllPrincipal (principal, payment, intRate, term)
{

  var arr = [];
  
  if (periods == 12 )
  {
    for (var i=0; i<12*30; i++)
    {
      arr.push(principal*Math.pow(1+intRate/periods,i)-payment*((Math.pow(1+intRate/periods,i)-1)/(intRate/periods)));
    }
  }
  else
  {
   for (var i=0; i<26*30+1; i++)
    {
      //removes every 2nd, 13th and 293rd record so there are 12 periods per year even though records are calcuated at 26 and a bit per year.
      if ((i-1) % 13 != 0  && (i-1) % 2 != 0 && (i-1) != 259) 
      {
        arr.push(principal*Math.pow(1+intRate/periods,i)-payment*((Math.pow(1+intRate/periods,i)-1)/(intRate/periods)));
      }
      
    } 
  }

  return arr;
}

function calcPrincipal (principal, payment, intRate, term, i)
{
  return principal*Math.pow(1+intRate/periods,i)-payment*((Math.pow(1+intRate/periods,i)-1)/(intRate/periods));
}


// ************** Event Handlers ****************
function SlideOver(d){

  d.position = d3.select(this).select("img").style("left").replace("px","")-0;
  d.value = document.getElementById(d.outputID).value;
}

function SlideMove(d){

  var minx = d.minx; 
  var maxx = d.maxx;
  var incr = d.incr;

  var width = d3.select(this).style("width").replace("px","");
  var pos = d3.mouse(this)[0];
  d3.select(this).select(".slideBut").style("left",Math.min(width-10,Math.max(0,pos-5)) +"px");
  d3.select(this).select(".slideTip").style("left",Math.min(width-52,Math.max(0,pos-26)) +"px")
        .text("Click to Set");

  var value = Math.min(Math.max(minx,minx + Math.round((maxx-minx)*pos/width/incr)*incr),maxx);
  document.getElementById(d.outputID).value = value;
  displayResult();

}

function SlideClick(d){
  var pos = d3.mouse(this)[0];
  var width = d3.select(this).style("width").replace("px","");
  d.position = pos;
  d3.select(this).select(".slideTic").style("left",Math.min(width,Math.max(0,pos)) +"px");
  d.value = document.getElementById(d.outputID).value;
}

function SlideOut(d){
  var width = d3.select(this).style("width").replace("px","");
  d3.select(this).select(".slideBut").style("left",Math.min(width,Math.max(0,d.position-5)) +"px");
  d3.select(this).select(".slideTip").text("");
  document.getElementById(d.outputID).value = d.value;
  displayResult();
}

function SlideTouch(d){

  d3.event.preventDefault();
  var minx = d.minx; 
  var maxx = d.maxx;
  var incr = d.incr;

  var width = d3.select(this).style("width").replace("px","");
  var pos = d3.touches(this)[0][0];

  d3.select(this).select(".slideBut").style("left",Math.min(width-10,Math.max(0,pos-5)) +"px");
  d3.select(this).select(".slideTic").style("left",Math.min(width,Math.max(0,pos)) +"px");

  var value = Math.min(Math.max(minx,minx + Math.round((maxx-minx)*pos/width/incr)*incr),maxx);
  document.getElementById(d.outputID).value = value;
  displayResult();
  d.value = document.getElementById(d.outputID).value;
  d.position = pos;

}


function switchToPayment() {
  fixedVar = "payment";
  
  $("#ctrlPay").hide();
  $("#ctrlPrin").show();
  $("#ctrlTerm").show();

  $("#outPay").show();
  $("#outPrin").hide();
  $("#outTerm").hide();

  $("#titlePay").show();
  $("#titlePrin").hide();
  $("#titleTerm").hide();

  $("#linkPay").hide();
  $("#linkPrin").show();
  $("#linkTerm").show();

  $('#titlePrin').insertBefore('#titlePay');
  $('#titleTerm').insertBefore('#titlePay');

  inputChange("term");
  inputChange("principal");
}

function switchToPrincipal() {
  fixedVar = "principal";
  
  $("#ctrlPay").show();
  $("#ctrlPrin").hide();
  $("#ctrlTerm").show();

  $("#outPay").hide();
  $("#outPrin").show();
  $("#outTerm").hide();

  $("#titlePay").hide();
  $("#titlePrin").show();
  $("#titleTerm").hide();

  $("#linkPay").show();
  $("#linkPrin").hide();
  $("#linkTerm").show();

  $('#titlePay').insertBefore('#titlePrin');
  $('#titleTerm').insertBefore('#titlePrin');

  inputChange("payment");
  inputChange("term");
}

function switchToTerm() {
  fixedVar = "term";
  
  $("#ctrlPay").show();
  $("#ctrlPrin").show();
  $("#ctrlTerm").hide();

  $("#outPay").hide();
  $("#outPrin").hide();
  $("#outTerm").show();

  $("#titlePay").hide();
  $("#titlePrin").hide();
  $("#titleTerm").show();

  $("#linkPay").show();
  $("#linkPrin").show();
  $("#linkTerm").hide();

  $('#titlePay').insertBefore('#titleTerm');
  $('#titlePrin').insertBefore('#titleTerm');

  inputChange("payment");
  inputChange("principal");
}

function toggleFreq(){
  var val;
  if (periods == 12) {
    periods = 26.089;
    d3.select("#ctrlPayFreq").text("biweekly");
    d3.select("#displayPayFreq").text("biweekly");
    d3.select("#interestNote").text("Calculations assume biweekly compounding interest");
    val = fmtCommaNum2(parseFloat(document.getElementById("payment").value.replace(/,/g,"").replace(/\$/g,""))/2);
  }
  else {
    periods = 12;
    d3.select("#ctrlPayFreq").text("monthly");
    d3.select("#displayPayFreq").text("monthly");
    d3.select("#interestNote").text("Calculations assume monthly compounding interest");
    val = fmtCommaNum2(parseFloat(document.getElementById("payment").value.replace(/,/g,"").replace(/\$/g,""))*2);
  }
  paymentData.value = val;
  document.getElementById("payment").value = val;
  inputChange("payment");
}

function toggleCanadian(){
  if (canadian)
  {
    canadian = false;
    if (periods == 12) {d3.select("#interestNote").text("Calculations assume monthly compounding interest");}
    else {d3.select("#interestNote").text("Calculations assume biweekly compounding interest");}
    d3.select("#canOrNot").html(" if this is a Canadian fixed rate mortgage");
  }
  else
  {
    canadian = true;
    d3.select("#interestNote").text("Calculations assume biannual compounding interest");
    d3.select("#canOrNot").html(" if this is <strong>not</strong> a Canadian fixed rate mortgage");
  }

  displayResult();
}


function inputChange(inputId)
{

  // move slider
  var input = inputId || this.id;
  var slider = "#" + input + "Slide"
  var width = d3.select(slider).style("width").replace("px","");
  var max = d3.select(slider).data()[0].maxx;
  var min = d3.select(slider).data()[0].minx;
  var value = parseFloat(document.getElementById(input).value.replace(/,/g,"").replace(/\$/g,"").replace(/a-z/g,"").replace(/A-Z/g,""));
  if (input == "intRate" && value > maxInt) { alert("That interest rate is too high for us to accurately make calculations. Try something less than " + maxInt + " percent.");}

  if (isNaN(value) || (input == "intRate" && value > maxInt))
  {
    document.getElementById(input).value = d3.select(slider).data()[0].value;
  }
  else
  {
    if (input == "intRate" && value >=maxInt/100)
    {
      value = value/100;
      document.getElementById(input).value = value;
      d3.select(slider).data()[0].value = value;
    }
    var pos = Math.min(width,Math.max(0,(value - min)/(max-min)*width));
    d3.select(slider).select(".slideBut").style("left",Math.min(width-10,Math.max(0,pos-5)) +"px");
    d3.select(slider).select(".slideTic").style("left",Math.min(width,Math.max(0,pos)) +"px");
  }

  
  //update results
  displayResult();
}

function princTooltip (){

  var monthCnt = Math.round(princXScale.invert(d3.mouse(this)[0]));
  var val = princDataset[monthCnt];

  var tipText = "$" + fmtCommaNum(val);
  if (isNaN(val) || val < 0) {tipText = "";}

  d3.select("#tooltip")
    .style("display","block")
    .text("test")
    .attr("x", function() { if(monthCnt<20*12) {return d3.mouse(this)[0]+3;} else{ return d3.mouse(this)[0]-46;}})
    .attr("y", princYScale(0)-5)
    .text(tipText);

  princSvg.selectAll("rect").style("fill", "")
    .filter(function(d,i){ if (i == monthCnt) {return true;} else {return false;}})
    .style("fill", "#31859c");

}

function tooltipRemove(){
  d3.select("#tooltip").style("display","none");
  princSvg.selectAll("rect").style("fill", "");
}
 
function displayResult()
{
  
  // capture input values
  var principal = parseFloat(document.getElementById("principal").value.replace(/[^0-9.]/g,""));
  var payment = parseFloat(document.getElementById("payment").value.replace(/[^0-9.]/g,""));
  var intRate = parseFloat(document.getElementById("intRate").value.replace(/[^0-9.%]/g,""));
  if (document.getElementById("intRate").value.slice(-1) == "%") {intRate = intRate/100;}
  if (intRate > maxInt/100) { intRate = d3.select("#intRateSlide").data()[0].value.replace(/[^0-9.]/g,"")/100; }
  var term = parseFloat(document.getElementById("term").value.replace(/[^0-9.]/g,""));
  term.toFixed(2);

  //if any of above are not numbers for some reason set to slider position value
  if (isNaN(principal)){principal = d3.select("#principalSlide").data()[0].value.replace(/[^0-9.]/g,"")*1;}
  if (isNaN(payment)){payment = d3.select("#paymentSlide").data()[0].value.replace(/[^0-9.]/g,"")*1;}
  if (isNaN(intRate)){intRate = d3.select("#intRateSlide").data()[0].value.replace(/[^0-9.]/g,"")/100;}
  if (isNaN(term) || term==Infinity){term = d3.select("#termSlide").data()[0].value.replace(/[^0-9.]/g,"")*1;}

  var adjIntRate = intRate;
  if (canadian) { adjIntRate = (Math.pow(Math.pow(1+intRate/2,2),1/periods)-1)*periods;}


  if (fixedVar == "payment")
  {
    payment = Math.ceil((principal*adjIntRate)/(periods*(1-Math.pow((1+adjIntRate/periods),-(periods*term))))*100)/100;
  }
  else if (fixedVar == "principal")
  {
    principal = Math.floor((payment*(periods*(1-Math.pow((1+adjIntRate/periods),-(periods*term)))))/adjIntRate);
  }
  else
  {
    term = - (Math.log(1-(principal/payment)*(adjIntRate/periods)))/Math.log(1+(adjIntRate/periods))/periods
  }

  var intTotal = periods*term*payment-principal;


  
  //update inputs
  document.getElementById("principal").value = "$"+fmtCommaNum(principal);
  document.getElementById("payment").value = "$"+fmtCommaNum2(payment);
  document.getElementById("intRate").value = fmtPercent(intRate);
  document.getElementById("term").value = fmtGeneralNum(term.toFixed(2)) + " Years";
  //update outputs
  d3.select("#displayPrin").text("$"+fmtCommaNum(principal));
  d3.select("#displayPay").text("$"+fmtCommaNum2(payment));



  // regular case
  if(!isNaN(term) && term!=Infinity)
  {   
    var years = Math.floor(term);
    var months = Math.ceil((term-years)*12-0.1);
    if (months == 12) {years=years+1; months=0;}
    
    // number years
    d3.select("#displayYrs").text(fmtCommaNum(years));
    
    // "year" vs "years" text
    if (years==1){d3.select("#displayYr").text("YEAR");}
    else {d3.select("#displayYr").text("YEARS");}

    // number months
    if (months == 0) { d3.select("#displayMonth").text(""); }
    else if (months == 1) {  d3.select("#displayMonth").text("+ " + months + " MONTH"); }
    else { d3.select("#displayMonth").text("+ " + months + " MONTHS"); }

    // total principal + interest
    d3.select("#displayTotal").text("$"+fmtCommaNum(principal+intTotal));
    d3.select("#principalBar").style("width",principal / (principal+ intTotal)*100 + "%");
    d3.select("#principalText")
      .text(String.fromCharCode(160) + " $"+fmtCommaNum(principal) + " of Debt")
      .style("width","50%");
    d3.select("#interestBar").style("width",intTotal / (principal+ intTotal)*100 + "%");
    d3.select("#interestText")
      .text("Interest of $"+fmtCommaNum(intTotal) + String.fromCharCode(160))
      .style("text-align","right")
      .style("width","50%");
  }
  //payments are too low
  else 
  {
    d3.select("#displayYrs").text("");
    d3.select("#displayYr").text("NEVER");
    d3.select("#displayMonth").text("payments less than interest");
    d3.select("#displayTotal").text("payments are too low");
    d3.select("#principalBar").style("width","0%");
    d3.select("#principalText")
      .text("")
      .style("width","0%");
    d3.select("#interestBar").style("width","100%");
    d3.select("#interestText")
      .text("Infinite Interest")
      .style("width","100%")
      .style("text-align","center");
  }

  princDataset = calcAllPrincipal(principal, payment, adjIntRate, term);
  redraw();
}


function redraw(){
  
  var tTime = 2;
  
  princYScale
    .domain([0, d3.max(princDataset, function(d) { return d; })]);
  
  princXScale.range([0 + princMarg, princW - princMarg*2]);

  princSvg.attr("width", princW)
    .select("g.axis").call(princXAxis);;

  princBars
    .data(princDataset)
    .transition()
    .duration(tTime)
    .attr("x", function(d,i){return princXScale(i);}) 
    .attr("width", (princW - princMarg*2)/princDataset.length)
    .attr("y", function(d){if(isNaN(d)){return 0;} else {return princYScale(d);}})
    .attr("height", function(d){if(isNaN(d)){return 0;} else {return princYScale(0)-princYScale(d);}});           
   
  d3.select("#val1")
    .text(function () {if(princDataset[0] < 0) { return "";} else { return "$" +  fmtCommaNum(princDataset[0]);}})
    .attr("x",princXScale(0))
    .attr("y",princYScale(princDataset[0])-5);  
  d3.select("#val2")
    .text(function () {if(princDataset[120] < 0) { return "";} else { return "$" +  fmtCommaNum(princDataset[120]);}})
    .attr("x",princXScale(120))
    .attr("y",princYScale(princDataset[120])-5);
  d3.select("#val3")
    .text(function () {if(princDataset[240] < 0) { return "";} else { return "$" +  fmtCommaNum(princDataset[240]);}})
    .attr("x",princXScale(240))
    .attr("y",princYScale(princDataset[240])-5);
  
  
}


function updateWindow(){
    x = document.body.clientWidth || window.innerWidth;
    if (x < 840) {princW = 400;}
    else         {princW = 800;}

    redraw();
}