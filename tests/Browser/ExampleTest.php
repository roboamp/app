<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class KnobScreenshotTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {


        $this->browse(function (Browser $browser) {
            $browser->visit('http://localhost:8000/knob/f36e92/1.0/0/0.01/200/0.51')
                ->screenshot('knob');
        });



    }
}