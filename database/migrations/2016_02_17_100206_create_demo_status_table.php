<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\MyClasses\DB;

class CreateDemoStatusTable extends Migration{

    private $table='demo_status';

    public function up()
    {
        DB::create($this->table, function (Blueprint $table) {
            $table->id()->unsigned();
            $table->string('name',25);
            $table->string('class',25);
        });
    }

    public function down(){
        DB::drop($this->table);
    }
}
