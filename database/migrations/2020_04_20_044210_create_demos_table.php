<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\MyClasses\DB;

class CreateDemosTable extends Migration{

    private $table='demos';

    public function up(){
        DB::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('status_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamp('deliverable_at',0)->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('demo_status');

        });
    }


    public function down(){
        DB::drop($this->table);
    }
}
