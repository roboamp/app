<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\MyClasses\DB;

class CreateTableNotificationsType extends Migration
{

    private $table='notifications_type';
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        //this table keeps records of notifications sent to users via email
        Schema::create($this->table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->char('name',50);


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::drop($this->table);
    }
}
