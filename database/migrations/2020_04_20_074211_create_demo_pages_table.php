<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\MyClasses\DB;

class CreateDemoPagesTable extends Migration{

    private $table='demo_pages';

    public function up(){
        DB::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('local_url')->nullable();
            $table->string('testing_url');
            $table->bigInteger('demo_id')->unsigned();
            $table->biginteger('status_id')->unsigned();
            $table->timestamp('eta')->nullable();
            $table->timestamps();

            $table->foreign('demo_id')->references('id')->on('demos');

            $table->foreign('status_id')->references('id')->on('demo_status');

        });
    }


    public function down(){
        DB::drop($this->table);
    }
}
