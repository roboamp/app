<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\MyClasses\DB;

class CreateLighthouseReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::create('lighthouse_reports', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('url');
            $table->unsignedInteger('lighthouse_category_id')->nullable();
            $table->string('type')->nullable()->default('client');
            $table->unsignedInteger('lighthouse_status_id')->nullable()->default(1);
            $table->json('audits')->nullable();
            $table->timestamp('added_on')->nullable();
            $table->timestamp('readed_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::drop('lighthouse_reports');
    }
}