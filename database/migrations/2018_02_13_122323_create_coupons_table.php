<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->string('id')->unique()->primary();
            $table->integer('percent_off')->default(10);
            $table->string('duration',20)->nullable();
            $table->integer('max_redemptions')->default(1)->nullable();
            $table->timestamp('redeem_by');
            $table->integer('duration_in_months')->nullable();
            $table->integer('times_redeemed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
