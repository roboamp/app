<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\MyClasses\DB;

class CreatePivotTableStatusTimeline extends Migration
{

    private $table='status_timeline';

    public function up()
    {
        DB::create($this->table, function (Blueprint $table) {
            $table->id();
            $table->foreignId('status_id')->constrained()->onDelete('cascade');
            $table->foreignId('timeline_id')->constrained()->onDelete('cascade');
            $table->char('description');
            $table->timestamps();

            $table->unique(['status_id', 'timeline_id']);
        });
    }

    public function down()
    {
        DB::drop($this->table);
    }
}
