<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\MyClasses\DB;
class Pivots extends Migration{

    private $tables=['demo_status'];
    public function up(){
        DB::create($this->tables[0], function (Blueprint $table) {
            $table->bigInteger('demo_id');
            $table->bigInteger('status_id');
            $table->primary(['demo_id','status_id']);
        });
    }


    public function down(){
        foreach ($this->tables as $table){
            DB::drop($table);
        }
    }
}
