<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\MyClasses\DB;

class CreateGeneratedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $table="r_pages_generated";
    public function up(){

        if(!Schema::hasTable($this->table)){
            Schema::create($this->table, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->uuid('property_id');
                $table->string('url');
                $table->string('view_name')->nullable();
                $table->timestamps();
                $table->foreign('property_id')->references('id')->on('properties');

            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::drop($this->table);
    }
}
