<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $incrementing = false;
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name');
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
            $table->string('email')->nullable();
            $table->string('temp_referral')->nullable();
            $table->integer('referral_link_id')->unsigned()->nullable();
            $table->string('password', 60);
            $table->string('coupon_id')->nullable();
            $table->boolean('testing')->default(0);
            $table->integer('notify_id')->unsigned()->nullable();
            $table->date('latest_notification_date')->nullable();

            $table->rememberToken();



        });

        Schema::table('customers', function (Blueprint $table) {

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('coupon_id')->references('id')->on('coupons');
            $table->foreign('notify_id')->references('id')->on('notify');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
