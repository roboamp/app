<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\MyClasses\DB;
class CreateSelectorTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $table="selector_types";

    public function up(){

        DB::create($this->table, function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        DB::drop($this->table);
    }
}
