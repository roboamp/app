<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\MyClasses\DB;

class CreateLighthouseFlagReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::create('lighthouse_flag_reports', function (Blueprint $table) {
            $table->id();
            $table->uuid('lighthouse_report_id');
            $table->unsignedBigInteger('lighthouse_flag_id');
            $table->decimal('score', 4,2);
            $table->decimal('score_normal', 4,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::drop('lighthouse_flag_reports');
    }
}
