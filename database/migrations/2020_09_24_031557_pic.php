<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\MyClasses\DB;

class Pic extends Migration
{
    private $table="pic";

    public function up(){

        DB::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('average_monthly_visitor');
            $table->tinyInteger('conversion_rate');
            $table->smallInteger('average_order_value');
            $table->decimal('loading_time');
            $table->tinyInteger('starting_speed');
            $table->integer('total');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::drop($this->table);
    }
}
