<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\MyClasses\DB;


class CreateSlugFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    private $table="slug_filters";

    public function up(){
        Schema::create($this->table, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('property_id');
            $table->string('slug');
            $table->tinyInteger('position');

            $table->foreign('property_id')->references('id')->on('properties');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        DB::drop($this->table);
    }
}
