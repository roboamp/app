<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\MyClasses\DB;
class CreateComponentsTable extends Migration
{

    private $table_name='components';

    public function up()
    {
        DB::create($this->table_name, function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('name','40');
            $table->string('url');
            $table->integer('component_category_id');
            $table->timestamps();
            //$table->foreign('component_category_id')->references('id')->on('components_categories')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::drop($this->table_name);
    }
}
