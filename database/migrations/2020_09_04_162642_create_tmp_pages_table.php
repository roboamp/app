<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_pages', function (Blueprint $table) {
            $table->id();
            $table->string('property_id',36);
            $table->unsignedBigInteger('section_id')->nullable();
            $table->string('name',100);
            $table->string('label',100)->default('');
            $table->string('parent_id',36)->default(0)->nullable();
            $table->string('url')->default('');
            $table->integer('demo_old')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_pages');
    }
}
