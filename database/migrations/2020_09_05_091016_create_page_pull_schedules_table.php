<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagePullSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_pull_schedules', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('page_id');
            $table->boolean('active')->default(false);
            $table->unsignedBigInteger('activity_id')->default(1);
            $table->unsignedBigInteger('timeframe_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_pull_schedules');
    }
}
