<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\MyClasses\DB;

class CreateTableNotify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //this table keeps records of notifications sent to users via email
        Schema::create('notify', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->char('name',50);
            $table->string('copy');
            $table->char('interval',50);
            $table->integer('notifications_type_id')->unsigned();

            $table->timestamps();

        });
        Schema::table('notify', function(Blueprint $table)
        {
            $table->foreign('notifications_type_id')->references('id')->on('notifications_type');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::drop('notify');
    }
}
