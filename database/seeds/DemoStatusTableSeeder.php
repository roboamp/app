<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as MyDB;
use Illuminate\Support\Facades\DB;

class DemoStatusTableSeeder extends Seeder{

    public function run(){
        $table='demo_status';

        MyDB::truncate($table);

        $statuses=[
            ['name'=>'Active','class'=>'label label-danger'],
            ['name'=>'Pending','class'=>'label label-danger'],
            ['name'=>'Suspended','class'=>'label label-danger'],
            ['name'=>'Canceled','class'=>'label label-danger'],
            ['name'=>'Development','class'=>'label label-warning']
        ];

        MyDB::insert_items_from_array("App\DemoStatus",$statuses);
        MyDB::SET_FOREIGN_KEY(1);

    }
}
