<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
class PriceBucketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('price_buckets');

        DB::table('price_buckets')->insert([
            'name' => 'Bronze',
            'pricing' => 300,
            'max' => 10
        ]);

        DB::table('price_buckets')->insert([
            'name' => 'Silver',
            'pricing' => 600,
            'max' => 100
        ]);

        DB::table('price_buckets')->insert([
            'name' => 'Gold',
            'pricing' => 1000,
            'max' => 500
        ]);

        DB::table('price_buckets')->insert([
            'name' => 'Platinum',
            'pricing' => 2000,
            'max' => 1000
        ]);
    }
}
