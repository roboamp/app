<?php

use App\FAQ;
use Illuminate\Database\Seeder;

class FAQSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        FAQ::factory()->count(50)->create();
        FAQ::factory()->count(5)->create([
            'answer' => null,
            'privacy' => 'private',
            'category_id' => null
        ]);

    }
}
