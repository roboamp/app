<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\PageSection;

class PageSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('page_sections');
        PageSection::factory()->count(10)->create();
    }
}
