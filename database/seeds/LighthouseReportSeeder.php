<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\LighthouseReport;

class LighthouseReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('lighthouse_reports');
        LighthouseReport::factory()->count(10)->create();
    }
}
