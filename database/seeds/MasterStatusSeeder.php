<?php

use App\MasterStatus;
use Illuminate\Database\Seeder;
use App\MyClasses\DB;
class MasterStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::truncate('statuses');

        $arr_names=['created', 'assigned', 'qa', 'delivered', 'closed'];

        DB::insert_items_from_array("App\MasterStatus",$arr_names);
    }
}
