<?php

use customers\CustomerSeeder;
use Database\Seeders\DevContractorSeeder;
use Database\Seeders\DevPageSeeder;
use Database\Seeders\DevProjectSeeder;
use Illuminate\Database\Seeder;

use App\MyClasses\Seeders;

//use App\MyClasses\Render\RenderFooter;
use App\MyClasses\Server;

//use App\MyClasses\Stripe\RoboStripe;
use App\MyClasses\Stripe\RoboStripeCli;

class DatabaseSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $server = new Server();

        $create_stripe_data = false;
        $delete_stripe_data = false;

        // $robostripe=new RoboStripe();


        $seeder = new Seeders($create_stripe_data);

        $server_info = $server->server_info();

        if ($server_info['testing_server'] == true && $delete_stripe_data) {
            echo "Deleting Data from Stripe...\n";
            // $robostripe->delete_all_testing_data();

        }


        $seeder->call(SelectorTypesSeeder::class);

        $seeder->call(ProductsTableSeeder::class);
        $seeder->call(PlansTableSeeder::class);
        $seeder->call(UserRolesTableSeeder::class);
        $seeder->call(DemoStatusTableSeeder::class);
        $seeder->call(NotificationsTypeTableSeeder::class);
        $seeder->call(NotifyTableSeeder::class);


        //if($server->testing_server()){
        $seeder->call(UsersTableSeeder::class);

        $seeder->call(CustomersTableSeeder::class);

        // FAQ Seeder
        $seeder->call(CategorySeeder::class);

        $seeder->call(FAQSeeder::class);
        $seeder->call(SearchLogSeeder::class);

        // Timeline
        $seeder->call(MasterStatusSeeder::class);
        $seeder->call(TimelineSeeder::class);

        //QA Seeder
        $seeder->call(DevContractorSeeder::class);
        $seeder->call(DevProjectSeeder::class);
        $seeder->call(DevPageSeeder::class);


        $seeder->call(SubDomainsTableSeeder::class);
        $seeder->call(PropertiesTableSeeder::class);


        $seeder->call(TemplatesTableSeeder::class);


        // $seeder->call( customers\CallBoxStorageSeeder::class);
        //$seeder->call(customers\TiepermanHealthSeeder::class);
        //$seeder->call(customers\StandishSeeder::class);
        $seeder->call(WidgetsTableSeeder::class);
        $seeder->call(CountersTableSeeder::class);

        //  }else{
        //  echo "\nTHIS IS NOT A TESTING SERVER\n";
        // echo "Users, Customers, Properties and Individual customers have not been seeded\n\n";

        //}
        $seeder->call(AMPScriptsTableSeeder ::class);

//        $seeder->call(Ampscript_Template::class); // missing class
//        $seeder->call(\database\seeds\amp4email\Ecommerce::class); // missing class

        $seeder->call(SlugFilterTableSeeder::class);

        //demo_old
//        $seeder->call(SubscriptionsTableSeeder::class);
        $seeder->call(PlatformsTableSeeder::class);
        $seeder->call(StepsTableSeeder::class);

        $seeder->call(ReferralProgramTableSeeder::class);
        $seeder->call(CouponsTableSeeder::class);

        $seeder->call(PagesTableSeeder::class);
        $seeder->call(DemosTableSeeder::class);
        $seeder->call(DemosPagesTableSeeder::class);

        $seeder->call(PriceBucketSeeder::class);
        $seeder->call(TimeframesSeeder::class);
        $seeder->call(ContentTypesSeeder::class);

//        Seeders that need factories fixed

//        $seeder->call(TemplatesTableSeeder::class);
//        $seeder->call(SubscriptionsTableSeeder::class);
//        $seeder->call(SubDomainsTableSeeder::class);
//        $seeder->call(SocialFollowersTableSeeder::class);
//        $seeder->call(SectionsTableSeeder::class);
//        $seeder->call(PageSectionSeeder::class);
//        $seeder->call(PagesDataSeeder::class);
//        $seeder->call(PageGeneratedSeeder::class);
//        $seeder->call(MasterTemplatesTableSeeder::class);
//        $seeder->call(HTMLSeeder::class);
//        $seeder->call(FootersTableSeeder::class);
//        $seeder->call(ConnectRelationshipsSeeder::class);


        //refresh Stripe
        //echo "\n\n Seeding Stripe\n\n";

        // $robostripe=new RoboStripeCli();
        //$robostripe->refresh();


//        Viktors seeders

        //Speedy (Lighthouse Report) Seeder
        $seeder->call(LighthouseCategorySeeder::class);
        $seeder->call(LighthouseFlagSeeder::class);
        $seeder->call(LighthouseStatusSeeder::class);
        $seeder->call(LighthouseReportSeeder::class);
        $seeder->call(LighthouseFlagReportSeeder::class);


        //Calculator Seeder
//        $seeder->call(FeatureSeeder::class);

        //Amp Generator Seeder
        $seeder->call(AttributeTypeSeeder::class);
        $seeder->call(AmpcomponentSeeder::class);
        $seeder->call(AttributeSeeder::class);
       $seeder->call(AmpTemplateSeeder::class);
        $seeder->call(AmptemplateAttributeSeeder::class);

        //Schedule Tasks Seeder
        $seeder->call(TimeframesSeeder::class);
        $seeder->call(ActivitiesSeeder::class);
        $seeder->call(TasksSeeder::class);

    }

}
