<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\PotentialCustomerEmail;
use App\MyClasses\DB as myDB;

class PotentialCustomerEmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('potential_customer_emails');
        PotentialCustomerEmail::factory()->count(25)->create();
    }
}
