<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\PotentialCustomer;
use App\MyClasses\DB as myDB;

class PotentialCustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('potential_customers');
        PotentialCustomer::factory()->count(5)->create();
    }
}
