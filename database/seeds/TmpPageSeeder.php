<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\TmpPage;

class TmpPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('tmp_pages');
        TmpPage::factory()->count(21)->create();
    }
}
