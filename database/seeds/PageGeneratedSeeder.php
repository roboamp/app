<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\PageGenerated;

class PageGeneratedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('page_generated');
        PageGenerated::factory()->count(100)->create();
    }
}
