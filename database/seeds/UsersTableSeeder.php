<?php

use Illuminate\Database\Seeder;
use App\MyClasses\Seeders;
use Carbon\Carbon;
use App\MyClasses\Server;
use App\MyClasses\DB as myDB;
use App\MyClasses\MyArray;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run(){
        $seeders= new Seeders();
        myDB::truncate('users');
		//Total Users = 58

        $lou_id='$2y$10$OEXDIJoXw.yp/5pvSCr6.uttfwYnrEiB016b4MKVWZsGB3eE7FF9q';
        $lou_password=bcrypt($lou_id);
        $tiffany_password=bcrypt('$2y$10$ZgGbIVoQxDjtBOo/.1zCjej9gAvg9H8MKoPZTzUuE6bYI45uaLQ0C');

        $password=bcrypt('secret');
        $pass = bcrypt('roboamp');

        $users=[
            ['name'=>'Accenture','email'=>'accenture@accenture.com','user_role_id'=>2,'password'=>$password],
            ['name'=>'dev','email'=>'dev@roboamp.com','user_role_id'=>4,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],

            ['name'=>'Admin','email'=>'admin@roboamp.com','user_role_id'=>3,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
            ['name'=>'Roberto','email'=>'roberto@roboamp.com','user_role_id'=>3,'password'=>$pass],
            ['name'=>'Lou Garcia','email'=>'lou@lgnetworksinc.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
            ['name'=>'Tiffany Boyle','email'=>'tiffany@reapmarketing.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
            ['name'=>'Kevin Vela','email'=>'kvela@velawoodlaw.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],

        ];


    myDB::insert_items_from_array('App\User', $users);


    }
}
