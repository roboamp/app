<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB;

class ContentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::truncate('content_types');

        $arr=['Text','Class','ID','Name'];
        $model="App\ContentType";

        DB::insert_items_from_array($model,$arr);

    }
}
