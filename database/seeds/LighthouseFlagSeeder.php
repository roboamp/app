<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;

class LighthouseFlagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('lighthouse_flags');
        DB::table('lighthouse_flags')->insert([
            'name' => 'First Contentful Paint',
            'slug' => 'first-contentful-paint',
            'message' => 'First Contentful Paint marks the time at which the first text or image is painted.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_flags')->insert([
            'name' => 'Speed Index',
            'slug' => 'speed-index',
            'message' => 'A Speed Index shows how quickly the contents of a page are visibly populated.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_flags')->insert([
            'name' => 'Interactive',
            'slug' => 'interactive',
            'message' => 'Time to interactive is the amount of time it takes for the page to become fully interactive.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_flags')->insert([
            'name' => 'Largest Contentful Paint',
            'slug' => 'largest-contentful-paint',
            'message' => 'Largest Contentful Paint marks the time at which the largest text or image is painted.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_flags')->insert([
            'name' => 'Bootup Time',
            'slug' => 'bootup-time',
            'message' => 'Consider reducing the time spent parsing, compiling, and executing JS. You may find delivering smaller JS payloads helps with this.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_flags')->insert([
            'name' => 'Total Blocking Time',
            'slug' => 'total-blocking-time',
            'message' => 'Sum of all time periods between FCP and Time to Interactive.',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
