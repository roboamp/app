<?php

use Illuminate\Database\Seeder;
use App\Events\FooterUpdate;
use App\MyClasses\DB as myDB;
use Carbon\Carbon;


class DemosTableSeeder extends Seeder{

    public function run(){
        $table='demos';
        myDB::truncate($table);

        $demos=[
            ['name'=>'Radisson','user_id'=>1,'status_id'=>3],
            ['name'=>'Some Other Demo','user_id'=>1,'status_id'=>5],
            ['name'=>'Admin Demo','user_id'=>2,'status_id'=>5],

        ];


        myDB::insert_items_from_array('App\Demo', $demos);
    }

}
