<?php

namespace Database\Seeders;

use App\DevProject;
use App\Pivot_timeline_status;
use App\Timeline;
use Illuminate\Database\Seeder;

class DevProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DevProject::create(['name' => 'Project', 'status_id' => 1]);
    }
}
