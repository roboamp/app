<?php

namespace Database\Seeders;

use App\DevPage;
use Illuminate\Database\Seeder;

class DevPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DevPage::factory()->count(2)->create();
    }
}
