<?php

namespace Database\Seeders;

use App\DevContractor;
use Illuminate\Database\Seeder;

class DevContractorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DevContractor::factory()->count(5)->create();
    }
}
