<?php

use Illuminate\Database\Seeder;
use App\MyClasses\Seeders;
use Carbon\Carbon;
use App\MyClasses\Server;
use App\MyClasses\DB as myDB;
use App\MyClasses\MyArray;
class UsersTableSeeder__2019_11_12_02_13_24 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run(){
        $seeders= new Seeders();
        myDB::truncate('users');
		//Total Users = 57

																								

        $lou_id='$2y$10$OEXDIJoXw.yp/5pvSCr6.uttfwYnrEiB016b4MKVWZsGB3eE7FF9q';
        $lou_password=bcrypt($lou_id);

        $tiffany_password=bcrypt('$2y$10$ZgGbIVoQxDjtBOo/.1zCjej9gAvg9H8MKoPZTzUuE6bYI45uaLQ0C');


        $password=bcrypt('secret');

        $users=[['name'=>'Admin','email'=>'admin@roboamp.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'Roberto','email'=>'roberto@roboamp.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'Lou Garcia','email'=>'lou@lgnetworksinc.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'Tiffany Boyle','email'=>'tiffany@reapmarketing.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'Kevin Vela','email'=>'kvela@velawoodlaw.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'TGMA','email'=>'tgma@tgma.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'Kim Langston','email'=>'kim@oskyblue.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'Kyle Bainter','email'=>'kbainter@callboxstorage.com','user_role_id'=>1,'password'=>'$2y$10$eNowFnHgou9hEm7PkqYs..wJcZlM2Qfva/0Y71492Rkvx.fQ0lMRG'],
['name'=>'ii','email'=>'koko@joo.com','user_role_id'=>1,'password'=>'$2y$10$Q1OZ07QrlOmsTSaCitSg2.PyABmA/u/RBSZvi7HkUslOwVfFrhQy6'],
['name'=>'AS','email'=>'jon@doe.com','user_role_id'=>1,'password'=>'$2y$10$.Sfi4PZLbWcf5QA28uHJ1OsmABWMxzmt/4QRYktM5I2JW05vjYj4y'],
['name'=>'Customer Name','email'=>'sas@LLL.com','user_role_id'=>1,'password'=>'$2y$10$HiW74Z8MVHjVzaJiroNCOulROgRwmI8RLi3PAvlvRjdOzkWnmGB9y'],
['name'=>'a','email'=>'KAKA@LL.com','user_role_id'=>1,'password'=>'$2y$10$b33LR96sdJusmlm5xahkFOONNRYKI.IfCkz6ZLwY.TYMbb9UuJqoy'],
['name'=>'a','email'=>'asd@KKKK.com','user_role_id'=>1,'password'=>'$2y$10$PwIilzTmRX9.bDIrgQSn1OQhyjN4SZ/koZve/P0u70MIbEiiSXgeC'],
['name'=>'asdasd','email'=>'asdas@kk.com','user_role_id'=>1,'password'=>'$2y$10$Pwx4JYm3Bc8bjNfe5OI.fuueSJ9QXxx9SUpse4Ewp2Ejok9LaSYre'],
['name'=>'ASDW','email'=>'asdasd22@PP.COM','user_role_id'=>1,'password'=>'$2y$10$gWKXofR/pidAaz9bnaGsIuH32BoYZbhA1vu2QvDlIogn.RL/XIGrW'],
['name'=>'ADSASD','email'=>'ASDA@9.COM','user_role_id'=>1,'password'=>'$2y$10$SMOSGM8RUeGDdpj9YPHolO.RaSeRrodUCB/ojJwlGDflJefaxb9Ci'],
['name'=>'asdaw','email'=>'lol@jjjj.com','user_role_id'=>1,'password'=>'$2y$10$5EN1C43Ua2O7CU3DcQbqnOryc6PznUkmE.ajCZB5nEKYbeklScsVq'],
['name'=>'asdw','email'=>'as@87s.com','user_role_id'=>1,'password'=>'$2y$10$Z3B3zxFVVEZz1juMrCtKFuN7kBh5jNVFqr3Ck0ava4OowBGcrLKo.'],
['name'=>'aed','email'=>'adasd@ll.com','user_role_id'=>1,'password'=>'$2y$10$JV28vPrmqb8.Qu6LWa20EOxq0m0m6vEUr6iC23dvYX4oGpkJKIMYG'],
['name'=>'adasdasd','email'=>'ass@dks.com','user_role_id'=>1,'password'=>'$2y$10$33ywJw.BlUTPfDA/ZEEsuuQRzknlkwoCPxfmLO.U9WuueZjINNhYu'],
['name'=>'adasdasd','email'=>'asdasdasda@kkk.com','user_role_id'=>1,'password'=>'$2y$10$tragoS/TjxOAKVViKpVEfu7USdd1BLYmnaqSRi096g2K77.vtKOoK'],
['name'=>'adasd','email'=>'adasad@adhsjahd.com','user_role_id'=>1,'password'=>'$2y$10$qf4jNYZW9/Aw6KOHA3s6SetG1uSspiyC.20Tu9mFP2gpScFynaOpq'],
['name'=>'asdasd','email'=>'asdada@asduisd.com','user_role_id'=>1,'password'=>'$2y$10$lpcmr74RE6fBhbgH2/aYReeQwTvfzhwM6QUNpyHMMxE5EjAzgHOt6'],
['name'=>'asdasd','email'=>'aad@tyatsdas.com','user_role_id'=>1,'password'=>'$2y$10$nNB0lSHhbfQ7tp2hoxb00OjPB/FP0.HQSNYtfnDpmly11X1SwtUDi'],
['name'=>'adadadasd','email'=>'asdad@trtrtrda.dsa','user_role_id'=>1,'password'=>'$2y$10$WizAluBD7cU.G/071fntXO4erO33ZqWbHJQWJVLDI7J64ge3n8fi2'],
['name'=>'sadasw','email'=>'anastasio@pereo.com','user_role_id'=>1,'password'=>'$2y$10$VC.Gsq3d3x3VriwwP1dDoO7QhE9/qnRy7okH6OfhgcAMmSv8SAj8G'],
['name'=>'Camilo','email'=>'camilo@lmao.com','user_role_id'=>1,'password'=>'$2y$10$7LLM2hyB.Bib2o5MCHuKEOd6j3mvklbMOiB4Y1VlTZqVy.p4Tzt/a'],
['name'=>'Michael','email'=>'michael@lol.com','user_role_id'=>1,'password'=>'$2y$10$lbb1UaXna6c0cDRuKfisjuAAIKXGLF9GjKl4A3FXfiUn1biqcsvnq'],
['name'=>'hahaha','email'=>'jojo@gg.com','user_role_id'=>1,'password'=>'$2y$10$z7pgm/EoJ27yhK/JwxQQeO9A/9MKQGQ3NHQI/2dA4xMRAxjdK1sqS'],
['name'=>'ajahsdjsahd','email'=>'koko@jj.com','user_role_id'=>1,'password'=>'$2y$10$2kTuzmCFY9sn2Hc1q1QoYet291H608lkQgsRkW4fz2n2660At/IZi'],
['name'=>'Pepe','email'=>'pepe@guardiola.com','user_role_id'=>1,'password'=>'$2y$10$vwXdi0GL2FrG1ctK50tOQOUVLCaZTs2zwR/76tw2/2w0z4QaG7v.y'],
['name'=>'Taytus','email'=>'juju@hihi.com','user_role_id'=>1,'password'=>'$2y$10$shkrjGNQ5Vpw57YtK4FzrecWwnKzX/33uJkf5SedIfI.oJ4w/w5a.'],
['name'=>'Malals','email'=>'gyg@jiji.com','user_role_id'=>1,'password'=>'$2y$10$VGVMxsvrkQczfm9Y3GNHeONeafsDGTvghNCJPL582tRzG6vWD0qw.'],
['name'=>'camilo','email'=>'asdasd@h4k.com','user_role_id'=>1,'password'=>'$2y$10$7YiR5eLddVFz3kGOrmCD4Oxj3XDbyo2sXJrIlFHMhH5dn6mS89k.S'],
['name'=>'malaka','email'=>'jojo@huyts.com','user_role_id'=>1,'password'=>'$2y$10$dx9aqcipnccf07nAWLJk6u7taWEW/FChqTTiBIJusSSCsdc3e9W0e'],
['name'=>'adasda','email'=>'asdad@ll.com','user_role_id'=>1,'password'=>'$2y$10$Sc610Ru0B52HotGzMI8Gs.DYdzzHKhWaIPm/YLXJbORQVPo0LnLDm'],
['name'=>'adasd','email'=>'asdasd@sad.com','user_role_id'=>1,'password'=>'$2y$10$apzaycyuk2CB/dnSW4He7OyeEGLaORdRpLOmNoG9Vx/mGGkyx9FzS'],
['name'=>'asdasdw','email'=>'adad@lll.com','user_role_id'=>1,'password'=>'$2y$10$VdEL06D9mPcosOnLobjUD.sFs9wh8FKK6MNYPJYSdAEkOqN01NdPi'],
['name'=>'NANA','email'=>'jiji@jiji.co','user_role_id'=>1,'password'=>'$2y$10$YIuKDgeLUgnKDvmAS/HuieobCxbkIMnvZ1CUjdyYtrmIK8nJszqrW'],
['name'=>'adad','email'=>'koko@oko.com','user_role_id'=>1,'password'=>'$2y$10$lCJfwacDuwLW0Vaf45Kheuc8.ceu4wegutcLu0DtWr4DXJaHyRqNa'],
['name'=>'asdad','email'=>'eyeyey@yeye.com','user_role_id'=>1,'password'=>'$2y$10$SLFI4Dfozo0UlEMgEV9kMOoF.IeeSuRnkh05KwGqb.Rxe.PVWsTG6'],
['name'=>'adasdada','email'=>'adasd@kk.com','user_role_id'=>1,'password'=>'$2y$10$HDHevjOJ6L4Za/6EKViU..GpBhkzdRh/mj6Fkvo95sjE2VHqqgn/6'],
['name'=>'adasd','email'=>'adasdads@sss.com','user_role_id'=>1,'password'=>'$2y$10$2vD61Xtd4SiUefJsXDLMSOq/zxIJX2/oo8kEUUu5tngcD2jHOBEbC'],
['name'=>'adasdasd','email'=>'asdasd@lll.com','user_role_id'=>1,'password'=>'$2y$10$vmYdfnkq91PaOoLo4944ZeSiUKUYm9YAKsrcE1eu8BmEieM7xBKO2'],
['name'=>'adadaw','email'=>'asdasd@hgh.com','user_role_id'=>1,'password'=>'$2y$10$zf40zKbS0/pac7A.e/DvneSyZAXy.F7ap/5z.1m0HzIYR8wRgQNhe'],
['name'=>'adsa','email'=>'as@adas3.com','user_role_id'=>1,'password'=>'$2y$10$kEPIc6J4/9hlbbbrdHy/6O8K/b9rsJIUwqyaR4.FcQSbiF6PxfpRO'],
['name'=>'asdasd','email'=>'asdasd@ll.com','user_role_id'=>1,'password'=>'$2y$10$1oQOYQ13D8EVh.NbRE6Tl..r.wJev523Jo.InN8vKKm2jJGEXTEke'],
['name'=>'adasd','email'=>'asda@ll.com','user_role_id'=>1,'password'=>'$2y$10$a0ccOv4AlJrdc.peikYHveS0EQx2UEPgSxrbJA78XCECybKKcI0ma'],
['name'=>'koko loco','email'=>'koko@kokoloco.com','user_role_id'=>1,'password'=>'$2y$10$JjwxPBR4MYMFMTQ/uIP4Je9hI9hLEcF3qaYjQKuX1n2ykgVekGQ7a'],
['name'=>'tetas','email'=>'tetas@tetas.com','user_role_id'=>1,'password'=>'$2y$10$FiJ4kuS1DAvrQYcpwPUy6u7FQsrDaKlm55hA6Qefii2bc1Ih.puCe'],
['name'=>'Tetass','email'=>'tetas2@tetas.com','user_role_id'=>1,'password'=>'$2y$10$NBgf4LnPIlWp00aA9WCv1.AfAQevZHRU0AVofSbRbzhLHy9NkIRJO'],
['name'=>'tetasthree','email'=>'tetas3@tetas.com','user_role_id'=>1,'password'=>'$2y$10$FqK2XGDG.DPhIqQ4zO62XOJ1YK4jDrxpH5fh8wNCPuK72kcAGwShq'],
['name'=>'asdasdasd','email'=>'koko@loloas.com','user_role_id'=>1,'password'=>'$2y$10$i1RBZAo4Im8uS3i16BBMQeiprADrdd5WL52u/gj.YqaHFJ7wLX5V2'],
['name'=>'adasd','email'=>'kk@lolp.com','user_role_id'=>1,'password'=>'$2y$10$D9IMIZodceBWMbx4ONT79OfCREmgpqc67brw5eqhljJnCla2odVRu'],
['name'=>'jom','email'=>'kiki@lolo.com','user_role_id'=>1,'password'=>'$2y$10$hnOhktd0daHB.r/c1raAuerHR3vL/0re5iRmxbVB9qMuNMlTyQ83u'],
['name'=>'Hihihihi','email'=>'adsad@lopo.cmo','user_role_id'=>1,'password'=>'$2y$10$yiTedbQTpj7HwI4.feCQMeIpd8CUxY03whiNVZLBlNn7ZEbubH.4e'],
['name'=>'asdasd','email'=>'koko@lol.com','user_role_id'=>1,'password'=>'$2y$10$vGgN/EsdqZPBEh6hc3dHfe8HC3mXpZ.k5S3fqcO1t0gUCDoiqQTo.']];


        $server=new Server();

        $now= Carbon::now();

        if($server->testing_server()) {


            myDB::insert_items_from_array('App\User', $users);

/*
            factory(App\User::class, 10)->create()->each(function ($u) {
                $seeders = new Seeders();
            });

            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
            */
        }




    }
}
