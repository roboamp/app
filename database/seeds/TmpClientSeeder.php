<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\TmpClient;

class TmpClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('tmp_clients');
        TmpClient::factory()->count(1)->create();
    }
}
