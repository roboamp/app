<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB;


class DemosPagesTableSeeder extends Seeder{

    public function run(){
        $table='demo_pages';
        DB::truncate($table);

        $base_url='127.0.0.1:8000/demo/radisson/';
        $testing_url="https://roboamp.com/some_url/";
        $demos=[
            ['name'=>'index','local_url'=>$base_url.'index','eta'=>'2020-07-15 01:56:26','testing_url'=>$testing_url.'index','demo_id'=>1,'status_id'=>5],
            ['name'=>'index2','local_url'=>$base_url.'index','eta'=>'2020-07-15 01:56:26','testing_url'=>$testing_url.'index','demo_id'=>2,'status_id'=>1],
            ['name'=>'index3','local_url'=>$base_url.'index','eta'=>'2020-07-15 01:56:26','testing_url'=>$testing_url.'index','demo_id'=>1,'status_id'=>2],
            ['name'=>'index4','local_url'=>$base_url.'index','eta'=>'2020-07-15 01:56:26','testing_url'=>$testing_url.'index','demo_id'=>3,'status_id'=>5],

        ];

        DB::insert_items_from_array('App\DemoPage', $demos);


    }

}
