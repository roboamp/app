<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;

class AttributeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('attribute_types');

        DB::table('attribute_types')->insert([
            'name' => 'Boolean',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipiscing elit nascetur interdum mus, condimentum ligula turpis ridiculus.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('attribute_types')->insert([
            'name' => 'String',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipiscing elit nascetur interdum mus, condimentum ligula turpis ridiculus.',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
