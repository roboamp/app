<?php

use Illuminate\Database\Seeder;
use App\MyClasses\Seeders;
use Carbon\Carbon;
use App\MyClasses\DB as myDB;
class ReferralProgramTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
    //@return void
     */

    public function run(){
        return true;

        $now=Carbon::now();

        $table='referral_programs';
        $seeders= new Seeders();
        myDB::truncate('referral_programs');

        $names=['partners','customers'];

        foreach ($names as $name){
            DB::table($table)->insert(['name'=>$name, 'uri'=>'register/'.$name, 'lifetime_minutes'=> 10080, 'created_at'=>$now->getTimestamp(),'updated_at'=>$now->getTimestamp()]);
        };

        myDB::SET_FOREIGN_KEY(1);

    }
}
