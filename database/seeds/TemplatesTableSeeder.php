<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;

use App\Template;


class TemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run(){
        $table="templates";
        $now=new \Carbon\Carbon();

        myDB::truncate($table);

        $property_id='3a256d94-4986-46a3-b466-ff90709c0277';

        $selector_id=\App\SelectorType::inRandomOrder()->pluck('id')->first();

        $templates=[
                ['id'=>'c5c30070-ce72-11e7-acde-9b4153c2a21e','selector_id'=>$selector_id,'property_id'=>$property_id,'name'=>'index','created_at'=>$now,'updated_at'=>$now],
                ['id'=>'c3d61bbe-3969-48c4-947a-6fc54dd3dbb8','selector_id'=>$selector_id,'property_id'=>$property_id,'name'=>'general','created_at'=>$now,'updated_at'=>$now]

        ];

        foreach ($templates as $item){
            $template = new Template();
            foreach ($item as $obj =>$val){
                $template->$obj=$val;
            }
            //echo $template->selector_id."(O)Y(O)\n";
            echo $template->selector_id."\n";
            $template->save();
            echo "nonononononono\n";
        };

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        ;

    }

}
