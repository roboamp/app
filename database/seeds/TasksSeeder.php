<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\Task;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('tasks');
        Task::factory()->count(20)->create();
    }
}
