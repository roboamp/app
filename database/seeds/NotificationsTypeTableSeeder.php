<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB;
use App\NotificationsType;
class NotificationsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $types=['User has not added a property','ROBOAMP code was cannot be found',
               'User has not confirmed the code'
        ];

         DB::insert_items_from_array('App\NotificationsType',$types);

    }
}
