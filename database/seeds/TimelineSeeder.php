<?php

use App\Timeline;
use App\MasterStatus;
use Illuminate\Database\Seeder;
use App\MyClasses\DB;
use Faker\Factory as Faker;



class TimelineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::truncate('timelines');
        DB::truncate('status_timeline');
        $faker = Faker::create();

        // hard coded timeline for project.

        $project = Timeline::create([
            'name' => 'Project Status',
            'description' => 'Project description'
        ]);

        $project->statuses_seeder()->attach(1, ['description' => 'Project Created.']);
        $project->statuses_seeder()->attach(2, ['description' => 'Project Assigned.']);
        $project->statuses_seeder()->attach(3, ['description' => 'Project QA.']);
        $project->statuses_seeder()->attach(4, ['description' => 'Project Delivered.']);
        $project->statuses_seeder()->attach(5, ['description' => 'Project Closed.']);

        Timeline::factory()->count(4)->create();

        foreach (Timeline::all() as $timeline) {
            $status = MasterStatus::all()->take(rand(1, 5))->pluck('id');
            if($timeline->id != 1) {
                foreach ($status as $description) {
                    $timeline->statuses_seeder()->attach($description, ['description' => $faker->sentence()]);
                }
            }
        }
    }
}
