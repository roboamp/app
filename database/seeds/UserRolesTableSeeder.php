<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\MyClasses\DB as myDB;
class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run(){
        $now=Carbon::now();
        $table='user_roles';
        myDB::truncate($table);

        $statuses=['user','partner','admin','developer'];

        foreach ($statuses as $status){
            DB::table($table)->insert(['name'=>$status,'created_at'=>$now,'updated_at'=>$now]);
        };


        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
