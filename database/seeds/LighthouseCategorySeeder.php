<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;

class LighthouseCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('lighthouse_categories');
        DB::table('lighthouse_categories')->insert([
            'name' => 'SEO Agencies',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipiscing elit nascetur interdum mus, condimentum ligula turpis ridiculus.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_categories')->insert([
            'name' => 'Web Development',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipiscing elit nascetur interdum mus, condimentum ligula turpis ridiculus.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_categories')->insert([
            'name' => 'Advertising',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipiscing elit nascetur interdum mus, condimentum ligula turpis ridiculus.',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_categories')->insert([
            'name' => 'Small & Medium Business',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipiscing elit nascetur interdum mus, condimentum ligula turpis ridiculus.',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
