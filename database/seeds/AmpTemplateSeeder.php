<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\Amptemplate;

class AmpTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('amptemplates');
        Amptemplate::factory()->count(10)->create();
    }
}
