<?php
use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\Feature;

class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('features');
        Feature::factory()->count(10)->create();
    }
}
