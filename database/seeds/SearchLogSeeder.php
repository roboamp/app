<?php

use App\Query;
use Illuminate\Database\Seeder;

class SearchLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Query::factory()->count(50)->create();
//        factory(App\SearchLog::class, 50)->create();
    }
}
