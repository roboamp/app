<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\AmptemplateAttribute;

class AmptemplateAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('amptemplate_attributes');
        AmptemplateAttribute::factory()->count(50)->create();
    }
}
