<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\Activity;

class ActivitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('activities');
        Activity::factory()->count(5)->create();
    }
}
