<?php

use Illuminate\Database\Seeder;
use App\MyClasses\Seeders;
use App\MyClasses\DB as myDB;

class UsersTableSeederTemp extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run(){
        $seeders= new Seeders();
        myDB::truncate('users');

        $password=bcrypt('secret');
        $pass = bcrypt('roboamp');

        $users=[
            ['name'=>'Valeria','email'=>'valeria@roboamp.com','user_role_id'=>1,'password'=>$password],
            ['name'=>'Accenture','email'=>'accenture@accenture.com','user_role_id'=>2,'password'=>$password],
            ['name'=>'Roberto','email'=>'roberto@roboamp.com','user_role_id'=>3,'password'=>$pass],
            ['name'=>'Viktor','email'=>'viktor@roboamp.com','user_role_id'=>4,'password'=>$pass]
        ];


    myDB::insert_items_from_array('App\User', $users);


    }
}
