<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\Attribute;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('attributes');
        Attribute::factory()->count(40)->create();
    }
}
