<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB;

class SelectorTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::truncate('selector_types');

        $arr=['tag','div','class','header_link'];
        $model="App\SelectorType";

        DB::insert_items_from_array($model,$arr);

    }
}
