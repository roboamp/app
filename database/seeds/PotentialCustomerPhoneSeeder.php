<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\PotentialCustomerPhone;
use App\MyClasses\DB as myDB;

class PotentialCustomerPhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('potential_customer_phones');
        PotentialCustomerPhone::factory()->count(25)->create();
    }
}
