<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\Ampcomponent;

class AmpcomponentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      myDB::truncate('ampcomponents');
      Ampcomponent::factory()->count(8)->create();
    }
}
