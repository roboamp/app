<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;

class LighthouseStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('lighthouse_statuses');
        DB::table('lighthouse_statuses')->insert([
            'name' => 'Pending',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit dis gravida tempus, pellentesque et auctor sapien inceptos ullamcorper praesent per felis, semper aenean tellus consequat hendrerit augue placerat mauris est.',
            'badge' => 'primary',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_statuses')->insert([
            'name' => 'Generated',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit dis gravida tempus, pellentesque et auctor sapien inceptos ullamcorper praesent per felis, semper aenean tellus consequat hendrerit augue placerat mauris est.',
            'badge' => 'info',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_statuses')->insert([
            'name' => 'Email Sended',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit dis gravida tempus, pellentesque et auctor sapien inceptos ullamcorper praesent per felis, semper aenean tellus consequat hendrerit augue placerat mauris est.',
            'badge' => 'warning',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_statuses')->insert([
            'name' => 'Email Readed',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit dis gravida tempus, pellentesque et auctor sapien inceptos ullamcorper praesent per felis, semper aenean tellus consequat hendrerit augue placerat mauris est.',
            'badge' => 'success',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('lighthouse_statuses')->insert([
            'name' => 'Report Error',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit dis gravida tempus, pellentesque et auctor sapien inceptos ullamcorper praesent per felis, semper aenean tellus consequat hendrerit augue placerat mauris est.',
            'badge' => 'danger',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
