<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\LighthouseFlagReport;

class LighthouseFlagReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('lighthouse_flag_reports');
        LighthouseFlagReport::factory()->count(40)->create();
    }
}
