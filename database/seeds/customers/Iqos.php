<?php
namespace database\seeds\customers;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;



class Iqos extends Seeder{
//To quickly generate a UUID just do

//Uuid::generate()

    public function run(){



        $now=time();

        $property_id='f638e0e0-fee6-4a5e-b917-3b4deed70268';
        $property_root_url="https://ch.iqos.com/en/";

        $pages=[
            ['id'=>'f088d41a-5135-4df2-b4e5-71f4d18a7e14','url'=>urlencode($property_root_url."/accessories"),'property_id'=>$property_id,'name'=>'accessories','created_at'=>$now,'updated_at'=>$now],
            ['id'=>'0a7336be-3649-47ce-9874-6c282af26351','url'=>urlencode($property_root_url.'/FamilyCodeNight'),'property_id'=>$property_id,'name'=>'FamilyCodeNight','created_at'=>$now,'updated_at'=>$now],
            ['id'=>'48450c48-7870-47cc-9ec8-66145873984e','url'=>urlencode($property_root_url.'/Parent-Teacher-Conference'),'property_id'=>$property_id,'name'=>'Parent-Teacher-Conference','created_at'=>$now,'updated_at'=>$now],
            ['id'=>'b9714ded-af47-4308-848a-bdc23547dada','url'=>urlencode($property_root_url.'/classparty'),'property_id'=>$property_id,'name'=>'classparty','created_at'=>$now,'updated_at'=>$now],
            ['id'=>'058f7525-1450-432a-909b-7ed754cab15e','url'=>urlencode($property_root_url.'/holiday'),'property_id'=>$property_id,'name'=>'holiday','created_at'=>$now,'updated_at'=>$now],
            ['id'=>'3f568e3f-9f28-4bb5-bff5-96ce6eae0cd9','url'=>urlencode($property_root_url.'/idea-center'),'property_id'=>$property_id,'name'=>'idea-center','created_at'=>$now,'updated_at'=>$now],


        ];
        return $pages;

    }

}