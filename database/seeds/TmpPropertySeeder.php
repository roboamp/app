<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;
use App\TmpProperty;

class TmpPropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('tmp_properties');
        TmpProperty::factory()->count(3)->create();
    }
}
