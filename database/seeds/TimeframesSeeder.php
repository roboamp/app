<?php

use Illuminate\Database\Seeder;
use App\MyClasses\DB as myDB;

class TimeframesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        myDB::truncate('timeframes');
        DB::table('timeframes')->insert([
            'name' => 'Hourly',
            'value' => 'hourly',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Every 2 Hours',
            'value' => 'everyTwoHours',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Every 4 Hours',
            'value' => 'everyFourHours',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Every 6 Hours',
            'value' => 'everySixHours',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Daily',
            'value' => 'daily',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Weekly',
            'value' => 'weekly',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Monthly',
            'value' => 'monthly',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Twice Monthly',
            'value' => 'twiceMonthly',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Every 3 Months',
            'value' => 'quarterly',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('timeframes')->insert([
            'name' => 'Yearly',
            'value' => 'yearly',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
