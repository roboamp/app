<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\SearchLog::class, function (Faker $faker) {
    return [
        'term' => $faker->word
    ];
});
