<?php

namespace Database\Factories;

use App\Attribute;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class AttributeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Attribute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'component_id' => $this->faker->numberBetween($min = 1, $max = 8),
            'name' => $this->faker->word,
            'description' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'type_id' => 2 ,
            'value' => '{"default": "'.$this->faker->word.'"}' ,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
