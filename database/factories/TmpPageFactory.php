<?php

namespace Database\Factories;

use App\TmpPage;
use App\PageSection;
use App\TmpProperty;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TmpPageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TmpPage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $properties = TmpProperty::all();
        $property = $this->faker->randomElement($array = $properties);
        $sections_all = $property->sections;
        $sections = $sections_all->flatten()->pluck('id')->unique();
        $sections[] = null;
        return [
            'property_id' => $property->id,
            'section_id' => $this->faker->randomElement($array = $sections),
            'name' => $this->faker->word,
            'url' => 'https://'.$property->domain.'/'.$this->faker->slug,
        ];
        }
}
