<?php

namespace Database\Factories;

use App\LighthouseReport;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LighthouseReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LighthouseReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'url' => 'https://'.$this->faker->domainName,
            'lighthouse_category_id' => $this->faker->numberBetween($min = 1, $max = 4),
            'type' => $this->faker->randomElement($array = array ('User','Client','Admin','Seller')),
            'lighthouse_status_id' => $this->faker->numberBetween($min = 1, $max = 4),
            'added_on' => now(),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
