<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\FAQ;
use Faker\Generator as Faker;

$factory->define(FAQ::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence,
        'answer' => $faker->sentence,
        'privacy' => $faker->randomElement(['public', 'private', 'secret']),
        'user_id' => factory(\App\User::class),
        'category_id' => $faker->numberBetween(1, 5),
    ];
});
