<?php

namespace Database\Factories;

use App\TmpProperty;
use App\PriceBucket;
use App\TmpClient;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TmpPropertyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TmpProperty::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //$clients = TmpClient::all();
        $client = 1;
        $buckets = PriceBucket::all();
        $domain = $this->faker->domainWord;
        return [
            'client_id' => $client,
            'price_bucket_id' => $this->faker->randomElement($array = $buckets->flatten()->pluck('id')->unique()),
            'name' => $domain,
            'domain' => $domain.'.com',
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
