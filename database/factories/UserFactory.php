<?php

namespace Database\Factories;

use App\MyClasses\Seeders;
use App\MyClasses\Strings;
use App\MyClasses\Stripe\RoboStripe;
use App\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $password;
        $seeders= new Seeders();
        $robostripe=new RoboStripe();

        $user=Array();

        $user['email']=$this->faker->unique()->safeEmail;

        $user['stripe_id']='temp_'.rand(1,1000);//$robostripe->create("Customer",$user);

        //$robostripe->create('source',($user['stripe_id']));



        $obj=Array();


        $obj['name']=$this->faker->name;
        $obj['password']= $password ?: $password = bcrypt('secret');
        $obj['remember_token']= Strings::random(10);
        $obj['test']=true;
        $obj['email']=$user['email'];
        $obj['stripe_id']=$user['stripe_id'];
        // $obj['user_role_id']=$seeders->field_from_random_record_from_table('userrole','id');

        return $obj;
    }
}
