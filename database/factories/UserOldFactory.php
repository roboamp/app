<?php

use App\MyClasses\Stripe\RoboStripe;
use App\MyClasses\Seeders;
use App\MyClasses\Strings;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $seeders= new Seeders();
    $robostripe=new RoboStripe();

    $user=Array();

    $user['email']=$faker->unique()->safeEmail;

    $user['stripe_id']='temp_'.rand(1,1000);//$robostripe->create("Customer",$user);

    //$robostripe->create('source',($user['stripe_id']));



    $obj=Array();


    $obj['name']=$faker->name;
    $obj['password']= $password ?: $password = bcrypt('secret');
    $obj['remember_token']= Strings::random(10);
    $obj['test']=true;
    $obj['email']=$user['email'];
    $obj['stripe_id']=$user['stripe_id'];
    // $obj['user_role_id']=$seeders->field_from_random_record_from_table('userrole','id');

    return $obj;

});
