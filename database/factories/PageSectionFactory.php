<?php

namespace Database\Factories;

use App\PageSection;
use App\TmpProperty;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PageSectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PageSection::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $properties = TmpProperty::all();
        $property = $this->faker->randomElement($array = $properties);
        return [
            'name' => $this->faker->word,
            'property_id' => $property->id,
            'description' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'url' => 'https://'.$property->domain.'/'.$this->faker->word,
            'limit' => $this->faker->numberBetween($min = 1, $max = 20)
        ];
    }
}
