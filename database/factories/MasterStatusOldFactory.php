<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MasterStatus;
use App\Model;
use Faker\Generator as Faker;

$factory->define(MasterStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
