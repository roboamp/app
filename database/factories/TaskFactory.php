<?php

namespace Database\Factories;

use App\Task;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'activity_id' => $this->faker->numberBetween($min = 1, $max = 5),
            'timeframe_id' => $this->faker->numberBetween($min = 1, $max = 10),
            'user_id' => $this->faker->numberBetween($min = 1, $max = 4),
            'parameters' => $this->faker->url,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
