<?php

namespace Database\Factories;

use App\DevProject;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DevProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DevProject::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'status_id' => 1
        ];
    }
}
