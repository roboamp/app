<?php

namespace Database\Factories;

use App\Activity;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Activity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'label' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'command' => 'php artisan command:'.$this->faker->word,
            'description' => $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
