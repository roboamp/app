<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Partials\Social::class, function (Faker\Generator $faker,$data) {
    static $words,$length=5;
    $content=[];

    $words=$faker->words($length,false);
    //creates array with words and urls

    for($j=0;$j<=$length-1;$j++){
        $tmp_content['caption']=$words[$j];
        $tmp_content['url']=$faker->url();
        $content[]=$tmp_content;
    }


    return [
        'template_id' => $data['template_id'],
        'content' => json_encode($content),
    ];

});
