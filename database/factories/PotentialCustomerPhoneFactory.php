<?php

namespace Database\Factories;

use App\PotentialCustomerPhone;
use App\PotentialCustomer;
use Illuminate\Database\Eloquent\Factories\Factory;

class PotentialCustomerPhoneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PotentialCustomerPhone::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $customers = PotentialCustomer::all();
        $pcustomers = $customers->pluck('id');
        return [
            'phone' => $this->faker->e164PhoneNumber,
            'potential_customer_id' => $this->faker->randomElement($array= $pcustomers),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
