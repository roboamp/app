<?php

namespace Database\Factories;

use App\AmptemplateAttribute;
use App\Amptemplate;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AmptemplateAttributeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AmptemplateAttribute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $templates = Amptemplate::all();
        $template = $this->faker->randomElement($array = $templates);
        $attributes = $template->component->attributes->flatten()->pluck('id')->unique();
        $attribute = $this->faker->randomElement($array = $attributes);
        
        return [
            'template_id' => $template,
            'attribute_id' => $attribute,
            'value' => $this->faker->word,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
