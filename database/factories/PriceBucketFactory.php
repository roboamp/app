<?php

namespace Database\Factories;

use App\PriceBucket;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PriceBucketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PriceBucket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'pricing' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 1000),
            'max' => $this->faker->numberBetween($min = 1, $max = 50),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
