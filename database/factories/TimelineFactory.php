<?php

namespace Database\Factories;

use App\Timeline;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TimelineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Timeline::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'description'=> $this->faker->sentence
        ];
    }
}
