<?php

namespace Database\Factories;

use App\Customer;
use App\MyClasses\Seeders;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $seeder= new Seeders();
        $now=\Carbon\Carbon::now();

        return [
            'id'=>Uuid::generate(4),
            'name'=>$this->faker->name(),
            'password'=>bcrypt('secret'),
            'user_id'=>$seeder->field_from_random_record_from_table('User','id'),
            'testing'=>1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ];
    }
}
