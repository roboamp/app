<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Partials\PartialTemplate::class, function (Faker\Generator $faker) {


    //template_id is based on how many templates I have;
    //so first I need to access to those templates

    $templates_total=\App\Template::all();
    $template_id=mt_rand(1,count($templates_total));
    //now I need to get their uuid so I can add it to the path


    //name for the child template
    $name=$faker->domainName();


    $now=\Carbon\Carbon::now();

    return [
        'template_id' => $template_id,
        'name'=>$name,
        'path'=> bin2hex(openssl_random_pseudo_bytes(8)),
        'created_at'=>$now,
        'updated_at'=>$now,
    ];

});
//'
