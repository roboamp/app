<?php

namespace Database\Factories;

use App\PageGenerated;
use App\PageSection;
use App\TmpProperty;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PageGeneratedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PageGenerated::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $properties = TmpProperty::all();
        $property = $this->faker->randomElement($array = $properties);
        $sections_all = $property->sections;
        $sections = $sections_all->flatten()->pluck('id')->unique();
        $sections[] = null;
        return [
            'property_id' => $property->id,
            'section_id' => $this->faker->randomElement($array = $sections),
            'page_url' => 'https://'.$property->domain.'/'.$this->faker->slug,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
