<?php

namespace Database\Factories;

use App\FAQ;
use App\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FAQFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FAQ::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'question' => $this->faker->sentence,
            'answer' => $this->faker->sentence,
            'privacy' => $this->faker->randomElement(['public', 'private', 'secret']),
            'user_id' => User::factory(),
            'category_id' => $this->faker->numberBetween(1, 5),
        ];
    }
}
