<?php
use Webpatser\Uuid\Uuid;
use App\MyClasses\Seeders;
use App\MyClasses\URL;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Property::class, function (Faker\Generator $faker) {
    $seeder= new Seeders();
    $now=\Carbon\Carbon::now();
    $url=new URL();
    return [
        'id'=>Uuid::generate(4),
        'status_id' => $faker->numberBetween(2,4),
        'customer_id'=>$seeder->field_from_random_record_from_table('customer','id'),
        'url'=>$url::get_domain($faker->domainName()),
        'plan_id'=>$faker->numberBetween(1,4),
        'steps_id'=>1,
        'created_at'=>$now,
        'updated_at'=>$now,
    ];
});
