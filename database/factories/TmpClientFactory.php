<?php

namespace Database\Factories;

use App\TmpClient;
use App\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TmpClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TmpClient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        //$users = User::all();
        $user = 2;

        return [
            'name' => 'Accenture',
            'email' => 'accenture@accenture.com',
            'phone' => $this->faker->e164PhoneNumber,
            'address' => $this->faker->address,
            'user_id' => $user,
            'created_at' => now(),
            'updated_at' => now()
        ];
        /*return [
            'name' => $this->faker->name,
            'email' => $this->faker->companyEmail,
            'phone' => $this->faker->e164PhoneNumber,
            'address' => $this->faker->address,
            'user_id' => $this->faker->randomElement($array = $users->flatten()->pluck('id')->unique()),
            'created_at' => now(),
            'updated_at' => now()
        ];*/
    }
}
