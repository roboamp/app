<?php
use App\MyClasses\Stripe\RoboStripe;
use Faker\Generator as Faker;
use App\MyClasses\Seeders;
use App\User;

///** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Subscription::class, function (Faker $faker) {

    $robostripe=new RoboStripe();
    $seeder=new Seeders();
    $now=\Carbon\Carbon::now();

    $user=$seeder->random_record_from_table('user');

    //some records don't have a stripe_id field
    while($user->stripe_id==null){
        $user=$seeder->random_record_from_table('user');
    }
    //customer is not OUR customer. This is our User but stripe calls them customer

    $plan=$seeder->random_record_from_table('plan');
    $subscription['customer']=$user->stripe_id;
    $subscription['items']=array(array('plan'=>$plan->stripe_id));


    //OBJ is the object I'm going to save on the table
    $obj['user_id']=$user->id;
    $obj['plan_id']=$plan->id;
    $obj['stripe_id']='temp_'.rand(1,1000);//$robostripe->create("Subscription",$subscription);
    $obj['quantity']=1;
    $obj['created_at']=$now;
    $obj['updated_at']=$now;


    return $obj;

});
