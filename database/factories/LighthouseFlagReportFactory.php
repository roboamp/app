<?php

namespace Database\Factories;

use App\LighthouseFlagReport;
use App\LighthouseReport;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class LighthouseFlagReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LighthouseFlagReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $uuid = LighthouseReport::all();
        return [
            'lighthouse_report_id' => $this->faker->randomElement($array = $uuid->flatten()->pluck('id')->unique()),
            'lighthouse_flag_id' => $this->faker->numberBetween($min = 1, $max = 6),
            'score' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0.6, $max = 1),
            'score_normal' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 0.5),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
