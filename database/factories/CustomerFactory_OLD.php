<?php
use Webpatser\Uuid\Uuid;
use App\MyClasses\Seeders;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    $seeder= new Seeders();
    $now=\Carbon\Carbon::now();


    return [
        'id'=>Uuid::generate(4),
        'name'=>$faker->name(),
        'password'=>bcrypt('secret'),
        'user_id'=>$seeder->field_from_random_record_from_table('User','id'),
        'testing'=>1,
        'created_at'=>$now,
        'updated_at'=>$now,
    ];
});
