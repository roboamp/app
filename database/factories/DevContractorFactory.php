<?php

namespace Database\Factories;

use App\DevContractor;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DevContractorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DevContractor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->email
        ];
    }
}
