<?php

namespace Database\Factories;

use App\PotentialCustomer;
use App\PotentialCustomerEmail;
use Illuminate\Database\Eloquent\Factories\Factory;

class PotentialCustomerEmailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PotentialCustomerEmail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $customers = PotentialCustomer::all();
        $pcustomers = $customers->pluck('id');
        return [
            'email' => $this->faker->email,
            'potential_customer_id' => $this->faker->randomElement($array= $pcustomers),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
