<?php

namespace Database\Factories;

use App\PotentialCustomer;
use Illuminate\Database\Eloquent\Factories\Factory;

class PotentialCustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PotentialCustomer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $domain = $this->faker->domainWord;
        return [
            'name' => $domain,
            'url' => 'https://'.$domain.'.com',
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
