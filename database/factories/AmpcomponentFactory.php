<?php

namespace Database\Factories;

use App\Ampcomponent;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class AmpcomponentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ampcomponent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'description' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'codebase' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'created_at' => now(),
            'updated_at' => now()
    ];
    }
}
