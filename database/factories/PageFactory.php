<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Page::class, function (Faker\Generator $faker) {

    $now=\Carbon\Carbon::now();
    $parent_ids=App\Property::all()->pluck('id');

    $index= $parent_ids[rand(0,count($parent_ids)-1)]."\n";


    return [
        'id'=>Uuid::generate(4),
        'name' => $faker->firstName(),
        'property_id'=>$index,
        'parent_id'=>0,
        'created_at'=>$now,
        'updated_at'=>$now,


    ];
});
