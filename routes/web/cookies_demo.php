<?php
use Illuminate\Support\Facades\Cookie;
Route::get('/consent','DemoCookiesController@consent');
Route::get('/amp-consent','DemoCookiesController@amp_consent');

route::match(['get', 'fetch'],'cookies',function(){
    $data['page_name']="Cookies";
    //$data['endpoint']="127.0.0.1:8000/amp/demos/cookies/favorite";
    return view('demo.cookies_demo',$data);
});
route::match(['get'],'cookies/favorite',function(){
    //returns the value of favorite
   // $value=true;
    $minutes=3600;
    $value =Cookie::get('favorite');

//    if($value==0 || $value =="")$value=false;

    return response()->json($value,200);


});
route::match(['post', 'fetch'],'cookies/favorite',function(\Illuminate\Http\Request $request){

    $minutes=3600;
    $pre_value=Cookie::get('favorite');

    if($pre_value==1 || $pre_value==true){$value=false;}else{$value=true;};

    $cookie = cookie('favorite', $value, $minutes);

//    dd($pre_value,$value,$cookie);
    return response()->json($value." SPIDERMAN",200)->cookie($cookie);
});