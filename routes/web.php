<?php


Route::get('/debug','DebugController@index');


Route::group(['prefix' => 'pic', 'name' => 'pic.'], function () {
    Route::get('/public', 'PicController@public')->name('pic.public');
    Route::get('/pic/{id}', 'PicController@show')->name('pic.show');
    Route::post('/store', 'PicController@store')->name('pic.store');
    Route::post('/pic/{id}', 'PicController@update')->name('pic.update');
    Route::post('/pic/delete/{id}', 'PicController@destroy')->name('pic.destroy');
});




Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function(){

    // Auth::routes();
    //Route::get('dashboard','AdminController@index')->name('dashboard');
    Route::get('/','AdminController@index')->name('dashboard');
    Route::get('/logout', function(){
        return redirect('login')->with(Auth::logout());

    });


    ///TEMPORARY FIX




        /*--- Features Routes ---*/
        Route::group(['prefix' => 'features', 'name' => 'features.'], function () {
            Route::get('/', 'FeatureController@index')->name('features.index');
            Route::get('create', 'FeatureController@create')->name('features.create');
            Route::post('/create', 'FeatureController@store')->name('features.store');
            Route::post('update', 'FeatureController@update')->name('features.update');
            Route::delete('destroy', 'FeatureController@destroy')->name('features.destroy');
        });

//Engagement Calculator
        Route::group(['prefix' => 'calculator', 'name' => 'calculator.'], function () {
            Route::get('/', 'EngagementController@index')->name('calculator.engagement');
            //print the PDF of the engagement
            Route::post('/downloadpdf', 'EngagementController@downloadpdf')->name('calculator.downloadpdf');
        });

    /*--- Speedy Routes ---*/
    Route::group(['prefix' => 'speedy', 'name' => 'speedy.'], function () {
        Route::get('/', 'LighthouseReportController@index')->name('speedy.reports');
        Route::get('show/{id}', 'LighthouseReportController@show')->name('speedy.show');
        Route::get('create', 'LighthouseReportController@create')->name('speedy.create');
        Route::post('/', 'LighthouseReportController@store')->name('speedy.store');
        Route::get('edit/{id}', 'LighthouseReportController@edit')->name('speedy.edit');
        Route::patch('update', 'LighthouseReportController@post')->name('speedy.update');
        Route::delete('destroy', 'LighthouseReportController@destroy')->name('speedy.destroy');

        Route::post('mail', 'LighthouseReportController@send_mail_to')->name('speedy.mailto');
        Route::get('status/change/{report}', 'LighthouseReportController@change_report_status')->name('speedy.status.change');

        Route::group(['prefix' => 'preview', 'name' => 'preview.'], function () {
            Route::get('email/{id}', 'LighthouseReportController@preview_email')->name('speedy.preview.pdf');
        });

    });

    /*---Admin FAQ Routes ---*/
    Route::group(['prefix' => 'list', 'name' => 'faq.'], function () {
        Route::get('/', 'AdminFAQController@index')->name('faq.admin_list');
        Route::get('preview/{privacy?}', 'AdminFAQController@list_preview')->name('faq.preview');
        Route::get('question/{id}', 'AdminFAQController@answer_show')->name('faq.show');
        Route::patch('question/{id}', 'AdminFAQController@answer_store')->name('faq.answer_store');
        Route::delete('question/delete/{id}', 'AdminFAQController@destroy')->name('faq.destroy');
        Route::get('pending', 'AdminFAQController@pending')->name('faq.pending');
        Route::get('reports', 'AdminFAQController@reports')->name('faq.reports');
    });

    /*--- FAQ Routes ---*/
    Route::group(['prefix' => 'faq', 'name' => 'faq.'], function () {
        Route::get('/', 'FAQController@index')->name('faq.index');
        Route::post('/', "FAQController@store")->name('faq.store');
        Route::get('/search', 'FAQController@search')->name('faq.search');
        Route::get('/public', 'FAQController@public_list')->name('faq.public_list');
        Route::get('/public/search', 'FAQController@public_search')->name('faq.public_list_search');
    });


    /*--- Categories Routes ---*/
    Route::group(['prefix' => 'categories', 'name' => 'faq.'], function () {
        Route::get('/', 'CategoriesController@index')->name('category.index');
        Route::get('/category', 'CategoriesController@show')->name('category.show');
        Route::post('/category/store', 'CategoriesController@store')->name('category.store');
        Route::post('/category/{id}', 'CategoriesController@update')->name('category.update');
        Route::post('/category/delete/{id}', 'CategoriesController@destroy')->name('category.destroy');
    });


    /*--- QA Routes ---*/
    Route::group(['prefix' => 'devQA', 'name' => 'devQA.'], function () {
        Route::get('/', 'DevProjectController@index')->name('devQA.index');
        Route::get('/list', 'DevProjectController@list')->name('devQA.list');
        Route::get('/create', 'DevProjectController@create')->name('devQA.create');
        Route::post('/', 'DevProjectController@store')->name('devQA.store');
        Route::get('/{id}', 'DevProjectController@show')->name('devQA.show');
        Route::patch('/update/{id}', 'DevProjectController@update')->name('devQA.update');
        Route::post('/destroy/{id}', 'DevProjectController@destroy')->name('devQA.destroy');
        Route::post('/delete/{id}', 'DevProjectController@destroy_project_pages')->name('devQA.destroy_project_pages');
    });

    /*--- Url Routes ---*/
    Route::group(['prefix' => 'url', 'name' => 'url.'], function () {
        Route::get('/', 'UrlController@index')->name('url.index');
        Route::get('/{id}', 'UrlController@show')->name('url.show');
    });



    /*--- Contractor Routes ---*/
    Route::group(['prefix' => 'contractors', 'name' => 'contractors.'], function () {
        Route::get('/', 'DevContractorController@index')->name('contractors.index');
        Route::get('/create', 'DevContractorController@create')->name('contractors.create');
        Route::post('/', 'DevContractorController@store')->name('contractors.store');
        Route::get('/{id}', 'DevContractorController@show')->name('contractors.show');
        Route::patch('/update/{id}', 'DevContractorController@update')->name('contractors.update');
        Route::post('/destroy/{id}', 'DevContractorController@destroy')->name('contractors.destroy');
    });

    /*--- Activities Routes ---*/
    Route::group(['prefix'=>'activities','name'=>'activities.'],function(){
        Route::get('/', 'ActivityController@index')->name('activities.index');
        Route::get('show', 'ActivityController@show')->name('activities.show');
        Route::get('create', 'ActivityController@create')->name('activities.create');
        Route::post('/create', 'ActivityController@store')->name('activities.store');
        Route::post('edit', 'ActivityController@edit')->name('activities.edit');
        Route::post('update', 'ActivityController@update')->name('activities.update');
        Route::delete('destroy', 'ActivityController@destroy')->name('activities.destroy');
        
    });


    Route::group(['prefix'=>'schedule','name'=>'schedule.'],function(){
        Route::get('/', 'ScheduleController@index')->name('schedule.index');
        Route::get('/create', 'ScheduleController@create')->name('schedule.create');
        Route::post('/', 'ScheduleController@store')->name('schedule.store');
        Route::get('/show/{id}', 'ScheduleController@show')->name('schedule.show');
        Route::get('/edit/{id}', 'ScheduleController@edit')->name('schedule.edit');
        Route::patch('/{id}', 'ScheduleController@update')->name('schedule.update');
        Route::delete('/', 'ScheduleController@destroy')->name('schedule.destroy');

        Route::group(['prefix'=>'activities','name'=>'activities.'],function(){
            Route::post('/command', 'ScheduleController@get_command')->name('schedule.activities.command');
        });

        Route::post('/pageSchedule', 'ScheduleController@page_schedule')->name('schedule.page');

    });

    /*--- Page Pull Schedule Routes ---*/
    Route::group(['prefix' => 'pullSchedule', 'name' => 'pullSchedule.'], function () {
        Route::get('/', 'PullScheduleController@index')->name('pullSchedule.index');
        Route::get('/{id}/show', 'PullScheduleController@show')->name('pullSchedule.show');
        Route::post('/properties', 'PullScheduleController@get_properties')->name('pullSchedule.getProperties');
        Route::post('/pages', 'PullScheduleController@get_pages')->name('pullSchedule.getPages');
    });

    /*--- Price Buckets Routes ---*/
    Route::group(['prefix'=>'buckets','name'=>'buckets.'],function(){
        Route::get('/', 'PriceBucketController@index')->name('buckets.index');
        Route::get('/create', 'PriceBucketController@create')->name('buckets.create');
        Route::post('/', 'PriceBucketController@store')->name('buckets.store');
        //Route::get('/{id}/show', 'PriceBucketController@show')->name('buckets.show');
        //Route::post('/edit', 'PriceBucketController@edit')->name('buckets.edit');
        Route::patch('/edit', 'PriceBucketController@update')->name('buckets.update');
        Route::delete('/', 'PriceBucketController@destroy')->name('buckets.destroy');
        
    });

    /*--- Page Clients Routes ---*/
    Route::group(['prefix'=>'clients','name'=>'clients.'],function(){
        Route::get('/', 'ClientController@index')->name('clients.index');
        Route::get('/create', 'ClientController@create')->name('clients.create');
        Route::post('/', 'ClientController@store')->name('clients.store');
        Route::get('/show/{id}', 'ClientController@show')->name('clients.show');
        Route::post('/edit/{id}', 'ClientController@edit')->name('clients.edit');
        Route::patch('/update', 'ClientController@update')->name('clients.update');
        Route::delete('/', 'ClientController@destroy')->name('clients.destroy');
    });

    /*--- Page Sections Routes ---*/
    Route::group(['prefix'=>'pagesections','name'=>'pagesections.'],function(){
        Route::get('/', 'PageSectionController@index')->name('pagesections.index');
        Route::get('/create', 'PageSectionController@create')->name('pagesections.create');
        Route::post('/', 'PageSectionController@store')->name('pagesections.store');
        Route::get('/{id}/show', 'PageSectionController@show')->name('pagesections.show');
        Route::post('/{id}/edit', 'PageSectionController@edit')->name('pagesections.edit');
        Route::patch('/update', 'PageSectionController@update')->name('pagesections.update');
        Route::delete('/destroy', 'PageSectionController@destroy')->name('pagesections.destroy');
    });

    /*--- Page Tmp Properties Routes ---*/
    Route::group(['prefix'=>'tmpproperties','name'=>'tmpproperties.'],function(){
        Route::get('/', 'TmpPropertyController@index')->name('tmpproperties.index');
        Route::get('/create', 'TmpPropertyController@create')->name('tmpproperties.create');
        Route::post('/', 'TmpPropertyController@store')->name('tmpproperties.store');
        Route::get('/{id}/show', 'TmpPropertyController@show')->name('tmpproperties.show');
        Route::post('/{id}/edit', 'TmpPropertyController@edit')->name('tmpproperties.edit');
        Route::patch('/update', 'TmpPropertyController@update')->name('tmpproperties.update');
        Route::delete('/', 'TmpPropertyController@destroy')->name('tmpproperties.destroy');
    });

    /*--- Usage Reports Routes ---*/
    Route::group(['prefix'=>'usage_reports','name'=>'usage_reports.'],function(){
        Route::get('/', 'UsageReportController@index')->name('usage_reports.index');
        Route::post('/show', 'UsageReportController@show')->name('usage_reports.show');

        Route::post('/sections', 'UsageReportController@getSections')->name('usage_reports.getSections');
        Route::post('/pages', 'UsageReportController@getPages')->name('usage_reports.getPages');
    });


    /*--- Timeline Routes ---*/
    Route::group(['prefix' => 'timeline', 'name' => 'timeline.'], function () {
        Route::get('/list', 'TimelineController@list')->name('timeline.list');
        Route::get('/create', 'TimelineController@create')->name('timeline.create');
        Route::get('/{id}', 'TimelineController@index')->name('timeline.index');
        Route::get('/status/{id}', 'TimelineController@status_preview')->name('timeline.preview');
        Route::post('/', 'TimelineController@store')->name('timeline.store');
        Route::post('/delete/{id}', 'TimelineController@destroy_status_timeline')->name('timeline.destroy_status_timeline');
        Route::get('/{id}/show', 'TimelineController@show')->name('timeline.show');
        Route::patch('/{id}/update', 'TimelineController@update')->name('timeline.update');
        Route::post('/timeline-delete/{id}', 'TimelineController@destroy')->name('timeline.destroy');
    });


    /*--- Statuses Routes ---*/
    Route::group(['prefix' => 'statuses', 'name' => 'status.'], function () {
        Route::get('/', 'StatusController@index')->name('status.index');
        Route::get('/status', 'StatusController@show')->name('status.show');
        Route::post('/status', 'StatusController@store')->name('status.store');
        Route::post('/status/{id}', 'StatusController@update')->name('status.update');
        Route::post('/status/delete/{id}', 'StatusController@destroy')->name('status.destroy');
    });

    /*--- PIC Routes ---*/
    Route::group(['prefix' => 'pic', 'name' => 'pic.'], function () {
        Route::get('/', 'PicController@index')->name('pic.index');
        Route::get('/pic/{id}', 'PicController@show')->name('pic.show');
        Route::post('/store', 'PicController@store')->name('pic.store');
        Route::post('/pic/{id}', 'PicController@update')->name('pic.update');
        Route::post('/pic/delete/{id}', 'PicController@destroy')->name('pic.destroy');
    });

    //////////////////////////////////////////////////END TEMPORARY FIX
    //////////////////////////////////////////////////END TEMPORARY FIX
    //////////////////////////////////////////////////END TEMPORARY FIX
    //////////////////////////////////////////////////END TEMPORARY FIX









    /*--- Page Tmp Properties Routes ---*/
    Route::group(['prefix'=>'usage_reports','name'=>'usage_reports.'],function(){
        Route::get('/', 'UsageReportController@index')->name('usage_reports.index');
        Route::post('/show', 'UsageReportController@show')->name('usage_reports.show');

        Route::post('/sections', 'UsageReportController@getSections')->name('usage_reports.getSections');
        Route::post('/pages', 'UsageReportController@getPages')->name('usage_reports.getPages');
    });

    /*--- Page Tmp Properties Routes ---*/
    Route::group(['prefix'=>'pull-schedule','name'=>'pullSchedule.'],function(){
        Route::get('/', 'PullScheduleController@index')->name('pullSchedule.index');
        Route::get('/show/{id}', 'PullScheduleController@show')->name('pullSchedule.show');
        Route::post('/properties', 'PullScheduleController@get_properties')->name('pullSchedule.getProperties');
        Route::post('/pages', 'PullScheduleController@get_pages')->name('pullSchedule.getPages');
    });





    Route::group(['prefix'=>'properties','name'=>'properties.'],function(){

        Route::get('list','PropertyController@index')->name('properties.list');
        Route::get('create','PropertyController@create')->name('properties.create');
        Route::post('create','PropertyController@store')->name('properties.store');


        Route::group(['prefix'=>'templates','name'=>'template.'],function(){
            Route::get('list','TemplateController@index')->name('properties.templates.list');
            Route::get('create','TemplateController@create')->name('properties.templates.create');
            Route::post('create','TemplateController@store')->name('properties.templates.store');

            //Route::post('show/{id}','TemplateController@store')->name('admin.properties.templates.create');
            //Route::get('edit}','TemplateController@show')->name('admin.properties.templates.create');

        });

        //Route::post('show/{id}','TemplateController@store')->name('admin.properties.templates.create');
        //Route::get('edit}','TemplateController@show')->name('admin.properties.templates.create');

    });




    Route::group(['prefix'=>'partners','name'=>'partners.'],function(){

        Route::name('demo.')->prefix('demo')->group(function () {
            Route::get('/', 'Admin\AdminDemoController@index')->name('index');
            Route::get('show/{id}', 'Admin\AdminDemoController@show')->name('show');
            Route::get('create', 'Admin\AdminDemoController@create')->name('create');
            Route::post('create', 'Admin\AdminDemoController@store')->name('create.post');
            Route::get('edit', 'Admin\AdminDemoController@edit')->name('store');
            Route::post('update', 'Admin\AdminDemoController@post')->name('update');
            Route::post('destroy', 'Admin\AdminDemoController@destroy')->name('destroy');
        });
    });

    /*--- Components Routes ---*/
    Route::group(['prefix'=>'ampcomponents','name'=>'ampcomponent.'],function(){
        Route::get('/', 'AmpComponentController@index')->name('ampcomponents.index');
        Route::get('create', 'AmpComponentController@create')->name('ampcomponents.create');
        Route::post('/create', 'AmpComponentController@store')->name('ampcomponents.store');
        Route::post('edit', 'AmpComponentController@edit')->name('ampcomponents.edit');
        Route::post('update', 'AmpComponentController@update')->name('ampcomponents.update');
        Route::delete('destroy', 'AmpComponentController@destroy')->name('ampcomponents.destroy');

        //Attributes Routes
        Route::group(['prefix'=>'attributes','name'=>'attributes.'],function(){
            Route::post('list', 'AttributeController@get_Attributes')->name('ampcomponents.attributes.list');
            Route::post('destroy', 'AttributeController@destroy')->name('ampcomponents.attributes.destroy');
        });
    });


    /*--- Templates Routes ---*/
    Route::group(['prefix'=>'amptemplates','name'=>'amptemplates.'],function(){
        Route::get('/', 'AmpTemplateController@index')->name('amptemplates.index');
        Route::get('show', 'AmpTemplateController@show')->name('amptemplates.show');
        Route::get('create', 'AmpTemplateController@create')->name('amptemplates.create');
        Route::post('/create', 'AmpTemplateController@store')->name('amptemplates.store');
        Route::post('edit', 'AmpTemplateController@edit')->name('amptemplates.edit');
        Route::post('update', 'AmpTemplateController@update')->name('amptemplates.update');
        Route::delete('destroy', 'AmpTemplateController@destroy')->name('amptemplates.destroy');

        //Get attributes from table amptemplate_attribute
        Route::group(['prefix'=>'attributes','name'=>'attributes.'],function(){
            Route::post('/attributes', 'AmpTemplateController@get_attributes')->name('amptemplates.attributes.index');
            Route::post('/upgrade', 'AmpTemplateController@upgrade_template_attributes')->name('amptemplates.attributes.upgrade');
        });

        //Code generator
        Route::group(['prefix'=>'codes','name'=>'code.'],function(){
            Route::post('code', 'AmpTemplateController@generate_code')->name('amptemplates.code.index');
        });
    });

    /*--- Get Contact Info Web Scrapper ---*/
    Route::group(['prefix'=>'web_scrapper', 'name' => 'web_scrapper.'], function(){
        Route::get('/', 'WebScrapperController@get_contact_info')->name('web_scrapper.get_contact_info');
        Route::post('/', 'WebScrapperController@scrapping_contact_info')->name('web_scrapper.scrapping_contact_info');
        Route::get('/potential-clients', 'WebScrapperController@potential_client_list')->name('web_scrapper.potential_client_list');
    });



    //admin.properties.template.create
    /*
      Route::group(['prefix'=>'properties','name'=>'properties.'],function(){
          Route::group(['prefix'=>'templates','name'=>'template.'],function(){
              Route::get('list','TemplateController@index')->name('properties.templates.list');

              Route::get('create','TemplateController@create')->name('properties.templates.create');
              Route::post('create','TemplateController@store')->name('properties.templates.store');

              //Route::post('show/{id}','TemplateController@store')->name('admin.properties.templates.create');
              //Route::get('edit}','TemplateController@show')->name('admin.properties.templates.create');

          });
      });
    */















//begin-child-node











































});

Route::prefix('admin')->name('admin.')->group(function(){
    Auth::routes();
});


//http verbs common usage
//get reads
//put edits
//post create
//http://127.0.0.1:8000/b5b1ba51-717d-4e4a-afb4-3b4deed70267/https%3A%2F%2Fwww.ioogo.com
//https://parser.roboamp.com/b5b1ba51-717d-4e4a-afb4-3b4deed70267/https%3A%2F%2Fwww.ioogo.com%2Findividuals

/* Demo of AMP cookies */

Route::name('cookies')->prefix('amp/demos')->group(__DIR__ . '/web/cookies_demo.php');




Route::get('/radisson/{page}', 'DemoController@radisson');
Route::get('/vodafone/{page}', 'DemoController@vodafone');
Route::get('/uwell', 'DemoController@uwell');
Route::get('/duo','DemoController@duo');


Route::get('8fc33172-1b47-4eb3-95b9-92e3d4007912/{property}','DemoController@show');


//https://127.0.0.1:8000/b5b1ba51-717d-4e4a-afb4-3b4deed70267/https://blog.ioogo.com/saas-taxable-in-texas
Route::get('{property_id}/{page}/{dev?}', 'RenderController@main')
    ->middleware('serverHeaders')
    ->where('page', '.*');


Route::get('/demo/radisson', function () {
    View::addNamespace('demo',base_path("demo"));
    $my_view = 'demo::radisson.index';
    if (view()->exists($my_view)) {
        return view($my_view);
    } else {
        abort(404,"THIS IS A DEMO PAGE");
    }
});


/*


Route::group(['prefix'=>'admin','name'=>'admin.'],function(){
  //  Auth::routes();
    //Route::get('dashboard','AdminController@index')->name('dashboard');
    //Route::get('/','AdminController@index')->name('dashboard');
    //admin.properties.template.create
    Route::group(['prefix'=>'properties','name'=>'properties.'],function(){
        Route::group(['prefix'=>'templates','name'=>'template.'],function(){
            Route::get('list','TemplateController@index')->name('properties.templates.list');
            Route::get('create','TemplateController@create')->name('properties.templates.create');
            Route::post('create','TemplateController@store')->name('properties.templates.store');

            //Route::post('show/{id}','TemplateController@store')->name('admin.properties.templates.create');
            //Route::get('edit}','TemplateController@show')->name('admin.properties.templates.create');

        });
    });

});*/




Route::name('demo')
    ->prefix('demo')
    ->group(__DIR__ . '/web/demo.php');
//CRUD for demos





//127.0.0.1:8000/b5b1ba51-717d-4e4a-afb4-3b4deed70267/https://blog.ioogo.com
Route::match(['post', 'fetch'], '/amp/email', 'EmailController@post_form');

Route::get('/amp_form', function () {
    return view('amp.amp_form');
});
Route::get('/email', 'EmailController@sender');
Route::get('/demo/{s}', 'DemoController@demo')->name('demo');
Route::post('/email_parser', 'EmailController@parser');

//Route::get('homebody',function(){return view('homebody');});

Route::get('/email_parser', 'EmailController@parser');

Route::prefix('amp4email')->name('amp4email.')->group(function(){
    //triger to submit the email
    Route::get('send','Amp4Email\Examples@send_email')->name('send_email');

    Route::get('asr','Amp4Email\Examples@asr')->name('asr');
    Route::match(['post','fetch','get'],'/asr_lol','Amp4Email\Examples@asr_post');
    Route::match(['post','fetch','get'],'asr_post','Amp4Email\Examples@asr_post')->name('asr_post');
    //hitting this one

    //initial page load
    Route::get('asr/{something?}/{somehing_else?}','Amp4Email\Examples@asr_post');

    Route::prefix('endpoints')->name('endpoints.')->group(function(){
        Route::get('hotels','Amp4Email\Endpoints@get_hotels')->name('hotels');

    });


    //shows the forms
    Route::prefix('examples')->name('examples.')->group(function(){



        Route::prefix('components')->name('components.')->group(function(){
            Route::get('amplist','Amp4Email\Examples@amplist')->name('amplist');
            Route::get('ecommerce/{filter?}','Amp4Email\Endpoints@get_ecommerce')->name('ecommerce');

        });
    });



});



Route::name('demo.')->prefix('templates')->group(function () {
    Route::get('/page/{id?}','DemoController@info')->name('row.info');
});

Route::name('templates.')->prefix('templates')->group(function () {
    Route::get('demo', 'DemoController@demo')->name('demo');
    Route::post('demo', 'DemoController@demo_post')->name('demo.post');
    // Route::get('/page/{id?}','DemoController@info')->name('admin.demos.row.info');
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
