<?php
Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function () {
    /*--- Timeline Routes ---*/
    Route::group(['prefix' => 'timeline', 'name' => 'timeline.'], function () {
        Route::get('/list', 'TimelineController@list')->name('timeline.list');
        Route::get('/create', 'TimelineController@create')->name('timeline.create');
        Route::get('/{id}', 'TimelineController@index')->name('timeline.index');
        Route::get('/status/{id}', 'TimelineController@status_preview')->name('timeline.preview');
        Route::post('/', 'TimelineController@store')->name('timeline.store');
        Route::post('/delete/{id}', 'TimelineController@destroy_status_timeline')->name('timeline.destroy_status_timeline');
        Route::get('/{id}/show', 'TimelineController@show')->name('timeline.show');
        Route::patch('/{id}/update', 'TimelineController@update')->name('timeline.update');
        Route::post('/timeline-delete/{id}', 'TimelineController@destroy')->name('timeline.destroy');
    });


    /*--- Statuses Routes ---*/
    Route::group(['prefix' => 'statuses', 'name' => 'timeline.'], function () {
        Route::get('/', 'StatusController@index')->name('status.index');
        Route::get('/status', 'StatusController@show')->name('status.show');
        Route::post('/status', 'StatusController@store')->name('status.store');
        Route::post('/status/{id}', 'StatusController@update')->name('status.update');
        Route::post('/status/delete/{id}', 'StatusController@destroy')->name('status.destroy');
    });
});