<?php

Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function() {

    /*--- Components Routes ---*/
    Route::group(['prefix'=>'ampcomponents','name'=>'ampcomponent.'],function(){
        Route::get('/', 'AmpComponentController@index')->name('ampcomponents.index');
        Route::get('create', 'AmpComponentController@create')->name('ampcomponents.create');
        Route::post('/create', 'AmpComponentController@store')->name('ampcomponents.store');
        Route::post('edit', 'AmpComponentController@edit')->name('ampcomponents.edit');
        Route::post('update', 'AmpComponentController@update')->name('ampcomponents.update');
        Route::delete('destroy', 'AmpComponentController@destroy')->name('ampcomponents.destroy');

        //Attributes Routes
        Route::group(['prefix'=>'attributes','name'=>'attributes.'],function(){
            Route::post('list', 'AttributeController@get_Attributes')->name('ampcomponents.attributes.list');
            Route::post('destroy', 'AttributeController@destroy')->name('ampcomponents.attributes.destroy');
        });
    });


    /*--- Templates Routes ---*/
    Route::group(['prefix'=>'amptemplates','name'=>'amptemplates.'],function(){
        Route::get('/', 'AmpTemplateController@index')->name('amptemplates.index');
        Route::get('show', 'AmpTemplateController@show')->name('amptemplates.show');
        Route::get('create', 'AmpTemplateController@create')->name('amptemplates.create');
        Route::post('/create', 'AmpTemplateController@store')->name('amptemplates.store');
        Route::post('edit', 'AmpTemplateController@edit')->name('amptemplates.edit');
        Route::post('update', 'AmpTemplateController@update')->name('amptemplates.update');
        Route::delete('destroy', 'AmpTemplateController@destroy')->name('amptemplates.destroy');

        //Get attributes from table amptemplate_attribute
        Route::group(['prefix'=>'attributes','name'=>'attributes.'],function(){
            Route::post('/attributes', 'AmpTemplateController@get_attributes')->name('amptemplates.attributes.index');
            Route::post('/upgrade', 'AmpTemplateController@upgrade_template_attributes')->name('amptemplates.attributes.upgrade');
        });

        //Code generator
        Route::group(['prefix'=>'codes','name'=>'code.'],function(){
            Route::post('code', 'AmpTemplateController@generate_code')->name('amptemplates.code.index');
        });
    });

});