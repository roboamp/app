<?php

Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function() {

    /*--- Get Contact Info Web Scrapper ---*/
    Route::group(['prefix'=>'web_scrapper', 'name' => 'web_scrapper.'], function(){
        Route::get('/', 'WebScrapperController@get_contact_info')->name('web_scrapper.get_contact_info');
        Route::post('/', 'WebScrapperController@scrapping_contact_info')->name('web_scrapper.scrapping_contact_info');
        Route::get('/potential-clients', 'WebScrapperController@potential_client_list')->name('web_scrapper.potential_client_list');
    });

});