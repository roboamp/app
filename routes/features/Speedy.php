<?php

Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function() {

    /*--- Speedy Routes ---*/
    Route::group(['prefix' => 'speedy', 'name' => 'speedy.'], function () {
        Route::get('/', 'LighthouseReportController@index')->name('speedy.reports');
        Route::get('show/{id}', 'LighthouseReportController@show')->name('speedy.show');
        Route::get('create', 'LighthouseReportController@create')->name('speedy.create');
        Route::post('/', 'LighthouseReportController@store')->name('speedy.store');
        Route::get('edit/{id}', 'LighthouseReportController@edit')->name('speedy.edit');
        Route::patch('update', 'LighthouseReportController@post')->name('speedy.update');
        Route::delete('destroy', 'LighthouseReportController@destroy')->name('speedy.destroy');

        Route::post('mail', 'LighthouseReportController@send_mail_to')->name('speedy.mailto');
        Route::get('status/change/{report}', 'LighthouseReportController@change_report_status')->name('speedy.status.change');

        Route::group(['prefix' => 'preview', 'name' => 'preview.'], function () {
            Route::get('email/{id}', 'LighthouseReportController@preview_email')->name('speedy.preview.pdf');
        });

    });

});