<?php

Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function () {

    /*---Admin FAQ Routes ---*/
    Route::group(['prefix' => 'list', 'name' => 'faq.'], function () {
        Route::get('/', 'AdminFAQController@index')->name('faq.admin_list');
        Route::get('preview/{privacy?}', 'AdminFAQController@list_preview')->name('faq.preview');
        Route::get('question/{id}', 'AdminFAQController@answer_show')->name('faq.show');
        Route::patch('question/{id}', 'AdminFAQController@answer_store')->name('faq.answer_store');
        Route::delete('question/{id}/delete', 'AdminFAQController@destroy')->name('faq.destroy');
        Route::get('pending', 'AdminFAQController@pending')->name('faq.pending');
        Route::get('reports', 'AdminFAQController@reports')->name('faq.reports');
    });

    /*--- FAQ Routes ---*/
    Route::group(['prefix' => 'faq', 'name' => 'faq.'], function () {
        Route::get('/', 'FAQController@index')->name('faq.index');
        Route::post('/', "FAQController@store")->name('faq.store');
        Route::get('/search', 'FAQController@search')->name('faq.search');
        Route::get('/public', 'FAQController@public_list')->name('faq.public_list');
        Route::get('/public/search', 'FAQController@public_search')->name('faq.public_list_search');
    });


    /*--- Categories Routes ---*/
    Route::group(['prefix' => 'categories', 'name' => 'faq.'], function () {
        Route::get('/', 'CategoriesController@index')->name('category.index');
        Route::get('/category', 'CategoriesController@show')->name('category.show');
        Route::post('/category', 'CategoriesController@store')->name('category.store');
        Route::post('/category/{id}', 'CategoriesController@update')->name('category.update');
        Route::post('/category/{id}/delete', 'CategoriesController@destroy')->name('category.destroy');
    });

});
