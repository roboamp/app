<?php

Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function() {

    /*--- Activities Routes ---*/
    Route::group(['prefix'=>'activities','name'=>'activities.'],function(){
        Route::get('/', 'ActivityController@index')->name('activities.index');
        Route::get('show', 'ActivityController@show')->name('activities.show');
        Route::get('create', 'ActivityController@create')->name('activities.create');
        Route::post('/create', 'ActivityController@store')->name('activities.store');
        Route::post('edit', 'ActivityController@edit')->name('activities.edit');
        Route::post('update', 'ActivityController@update')->name('activities.update');
        Route::delete('destroy', 'ActivityController@destroy')->name('activities.destroy');
        
    });

    /*--- Schedule Task ---*/
    Route::group(['prefix'=>'schedule','name'=>'schedule.'],function(){
        Route::get('/', 'ScheduleController@index')->name('schedule.index');
        Route::get('/create', 'ScheduleController@create')->name('schedule.create');
        Route::post('/', 'ScheduleController@store')->name('schedule.store');
        Route::get('/show/{id}', 'ScheduleController@show')->name('schedule.show');
        Route::get('/edit/{id}', 'ScheduleController@edit')->name('schedule.edit');
        Route::patch('/{id}', 'ScheduleController@update')->name('schedule.update');
        Route::delete('/', 'ScheduleController@destroy')->name('schedule.destroy');

        Route::group(['prefix'=>'activities','name'=>'activities.'],function(){
            Route::post('/command', 'ScheduleController@get_command')->name('schedule.activities.command');
        });

        Route::post('/pageSchedule', 'ScheduleController@page_schedule')->name('schedule.page');

    });

    /*--- Page Tmp Properties Routes ---*/
    Route::group(['prefix' => 'pullSchedule', 'name' => 'pullSchedule.'], function () {
        Route::get('/', 'PullScheduleController@index')->name('pullSchedule.index');
        Route::get('/{id}/show', 'PullScheduleController@show')->name('pullSchedule.show');
        Route::post('/properties', 'PullScheduleController@get_properties')->name('pullSchedule.getProperties');
        Route::post('/pages', 'PullScheduleController@get_pages')->name('pullSchedule.getPages');
    });


    ///////////////////////
    ///
    ///

});