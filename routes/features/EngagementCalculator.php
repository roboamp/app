<?php

Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function() {

    /*--- Features Routes ---*/
    Route::group(['prefix' => 'features', 'name' => 'features.'], function () {
        Route::get('/', 'FeatureController@index')->name('features.index');
        Route::get('create', 'FeatureController@create')->name('features.create');
        Route::post('/create', 'FeatureController@store')->name('features.store');
        Route::post('update', 'FeatureController@update')->name('features.update');
        Route::delete('destroy', 'FeatureController@destroy')->name('features.destroy');
    });

//Engagement Calculator
    Route::group(['prefix' => 'calculator', 'name' => 'calculator.'], function () {
        Route::get('/', 'EngagementController@index')->name('calculator.engagement');
        //print the PDF of the engagement
        Route::post('/downloadpdf', 'EngagementController@downloadpdf')->name('calculator.downloadpdf');
    });

});