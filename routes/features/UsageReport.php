<?php

Route::name('admin.')->prefix('admin')->middleware(['auth'])->group(function() {
    /*--- Price Buckets Routes ---*/
    Route::group(['prefix'=>'buckets','name'=>'buckets.'],function(){
        Route::get('/', 'PriceBucketController@index')->name('buckets.index');
        Route::get('/create', 'PriceBucketController@create')->name('buckets.create');
        Route::post('/', 'PriceBucketController@store')->name('buckets.store');
        //Route::get('/{id}/show', 'PriceBucketController@show')->name('buckets.show');
        //Route::post('/edit', 'PriceBucketController@edit')->name('buckets.edit');
        Route::patch('/edit', 'PriceBucketController@update')->name('buckets.update');
        Route::delete('/', 'PriceBucketController@destroy')->name('buckets.destroy');
        
    });

    /*--- Page Clients Routes ---*/
    Route::group(['prefix'=>'clients','name'=>'clients.'],function(){
        Route::get('/', 'ClientController@index')->name('clients.index');
        Route::get('/create', 'ClientController@create')->name('clients.create');
        Route::post('/', 'ClientController@store')->name('clients.store');
        Route::get('/show/{id}', 'ClientController@show')->name('clients.show');
        Route::post('/edit/{id}', 'ClientController@edit')->name('clients.edit');
        Route::patch('/update', 'ClientController@update')->name('clients.update');
        Route::delete('/', 'ClientController@destroy')->name('clients.destroy');
    });

    /*--- Page Tmp Properties Routes ---*/
    Route::group(['prefix'=>'tmpproperties','name'=>'tmpproperties.'],function(){
        Route::get('/', 'TmpPropertyController@index')->name('tmpproperties.index');
        Route::get('/create', 'TmpPropertyController@create')->name('tmpproperties.create');
        Route::post('/', 'TmpPropertyController@store')->name('tmpproperties.store');
        Route::get('/{id}/show', 'TmpPropertyController@show')->name('tmpproperties.show');
        Route::post('/{id}/edit', 'TmpPropertyController@edit')->name('tmpproperties.edit');
        Route::patch('/update', 'TmpPropertyController@update')->name('tmpproperties.update');
        Route::delete('/', 'TmpPropertyController@destroy')->name('tmpproperties.destroy');
    });

    /*--- Page Sections Routes ---*/
    Route::group(['prefix'=>'pagesections','name'=>'pagesections.'],function(){
        Route::get('/', 'PageSectionController@index')->name('pagesections.index');
        Route::get('/create', 'PageSectionController@create')->name('pagesections.create');
        Route::post('/', 'PageSectionController@store')->name('pagesections.store');
        Route::get('/{id}/show', 'PageSectionController@show')->name('pagesections.show');
        Route::post('/{id}/edit', 'PageSectionController@edit')->name('pagesections.edit');
        Route::patch('/update', 'PageSectionController@update')->name('pagesections.update');
        Route::delete('/destroy', 'PageSectionController@destroy')->name('pagesections.destroy');
    });

    /*--- Usage Reports Routes ---*/
    Route::group(['prefix'=>'usage_reports','name'=>'usage_reports.'],function(){
        Route::get('/', 'UsageReportController@index')->name('usage_reports.index');
        Route::post('/show', 'UsageReportController@show')->name('usage_reports.show');

        Route::post('/sections', 'UsageReportController@getSections')->name('usage_reports.getSections');
        Route::post('/pages', 'UsageReportController@getPages')->name('usage_reports.getPages');
    });
});