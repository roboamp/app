<!doctype html>
<!--{/*
@info
Generated on: Mon, 27 Aug 2018 12:57:30 GMT
Initiator: ROBOAMP AMP Generator
Generator version: 0.9.6
*/}-->
<html ⚡ lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>Vela Wood | Dallas and Austin Business Lawyers</title>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <script src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js" async="async" custom-element="amp-analytics"></script>
        <script src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async="async" custom-element="amp-sidebar"></script>
        <link rel="canonical" href="https://velawoodlaw.com/">
        <meta name="generator" content="https://generator.rabbit.gomobile.jp">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta name="description" content="Vela Wood | Dallas and Austin Business Lawyers">
        <meta property="og:title" content="Homepage">
        <meta property="og:site_name" content="Vela Wood Law">
        <meta property="og:type" content="blog">
        <meta property="og:url" content="https://velawoodlaw.com/">
        <meta property="og:image" content="http://velawoodlaw.com/wp-content/themes/VK2014/images/mark-logo.png">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@velawoodlaw">
        <meta name="twitter:title" content="Homepage">
        <meta name="twitter:text:description" content="WELCOME TO VELA | WOOD We are a boutique corporate law firm with a local feel and a global impact. We focus our practice in the areas of M&amp;A, Private Equity, Fund Representation, and Venture Transactions.

We …">
        <meta name="twitter:image" content="http://velawoodlaw.com/wp-content/themes/VK2014/images/mark-logo.png">
        <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript data-amp-spec=""><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic%7CMontserrat:400,700">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!--{/* @notice */}-->
        <!--{/* The author's stylesheet specified in the 'style' tag is too long (105092 bytes). Please reduce the size of the stylesheet to be under 50000 bytes. */}-->
        <style amp-custom="">.gform_wrapper {
  margin: 16px 0;
  max-width: 100%
}
.gform_wrapper form {
  text-align: left;
  max-width: 100%;
  margin: 0 auto
}
.gform_wrapper *,
.gform_wrapper :after,
.gform_wrapper :before {
  box-sizing: border-box
}
.gform_wrapper input:not([type=radio]):not([type=checkbox]):not([type=submit]):not([type=button]):not([type=image]):not([type=file]) {
  font-size: inherit;
  font-family: inherit;
  padding: 5px 4px;
  letter-spacing: normal
}
.gform_wrapper ul.gform_fields {
  margin: 0;
  list-style-type: none;
  display: block
}
.gform_wrapper ul {
  text-indent: 0
}
.gform_wrapper form li,
.gform_wrapper li {
  margin-left: 0;
  list-style: none;
  overflow: visible
}
.gform_wrapper ul li.gfield {
  clear: both
}
.gform_wrapper ul li:after,
.gform_wrapper ul li:before,
.gform_wrapper ul.gform_fields {
  padding: 0;
  margin: 0;
  overflow: visible
}
.gform_wrapper .gform_heading {
  width: 100%;
  margin-bottom: 18px
}
.gform_wrapper label.gfield_label {
  font-weight: 700;
  font-size: inherit
}
.gform_wrapper .top_label .gfield_label {
  display: -moz-inline-stack;
  display: inline-block;
  line-height: 1.3;
  clear: both
}
body .gform_wrapper .top_label div.ginput_container {
  margin-top: 8px
}
.gform_wrapper input.medium {
  width: 100%
}
.gform_wrapper span.gform_description {
  font-weight: 400;
  display: block;
  width: calc(100% - 16px);
  margin-bottom: 16px
}
.gform_wrapper .gfield_required {
  color: #790000;
  margin-left: 4px
}
.gform_wrapper .gform_footer {
  padding: 16px 0 10px;
  margin: 16px 0 0;
  clear: both;
  width: 100%
}
.gform_wrapper .gform_footer input.button,
.gform_wrapper .gform_footer input[type=submit] {
  font-size: 1em;
  width: 100%;
  margin: 0 0 16px
}
.gform_wrapper .gform_hidden,
.gform_wrapper input.gform_hidden,
.gform_wrapper input[type=hidden] {
  display: none;
  max-height: 1px;
  overflow: hidden
}
body .gform_wrapper ul li.gfield {
  margin-top: 16px;
  padding-top: 0
}
.gform_wrapper input:not([type=radio]):not([type=checkbox]):not([type=image]):not([type=file]) {
  line-height: 2;
  min-height: 2rem
}
.gform_wrapper.gf_browser_iphone ul li:after,
.gform_wrapper.gf_browser_iphone ul li:before {
  content: none
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 300;
  src: local('Open Sans Light Italic'),local('OpenSans-LightItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hmIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 300;
  src: local('Open Sans Light Italic'),local('OpenSans-LightItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hvIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 300;
  src: local('Open Sans Light Italic'),local('OpenSans-LightItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hnIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 300;
  src: local('Open Sans Light Italic'),local('OpenSans-LightItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hoIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 300;
  src: local('Open Sans Light Italic'),local('OpenSans-LightItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hkIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 300;
  src: local('Open Sans Light Italic'),local('OpenSans-LightItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hlIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 300;
  src: local('Open Sans Light Italic'),local('OpenSans-LightItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWyV9hrIqOxjaPX.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: local('Open Sans Italic'),local('OpenSans-Italic'),url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0Udc1GAK6bt6o.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: local('Open Sans Italic'),local('OpenSans-Italic'),url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0ddc1GAK6bt6o.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: local('Open Sans Italic'),local('OpenSans-Italic'),url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0Vdc1GAK6bt6o.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: local('Open Sans Italic'),local('OpenSans-Italic'),url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0adc1GAK6bt6o.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: local('Open Sans Italic'),local('OpenSans-Italic'),url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0Wdc1GAK6bt6o.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: local('Open Sans Italic'),local('OpenSans-Italic'),url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0Xdc1GAK6bt6o.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 400;
  src: local('Open Sans Italic'),local('OpenSans-Italic'),url(https://fonts.gstatic.com/s/opensans/v15/mem6YaGs126MiZpBA-UFUK0Zdc1GAK6b.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: local('Open Sans SemiBold Italic'),local('OpenSans-SemiBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKXGUdhmIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: local('Open Sans SemiBold Italic'),local('OpenSans-SemiBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKXGUdhvIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: local('Open Sans SemiBold Italic'),local('OpenSans-SemiBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKXGUdhnIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: local('Open Sans SemiBold Italic'),local('OpenSans-SemiBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKXGUdhoIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: local('Open Sans SemiBold Italic'),local('OpenSans-SemiBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKXGUdhkIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: local('Open Sans SemiBold Italic'),local('OpenSans-SemiBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKXGUdhlIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 600;
  src: local('Open Sans SemiBold Italic'),local('OpenSans-SemiBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKXGUdhrIqOxjaPX.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 700;
  src: local('Open Sans Bold Italic'),local('OpenSans-BoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhmIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 700;
  src: local('Open Sans Bold Italic'),local('OpenSans-BoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhvIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 700;
  src: local('Open Sans Bold Italic'),local('OpenSans-BoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhnIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 700;
  src: local('Open Sans Bold Italic'),local('OpenSans-BoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhoIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 700;
  src: local('Open Sans Bold Italic'),local('OpenSans-BoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhkIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 700;
  src: local('Open Sans Bold Italic'),local('OpenSans-BoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhlIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 700;
  src: local('Open Sans Bold Italic'),local('OpenSans-BoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKWiUNhrIqOxjaPX.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 800;
  src: local('Open Sans ExtraBold Italic'),local('OpenSans-ExtraBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKW-U9hmIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 800;
  src: local('Open Sans ExtraBold Italic'),local('OpenSans-ExtraBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKW-U9hvIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 800;
  src: local('Open Sans ExtraBold Italic'),local('OpenSans-ExtraBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKW-U9hnIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 800;
  src: local('Open Sans ExtraBold Italic'),local('OpenSans-ExtraBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKW-U9hoIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 800;
  src: local('Open Sans ExtraBold Italic'),local('OpenSans-ExtraBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKW-U9hkIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 800;
  src: local('Open Sans ExtraBold Italic'),local('OpenSans-ExtraBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKW-U9hlIqOxjaPXZSk.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: italic;
  font-weight: 800;
  src: local('Open Sans ExtraBold Italic'),local('OpenSans-ExtraBoldItalic'),url(https://fonts.gstatic.com/s/opensans/v15/memnYaGs126MiZpBA-UFUKW-U9hrIqOxjaPX.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'),local('OpenSans-Light'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OX-hpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'),local('OpenSans-Light'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OVuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'),local('OpenSans-Light'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OXuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'),local('OpenSans-Light'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OUehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'),local('OpenSans-Light'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OXehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'),local('OpenSans-Light'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OXOhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'),local('OpenSans-Light'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN_r8OUuhpKKSTjw.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans Regular'),local('OpenSans-Regular'),url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWJ0bf8pkAp6a.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans Regular'),local('OpenSans-Regular'),url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFUZ0bf8pkAp6a.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans Regular'),local('OpenSans-Regular'),url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWZ0bf8pkAp6a.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans Regular'),local('OpenSans-Regular'),url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVp0bf8pkAp6a.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans Regular'),local('OpenSans-Regular'),url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWp0bf8pkAp6a.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans Regular'),local('OpenSans-Regular'),url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFW50bf8pkAp6a.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans Regular'),local('OpenSans-Regular'),url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0bf8pkAg.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans SemiBold'),local('OpenSans-SemiBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOX-hpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans SemiBold'),local('OpenSans-SemiBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOVuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans SemiBold'),local('OpenSans-SemiBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOXuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans SemiBold'),local('OpenSans-SemiBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOUehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans SemiBold'),local('OpenSans-SemiBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOXehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans SemiBold'),local('OpenSans-SemiBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOXOhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 600;
  src: local('Open Sans SemiBold'),local('OpenSans-SemiBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UNirkOUuhpKKSTjw.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'),local('OpenSans-Bold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOX-hpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'),local('OpenSans-Bold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOVuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'),local('OpenSans-Bold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOXuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'),local('OpenSans-Bold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOUehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'),local('OpenSans-Bold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOXehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'),local('OpenSans-Bold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOXOhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Bold'),local('OpenSans-Bold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN7rgOUuhpKKSTjw.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans ExtraBold'),local('OpenSans-ExtraBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN8rsOX-hpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans ExtraBold'),local('OpenSans-ExtraBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN8rsOVuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans ExtraBold'),local('OpenSans-ExtraBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN8rsOXuhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans ExtraBold'),local('OpenSans-ExtraBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN8rsOUehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans ExtraBold'),local('OpenSans-ExtraBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN8rsOXehpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans ExtraBold'),local('OpenSans-ExtraBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN8rsOXOhpKKSTj5PW.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 800;
  src: local('Open Sans ExtraBold'),local('OpenSans-ExtraBold'),url(https://fonts.gstatic.com/s/opensans/v15/mem5YaGs126MiZpBA-UN8rsOUuhpKKSTjw.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 400;
  src: local('Playfair Display Italic'),local('PlayfairDisplay-Italic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFkD-vYSZviVYUb_rj3ij__anPXDTnohkk7yRZrPJ-M.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 400;
  src: local('Playfair Display Italic'),local('PlayfairDisplay-Italic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFkD-vYSZviVYUb_rj3ij__anPXDTnojUk7yRZrPJ-M.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 400;
  src: local('Playfair Display Italic'),local('PlayfairDisplay-Italic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFkD-vYSZviVYUb_rj3ij__anPXDTnojEk7yRZrPJ-M.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 400;
  src: local('Playfair Display Italic'),local('PlayfairDisplay-Italic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFkD-vYSZviVYUb_rj3ij__anPXDTnogkk7yRZrPA.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 700;
  src: local('Playfair Display Bold Italic'),local('PlayfairDisplay-BoldItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngOWwu4DRmFqWF_ljR.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 700;
  src: local('Playfair Display Bold Italic'),local('PlayfairDisplay-BoldItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngOWwu6zRmFqWF_ljR.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 700;
  src: local('Playfair Display Bold Italic'),local('PlayfairDisplay-BoldItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngOWwu6jRmFqWF_ljR.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 700;
  src: local('Playfair Display Bold Italic'),local('PlayfairDisplay-BoldItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngOWwu5DRmFqWF_g.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 900;
  src: local('Playfair Display #000 Italic'),local('PlayfairDisplay-BlackItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngAW4u4DRmFqWF_ljR.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 900;
  src: local('Playfair Display #000 Italic'),local('PlayfairDisplay-BlackItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngAW4u6zRmFqWF_ljR.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 900;
  src: local('Playfair Display #000 Italic'),local('PlayfairDisplay-BlackItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngAW4u6jRmFqWF_ljR.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Playfair Display';
  font-style: italic;
  font-weight: 900;
  src: local('Playfair Display #000 Italic'),local('PlayfairDisplay-BlackItalic'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFnD-vYSZviVYUb_rj3ij__anPXDTngAW4u5DRmFqWF_g.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 400;
  src: local('Playfair Display Regular'),local('PlayfairDisplay-Regular'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFiD-vYSZviVYUb_rj3ij__anPXDTjYgEM86xRbPQ.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 400;
  src: local('Playfair Display Regular'),local('PlayfairDisplay-Regular'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFiD-vYSZviVYUb_rj3ij__anPXDTPYgEM86xRbPQ.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 400;
  src: local('Playfair Display Regular'),local('PlayfairDisplay-Regular'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFiD-vYSZviVYUb_rj3ij__anPXDTLYgEM86xRbPQ.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 400;
  src: local('Playfair Display Regular'),local('PlayfairDisplay-Regular'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFiD-vYSZviVYUb_rj3ij__anPXDTzYgEM86xQ.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 700;
  src: local('Playfair Display Bold'),local('PlayfairDisplay-Bold'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lWoe5j5hNKe1_w.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 700;
  src: local('Playfair Display Bold'),local('PlayfairDisplay-Bold'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lWEe5j5hNKe1_w.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 700;
  src: local('Playfair Display Bold'),local('PlayfairDisplay-Bold'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lWAe5j5hNKe1_w.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 700;
  src: local('Playfair Display Bold'),local('PlayfairDisplay-Bold'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBYf9lW4e5j5hNKc.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 900;
  src: local('Playfair Display Black'),local('PlayfairDisplay-Black'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lWoe5j5hNKe1_w.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 900;
  src: local('Playfair Display Black'),local('PlayfairDisplay-Black'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lWEe5j5hNKe1_w.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 900;
  src: local('Playfair Display Black'),local('PlayfairDisplay-Black'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lWAe5j5hNKe1_w.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'Playfair Display';
  font-style: normal;
  font-weight: 900;
  src: local('Playfair Display Black'),local('PlayfairDisplay-Black'),url(https://fonts.gstatic.com/s/playfairdisplay/v13/nuFlD-vYSZviVYUb_rj3ij__anPXBb__lW4e5j5hNKc.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 100;
  src: local('Lato Hairline Italic'),local('Lato-HairlineItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u-w4BMUTPHjxsIPx-mPCLC79U11vU.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 100;
  src: local('Lato Hairline Italic'),local('Lato-HairlineItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u-w4BMUTPHjxsIPx-oPCLC79U1.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 300;
  src: local('Lato Light Italic'),local('Lato-LightItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u_w4BMUTPHjxsI9w2_FQftx9897sxZ.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 300;
  src: local('Lato Light Italic'),local('Lato-LightItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u_w4BMUTPHjxsI9w2_Gwftx9897g.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 400;
  src: local('Lato Italic'),local('Lato-Italic'),url(https://fonts.gstatic.com/s/lato/v14/S6u8w4BMUTPHjxsAUi-qNiXg7eU0.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 400;
  src: local('Lato Italic'),local('Lato-Italic'),url(https://fonts.gstatic.com/s/lato/v14/S6u8w4BMUTPHjxsAXC-qNiXg7Q.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 700;
  src: local('Lato Bold Italic'),local('Lato-BoldItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u_w4BMUTPHjxsI5wq_FQftx9897sxZ.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 700;
  src: local('Lato Bold Italic'),local('Lato-BoldItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u_w4BMUTPHjxsI5wq_Gwftx9897g.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 900;
  src: local('Lato #000 Italic'),local('Lato-BlackItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u_w4BMUTPHjxsI3wi_FQftx9897sxZ.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: italic;
  font-weight: 900;
  src: local('Lato #000 Italic'),local('Lato-BlackItalic'),url(https://fonts.gstatic.com/s/lato/v14/S6u_w4BMUTPHjxsI3wi_Gwftx9897g.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 100;
  src: local('Lato Hairline'),local('Lato-Hairline'),url(https://fonts.gstatic.com/s/lato/v14/S6u8w4BMUTPHh30AUi-qNiXg7eU0.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 100;
  src: local('Lato Hairline'),local('Lato-Hairline'),url(https://fonts.gstatic.com/s/lato/v14/S6u8w4BMUTPHh30AXC-qNiXg7Q.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 300;
  src: local('Lato Light'),local('Lato-Light'),url(https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh7USSwaPGQ3q5d0N7w.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 300;
  src: local('Lato Light'),local('Lato-Light'),url(https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh7USSwiPGQ3q5d0.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'),local('Lato-Regular'),url(https://fonts.gstatic.com/s/lato/v14/S6uyw4BMUTPHjxAwXiWtFCfQ7A.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 400;
  src: local('Lato Regular'),local('Lato-Regular'),url(https://fonts.gstatic.com/s/lato/v14/S6uyw4BMUTPHjx4wXiWtFCc.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 700;
  src: local('Lato Bold'),local('Lato-Bold'),url(https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh6UVSwaPGQ3q5d0N7w.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 700;
  src: local('Lato Bold'),local('Lato-Bold'),url(https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh6UVSwiPGQ3q5d0.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 900;
  src: local('Lato Black'),local('Lato-Black'),url(https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh50XSwaPGQ3q5d0N7w.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Lato;
  font-style: normal;
  font-weight: 900;
  src: local('Lato Black'),local('Lato-Black'),url(https://fonts.gstatic.com/s/lato/v14/S6u9w4BMUTPHh50XSwiPGQ3q5d0.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 400;
  src: local('Montserrat Regular'),local('Montserrat-Regular'),url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459WRhyyTh89ZNpQ.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 400;
  src: local('Montserrat Regular'),local('Montserrat-Regular'),url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459W1hyyTh89ZNpQ.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 400;
  src: local('Montserrat Regular'),local('Montserrat-Regular'),url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459WZhyyTh89ZNpQ.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 400;
  src: local('Montserrat Regular'),local('Montserrat-Regular'),url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459WdhyyTh89ZNpQ.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 400;
  src: local('Montserrat Regular'),local('Montserrat-Regular'),url(https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 700;
  src: local('Montserrat Bold'),local('Montserrat-Bold'),url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gTD_vx3rCubqg.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 700;
  src: local('Montserrat Bold'),local('Montserrat-Bold'),url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3g3D_vx3rCubqg.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 700;
  src: local('Montserrat Bold'),local('Montserrat-Bold'),url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gbD_vx3rCubqg.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 700;
  src: local('Montserrat Bold'),local('Montserrat-Bold'),url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gfD_vx3rCubqg.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Montserrat;
  font-style: normal;
  font-weight: 700;
  src: local('Montserrat Bold'),local('Montserrat-Bold'),url(https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gnD_vx3rCs.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 400;
  src: local('PT Serif Italic'),local('PTSerif-Italic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRTQgYoZZY2vCFuvAFT_rC1cgT9rct48Q.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 400;
  src: local('PT Serif Italic'),local('PTSerif-Italic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRTQgYoZZY2vCFuvAFT_rm1cgT9rct48Q.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 400;
  src: local('PT Serif Italic'),local('PTSerif-Italic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRTQgYoZZY2vCFuvAFT_rO1cgT9rct48Q.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 400;
  src: local('PT Serif Italic'),local('PTSerif-Italic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRTQgYoZZY2vCFuvAFT_r21cgT9rcs.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 700;
  src: local('PT Serif Bold Italic'),local('PTSerif-BoldItalic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRQQgYoZZY2vCFuvAFT9gaQZyTfoOFC-I2irw.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 700;
  src: local('PT Serif Bold Italic'),local('PTSerif-BoldItalic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRQQgYoZZY2vCFuvAFT9gaQZy3foOFC-I2irw.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 700;
  src: local('PT Serif Bold Italic'),local('PTSerif-BoldItalic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRQQgYoZZY2vCFuvAFT9gaQZyffoOFC-I2irw.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'PT Serif';
  font-style: italic;
  font-weight: 700;
  src: local('PT Serif Bold Italic'),local('PTSerif-BoldItalic'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRQQgYoZZY2vCFuvAFT9gaQZynfoOFC-I0.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 400;
  src: local('PT Serif'),local('PTSerif-Regular'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRVQgYoZZY2vCFuvAFbzr-_dSb_nco.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 400;
  src: local('PT Serif'),local('PTSerif-Regular'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRVQgYoZZY2vCFuvAFSzr-_dSb_nco.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 400;
  src: local('PT Serif'),local('PTSerif-Regular'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRVQgYoZZY2vCFuvAFYzr-_dSb_nco.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 400;
  src: local('PT Serif'),local('PTSerif-Regular'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRVQgYoZZY2vCFuvAFWzr-_dSb_.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 700;
  src: local('PT Serif Bold'),local('PTSerif-Bold'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRSQgYoZZY2vCFuvAnt66qfVyvVp8NAyIw.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 700;
  src: local('PT Serif Bold'),local('PTSerif-Bold'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRSQgYoZZY2vCFuvAnt66qWVyvVp8NAyIw.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 700;
  src: local('PT Serif Bold'),local('PTSerif-Bold'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRSQgYoZZY2vCFuvAnt66qcVyvVp8NAyIw.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: 'PT Serif';
  font-style: normal;
  font-weight: 700;
  src: local('PT Serif Bold'),local('PTSerif-Bold'),url(https://fonts.gstatic.com/s/ptserif/v9/EJRSQgYoZZY2vCFuvAnt66qSVyvVp8NA.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Mate;
  font-style: italic;
  font-weight: 400;
  src: local('Mate Italic'),local('Mate-Italic'),url(https://fonts.gstatic.com/s/mate/v6/m8JTjftRd7WZ6z-GWa3WWrZNbg.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Mate;
  font-style: normal;
  font-weight: 400;
  src: local('Mate-Regular'),url(https://fonts.gstatic.com/s/mate/v6/m8JdjftRd7WZ6zq2W6fReLQ.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 100;
  src: local('Roboto Thin Italic'),local('Roboto-ThinItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOiCnqEu92Fr1Mu51QrEz0dL-vwnYh2eg.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 100;
  src: local('Roboto Thin Italic'),local('Roboto-ThinItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOiCnqEu92Fr1Mu51QrEzQdL-vwnYh2eg.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 100;
  src: local('Roboto Thin Italic'),local('Roboto-ThinItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOiCnqEu92Fr1Mu51QrEzwdL-vwnYh2eg.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 100;
  src: local('Roboto Thin Italic'),local('Roboto-ThinItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOiCnqEu92Fr1Mu51QrEzMdL-vwnYh2eg.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 100;
  src: local('Roboto Thin Italic'),local('Roboto-ThinItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOiCnqEu92Fr1Mu51QrEz8dL-vwnYh2eg.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 100;
  src: local('Roboto Thin Italic'),local('Roboto-ThinItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOiCnqEu92Fr1Mu51QrEz4dL-vwnYh2eg.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 100;
  src: local('Roboto Thin Italic'),local('Roboto-ThinItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOiCnqEu92Fr1Mu51QrEzAdL-vwnYg.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 300;
  src: local('Roboto Light Italic'),local('Roboto-LightItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TjASc3CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 300;
  src: local('Roboto Light Italic'),local('Roboto-LightItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TjASc-CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 300;
  src: local('Roboto Light Italic'),local('Roboto-LightItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TjASc2CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 300;
  src: local('Roboto Light Italic'),local('Roboto-LightItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TjASc5CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 300;
  src: local('Roboto Light Italic'),local('Roboto-LightItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TjASc1CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 300;
  src: local('Roboto Light Italic'),local('Roboto-LightItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TjASc0CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 300;
  src: local('Roboto Light Italic'),local('Roboto-LightItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TjASc6CsTYl4BO.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 400;
  src: local('Roboto Italic'),local('Roboto-Italic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1Mu51xFIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 400;
  src: local('Roboto Italic'),local('Roboto-Italic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1Mu51xMIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 400;
  src: local('Roboto Italic'),local('Roboto-Italic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1Mu51xEIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 400;
  src: local('Roboto Italic'),local('Roboto-Italic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1Mu51xLIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 400;
  src: local('Roboto Italic'),local('Roboto-Italic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1Mu51xHIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 400;
  src: local('Roboto Italic'),local('Roboto-Italic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1Mu51xGIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 400;
  src: local('Roboto Italic'),local('Roboto-Italic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1Mu51xIIzIXKMny.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 500;
  src: local('Roboto Medium Italic'),local('Roboto-MediumItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51S7ACc3CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 500;
  src: local('Roboto Medium Italic'),local('Roboto-MediumItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51S7ACc-CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 500;
  src: local('Roboto Medium Italic'),local('Roboto-MediumItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51S7ACc2CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 500;
  src: local('Roboto Medium Italic'),local('Roboto-MediumItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51S7ACc5CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 500;
  src: local('Roboto Medium Italic'),local('Roboto-MediumItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51S7ACc1CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 500;
  src: local('Roboto Medium Italic'),local('Roboto-MediumItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51S7ACc0CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 500;
  src: local('Roboto Medium Italic'),local('Roboto-MediumItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51S7ACc6CsTYl4BO.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 700;
  src: local('Roboto Bold Italic'),local('Roboto-BoldItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TzBic3CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 700;
  src: local('Roboto Bold Italic'),local('Roboto-BoldItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TzBic-CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 700;
  src: local('Roboto Bold Italic'),local('Roboto-BoldItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TzBic2CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 700;
  src: local('Roboto Bold Italic'),local('Roboto-BoldItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TzBic5CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 700;
  src: local('Roboto Bold Italic'),local('Roboto-BoldItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TzBic1CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 700;
  src: local('Roboto Bold Italic'),local('Roboto-BoldItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TzBic0CsTYl4BOQ3o.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: italic;
  font-weight: 700;
  src: local('Roboto Bold Italic'),local('Roboto-BoldItalic'),url(https://fonts.gstatic.com/s/roboto/v18/KFOjCnqEu92Fr1Mu51TzBic6CsTYl4BO.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 100;
  src: local('Roboto Thin'),local('Roboto-Thin'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1MmgVxFIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 100;
  src: local('Roboto Thin'),local('Roboto-Thin'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1MmgVxMIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 100;
  src: local('Roboto Thin'),local('Roboto-Thin'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1MmgVxEIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 100;
  src: local('Roboto Thin'),local('Roboto-Thin'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1MmgVxLIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 100;
  src: local('Roboto Thin'),local('Roboto-Thin'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1MmgVxHIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 100;
  src: local('Roboto Thin'),local('Roboto-Thin'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1MmgVxGIzIXKMnyrYk.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 100;
  src: local('Roboto Thin'),local('Roboto-Thin'),url(https://fonts.gstatic.com/s/roboto/v18/KFOkCnqEu92Fr1MmgVxIIzIXKMny.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 300;
  src: local('Roboto Light'),local('Roboto-Light'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmSU5fCRc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 300;
  src: local('Roboto Light'),local('Roboto-Light'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmSU5fABc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 300;
  src: local('Roboto Light'),local('Roboto-Light'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmSU5fCBc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 300;
  src: local('Roboto Light'),local('Roboto-Light'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmSU5fBxc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 300;
  src: local('Roboto Light'),local('Roboto-Light'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmSU5fCxc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 300;
  src: local('Roboto Light'),local('Roboto-Light'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmSU5fChc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 300;
  src: local('Roboto Light'),local('Roboto-Light'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmSU5fBBc4AMP6lQ.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'),local('Roboto-Regular'),url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu72xKKTU1Kvnz.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'),local('Roboto-Regular'),url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu5mxKKTU1Kvnz.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'),local('Roboto-Regular'),url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7mxKKTU1Kvnz.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'),local('Roboto-Regular'),url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4WxKKTU1Kvnz.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'),local('Roboto-Regular'),url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7WxKKTU1Kvnz.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'),local('Roboto-Regular'),url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKKTU1Kvnz.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 400;
  src: local('Roboto'),local('Roboto-Regular'),url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'),local('Roboto-Medium'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fCRc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'),local('Roboto-Medium'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fABc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'),local('Roboto-Medium'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fCBc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'),local('Roboto-Medium'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBxc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'),local('Roboto-Medium'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fCxc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'),local('Roboto-Medium'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fChc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 500;
  src: local('Roboto Medium'),local('Roboto-Medium'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc4AMP6lQ.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'),local('Roboto-Bold'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfCRc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'),local('Roboto-Bold'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfABc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'),local('Roboto-Bold'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfCBc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+1F00-1FFF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'),local('Roboto-Bold'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBxc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0370-03FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'),local('Roboto-Bold'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfCxc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0102-0103,U+0110-0111,U+1EA0-1EF9,U+20AB
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'),local('Roboto-Bold'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfChc4AMP6lbBP.woff2) format('woff2');
  unicode-range: U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF
}
@font-face {
  font-family: Roboto;
  font-style: normal;
  font-weight: 700;
  src: local('Roboto Bold'),local('Roboto-Bold'),url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4AMP6lQ.woff2) format('woff2');
  unicode-range: U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD
}
button::-moz-focus-inner {
  padding: 0;
  border: 0
}
@font-face {
  font-family: SSSocialCircle;
  src: url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-circle.eot);
  src: url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-circle.eot?#iefix) format('embedded-opentype'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-circle.woff) format('woff'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-circle.ttf) format('truetype'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-circle.svg#SSSocialCircle) format('svg');
  font-weight: 400;
  font-style: normal
}
html:hover [class^=ss-] {
  -ms-zoom: 1
}
[class*=" ss-"]:before,
[class^=ss-]:before {
  font-family: SSSocialCircle;
  font-style: normal;
  font-weight: 400;
  text-decoration: none;
  text-rendering: optimizeLegibility;
  white-space: nowrap;
  -moz-font-feature-settings: "liga=1";
  -moz-font-feature-settings: "liga";
  -ms-font-feature-settings: "liga" 1;
  -o-font-feature-settings: "liga";
  font-feature-settings: "liga";
  -webkit-font-smoothing: antialiased
}
.ss-facebook:before {
  content: ''
}
.ss-twitter:before {
  content: ''
}
.ss-linkedin:before {
  content: ''
}
.ss-instagram:before {
  content: ''
}
.ss-phone:before {
  content: '📞'
}
.ss-mail:before {
  content: '✉'
}
@font-face {
  font-family: SSSocialRegular;
  src: url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-regular.eot);
  src: url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-regular.eot?#iefix) format('embedded-opentype'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-regular.woff) format('woff'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-regular.ttf) format('truetype'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-social-regular.svg#SSSocialRegular) format('svg');
  font-weight: 400;
  font-style: normal
}
html:hover [class^=ss-] {
  -ms-zoom: 1
}
[class*=" ss-"].ss-social-regular:before,
[class*=" ss-"]:before,
[class^=ss-].ss-social-regular:before,
[class^=ss-]:before {
  font-family: SSSocialRegular;
  font-style: normal;
  font-weight: 400;
  text-decoration: none;
  text-rendering: optimizeLegibility;
  white-space: nowrap;
  -moz-font-feature-settings: "liga=1";
  -moz-font-feature-settings: "liga";
  -ms-font-feature-settings: "liga" 1;
  -o-font-feature-settings: "liga";
  font-feature-settings: "liga";
  -webkit-font-smoothing: antialiased
}
.ss-facebook:before {
  content: ''
}
.ss-twitter:before {
  content: ''
}
.ss-linkedin:before {
  content: ''
}
.ss-instagram:before {
  content: ''
}
.ss-phone:before {
  content: '📞'
}
.ss-mail:before {
  content: '✉'
}
@font-face {
  font-family: SSSymbolicons;
  src: url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-symbolicons-block.eot);
  src: url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-symbolicons-block.eot?#iefix) format('embedded-opentype'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-symbolicons-block.woff) format('woff'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-symbolicons-block.ttf) format('truetype'),url(https://velawoodlaw.com/wp-content/themes/VK2014/webfonts/ss-symbolicons-block.svg#SSSymboliconsBlock) format('svg');
  font-weight: 400;
  font-style: normal
}
html:hover [class^=ss-] {
  -ms-zoom: 1
}
[class*=" ss-"].ss-symbolicons-block:before,
[class*=" ss-"]:before,
[class^=ss-].ss-symbolicons-block:before,
[class^=ss-]:before {
  font-family: SSSymbolicons;
  font-style: normal;
  font-weight: 400;
  text-decoration: none;
  text-rendering: optimizeLegibility;
  white-space: nowrap;
  -moz-font-feature-settings: "liga=1";
  -moz-font-feature-settings: "liga";
  -ms-font-feature-settings: "liga" 1;
  -o-font-feature-settings: "liga";
  font-feature-settings: "liga";
  -webkit-font-smoothing: antialiased
}
.ss-phone:before {
  content: '📞'
}
.ss-mail:before {
  content: '✉'
}
.ss-fax:before {
  content: '📠'
}
.clear {
  clear: both;
  display: block;
  overflow: hidden;
  visibility: hidden;
  width: 0;
  height: 0
}
.clearfix:after,
.clearfix:before,
.grid-20:after,
.grid-20:before,
.grid-30:after,
.grid-30:before,
.grid-33:after,
.grid-33:before,
.grid-50:after,
.grid-50:before,
.grid-70:after,
.grid-70:before,
.grid-80:after,
.grid-80:before,
.mobile-grid-100:after,
.mobile-grid-100:before {
  content: ".";
  display: block;
  overflow: hidden;
  visibility: hidden;
  font-size: 0;
  line-height: 0;
  width: 0;
  height: 0
}
.clearfix:after,
.grid-20:after,
.grid-30:after,
.grid-33:after,
.grid-50:after,
.grid-70:after,
.grid-80:after,
.mobile-grid-100:after {
  clear: both
}
.grid-20,
.grid-30,
.grid-33,
.grid-50,
.grid-70,
.grid-80,
.mobile-grid-100 {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  padding-left: 10px;
  padding-right: 10px
}
.grid-parent {
  padding-left: 0;
  padding-right: 0
}
.mobile-grid-100 {
  clear: both;
  width: 100%
}
a,
amp-img,
body,
div,
form,
h1,
h2,
h3,
h4,
html,
i,
iframe,
label,
li,
p,
span,
ul {
  background: 0 0;
  border: 0;
  margin: 0
}
body,
html {
  border: 0;
  margin: 0;
  padding: 0
}
body {
  font: 100%/1.25 arial,helvetica,sans-serif
}
h1,
h2,
h3,
h4 {
  margin: 0;
  padding: 0;
  font-weight: 400
}
h1 {
  padding: 30px 0 25px 0;
  letter-spacing: -1px;
  font: 2em arial,helvetica,sans-serif
}
h2 {
  padding: 20px 0;
  letter-spacing: -1px;
  font: 1.5em arial,helvetica,sans-serif
}
h3 {
  font: 1em arial,helvetica,sans-serif;
  font-weight: 700
}
p,
ul {
  margin: 0;
  padding: 0 0 18px 0
}
ul {
  list-style: none;
  padding: 0 0 0 40px
}
amp-img {
  border: 0
}
a,
a:visited {
  text-decoration: none
}
form {
  margin: 0;
  padding: 0;
  display: inline
}
input {
  font: 1em arial,helvetica,sans-serif
}
input::-moz-focus-inner {
  border: 0;
  padding: 0
}
label {
  cursor: pointer
}
.clear {
  clear: both
}
.clearfix:after {
  content: ".";
  display: block;
  height: 0;
  clear: both;
  visibility: hidden
}
.clearfix {
  display: inline-block
}
.clearfix {
  display: block
}
* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box
}
.central {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
.centered {
  text-align: center
}
.uppercase {
  text-transform: uppercase
}
#container,
body,
html {
  height: 100%;
  width: 100%
}
body > #container {
  height: auto;
  min-height: 100%
}
body {
  width: 100%;
  word-wrap: break-word;
  background: url(https://velawoodlaw.com/wp-content/themes/VK2014/images/bg.png) repeat 0 0
}
body.page-template-homepage-php {
  background: #2a2a2a
}
#photo {
  width: 100%;
  height: 100vh;
  position: relative;
  text-align: center;
  border-bottom: 1px solid #fff
}
#photo .logo {
  width: 100%;
  text-align: center;
  position: absolute;
  top: 35%;
  z-index: 150
}
#photo .logo amp-img {
  width: 90%;
  height: auto;
  max-width: 575px
}
ul#banner-rotator {
  padding: 0;
  position: relative
}
ul#banner-rotator li {
  padding: 0;
  margin: 0;
  position: relative;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  height: 100vh;
  width: 100%;
  border-bottom: 1px solid #fff
}
#nav {
  min-height: 75px;
  width: 100%;
  z-index: 200;
  background: rgba(42,42,42,.8);
  border-bottom: 1px solid #fff;
  position: fixed;
  top: 0;
  left: 0;
  opacity: 1
}
#nav amp-img.logo {
  max-width: 210px;
  height: auto;
  top: -5px
}
#arrow {
  width: 75px;
  height: 85px;
  position: absolute;
  bottom: 0;
  left: 50%;
  margin-left: -37px;
  border-top: 1px solid #fff;
  border-right: 1px solid #fff;
  border-left: 1px solid #fff;
  background: url(https://velawoodlaw.com/wp-content/themes/VK2014/images/arrow.png) no-repeat 0 0;
  display: none
}
#arrow:hover {
  cursor: pointer;
  background: rgba(0,0,0,.4) url(https://velawoodlaw.com/wp-content/themes/VK2014/images/arrow.png) no-repeat 0 0
}
#main {
  width: 100%;
  padding: 0;
  margin: 0 auto
}
#content {
  width: 100%;
  padding-bottom: 40px;
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 14px;
  letter-spacing: 0
}
#footer {
  position: relative;
  width: 100%;
  background: #dfded8;
  padding: 90px 0 20px 0;
  height: auto;
  clear: both
}
#footer .plane {
  width: 100%;
  background: url(https://velawoodlaw.com/wp-content/themes/VK2014/images/plane.png) no-repeat center center;
  height: 230px;
  top: -80px;
  position: absolute
}
#footer-locations {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 14px;
  letter-spacing: 0;
  text-align: center
}
#footer-locations .location-details {
  position: relative
}
.footer-content {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  text-align: center;
  padding-top: 180px;
  height: 100%;
  position: relative
}
.footer-content #newsletter {
  width: 100%;
  text-align: center;
  position: relative
}
.footer-content #copyright {
  width: 100%;
  border-top: 1px solid #c0bbbb;
  padding-top: 20px;
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 12px;
  color: #2a2a2a;
  left: 0;
  text-align: left;
  position: relative
}
h2.newsletter {
  display: inline-block;
  vertical-align: top;
  margin: 0 30px 0 20px;
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 23px;
  text-transform: uppercase;
  color: #2a2a2a
}
h2.newsletter .script {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 23px;
  font-style: italic;
  text-transform: none
}
#gform_wrapper_1 {
  padding-top: 0;
  margin-top: 0;
  width: 320px;
  display: inline-block;
  vertical-align: top;
  position: relative;
  top: -2px;
  height: auto
}
#gform_wrapper_1 form {
  position: relative;
  top: -15px
}
#gform_wrapper_1 ul.gform_fields li {
  margin-top: 0
}
#gform_wrapper_1 label {
  display: none;
  margin: 0
}
#gform_wrapper_1 input[type=text] {
  background: #dfded8;
  border: 1px solid #bfbdbd;
  padding: 6px 15px 6px 6px;
  width: 100%;
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 14px;
  font-style: italic;
  color: #292722
}
#gform_wrapper_1 input[type=text]:focus {
  background: #e3e0d9;
  border: 1px solid #bfbdbd
}
#gform_wrapper_1 ::-webkit-input-placeholder {
  color: #292722
}
#gform_wrapper_1 :-moz-placeholder {
  color: #292722
}
#gform_wrapper_1 ::-moz-placeholder {
  color: #292722
}
#gform_wrapper_1 :-ms-input-placeholder {
  color: #292722
}
#gform_wrapper_1 .gform_heading {
  margin: 0
}
#gform_wrapper_1 .gform_footer {
  margin-top: 0;
  padding-top: 0;
  text-align: center;
  position: absolute;
  width: 25px;
  height: 20px;
  display: inline-block;
  bottom: 27px;
  right: -140px
}
#gform_wrapper_1 .gform_footer input[type=submit] {
  background: url(https://velawoodlaw.com/wp-content/themes/VK2014/images/button-arrow-dark.png) no-repeat 0 0;
  text-align: left;
  text-indent: -9999px;
  width: 25px;
  height: 20px;
  display: :block;
  border: none
}
#gform_wrapper_1 .gform_footer input[type=submit]:hover {
  cursor: pointer;
  opacity: .8
}
h1.section-head {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 36px;
  text-transform: uppercase;
  color: #292722;
  letter-spacing: .03em;
  text-align: center
}
h1.section-head span.script {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 36px;
  text-transform: none;
  font-style: italic
}
#welcome {
  padding: 120px 0 120px 0;
  min-height: 325px;
  width: 100%;
  background: #2a2a2a;
  position: relative
}
#welcome > div {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
#welcome > div {
  color: #fff;
  font-style: italic;
  padding: 10px 90px 10px 90px;
  line-height: 2;
  font-size: 18px;
  text-align: center
}
#welcome > div .uppercase {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 18px;
  font-style: normal
}
#welcome .quote {
  display: block;
  width: 75px;
  height: 75px;
  position: absolute
}
#welcome .quote.open {
  background: url(https://velawoodlaw.com/wp-content/themes/VK2014/images/open-quote.png) no-repeat 0 0;
  top: 0;
  left: 0
}
#welcome .quote.close {
  background: url(https://velawoodlaw.com/wp-content/themes/VK2014/images/close-quote.png) no-repeat 0 0;
  bottom: 0;
  right: 0
}
#practice-areas {
  min-height: 655px;
  width: 100%;
  background: #f2f1eb;
  position: relative;
  background: #f2f1eb url(https://velawoodlaw.com/wp-content/themes/VK2014/images/gray-arrow-top.png) no-repeat top center;
  padding: 90px 0 90px 0;
  text-align: center
}
#practice-areas > div {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
div#services {
  padding: 0;
  margin: 0 0 30px 0
}
div#services .service-item {
  padding: 200px 30px 0 30px;
  margin: 0 10px 0 10px;
  position: relative
}
div#services .service-item h2 {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 16px;
  text-transform: uppercase;
  color: #292722;
  letter-spacing: .03em
}
div#services .service-item:hover a.readmore {
  opacity: .8
}
ul#home-staff {
  width: 100%
}
ul#home-staff li {
  text-align: center;
  padding: 0 50px 0 50px;
  color: #f2f1eb;
  min-height: 580px;
  background: 0 0;
  outline: 0
}
ul#home-staff li h2 {
  color: #f2f1eb;
  font-family: Montserrat,sans-serif;
  font-size: 15px;
  padding: 0 0 4px 0;
  letter-spacing: .2em;
  text-transform: uppercase;
  font-weight: 400
}
ul#home-staff li h3 {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 16px;
  font-style: italic;
  color: #f2f1eb;
  text-transform: none
}
ul#home-staff li a.readmore {
  color: #767572;
  -webkit-opacity: 1;
  -moz-opacity: 1;
  opacity: 1
}
ul#home-staff li ul.social-links li {
  min-height: 0;
  padding: 0
}
ul#home-staff li div.info {
  line-height: 1.5
}
ul#home-staff li div.thumb {
  width: 100%;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  display: inline-block;
  vertical-align: top;
  width: 220px;
  height: 220px;
  background: #cdcbb4;
  border: 2px solid #fff;
  border-radius: 50%;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  margin-bottom: 15px;
  position: relative
}
ul#home-staff li div.thumb .staff-quote {
  position: absolute;
  width: 220px;
  height: 220px;
  background: rgba(0,0,0,.6);
  color: #fff;
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 16px;
  font-style: italic;
  border: 2px solid #fff;
  border-radius: 110px;
  -webkit-border-radius: 110px;
  -moz-border-radius: 110px;
  top: -2px;
  left: -2px;
  display: table;
  opacity: 1
}
ul#home-staff li div.thumb .staff-quote > div {
  text-align: center;
  display: table-cell;
  vertical-align: middle;
  padding: 20px
}
ul.social-links li {
  color: #cdcbb4;
  background: 0 0;
  font-size: 18px;
  padding-left: 0;
  position: relative;
  display: inline-block;
  vertical-align: top;
  margin: 0 4px 0 0
}
ul.social-links li:hover {
  color: #fff
}
ul.social-links li a {
  display: block;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0
}
ul.social-links li.disabled {
  opacity: .5
}
ul.social-links li.disabled:hover {
  color: #cdcbb4
}
#about {
  min-height: 575px;
  width: 100%;
  background: #fff;
  position: relative;
  padding: 0;
  overflow: visible
}
#about > div {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
#about > div {
  overflow: visible;
  position: relative
}
#about h1.section-head {
  margin-bottom: 30px;
  text-align: center;
  display: none
}
ul#about-items {
  width: 100%;
  max-width: 100%;
  min-height: 640px;
  top: -37px;
  left: 0;
  background: url(https://velawoodlaw.com/wp-content/themes/VK2014/images/about-bg.png) no-repeat 0 0;
  position: absolute;
  z-index: 30;
  padding-left: 405px;
  padding-top: 30px
}
ul#about-items li {
  min-height: 195px;
  border-bottom: 1px solid #e4e4e4;
  padding-left: 200px;
  padding-top: 40px;
  line-height: 1.5;
  word-wrap: break-word
}
ul#about-items li h2 {
  color: #292722;
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 20px;
  text-transform: uppercase
}
ul#about-items li:last-child {
  border-bottom: none
}
#our-staff {
  min-height: 790px;
  width: 100%;
  background: #2a2a2a;
  position: relative;
  padding: 100px 0 100px 0;
  text-align: center
}
#our-staff > div {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
#our-staff > div {
  width: 100%;
  max-width: 100%
}
#our-staff h1 {
  color: #fff;
  padding-bottom: 20px
}
#latest {
  min-height: 200px;
  width: 100%;
  background: #fff;
  position: relative;
  height: auto;
  text-align: center;
  padding: 40px 0 40px 0
}
#latest > div {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
#features {
  padding: 0;
  margin: 30px 0 60px 0;
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
#features .feature-item {
  display: inline-block;
  vertical-align: top;
  padding: 0;
  margin: 0 0 20px 0;
  border-right: 3px solid #fff;
  border-left: 3px solid #fff;
  position: relative;
  text-align: center;
  line-height: 1.4
}
#features .feature-item .blog-tease {
  padding: 30px 20px 10px 20px;
  color: #2a2a2a;
  min-height: 300px;
  background: #f2f1eb
}
#features .feature-item .blog-tease:hover {
  background: #e3e2da
}
#features .feature-item span.top-border {
  width: 100%;
  display: block;
  border-top: 1px solid #bdbdbd;
  background: #fff;
  height: 4px
}
#features .feature-item h2 {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 18px;
  line-height: 1.4;
  color: #2a2a2a;
  text-transform: uppercase;
  padding-bottom: 12px
}
#features .feature-item h3 {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 16px;
  text-transform: none;
  color: #2a2a2a;
  font-style: italic;
  padding-bottom: 12px
}
#features .feature-item a.readmore {
  color: #767572
}
a.details {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 14px;
  text-transform: uppercase;
  color: #cdc9b2
}
a.details:hover {
  opacity: .8
}
#tweets {
  min-height: 415px;
  width: 100%;
  background: #f2f1eb;
  position: relative;
  padding: 120px 0 120px 0;
  color: #2a2a2a;
  background: #f2f1eb url(https://velawoodlaw.com/wp-content/themes/VK2014/images/twitter.png) no-repeat center center
}
#tweets > div {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
#tweets > div {
  word-wrap: break-word;
  text-align: center
}
#tweets > div .tweet {
  margin-top: 25px;
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 28px;
  font-style: italic;
  padding: 0 100px 0 100px
}
#tweets > div .tweet a {
  -webkit-opacity: .7;
  -moz-opacity: .7;
  opacity: .7
}
#tweets > div .tweet .home-tweet-date {
  font-size: 18px
}
#contact {
  min-height: 415px;
  width: 100%;
  background: #2a2a2a;
  position: relative;
  text-align: center;
  padding: 140px 0 140px 0
}
#contact > div {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
#contact h1 {
  color: #fff;
  padding-bottom: 36px
}
.location-details {
  padding: 0;
  margin-bottom: 30px;
  text-align: center
}
.location-details span:before {
  margin-right: 12px
}
#main-menu {
  display: inline-block;
  vertical-align: top;
  position: relative;
  text-align: right
}
#nav amp-img.logo {
  width: 100%;
  height: auto;
  margin-top: 12px;
  margin-right: 45px
}
#menu-main-navigation-1,
ul.main-nav {
  display: inline-block;
  vertical-align: top;
  list-style: none;
  padding: 8px 0 0 0;
  position: relative;
  height: 100%
}
#menu-main-navigation-1 li,
ul.main-nav li {
  display: inline-block;
  text-align: center;
  margin: 0 5px 0 5px
}
#menu-main-navigation-1 li a,
ul.main-nav li a {
  padding: 15px 0 25px 0;
  font-family: Montserrat,sans-serif;
  text-transform: uppercase;
  display: block;
  color: #cdcbb4;
  font-size: 14px;
  font-weight: 400;
  position: relative
}
#menu-main-navigation-1 li a:hover,
ul.main-nav li a:hover {
  color: #fff
}
ul#menu-social-navigation,
ul#menu-social-navigation-1,
ul.social-nav {
  display: inline-block;
  vertical-align: top;
  padding: 4px 0 2px 15px;
  margin: 18px 0 0 10px;
  border-left: 1px solid #cdcbb4
}
ul#menu-social-navigation li,
ul#menu-social-navigation-1 li,
ul.social-nav li {
  color: #cdcbb4;
  display: inline-block;
  vertical-align: top;
  vertical-align: middle;
  font-size: 16px;
  position: relative;
  padding: 0;
  margin: 0 3px 0 3px;
  text-shadow: 0 -1px 1px rgba(0,0,0,.6);
  line-height: 1
}
ul#menu-social-navigation li a,
ul#menu-social-navigation-1 li a,
ul.social-nav li a {
  display: block;
  width: 100%;
  height: 100%;
  text-align: left;
  text-indent: -9999px;
  position: absolute;
  top: 0;
  left: 0
}
ul#menu-social-navigation li:hover,
ul#menu-social-navigation-1 li:hover,
ul.social-nav li:hover {
  color: #fff
}
ul#menu-social-navigation-1.footer,
ul.social-nav.footer {
  display: inline-block;
  vertical-align: top;
  padding: 0;
  margin: 0;
  border-left: none
}
ul#menu-social-navigation-1.footer li,
ul.social-nav.footer li {
  font-size: 18px;
  color: #2a2a2a;
  text-shadow: none
}
ul#menu-social-navigation-1.footer li:hover,
ul.social-nav.footer li:hover {
  opacity: .7
}
#filters #searchform.blog ::-webkit-input-placeholder {
  color: #fff;
  -webkit-opacity: 1;
  -moz-opacity: 1;
  opacity: 1
}
#filters #searchform.blog ::-moz-placeholder {
  color: #fff;
  -webkit-opacity: 1;
  -moz-opacity: 1;
  opacity: 1
}
#filters #searchform.blog :-ms-input-placeholder {
  color: #fff;
  -webkit-opacity: 1;
  -moz-opacity: 1;
  opacity: 1
}
#filters #searchform.blog :-moz-placeholder {
  color: #fff;
  -webkit-opacity: 1;
  -moz-opacity: 1;
  opacity: 1
}
#searchform ::-webkit-input-placeholder {
  color: #292722;
  opacity: .8
}
#searchform :-moz-placeholder {
  color: #292722;
  opacity: .8
}
#searchform ::-moz-placeholder {
  color: #292722;
  opacity: .8
}
#searchform :-ms-input-placeholder {
  color: #292722;
  opacity: .8
}
ul {
  list-style: none;
  padding: 10px 0 10px 0;
  margin: 0
}
ul li {
  padding: 0 0 0 5px;
  margin: 0 0 6px 0
}
p {
  padding: 0 0 12px 0
}
ul li ul {
  padding-bottom: 0
}
a:active,
a:link,
a:visited {
  color: #333
}
a:hover {
  color: #111
}
.clear {
  clear: both
}
a.block {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 5
}
a.button,
input[type=submit] {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 20px;
  text-transform: uppercase;
  display: inline-block;
  text-align: center;
  border: 1px solid #848173;
  padding: 10px;
  color: #848173;
  -webkit-opacity: 1;
  -moz-opacity: 1;
  opacity: 1;
  position: relative;
  margin-top: 5px
}
a.button:hover,
input[type=submit]:hover {
  background: rgba(255,255,255,.6)
}
a.button.white {
  color: #fff;
  border: 1px solid #fff
}
a.readmore {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 14px;
  text-transform: uppercase;
  display: inline-block;
  vertical-align: top
}
a.readmore:hover {
  opacity: .8
}
input[type=submit] {
  padding-right: 10px
}
h1 {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 36px;
  text-transform: uppercase;
  color: #292722;
  padding: 0 0 12px 0
}
h2 {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 36px;
  color: #292722;
  padding: 0 0 12px 0;
  letter-spacing: .03em
}
h3 {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 28px;
  color: #292722;
  font-style: italic;
  padding: 0 0 8px 0
}
h4 {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 24px;
  text-transform: uppercase;
  color: #c1b49c;
  padding: 0 0 8px 0;
  letter-spacing: .03em
}
.gform_wrapper {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 16px;
  width: 100%;
  margin-top: 0
}
.gform_wrapper label {
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 16px;
  text-transform: uppercase;
  display: block;
  color: #292722;
  margin-bottom: 8px;
  margin-top: 5px
}
.gform_wrapper input[type=text] {
  background: #f7f7f7;
  border: 1px solid #d3d2cc;
  padding: 8px;
  width: 100%;
  color: #333;
  font-size: 14px
}
.gform_wrapper input[type=text]:focus {
  background: #fcfcfc;
  border: 1px solid #d3d2cc
}
.gform_wrapper ul.gform_fields {
  padding: 0 0 10px 0;
  margin: 0
}
.gform_wrapper ul.gform_fields li {
  padding-left: 0;
  padding-bottom: 12px;
  background: 0 0;
  margin-bottom: 0
}
.gform_footer {
  margin-top: 0;
  padding-top: 0;
  text-align: left
}
.gform_footer input[type=submit] {
  font-family: Lora,serif;
  font-weight: 400;
  font-size: 24px;
  font-style: italic;
  text-transform: none;
  background: 0 0;
  border: none;
  padding: 0
}
.gform_footer input[type=submit]:hover {
  cursor: pointer
}
h2.newsletter {
  margin-bottom: 15px
}
button::-moz-focus-inner {
  padding: 0;
  border: 0
}
* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box
}
.central {
  width: 100%;
  max-width: 1150px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 10px 0 10px;
  position: relative
}
.centered {
  text-align: center
}
.uppercase {
  text-transform: uppercase
}
ul#menu-main-navigation {
  width: 100%;
  background: #555;
  padding: 0;
  margin: 0;
  display: none
}
ul#menu-main-navigation li {
  display: block;
  text-align: center;
  padding: 0;
  margin: 0
}
ul#menu-main-navigation li a {
  display: block;
  width: 100%;
  padding: 15px;
  font-family: Montserrat,sans-serif;
  font-weight: 400;
  font-size: 18px;
  color: #fff;
  text-transform: uppercase;
  border-bottom: 1px solid #bdbdbd
}
a#menu-toggle {
  color: #fff;
  font-size: 24px;
  position: absolute;
  top: 10px;
  right: 10px;
  display: none;
  z-index: 500
}
#menu-social-navigation {
  display: none
}
#about {
  padding: 80px 0 80px 0
}
#about h1.section-head {
  display: block
}
ul#about-items {
  top: 0;
  position: relative;
  background: 0 0;
  padding-left: 0
}
ul#about-items li {
  text-align: center;
  padding: 190px 0 30px 0;
  background-position: top center
}
ul#menu-main-navigation-1 li a {
  font-size: 12px
}
a#menu-toggle {
  display: inline-block
}
#menu-main-navigation-1 {
  display: none
}
#welcome {
  padding-top: 50px;
  padding-bottom: 50px
}
#welcome > div {
  padding: 0 20px 0 20px
}
#welcome span.quote {
  display: none
}
#footer .plane {
  display: none
}
#copyright > div {
  text-align: center
}
#copyright ul#menu-social-navigation-1 {
  margin-top: 20px
}
.location-details {
  text-align: center
}
#photo .logo amp-img {
  max-width: 300px
}
#arrow {
  display: none
}
.tweet {
  padding: 0 40px 0 40px
}
.rbt-inline-0 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/city.jpg) no-repeat center center
}
.rbt-inline-1 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/austin.jpg) no-repeat center center
}
.rbt-inline-2 {
  clear: both
}
.rbt-inline-3 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/corporate-law-180x180.png) no-repeat top center
}
.rbt-inline-4 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/start-ups-new-180x180.png) no-repeat top center
}
.rbt-inline-5 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/05/VW_PracticeAreaIcons_Equity-180x180.png) no-repeat top center
}
.rbt-inline-6 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/04/fantasy-sports-180x180.png) no-repeat top center
}
.rbt-inline-7 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/05/VW_PracticeAreaIcons_Sports-180x180.png) no-repeat top center
}
.rbt-inline-8 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/04/trademarks-180x180.png) no-repeat top center
}
.rbt-inline-9 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/VW_PracticeAreaIcons_RealEstate-180x180.png) no-repeat top center
}
.rbt-inline-10 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/globe-1.png) no-repeat left center
}
.rbt-inline-11 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/graph.png) no-repeat left center
}
.rbt-inline-12 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2014/03/podcast2.png) no-repeat left center
}
.rbt-inline-13 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2016/06/Kevin_Square_Uniform-300x300.jpg) no-repeat center center
}
.rbt-inline-14 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2016/06/Rad_Square_Uniform-300x300.jpg) no-repeat center center
}
.rbt-inline-15 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2016/08/DSC_5519_2-300x300.png) no-repeat center center
}
.rbt-inline-16 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2016/06/Vela-Wood-Lindsey-Altmeyer-Thumbnail-300x300.jpg) no-repeat center center
}
.rbt-inline-17 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2016/06/Lara_Square_Uniform-300x300.jpg) no-repeat center center
}
.rbt-inline-18 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2016/05/Caroline-New-300x300.png) no-repeat center center
}
.rbt-inline-19 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2015/04/Meaghan_Square_Uniform-300x300.jpg) no-repeat center center
}
.rbt-inline-20 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2017/01/DSC_5285-300x300.png) no-repeat center center
}
.rbt-inline-21 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2017/05/DSC_5494_2-300x300.png) no-repeat center center
}
.rbt-inline-22 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2015/04/Jeff_Square_Uniform-300x300.jpg) no-repeat center center
}
.rbt-inline-23 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2016/06/Jenny_Square_Uniform-300x300.jpg) no-repeat center center
}
.rbt-inline-24 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2018/05/ZJ.jpg) no-repeat center center
}
.rbt-inline-25 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2018/08/B.Hart-Headshot-227x300.jpg) no-repeat center center
}
.rbt-inline-26 {
  background: url(https://velawoodlaw.com/wp-content/uploads/2018/08/Lacey-300x300.jpeg) no-repeat center center
}
.rbt-inline-27 {
  margin: 10px
}
#gform_ajax_frame_1 {
  display: none;
  width: 0;
  height: 0
}
.rbt-inline-29 {
  display: inline-block;
  position: relative
}
.rbt-inline-30 {
  display: block
}
.rbt-inline-35 {
  text-align: right
}
#rbt-sidebar {
  width: 80%;
  background: #eee
}
#rbt-sidebar ul li {
  display: block
}
#rbt-sidebar-openner {
  font-size: 22px;
  line-height: 34px;
  color: #000;
  text-align: center;
  background: #fff;
  border: 1px solid #000;
  border-radius: 6px;
  position: absolute;
  top: 10px;
  right: 10px;
  display: block;
  width: 36px;
  height: 36px;
  overflow: hidden;
  cursor: pointer;
  z-index: 10001
}
#menu-main-navigation-1 {
  display: block
}
section.rbt-accordion-section > header {
  background-color: transparent;
  padding: 0;
  margin: 0;
  border: 0
}</style>
    </head>
    <body id="mobile" class="home page-template page-template-homepage page-template-homepage-php page page-id-4 handheld mobile ios iphone safari singular mac sf two-column right-sidebar">
        <amp-sidebar id="rbt-sidebar" layout="nodisplay">
            <div id="main-menu" class="grid-70 mobile-grid-100" data-rbt-accordion-index="0" data-rbt-accordion-section-index="0">
                <ul id="menu-main-navigation-1" class="main-nav">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24"><a href="https://velawoodlaw.com/about/">About</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25"><a href="https://velawoodlaw.com/practice-areas/">Practice Areas</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2964"><a href="https://velawoodlaw.com/startup-central/">Startups</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a href="https://velawoodlaw.com/attorneys/">Attorneys</a></li>
                    <li class="blog-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://velawoodlaw.com/blog/">Blog</a></li>
                    <li class="contact-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://velawoodlaw.com/contact/">Contact</a></li>
                </ul>
                <ul id="menu-social-navigation" class="social-nav">
                    <li id="menu-item-29" class="ss-facebook ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-29"><a target="_blank" href="https://www.facebook.com/pages/Vela-Wood/189659407750086">Facebook</a></li>
                    <li id="menu-item-30" class="ss-twitter ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-30"><a target="_blank" href="https://twitter.com/velawoodlaw">Twitter</a></li>
                    <li id="menu-item-3001" class="ss-instagram ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-3001"><a target="_blank" href="https://www.instagram.com/velawood/">Instagram</a></li>
                    <li id="menu-item-31" class="ss-linkedin ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-31"><a target="_blank" href="https://www.linkedin.com/company/vela-wood">LinkedIn</a></li>
                </ul>
            </div>
        </amp-sidebar>
        <amp-analytics type="googleanalytics" id="rbt-ga-1">
            <script type="application/json">
                {
                    "vars": {
                        "account": "UA-71965919-1"
                    },
                    "triggers": {
                        "trackPageview": {
                            "on": "visible",
                            "request": "pageview"
                        }
                    }
                }
            </script>
        </amp-analytics>

        <nav id="mobile-nav" role="navigation">
            <ul id="menu-main-navigation" class="mobile-slide">
                <li id="menu-item-24" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24"><a href="https://velawoodlaw.com/about/">About</a></li>
                <li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25"><a href="https://velawoodlaw.com/practice-areas/">Practice Areas</a></li>
                <li id="menu-item-2964" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2964"><a href="https://velawoodlaw.com/startup-central/">Startups</a></li>
                <li id="menu-item-26" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a href="https://velawoodlaw.com/attorneys/">Attorneys</a></li>
                <li id="menu-item-27" class="blog-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://velawoodlaw.com/blog/">Blog</a></li>
                <li id="menu-item-28" class="contact-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://velawoodlaw.com/contact/">Contact</a></li>
            </ul>
        </nav>

        <div id="container">
            <div id="main" class="clearfix">
                <div id="nav">
                    <a id="menu-toggle" href="#"><i class="fa fa-bars"></i></a>
                    <div class="central grid-parent">
                        <div class="header-logo grid-30 mobile-grid-100"><span id="rbt-sidebar-openner" on="tap:rbt-sidebar.toggle" role="button" tabindex="-1">☰</span>
                            <a href="https://velawoodlaw.com/">
                                <amp-img src="https://velawoodlaw.com/wp-content/themes/VK2014/images/logo.svg" class="logo" width="381.9" height="88.5" layout="fixed"></amp-img>
                            </a>
                        </div>
                        <!--{/* @notice */}-->
                        <!--{/* The following code was moved to amp-sidebar. */}-->
                        <!-- <div id="main-menu" class="grid-70 mobile-grid-100">
		<ul id="menu-main-navigation-1" class="main-nav"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24"><a href="https://velawoodlaw.com/about/">About</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25"><a href="https://velawoodlaw.com/practice-areas/">Practice Areas</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2964"><a href="https://velawoodlaw.com/startup-central/">Startups</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a href="https://velawoodlaw.com/attorneys/">Attorneys</a></li>
<li class="blog-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a href="https://velawoodlaw.com/blog/">Blog</a></li>
<li class="contact-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="https://velawoodlaw.com/contact/">Contact</a></li>
</ul>        <ul id="menu-social-navigation" class="social-nav"><li id="menu-item-29" class="ss-facebook ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-29"><a target="_blank" href="https://www.facebook.com/pages/Vela-Wood/189659407750086">Facebook</a></li>
<li id="menu-item-30" class="ss-twitter ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-30"><a target="_blank" href="https://twitter.com/velawoodlaw">Twitter</a></li>
<li id="menu-item-3001" class="ss-instagram ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-3001"><a target="_blank" href="https://www.instagram.com/velawood/">Instagram</a></li>
<li id="menu-item-31" class="ss-linkedin ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-31"><a target="_blank" href="https://www.linkedin.com/company/vela-wood">LinkedIn</a></li>
</ul>    </div> -->

                    </div>
                </div>

                <div id="photo">

                    <ul id="banner-rotator">
                        <li class="rbt-inline-0">
                        </li>
                        <li class="rbt-inline-1">
                        </li>
                    </ul>

                    <div id="arrow"></div>
                    <div class="logo">
                        <amp-img src="https://velawoodlaw.com/wp-content/themes/VK2014/images/logo-banner.svg" width="530.1" height="354.6" layout="fixed"></amp-img>
                    </div>

                </div>

                <div id="content" class="homepage">
                    <br class="rbt-inline-2">
                    <div class="section" id="welcome">
                        <div>
                            <span class="quote open"></span>
                            <span class="quote close"></span>
                            <p><span class="uppercase">WELCOME TO VELA | WOOD </span>We are a boutique corporate law firm with a local feel and a global impact. We focus our practice in the areas of M&amp;A, Private Equity, Fund Representation, and Venture Transactions.</p>
                            <p>We provide a full suite of services to our clients. With a flexible billing model that focuses on relationships, not time, we seek to enable our clients to fulfill their personal and professional goals. We thrive on turning an initial consultation into a successful, long-term relationship.</p>
                        </div>
                    </div>

                    <div class="section" id="practice-areas">
                        <div>
                            <h1 class="section-head"><span class="script">Our</span> Practice Areas</h1>
                            <div id="services" class="homepage">
                                <div class="service-item rbt-inline-3">
                                    <a class="block" href="https://velawoodlaw.com/practice-area/ma-corporate-law/"></a>
                                    <h2>M&#038;A &#038; Corporate Law</h2>
                                    <p>Need help restructuring your entity, cleaning up your cap table, or getting your corporate governance in order? It&#8217;s not as hard as it seems. Let us walk you through it. <a href="https://velawoodlaw.com/practice-area/ma-corporate-law/" class="readmore">Read More</a></p>
                                </div>
                                <div class="service-item rbt-inline-4">
                                    <a class="block" href="https://velawoodlaw.com/practice-area/startups/"></a>
                                    <h2>Startups</h2>
                                    <p>VW represents hundreds of startups all over the world. Our vision and goal is to be the best startup firm in Texas, plain and simple. <a href="https://velawoodlaw.com/practice-area/startups/" class="readmore">Read More</a></p>
                                </div>
                                <div class="service-item rbt-inline-5">
                                    <a class="block" href="https://velawoodlaw.com/practice-area/private-equity-venture-capital-funds/"></a>
                                    <h2>Private Equity &#038; Venture Capital Funds</h2>
                                    <p>VW represents all manner of private equity funds, from venture capital to family offices. Fundraising and deploying capital can be complex, but we’re here to make it easy. <a href="https://velawoodlaw.com/practice-area/private-equity-venture-capital-funds/" class="readmore">Read More</a></p>
                                </div>
                                <div class="service-item rbt-inline-6">
                                    <a class="block" href="https://velawoodlaw.com/practice-area/fantasy-sports-and-gaming-law/"></a>
                                    <h2>Fantasy Sports and Gaming Law</h2>
                                    <p>From full season fantasy leagues to daily fantasy sports to eight liners to amusement machines, we&#8217;ve got you covered. <a href="https://velawoodlaw.com/practice-area/fantasy-sports-and-gaming-law/" class="readmore">Read More</a></p>
                                </div>
                                <div class="service-item rbt-inline-7">
                                    <a class="block" href="https://velawoodlaw.com/practice-area/sports-and-entertainment/"></a>
                                    <h2>Sports and Entertainment</h2>
                                    <p>Whether you’re a current or former athlete, or a growing sports startup, we’ve got the playbook you need. <a href="https://velawoodlaw.com/practice-area/sports-and-entertainment/" class="readmore">Read More</a></p>
                                </div>
                                <div class="service-item rbt-inline-8">
                                    <a class="block" href="https://velawoodlaw.com/practice-area/trademarks/"></a>
                                    <h2>Trademarks</h2>
                                    <p>Need help protecting your brand, slogan, or logo? Click here to get started. <a href="https://velawoodlaw.com/practice-area/trademarks/" class="readmore">Read More</a></p>
                                </div>
                                <div class="service-item rbt-inline-9">
                                    <a class="block" href="https://velawoodlaw.com/practice-area/real-estate-law/"></a>
                                    <h2>Commercial Real Estate Law</h2>
                                    <p>Vela Wood has a wealth of experience representing all parties in real estate transactions, including, buyers, sellers, lenders, landlords and investors. <a href="https://velawoodlaw.com/practice-area/real-estate-law/" class="readmore">Read More</a></p>
                                </div>
                            </div>

                            <a class="button" href="https://velawoodlaw.com/practice-areas">View All</a>

                        </div>
                    </div>

                    <div class="section" id="about">
                        <div>
                            <h1 class="section-head"><span class="script">Get to</span> Know Us</h1>
                            <ul id="about-items">
                                <li class="rbt-inline-10">
                                    <h2>See Our Infographics</h2>
                                    <p>Explore our interactive content. You&#8217;ll find our annual graphics depicting our firm at work and at play, a guide to help you navigate your startup&#8217;s lifecycle, and more. <a class="details" title="Annual Reports" href="https://velakeller.wpengine.com/infographics/">View Infographics</a></p>
                                </li>
                                <li class="rbt-inline-11">
                                    <div id="post-title-main">
                                        <h2 class="blogtitle single">Venture Deals in 2016</h2>
                                    </div>
                                    <div class="grid-parent">To provide analysis on early-stage venture financing in North Texas, we created an audit of the capital raises we facilitated in 2016. <a class="details" href="https://velakeller.wpengine.com/report/2016-vw-venture-deals/" target="_blank" rel="shadowbox;width=800;height=450 noopener">See Report</a></div>
                                </li>
                                <li class="rbt-inline-12">
                                    <h2>VW Podcast</h2>
                                    <p>We have a lot to say, so we will be producing podcasts to share information relevant to you and your industry. Learn from our expertise (for free!) <a class="details" title="About" href="https://velakeller.wpengine.com/category/podcasts/">Listen Now</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="section" id="our-staff">
                        <div>
                            <h1 class="section-head"><span class="script">Meet</span> Our Attorneys</h1>

                            <ul id="home-staff">
                                <li id="76">

                                    <div class="thumb rbt-inline-13">
                                        <a class="block" href="https://velawoodlaw.com/attorney/kevin-vela/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;Vela, stop shooting so much.&#8221;</p>
                                                <p class="p1">– Coach Cranfill</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Kevin Vela</h2>
                                        <h3>Partner</h3>
                                        <p>Kevin is the managing partner at Vela Wood. He focuses his practice in the areas of M&#038;A, venture financing, and fund formation. <a href="https://velawoodlaw.com/attorney/kevin-vela/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="http://www.linkedin.com/in/kevinvela" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="https://www.facebook.com/velawood/" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="http://www.twitter.com/velawoodlaw" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:kvela@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="4437">

                                    <div class="thumb rbt-inline-14">
                                        <a class="block" href="https://velawoodlaw.com/attorney/radney-wood/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;The impediment to action advances action. What stands in the way becomes the way.&#8221;<br> — Marcus Aurelius</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Radney Wood</h2>
                                        <h3>Partner</h3>
                                        <p>Radney is a named partner at Vela Wood. He divides his practice between commercial litigation and general corporate matters, concentrating on partnership disputes, corporate governance, venture financing, and contract negotiations. <a href="https://velawoodlaw.com/attorney/radney-wood/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/pub/radney-wood/78/a8/320" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="http://www.facebook.com/velawood" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="https://twitter.com/Wood_Rad" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:rwood@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="2867">

                                    <div class="thumb rbt-inline-15">
                                        <a class="block" href="https://velawoodlaw.com/attorney/carr-staley/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;Be yourself; everyone else is already taken.&#8221;</p>
                                                <p class="p1">– Anonymous</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Carr Staley</h2>
                                        <h3>Partner</h3>
                                        <p>Carr is a partner at Vela Wood. He focuses his practice in the areas of mergers and acquisitions and real estate transactions and investment activities. <a href="https://velawoodlaw.com/attorney/carr-staley/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/carr-staley-3462a0b0?authType=NAME_SEARCH&authToken=jFQn&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A394855398%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1470665102156%2Ctas%3ACarr%20Staley" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="https://www.facebook.com/velawood/" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="https://twitter.com/velawoodlaw" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:cstaley@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="2696">

                                    <div class="thumb rbt-inline-16">
                                        <a class="block" href="https://velawoodlaw.com/attorney/lindsey-altmeyer/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;Don&#8217;t count the things you do, do the things that count.&#8221;</p>
                                                <p class="p1">– Zig Ziglar</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Lindsey Altmeyer</h2>
                                        <h3>Associate</h3>
                                        <p>Lindsey is an associate at Vela Wood. She focuses her practice in the areas of venture financing, securities exemptions, and general corporate governance. <a href="https://velawoodlaw.com/attorney/lindsey-altmeyer/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="http://www.linkedin.com/in/lkaltmeyer" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="http://www.facebook.com/linkateray" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="http://twitter.com/LRay113" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:laltmeyer@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="1722">

                                    <div class="thumb rbt-inline-17">
                                        <a class="block" href="https://velawoodlaw.com/attorney/lara-manor/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>Give them what they never knew they wanted. – Diana Vreeland</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Lara Bubalo Manor</h2>
                                        <h3>Senior Associate</h3>
                                        <p>Lara is a senior associate at Vela Wood. She focuses her practice in the areas of mergers and acquisitions, real estate transactions, and private placements. <a href="https://velawoodlaw.com/attorney/lara-manor/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/pub/lara-bubalo-manor/a3/241/28a" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="http://www.facebook.com/velawood" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="http://www.twitter.com/larabubalomanor" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:lmanor@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="2568">

                                    <div class="thumb rbt-inline-18">
                                        <a class="block" href="https://velawoodlaw.com/attorney/caroline-fabacher/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;Learn from others whom have walked the path before you, but be smart enough to know when to cut your own trail&#8221;</p>
                                                <p class="p1">– Narciso Rodriguez</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Caroline R. Fabacher</h2>
                                        <h3>Associate</h3>
                                        <p>Caroline is an associate at Vela Wood. She focuses her practice in the areas of entity formation, capital raises, corporate governance, and daily fantasy sports regulations. <a href="https://velawoodlaw.com/attorney/caroline-fabacher/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/carolinerfabacher" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="http://www.facebook.com/velawood" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="http://www.twitter.com/crfabacher" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:cfabacher@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="2035">

                                    <div class="thumb rbt-inline-19">
                                        <a class="block" href="https://velawoodlaw.com/attorney/meaghan-johnston/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;You can&#8217;t be that kid standing at the top of the waterslide, overthinking it. You have to go down the chute.&#8221;</p>
                                                <p class="p1">– Tina Fey</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Meaghan Johnston</h2>
                                        <h3>Senior Associate</h3>
                                        <p>Meaghan is a senior associate at Vela Wood. She focuses her practice in the areas of mergers and acquisitions, private placements, corporate governance, and incentive plans. <a href="https://velawoodlaw.com/attorney/meaghan-johnston/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/profile/view?id=46658303&authType=NAME_SEARCH&authToken=xwHD&locale=en_US&srchid=4059511221429556794971&srchindex=1&srchtotal=2&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A4059511221429556794971%2CVSRPtargetId%3A46658303%2CVSRPcmpt%3Aprimary%2CVSRPnm%3Atrue" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="https://www.facebook.com/meaghan.goldner?fref=ts" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="http://www.twitter.com/meaghanjohnst1" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:mjohnston@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="4029">

                                    <div class="thumb rbt-inline-20">
                                        <a class="block" href="https://velawoodlaw.com/attorney/nic-obrien/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;A lot of people give up just before they&#8217;re about to make it. You never know when that next obstacle is going to be the last one.&#8221; – Chuck Norris</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Nic O&#8217;Brien</h2>
                                        <h3>Associate</h3>
                                        <p>Nic is an associate at Vela Wood. He focuses his practice in the areas of venture financings, mergers and acquisitions, commercial and technology transactions, and general corporate governance. <a href="https://velawoodlaw.com/attorney/nic-obrien/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/nic-o-brien-742bb7135?authType=name&authToken=0_tp&trk=mirror-profile-memberlist-photo" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular disabled"></li>
                                            <li class="ss-twitter ss-social-regular disabled"></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:nobrien@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="4267">

                                    <div class="thumb rbt-inline-21">
                                        <a class="block" href="https://velawoodlaw.com/attorney/juan-ramos/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;Do it well, and do it fast.&#8221;</p>
                                                <p>– Juan Ramos Baeza, Dad</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Juan Ramos</h2>
                                        <h3>Associate</h3>
                                        <p>Juan is an associate at Vela Wood. He focuses his practice in the areas of fund formation, M&#038;A, real estate transactions, contract negotiation, and dispute resolution. <a href="https://velawoodlaw.com/attorney/juan-ramos/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/juanalframos/" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="http://www.facebook.com/velawood/" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="https://twitter.com/juanalframos" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:jramos@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="2030">

                                    <div class="thumb rbt-inline-22">
                                        <a class="block" href="https://velawoodlaw.com/attorney/jeff-villalobos/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;When something is important enough, you do it even if the odds are not in your favor.&#8221;</p>
                                                <p class="p1">– Elon Musk</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Jeff Villalobos</h2>
                                        <h3>Associate</h3>
                                        <p>Jeff is an associate at Vela Wood. He focuses his practice in the areas of intellectual property, technology commercialization, privacy, advertising, and corporate transactional matters. <a href="https://velawoodlaw.com/attorney/jeff-villalobos/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/jeff-villalobos/" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="http://www.facebook.com/velawood/" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="http://www/twitter.com/velawoodlaw" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:jvillalobos@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="2649">

                                    <div class="thumb rbt-inline-23">
                                        <a class="block" href="https://velawoodlaw.com/attorney/jenny-young/"></a>
                                        <div class="staff-quote">
                                            <div>
                                                <p>&#8220;Life shrinks or expands in proportion to one’s courage.&#8221;</p>
                                                <p class="p1">– Anais Nin</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Jenny Young</h2>
                                        <h3>Senior Associate</h3>
                                        <p>Jenny is a senior associate at Vela Wood. She focuses her practice on employment law and general corporate matters. <a href="https://velawoodlaw.com/attorney/jenny-young/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="http://www.linkedin.com/company/vela-wood" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="http://www.facebook.com/velawood" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="http://www.twitter.com/velawoodlaw" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:jyoung@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="4969">

                                    <div class="thumb rbt-inline-24">
                                        <a class="block" href="https://velawoodlaw.com/attorney/zach-jones/"></a>
                                        <div class="staff-quote">
                                            <div></div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Zach Jones</h2>
                                        <h3>Senior Associate</h3>
                                        <p>Zach is a senior associate at Vela Wood. He focuses his practice on tax disputes with government authorities and complicated business transactions. <a href="https://velawoodlaw.com/attorney/zach-jones/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/zacharytoddjones/" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular"><a href="https://www.facebook.com/velawood/?ref=aymt_homepage_panel" target="_blank"></a></li>
                                            <li class="ss-twitter ss-social-regular"><a href="https://twitter.com/VelaWoodLaw" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:zjones@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="5028">

                                    <div class="thumb rbt-inline-25">
                                        <a class="block" href="https://velawoodlaw.com/attorney/blake-hart/"></a>
                                        <div class="staff-quote">
                                            <div></div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Blake Hart</h2>
                                        <h3>Of Counsel</h3>
                                        <p>Blake is Of Counsel at Vela Wood. She focuses her practice on contract drafting, review, and negotiation on behalf of sports and entertainment individuals and enterprises. <a href="https://velawoodlaw.com/attorney/blake-hart/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/blakehartutlaw/" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular disabled"></li>
                                            <li class="ss-twitter ss-social-regular disabled"></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:bhart@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                                <li id="5032">

                                    <div class="thumb rbt-inline-26">
                                        <a class="block" href="https://velawoodlaw.com/attorney/lacey-shrum/"></a>
                                        <div class="staff-quote">
                                            <div></div>
                                        </div>
                                    </div>

                                    <div class="info">
                                        <h2>Lacey Shrum</h2>
                                        <h3>Senior Counsel</h3>
                                        <p>Lacey is Senior Counsel at Vela Wood. She implements blockchain and smart contract solutions in securities compliance. <a href="https://velawoodlaw.com/attorney/lacey-shrum/" class="readmore">Read More</a></p>
                                        <ul class="social-links">
                                            <li class="ss-linkedin ss-social-regular"><a href="https://www.linkedin.com/in/lacey-shrum-17595284/" target="_blank"></a></li>
                                            <li class="ss-facebook ss-social-regular disabled"></li>
                                            <li class="ss-twitter ss-social-regular"><a href="https://twitter.com/lacey_shrum" target="_blank"></a></li>

                                            <li class="ss-mail ss-social-regular"><a href="mailto:lshrum@velawoodlaw.com"></a></li>

                                        </ul>
                                    </div>
                                </li>

                            </ul>

                            <a class="button" href="https://velawoodlaw.com/attorneys">View All</a>
                        </div>
                        <div id="slider-next"></div>
                        <div id="slider-prev"></div>
                    </div>

                    <div class="section" id="latest">
                        <div>
                            <h1 class="section-head"><span class="script">Latest</span> Blog Posts</h1>

                            <div id="features" class="grid-parent">

                                <div class="grid-33 mobile-grid-100 feature-item">

                                    <span class="top-border"></span>
                                    <div class="blog-tease">
                                        <h2>How to Angel Invest: What is an Accredited Investor?</h2>
                                        <h3>August 20, 2018</h3>
                                        <p>Who can play the game Angel Investors are individuals who provide seed or startup financing to entrepreneurs. If you give your little brother a few hundred dollars to start a business, technically, you’re an angel investor. However, the more intricate &hellip; <a href="https://velawoodlaw.com/how-to-angel-invest-what-is-an-accredited-investor/" class="readmore">Read More</a></p>
                                    </div>

                                </div>

                                <div class="grid-33 mobile-grid-100 feature-item">

                                    <span class="top-border"></span>
                                    <div class="blog-tease">
                                        <h2>How to Angel Invest</h2>
                                        <h3>August 20, 2018</h3>
                                        <p>You’ve noticed it everywhere. Whether you see Mr. Wonderful grilling someone on Shark Tank, hear about Kevin Durant building a tech empire and basketball dynasty in California, or read about George Clooney selling a Tequila brand for $1 billion, everyone &hellip; <a href="https://velawoodlaw.com/how-to-angel-invest/" class="readmore">Read More</a></p>
                                    </div>

                                </div>

                                <div class="grid-33 mobile-grid-100 feature-item">

                                    <span class="top-border"></span>
                                    <div class="blog-tease">
                                        <h2>Understanding Basic Contracts: The Payment Provision</h2>
                                        <h3>August 16, 2018</h3>
                                        <p>I know, it seems like a simple contractual provision. It can be, but a lack of detail in the payment section of a contract can be disastrous. Take the following provision for example: Payment: &#8220;Contractor shall be paid $40,000 for &hellip; <a href="https://velawoodlaw.com/understanding-basic-contracts-the-payment-provision/" class="readmore">Read More</a></p>
                                    </div>

                                </div>

                                <br class="clear">
                            </div>

                            <a class="button rbt-inline-27" href="https://velawoodlaw.com/blog">View All Blog Posts</a>

                        </div>
                    </div>

                    <div class="section" id="tweets">
                        <div>
                            <h1 class="section-head"><span class="script">Recent</span> Tweets</h1>
                            <div class="tweet">
                                <p>Shoutout to our client <a href="http://twitter.com/mattalexand" target="_blank">@mattalexand</a> of <a href="http://twitter.com/nbhdgoods" target="_blank">@nbhdgoods</a>!
                                    <a href="https://t.co/ARsD2IjMII" target="_blank">https://t.co/ARsD2IjMII</a><br><span class="home-tweet-date"><a href="http://twitter.com/VelaWoodLaw">7:54 PM Aug 23rd</a></span></p>
                            </div>
                            <a href="https://twitter.com/VelaWoodLaw" target="_blank" class="button">All Tweets</a>
                        </div>
                    </div>

                    <div class="section" id="contact">
                        <div>
                            <h1 class="section-head"><span class="script">Let's</span> Get to Work</h1>
                            <a class="button white" href="https://velawoodlaw.com/contact">Contact Us</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div id="footer">

            <div class="plane">
            </div>

            <div class="footer-content">
                <div id="newsletter">
                    <h2 class="newsletter"><span class="script">Sign Up</span> for Our Newsletter</h2>
                    <div class="gf_browser_safari gf_browser_iphone gform_wrapper" id="gform_wrapper_1"><a id="gf_1" class="gform_anchor"></a>
                        <!--{/* @notice */}-->
                        <!--{/* form[method=POST] is not supported in AMP Generator. */}-->
                        <!-- <form method="post" enctype="multipart/form-data" target="gform_ajax_frame_1" id="gform_1" action="/#gf_1">
                        <div class="gform_heading">
                            <span class="gform_description"></span>
                        </div>
                        <div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_1" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_1">Email Address<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_email">
                            <input name="input_1" id="input_1_1" type="text" value="" class="medium" aria-required="true" aria-invalid="false">
                        </div></li>
                            </ul></div>
        <div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" onclick="if(window['gf_submitting_1']){return false;}  window['gf_submitting_1']=true;  " onkeypress="if( event.keyCode == 13 ){ if(window['gf_submitting_1']){return false;} window['gf_submitting_1']=true;  jQuery('#gform_1').trigger('submit',[true]); }"> <input type="hidden" name="gform_ajax" value="form_id=1&amp;title=&amp;description=1&amp;tabindex=1">
            <input type="hidden" class="gform_hidden" name="is_submit_1" value="1">
            <input type="hidden" class="gform_hidden" name="gform_submit" value="1">
            
            <input type="hidden" class="gform_hidden" name="gform_unique_id" value="">
            <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsImQyMzgyOTE4NWJmOTQ3YjA0MzExODc4ZTE5MTM0N2RlIl0=">
            <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0">
            <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1">
            <input type="hidden" name="gform_field_values" value="">
            
        </div>
                        </form> -->
                    </div>
                    <!--{/* @notice */}-->
                    <!--{/* amp-iframe elements with non–HTTPS src URL are not supported. */}-->
                    <!-- <iframe style="display:none;width:0px;height:0px;" src="about:blank" name="gform_ajax_frame_1" id="gform_ajax_frame_1">This iframe contains the logic required to handle Ajax powered Gravity Forms.</iframe> -->
                </div>

                <div id="footer-locations" class="grid-parent">

                    <div class="location-details grid-50 mobile-grid-100">

                        <div class="centered rbt-inline-29">
                            <a class="block" href="https://velawoodlaw.com/location/dallas/"></a>
                            <h4>Dallas</h4>
                            5307 E. Mockingbird Ln.<br> Suite 802<br> Dallas, TX 75206 <span class="ss-symbolicons-block ss-phone rbt-inline-30">(214) 821-2300</span> <span class="ss-symbolicons-block ss-fax rbt-inline-30">(214) 821-2844</span>
                        </div>

                    </div>

                    <div class="location-details grid-50 mobile-grid-100">

                        <div class="centered rbt-inline-29">
                            <a class="block" href="https://velawoodlaw.com/location/austin/"></a>
                            <h4>Austin</h4>
                            500 W 2nd St., <br> Suite 1900<br> Austin, TX 78701 <span class="ss-symbolicons-block ss-phone rbt-inline-30">(512) 813-7300</span> <span class="ss-symbolicons-block ss-fax rbt-inline-30">(512) 893-7363</span>
                        </div>

                    </div>

                    <br class="clear">
                </div>

                <div id="copyright" class="grid-parent">
                    <div class="grid-80 mobile-grid-100">&copy;2018 Vela Wood&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;All Rights Reserved&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Site By <a href="http://www.creativepickle.com/" target="_blank">CreativePickle</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://velawoodlaw.com/servicios">Lea acerca de nuestros servicios en español</a></div>
                    <div class="grid-20 mobile-grid-100 rbt-inline-35">
                        <ul id="menu-social-navigation-1" class="social-nav footer">
                            <li class="ss-facebook ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-29"><a target="_blank" href="https://www.facebook.com/pages/Vela-Wood/189659407750086">Facebook</a></li>
                            <li class="ss-twitter ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-30"><a target="_blank" href="https://twitter.com/velawoodlaw">Twitter</a></li>
                            <li class="ss-instagram ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-3001"><a target="_blank" href="https://www.instagram.com/velawood/">Instagram</a></li>
                            <li class="ss-linkedin ss-social-regular menu-item menu-item-type-custom menu-item-object-custom menu-item-31"><a target="_blank" href="https://www.linkedin.com/company/vela-wood">LinkedIn</a></li>
                        </ul>
                    </div><br class="clear">
                </div>
            </div>
        </div>

    </body>
</html>