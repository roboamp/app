<?php
$post_form_url='form.post';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>ROBOAMP - {{$form_title}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('Tools/Parser')}}/images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--========================================={{asset('Tools/Parser')}}/======================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/css/util.css">
    <link rel="stylesheet" type="text/css" href="{{asset('Tools/Parser')}}/css/main.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!--===============================================================================================-->

</head>
<body>


<div class="container-contact100">

    <div class="wrap-contact100">
        <form class="contact100-form validate-form"  method="post" action="{{route('customer.parser.'.$post_form_url)}}" >
            @csrf
            <span class="contact100-form-title">
					ROBOAMP - {{$form_title}}
                    <div>@include('flash::message')</div>


            </span>
            @if(isset($parsed_url))
                @if(!is_null($parsed_url))
                    {!! $parsed_url!!}
                @endif
            @endif

            @if(isset($login))



            <div class="wrap-input100 validate-input" data-validate = "Please enter your email: username@provider">
                <input class="input100" type="text" name="email" placeholder="E-mail">
                <span class="focus-input100"></span>
            </div>

            <div class="wrap-input100 validate-input" data-validate = "Please enter your Password">
                <input class="input100" type="password" name="password" placeholder="Password">
                <span class="focus-input100"></span>
            </div>
            @endif

            <div class="wrap-input100 validate-input" data-validate = "Please enter URLs">
                <textarea class="input100" name="urls" placeholder="{{$text_area_placeholder}}"></textarea>
                <span class="focus-input100"></span>
            </div>

            <div class="container-contact100-form-btn">
                <button class="contact100-form-btn">
						<span>
							<i class="fa fa-paper-plane-o m-r-6" aria-hidden="true"></i>
							Send
						</span>
                </button>
            </div>
        </form>
        <div id="dropDownSelect1"></div>


    </div>
</div>



<!--===============================================================================================-->
<script src="{{asset('Tools/Parser')}}/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="{{asset('Tools/Parser')}}/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="{{asset('Tools/Parser')}}/vendor/bootstrap/js/popper.js"></script>
<script src="{{asset('Tools/Parser')}}/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="{{asset('Tools/Parser')}}/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="{{asset('Tools/Parser')}}/vendor/daterangepicker/moment.min.js"></script>
<script src="{{asset('Tools/Parser')}}/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="{{asset('Tools/Parser')}}/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="{{asset('Tools/Parser')}}/js/main.js"></script>
<script src="//code.jquery.com/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
    $('#flash-overlay-modal').modal();

</script>
<script>
    $('div.alert').not('.alert-important').delay(5000).fadeOut(350);
</script>

</body>
</html>
