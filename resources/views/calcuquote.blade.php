<!doctype html>
<!--{/*
@info
Generated on: Wed, 11 Jul 2018 19:52:35 GMT
Initiator: ROBOAMP AMP Generator
Generator version: 0.9.6
*/}-->
<html ⚡ lang="en">
    <head>
        <meta charset="utf-8">
        <title>CalcuQuote - Made for EMS - Home</title>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <script src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async="async" custom-element="amp-accordion"></script>
        <link rel="canonical" href="https://calcuquote.com/">
        <meta name="generator" content="https://generator.rabbit.gomobile.jp">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta name="keywords" content="CalcuQuote RFQ Electronics Manufacturing Quotes">
        <meta name="description" content="CalcuQuote - RFQs management system for PCBA">
        <meta name="author" content="CalcuQuote">
        <meta name="keywords" content="CalcuQuote, PCBA, Circuit Board, Assembly, RFQ, Request for Quote, Electronics Assembly, PCB Assembly">
        <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript data-amp-spec=""><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CLato">
        <style amp-custom="">html {
  font-family: sans-serif;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%
}
body {
  margin: 0
}
footer,
header,
section {
  display: block
}
a {
  background-color: transparent
}
a:active,
a:hover {
  outline: 0
}
strong {
  font-weight: 700
}
h1 {
  margin: .67em 0;
  font-size: 2em
}
amp-img {
  border: 0
}
hr {
  height: 0;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box
}
button::-moz-focus-inner,
input::-moz-focus-inner {
  padding: 0;
  border: 0
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  height: auto
}
input[type=search]::-webkit-search-cancel-button,
input[type=search]::-webkit-search-decoration {
  -webkit-appearance: none
}
@font-face {
  font-family: 'Glyphicons Halflings';
  src: url(https://calcuquote.com/vendor/bootstrap/fonts/glyphicons-halflings-regular.eot);
  src: url(https://calcuquote.com/vendor/bootstrap/fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(https://calcuquote.com/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff2) format('woff2'),url(https://calcuquote.com/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff) format('woff'),url(https://calcuquote.com/vendor/bootstrap/fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(https://calcuquote.com/vendor/bootstrap/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')
}
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
html {
  font-size: 10px;
  -webkit-tap-highlight-color: transparent
}
body {
  font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
  font-size: 14px;
  line-height: 1.42857143;
  color: #333;
  background-color: #fff
}
a {
  color: #337ab7;
  text-decoration: none
}
a:focus,
a:hover {
  color: #23527c;
  text-decoration: underline
}
a:focus {
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
.img-responsive {
  display: block;
  max-width: 100%;
  height: auto
}
hr {
  margin-top: 20px;
  margin-bottom: 20px;
  border: 0;
  border-top: 1px solid #eee
}
h1,
h2,
h4 {
  font-family: inherit;
  font-weight: 500;
  line-height: 1.1;
  color: inherit
}
h1,
h2 {
  margin-top: 20px;
  margin-bottom: 10px
}
h4 {
  margin-top: 10px;
  margin-bottom: 10px
}
h1 {
  font-size: 36px
}
h2 {
  font-size: 30px
}
h4 {
  font-size: 18px
}
p {
  margin: 0 0 10px
}
.lead {
  margin-bottom: 20px;
  font-size: 16px;
  font-weight: 300;
  line-height: 1.4
}
ul {
  margin-top: 0;
  margin-bottom: 10px
}
blockquote {
  padding: 10px 20px;
  margin: 0 0 20px;
  font-size: 17.5px;
  border-left: 5px solid #eee
}
blockquote p:last-child {
  margin-bottom: 0
}
.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto
}
.row {
  margin-right: -15px;
  margin-left: -15px
}
.col-md-12,
.col-md-2,
.col-md-3,
.col-md-4,
.col-md-6,
.col-md-8,
.col-sm-6 {
  position: relative;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: 700
}
.form-control::-moz-placeholder {
  color: #999;
  opacity: 1
}
.form-control:-ms-input-placeholder {
  color: #999
}
.form-control::-webkit-input-placeholder {
  color: #999
}
.form-control::-ms-expand {
  background-color: transparent;
  border: 0
}
.btn {
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px
}
.btn:active:focus,
.btn:focus {
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
.btn:focus,
.btn:hover {
  color: #333;
  text-decoration: none
}
.btn:active {
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
  box-shadow: inset 0 3px 5px rgba(0,0,0,.125)
}
.btn-lg {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
  border-radius: 6px
}
.collapse {
  display: none
}
.collapse.in {
  display: block
}
.panel {
  margin-bottom: 20px;
  background-color: #fff;
  border: 1px solid transparent;
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
  box-shadow: 0 1px 1px rgba(0,0,0,.05)
}
.panel-body {
  padding: 15px
}
.panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px
}
.panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 16px;
  color: inherit
}
.panel-title > a {
  color: inherit
}
.panel-group {
  margin-bottom: 20px
}
.panel-group .panel {
  margin-bottom: 0;
  border-radius: 4px
}
.panel-group .panel + .panel {
  margin-top: 5px
}
.panel-group .panel-heading {
  border-bottom: 0
}
.panel-default {
  border-color: #ddd
}
.panel-default > .panel-heading {
  color: #333;
  background-color: #f5f5f5;
  border-color: #ddd
}
.container:after,
.container:before,
.panel-body:after,
.panel-body:before,
.row:after,
.row:before {
  display: table;
  content: " "
}
.container:after,
.panel-body:after,
.row:after {
  clear: both
}
.fa,
.fas {
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  display: inline-block;
  font-style: normal;
  font-variant: normal;
  text-rendering: auto;
  line-height: 1
}
.fa-arrow-right:before {
  content: "\f061"
}
.fa-binoculars:before {
  content: "\f1e5"
}
.fa-bullhorn:before {
  content: "\f0a1"
}
.fa-chart-line:before {
  content: "\f201"
}
.fa-check:before {
  content: "\f00c"
}
.fa-comment:before {
  content: "\f075"
}
.fa-microchip:before {
  content: "\f2db"
}
.fa-user-secret:before {
  content: "\f21b"
}
.fa-users:before {
  content: "\f0c0"
}
.fa-wifi:before {
  content: "\f1eb"
}
.fa-wrench:before {
  content: "\f0ad"
}
@font-face {
  font-family: Font Awesome\ 5 Brands;
  font-style: normal;
  font-weight: 400;
  src: url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-brands-400.eot);
  src: url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-brands-400.eot?#iefix) format("embedded-opentype"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-brands-400.woff2) format("woff2"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-brands-400.woff) format("woff"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-brands-400.ttf) format("truetype"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-brands-400.svg#fontawesome) format("svg")
}
@font-face {
  font-family: Font Awesome\ 5 Free;
  font-style: normal;
  font-weight: 400;
  src: url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-regular-400.eot);
  src: url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-regular-400.eot?#iefix) format("embedded-opentype"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-regular-400.woff2) format("woff2"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-regular-400.woff) format("woff"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-regular-400.ttf) format("truetype"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-regular-400.svg#fontawesome) format("svg")
}
@font-face {
  font-family: Font Awesome\ 5 Free;
  font-style: normal;
  font-weight: 900;
  src: url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-solid-900.eot);
  src: url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-solid-900.eot?#iefix) format("embedded-opentype"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-solid-900.woff2) format("woff2"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-solid-900.woff) format("woff"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-solid-900.ttf) format("truetype"),url(https://use.fontawesome.com/releases/v5.0.13/webfonts/fa-solid-900.svg#fontawesome) format("svg")
}
.fa,
.fas {
  font-family: Font Awesome\ 5 Free
}
.fa,
.fas {
  font-weight: 900
}
@font-face {
  font-family: simple-line-icons;
  src: url(https://calcuquote.com/vendor/simple-line-icons/fonts/Simple-Line-Icons.eot?v=2.4.0);
  src: url(https://calcuquote.com/vendor/simple-line-icons/fonts/Simple-Line-Icons.eot?v=2.4.0#iefix) format('embedded-opentype'),url(https://calcuquote.com/vendor/simple-line-icons/fonts/Simple-Line-Icons.woff2?v=2.4.0) format('woff2'),url(https://calcuquote.com/vendor/simple-line-icons/fonts/Simple-Line-Icons.ttf?v=2.4.0) format('truetype'),url(https://calcuquote.com/vendor/simple-line-icons/fonts/Simple-Line-Icons.woff?v=2.4.0) format('woff'),url(https://calcuquote.com/vendor/simple-line-icons/fonts/Simple-Line-Icons.svg?v=2.4.0#simple-line-icons) format('svg');
  font-weight: 400;
  font-style: normal
}
.owl-carousel {
  -webkit-tap-highlight-color: transparent;
  position: relative
}
.owl-carousel {
  display: none;
  width: 100%;
  z-index: 1
}
button::-moz-focus-inner {
  padding: 0;
  border: 0
}
html {
  direction: ltr;
  overflow-x: hidden;
  box-shadow: none
}
body {
  background-color: #fff;
  color: #777;
  font-family: Lato,Arial,sans-serif;
  font-size: 14px;
  line-height: 22px;
  margin: 0
}
body a {
  outline: 0
}
li {
  line-height: 24px
}
#header {
  position: relative;
  z-index: 100
}
#header .header-body {
  background: #1a71d2;
  border-top: 5px solid #ededed;
  border-bottom: 1px solid transparent;
  padding: 8px 0;
  width: 100%;
  z-index: 1001;
  min-height: 125px
}
#header .container {
  position: relative
}
#header .header-container {
  position: relative;
  display: table
}
#header .header-row {
  display: table-row;
  clear: both
}
#header.header-narrow .header-body {
  min-height: 0
}
#header .header-container {
  display: block
}
#header .header-row {
  display: block
}
section.section {
  background: #f4f4f4;
  border-top: 5px solid #f1f1f1;
  margin: 30px 0;
  padding: 50px 0
}
section.section.section-footer {
  margin-bottom: -50px
}
.slider-container {
  background: #171717;
  height: 500px;
  overflow: hidden;
  width: 100%;
  direction: ltr
}
.slider-container .top-label {
  color: #fff;
  font-size: 24px;
  font-weight: 300
}
.slider-container .main-label {
  color: #fff;
  font-size: 62px;
  line-height: 62px;
  font-weight: 800;
  text-shadow: 2px 2px 4px rgba(0,0,0,.15)
}
.slider-container .btn-slider-action {
  font-size: 22px;
  font-weight: 600;
  line-height: 20px;
  padding: 20px 25px
}
section.section-custom-map-2 {
  background: transparent url(https://calcuquote.com/img/map-2.png) center 0 no-repeat;
  padding: 129px 0 0;
  margin: 50px 0 0;
  border: 0
}
section.section-custom-map-2 section.section {
  border-top-color: rgba(241,241,241,.8);
  background: rgba(244,244,244,.8)
}
.home-intro {
  background-color: #3a3a3a;
  margin-bottom: 60px;
  overflow: hidden;
  padding: 20px 0 10px 0;
  position: relative;
  text-align: left
}
.home-intro p {
  color: #fff;
  display: inline-block;
  font-size: 1.4em;
  font-weight: 300;
  max-width: 800px;
  padding-top: 5px
}
.home-intro p span {
  color: #fff;
  display: block;
  font-size: .8em;
  padding-top: 5px
}
.home-intro p em {
  font-family: Lato,cursive;
  font-size: 1.6em
}
.home-intro .get-started {
  margin-top: 15px;
  margin-bottom: 15px;
  position: relative;
  text-align: right
}
.home-intro .get-started .btn {
  position: relative;
  z-index: 1
}
.home-intro .get-started a {
  color: #1a71d2
}
.home-intro .get-started a:not(.btn) {
  color: #c7c7c7
}
.home-intro .learn-more {
  margin-left: 15px
}
.home-concept {
  background: transparent url(https://calcuquote.com/img/home-concept.png) no-repeat center 0;
  width: 100%;
  overflow: hidden
}
.home-concept strong {
  display: block;
  font-family: Lato,cursive;
  font-size: 2.1em;
  font-weight: 400;
  position: relative;
  margin-top: 30px
}
.home-concept .row {
  position: relative
}
.home-concept .process-image {
  background: transparent url(https://calcuquote.com/img/home-concept-item.png) no-repeat 0 0;
  width: 160px;
  margin: 135px auto 0 auto;
  padding-bottom: 50px;
  position: relative;
  z-index: 1
}
.home-concept .process-image amp-img {
  border-radius: 150px;
  margin: 7px 8px;
  width: auto;
  height: auto;
  max-width: 145px;
  max-height: 145px
}
.home-concept .our-work {
  margin-top: 52px;
  font-size: 2.6em
}
.home-concept .project-image {
  background: transparent url(https://calcuquote.com/img/home-concept-item.png) no-repeat 100% 0;
  width: 350px;
  margin: 15px 0 0 -30px;
  padding-bottom: 45px;
  position: relative;
  z-index: 1
}
.home-concept .sun {
  background: transparent url(https://calcuquote.com/img/home-concept-icons.png) no-repeat 0 0;
  width: 60px;
  height: 56px;
  display: block;
  position: absolute;
  left: 10%;
  top: 35px
}
.home-concept .cloud {
  background: transparent url(https://calcuquote.com/img/home-concept-icons.png) no-repeat 100% 0;
  width: 116px;
  height: 56px;
  display: block;
  position: absolute;
  left: 57%;
  top: 35px
}
.home-concept {
  background: 0 0
}
.home-concept .project-image {
  margin: 20px auto 0 auto
}
.home-concept .process-image {
  margin-top: 0;
  padding-bottom: 25px
}
.mb-none {
  margin-bottom: 0
}
.mb-sm {
  margin-bottom: 10px
}
#footer {
  background: #3a3a3a;
  border-top: 4px solid #0e0e0e;
  font-size: .9em;
  margin-top: 50px;
  padding: 70px 0 0;
  position: relative;
  clear: both
}
#footer.short {
  padding-top: 50px
}
h1,
h2,
h4 {
  color: #1d2127;
  font-weight: 200;
  letter-spacing: -1px;
  margin: 0
}
h1 {
  font-size: 2.6em;
  line-height: 44px;
  margin: 0 0 32px 0
}
h2 {
  font-size: 2.2em;
  font-weight: 300;
  line-height: 42px;
  margin: 0 0 32px 0
}
h4 {
  font-size: 1.4em;
  font-weight: 400;
  letter-spacing: normal;
  line-height: 27px;
  margin: 0 0 14px 0
}
blockquote {
  font-size: 1em
}
p {
  color: #3a3a3a;
  line-height: 24px;
  margin: 0 0 20px
}
p.tall {
  margin-bottom: 20px
}
a,
a:focus,
a:hover {
  color: #ccc
}
p.drop-caps:first-letter {
  float: left;
  font-size: 75px;
  line-height: 60px;
  padding: 4px;
  margin-right: 5px;
  margin-top: 5px;
  font-family: Georgia
}
p.drop-caps.drop-caps-style-2:first-letter {
  background-color: #ccc;
  color: #fff;
  padding: 6px;
  margin-right: 5px;
  border-radius: 4px
}
.appear-animation {
  opacity: 1
}
.panel-group .panel-heading {
  padding: 0;
  border-radius: 3px
}
.panel-group .panel-heading a {
  display: block;
  padding: 10px 15px
}
.panel-group .panel-heading a:focus,
.panel-group .panel-heading a:hover {
  text-decoration: none
}
.panel-group .panel-heading a .fa {
  display: inline-block;
  margin-right: 5px;
  position: relative;
  top: -1px
}
.toggle > label:-moz-selection {
  background: 0 0
}
.toggle > label:selection {
  background: 0 0
}
.owl-carousel {
  margin-bottom: 20px
}
.center {
  text-align: center
}
.inverted {
  color: #fff;
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px
}
h1 .inverted {
  padding-left: 10px;
  padding-right: 10px
}
.learn-more {
  display: inline-block;
  white-space: nowrap
}
hr {
  background-image: -webkit-linear-gradient(left,transparent,#dbdbdb,transparent);
  background-image: linear-gradient(to right,transparent,#dbdbdb,transparent);
  border: 0;
  height: 1px;
  margin: 22px 0
}
hr.tall {
  margin: 44px 0
}
.feature-box {
  clear: both
}
.feature-box .feature-box-icon {
  background: #ccc;
  border-radius: 35px;
  color: #fff;
  display: inline-block;
  float: left;
  height: 35px;
  line-height: 35px;
  margin-right: 10px;
  position: relative;
  text-align: center;
  top: 5px;
  width: 35px
}
.feature-box .feature-box-info {
  padding-left: 50px
}
.testimonial {
  margin-bottom: 20px
}
.testimonial blockquote {
  background: #ccc;
  border-radius: 10px;
  border: 0;
  color: #666;
  font-family: Georgia,serif;
  font-style: italic;
  margin: 0;
  padding: 10px 50px;
  position: relative
}
.testimonial blockquote:before {
  left: 10px;
  top: 0;
  color: #fff;
  content: "“";
  font-size: 80px;
  font-style: normal;
  line-height: 1;
  position: absolute
}
.testimonial blockquote:after {
  color: #fff;
  content: "”";
  font-size: 80px;
  font-style: normal;
  line-height: 1;
  position: absolute;
  bottom: -.5em;
  right: 10px
}
.testimonial blockquote p {
  color: #fff;
  font-family: Georgia,serif;
  font-style: normal;
  font-size: 1.2em;
  line-height: 1.4
}
.testimonial .testimonial-arrow-down {
  border-left: 15px solid transparent;
  border-right: 15px solid transparent;
  border-top: 15px solid #ccc;
  height: 0;
  margin: 0 0 0 17px;
  width: 0
}
.testimonial .testimonial-author {
  margin: 12px 0 0 0
}
.testimonial .testimonial-author::after {
  clear: both;
  content: "";
  display: table
}
.testimonial .testimonial-author strong {
  color: #111;
  display: block;
  padding-top: 10px;
  margin-bottom: -5px
}
.testimonial .testimonial-author span {
  color: #666;
  display: block;
  font-size: 12px
}
.testimonial .testimonial-author p {
  color: #999;
  margin: 0 0 0 25px;
  text-align: left
}
.counters .counter {
  text-align: center
}
.counters strong {
  display: block;
  font-weight: 700;
  font-size: 50px;
  line-height: 50px
}
.counters label {
  display: block;
  font-weight: 100;
  font-size: 20px;
  line-height: 20px
}
.counters.counters-text-light .counter {
  color: #fff
}
.counters.counters-text-light .counter label,
.counters.counters-text-light .counter strong {
  color: #fff
}
.word-rotate {
  display: inline-block;
  overflow: hidden;
  text-align: center;
  position: relative;
  max-height: 22px;
  line-height: 22px
}
.word-rotate .word-rotate-items {
  position: relative;
  top: 0;
  width: 0;
  display: inline-block
}
.word-rotate .word-rotate-items span {
  display: inline-block;
  white-space: nowrap
}
.word-rotate .word-rotate-items span:not(:first-child) {
  display: none
}
h1.word-rotator-title .inverted {
  min-height: 56px
}
h1.word-rotator-title .word-rotate {
  max-height: 46px;
  line-height: 46px;
  margin-bottom: -9px
}
h2.word-rotator-title .word-rotate {
  max-height: 42px;
  line-height: 42px;
  margin-bottom: -9px
}
@font-face {
  font-family: star;
  src: url(https://calcuquote.com/css/fonts/star.eot);
  src: url(https://calcuquote.com/css/fonts/star.eot?#iefix) format("embedded-opentype"),url(https://calcuquote.com/css/fonts/star.woff) format("woff"),url(https://calcuquote.com/css/fonts/star.ttf) format("truetype"),url(https://calcuquote.com/css/fonts/star.svg#star) format("svg");
  font-weight: 400;
  font-style: normal
}
.shop .quantity .qty::-webkit-inner-spin-button,
.shop .quantity .qty::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0
}
.shop .quantity .qty::-ms-clear {
  display: none
}
@font-face {
  font-family: revicons;
  src: url(https://calcuquote.com/vendor/rs-plugin/fonts/revicons/revicons.eot?5510888);
  src: url(https://calcuquote.com/vendor/rs-plugin/fonts/revicons/revicons.eot?5510888#iefix) format('embedded-opentype'),url(https://calcuquote.com/vendor/rs-plugin/fonts/revicons/revicons.woff?5510888) format('woff'),url(https://calcuquote.com/vendor/rs-plugin/fonts/revicons/revicons.ttf?5510888) format('truetype'),url(https://calcuquote.com/vendor/rs-plugin/fonts/revicons/revicons.svg?5510888#revicons) format('svg');
  font-weight: 400;
  font-style: normal
}
.rev_slider_wrapper {
  position: relative;
  z-index: 0;
  width: 100%
}
.rev_slider {
  position: relative;
  overflow: visible
}
.rev_slider a {
  box-shadow: none
}
.rev_slider amp-img {
  max-width: none;
  margin: 0;
  border: none
}
.rev_slider > ul,
.rev_slider > ul > li,
.rev_slider > ul > li:before {
  list-style: none;
  position: absolute;
  margin: 0;
  padding: 0;
  overflow-x: visible;
  overflow-y: visible;
  background-image: none;
  background-position: 0 0;
  text-indent: 0;
  top: 0;
  left: 0
}
.rev_slider > ul > li,
.rev_slider > ul > li:before {
  visibility: hidden
}
.rev_slider .tp-caption {
  position: relative;
  visibility: hidden;
  white-space: nowrap;
  display: block;
  -webkit-font-smoothing: antialiased;
  z-index: 1
}
.rev_slider .tp-caption {
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  -o-user-select: none
}
.rs-background-video-layer audio::-webkit-media-controls,
.rs-background-video-layer video::-webkit-media-controls,
.rs-background-video-layer video::-webkit-media-controls-start-playback-button {
  display: none
}
.tp-caption amp-img {
  background: 0 0
}
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
.rev_slider {
  overflow: hidden
}
.rev_slider {
  overflow: hidden
}
#mc_embed_signup input[type=email]::-webkit-input-placeholder {
  color: #888
}
#mc_embed_signup input[type=email]::-moz-placeholder {
  color: #888
}
#mc_embed_signup input[type=email]:-ms-input-placeholder {
  color: #888
}
.fc-slideshow {
  position: relative;
  width: 338px;
  height: 338px;
  border-radius: 50%;
  margin: 0 auto;
  -webkit-perspective: 1200px;
  -moz-perspective: 1200px;
  perspective: 1200px;
  top: 6px
}
ul.fc-slides {
  list-style: none;
  margin: 0;
  padding: 0
}
ul.fc-slides li {
  display: none
}
.fc-slideshow amp-img {
  border-radius: 50%
}
a {
  color: #1a71d2
}
a:hover {
  color: #0099e6
}
a:focus {
  color: #0099e6
}
a:active {
  color: #0077b3
}
html .heading-primary {
  color: #1a71d2
}
p.drop-caps:first-letter {
  color: #1a71d2
}
p.drop-caps.drop-caps-style-2:first-letter {
  background-color: #1a71d2
}
html .btn-secondary {
  color: #1a71d2;
  background-color: #fff;
  border-color: #fff #fff #fff
}
html .btn-secondary:hover {
  border-color: #fff #fff #fff;
  background-color: #daf0ff
}
html .btn-secondary:active,
html .btn-secondary:active:focus,
html .btn-secondary:active:hover,
html .btn-secondary:focus {
  border-color: #fff #fff #fff;
  background-color: #fff
}
html .btn-secondary:active:focus,
html .btn-secondary:active:hover,
html .btn-secondary:focus,
html .btn-secondary:hover {
  color: #1a7ad2
}
html .btn-tertiary {
  color: #fff;
  background-color: #1a71d2;
  border-color: #1a71d2 #1a71d2 #1a71d2
}
html .btn-tertiary:hover {
  border-color: #30bec6 #30bec6 #26969c;
  background-color: #30bec6
}
html .btn-tertiary:active,
html .btn-tertiary:active:focus,
html .btn-tertiary:active:hover,
html .btn-tertiary:focus {
  border-color: #26969c #26969c #26969c;
  background-color: #26969c
}
html .btn-tertiary:active:focus,
html .btn-tertiary:active:hover,
html .btn-tertiary:focus,
html .btn-tertiary:hover {
  color: #fff
}
html section.section-primary {
  background-color: #1a71d2;
  border-color: #0077b3
}
.inverted {
  background-color: #1a71d2
}
.panel-group .panel-heading a {
  color: #1a71d2
}
.testimonial blockquote {
  background: #0099e6
}
.testimonial .testimonial-arrow-down {
  border-top-color: #0099e6
}
html .testimonial-primary blockquote {
  background: #1a71d2
}
html .testimonial-primary .testimonial-arrow-down {
  border-top-color: #1a7ad2
}
.feature-box .feature-box-icon {
  background-color: #1a71d2
}
.home-concept strong {
  color: #1a71d2
}
.home-intro p em {
  color: #fff
}
.em-light-white {
  color: #fff
}
.em-learn-more {
  color: #c7c7c7
}
.testimonial blockquote p {
  color: #dbecff;
  font-family: Lato,Georgia,serif;
  font-style: normal;
  font-size: 1.2em;
  line-height: 1.4
}
.emphasize {
  color: #fff;
  font-weight: 700
}
.home-intro-blue {
  background-color: #1a71d2
}
#slider {
  height: 500px
}
.rbt-inline-1 {
  z-index: 50
}
.rbt-inline-2 {
  z-index: 5;
  opacity: .2;
  padding: 2cm 4cm 3cm 4cm
}
.rbt-inline-3 {
  z-index: 5
}
section.rbt-accordion-section > header {
  background-color: transparent;
  padding: 0;
  margin: 0;
  border: 0
}</style>
    </head>
    <body>
        <div class="body">
            <div role="main" class="main">

                <div id="slider" class="slider-container rev_slider_wrapper">
                    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider="" data-plugin-options="{'sliderLayout': 'fullwidth', 'fullScreenOffsetContainer': '', 'fullScreenOffset': '0'}">
                        <ul>
                            <li data-transition="fade">
                                <amp-img src="https://calcuquote.com/img/slides/slide-bg-full.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" width="1800" height="1350" layout="fixed"></amp-img>
                                <div class="tp-caption rbt-inline-1" data-x="center" data-hoffset="-200" data-y="center" data-voffset="-60" data-start="1000" data-transform_in="x:[-300%];opacity:0;s:500;">
                                    <amp-img src="https://calcuquote.com/img/slides/slide-title-border.png" alt="" width="37" height="10" layout="fixed"></amp-img>
                                </div>

                                <div class="tp-caption main-label rbt-inline-2" data-x="center" data-hoffset="0" data-y="center" data-voffset="-100" data-start="150" data-transform_in="y:[100%];s:500;" data-transform_out="opacity:0;s:500;" data-whitespace="nowrap" data-mask_in="x:0px;y:0px;" data-appear-animation="pulse" data-appear-animation-delay="0" data-appear-animation-duration="1s">CalcuQuote
                                </div>

                                <div class="tp-caption top-label rbt-inline-3" data-x="center" data-hoffset="0" data-y="center" data-voffset="-60" data-start="500" data-transform_in="y:[-300%];opacity:0;s:500;">RFQ Management System
                                </div>

                                <div class="tp-caption rbt-inline-3" data-x="center" data-hoffset="200" data-y="center" data-voffset="-60" data-start="1000" data-transform_in="x:[300%];opacity:0;s:500;">
                                    <amp-img src="https://calcuquote.com/img/slides/slide-title-border.png" alt="" width="37" height="10" layout="fixed"></amp-img>
                                </div>

                                <div class="tp-caption main-label rbt-inline-3" data-x="center" data-hoffset="0" data-y="center" data-voffset="20" data-start="1500" data-whitespace="nowrap" data-transform_in="y:[100%];s:500;" data-transform_out="opacity:0;s:500;" data-mask_in="x:0px;y:0px;">Made for EMS.
                                </div>

                                <a class="tp-caption btn btn-lg btn-tertiary btn-slider-action rbt-inline-3" data-hash="" data-hash-offset="85" href="#intro" data-x="center" data-hoffset="0" data-y="center" data-voffset="100" data-start="2200" data-whitespace="nowrap" data-transform_in="y:[100%];s:500;" data-transform_out="opacity:0;s:500;" data-mask_in="x:0px;y:0px;">Learn More</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="intro">

                    <header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAtElement': '#header', 'stickySetTop': '0', 'stickyChangeLogo': false}">

                        <div class="header-body">

                            <div class="header-container container">

                                <div id="menu" class="header-row">

                                </div>

                            </div>

                        </div>

                    </header>

                </div>

                <div class="container">

                    <div class="row">

                        <hr class="tall">

                    </div>

                </div>

                <div id="cq-word-rotator" class="container">

                    <div class="row center">

                        <div class="col-md-12">

                            <h1 class="mb-sm word-rotator-title">

                                Your RFQ process should be

                                <strong class="inverted">

									<span class="word-rotate" data-plugin-options="{'delay': 4000, 'animDelay': 500}">

										<span class="word-rotate-items">

											<span>simple.</span>

											<span>fast.</span>

											<span>accurate.</span>

											<span>value added.</span>

										</span>

									</span>

								</strong>

                            </h1>

                            <p class="lead">

                                CalcuQuote is your one-stop-shop for the entire RFQ process beginning with RFQ receipt to sending a professional looking quote back to your customer (and everything in between).

                            </p>

                        </div>

                    </div>

                </div>

                <div id="cq-path-image" class="home-concept">

                    <div class="container">

                        <div class="row center">

                            <span class="sun"></span>

                            <span class="cloud"></span>

                            <div class="col-md-2 col-md-offset-1">

                                <div class="process-image appear-animation" data-appear-animation="bounceIn">

                                    <amp-img src="https://calcuquote.com/img/home-concept-item-1.png" alt="" width="145" height="145" layout="fixed"></amp-img>

                                    <strong>Materials</strong>

                                </div>

                            </div>

                            <div class="col-md-2">

                                <div class="process-image appear-animation" data-appear-animation="bounceIn" data-appear-animation-delay="300">

                                    <amp-img src="https://calcuquote.com/img/home-concept-item-2.png" alt="" width="145" height="145" layout="fixed"></amp-img>

                                    <strong>Labor</strong>

                                </div>

                            </div>

                            <div class="col-md-2">

                                <div class="process-image appear-animation" data-appear-animation="bounceIn" data-appear-animation-delay="600">

                                    <amp-img src="https://calcuquote.com/img/home-concept-item-3.png" alt="" width="145" height="145" layout="fixed"></amp-img>

                                    <strong>Overhead</strong>

                                </div>

                            </div>

                            <div class="col-md-4 col-md-offset-1">

                                <div class="project-image">

                                    <div id="fcSlideshow" class="fc-slideshow">

                                        <ul class="fc-slides">

                                            <li>

                                                <amp-img class="img-responsive" src="https://calcuquote.com/img/projects/project-home-1.png" alt="" width="338" height="338" layout="responsive"></amp-img>

                                            </li>

                                            <li>

                                                <amp-img class="img-responsive" src="https://calcuquote.com/img/projects/project-home-2.png" alt="" width="338" height="338" layout="responsive"></amp-img>

                                            </li>

                                            <li>

                                                <amp-img class="img-responsive" src="https://calcuquote.com/img/projects/project-home-3.png" alt="" width="338" height="338" layout="responsive"></amp-img>

                                            </li>

                                        </ul>

                                    </div>

                                    <strong class="our-work">Deliver the Quote!</strong>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="home-intro home-intro-blue" id="home-intro">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-8">

                                <p>

                                    Improve the <em class="em-light-white">speed and accuracy </em> of your RFQ process

                                    <span class="em-light-white">Designed for EMS industry by EMS veterans.</span>

                                </p>

                            </div>

                            <div class="col-md-4">

                                <div class="get-started">

                                    <a href="https://calcuquote.com/demo.html" class="btn btn-lg btn-secondary">Schedule Demo!</a>

                                    <div class="learn-more em-learn-more">or <a href="https://calcuquote.com/products-rfq-management-system.html" class="em-learn-more">Learn More.</a></div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="container">

                    <div class="row">

                        <div class="col-md-8">

                            <h2>What are the <strong>Benefits?</strong></h2>

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fa fa-wifi"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote on the go</h4>

                                            <p class="tall">Have your sales team capture RFQs from anywhere.</p>

                                        </div>

                                    </div>

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fa fa-arrow-right"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote with consistency</h4>

                                            <p class="tall">CalcuQuote puts guardrails on your quotes to ensure consistently accurate quotes.</p>

                                        </div>

                                    </div>

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fa fa-wrench"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote the right opportunities</h4>

                                            <p class="tall">Configure your own, unique labor profiles so you win the RIGHT type of business.</p>

                                        </div>

                                    </div>

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fa fa-microchip"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote materials instantly</h4>

                                            <p class="tall">Easily select real-time pricing and availability directly from distributors.</p>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-sm-6">

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fa fa-users"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote like a professional</h4>

                                            <p class="tall">Automatically generate a formatted quote for a professional presentation.</p>

                                        </div>

                                    </div>

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fa fa-check"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote with confidence</h4>

                                            <p class="tall">Prevent unauthorized quotes and price edits by setting user level permissions.</p>

                                        </div>

                                    </div>

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fa fa-binoculars"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote with visibility</h4>

                                            <p class="tall">Keep track of open RFQs and follow up with customers to win more business.</p>

                                        </div>

                                    </div>

                                    <div class="feature-box">

                                        <div class="feature-box-icon">

                                            <i class="fas fa-chart-line"></i>

                                        </div>

                                        <div class="feature-box-info">

                                            <h4 class="heading-primary mb-none">Quote the CalcuQuote way!</h4>

                                            <p class="tall">Analyze key process metrics for continuous improvement.</p>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-md-4">

                            <h2>and more...</h2>

                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">

                                    <div class="panel-heading">

                                        <h4 class="panel-title">

                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">

												<i class="fa fa-user-secret"></i>

												Security

											</a>

                                        </h4>

                                    </div>

                                    <div id="collapseOne" class="accordion-body collapse in">

                                        <div class="panel-body">

                                            We use industry best practices to protect your data and block unauthorized access.

                                        </div>

                                    </div>

                                </div>

                                <amp-accordion>
                                    <section class="panel panel-default">
                                        <!-- <div class="panel panel-default"> -->

                                        <header>
                                            <div class="panel-heading">

                                                <h4 class="panel-title">

                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">

												<i class="fa fa-comment"></i>

												Support

											</a>

                                                </h4>

                                            </div>
                                        </header>

                                        <div id="collapseTwo" class="accordion-body collapse">

                                            <div class="panel-body">

                                                Get the best service you've ever gotten from a vendor - bar none.

                                            </div>

                                        </div>

                                        <!-- </div> -->
                                    </section>
                                    <section class="panel panel-default">
                                        <!-- <div class="panel panel-default"> -->

                                        <header>
                                            <div class="panel-heading">

                                                <h4 class="panel-title">

                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">

												<i class="fa fa-bullhorn"></i>

												And it keeps getting better...

											</a>

                                                </h4>

                                            </div>
                                        </header>

                                        <div id="collapseThree" class="accordion-body collapse">

                                            <div class="panel-body">

                                                We are constantly improving and evolving based directly on customer feedback.

                                            </div>

                                        </div>

                                        <!-- </div> -->
                                    </section>
                                </amp-accordion>

                            </div>

                        </div>

                    </div>

                </div>

                <hr class="tall">

                <div class="row center">

                    <div class="col-md-12">

                        <h2 class="mb-sm word-rotator-title">

                            We're not the only ones excited about

                            <strong>

								<span class="word-rotate" data-plugin-options="{'delay': 3500, 'animDelay': 400}">

									<span class="word-rotate-items">

										<span>better</span>

										<span>faster</span>

										<span>accurate</span>

										<span>painless</span>

									</span>

								</span>

							</strong> Quotes.

                        </h2>

                    </div>

                </div>

                <div class="row center">

                    <section class="section section-primary">

                        <div class="container">

                            <div class="row">

                                <div class="counters counters-text-light">

                                    <div class="col-md-3 col-sm-6">

                                        <div class="counter">

                                            <strong data-to="28000" data-append="+">0</strong>

                                            <label>Published Quotes</label>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6">

                                        <div class="counter">

                                            <strong data-to="400" data-append="+">0</strong>

                                            <label>Active Users</label>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6">

                                        <div class="counter">

                                            <strong data-to="300000" data-append="+">0</strong>

                                            <label>Line Items Quoted</label>

                                        </div>

                                    </div>

                                    <div class="col-md-3 col-sm-6">

                                        <div class="counter">

                                            <strong data-to="13">0</strong>

                                            <label>Distributor Integrations</label>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>

                </div>

                <section class="section section-custom-map-2">

                    <section class="section section-default section-footer">

                        <div class="container">

                            <div class="row">

                                <div class="col-md-6">

                                    <h2><strong>Tweet</strong> us</h2>

                                    <div class="row">

                                        <a class="twitter-timeline" data-chrome="transparent noheader nofooter" href="https://twitter.com/CalcuQuote" data-widget-id="619953134824067072">Tweets by @CalcuQuote</a>

                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <h2>What <strong>Our Customers</strong> are saying...</h2>

                                    <div class="row">

                                        <div class="owl-carousel owl-theme mb-none" data-plugin-options="{'items': 1, 'autoplay': true, 'autoplayTimeout': 9000}">

                                            <div>

                                                <div class="col-md-12">

                                                    <div class="testimonial testimonial-primary">

                                                        <blockquote>

                                                            <p>This was by far the <span class="emphasize">easiest implementation</span> and had the <span class="emphasize">greatest ROI</span> of any system I have been involved with.</p>

                                                            <p>We went from a system requiring its own server on an expensive operating platform with significant IT maintenance requirements, to a <span class="emphasize">simple</span> web application requiring no internal server and no internal IT maintenance.</p>

                                                        </blockquote>

                                                        <div class="testimonial-arrow-down"></div>

                                                        <div class="testimonial-author">

                                                            <p><strong>Controller</strong><span>Customer since 2015</span></p>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div>

                                                <div class="col-md-12">

                                                    <div class="testimonial testimonial-primary">

                                                        <blockquote>

                                                            <p><span class="emphasize">We highly recommend them.</span></p>

                                                            <p>They have a continuous improvement for their product and we are very very excited about their customer service. They have a personal touch: one-to-one, webinars, online chats...all kinds of tools to make sure that they answer your specific needs. Check them out...<span class="emphasize">you won't regret it.</span></p>

                                                        </blockquote>

                                                        <div class="testimonial-arrow-down"></div>

                                                        <div class="testimonial-author">

                                                            <p><strong>Vice President - Business Development</strong><span>Customer since 2015</span></p>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div>

                                                <div class="col-md-12">

                                                    <div class="testimonial testimonial-primary">

                                                        <blockquote>

                                                            <p>The system is <span class="emphasize">very intuitive</span>, and the staff at CalcuQuote was always available for any clarification needed. CalcuQuote makes it very easy to get price and availability <span class="emphasize">quickly</span>. My only reservation is that I am recommending it to our competitors.</p>

                                                        </blockquote>

                                                        <div class="testimonial-arrow-down"></div>

                                                        <div class="testimonial-author">

                                                            <p><strong>Controller</strong><span>Customer since 2015</span></p>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div>

                                                <div class="col-md-12">

                                                    <div class="testimonial testimonial-primary">

                                                        <blockquote>

                                                            <p>As the controller for a small business with complex quoting needs we found this tool very helpful in preparing <span class="emphasize">more accurate</span> time estimates, <span class="emphasize">reducing time</span> to quote and <span class="emphasize">improving visibility</span> into the quotation process.</p>

                                                            <p>The Calcuquote staff was very helpful during the implementation.</p>

                                                        </blockquote>

                                                        <div class="testimonial-arrow-down"></div>

                                                        <div class="testimonial-author">

                                                            <p><strong>Director of Accounting and Logistics</strong><span>Customer since 2015</span></p>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div>

                                                <div class="col-md-12">

                                                    <div class="testimonial testimonial-primary">

                                                        <blockquote>

                                                            <p>We have been very pleased with the impact that it has made in our quoting sytem. CQ is very <span class="emphasize">flexible, user friendly,</span> and more than anything, it <span class="emphasize">provides fast response time</span> for your quoting needs.</p>

                                                            <p>It has made a tremendous difference in the speed of how we turn quotes. I've been in the EMS business for 35 years, and it's great to see a package that you can customize to your particular needs, to what you do in your particular facility.</p>

                                                        </blockquote>

                                                        <div class="testimonial-arrow-down"></div>

                                                        <div class="testimonial-author">

                                                            <p><strong>Vice President - Business Development</strong><span>Customer since 2015</span></p>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>

                </section>

            </div>

            <footer class="short" id="footer">

            </footer>
        </div>

    </body>
</html>