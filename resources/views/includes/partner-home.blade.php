<home :customer="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-header">Welcome, {{$user->name}}</div>
                    </div>
                    <div class="panel-body">
                        <table class="table table striped">
                            <tr>

                            </tr>
                        </table>
                        Share these links!
                        @forelse($user->getReferrals() as $referral)
                            <h4>
                                <?php  echo(ucfirst($referral->program->name) . ' Sign-up') ?>
                            </h4>
                            <code>
                                {{ $referral->link }}
                            </code>
                            <p>
                            Number of referrals: {{ $referral->relationships($referral->id) }}
                            <br>                                
                            </p>
                        @empty
                            No referrals
                        @endforelse
                            <p>
                            </p>
                        <div class="text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>