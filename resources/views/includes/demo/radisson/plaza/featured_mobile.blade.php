<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="artand#39;otel berlin-mitte - Art Suite XL" class="lazy card__image loaded" src="https://media.radissonhotels.net/image/Park-Plaza-Westminster-Bridge-London/Suite/16256-114932-f67629515_3XL.jpg?impolicy=Card" data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-westminster-bridge-london" target="_self" class="color-black" tabindex="0">
                                Park Plaza Westminster Bridge London</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <small class="card--hotel__address text-left">
                        <span class="card--hotel__address__address">200 Westminster Bridge Road</span>,
                        <span class="card--hotel__address__city">London</span>,
                        <span class="card--hotel__address__zip"> SE1 7UT</span>,
                        <span class="card--hotel__address__country">United Kingdom</span>
                    </small>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic"
                                 src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-MCID-5.svg"
                                 alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">22202 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic"
                     alt="Park Plaza Verudela Pula - Lounge Sea View" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Verudela-Pula/BarLounge/16256-113843-f63146086_3XL.jpg?impolicy=CustomCrop&cwidth=670&cheight=384" data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-verudela-pula" target="_self" class="color-black" tabindex="0" data-di-id="di-id-d6c6ab20-3a45cfcc">
                                Park Plaza Verudela Pula</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <small class="card--hotel__address text-left">
                        <span class="card--hotel__address__address">Verudella 11</span>,
                        <span class="card--hotel__address__city">Pula</span>,
                        <span class="card--hotel__address__zip"> 52100</span>,
                        <span class="card--hotel__address__country">Croatia</span>
                    </small>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic"
                                 src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">1035 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Verudela Pula - Lounge Sea View" class="lazy card__image loaded" src="https://media.radissonhotels.net/image/Park-Plaza-Victoria-Amsterdam/Guestroom/16256-114950-f68882636_3XL.jpg?impolicy=Card" data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-victoria-amsterdam" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Victoria Amsterdam</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-victoria-amsterdam"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">
                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address">Damrak 1-5</span>,
                            <span class="card--hotel__address__city">Amsterdam</span>,
                            <span class="card--hotel__address__zip"> 1012 LG</span>,
                            <span class="card--hotel__address__country">Netherlands</span>
                        </small>
                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">883 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza London Riverbank - One Bedroom Suite River View lounge" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-London-Riverbank/Suite/16256-114928-f67786698_3XL.jpg?impolicy=Card"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-london-riverbank" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza London Riverbank</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-london-riverbank"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">

                            <small class="card--hotel__address text-left">
                                <span class="card--hotel__address__address">18 Albert Embankment</span>,
                                <span class="card--hotel__address__city">London</span>,
                                <span class="card--hotel__address__zip"> SE1 7TJ</span>,
                                <span class="card--hotel__address__country">United Kingdom</span>
                            </small>


                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">1526 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Beijing Wangfujing - Exterior" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Beijing-Wangfujing/Exteriorview/16256-114921-f63601962_3XL.jpg?impolicy=Card&gravity=South"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-beijing-wangfujing" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Beijing Wangfujing</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-beijing-wangfujing"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">

                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address"></span>
                            <span class="card--hotel__address__city">Beijing</span>,
                            <span class="card--hotel__address__country">China</span>
                        </small>


                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">3368 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Vondelpark, Amsterdam - Studio King" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Vondelpark-Amsterdam/Guestroom/16256-114953-f65053195_3XL.jpg?impolicy=Card"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-vondelpark-amsterdam" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Vondelpark, Amsterdam</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-vondelpark-amsterdam"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">

                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address">Koningslaan 3</span>,
                            <span class="card--hotel__address__city">Amsterdam</span>,
                            <span class="card--hotel__address__zip"> 1075 AA</span>,
                            <span class="card--hotel__address__country">Netherlands</span>
                        </small>


                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">91 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Histria Pula - Family Suite Living Room" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Histria-Pula/Suite/16256-113842-f64962823_3XL.jpg?impolicy=Card"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-histria-pula" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Histria Pula</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-histria-pula"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">

                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address">Verudella 17</span>,
                            <span class="card--hotel__address__city">Pula</span>,
                            <span class="card--hotel__address__zip"> 52100</span>,
                            <span class="card--hotel__address__country">Croatia</span>
                        </small>


                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">1788 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Beijing Science Park - Hotel Exterior" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Beijing-Science-Park/Exteriorview/16256-114920-f63601432_3XL.jpg?impolicy=Card&amp;gravity=SouthEast"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-beijing-science-park" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Beijing Science Park</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-beijing-science-park"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">

                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address">25 Zhi Chun Rd, Haidian District</span>,
                            <span class="card--hotel__address__city">Beijing</span>,
                            <span class="card--hotel__address__zip">100083</span>,
                            <span class="card--hotel__address__country">China</span>
                        </small>


                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">516 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Nuremberg - Superior Room" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Nuremberg/Guestroom/16256-114922-f63136397_3XL.jpg?impolicy=CustomCrop&amp;cwidth=670&amp;cheight=384"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-nuremberg" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Nuremberg</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-nuremberg"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">

                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address">Bahnhofstraße 5</span>,
                            <span class="card--hotel__address__city">Nuremberg</span>,
                            <span class="card--hotel__address__zip"> 90402</span>,
                            <span class="card--hotel__address__country">Germany</span>
                        </small>


                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">380 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Arena Pula - Two Bedroom Suite" class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Arena-Pula/Suite/16256-114940-f63146752_3XL.jpg?impolicy=CustomCrop&amp;cwidth=670&amp;cheight=384"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-arena-pula" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Arena Pula</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-arena-pula"
                       target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">

                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address">Verudella 31</span>,
                            <span class="card--hotel__address__city">Pula</span>,
                            <span class="card--hotel__address__zip"> HR-52100</span>,
                            <span class="card--hotel__address__country">Croatia</span>
                        </small>


                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">631 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>


