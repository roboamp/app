<ul class="slick-dots d-lg-none" role="tablist">
    <li class="slick-active" aria-hidden="false" role="button" aria-selected="true"
        aria-controls="navigation00" id="slick-slide00" [class]="store.rooms_slide == 0 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=0)" tabindex="0">
        <button type="button" data-role="none" role="button">1</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 1 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=1)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation01" id="slick-slide01">
        <button type="button" data-role="none" role="button">2</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 2 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=2)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation02" id="slick-slide02">
        <button type="button" data-role="none" role="button">3</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 3 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=3)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation03" id="slick-slide03">
        <button type="button" data-role="none" role="button">4</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 4 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=4)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation04" id="slick-slide04">
        <button type="button" data-role="none" role="button">5</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 5 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=5)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation05" id="slick-slide05">
        <button type="button" data-role="none" role="button">6</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 6 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=6)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation06" id="slick-slide06">
        <button type="button" data-role="none" role="button">7</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 7 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=7)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation07" id="slick-slide07">
        <button type="button" data-role="none" role="button">8</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 8 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=8)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation08" id="slick-slide08">
        <button type="button" data-role="none" role="button">9</button>
    </li>
    <li aria-hidden="true" [class]="store.rooms_slide == 9 ? 'slick-active': ''"
        on="tap:radisson--hotels.goToSlide(index=9)" role="button" tabindex="0"
        aria-selected="false" aria-controls="navigation09" id="slick-slide09">
        <button type="button" data-role="none" role="button">10</button>
    </li>

</ul>