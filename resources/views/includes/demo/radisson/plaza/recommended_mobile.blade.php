    <div class="col-md-12 slick-slide slick-current slick-active" tabindex="-1" role="option" aria-describedby="slick-slide00" data-slick-index="0" aria-hidden="false">
        <div class="card text-center mb-3">
            <amp-img layout="intrinsic" width="670" height="384" class="card__image lazy loaded" alt="Park Plaza Verudela Pula - Pool" src="https://media.radissonhotels.net/image/Park-Plaza-Verudela-Pula/Poolview/16256-113843-f64962107_3XL.jpg?impolicy=Card" data-was-processed="true"></amp-img>

            <div class="card__body">
                <h3 class="h3 card__title">

                    Croatia Early Bird</h3>
                <div class="card__text mb-3">


                    Stunning views of the Adriatic Sea and plenty of adventure in Croatia await you. Book now for a special rate.</div>
                <a href="https://www.radissonhotels.com/en-us/brand/park-plaza/destinations" class="btn btn-secondary btn-sm mb-3">Discover more</a>
            </div>
        </div>
    </div>
    <div class="col-md-12 slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide01" data-slick-index="1" aria-hidden="false">
        <div class="card text-center mb-3">
            <amp-img layout="intrinsic" width="670" height="384" class="card__image lazy loaded" alt="Holmes Hotel London - Detail" src="https://media.radissonhotels.net/image/Holmes-Hotel-London/Suite/16256-114929-f65334067_3XL.jpg?impolicy=Card" data-was-processed="true"></amp-img>

            <div class="card__body">
                <h3 class="h3 card__title">

                    Family Getaway Package</h3>
                <div class="card__text mb-3">


                    Create unique family moments with Park Plaza Hotels & Resorts and our Family Getaway Package valid across Europe.</div>
                <a href="https://www.radissonhotels.com/en-us/hotel-deals/park-plaza-family-getaway" class="btn btn-secondary btn-sm mb-3">Discover more</a>
            </div>
        </div>
    </div>
    <div class="col-md-12 slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide02" data-slick-index="2" aria-hidden="false">
        <div class="card text-center mb-3">
            <amp-img layout="intrinsic" width="670" height="384" class="card__image lazy loaded" alt="Park Plaza Victoria Amsterdam - Room Service" src="https://media.radissonhotels.net/image/Park-Plaza-Victoria-Amsterdam/Guestroomamenity/16256-114950-f63141591_3XL.jpg?impolicy=CustomCrop&cwidth=670&cheight=384" data-was-processed="true"></amp-img>

            <div class="card__body">
                <h3 class="h3 card__title">

                    Bed & Breakfast</h3>
                <div class="card__text mb-3">


                    Looking for a relaxing stay in our city centre hotels? Our Bed & Breakfast rate is the perfect start to your day after a restful night.</div>
                <a href="https://www.radissonhotels.com/en-us/brand/park-plaza/restaurant-bar" class="btn btn-secondary btn-sm mb-3">Book now</a>
            </div>
        </div>
    </div>
