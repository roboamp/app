<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="artand#39;otel berlin-mitte - Art Suite XL" class="lazy card__image loaded" src="https://media.radissonhotels.net/image/Park-Plaza-Westminster-Bridge-London/Suite/16256-114932-f67629515_3XL.jpg?impolicy=Card" data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-westminster-bridge-london" target="_self" class="color-black" tabindex="0">
                                Park Plaza Westminster Bridge London</a>

                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <small class="card--hotel__address text-left">
                        <span class="card--hotel__address__address">200 Westminster Bridge Road</span>,
                        <span class="card--hotel__address__city">London</span>,
                        <span class="card--hotel__address__zip"> SE1 7UT</span>,
                        <span class="card--hotel__address__country">United Kingdom</span>
                    </small>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic"
                                 src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.5-MCID-5.svg"
                                 alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">22202 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic"
                     alt="Park Plaza Verudela Pula - Lounge Sea View"
                     class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Verudela-Pula/BarLounge/16256-113843-f63146086_3XL.jpg?impolicy=CustomCrop&cwidth=670&cheight=384impolicy=Card"
                     data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-verudela-pula" target="_self" class="color-black" tabindex="0" data-di-id="di-id-d6c6ab20-3a45cfcc">
                                Park Plaza Verudela Pula</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <small class="card--hotel__address text-left">
                        <span class="card--hotel__address__address">Verudella 11</span>,
                        <span class="card--hotel__address__city">Pula</span>,<span class="card--hotel__address__zip"> 52100</span>,
                        <span class="card--hotel__address__country">Croatia</span>
                    </small>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">1035 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6 slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide12" data-slick-index="2" aria-hidden="true">
    <div class="card card--hotel text-center">
        <div>
            <amp-img width="670" height="384" layout="intrinsic" alt="Park Plaza Verudela Pula - Lounge Sea View"
                     class="lazy card__image loaded"
                     src="https://media.radissonhotels.net/image/Park-Plaza-Victoria-Amsterdam/Guestroom/16256-114950-f68882636_3XL.jpg?impolicy=Card" data-was-processed="true"></amp-img>
        </div>
        <div class="card__body">
            <div class="row">
                <div class="col-9">
                    <div class="hotel-name">
                        <h2 class="h2 card__title text-left mb-3">
                            <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-victoria-amsterdam" target="_self" class="color-black" tabindex="-1" data-di-id="di-id-e3c69e66-2375c47c">

                                Park Plaza Victoria Amsterdam</a>
                        </h2>
                    </div>
                </div>
                <div class="col-3 text-right">
                    <div class="logo-brand rco"></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 d-flex">
                    <i class="icon-pin xs"></i>
                    <a href="https://www.radissonhotels.com/en-us/hotels/park-plaza-victoria-amsterdam" target="_self" class="color-black" tabindex="0" data-di-id="di-id-91202887-d0e19292">
                        <small class="card--hotel__address text-left">
                            <span class="card--hotel__address__address">Damrak 1-5</span>,
                            <span class="card--hotel__address__city">Amsterdam</span>,
                            <span class="card--hotel__address__zip"> 1012 LG</span>,
                            <span class="card--hotel__address__country">Netherlands</span>
                        </small>

                    </a>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5 d-flex align-items-top">
                    <div class="hotel-reviews">
                        <amp-img width="119" height="20" layout="intrinsic" src="https://www.tripadvisor.com/img/cdsi/img2/ratings/traveler/4.0-MCID-5.svg" alt="" class="tripadvisor-rating"></amp-img>
                        <span class="review-number">883 reviews</span>
                    </div>
                </div>
                <div class="col-7">
                    <button class="btn btn-secondary btn-block text-uppercase font-medium" rel="nofollow" tabindex="-1">Check availability</button>
                </div>
            </div>
        </div>
    </div>
</div>
