<div class="banner" id="banner">
    <div class="banner-item banner-item-style-1">

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="banner-bg-1">
                        <div class="banner-content">
                            <h1 class="banner-title">Lightning fast webpages<br/>with zero programming</h1>
                            <p class="banner-text">With just one line of code, we make your website insanely quick</p>
                            <div class="banner-button">
                                <a class="btn btn-primary" href="#features">Features</a>
                            </div>
                            <div class="play-button">
                                <a class="play-video" href="https://www.youtube.com/watch?v=lBTCB7yLs8Y"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                        <!-- <div class="download-links">
                            <h5>Available on:</h5>
                            <div class="icons">
                                <a href="#"><i class="fa fa-android"></i></a>
                                <a href="#"><i class="fa fa-apple"></i></a>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="banner-image hidden-sm hidden-xs">
                        <img class="img-responsive mocks-background" src="{{asset('img/mocks/iPhone5-white-1.png')}}" alt="Banner Mock">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>