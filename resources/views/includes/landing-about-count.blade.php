<div class="app-about primary-gradient-bg section section-padding " id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="about">
                    <div class="section-header">
                        <h3 class="section-title">About Our App</h3>
                    </div>
                    <div class="about-text">
                        <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur. From a Lorem Ipsum passage, and going through the cites of the word in classical literature.</p>
                        <p>discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.</p>
                    </div>
                    <div class="app-download-icon">
                        <a href="#"><img src="{{asset('img/google-play.png')}}" alt=""></a>
                        <a href="#"><img src="{{asset('img/app-store.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 ">
                <div class="counter-items ">
                    <div class="counter-item">
                        <div class="counter-content">
                            <div class="counter-icon"><i class="fa fa-cloud-download"></i></div>
                            <div class="counter-body">
                                <h3 class="fact-number">100K+</h3>
                                <p class="fact-text">App Download</p>
                            </div>
                        </div>
                    </div>
                    <div class="counter-item small">
                        <div class="counter-content">
                            <div class="counter-icon"><i class="fa fa-heart"></i></div>
                            <div class="counter-body">
                                <h3 class="fact-number">50000</h3>
                                <p class="fact-text">Happy User</p>
                            </div>
                        </div>
                    </div>
                    <div class="counter-item small">
                        <div class="counter-content">
                            <div class="counter-icon"><i class="fa fa-comments"></i></div>
                            <div class="counter-body">
                                <h3 class="fact-number">2567</h3>
                                <p class="fact-text">Feedback</p>
                            </div>
                        </div>
                    </div>
                    <div class="counter-item small">
                        <div class="counter-content">
                            <div class="counter-icon"><i class="fa fa-star"></i></div>
                            <div class="counter-body">
                                <h3 class="fact-number">4.80</h3>
                                <p class="fact-text">User Rating</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>