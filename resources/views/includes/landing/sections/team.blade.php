@isset($team_members)
    <!-- Team -->
        <div class="team-area section section-padding" id="team">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-xs-12">
                        <div class="section-header text-center">
                            <h3 class="section-title">ROBOAMP Crew</h3>
                            <p class="section-text"></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="team-items">
                        @foreach($team_members as $member)
                            <div class="col-md-4">
                                @include ('includes.landing.landing-team-members', $member)
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endisset