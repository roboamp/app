
@isset($case_studies)
    <!-- Screenshot carousel -->
    <div class="screenshot-area section section-padding" id="success_stories">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-xs-12">
                    <div class="section-header text-center">
                        <h3 class="section-title">Tier one brands are already using AMP</h3>
                        <!-- <p class="section-text">They've already converted!</p> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="screenshot-carousel-1" class="card-container grid screenshot-carousel-1 owl-carousel owl-theme">
                    @foreach ($case_studies as $case_study)
                        @include ('includes.landing.landing-screenshot-carousel', $case_study)
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endisset