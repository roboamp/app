@isset($carousel_testimonials)

        <!-- Testimonial -->
        <div class="testimonial section section-padding bottom-0" id="testimonial">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-xs-12">
                        <div class="section-header text-center">
                            <h3 class="section-title">People are Saying</h3>
                            <p class="section-text">Folks love the results of AMP!</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="testimonials-wrap">
                        <div class="testimonial-carousel-1 owl-carousel" id="testimonial-carousel">
                            @foreach ($carousel_testimonials as $testimonial)
                                @include ('includes.landing.landing-testimonials', $testimonial)
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endisset