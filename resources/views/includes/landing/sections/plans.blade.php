@isset($plans)
        <!-- Pricing Table -->
        <div class="pricing-area primary-gradient-bg section section-padding bottom-0" id="pricing">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-xs-12">
                        <div class="section-header text-center">
                            <h3 class="section-title text-white">Instant speed. Zero programming.</h3>
                            <p class="section-text text-white"><strong>Close more deals. Make more sales.</strong></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="pricing-packages">
                        @foreach ($plans as $plan)
                            @include ('includes.landing.landing-pricing-table', $plan)
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endisset