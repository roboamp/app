<div class="screenshot-item">
	<a class="card" href="{{$url}}" target="_blank" rel="noopener">
		<div class="card-inner">
			<div class="card-meta">
				<span class="light">{{$date}}</span>
			</div>
			<div class="card-logo">
				<!--<img alt="..."
				<img alt="Testimonial Image"
					height="80"
					layout="fixed"
					noloading
					src="{{--asset($image)--}}"
				width="231"></img>-->

				<picture height="80"
						 layout="fixed"
						 noloading width="231">
					<source type="image/webp" srcset="images/webp/screenshot/{{$image}}.webp" alt="..">

					<img src="images/jpg/screenshot/{{$image}}.png" alt="..">
				</picture>

			</div>
			<div class="card-title">
				<h4>{{$content}}</h4>
			</div>
			<div class="card-link">
				<p class="text-label">  READ THE CASE STUDY
				</p>
			</div>
		</div>
	</a>
</div>
