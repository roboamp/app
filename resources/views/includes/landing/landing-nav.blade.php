<nav class="navbar navbar-default" data-spy="affix" data-offset-top="60">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="{{asset('img/nav-logo2.png')}}" alt="ROBOAMP Logo"></a>
        </div>

        <div class="collapse navbar-collapse onepage-nav" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#features">Overview</a></li>
                <li><a href="#success_stories">Success Stories</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li><a href="#team">Team</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>