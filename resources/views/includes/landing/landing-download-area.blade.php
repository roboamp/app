<div class="download-area section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="section-header text-left">
                    <h3 class="section-title">Download App Now</h3>
                    <p class="section-text">Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy</p>
                </div>
                <div class="download-icons">
                    <a href="#"><img src="{{asset('img/google-play.png')}}" alt=""></a>
                    <a href="#"><img src="{{asset('img/app-store.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <img class="img-responsive download-side-img" src="{{asset('img/banner/download-mockup.png')}}" alt="">
            </div>
        </div>
    </div>
</div>