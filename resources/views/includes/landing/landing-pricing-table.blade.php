<div class="col-sm-6 col-xs-12">
    <div class="package">
        <div class="package-header">
            <h4 class="pack-name">{{$title}}</h4>
            <p>{{$sub_header}}</p>
        </div>
        <div class="package-body">
            <p class="pack-price {{$price_color}}">{{$price}}</p>
            <p class="text-center">Per Domain</p>
            <ul class="pack-feature-list">
                @foreach ($feature_list as $feature)
                    <li>{{$feature}}</li>
                @endforeach
            </ul>
        </div>
        <div class="pack-footer">
            <a class="btn btn-primary {{$button_color}}" href="/customers/register">Get Started</a>
        </div>
    </div>
</div>
            