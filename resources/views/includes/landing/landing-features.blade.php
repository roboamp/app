<div class="app-features section section-padding" id="features">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-xs-12">
                <div class="section-header text-center">
                    <h3 class="section-title">What can <span style="font-weight: 600;">ROBOAMP</span> do for you?</h3>
                    <p class="section-text">Our mission is to make the web better for all with just a single line of code.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="mocked-features style-1">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="feature-item">
                        <div class="feature-icon">
                            <picture class="featured-mock img-responsive">
                                <source type="image/webp" srcset="images/webp/icons/moc-f-3.webp" alt="SEO Image">
                                <img src="images/jpg/icons/moc-f-3.png" alt="SEO Image">
                            </picture>

                        </div>
                        <div class="feature-body">
                            <h4 class="feature-title">Improved SEO</h4>
                            <p>When searching on mobile, AMP articles are prioritized in a Top Stories carousel above other search results.</p>
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="feature-icon">
                            <picture class="featured-mock img-responsive">
                                <source type="image/webp" srcset="images/webp/icons/moc-f-2.webp" alt="Performance Image">
                                <img src="images/jpg/icons/moc-f-2.png" alt="Performance Image">
                            </picture>
                        </div>
                        <div class="feature-body">
                            <h4 class="feature-title">Higher Performance</h4>
                            <p>Web pages and ads published in the AMP open-source format load near instantly, giving users a smooth, more engaging experience on mobile and desktop.</p>
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="feature-icon">
                            <picture class="featured-mock img-responsive">
                                <source type="image/webp" srcset="images/webp/icons/how-2.webp" alt="Revenue Image">
                                <img src="images/jpg/icons/how-2.png" alt="Revenue Image">
                            </picture>

                        </div>
                        <div class="feature-body">
                            <h4 class="feature-title">Maximize your revenue</h4>
                            <p>Conversions fall by 12 percent for every extra second a webpage takes to load. AMP gives users a faster experience everywhere -- on ads, landing pages or your entire website.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center hidden-sm hidden-xs mocks-background">

                    <picture class="featured-mock img-responsive">
                        <source type="image/webp" srcset="images/webp/mockup/feature-iPhone5-white.webp" alt="Mock iPhoneX">
                    <!--<source type="image/jp2" srcset="images/jp2/team/{{--$image}}.jp2" alt="Team Member">
                <source type="image/jxr" srcset="images/jxr/team/{{$image--}}.jxr" alt="Team Member">-->
                        <img src="images/jpg/mockup/feature-iPhone5-white.png" alt="Mock iPhoneX">
                    </picture>


                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 features-right">
                    <div class="feature-item">
                        <div class="feature-icon">
                            <picture class="featured-mock img-responsive">
                                <source type="image/webp" srcset="images/webp/icons/moc-f-6.webp" alt="UX Image">
                                <img src="images/jpg/icons/moc-f-6.png" alt="UX Image">
                            </picture>
                        </div>
                        <div class="feature-body">
                            <h4 class="feature-title">Richer UX</h4>
                            <p>AMP pages load in under one second. Your customer's get what they’re looking for, when they’re looking for it.</p>
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="feature-icon">

                            <picture class="featured-mock img-responsive">
                                <source type="image/webp" srcset="images/webp/icons/moc-f-4.webp" alt="ROI Image">
                                <img src="images/jpg/icons/moc-f-4.png" alt="ROI Image">
                            </picture>
                        </div>
                        <div class="feature-body">
                            <h4 class="feature-title">Maximize your ROI</h4>
                            <p>With AMPHTML Ads, your ads can serve on both AMP and non-AMP pages, meaning you can build your ads once and deliver a memorable brand experience everywhere.</p>
                            
                        </div>
                    </div>
                    <div class="feature-item">
                        <div class="feature-icon">
                            <picture class="featured-mock img-responsive">
                                <source type="image/webp" srcset="images/webp/icons/moc-f-5.webp" alt="Flexibility Image">
                                <img src="images/jpg/icons/moc-f-5.png" alt="Flexibility Image">
                            </picture>
                        </div>
                        <div class="feature-body">
                            <h4 class="feature-title">Flexibility and Results</h4>
                            <p>Publishers and advertisers can decide how to present their content and what technology vendors to use, all while maintaining and improving key performance indicators.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>