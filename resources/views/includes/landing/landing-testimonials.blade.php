<div class="testimonial-item">
    <div class="reviewer-pic">
        <div class="">
            <picture alt="Testimonails">
                <source type="image/webp" srcset="images/webp/client/{{$image}}.webp" alt="Team Member">
            <!--<source type="image/jp2" srcset="images/jp2/team/{{--$image}}.jp2" alt="Team Member">
                <source type="image/jxr" srcset="images/jxr/team/{{$image--}}.jxr" alt="Team Member">-->
                <img src="images/jpg/client/{{$image}}.png" alt="Team Member">
            </picture>
        </div>
    </div>
    <h4 class="reviewer-name">{{$name}}</h4>
    <blockquote>{{$testimonial}}</blockquote>
    <!-- <div class="star-rating">
        @foreach ($rating as $key => $value)
            @if($value != '')
                <span class="star on"></span>
            @else
                <span class="star"></span>
            @endif
        @endforeach
    </div> -->
</div>
