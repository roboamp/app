<div class="how-it-works section" id="how-it-works">
    <div class="container">
        <!-- <div class="row">
            <div class="col-md-6 col-md-offset-3 col-xs-12">
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-6 col-sm-12 text-center url-try-it-yourself">
                <div class="section-header text-center">
                    <h3 class="section-title">Try it for yourself</h3>
                    <p class="section-text">Instant speed with zero programming</p>
                    <p class="section-text">Start Free Trial!</p>
                </div>
                <div class="contact-area">
                    <form id="demoForm" class="contact-form" method="post">
                        <label for="demoemail" class="visuallyhidden">EMAIL</label>
                        <input id="demoemail" class="text-center" type="email" name="email" placeholder="Enter Email" required/>
                        <button type="submit">Request a demo!</button>
                        <div class="clearfix"></div>
                        <p class="input-success">Your email has been sent!  We will be contacting you soon.</p>
                        <p class="input-error">Sorry, something went wrong. try again later.</p>
                    </form>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1 col-sm-12 text-center iphone">
                <picture>
                    <source type="image/webp" srcset="images/webp/mockup/iphone.webp" alt="iPhone Image">
                <!--<source type="image/jp2" srcset="images/jp2/team/{{--$image}}.jp2" alt="Team Member">
                <source type="image/jxr" srcset="images/jxr/team/{{$image--}}.jxr" alt="Team Member">-->
                    <img src="images/jpg/mockup/iphone.png" alt="iPhone Image">
                </picture>
            </div>
        </div>
    </div>
</div>
