<head>
    @include('includes.landing.analytics')
    <!-- Site information -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Do you want your website to load in one second or less? ROBOAMP is the easiest way to make your website lighting fast. No coding needed. Try it now!">
    <link rel="manifest" href="{{asset('manifest.json')}}" />
    <title>ROBOAMP</title>

    <!-- External CSS -->
    <!-- <link rel="stylesheet" href="{{asset('css/testing_css.css')}}"> -->
    <style type="text/css">.nav>li,.nav>li>a,.navbar{position:relative}.nav>li,.nav>li>a,.navbar-brand>img{display:block}.nav{padding-left:0;margin-bottom:0;list-style:none}.nav>li>a{padding:10px 15px}.navbar{min-height:50px;margin-bottom:20px;border:1px solid transparent;border-radius:4px}.navbar-collapse{padding-right:15px;padding-left:15px;overflow-x:visible;-webkit-overflow-scrolling:touch;border-top:1px solid transparent;-webkit-box-shadow:inset 0 1px 0 rgba(255,255,255,.1);box-shadow:inset 0 1px 0 rgba(255,255,255,.1)}.navbar-brand{float:left;height:50px;padding:15px;font-size:18px;line-height:20px}.navbar-default{background-color:#f8f8f8;border-color:#e7e7e7}.navbar-default .navbar-brand{margin-top:50px;margin-bottom:5px;height:70px;padding:10px 0}.navbar-default .navbar-nav{margin-top:60px;margin-bottom:0;-webkit-transition:all .3s ease;transition:all .3s ease}*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}@media (min-width:768px){.navbar-header,.navbar-nav>li{float:left}.navbar-nav{float:left;margin:0}.navbar-nav>li>a{padding-top:15px;padding-bottom:15px}.navbar-right{float:right!important;margin-right:-15px}}.app-features{margin-top:100px}.mocked-features.style-1 .feature-item .feature-body{width:100%;float:left}.mocked-features.style-1 .feature-item .feature-icon{border-radius:50%;background-color:#def0ff;width:70px;height:70px;line-height:60px;text-align:center;font-size:2.6rem;float:left;margin-right:30px;margin-top:30px;color:#3aa6fe}.mocked-features.style-1 .col-md-4:first-child .feature-item .feature-icon{float:right;margin-right:0;margin-left:30px}.mocked-features.style-1 .feature-item .feature-body .feature-title{margin-top:0;margin-bottom:15px;font-size:24px;font-weight:400}.mocked-features.style-1 .feature-item .feature-icon+.feature-body{width:calc(100% - 100px);margin-top:-7px}.section-header{margin-bottom:70px}.section-header .section-title{margin:0;font-size:40px;line-height:1}.section-header .section-text{margin-top:25px;margin-bottom:0;font-size:20px;color:#505050;line-height:30px}</style>
    <link rel="stylesheet" href="{{asset('css/landing_css.css')}}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Barlow:400,500,600,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600" rel="stylesheet">


    <!-- Favicon -->
    <link rel="icon" href="{{asset('img/logo-1.png')}}">
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/logo-1.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/logo-1.png')}}">

    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->

    


</head>