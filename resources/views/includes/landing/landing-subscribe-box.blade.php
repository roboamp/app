<div class="subscribe-area primary-gradient-bg section section-padding" id="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-xs-12">
                <div class="subscribe-body">
                    <div class="logo"><img src="{{asset('img/logo-2.png')}}" alt="ROBOAMP Logo"></div>
                    <h2 class="subscribe-heading">Take the first step to optimize your webpage</h2>
                    <form id="subscriptionForm" class="subscription subscribe-form" method="post">
                        <label for="subscription_email" class="visuallyhidden">EMAIL</label>
                        <input id="subscription_email" type="email" name="email" placeholder="Enter email address" required="">
                        <button type="submit">SUBSCRIBE</button>
                        <div class="clearfix"></div>

                    </form>
                    <p class="input-success newsletter-success">You have successfully subscribed!</p>
                    <p class="input-error newsletter-error">There was an error, please try again.</p>

                </div>
            </div>
        </div>
    </div>
</div>	