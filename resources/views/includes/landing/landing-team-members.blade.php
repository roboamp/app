<div class="member">
    <div class="member-image">
            <picture>
                <source type="image/webp" srcset="images/webp/team/{{$image}}.webp" alt="Team Member">
                <img src="images/jpg/team/{{$image}}.jpg" alt="Team Member">
            </picture>

    </div>
    <div class="member-content">
        <h4 class="member-title"><a href="#">{{$name}}</a></h4>
        <p class="member-position">{{$title}}</p>
        <div class="social">
            @foreach ($social_media as $key =>$value)
                @if($value != '')
                    <a href="{{$value}}" target="_blank" rel="noopener" title="{{$key}}"><i class="fa fa-{{$key}}"></i></a>
                @endif
            @endforeach
        </div>
    </div>
</div>