<footer>
    <div class="container">
        <div class="footer-bottom">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="footer-logo">
                        <img src="{{asset('img/nav-logo2.png')}}" alt="Footer Logo">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 sm-text-center">
                    <!-- Do we really need these? -->
                    <!-- <ul class="footer-menu">
                        <li><a href="#">Features</a></li>
                        <li><a href="#">Overview</a></li>
                        <li><a href="#">Screenshots</a></li>
                        <li><a href="#">Pricing</a></li>
                        <li><a href="#">Contact</a></li>
                    </ul> -->
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="footer-social">
                        <!-- <a href="#"><i class="fa fa-facebook"></i></a> -->
                        <a href="https://twitter.com/_ROBOAMP_" target="_blank" rel="noopener" title="twitter"><i class="fa fa-twitter"></i></a>
                        <!-- <a href="#"><i class="fa fa-google-plus"></i></a> -->
                        <!-- <a href="#"><i class="fa fa-linkedin"></i></a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>