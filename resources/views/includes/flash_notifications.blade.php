@if (session('error'))
    <div class="alert alert-danger" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{ session('error') }}
    </div>
@endif
@if (session('success'))


    <div class="alert alert-success" style="text-align: center">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{session('success')}}
    </div>
@endif

