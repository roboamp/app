@extends('spark::layouts.app')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/tippy.js@2.5.0/dist/tippy.all.min.js"></script>



    <home :customer="user" inline-template>
        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">

            @include('customers.dashboard.includes.select_platform',array('login_type'=>'users','property'=>$property))

                    </div>
                </div>
            </div>
        </div>
    </home>
    @include('customers.js.select_platform')

@endsection
