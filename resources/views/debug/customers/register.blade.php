<?php

if($testing){
    $faker=Faker\Factory::create();
    $name=$faker->name;
    $email=$faker->email;
    $psw=$faker->password();
    $checked="checked";
    $testing_customer_txt="<input type='hidden' name='txt_testing_customer' class='form-control' placeholder='1'   value='1'>";


}else{
    $name="";
    $email="";
    $psw="";
    $checked="";
    $testing_customer_txt="";


}
$discount_txt="";
if(isset($discount_code)){
    if($discount_code!=""){
        $discount_txt="<input type='hidden' name='txt_discount_code' class='form-control' placeholder='Discount Code'   value='".$discount_code."'>";
    }else{
        $discount_txt="";
    }
}
?>

<input type="text" name="name" value="{{ old('name',$name) }}" class="form-control" placeholder="Name" required autofocus>
<input type="text" name="email" value="{{ old('email',$email) }}" class="form-control" placeholder="Email" required autofocus>
<input type="password" name="password" class="form-control" placeholder="Password" required value="{{$psw}}" >
<input type="password" name="password_confirmation" class="form-control" placeholder="Re-type Password" required  value="{{$psw}}">
{!!$discount_txt!!}
{!!$testing_customer_txt!!}
<label class="checkbox">
    <input type="checkbox" value="agree this condition" name="terms" required {{$checked}}> I agree to the <a href="/terms">Terms of Service and Privacy Policy</a>
</label>
