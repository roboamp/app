<!doctype html>
<!--{/*
@info
        Generated on: Fri, 09 Feb 2018 01:27:57 GMT
        Initiator: MobilizeToday AMP Generator
        Generator version: 0.9.6
        */}-->
<html ⚡ lang="en">
<head>
    <meta charset="utf-8">
    <title>Playboy | Articles, Interviews &amp; More Since 1953 | Playboy</title>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async="async" custom-element="amp-accordion"></script>
    <script src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async="async" custom-element="amp-sidebar"></script>
    <link rel="canonical" href="http://www.playboy.com/">
    <meta name="generator" content="https://html2amp.mobilizetoday.ru">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <meta content="The original men&#39;s magazine, Playboy&#39;s been pushing (and removing) buttons for 60+ years. Playboy.com features: beautiful women &amp; celebrities; sex, culture, humor &amp; style; Hef, Playmates &amp; the Mansion." name="description">
    <meta content="summary_large_image" name="twitter:card">
    <meta content="@playboy" name="twitter:site">
    <meta content="Playboy | Articles, Interviews &amp; More Since 1953" name="twitter:title">
    <meta content="The original men&#39;s magazine, Playboy&#39;s been pushing (and removing) buttons for 60+ years. Playboy.com features: beautiful women &amp; celebrities; sex, culture, humor &amp; style; Hef, Playmates &amp; the Mansion." name="twitter:description">
    <meta content="https://cdn.playboy.com/assets/logo-black-box-b444c507e346c7476f6b5872fd3933e45d72a1f05c79d2f3a571f52668e7768d.png" name="twitter:image">
    <meta content="290743504454630" property="fb:app_id">
    <meta content="6280019085" property="fb:pages">
    <meta content="http://www.playboy.com/" property="og:url">
    <meta content="article" property="og:type">
    <meta content="Playboy" property="og:site_name">
    <meta content="Playboy | Articles, Interviews &amp; More Since 1953" property="og:title">
    <meta content="The original men&#39;s magazine, Playboy&#39;s been pushing (and removing) buttons for 60+ years. Playboy.com features: beautiful women &amp; celebrities; sex, culture, humor &amp; style; Hef, Playmates &amp; the Mansion." property="og:description">
    <meta content="https://cdn.playboy.com/assets/logo-black-box-b444c507e346c7476f6b5872fd3933e45d72a1f05c79d2f3a571f52668e7768d.png" property="og:image">
    <meta content="image/jpeg" property="og:image:type">
    <meta content="438" property="og:image:width">
    <meta content="220" property="og:image:height">
    <meta content="Playboy | Articles, Interviews &amp; More Since 1953" itemprop="name">
    <meta content="The original men&#39;s magazine, Playboy&#39;s been pushing (and removing) buttons for 60+ years. Playboy.com features: beautiful women &amp; celebrities; sex, culture, humor &amp; style; Hef, Playmates &amp; the Mansion." itemprop="description">
    <meta content="https://cdn.playboy.com/assets/logo-black-box-b444c507e346c7476f6b5872fd3933e45d72a1f05c79d2f3a571f52668e7768d.png" itemprop="image">
    <meta content="Playboy | Articles, Interviews &amp; More Since 1953" name="sailthru.title">
    <meta content="playboy,playmates" name="sailthru.tags">
    <meta content="app-id=com.playboy.playboynow" name="google-play-app">
    <meta content="app-id=930678202 app-argument=pbnow://" name="itunes-app">
    <meta content="noindex, follow" name="robots">
    <meta content="noindex, follow" name="robots">
    <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
    <noscript data-amp-spec=""><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <style amp-custom="">@font-face {
            font-family: RabbitSlab;
            src: url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-Medium-5fcf3922f88c2e2f73bc592e539def6c793b95cf5c57d161b86a7b3cc265c772.woff) format("woff"),url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-Medium-e5aa70cea36623298af1c495d188dcc929945b8d4f3e9c9502c01dc791a4ccfe.ttf) format("truetype");
            font-style: normal;
            font-weight: 500
        }
        @font-face {
            font-family: RabbitSlab;
            src: url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-MediumItalic-5d9b969037cb69c54c91a0f2a424e964b1e79ab6adce24ab191be1420fd0fad3.woff) format("woff"),url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-MediumItalic-d325ab091c5a2a6239212c9a7effab08f2a64a3e1456e0460a1dcb521a1980b4.ttf) format("truetype");
            font-style: italic;
            font-weight: 500
        }
        @font-face {
            font-family: RabbitSlab;
            src: url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-Bold-f60d49db3701075e58745d7c458d92652bb6db92a3d4b0cf53cf58455dfdc087.woff) format("woff"),url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-Bold-a0144e7f6348c117432b65d006b57dc413adf514bba3a5125f5f3b4974daa409.ttf) format("truetype");
            font-style: normal;
            font-weight: 600
        }
        @font-face {
            font-family: RabbitSlab;
            src: url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-BoldItalic-ad13ab5af8893b2434d3817a1db48b70b25a4ce452491fab0977fefe5994f577.woff) format("woff"),url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-BoldItalic-5fa95f5356021914325994601400ba2b2614990d507cd4b2f0f902ed5b787d0c.ttf) format("truetype");
            font-style: italic;
            font-weight: 600
        }
        @font-face {
            font-family: RabbitSlab;
            src: url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-Black-10e227fe8916c11a46a262cb62ec37ac36c7f23abba8ac651d4f9b418314e7c3.woff) format("woff"),url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-Black-92cf096b7fd01c8288f054181104558f5712c5413ee37deab0874e15e9a0d31a.ttf) format("truetype");
            font-style: normal;
            font-weight: 700
        }
        @font-face {
            font-family: RabbitSlab;
            src: url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-BlackItalic-b7e501433a86146ce2acf795b55ec70260d862a0d2bf3141b9cdddbbe52a4426.woff) format("woff"),url(http://cdn.playboy.com/assets/rabbitslab/RabbitSlab-BlackItalic-dceb0dd3ae0b03781d1e72513d90139b5c35b5ae0a07279ff59729f639b2dbbb.ttf) format("truetype");
            font-style: italic;
            font-weight: 700
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-ExThin-5cacc1371cc6bdcb4951521b792dadecc634a2deb72bc54e2917b368a92714e9.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-ExThin-ff80b798fca8b3fc4557dafea3839b5d2e8688b5c105b417253641de79fa39b5.ttf) format("truetype");
            font-style: normal;
            font-weight: 100
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-ExThinItalic-da91a53c9f6fe36878a484963ea77f348e5ab1787e6bed959b459eadbde7293e.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-ExThinItalic-e5e6c5349da2362f7f535304f86af48d27011ffd365c26a1366d23e16b9dbd84.ttf) format("truetype");
            font-style: italic;
            font-weight: 100
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Thin-72855bb49675c9fedd8d790be0e3013903fd730b06cb5c3e076c1efe112bb598.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Thin-89c69a493f2816473306374bc9650a4810723c9c57942c4db6ff905b0a7cdefd.ttf) format("truetype");
            font-style: normal;
            font-weight: 200
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-ThinItalic-f7a9d207c0c57b116d26c0a3095a31dc16bf67f21bd5f9ad8edc5e3c71055320.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-ThinItalic-fbfc0b679ea4ce99f4e2b03cb98478e807accbcaf4a7a5946667de79bcedc3f3.ttf) format("truetype");
            font-style: italic;
            font-weight: 200
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Light-e0b3c237749a9afc1109b1332ca5498b437205faebf0cbd4ea2a0bce938d6b5b.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Light-c6e2ee0f6a78a6c71cce4c6f2cef85ce5317aca077a18474898280ee9321fbc8.ttf) format("truetype");
            font-style: normal;
            font-weight: 300
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-LightItalic-f2007d5364f5e078c815e3a6ff2308d3dbe4930357b014de0bf5b6b9bfc5cc94.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-LightItalic-f2abfeeff93f8dd3063bead0c33529d71ab25dc154f6cb9d3b69d042ab5c8915.ttf) format("truetype");
            font-style: italic;
            font-weight: 300
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Regular-2e4a68670b74b0ff8e7aa88febd9c4a5505878e2f7f4cc5abbac73055c4d5f58.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Regular-db3976c941fe61ad24ed9d130e86c28e5a493eb8f0b345b07ef1c19a8754a201.ttf) format("truetype");
            font-style: normal;
            font-weight: 400
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-RegItalic-e664afbb352705a060a5e84a4aa890aabfb537ab3fcf7c0524aa2bee50524729.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-RegItalic-a7357958b024ca64e5cf23b656f870477986cc4cccff0b59568b6598963d9042.ttf) format("truetype");
            font-style: italic;
            font-weight: 400
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Medium-2a65b2cf5a29c9512fe01cac922e7d7a50a5b52ed305d71dd2a74757f24aa2d8.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Medium-a26c5b8c1301a0cd2acf5ec34ec1514b8eabffd11633f6b0f7fbb2f6770849e6.ttf) format("truetype");
            font-style: normal;
            font-weight: 500
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-MedItalic-1bc1b56bb8fedac9c002664fbd55d93947739bd571e9f508b4ed8b69006c83ba.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-MedItalic-241ca70917b8124d7d74cea5e299f7398a44db3b26be26b335c532766b41743b.ttf) format("truetype");
            font-style: italic;
            font-weight: 500
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Bold-7560f3997eba4185f99ed865bdc21d434402cddd16ee3c0f2ed0d2dd3f41ef73.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Bold-1acf1829efb56763ef66d52e75fa51ef37851602db767c8374719e550f122dcd.ttf) format("truetype");
            font-style: normal;
            font-weight: 700
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-BoldItalic-1fd921a50a8c47b963ca3e05dd17944c0ba542774079a215b2378a871fa96f3f.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-BoldItalic-8f2ae0e439df4340706bdac0a9f8cb26242dd954517a3d15a113cfb15699f045.ttf) format("truetype");
            font-style: italic;
            font-weight: 700
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Ultra-23f52adfef07f0de5a0ff20024cf5e2275ec2b1c2383253f85e3e413ad458b5b.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-Ultra-e36105257d190aad1a2c1f07586ccf755ab2a3fcc6cc9e247b93072ee642dc20.ttf) format("truetype");
            font-style: normal;
            font-weight: 900
        }
        @font-face {
            font-family: AlrightSans;
            src: url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-UltraItalic-0f3d03806a9d4239ebf4814f0e4738acafdf981313ead29a91d514e2239cf4b8.woff) format("woff"),url(http://cdn.playboy.com/assets/alrightsans/AlrightSans-UltraItalic-4c49c19ab6029947b5f3f6b515ff97cb29c1176afa0627fd5671b3a110e912d5.ttf) format("truetype");
            font-style: italic;
            font-weight: 900
        }
        a,
        amp-img,
        aside,
        body,
        div,
        em,
        footer,
        h2,
        h3,
        h5,
        header,
        html,
        iframe,
        li,
        nav,
        p,
        section,
        span,
        strong,
        ul {
            margin: 0;
            border: 0
        }
        aside,
        footer,
        header,
        nav,
        section {
            display: block
        }
        body {
            line-height: 1
        }
        body {
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
            padding-top: 60px;
            position: relative;
            overflow-x: hidden;
            color: #000;
            font-family: AlrightSans,"Lucida Grande","Lucida Sans Unicode","Lucida Sans",Geneva,Verdana,sans-serif;
            font-size: 16px;
            font-weight: 400;
            line-height: 1.5
        }
        body h2,
        body h3,
        body h5 {
            font-family: RabbitSlab,Georgia,serif
        }
        body a,
        body p,
        body span {
            color: inherit;
            font-weight: 300;
            font-family: AlrightSans,"Lucida Grande","Lucida Sans Unicode","Lucida Sans",Geneva,Verdana,sans-serif;
            line-height: 1.5em
        }
        body p {
            margin: 20px 0;
            font-size: 16px;
            font-weight: 400;
            line-height: 25px
        }
        body a {
            text-decoration: none;
            cursor: pointer
        }
        body a:hover {
            color: #999
        }
        body amp-img {
            width: 100%
        }
        body strong {
            font-weight: 700
        }
        body em {
            font-style: italic
        }
        #social-icon-sprite {
            display: none
        }
        .main.body {
            overflow: hidden;
            background-color: #fff
        }
        .content.left {
            float: none
        }
        .container {
            margin: 0 auto
        }
        .nsfw {
            color: #ad7cab
        }
        .nsfw:visited {
            color: #ad7cab
        }
        .bg-bunny {
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/bunny20x30-47d08f18fecbed38462ee3f8decbff3d432e30c98fe22c49429cf5b7ff5590a0.svg);
            background-repeat: no-repeat
        }
        .bg-bunny-inverse {
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/bunny-inverse-552163f3d0bf7acaa9b26fee81c5b076fdddf5310c748864f331c89f3353f4c2.svg);
            background-repeat: no-repeat
        }
        a.button {
            display: inline-block;
            line-height: 50px;
            text-transform: uppercase;
            font-size: 14px;
            width: 100%;
            margin-top: 5px;
            text-align: center
        }
        li.layout {
            list-style: none
        }
        .module {
            padding: 25px;
            margin-bottom: 15px
        }
        .left {
            float: left
        }
        .right {
            float: right
        }
        .box-1 {
            box-shadow: 1px 1px 3px #aaa
        }
        #social-icon-sprite {
            display: none
        }
        .padless-right {
            margin-right: -15px
        }
        .ad {
            padding: 3px 0;
            text-align: center;
            position: relative
        }
        .ad:after {
            content: "Advertisement";
            color: #999;
            position: absolute;
            font-size: 10px;
            left: .5em;
            bottom: -15px;
            font-family: AlrightSans,"Lucida Grande","Lucida Sans Unicode","Lucida Sans",Geneva,Verdana,sans-serif
        }
        .ad:empty {
            display: none;
            margin: 0;
            width: 0
        }
        .ad:empty:after {
            content: ""
        }
        .ad-bg {
            background: #fff;
            width: 100%;
            position: fixed;
            z-index: 1;
            bottom: 0
        }
        .ad-bg .ad {
            margin: 0 auto
        }
        .ad-bg .ad:after {
            content: ""
        }
        body > .ad {
            background-color: #fff;
            margin: 0 auto
        }
        body > .ad:after {
            content: ""
        }
        .main.content .ad {
            margin: 10px auto;
            width: 300px
        }
        .sidebar .ad {
            position: relative;
            margin: 0 auto 20px;
            width: 300px;
            display: none
        }
        .sidebar .ad:after {
            content: ""
        }
        body {
            z-index: 0
        }
        .hamburger,
        header.main {
            z-index: 2
        }
        .icon.hamburger {
            z-index: 2
        }
        .magazine-cta {
            z-index: 2
        }
        .search-base {
            z-index: 2
        }
        .search-base .form {
            z-index: 2
        }
        input {
            width: calc(100% - 20px);
            font-size: 14px;
            font-weight: 500;
            margin-top: 1px;
            padding: 13px 10px;
            border: 0
        }
        input:focus {
            outline: 0
        }
        header.main {
            background-position: 20px 15px;
            background-color: #f6f6f6;
            width: calc(100% - 70px);
            height: 36px;
            padding: 12px 20px 12px 50px;
            position: fixed;
            left: 0;
            top: 0
        }
        header.main .logo {
            display: inline-block;
            width: 131px;
            height: 43px;
            background-image: url(http://cdn.playboy.com/assets/logo-outline-black-c028ac1e563ce2db073e806858a384b4cc99624dc7cb37f1ec2ade745c988c66.svg);
            background-repeat: no-repeat;
            background-size: contain
        }
        header.main .search {
            position: absolute;
            right: 54px;
            top: 10px;
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/search/search-50a61bec78b9d4e85846a50806d439f25c1dd45783d2f3d449ab5bae846b3029.svg)
        }
        .icon {
            height: 20px;
            width: 20px;
            display: inline-block;
            padding: 10px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: 20px
        }
        .hamburger {
            position: fixed;
            right: 10px;
            top: 10px;
            background-image: url(http://cdn.playboy.com/assets/buttons/hamburger-btn-5e797ff7ae4e509422188a36142049ca9bb8b2598dded0b2d042e160652d4a7f.svg)
        }
        nav.main {
            position: fixed;
            width: 100%;
            top: 0;
            bottom: 0;
            left: 100%;
            background-color: #000
        }
        nav.main .bg-global,
        nav.main .bg-super {
            background-color: inherit;
            color: #999;
            overflow: hidden
        }
        nav.main .bg-global {
            position: fixed;
            overflow-y: none;
            height: 70%;
            width: 105%
        }
        nav.main .global,
        nav.main .super {
            width: 100%
        }
        nav.main .super {
            display: none
        }
        nav.main .global {
            width: 100%;
            position: relative
        }
        nav.main .global li {
            display: block;
            margin-left: 15px;
            padding-left: 15px
        }
        nav.main .global li:first-child {
            margin-top: 10px
        }
        nav.main .global li.active {
            color: #fff;
            border-left: #fff solid 3px
        }
        nav.main .global li.nsfw a {
            color: #ad7cab
        }
        nav.main .global li.holiday span {
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/shop-13df96f9fb855a108968c8cddde936124775392a9ba1eb5f3ccf60ad391dfeb7.svg);
            background-position: 0 0;
            background-repeat: no-repeat;
            color: red;
            padding-left: 20px;
            background-size: 15px
        }
        nav.main .global .magazine-dropdown a {
            color: #f43
        }
        nav.main .global a {
            font-family: RabbitSlab,Georgia,serif;
            font-size: 21px;
            font-weight: 700;
            line-height: 40px;
            white-space: nowrap
        }
        nav.main .cta-link {
            background-color: inherit;
            position: absolute;
            bottom: 0;
            padding-bottom: 25px;
            left: 30px;
            height: 60px;
            width: calc(100% - 60px)
        }
        nav.main .cta-link a {
            display: block;
            overflow: visible;
            color: #fff
        }
        nav.main .cta-link span {
            display: inline-block;
            border-top: #fff solid 1px;
            margin-top: 10px;
            width: 270px;
            padding-top: 20px;
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/bunny-inverse-552163f3d0bf7acaa9b26fee81c5b076fdddf5310c748864f331c89f3353f4c2.svg);
            background-repeat: no-repeat;
            background-position: 250px 17px;
            background-size: 20px;
            font-family: RabbitSlab,Georgia,serif;
            font-size: 21px;
            font-weight: 700
        }
        nav.main .magazine-cta {
            position: absolute;
            top: 70px;
            right: 0;
            width: 300px;
            padding: 20px;
            background-color: #222;
            display: none
        }
        nav.main .magazine-cta .thumbnail {
            width: 30%;
            float: left
        }
        nav.main .magazine-cta .copy p {
            font-family: RabbitSlab,Georgia,serif;
            font-style: italic;
            line-height: 1.2;
            font-weight: 700;
            text-transform: none;
            color: #fff;
            text-align: left;
            float: right;
            width: 62%
        }
        nav.main .magazine-cta .button {
            color: #fff;
            background: #f43;
            text-transform: none;
            letter-spacing: 0;
            height: 40px;
            position: relative;
            width: auto;
            line-height: 40px;
            font-weight: 700;
            font-size: 16px;
            font-family: RabbitSlab,Georgia,serif;
            display: block;
            margin: 12px 0 0
        }
        nav.main .magazine-cta:hover {
            display: block
        }
        nav.main .login:hover {
            text-shadow: -1px 1px 8px #fff,1px -1px 8px #fff
        }
        nav.main .login:hover svg {
            fill: #fff
        }
        footer.main {
            width: calc(100% - 40px);
            padding: 20px 20px 100px;
            background-color: #000;
            background-position: 50% 95%;
            color: #fff;
            text-align: center
        }
        footer.main h3 {
            text-align: center;
            font-size: 30px;
            letter-spacing: -.5px;
            width: 100%;
            font-weight: 500;
            margin: 40px 0 15px
        }
        footer.main a,
        footer.main p {
            font-size: 14px;
            line-height: 1.3em;
            margin: 10px 0
        }
        footer.main a {
            display: block
        }
        footer.main a.button {
            font-weight: 500;
            text-transform: uppercase;
            background-color: #eee;
            color: #000;
            padding: 20px 0 17px;
            margin-top: 20px
        }
        footer.main p {
            font-weight: 500;
            clear: both
        }
        footer.main amp-img {
            margin: 0 auto
        }
        footer.main .footer-nav {
            margin: 2px 0
        }
        footer.main .wrapper {
            display: inline-block
        }
        footer.main > .wrapper {
            width: 100%;
            max-width: 1024px;
            margin: 0 auto;
            display: block
        }
        .login .toggle {
            width: auto;
            line-height: 60px;
            display: block;
            padding-left: 20px
        }
        .login .toggle span {
            color: #fff;
            font-size: 15px;
            font-weight: 500;
            line-height: 15px;
            text-transform: uppercase
        }
        .login .toggle svg {
            height: 16px;
            width: 40px;
            stroke: #fff;
            fill: transparent;
            position: relative;
            top: 3px;
            left: -7px
        }
        .search-base {
            background-color: #fff;
            display: none;
            width: 100%;
            position: relative;
            top: 0
        }
        .search-base .form {
            height: 60px;
            position: fixed;
            top: 0;
            width: 100%;
            background-color: #fff;
            max-width: inherit
        }
        .search-base .form:after {
            content: '';
            height: 20px;
            width: 20px;
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/search/search-50a61bec78b9d4e85846a50806d439f25c1dd45783d2f3d449ab5bae846b3029.svg);
            background-repeat: no-repeat;
            background-size: 20px;
            display: inline-block;
            position: absolute;
            top: 20px;
            left: 80px
        }
        .search-base .results {
            clear: both;
            overflow: none;
            position: relative;
            display: flex;
            display: -webkit-flex;
            flex-wrap: wrap;
            -webkit-flex-wrap: wrap
        }
        .search-base input {
            border: 0;
            background-color: #f6f6f6;
            padding: 0 50px 0 110px;
            margin: 0;
            height: 60px;
            width: calc(100% - 160px);
            line-height: 24px;
            font-size: 21px;
            letter-spacing: 0;
            font-weight: 500
        }
        .search-base .clear,
        .search-base .exit {
            position: absolute
        }
        .search-base .clear:after,
        .search-base .exit:after {
            content: '';
            border-right: #ccc solid 1px;
            position: absolute;
            right: 0;
            height: 40px;
            display: inline-block;
            top: 10px
        }
        .search-base .exit {
            padding: 20px
        }
        .search-base .clear {
            padding: 18px;
            display: none
        }
        .search-base .toolbar {
            font-size: 11px;
            line-height: 11px;
            background-color: #fff;
            margin: 0 15px 15px;
            border-bottom: 1px solid #999;
            width: calc(100% - 30px);
            height: 40px
        }
        .search-base .facet {
            display: inline-block;
            position: relative;
            cursor: pointer;
            width: 100px
        }
        .search-base .stats {
            float: right;
            display: inline-block
        }
        .search-base .series,
        .search-base .tags {
            padding: 60px 10px 0
        }
        .search-base .series header h5,
        .search-base .tags header h5 {
            margin: 15px 0;
            font-family: AlrightSans,"Lucida Grande","Lucida Sans Unicode","Lucida Sans",Geneva,Verdana,sans-serif;
            font-size: 14px;
            text-transform: uppercase;
            font-weight: 700
        }
        .search-base .series a,
        .search-base .tags a {
            border: 0;
            background-color: #eee
        }
        .search-base .series a h5 {
            white-space: nowrap;
            font-size: 20px;
            line-height: 20px;
            margin: 18px 50px 12px 10px;
            font-family: RabbitSlab,Georgia,serif;
            font-weight: 600
        }
        .search-base .series amp-img {
            width: 30px;
            height: 30px;
            float: left
        }
        .search-base .tags {
            padding-bottom: 60px
        }
        .search-base .tags a {
            text-decoration: none;
            background-color: #eee;
            padding: 13px 10px 8px;
            margin-right: 4px;
            font-size: 15px;
            text-transform: uppercase;
            font-weight: 700;
            display: inline-block;
            width: auto;
            line-height: 15px
        }
        .search-base .loading-indicator {
            text-align: center;
            border: #ccc solid 2px;
            line-height: 54px;
            margin: 15px;
            width: calc(100% - 49px);
            font-family: AlrightSans,"Lucida Grande","Lucida Sans Unicode","Lucida Sans",Geneva,Verdana,sans-serif
        }
        .search-base .padless-right {
            margin-right: -15px
        }
        .tile {
            width: calc(100% - 15px);
            flex: 1 0 auto;
            -webkit-flex: 1 0 auto;
            margin: 0 0 15px 0
        }
        .tile picture {
            position: relative;
            display: block;
            width: 100%
        }
        .tile picture:after {
            background-repeat: no-repeat;
            background-size: contain;
            content: "";
            width: 32px;
            height: 50px;
            position: absolute;
            bottom: 8px;
            left: 8px
        }
        .tile .article picture:after {
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/search/icon-article-5e69c8fdc1ba221c745c0f98262a0021d46a16d7a42951d3f200e2ef9ac0e3d1.svg)
        }
        .tile .gallery picture:after {
            background-image: url(http://cdn.playboy.com/dashboard_assets/icons/search/icon-gallery-d5b386f14735f363a050405c3ead154dc14205c36da1fe804ca3258379f593f1.svg)
        }
        .tile amp-img {
            width: 100%
        }
        .tile div {
            padding: 10px
        }
        .tile .title {
            font-family: AlrightSans,"Lucida Grande","Lucida Sans Unicode","Lucida Sans",Geneva,Verdana,sans-serif;
            font-size: 18px;
            color: #000;
            line-height: 1.1;
            font-weight: 500;
            margin-bottom: 10px
        }
        .tile .author {
            color: #666;
            font-size: 14px
        }
        .tile-container {
            padding: 0 15px;
            width: calc(100% - 30px);
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap
        }
        .tile {
            width: calc(50% - 15px);
            max-width: calc(50% - 15px);
            margin-right: 15px
        }
        .tile .title {
            font-size: 14px
        }
        .voices h2 {
            border-bottom: 1px solid rgba(0,0,0,.7);
            padding-bottom: 5px;
            margin-bottom: 0;
            font-weight: 500;
            font-size: 21px;
            letter-spacing: -.25px
        }
        .voices ul li {
            border-bottom: 1px solid rgba(0,0,0,.7);
            padding: 7.5px 0
        }
        .voices ul li > a {
            height: 70px;
            position: absolute
        }
        .voices ul li p {
            margin: 0 0 0 85px;
            height: 70px;
            display: inline-flex;
            display: -webkit-inline-flex;
            flex-direction: column;
            -webkit-flex-direction: column;
            vertical-align: middle;
            justify-content: center;
            -webkit-justify-content: center
        }
        .voices ul li p a:last-child {
            font-size: 13px;
            line-height: 15px
        }
        .home-social-links ul {
            display: flex;
            flex-wrap: nowrap;
            justify-content: space-between;
            display: -webkit-flex;
            -webkit-flex-wrap: nowrap;
            -webkit-justify-content: space-between;
            width: 100%
        }
        .home-social-links h2 {
            font-size: 21px;
            margin: 10px 0
        }
        .home-social-links li {
            list-style: none;
            max-width: 50px;
            max-height: 50px;
            flex: 1 0 auto
        }
        .home-social-links a {
            display: inline-block
        }
        .home-social-links span {
            display: block;
            padding: 10px;
            border-radius: 25px;
            width: 30px;
            height: 30px;
            background-color: #000
        }
        .home-social-links svg {
            width: 30px;
            height: 30px;
            fill: #fff;
            display: block;
            margin: 0 auto
        }
        .home-social-links .facebook:hover span {
            background-color: #4964a1
        }
        .home-social-links .twitter:hover span {
            background-color: #59adeb
        }
        .home-social-links .google-plus:hover span {
            background-color: #d94b3f
        }
        .home-social-links .instagram:hover span {
            background: radial-gradient(circle at 33% 100%,#fed373 4%,#f15245 30%,#d92e7f 62%,#9b36b7 85%,#515ecf)
        }
        .home-social-links .youtube:hover span {
            background-color: #cd201f
        }
        @font-face {
            font-family: slick;
            src: url(http://cdn.playboy.com/assets/slick-carousel/slick-06d80cf01250132fd1068701108453feee68854b750d22c344ffc0de395e1dcb.eot);
            src: url(http://cdn.playboy.com/assets/slick-carousel/slick-06d80cf01250132fd1068701108453feee68854b750d22c344ffc0de395e1dcb.eot?#iefix) format("embedded-opentype"),url(http://cdn.playboy.com/assets/slick-carousel/slick-26726bac4060abb1226e6ceebc1336e84930fe7a7af1b3895a109d067f5b5dcc.woff) format("woff"),url(http://cdn.playboy.com/assets/slick-carousel/slick-37bc99cfdbbc046193a26396787374d00e7b10d3a758a36045c07bd8886360d2.ttf) format("truetype"),url(http://cdn.playboy.com/assets/slick-carousel/slick-12459f221a0b787bf1eaebf2e4c48fca2bd9f8493f71256c3043e7a0c7e932f6.svg#slick) format("svg");
            font-weight: 400;
            font-style: normal
        }
        .feature-carousel {
            overflow: hidden;
            position: relative;
            margin-bottom: 15px
        }
        .feature-carousel .slide:not(.slick-slide):not(:first-child) {
            display: none
        }
        .feature-carousel .title {
            position: absolute;
            bottom: 0;
            background: -webkit-linear-gradient(bottom,#000,transparent);
            background: linear-gradient(bottom,#000,transparent);
            color: #fff;
            width: 100%
        }
        .feature-carousel .title h2 {
            padding: 0 20px 30px;
            font-size: 1.75em;
            line-height: 1;
            text-shadow: 1px 1px 2px rgba(0,0,0,.8)
        }
        .feature-carousel .title h2 span {
            border-bottom: 2px solid #fff;
            font-family: AlrightSans,"Lucida Grande","Lucida Sans Unicode","Lucida Sans",Geneva,Verdana,sans-serif;
            display: inline-block;
            font-size: 9px;
            margin-bottom: 8px;
            padding-bottom: 0;
            letter-spacing: 2px
        }
        .main.body {
            background-color: #fff
        }
        .main.body:after {
            content: "";
            display: table;
            clear: both
        }
        .sidebar {
            position: relative;
            width: 100%
        }
        .rbt-inline-0 {
            display: none;
            visibility: hidden
        }
        .rbt-inline-1 {
            fill: #fff
        }
        #search-toolbar {
            display: none
        }
        .rbt-inline-3 {
            display: none
        }
        .rbt-inline-4 {
            display: none
        }
        #rbt-sidebar {
            width: 80%;
            background: #eee
        }
        #rbt-sidebar ul li {
            display: block
        }
        #rbt-sidebar > nav {
            position: static
        }
        #rbt-sidebar {
            background-color: #000
        }
        #rbt-sidebar > nav > div > div > ul {
            display: block
        }
        section.rbt-accordion-section > header {
            background-color: transparent;
            padding: 0;
            margin: 0;
            border: 0
        }</style>
</head>
<body class=" verticals index" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<amp-sidebar id="rbt-sidebar" layout="nodisplay">
    <nav class="main">
        <div class="bg-super">
            <div class="container">
                <ul class="super">
                    <li><a data-label="Hop" href="https://hop.playboy.com/">Experience the Rabbit</a></li>
                    <li><a data-label="Pose For Playboy" href="http://www.playboy.com/pose-for-playboy">Pose for Playboy</a></li>
                    <li class="signup"><a class="nsfw" data-label="PB Plus Sign Up" href="http://join.playboyplus.com/track/MTAwMzk1OS4xMDAxMS4xMDIzLjMwNDEuMC4wLjAuMC4w/join?autocamp=pbcomtopbar">Sign Up For Playboy Plus</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="login"><template></template><template><a class="toggle" data-eventaction="Super Nav" data-eventlabel="Login Modal" href="#" id="login-modal"><svg role="img"><use xlink:href="#icon-bow-tie" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg><span>Sign In To Playboy</span></a></template></div>
        </div>
        <div class="bg-global">
            <div class="container">
                <ul class="global">
                    <li class="active"><a href="http://www.playboy.com/">Home</a></li>
                    <li><a data-label="Entertainment" href="http://www.playboy.com/entertainment">Entertainment</a></li>
                    <li><a data-label="Off Hours" href="http://www.playboy.com/off-hours">Off Hours</a></li>
                    <li><a data-label="Bunnies" href="http://www.playboy.com/bunnies">Bunnies</a></li>
                    <li><a data-label="Sex &amp; Culture" href="http://www.playboy.com/sex-and-culture">Sex &amp; Culture</a></li>
                    <li><a data-label="Heritage" href="http://www.playboy.com/heritage">Heritage</a></li>
                    <li><a data-label="Video" href="http://www.playboy.com/videos">Video</a></li>
                    <li class="nsfw ">
                        <div class="magazine-cta">
                            <div class="thumbnail">
                            <!--{/* @notice */}-->
                                <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                                <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/4PA10HPf7q6Y2uqGWoYqW/ce0f9027ec0fac1ec2b6e65d966a907f/magazine-cover.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="150" layout="fixed"></amp-img> -->
                            </div>
                            <div class="copy">
                                <p>Get the Magazine That Changed It All<a class="button" href="http://ma.playboyplus.com/age-gate?rul=http://www.playboyplus.com/go/?id=ed5075f65e183695">Subscribe</a></p>
                            </div>
                        </div><a data-label="PlayboyNSFW" href="http://ma.playboyplus.com/age-gate?rul=http://www.playboyplus.com/go/?id=ed5075f65e183695">PlayboyNSFW</a></li>
                    <li class="holiday ">
                        <div class="magazine-cta">
                            <div class="thumbnail">
                            <!--{/* @notice */}-->
                                <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                                <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/4PA10HPf7q6Y2uqGWoYqW/ce0f9027ec0fac1ec2b6e65d966a907f/magazine-cover.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="150" layout="fixed"></amp-img> -->
                            </div>
                            <div class="copy">
                                <p>Get the Magazine That Changed It All<a class="button" href="http://www.playboyshop.com/?utm_source=playboy&amp;utm_medium=native&amp;utm_campaign=topnav">Subscribe</a></p>
                            </div>
                        </div><span class="holiday"><a data-label="Shop" href="http://www.playboyshop.com/?utm_source=playboy&amp;utm_medium=native&amp;utm_campaign=topnav">Shop</a></span></li>
                    <li class="magazine-dropdown ">
                        <div class="magazine-cta">
                            <div class="thumbnail">
                            <!--{/* @notice */}-->
                                <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                                <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/4PA10HPf7q6Y2uqGWoYqW/ce0f9027ec0fac1ec2b6e65d966a907f/magazine-cover.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="150" layout="fixed"></amp-img> -->
                            </div>
                            <div class="copy">
                                <p>Get the Magazine That Changed It All<a class="button" href="http://www.playboy.com/magazine">Subscribe</a></p>
                            </div>
                        </div><a data-label="Magazine" href="http://www.playboy.com/magazine">Magazine</a></li>
                </ul>
            </div>
        </div>
        <div class="cta-link" data-action="Global Navigation" data-category="Navigation"><a data-label="Hop" href="https://hop.playboy.com/"><span>Experience the Rabbit</span></a></div>
    </nav>
</amp-sidebar><svg id="social-icon-sprite">

    <symbol id="icon-twitter" viewbox="0 0 19 19">
        <title>Twitter</title>
        <path d="M12.582156,2.5 C10.6868857,2.5 9.15003071,4.11419355 9.15003071,6.10483871 C9.15003071,6.38741935 9.18043612,6.66274194 9.23894349,6.92645161 C6.38636364,6.77645161 3.85718673,5.34129032 2.16461916,3.16 C1.86885749,3.69225806 1.69978501,4.3116129 1.69978501,4.97258065 C1.69978501,6.22290323 2.30558968,7.3266129 3.22650491,7.97306452 C2.66400491,7.95419355 2.13467445,7.79209677 1.67214373,7.52209677 L1.67168305,7.56758065 C1.67168305,9.31387097 2.85472973,10.7712903 4.42521499,11.1022581 C4.13682432,11.1845161 3.83369165,11.2285484 3.52088452,11.2285484 C3.2997543,11.2285484 3.08461302,11.2058065 2.875,11.1641935 C3.31173219,12.5959677 4.57954545,13.6382258 6.08138821,13.6677419 C4.90663391,14.6345161 3.42644349,15.2108065 1.81864251,15.2108065 C1.54176904,15.2108065 1.26858108,15.193871 1,15.1604839 C2.51888821,16.1833871 4.32294226,16.78 6.26105651,16.78 C12.5743243,16.78 16.0267199,11.2870968 16.0267199,6.52290323 C16.0267199,6.3666129 16.0230344,6.21129032 16.0165848,6.05693548 C16.6873464,5.5483871 17.2691953,4.91354839 17.7294226,4.19064516 C17.1139435,4.47709677 16.4523956,4.67112903 15.7576781,4.75822581 C16.4666769,4.31209677 17.0107494,3.60564516 17.2668919,2.76370968 C16.6039619,3.17693548 15.8691646,3.47693548 15.0873771,3.63854839 C14.4613022,2.93790323 13.5694103,2.5 12.582156,2.5"/>
    </symbol>

    <symbol id="icon-facebook" viewbox="0 0 19 19">
        <title>Facebook</title>
        <path d="M10.78265,18.5827895 L10.78265,10.5625526 L13.40845,10.5625526 L13.80175,7.43702632 L10.78265,7.43702632 L10.78265,5.44097368 C10.78265,4.53642105 11.02775,3.91910526 12.29315,3.91910526 L13.90815,3.91910526 L13.90815,1.12365789 C13.62885,1.08471053 12.6703,1 11.555,1 C9.22655,1 7.6334,2.45663158 7.6334,5.13134211 L7.6334,7.43702632 L5,7.43702632 L5,10.5625526 L7.6334,10.5625526 L7.6334,18.5827895 L10.78265,18.5827895"/>
    </symbol>

    <symbol id="icon-instagram" viewbox="0 0 19 19">
        <title>Instagram</title>
        <path d="M3.176,0.5 L15.7774646,0.5 C16.9743333,0.5 17.9534646,1.47930303 17.9534646,2.676 L17.9534646,15.2774646 C17.9534646,16.4743333 16.9743333,17.4534646 15.7774646,17.4534646 L3.176,17.4534646 C1.97930303,17.4534646 1,16.4743333 1,15.2774646 L1,2.676 C1,1.47930303 1.97930303,0.5 3.176,0.5 L3.176,0.5 Z M13.349899,2.38373737 C12.9300505,2.38373737 12.5866162,2.72717172 12.5866162,3.14684848 L12.5866162,4.97374747 C12.5866162,5.39359596 12.9300505,5.7370303 13.349899,5.7370303 L15.2660909,5.7370303 C15.6859394,5.7370303 16.0293737,5.39359596 16.0293737,4.97374747 L16.0293737,3.14684848 C16.0293737,2.72717172 15.6859394,2.38373737 15.2660909,2.38373737 L13.349899,2.38373737 L13.349899,2.38373737 Z M16.0372727,7.66953535 L14.5450505,7.66953535 C14.6863737,8.13059596 14.7624444,8.61878788 14.7624444,9.12415152 C14.7624444,11.9440909 12.4032222,14.2299899 9.4929596,14.2299899 C6.58269697,14.2299899 4.22330303,11.9440909 4.22330303,9.12415152 C4.22330303,8.61878788 4.29954545,8.13059596 4.44069697,7.66953535 L2.88373737,7.66953535 L2.88373737,14.831 C2.88373737,15.2015657 3.1869899,15.5048182 3.55772727,15.5048182 L15.3634545,15.5048182 C15.7340202,15.5048182 16.0372727,15.2015657 16.0372727,14.831 L16.0372727,7.66953535 L16.0372727,7.66953535 Z M9.4929596,5.64086869 C7.61248485,5.64086869 6.0879798,7.1179798 6.0879798,8.94007071 C6.0879798,10.7621616 7.61248485,12.239101 9.4929596,12.239101 C11.3734343,12.239101 12.8977677,10.7621616 12.8977677,8.94007071 C12.8977677,7.1179798 11.3734343,5.64086869 9.4929596,5.64086869 L9.4929596,5.64086869 Z"/>
    </symbol>

    <symbol id="icon-google-plus" viewbox="0 0 19 19">
        <title>Google+</title>
        <path d="M15.778875,8.875 L15.778875,6.8125 L14.403875,6.8125 L14.403875,8.875 L12.341375,8.875 L12.341375,10.25 L14.403875,10.25 L14.403875,12.3125 L15.778875,12.3125 L15.778875,10.25 L17.841375,10.25 L17.841375,8.875 L15.778875,8.875 L15.778875,8.875 Z M12.1261875,2 L7.5886875,2 C4.7781875,2 2.7060625,3.700875 2.7060625,5.8891875 C2.7060625,8.0775 4.6613125,8.9245 6.261125,8.9245 C6.6399375,8.9245 6.99125,8.89975 7.3219375,8.8585 C7.138375,9.110125 6.939,9.484125 6.939,9.9330625 C6.939,10.589625 7.3274375,11.139625 7.841,11.596125 C7.7365,11.5940625 7.63475,11.5899375 7.5254375,11.5899375 C4.61525,11.5899375 2,12.6115625 2,14.841125 C2,15.3656875 2.6180625,17.82625 6.833125,17.82625 C11.0481875,17.82625 11.66075,15.2715 11.66075,13.884125 C11.66075,13.7858125 11.6525,13.6909375 11.640125,13.5974375 C11.5404375,12.527 10.8983125,11.7508125 10.0245,11.075 C9.0736875,10.3400625 8.8839375,10.0843125 8.8839375,9.500625 C8.8839375,8.917625 9.16375,8.4783125 9.8230625,7.9090625 C9.81275,7.916625 9.803125,7.92075 9.792125,7.9283125 C10.6095625,7.292375 10.9931875,6.4275 10.9931875,5.5495625 C10.9931875,3.7386875 9.563875,2.697125 9.563875,2.697125 L10.8295625,2.697125 L12.1261875,2 L12.1261875,2 Z M10.2919375,14.6561875 C10.2919375,15.9404375 9.04,17.107125 7.02975,17.107125 C5.0201875,17.107125 4.0796875,16.0408125 4.0796875,14.37775 C4.0796875,12.715375 5.91875,12.2843125 8.7175625,12.2843125 C9.599625,12.9718125 10.2919375,13.37125 10.2919375,14.6561875 L10.2919375,14.6561875 Z M7.3563125,8.1758125 C6.0610625,8.1758125 4.76375,7.0263125 4.76375,4.8703125 C4.76375,3.2120625 5.8369375,2.6785625 6.708,2.6785625 C8.2541875,2.6785625 8.9141875,4.5905 8.9141875,6.261125 C8.9141875,7.9310625 8.0561875,8.1758125 7.3563125,8.1758125 L7.3563125,8.1758125 Z"/>
    </symbol>

    <symbol id="icon-tumblr" viewbox="0 0 19 19">
        <title>Tumblr</title>
        <path d="M11.2264583,17.4664 C7.87295833,17.4664 6.59916667,14.9672 6.59916667,13.2016 L6.59916667,7.984 L5,7.984 L5,5.9216 C7.396375,5.0488 7.9735,2.864 8.108875,1.6192 C8.11758333,1.5336 8.18408333,1.5 8.22208333,1.5 L10.5369167,1.5 L10.5369167,5.5672 L13.6980417,5.5672 L13.6980417,7.984 L10.5250417,7.984 L10.5250417,12.9544 C10.536125,13.6192 10.7720417,14.5296 11.9785417,14.5296 C12.0007083,14.5296 12.0236667,14.5296 12.0458333,14.5288 C12.4654167,14.5176 13.0275,14.3944 13.3212083,14.2528 L14.0812083,16.5304 C13.7954167,16.9536 12.5057917,17.4456 11.3452083,17.4656 C11.3048333,17.4664 11.2660417,17.4664 11.2264583,17.4664"/>
    </symbol>

    <symbol id="icon-youtube" viewbox="0 0 20 20">
        <title>YouTube</title>
        <path d="M19.6792314,13.9767318 C19.6792314,13.9767318 19.4849385,15.3469434 18.8887643,15.950359 C18.1326748,16.7423055 17.2851521,16.7462376 16.8964885,16.7925868 C14.1140375,16.9937319 9.93591314,17 9.93591314,17 C9.93591314,17 4.76621564,16.9528138 3.17547061,16.800276 C2.73294378,16.7172718 1.73938512,16.7423055 0.983003552,15.950359 C0.38684889,15.3469434 0.192847996,13.9767318 0.192847996,13.9767318 C0.192847996,13.9767318 -0.006,12.367669 -0.006,10.7586062 L-0.006,9.25010618 C-0.006,7.64104338 0.192847996,6.03198058 0.192847996,6.03198058 C0.192847996,6.03198058 0.38684889,4.66176897 0.983003552,4.05837286 C1.73938512,3.26640694 2.5866158,3.26249422 2.9752989,3.21612555 C5.75784714,3.015 9.93157217,3.015 9.93157217,3.015 L9.94021519,3.015 C9.94021519,3.015 14.1140375,3.015 16.8964885,3.21612555 C17.2851521,3.26249422 18.1326748,3.26640694 18.8887643,4.05837286 C19.4849385,4.66176897 19.6792314,6.03198058 19.6792314,6.03198058 C19.6792314,6.03198058 19.8777874,7.64104338 19.8777874,9.25010618 L19.8777874,10.7586062 C19.8777874,12.367669 19.6792314,13.9767318 19.6792314,13.9767318 L19.6792314,13.9767318 Z M7.91142058,6.9998243 L7.91142058,12.6061114 L13.2451798,9.80296785 L7.91142058,6.9998243 L7.91142058,6.9998243 Z"/>
    </symbol>

    <symbol id="icon-email" viewbox="0 0 19 19">
        <title>E-Mail</title>
        <path d="M15.6383077,0 L-0.138307692,0 L7.75,7.9392 L15.6383077,0"/>
        <path d="M15.6985192,9.9396 L15.6985192,2.0604 L11.7847692,6 L15.6985192,9.9396"/>
        <path d="M3.71523077,6 L-0.198519231,2.0604 L-0.198519231,9.9396 L3.71523077,6"/>
        <path d="M-0.138307692,12 L15.6383077,12 L10.7307692,7.0608 L7.75,10.0608 L4.76923077,7.0608 L-0.138307692,12"/>
    </symbol>

    <symbol id="icon-whatsapp" viewbox="0 0 19 19">
        <title>WhatsApp</title>
        <path d="M9.5,0 C4.41290323,0 0.306451613,4.10645161 0.306451613,9.19354839 C0.306451613,10.9403226 0.796774194,12.5645161 1.62419355,13.9435484 L-0.0306451613,18.8774194 L5.05645161,17.2532258 C6.37419355,17.9887097 7.90645161,18.3870968 9.5,18.3870968 C14.5870968,18.3870968 18.6935484,14.2806452 18.6935484,9.19354839 C18.7241935,4.10645161 14.5870968,0 9.5,0 L9.5,0 Z M9.5,16.8548387 C7.93709677,16.8548387 6.49677419,16.3951613 5.27096774,15.5983871 L2.35967742,16.5483871 L3.30967742,13.6983871 C2.39032258,12.4419355 1.83870968,10.8790323 1.83870968,9.19354839 C1.83870968,4.96451613 5.27096774,1.53225806 9.5,1.53225806 C13.7290323,1.53225806 17.1612903,4.96451613 17.1612903,9.19354839 C17.1612903,13.4225806 13.7290323,16.8548387 9.5,16.8548387 L9.5,16.8548387 Z M13.8209677,11.2774194 C13.5758065,11.1548387 12.4725806,10.5419355 12.2580645,10.45 C12.0435484,10.3580645 11.8903226,10.3274194 11.7370968,10.5419355 C11.583871,10.7564516 11.0935484,11.2774194 10.9709677,11.4306452 C10.8177419,11.583871 10.6951613,11.583871 10.45,11.4612903 C10.2048387,11.3387097 9.46935484,11.0629032 8.61129032,10.2354839 C7.93709677,9.59193548 7.50806452,8.79516129 7.35483871,8.55 C7.23225806,8.30483871 7.35483871,8.18225806 7.47741935,8.05967742 C7.6,7.96774194 7.72258065,7.78387097 7.84516129,7.66129032 C7.96774194,7.53870968 7.9983871,7.44677419 8.09032258,7.29354839 C8.18225806,7.14032258 8.1516129,6.98709677 8.09032258,6.89516129 C8.02903226,6.77258065 7.6,5.60806452 7.44677419,5.11774194 C7.26290323,4.62741935 7.07903226,4.71935484 6.92580645,4.71935484 C6.80322581,4.71935484 6.61935484,4.68870968 6.46612903,4.68870968 C6.31290323,4.68870968 6.06774194,4.71935484 5.82258065,4.96451613 C5.60806452,5.17903226 4.96451613,5.73064516 4.93387097,6.89516129 C4.90322581,8.05967742 5.7,9.19354839 5.79193548,9.34677419 C5.91451613,9.5 7.32419355,12.0129032 9.68387097,13.0548387 C12.0435484,14.0967742 12.0435484,13.7596774 12.4725806,13.7596774 C12.9016129,13.7290323 13.8822581,13.2387097 14.0967742,12.7177419 C14.3112903,12.166129 14.3419355,11.7064516 14.2806452,11.6145161 C14.2193548,11.4919355 14.066129,11.4306452 13.8209677,11.2774194 L13.8209677,11.2774194 Z"/>
    </symbol>

    <symbol id="icon-bow-tie" viewbox="0 0 26 16">
        <title>Sign In</title>
        <path d="M2,2 L2,14 L24,2 L24,14 L2,2 Z" stroke-width="2"/>
    </symbol>

    <symbol id="icon-check" viewbox="0 0 25 19">
        <title>Check</title>
        <path d="M8.48528137,14.1421356 L2.12132034,7.77817459 L-1.77635684e-15,9.89949494 L7.4246212,17.3241161 L8.48528137,18.3847763 L24.7487373,2.12132034 L22.627417,0 L8.48528137,14.1421356 L8.48528137,14.1421356 Z"/>
    </symbol>

    <symbol id="icon-close" viewbox="0 0 23 23">
        <title>Close</title>
        <path d="M9.19238816,7.07106781 L2.12132034,1.77635684e-15 L8.8817842e-16,2.12132034 L7.07106781,9.19238816 L0,16.263456 L2.12132034,18.3847763 L9.19238816,11.3137085 L16.263456,18.3847763 L18.3847763,16.263456 L11.3137085,9.19238816 L18.3847763,2.12132034 L16.263456,0 L9.19238816,7.07106781 L9.19238816,7.07106781 L9.19238816,7.07106781 Z"/>
    </symbol>

    <symbol id="icon-snapchat" viewbox="0 0 20 20">
        <title>snapchat</title><path d="M10.12,19.25H9.88a4.38,4.38,0,0,1-2.65-1,3.82,3.82,0,0,0-1.51-.77,4.88,4.88,0,0,0-.8-0.07,5.45,5.45,0,0,0-1.1.12,2.45,2.45,0,0,1-.42.06,0.29,0.29,0,0,1-.31-0.23C3,17.17,3,17,3,16.87a0.74,0.74,0,0,0-.28-0.61C1.22,16,.35,15.69.18,15.29a0.38,0.38,0,0,1,0-.13,0.24,0.24,0,0,1,.2-0.25,5.14,5.14,0,0,0,3.09-1.84,6.92,6.92,0,0,0,1.05-1.64h0a1.06,1.06,0,0,0,.1-0.88,1.9,1.9,0,0,0-1.23-.78L3.08,9.67a1.11,1.11,0,0,1-.89-0.88A0.82,0.82,0,0,1,3,8.27a0.57,0.57,0,0,1,.24,0,2.42,2.42,0,0,0,1,.26A0.84,0.84,0,0,0,4.8,8.41c0-.19,0-0.39,0-0.59h0a9.94,9.94,0,0,1,.24-4A5.16,5.16,0,0,1,9.8.75h0.4A5.17,5.17,0,0,1,15,3.84a10,10,0,0,1,.24,4V7.88c0,0.18,0,.36,0,0.52a0.81,0.81,0,0,0,.52.17,2.5,2.5,0,0,0,.94-0.26A0.74,0.74,0,0,1,17,8.26a0.93,0.93,0,0,1,.35.07h0a0.65,0.65,0,0,1,.49.54,1.06,1.06,0,0,1-.9.81l-0.28.09a1.9,1.9,0,0,0-1.23.78,1.05,1.05,0,0,0,.1.88h0c0.05,0.12,1.32,3,4.14,3.47a0.24,0.24,0,0,1,.2.25,0.38,0.38,0,0,1,0,.13c-0.17.4-1,.74-2.51,1a0.73,0.73,0,0,0-.28.61c0,0.15-.07.3-0.11,0.45a0.27,0.27,0,0,1-.29.22h0a2.36,2.36,0,0,1-.42-0.05,5.51,5.51,0,0,0-1.1-.12,4.89,4.89,0,0,0-.8.07,3.82,3.82,0,0,0-1.51.77,4.38,4.38,0,0,1-2.65,1" class="rbt-inline-1"/>
    </symbol>

</svg>
<div id="vue-app">
    <div class="ad-bg">
        <div class="ad" id="a970"></div>
    </div>
    <header class="main bg-bunny box-1"><a class="toggle" data-action="Super Nav" data-label="My Account" href="http://www.playboy.com/accounts" on="tap:rbt-sidebar.toggle" role="button" tabindex="-1"><svg role="img"><use xlink:href="#icon-bow-tie" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg><span>My Account</span></a>
    <!--{/* @notice */}-->
        <!--{/* The following code was moved to amp-sidebar. */}-->
    <!-- <nav class="main"><div class="bg-super"><div class="container"><ul class="super"><li><a data-label="Hop" href="https://hop.playboy.com/">Experience the Rabbit</a></li><li><a data-label="Pose For Playboy" href="http://www.playboy.com/pose-for-playboy">Pose for Playboy</a></li><li class="signup"><a class="nsfw" data-label="PB Plus Sign Up" href="http://join.playboyplus.com/track/MTAwMzk1OS4xMDAxMS4xMDIzLjMwNDEuMC4wLjAuMC4w/join?autocamp=pbcomtopbar">Sign Up For Playboy Plus</a></li></ul></div></div><div class="container"><div class="login"><template v-if="isLoggedIn"></template><template v-else=""><a @click.prevent="toggleLoginModal" class="toggle" data-eventaction="Super Nav" data-eventlabel="Login Modal" href="#" id="login-modal"><svg role="img"><use xlink:href="#icon-bow-tie" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg><span>Sign In To Playboy</span></a></template></div></div><div class="bg-global"><div class="container"><ul class="global"><li class="active"><a href="http://www.playboy.com/">Home</a></li><li><a data-label="Entertainment" href="http://www.playboy.com/entertainment">Entertainment</a></li><li><a data-label="Off Hours" href="http://www.playboy.com/off-hours">Off Hours</a></li><li><a data-label="Bunnies" href="http://www.playboy.com/bunnies">Bunnies</a></li><li><a data-label="Sex &amp; Culture" href="http://www.playboy.com/sex-and-culture">Sex &amp; Culture</a></li><li><a data-label="Heritage" href="http://www.playboy.com/heritage">Heritage</a></li><li><a data-label="Video" href="http://www.playboy.com/videos">Video</a></li><li class="nsfw "><div class="magazine-cta"><div class="thumbnail"><!--{/* @notice */}-->
        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
        <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/4PA10HPf7q6Y2uqGWoYqW/ce0f9027ec0fac1ec2b6e65d966a907f/magazine-cover.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="150" layout="fixed"></amp-img> -->
        <div class="copy">
            <p>Get the Magazine That Changed It All<a class="button" href="http://ma.playboyplus.com/age-gate?rul=http://www.playboyplus.com/go/?id=ed5075f65e183695">Subscribe</a></p>
        </div><a data-label="PlayboyNSFW" href="http://ma.playboyplus.com/age-gate?rul=http://www.playboyplus.com/go/?id=ed5075f65e183695">PlayboyNSFW</a>
        <li class="holiday ">
            <div class="magazine-cta">
                <div class="thumbnail">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/4PA10HPf7q6Y2uqGWoYqW/ce0f9027ec0fac1ec2b6e65d966a907f/magazine-cover.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="150" layout="fixed"></amp-img> -->
                </div>
                <div class="copy">
                    <p>Get the Magazine That Changed It All<a class="button" href="http://www.playboyshop.com/?utm_source=playboy&amp;utm_medium=native&amp;utm_campaign=topnav">Subscribe</a></p>
                </div>
            </div><span class="holiday"><a data-label="Shop" href="http://www.playboyshop.com/?utm_source=playboy&amp;utm_medium=native&amp;utm_campaign=topnav">Shop</a></span></li>
        <li class="magazine-dropdown ">
            <div class="magazine-cta">
                <div class="thumbnail">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/4PA10HPf7q6Y2uqGWoYqW/ce0f9027ec0fac1ec2b6e65d966a907f/magazine-cover.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="150" layout="fixed"></amp-img> -->
                </div>
                <div class="copy">
                    <p>Get the Magazine That Changed It All<a class="button" href="http://www.playboy.com/magazine">Subscribe</a></p>
                </div>
            </div><a data-label="Magazine" href="http://www.playboy.com/magazine">Magazine</a></li>
        <div class="cta-link" data-action="Global Navigation" data-category="Navigation"><a data-label="Hop" href="https://hop.playboy.com/"><span>Experience the Rabbit</span></a></div> -->
        <div class="container"><a class="logo" data-label="Home Logo" href="http://www.playboy.com/"></a><a class="icon search"></a></div>
    </header><a class="icon hamburger"></a>
<!--{/* @notice */}-->
    <!--{/* Tag account-login isn't supported in AMP. */}-->
<!-- <account-login @close="showLoginModal = false" v-if="showLoginModal"> -->
    <!-- </account-login> -->
<!--{/* @notice */}-->
    <!--{/* Tag age-gate isn't supported in AMP. */}-->
<!-- <age-gate :url="ageGateTargetHref" @close="showAgeGate = false" v-if="showAgeGate"> -->
    <!-- </age-gate> -->
</div>
<div class="main body container">
    <section class="feature-carousel">
        <div class="slide">
            <a data-action="Feature Carousel" data-category="Homepage" data-label="Home Feature: Out on the Mountain" href="http://www.playboy.com/articles/out-on-the-mountain">
            <!--{/* @notice */}-->
                <!--{/* Tag picture isn't supported in AMP. */}-->
                <!-- <picture> -->
                <source media="(max-width: 362px)" srcset="https://images.contentful.com/ogz4nxetbde6/yCQMA8UREWYgsGoIwmSMm/95c56ac03742824f00d4404f2aa80561/Gay-olympians_thumb.jpg?w=370&amp;h=370&amp;fm=jpg&amp;fit=fill">
                <source media="(max-width: 481px)" srcset="https://images.contentful.com/ogz4nxetbde6/yCQMA8UREWYgsGoIwmSMm/95c56ac03742824f00d4404f2aa80561/Gay-olympians_thumb.jpg?w=500&amp;h=500&amp;fm=jpg&amp;fit=fill">
                <source media="(max-width: 541px)" srcset="https://images.contentful.com/ogz4nxetbde6/yCQMA8UREWYgsGoIwmSMm/95c56ac03742824f00d4404f2aa80561/Gay-olympians_thumb.jpg?w=550&amp;h=550&amp;fm=jpg&amp;fit=fill">
                <source media="(max-width: 768px)" srcset="https://images.contentful.com/ogz4nxetbde6/24HfsDMoDmMIYWCK4KUyGe/9ed16426d3287ce5d64f6258d9d9ff94/olympians-gay-caro.jpg?w=800&amp;h=320&amp;fm=jpg&amp;f=faces&amp;fit=fill">
                <source media="(min-width: 769px)" srcset="https://images.contentful.com/ogz4nxetbde6/24HfsDMoDmMIYWCK4KUyGe/9ed16426d3287ce5d64f6258d9d9ff94/olympians-gay-caro.jpg?w=1240&amp;h=451&amp;fm=jpg&amp;f=faces&amp;fit=fill">
            <!--{/* @notice */}-->
                <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/24HfsDMoDmMIYWCK4KUyGe/9ed16426d3287ce5d64f6258d9d9ff94/olympians-gay-caro.jpg?w=400&amp;h=400&amp;fm=jpg&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                <!-- </picture> -->
                <div class="title">
                    <h2><span>SEX &amp; CULTURE</span><br>Out on the Mountain: How Gay Olympians Are Changing the Face of Masculinity</h2>
                </div>
            </a>
        </div>
    </section>
    <section class="main content left padless-right">
        <div class="tile-container" data-action="Home Story Tiles" data-category="Navigation" data-vert="home" id="vue-load">
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/stuart-parr-vintage-motorcycles">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/83c16UaVygY6owW064kAo/ef6ccf1191187609566a8cd0e90045f9/motorcycles_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">A Hollywood Producer’s Love for Vintage Motorcycles Has Birthed a Beautiful Collection</h3>
                        <p class="author">by Marcus Amick</p>
                    </div>
                </a>
            </div>
            <div class="nativo-container" id="nativo"></div>
            <div class="tile box-1">
                <a class="gallery" href="http://www.playboy.com/galleries/all-day-and-night-with-danielle-krivak">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/G4WeCVuuSkuiMu6SeusAW/cd3528e37cd0752ec13b04d298551d9f/BenTsui_DanielleKrivak_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">All Day and Night With Danielle Křivák</h3>
                        <p class="author">by Ben Tsui</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/black-panther-movie-review">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/AYphZD4HYsI4qSWYWyw2c/78218a9e87a651e9727d0e8bb6a290f0/black-panther_review_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Revolutionary &#39;Black Panther&#39; Lives Up to the Hype</h3>
                        <p class="author">by Stephen Rebello</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/fifty-shades-of-grey-legacy-sex">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/7vY980bUXYw6mu2sg4aQoc/e99f744bbca437d166166e11e12b3fa8/fifty-shades-freed_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">The Complicated Legacy of &#39;Fifty Shades of Grey&#39;</h3>
                        <p class="author">by Adam Howard</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/the-chunky-sneaker-vetements-balenciaga">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/4QdTZZTGXCs6sCki2Yc2kI/7d947a88d3ff2636c98861f32c0660ab/Vetements_sneakers_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">The Chunky Sneaker Trend is Only Going to Get More Bizarre</h3>
                        <p class="author">by Tyler Watamanuk</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/opioids-epidemic-saves-kills">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/6L0M0vJ8XugWAEmeKKaiwk/5bb457f892b512804f4575ceafc5fcbe/opioid-thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">The Opioid Epidemic: How to Combat the Drug That Helps As Many Lives As It Kills</h3>
                        <p class="author">by Tori Bilcik</p>
                    </div>
                </a>
            </div>
            <div class="ad" id="a300-mobile"></div>
            <div class="ad" id="a300-tablet"></div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/quentin-tarantino-uma-thurman-controversy">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/5I5074jSQEei2M8eQ8W0WS/ac12ac5296de591d72e7d9433ee35bbf/Screen_Shot_2018-02-08_at_12.27.35_AM.png?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Will Quentin Tarantino&#39;s Early Retirement Come Sooner Than He Planned?</h3>
                        <p class="author">by Daniel Barna</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/channel-zero-butchers-block-horror">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/3FUbGMbslGIgSoa8kayoS8/f9c4148199dd094d4e8b81582abac716/channel-zero_thumb_copy.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">How &#39;Channel Zero: Butcher&#39;s Block&#39; Slices a New Path for Horror</h3>
                        <p class="author">by Carli Velocci</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/declaring-dissent-as-un-american">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/1oBz724qg8y8syk4YWM4Uq/97c27373ef251db4339e4d1059547e19/shutterstock_6727174e.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Lest We Forget What It Means to Be an American...</h3>
                        <p class="author">by Brian Karem</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/vagina-camera-artist-dani-lessnau">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/2qmKioDNOo0iU08cM2y0cW/a8627954c721b8c90fdc51c37c863062/Untitled_3.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">The Artist Who Turned Her Vagina Into a Camera</h3>
                        <p class="author">by Kyle Fitzpatrick</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/game-of-thrones-creators-star-wars">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/4xM040VBqEWcIkWyw0Eayy/268ee1e7341b16006ed4a9bc63fcf1b5/game_of_thrones_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Will &#39;Game of Thrones&#39; Creators Fit With the &#39;Star Wars&#39; Universe?</h3>
                        <p class="author">by Matthew Jackson</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/contract-for-consent">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/1ez7vLRk4eWQwAYoUKqoK/4d19a0cc9069b6b1cfa81d56afc02dcd/consent-contracts-app_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Contracts for Consent Might Be The Future of Sex</h3>
                        <p class="author">by Bobby Box</p>
                    </div>
                </a>
            </div>
            <div class="ad" id="b300-mobile"></div>
            <div class="ad" id="b300-tablet"></div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/disney-solo-a-star-wars-story-trailer">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/2ckc7r0Z1yaasgCMqAokWW/5129321601cbe5811dae9991d4042667/han-solo_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Should &#39;Star Wars&#39; Fans Still Worry About the Han Solo Movie?</h3>
                        <p class="author">by Daniel Barna</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/robot-sex-dating">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/1O9idGmT3eCISiuIaakw4O/97fa35d2b42001f88f55e229acb15bb0/sex-robot-thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Let&#39;s Experiment With Robot Sex</h3>
                        <p class="author">by Lisa Beebe</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/why-this-years-winter-olympics-are-a-letdown">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/6nioPhiR9u4uKKggwGkIsK/7ecae44cc40b897d10162baf21e2ea93/winter-olympics_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Why Americans Won&#39;t Tune Into the Winter Olympics This Year</h3>
                        <p class="author">by Aaron Gordon</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/super-bowl-eagles-justin-timberlake">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/3vbMq46jO8W8qCUWiomiqK/87f7e6e7401db22f38a912f486fd3022/JT-superbowl_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Justin Timberlake Gave Us a Halftime Show Devoid of Meaning</h3>
                        <p class="author">by Tom Carson</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/devin-nunes-memo">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/61IX2tqKFqOYyGwoyWAkO8/5eb92314ba8c33f6f37bc229aef2a9f9/8536263j.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">There is No Such Thing as Transparency Anymore</h3>
                        <p class="author">by Brian Karem</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/this-week-in-sex-your-dick-is-attractive">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/3tKlAyQcQo2Q824k68yWYi/b9069574bae60d637bec516f547463c2/Cherie-Noel_Evan-Woods__03_-thumbnail.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">This Week in Sex: A Primer on Hot Tub Sex, Dirty Talk and More</h3>
                        <p class="author">by Zaron Burnett III</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/incest-porn-playboy">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/30KnL87gZGQSI0EK0MKoQi/4f154845d907ab72c9b5f3901198b67e/incest-porn_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Incest Porn Is More Popular than You Think</h3>
                        <p class="author">by Debra W. Soh</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="gallery" href="http://www.playboy.com/galleries/christine-sofie-johansen-is-a-force-of-nature">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/5fu8Adkm2AaMqc0c2U4Iao/fbec073cf9dccedd91cb17d3c62e42b7/CHRISTINE-SOFIE-JOHANSEN_HeatherHazzan_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Christine Sofie Johansen Is a Force of Nature</h3>
                        <p class="author">by Heather Hazzan</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/a-fantastic-woman-chile-movie-review">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/1tGCpQdEgAQkOOyykkCYg6/8fd1f9a1ad7d95c7d7afb35c952c2b61/fantastic-woman_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Examining Gender Identity, &#39;A Fantastic Woman&#39; Deserves Its Oscars Love</h3>
                        <p class="author">by Stephen Rebello</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/8-former-nfl-players-cannabis">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/2BRYtPM9HuWmS62meqo6Kq/a38928216ec82baed775a7989607a516/cannabis-nfl_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">8 Former NFL Players Share Their Thoughts on Cannabis</h3>
                        <p class="author">by Javier Hasse</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/bloom-farms-socially-conscious-cannabis">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/2jJ1YRzvragka0MSCgImEi/1937c9141f096559ed38e765eea2ba09/bloom-farm_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">Bloom Farms: For the Socially Conscious Stoner</h3>
                        <p class="author">by Kyle Fitzpatrick</p>
                    </div>
                </a>
            </div>
            <div class="tile box-1">
                <a class="article" href="http://www.playboy.com/articles/the-art-of-apology-respect-means-having-to-say-youre-sorry">
                <!--{/* @notice */}-->
                    <!--{/* Tag picture isn't supported in AMP. */}-->
                    <!-- <picture> -->
                    <amp-img src="https://images.contentful.com/ogz4nxetbde6/4QNKyYrtkswyMMKG4ksmmE/48015608d1683ea1198c95cf3000dd98/how-to-apologize_thumb.jpg?w=400&h=400&fm=jpg&f=faces&fit=fill" width="400" height="400" layout="responsive"></amp-img>
                    <!-- </picture> -->
                    <div>
                        <h3 class="title">The Art of Apology: Respect Means Having to Say You&#39;re Sorry</h3>
                        <p class="author">by Jennifer Neal</p>
                    </div>
                </a>
            </div>
        <!--{/* @notice */}-->
            <!--{/* Tag load-more isn't supported in AMP. */}-->
            <!-- <load-more> -->
            <!-- </load-more> -->
        </div>
    </section>
    <aside class="sidebar right">
        <div class="ad" id="a300-desktop"></div>
        <section class="module voices" data-action="Voices">
            <meta content="noindex, follow" name="robots">
            <h2>Voices</h2>
            <ul data-category="Voices">
                <li class="layout" data-ident="4tJHIeBl8sIGucEKmekequ">
                    <a data-label="Author select: Minda Honey" href="http://www.playboy.com/authors/minda-honey" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Minda Honey" src="https://images.contentful.com/ogz4nxetbde6/6vSbg3TGs8q4SQqUiKQs6g/c25925a4aade134f043ace51dbc4aff6/profile-author_minda-honey.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Author select: Minda Honey" href="http://www.playboy.com/authors/minda-honey" itemprop="relatedLink"><strong>Minda Honey</strong></a><a data-label="Author Feature select: At O.School, Queer Women and People of Color Are Changing the Meaning of Sex Ed" href="http://www.playboy.com/articles/o-school-profile" itemprop="relatedLink"><em>Redefining sex-ed</em></a></p>
                </li>
                <li class="layout" data-ident="1DFbKvLiIAoI8SG0AyAISw">
                    <a data-label="Author select: Buzz Poole" href="http://www.playboy.com/authors/buzz-poole" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Buzz Poole" src="https://images.contentful.com/ogz4nxetbde6/76nZn2rfFYAqSiwOMiqYyW/583f39e93d65862837cb737f0d41a8a3/Buzz_Poole_Pic.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Author select: Buzz Poole" href="http://www.playboy.com/authors/buzz-poole" itemprop="relatedLink"><strong>Buzz Poole</strong></a><a data-label="Author Feature select: &#39;Slugfest&#39; Explores the Raging Rivalry Between Marvel and DC Comics" href="http://www.playboy.com/articles/slugfest-marvel-dc-comics-rivalry-reed-tucker" itemprop="relatedLink"><em>Probing the Marvel/DC rivalry</em></a></p>
                </li>
                <li class="layout" data-ident="18NHDKTjUyc8QMswso4u8A">
                    <a data-label="Author select: Caroline Orr" href="http://www.playboy.com/authors/caroline-orr" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Caroline Orr" src="https://images.contentful.com/ogz4nxetbde6/4R5dG5C8HYiEu22Kyu8qcW/0b4ac331b3e832cc0d3a2752aac2d19b/profile_caroline-orr.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Author select: Caroline Orr" href="http://www.playboy.com/authors/caroline-orr" itemprop="relatedLink"><strong>Caroline Orr</strong></a><a data-label="Author Feature select: Donald Trump Doesn&#39;t Care About Young People" href="http://www.playboy.com/articles/trumpcare-healthcare-young-people" itemprop="relatedLink"><em>Trump doesn’t care about young people</em></a></p>
                </li>
                <li class="layout" data-ident="7G1K2enUJOYkeieWiq2ew">
                    <a data-label="Author select: Shane Michael Singh" href="http://www.playboy.com/authors/shane-michael-singh" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Shane Michael Singh" src="https://images.contentful.com/ogz4nxetbde6/56dssyD1HWoos8yQMYKQW8/356f6a8dbc9ba328a03d0f6755baef1f/3838_10100551687969705_240164078_n.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Author select: Shane Michael Singh" href="http://www.playboy.com/authors/shane-michael-singh" itemprop="relatedLink"><strong>Shane Michael Singh</strong></a><a data-label="Author Feature select: The Female Sexual Revolution Will Be Televised" href="http://www.playboy.com/articles/female-sexuality-on-tv" itemprop="relatedLink"><em>Women tackle sex on TV better</em></a></p>
                </li>
                <li class="layout" data-ident="1znyzd7KZic40ygw8aYK2W">
                    <a data-label="Author select: Bridget Phetasy" href="http://www.playboy.com/authors/bridget-phetasy" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Bridget Phetasy" src="https://images.contentful.com/ogz4nxetbde6/5Tp8JTvYiIsoA8cguC024u/0b5536c654226279f83f9ec3df644917/thumb_bridget.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Author select: Bridget Phetasy" href="http://www.playboy.com/authors/bridget-phetasy" itemprop="relatedLink"><strong>Bridget Phetasy</strong></a><a data-label="Author Feature select: Should You Ghost Her? A Flowchart" href="http://www.playboy.com/articles/should-you-ghost-her-a-flowchart" itemprop="relatedLink"><em>Should you ghost her?</em></a></p>
                </li>
                <li class="layout" data-ident="6Kgtf0jx8Qm8qaUy202Cu0">
                    <a data-label="Author select: Anna del Gaizo" href="http://www.playboy.com/authors/anna-del-gaizo" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Anna del Gaizo" src="https://images.contentful.com/ogz4nxetbde6/3ev0wg9RUAWeGI0EWYuqc8/fdbc08eb020f23659bab1374acc36c82/author_anna-del-gaizo.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Author select: Anna del Gaizo" href="http://www.playboy.com/authors/anna-del-gaizo" itemprop="relatedLink"><strong>Anna del Gaizo</strong></a><a data-label="Author Feature select: How Weed Is Making Sex More Fun for Women " href="http://www.playboy.com/articles/how-weed-is-making-sex-more-fun-for-women" itemprop="relatedLink"><em>Weed is the new aphrodisiac</em></a></p>
                </li>
            </ul>
        </section>
        <div class="ad" id="b300-desktop"></div>
        <section class="module voices" data-action="Most Popular">
            <meta content="noindex, follow" name="robots">
            <h2>Most Popular</h2>
            <ul data-category="Most Popular">
                <li class="layout" data-ident="1085pVAbiE0W0QueE8w0SQ">
                    <a data-label="Post select: The Social Influencer: How Jeff Flake Became the Senate&#39;s Most Watched Man" href="http://www.playboy.com/articles/jeff-flake" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="The Social Influencer: How Jeff Flake Became the Senate&#39;s Most Watched Man" src="https://images.contentful.com/ogz4nxetbde6/6Tcbj6aDZeYyAmqaa8McOq/b77c962c2ce2f41d9c2511028b4f2a6c/thumb_jeffflake_final_w1.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Post select: The Social Influencer: How Jeff Flake Became the Senate&#39;s Most Watched Man" href="http://www.playboy.com/articles/jeff-flake" itemprop="relatedLink"><strong>The Social Influencer: How Jeff Flake Became the Senate&#39;s Most Watched Man</strong></a></p>
                </li>
                <li class="layout" data-ident="2h5myrcnB6eEqYs8wUooIk">
                    <a data-label="Post select:  Dating a Hacker is a Dark Web" href="http://www.playboy.com/articles/dating-hacker" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt=" Dating a Hacker is a Dark Web" src="https://images.contentful.com/ogz4nxetbde6/3lbDpukUFWmE0KaGeuUYA4/86d15d3c9c07960d8f2f52f37b68caa1/darkside-hacker-thumb.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Post select:  Dating a Hacker is a Dark Web" href="http://www.playboy.com/articles/dating-hacker" itemprop="relatedLink"><strong> Dating a Hacker is a Dark Web</strong></a></p>
                </li>
                <li class="layout" data-ident="3N7Jwy5cCA4qEiEESiwkmC">
                    <a data-label="Post select: The Myth of the Male Feminist" href="http://www.playboy.com/articles/male-feminists" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="The Myth of the Male Feminist" src="https://images.contentful.com/ogz4nxetbde6/1YeAdCPfAgqO4Wois26kaw/8daa2a6f7807937258b89440c26d5f69/male-feminist-thumb.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Post select: The Myth of the Male Feminist" href="http://www.playboy.com/articles/male-feminists" itemprop="relatedLink"><strong>The Myth of the Male Feminist</strong></a></p>
                </li>
                <li class="layout" data-ident="12G88oZAnKe0keoaSW6iqE">
                    <a data-label="Post select: Navigating Tinder in Lebanon" href="http://www.playboy.com/articles/navigating-tinder-in-lebanon" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Navigating Tinder in Lebanon" src="https://images.contentful.com/ogz4nxetbde6/2INjA6Gjw4YUu44KYEwYA6/8f65c2e9a86f6b0c5877cea1ba763d72/Lebanon-tinder_thumb.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Post select: Navigating Tinder in Lebanon" href="http://www.playboy.com/articles/navigating-tinder-in-lebanon" itemprop="relatedLink"><strong>Navigating Tinder in Lebanon</strong></a></p>
                </li>
                <li class="layout" data-ident="6kHhN4qbPaUKUkc60M0sIa">
                    <a data-label="Post select: What 2018 Could Look Like For Our National Parks" href="http://www.playboy.com/articles/2018-national-parks" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="What 2018 Could Look Like For Our National Parks" src="https://images.contentful.com/ogz4nxetbde6/2E6SzQH8JeUSUIwYqMUeOg/78d439e72c3c4ff3cd3117346e996945/national-parks_thumb_480.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Post select: What 2018 Could Look Like For Our National Parks" href="http://www.playboy.com/articles/2018-national-parks" itemprop="relatedLink"><strong>What 2018 Could Look Like For Our National Parks</strong></a></p>
                </li>
                <li class="layout" data-ident="nYEzZRUs7IG22SIUEa4Um">
                    <a data-label="Post select: Meet Luci 6000, the Sexting Robot" href="http://www.playboy.com/articles/luci-6000-sexting-robot" itemprop="relatedLink">
                    <!--{/* @notice */}-->
                        <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                        <!-- <amp-img alt="Meet Luci 6000, the Sexting Robot" src="https://images.contentful.com/ogz4nxetbde6/36up25xpMk60qywEqwIG2u/183152a08c283476767f31b47fe2797b/luci-by-maggie-west-8.jpg?w=70&amp;h=70&amp;fm=jpg&amp;f=face&amp;fit=thumb" width="200" height="150" layout="fixed"></amp-img> -->
                    </a>
                    <p><a data-label="Post select: Meet Luci 6000, the Sexting Robot" href="http://www.playboy.com/articles/luci-6000-sexting-robot" itemprop="relatedLink"><strong>Meet Luci 6000, the Sexting Robot</strong></a></p>
                </li>
            </ul>
        </section>
        <div class="ad sticky" id="c300-desktop"></div>
        <div class="module home-social-links" data-action="Follow:Modal" data-category="Social">
            <h2>Follow The Bunnies</h2>
            <ul>
                <li class="facebook"><a data-label="Follow Facebook" href="https://www.facebook.com/playboy" title="Playboy on Facebook"><span><svg role="img" title="Facebook"><use xlink:href="#icon-facebook" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg></span></a></li>
                <li class="twitter"><a data-label="Follow Twitter" href="https://twitter.com/playboy" title="Playboy on Twitter"><span><svg role="img" title="Twitter"><use xlink:href="#icon-twitter" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg></span></a></li>
                <li class="instagram"><a data-label="Follow Instagram" href="http://instagram.com/playboy" title="Playboy on Instagram"><span><svg role="img" title="Instagram"><use xlink:href="#icon-instagram" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg></span></a></li>
                <li class="google-plus"><a data-label="Follow GooglePlus" href="https://plus.google.com/108330879683865530444/about" title="Playboy on Google+"><span><svg role="img" title="Google+"><use xlink:href="#icon-google-plus" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg></span></a></li>
                <li class="youtube"><a data-label="Follow YouTube" href="https://www.youtube.com/playboy" title="Playboy on YouTube"><span><svg role="img" title="YouTube"><use xlink:href="#icon-youtube" xmlns:xlink="http://www.w3.org/1999/xlink"/></svg></span></a></li>
            </ul>
        </div>
    </aside>
</div>
<div class="search-base" id="search-base">
    <div class="container">
        <div class="form" id="search-form">
            <a class="exit" data-action="Search Exit" data-category="Search" data-label="Click X to Exit">
                <amp-img alt="Exit" src="http://cdn.playboy.com/dashboard_assets/icons/close-8458884cd4bdc91198a3ea80f823651dbc8739587c228c579377a410c7ca8bdc.svg" width="19" height="19" layout="fixed"></amp-img>
            </a>
            <a class="clear" data-action="Search Box" data-category="Search" data-label="Click X to Clear">
                <amp-img alt="Clear" src="http://cdn.playboy.com/dashboard_assets/icons/search/search-clear-5ee5ce2a713b25f0eef519c1c4f1e2b93eb03d7e806717656282c6ed16c00006.svg" width="22" height="22" layout="fixed"></amp-img>
            </a><input autocomplete="off" id="search-input" type="text">
            <div class="toolbar" id="search-toolbar">
                <div class="facet" id="facet-refine"></div>
                <div class="stats" id="search-stats"></div>
            </div>
        </div>
        <amp-accordion>
            <section class="padless-right">
                <!-- <div class="padless-right"> -->
                <header>
                    <div class="results" id="search-results"></div>
                </header>
                <div class="loading-indicator rbt-inline-3">Loading...</div>
                <!-- </div> -->
            </section>
        </amp-accordion>
        <div class="series" id="search-series">
            <header>
                <h5>Popular Series</h5>
            </header>
            <div class="content">
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Playboy Fiction" data-type="tags" href="http://www.playboy.com/series/playboy-fiction">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Playboy Fiction" src="https://images.contentful.com/ogz4nxetbde6/1u6rzMjdFeKuK8IwAQsKo2/8ed2dce32f9853768b5b2e1e9d8e758b/PlayboyFiction.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Playboy Fiction</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="American Playboy" data-type="tags" href="http://www.playboy.com/series/american-playboy">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="American Playboy" src="https://images.contentful.com/ogz4nxetbde6/6c6Ye3QYUwmmgAcseikEOM/06275290437262d37195a95be0ea2eda/american-playboy-icon.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>American Playboy</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="The New Creatives" data-type="tags" href="http://www.playboy.com/series/the-new-creatives">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="The New Creatives" src="https://images.contentful.com/ogz4nxetbde6/5T5nnl2NfawAAsuGSaukKY/5909fc034755e1e8bd5b41609d8d11a2/new-creatives.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>The New Creatives</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Playboy Interview" data-type="tags" href="http://www.playboy.com/series/playboy-interview">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Playboy Interview" src="https://images.contentful.com/ogz4nxetbde6/3YVFuy60LYAycgOiuOKeEO/9e8dd8720d5c5b1c9d75141fe44a6612/PlayboyInterview.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Playboy Interview</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Playboy Advisor" data-type="tags" href="http://www.playboy.com/series/playboy-advisor">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Playboy Advisor" src="https://images.contentful.com/ogz4nxetbde6/jZwaGtkAKsk0oiyQm2EiK/23bf8dc9b81a435a403615c97ab1e09c/PlayboyAdvisorWhite_vhghev_.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Playboy Advisor</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Turn Ons/Turns Offs" data-type="tags" href="http://www.playboy.com/series/turn-ons-turn-offs">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Turn Ons/Turns Offs" src="https://images.contentful.com/ogz4nxetbde6/2QHOcQo5qE8KGIaUMYOIk2/b32546c0501ecac80e04f029de0faf80/turnons.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Turn Ons/Turns Offs</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Adults &amp; Crafts" data-type="tags" href="http://www.playboy.com/series/adults-and-crafts">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Adults &amp; Crafts" src="https://images.contentful.com/ogz4nxetbde6/6vJePhCkAoaKA4eKaSMaeM/40a9b8af933a3acd1155a27ee55da0a8/Adults.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Adults &amp; Crafts</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Playboy Conversation" data-type="tags" href="http://www.playboy.com/series/playboy-conversation">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Playboy Conversation" src="https://images.contentful.com/ogz4nxetbde6/7lR6qNGbZuEycsYoK2kSEW/7ce711d93422610ea9e6c0dc4318deb2/PBConversation.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Playboy Conversation</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Just The Tips" data-type="tags" href="http://www.playboy.com/series/just-the-tips">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Just The Tips" src="https://images.contentful.com/ogz4nxetbde6/12kGYqswhoG4eaeuYqEkcK/f42c89725b387e0c8b8c54fda1e0f88c/JustTheTips.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Just The Tips</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Playmates On" data-type="tags" href="http://www.playboy.com/series/playmates-on">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Playmates On" src="https://images.contentful.com/ogz4nxetbde6/5mwQnYL3EsOKCsKakuwU2k/2d44f8a5a1c9aa63fabfa823d04375e8/Playmates-On.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Playmates On</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Drinking Decoded" data-type="tags" href="http://www.playboy.com/series/drinking-decoded">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Drinking Decoded" src="https://images.contentful.com/ogz4nxetbde6/37zXLYoWWsCaOwQ02C6Qkw/10dba5b67a84d3833bbb9ae2b4302a09/drinking-decoded.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Drinking Decoded</h5>
                </a>
                <a class="button" data-action="Popular Series" data-category="Search" data-label="Bartender Confidential" data-type="tags" href="http://www.playboy.com/series/bartender-confidential">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img alt="Bartender Confidential" src="https://images.contentful.com/ogz4nxetbde6/1T38QuN9dOAIy6YQqWSu2C/5e9f7e044fd80cbd98972873a5ae3a33/BartenderConfidential.png?w=70&amp;h=70&amp;fm=jpg&amp;fit=scale" width="200" height="150" layout="fixed"></amp-img> -->
                    <h5>Bartender Confidential</h5>
                </a>
            </div>
        </div>
        <div class="tags" id="search-tags">
            <header>
                <h5>Trending Tags</h5>
            </header>
            <div class="content"><a class="button" data-action="Trending Tags" data-category="Search" data-label="blondes" data-type="tags" href="http://www.playboy.com/tags/blondes">blondes</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="brunettes" data-type="tags" href="http://www.playboy.com/tags/brunettes">brunettes</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="cars" data-type="tags" href="http://www.playboy.com/tags/cars">cars</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="celebrities" data-type="tags" href="http://www.playboy.com/tags/celebrities">celebrities</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="dating advice" data-type="tags" href="http://www.playboy.com/tags/dating-advice">dating advice</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="food and drink" data-type="tags" href="http://www.playboy.com/tags/food-and-drink">food and drink</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="funny" data-type="tags" href="http://www.playboy.com/tags/funny">funny</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="gaming" data-type="tags" href="http://www.playboy.com/tags/gaming">gaming</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="gear" data-type="tags" href="http://www.playboy.com/tags/gear">gear</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="Health &amp; Fitness" data-type="tags" href="http://www.playboy.com/tags/health-fitness">Health &amp; Fitness</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="hugh hefner" data-type="tags" href="http://www.playboy.com/tags/hugh-hefner">hugh hefner</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="movies" data-type="tags" href="http://www.playboy.com/tags/movies">movies</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="music" data-type="tags" href="http://www.playboy.com/tags/music">music</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="music festival" data-type="tags" href="http://www.playboy.com/tags/music-festival">music festival</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="news" data-type="tags" href="http://www.playboy.com/tags/news">news</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="playboy mansion" data-type="tags" href="http://www.playboy.com/tags/playboy-mansion">playboy mansion</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="playmates" data-type="tags" href="http://www.playboy.com/tags/playmates">playmates</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="sports" data-type="tags" href="http://www.playboy.com/tags/sports">sports</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="travel" data-type="tags" href="http://www.playboy.com/tags/travel">travel</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="tv" data-type="tags" href="http://www.playboy.com/tags/tv">tv</a><a class="button" data-action="Trending Tags" data-category="Search" data-label="weed" data-type="tags" href="http://www.playboy.com/tags/weed">weed</a></div>
        </div>
    </div>
</div>
<div class="ad" id="b970"></div>
<footer class="main bg-bunny-inverse" data-action="footer" data-category="Navigation" role="contentinfo">
    <div class="wrapper">
        <section data-label-prefix="Magazine">
            <h3>Magazine</h3>
            <div class="wrapper">
                <a data-label="MagCover Icon" href="https://secure.customersvc.com/servlet/Show?WESPAGE=pm/Pages/load_order.jsp&amp;WESACTIVESESSION=TRUE&amp;PAGE_ID=14198DG001&amp;MAGCODE=PY&amp;MSCCMPLX=1004" rel="">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/4PA10HPf7q6Y2uqGWoYqW/ce0f9027ec0fac1ec2b6e65d966a907f/magazine-cover.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="215" layout="fixed"></amp-img> -->
                </a><a class="button" data-label="MagCover Icon" href="https://secure.customersvc.com/servlet/Show?WESPAGE=pm/Pages/load_order.jsp&amp;WESACTIVESESSION=TRUE&amp;PAGE_ID=14198DG001&amp;MAGCODE=PY&amp;MSCCMPLX=1004" rel="">Subscribe</a></div>
        </section>
        <section data-label-prefix="NSFW">
            <h3>NSFW</h3>
            <div class="wrapper">
                <a data-label="PB Plus Icon" href="http://ma.playboyplus.com/age-gate?url=http://www.playboyplus.com/go/?id=f6b9321b495bf7d2" rel="nofollow">
                <!--{/* @notice */}-->
                    <!--{/* Unable to detect image width or height. Default values were applied. */}-->
                    <!-- <amp-img src="https://images.contentful.com/ogz4nxetbde6/23YnwYGAwIqYaIYmYUYSSw/8419cfaf8cdcf44fb8785f0fa3f0cf6a/pb-plus-house-ad-SM.jpg?fm=jpg&amp;fit=scale&amp;h=215" width="200" height="215" layout="fixed"></amp-img> -->
                </a><a class="button" data-label="PB Plus Icon" href="http://ma.playboyplus.com/age-gate?url=http://www.playboyplus.com/go/?id=f6b9321b495bf7d2" rel="nofollow">Subscribe</a></div>
        </section>
        <section data-label-prefix="Content">
            <h3>Content</h3>
            <div class="wrapper"><a class="footer-nav" data-label="Entertainment" href="http://www.playboy.com/entertainment" rel="">Entertainment</a><a class="footer-nav" data-label="Off Hours" href="http://www.playboy.com/off-hours" rel="">Off Hours</a><a class="footer-nav" data-label="Bunnies" href="http://www.playboy.com/bunnies" rel="">Bunnies</a><a class="footer-nav" data-label="SexCulture" href="http://www.playboy.com/sex-and-culture" rel="">Sex &amp; Culture</a><a class="footer-nav" data-label="heritage" href="http://www.playboy.com/heritage" rel="">Heritage</a><a class="footer-nav" data-label="Video" href="http://www.playboy.com/videos" rel="">Video</a><a class="footer-nav" data-label="" href="https://s3.amazonaws.com/playboy-www-production/assets/Playboy_2018_Media_Kit.pdf" rel="">Advertise With Us </a><a class="footer-nav" data-label="About Us" href="http://www.playboy.com/about" rel="">About Us</a><a class="footer-nav" data-label="Privacy" href="http://www.playboy.com/privacy-policy" rel="">Privacy Policy</a><a class="footer-nav" data-label="Sitemap" href="http://www.playboy.com/sitemap" rel="">Sitemap</a><a class="footer-nav" data-label="TOS" href="http://www.playboyenterprises.com/terms-of-use/" rel="">Terms of Use</a></div>
        </section>
        <section data-label-prefix="Sites">
            <h3>Sites</h3>
            <div class="wrapper"><a class="footer-nav" data-label="Playmates.com" href="http://playmates.com/" rel="">Playmates.com</a><a class="footer-nav" data-label="Playboy Mobile" href="http://playboymobile.com/mobile/" rel="">Download the App</a><a class="footer-nav" data-label="PB Shop" href="http://www.playboyshop.com/?utm_source=playboy&amp;utm_medium=native&amp;utm_campaign=footer" rel="">Playboy Shop</a><a class="footer-nav" data-label="Playboy Archive" href="http://join-iplayboy.covertocover.com/track/MTAwMzk1OS4xLjExLjExLjAuMC4wLjAuMA?autocamp=playboycom_footer" rel="">Playboy Archive</a><a class="footer-nav" data-label="studios.playboy.com" href="http://studios.playboy.com/" rel="">Playboy Studios</a><a class="footer-nav" data-label="PB Enterprises" href="http://www.playboyenterprises.com/" rel="">Playboy Enterprises</a><a class="footer-nav" data-label="" href="https://careers-playboy.icims.com/" rel="">Playboy Careers</a><a class="footer-nav" data-label="Hop" href="https://hop.playboy.com/" rel="">Playboy Events</a><a class="footer-nav" data-label="Playboy Events" href="http://playboyevents.com/" rel="">Playmate Promotions</a><a class="footer-nav" data-label="Playboy Radio" href="http://playboyradio.com/" rel="">Playboy Radio</a><a class="footer-nav" data-label="Playboy TV" href="http://join.playboy.tv/track/MTAwMzk1OS4xMDAxMS4xMDMxLjExMDMuNjk0LjAuMC4wLjA/join?layout=showpage&amp;pg=1" rel="">Playboy.TV</a><a class="footer-nav" data-label="Mag Support" href="https://secure.customersvc.com/servlet/Show?WESPAGE=csp/PB/login.jsp&amp;MSRSMAG=PY" rel="">Magazine Customer Support</a></div>
        </section>
        <p>&copy2018 Playboy Enterprises All&nbsp;rights&nbsp;reserved</p>
    </div>
</footer>
<div class="ad" id="footerOverlay"></div>
<div class="ad" id="flexTakeover"></div>
<div class="ad" id="sideSkins"></div>
<!--{/* @notice */}-->
<!--{/* Tag transition isn't supported in AMP. */}-->
<!-- <transition name="modal"> -->
<!-- </transition> -->
<!--{/* @notice */}-->
<!--{/* Tag transition isn't supported in AMP. */}-->
<!-- <transition name="modal"> -->
<!-- </transition> -->
<!--{/* @notice */}-->
<!--{/* Tag transition isn't supported in AMP. */}-->
<!-- <transition name="modal"> -->
<!-- </transition> -->
</body>
</html>