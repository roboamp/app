@extends("main")

@section ("nonAMP")
    <link rel="canonical" href="{{$path}}" />
@endsection
@section("description")
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "NewsArticle",
        "headline": "{{$headline}}",
        "datePublished": "{{$date}}",
        "image": [
          "{{$theme}}.jpg"
        ]
      }
    </script>
@endsection


@section("style")

    <style amp-custom>
        /* any custom style goes here */
        body {
            background-color: pink;
        }
        amp-img {
            background-color: gray;
            border: 10px solid blue;
        }
    </style>
@endsection

