<?php if($scripts != false){
    $open_script = '<script>';
    $close_script = '</script>';
}else{
    $open_script = '';
    $close_script = '';
}
?>



{!!$open_script!!}
function countUp{{$counter_name}}(count){
    var div_by = 1,
        speed = Math.round(count / div_by),
        $display = $('.{{$counter_name}}'),
        run_count = 1,
        int_speed = 24;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}
countUp{{$counter_name}}({{$new_monthly_count}});
{!!$close_script!!}
