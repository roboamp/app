<section class="panel">
	<div class="symbol" style="background: {{$color}};">
		<i class="fa fa-{{$icon}}"></i>
	</div>
	<div class="value">
		<h1 class="{{$counter_name}}">
		0
		</h1>
		<p>{{$label}} Added in {{$current_month}} {{$year}}</p>
	</div>
</section>