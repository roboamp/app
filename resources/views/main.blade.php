<!doctype html>
<html amp lang="en">
<head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    @yield("title")

    @yield('nonAMP')

    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">


    @yield("description")

    @yield("custom_elements")

    @yield("style")

    @yield("font")


</head>
<body>

    @yield("content")


</body>
</html>