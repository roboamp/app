<div id="{{$id}}"  class="{{$stripe_id}} box pa4 bfree {{$class = (isset($default)?"selected_plan":"")}}">
	<h2 class="mv0"><span style="color: {{$color}};">{{$nickname}}</span></h2>
	<h5 class="mt2 mb4" >{{$sub_title}}</h5>
	<img src="{{$photo}}" />
	<p>
		<br/>
		<strong>{{$title_caption}}</strong>
		<br/>{{$learn_more}}
	</p>
	<button class="cta mt3 bfree" style="background-color:{{$color}}">SELECT PLAN</button>
	<div class="banner pv2" style="color:{{$color}}">{{$label}}</div>
</div>


