<?php
$includes='amp4email.components.includes.';
$source_path="ecommerce/";
//$server="https://amp4email2.roboamp.com";
$server="localhost";
?><!--
## Introduction

This sample demonstrates how to display data from a server in an email and refresh it subsequently
while taking the user's input into account.

Because AMP for Email doesn't allow binding to `[src]` in `amp-list`, it uses a combination of
[`amp-list`](/documentation/components/amp-list) and [`amp-form`](/documentation/components/amp-form)
that share the same [`amp-mustache`](/documentation/components/amp-mustache) template.
When the form is submitted for the first time, the `amp-list` is hidden and the form's response takes its place.
-->

<!-- -->
<!doctype html>
<html ⚡4email>
<head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <!--<style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    -->
    <style amp4email-boilerplate>body{visibility:hidden}</style>

    @include($includes.'style')

</head>
<!-- ## Implementation -->
<!-- -->
<body>



<template id="animal-template" type="amp-mustache">
    @{{#items}}
    <a href="https://google.com" class="commerce-listing-product text-decoration-none inline-block col-6 md-col-4 lg-col-3 px1 mb2 md-mb4 relative">
        <div class="flex flex-column justify-between">
            <div>
                <amp-img class="commerce-listing-product-image mb2" src="@{{image}}" width="340" height="340" layout="responsive" alt="@{{ name }}" noloading="">
                    <div placeholder="" class="commerce-loader"></div></amp-img>
                <h2 class="commerce-listing-product-name h6">@{{ name }}</h2>
                @{{ description }}
            </div>
            <div class="h6 mt1">TAYTUS@{{ price }}</div>
        </div>
    </a>
    @{{/#items}}
</template>
<div>
    <form id="animal-form" method="get" action-xhr="https://amp4email2.roboamp.com/amp4email/asr_lol">

        <div>
            <p>Select an animal to update the server response.</p>
            <select name="animal" on="change:animal-form.submit,animal-list.hide">
                <option value="dog">Puppies</option>
                <option value="cat">Cat</option>
                <option value="parrot">Parrot</option>
            </select>
        </div>
        <div submit-error>Failed to load data.</div>

        <div submit-success template="animal-template"></div>
    </form>


    <amp-list id="animal-list"  template="animal-template" src="https://amp4email2.roboamp.com/amp4email/asr/a/b" height=1000 width=300 layout="responsive"></amp-list>




</div>
</body>
</html>