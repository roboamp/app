<?php
$includes='amp4email.components.includes.';
$source_path="ecommerce/";
?>
<!doctype html>

<!---
Copyright 2017 The AMP Start Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS-IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<html ⚡="" lang="en">

<head>
    <meta charset="utf-8">
    <title>Commerce</title>
    <link rel="canonical" href="https://www.ampstart.com/templates/e-commerce/product-listing.amp">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <meta name="amp-google-client-id-api" content="googleanalytics">

    <script async="" src="https://cdn.ampproject.org/v0.js"></script>



    <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>


    <script custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async=""></script>
    <script custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js" async=""></script>
    <script custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js" async=""></script>

    <script custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js" async=""></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet">

@include($includes.'style')
</head>
<body>

<!-- Start Navbar -->
<header class="ampstart-headerbar fixed flex justify-start items-center top-0 left-0 right-0 pl2 pr4 pt2 md-pt0">
    <div role="button" aria-label="open sidebar" on="tap:header-sidebar.toggle" tabindex="0" class="ampstart-navbar-trigger  pr2 absolute top-0 pr0 mr2 mt2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="block"><path fill="none" d="M0 0h24v24H0z"></path><path fill="currentColor" d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path></svg>
    </div>
    <a href="landing.amp.html" class="text-decoration-none inline-block mx-auto ampstart-headerbar-home-link mb1 md-mb0 ">
        <amp-img src="../img/e-commerce/logo.png" width="320" height="132" layout="fixed" class="my0 mx-auto " alt=""></amp-img>
    </a>
    <!--
      TODO: currently "fixeditems" is an array, therefore it's not possible to
      add additional classes to it. An alternative solution would be to make it
      an oject, with a "classes" and "items" sub-properties:
     "fixeditems": {
       "classes": "col-3",
       "items": [{
         "link": {
           "url": "mailto:contact@lune.com",
           "text": "—contact@lune.com",
           "classes": "xs-small sm-hide h6 bold"
         }
       }]
     }
     -->
    <div class="ampstart-headerbar-fixed center m0 p0 flex justify-center nowrap absolute top-0 right-0 pt2 pr3">
        <div class="mr2">
        </div>
        <a href="cart.amp.html" class="text-decoration-none mr2 ampstart-headerbar-fixed-link ">

            <div class="ampstart-headerbar-icon-wrapper relative"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 37 35"><g><path d="M.8.47598c-.4 0-.8.3-.8.8s.4.9.8.9h4.4l4.4 21.6c0 .5.4 1 .9.9h21c.4 0 .9-.4.9-.9s-.4-.9-.9-.9H11.2l-.7-3.5h22.7c.4 0 .7-.3.8-.7l2.9-13c.1-.5-.3-1.1-.8-1.1H7.5l-.8-3.5c-.1-.3-.4-.6-.8-.6H.8zm7 6h27.3l-2.6 11.3H10.1l-2.3-11.3zm6.9 19.9c-2.1 0-3.8 1.8-3.8 3.9s1.7 3.9 3.8 3.9c2.1 0 3.8-1.8 3.8-3.9 0-2.1-1.7-3.9-3.8-3.9zm12.6 0c-2.1 0-3.8 1.8-3.8 3.9s1.7 3.9 3.8 3.9c2.1 0 3.8-1.8 3.8-3.9 0-2.1-1.7-3.9-3.8-3.9zm-12.6 1.7c1.2 0 2.1 1 2.1 2.2 0 1.2-.9 2.2-2.1 2.2-1.2 0-2.1-1-2.1-2.2 0-1.2.9-2.2 2.1-2.2zm12.6 0c1.2 0 2.1 1 2.1 2.2 0 1.2-.9 2.2-2.1 2.2-1.2 0-2.1-1-2.1-2.2 0-1.2 1-2.2 2.1-2.2z" fill="#222"></path></g></svg></div>
        </a>

    </div>
</header>

<!-- Start Sidebar -->
@include($includes.'sidebar')
<!-- End Sidebar -->
<!-- End Navbar -->
<main id="content" role="main" class="main commerce-listing">
    <amp-state id="products">
        <script type="application/json">
      {
        "category": "all",
        "filter": "high-low"
      }
    </script>
    </amp-state>
   
    <amp-img class="commerce-listing-banner xs-hide sm-hide" src="../img/e-commerce/wide-listings-hero.jpg" width="2560" height="400" layout="responsive" alt="Product listing" noloading=""><div placeholder="" class="commerce-loader"></div></amp-img>
    <section class="commerce-listing-content mx-auto flex flex-wrap pb4">
        <div class="col-3 xs-hide sm-hide flex flex-column">
            <div class="commerce-side-panel pt4 pr4 self-center">
                <h2 class="h5 mb2">Categories</h2>










                <!-- Start Radio -->
                <div class="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 " on="change: AMP.setState({products: {category: 'all'}})">
                    <input type="radio" value="" name="category" id="all" class="relative" checked="">
                    <label for="all" class="" aria-hidden="true">All</label>
                </div>
                <!-- End Radio -->
                <!-- Start Radio -->
                <div class="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 " on="change: AMP.setState({products: {category: 'bikes'}})">
                    <input type="radio" value="" name="category" id="bikes" class="relative">
                    <label for="bikes" class="" aria-hidden="true">Bikes</label>
                </div>
                <!-- End Radio -->
                <!-- Start Radio -->
                <div class="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 " on="change: AMP.setState({products: {category: 'accessories'}})">
                    <input type="radio" value="" name="category" id="accessories" class="relative">
                    <label for="accessories" class="" aria-hidden="true">Accessories</label>
                </div>
                <!-- End Radio -->
                <!-- Start Radio -->
                <div class="ampstart-input ampstart-input-radio inline-block relative m0 p0 mb3 " on="change: AMP.setState({products: {category: 'components'}})">
                    <input type="radio" value="" name="category" id="components" class="relative">
                    <label for="components" class="" aria-hidden="true">Components</label>
                </div>
                <!-- End Radio -->
            </div>
        </div>

        <div class="col-12 md-col-7 pt2 pb3 md-px4 md-pt1 md-pb7">
            <div class="md-commerce-header relative md-flex flex-wrap items-center md-mx0 md-mb2">
                <h1 class="h3 mb2 md-mt2 md-mb2 md-ml0 flex-auto px2">Products</h1>
                <div class="commerce-listing-filters pt2 pb2 mb3 md-mb0">
                    <div class="commerce-select-wrapper inline-block md-mr1 pl2 md-hide lg-hide">
                        <label for="categories" class="bold caps h6 md-h7">Show:</label>
                        <select name="categories" id="categories" class="commerce-select h6 md-h7" on="change: AMP.setState({products: {category: event.value}})">
                            <option value="all">all</option>
                            <option value="bikes">bikes</option>
                            <option value="accessories">accessories</option>
                            <option value="components">components</option>
                        </select>
                    </div>
                    <div class="commerce-select-wrapper inline-block  ">
                        <label for="price" class="bold caps h6 md-h7">Sort by:</label>
                        <select name="price" id="price" class="commerce-select h6 md-h7" on="change: AMP.setState({products: {filter: event.value}})">
                            <option value="high-low">Price: High-Low</option>
                            <option value="low-high">Price: Low-High</option>
                        </select>
                    </div>
                </div>
            </div>

           <!-- <amp-list class="mx1 md-mxn1" [src]="'/email/ecommerce/api/' + products.filter + '-' + products.category + '-products.json'" src="/email/ecommerce/api/high-low-all-products.json" height="1000" width="300" layout="responsive">-->
            <amp-list class="mx1 md-mxn1" [src]="'{!! $source_path!!}' + products.filter + '|' + products.category + '.json'" src="https://amp2/amp4email/endpoints/ecommerce/high-low|all-products.json" height="1000" width="300" layout="responsive">
                <template type="amp-mustache">
                    <a href="product-details.amp.html" target="_self" class="commerce-listing-product text-decoration-none inline-block col-6 md-col-4 lg-col-3 px1 mb2 md-mb4 relative">
                        <div class="flex flex-column justify-between">
                            <div>
                                <amp-img class="commerce-listing-product-image mb2" src="@{{image}}" width="340" height="340" layout="responsive" alt="@{{ name }}" noloading=""><div placeholder="" class="commerce-loader"></div></amp-img>
                                <h2 class="commerce-listing-product-name h6">@{{ name }}</h2>
                                @{{ description }}
                            </div>
                            <div class="h6 mt1">&#163;@{{ price }}</div>
                        </div>
                    </a>
                </template>
            </amp-list>
        </div>
    </section>
</main>
@include($includes.'footer')
</body>
</html>
