<!doctype html>
<html ⚡><!-- saved from url=(0069)https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor -->
<head>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <meta name="viewport" content="width=device-width,minimum-scale=1">



<title>A day in the life of an engineer turned investor</title>

<meta name="description" content="When I talk to former colleagues about my life at Matter, and in particular how much of my day I spend talking to people. As">
<meta name="generator" content="Known https://withknown.com">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Status" content="200">
<!--Le fav and touch icons-->
<link rel="apple-touch-icon" sizes="57x57" href="https://werd.io/gfx/logos/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://werd.io/gfx/logos/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://werd.io/gfx/logos/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="144x144" href="https://werd.io/gfx/logos/apple-icon-144x144.png">
<link rel="shortcut icon" type="image/jpg" href="{{asset('ben')}}/thumb.jpg">
<!-- Make this an "app" when saved to an ios device's home screen -->
<link rel="apple-touch-icon-precomposed" href="{{asset('ben')}}/thumb.jpg">
<!-- Make this an "app" when saved to an ios device's home screen -->
<link rel="apple-touch-icon" href="{{asset('ben')}}/thumb.jpg">
<!-- <meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black"> -->

<!-- Android -->
<link rel="manifest" href="https://werd.io/chrome/manifest.json">

<meta property="og:type" content="article">
<meta property="og:title" content="A day in the life of an engineer turned investor">
<meta property="og:site_name" content="Ben Werdmüller">
<meta property="og:image" content="https://werd.io/file/56c4b818bed7de5b507fa2a5/thumb.jpg">
<meta property="og:description" content="When I talk to former colleagues about my life at Matter, and in particular how much of my day I spend talking to people. As">
<meta property="og:url" content="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor">
<meta property="og:image:width" content="300">
<meta property="og:image:height" content="300">


    <!-- Twitter card -->
            <meta name="twitter:card" content="summary">
            <meta name="twitter:site" content="@benwerd">
            <meta name="twitter:title" content="A day in the life of an engineer turned investor">
            <meta name="twitter:description" content="When I talk to former colleagues about my life at Matter, and in particular how much of my day I spend talking to people. As">

            <meta name="twitter:image" content="https://werd.io/file/56c4b818bed7de5b507fa2a5/thumb.jpg">
<!-- Dublin Core -->
<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<meta name="DC.title" content="A day in the life of an engineer turned investor">
<meta name="DC.description" content="When I talk to former colleagues about my life at Matter, and in particular how much of my day I spend talking to people. As">                <meta name="DC.creator" content="Ben Werdmüller">                <meta name="DC.date" content="2018-03-14T17:03:55+00:00">                <meta name="DC.identifier" content="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor"><!-- We need jQuery at the top of the page -->
<div class="fit-vids-style">­<style>               .fluid-width-video-wrapper {                 width: 100%;                              position: relative;                       padding: 0;                            }                                                                                   .fluid-width-video-wrapper iframe,        .fluid-width-video-wrapper object,        .fluid-width-video-wrapper embed {           position: absolute;                       top: 0;                                   left: 0;                                  width: 100%;                              height: 100%;                          }                                       </style></div><script async="" src="{{asset('ben')}}/analytics.js"></script><script src="{{asset('ben')}}/jquery-3.2.1.min.js"></script><style>@media print {#ghostery-purple-box {display:none !important}}</style>

<!-- Le styles -->
<link href="{{asset('ben')}}/bootstrap.min.css" rel="stylesheet">
<link href="https://werd.io/external/bootstrap/assets/css/bootstrap-theme.min.css">
<script src="{{asset('ben')}}/bootstrap.min.js"></script>

<!-- Accessibility -->
<link rel="stylesheet" href="{{asset('ben')}}/bootstrap-accessibility_1.0.3.css">
<script src="{{asset('ben')}}/bootstrap-accessibility_1.0.3.min.js"></script>

<!-- Fonts -->
<link rel="stylesheet" href="{{asset('ben')}}/fontawesome-all.min.css">

<style>
    body {
        padding-top: 100px; /* 60px to make the container go all the way to the bottom of the topbar */
    }
</style>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script
    src="https://werd.io/external/bootstrap/assets/js/html5shiv.js"></script>
<![endif]-->
<script>
    var known = {"session":{"loggedIn":false,"admin":false},"config":{"displayUrl":"https:\/\/werd.io\/","debug":false},"page":{"currentUrl":"https:\/\/werd.io\/2018\/a-day-in-the-life-of-an-engineer-turned-investor","currentUrlFragments":{"scheme":"https","host":"werd.io","path":"\/2018\/a-day-in-the-life-of-an-engineer-turned-investor"}}};
</script>
<!-- Default Known JavaScript -->
<script src="{{asset('ben')}}/default.min.js"></script>

<script src="{{asset('ben')}}/bootstrap-toggle.js"></script>

<link href="{{asset('ben')}}/default.min.css" rel="stylesheet">
<link href="{{asset('ben')}}/defaultb3.min.css" rel="stylesheet">

<!-- Syndication -->
<link href="{{asset('ben')}}/bootstrap-toggle.min.css" rel="stylesheet">

<!-- Mention styles -->
<link rel="stylesheet" type="text/css" href="{{asset('ben')}}/recommended-styles.css">


<!-- To silo is human, to syndicate divine -->
<link rel="alternate" type="application/rss+xml" title="A day in the life of an engineer turned investor" href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor?_t=rss">
<link rel="feed" type="application/rss+xml" title="A day in the life of an engineer turned investor" href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor?_t=rss">
<link rel="alternate feed" type="application/rss+xml" title="Ben Werdmüller: all content" href="https://werd.io/content/all?_t=rss">
<link rel="alternate" type="application/json" title="Ben Werdmüller: all content" href="https://werd.io/content/all?_t=rss">
<link rel="feed" type="text/html" title="Ben Werdmüller" href="https://werd.io/content/all">

<!-- Webmention endpoint -->
<link href="https://werd.io/webmention/" rel="http://webmention.org/">
<link href="https://werd.io/webmention/" rel="webmention">

    <!-- Pubsubhubbub -->
    <link href="http://withknown.superfeedr.com/" rel="hub">
<script src="{{asset('ben')}}/wavesurfer.min.js"></script><script src="{{asset('ben')}}/embed.js"></script><link rel="openid.delegate" href="https://werd.io/">
<link rel="openid.server" href="https://indieauth.com/openid">
<link rel="authorization_endpoint" href="https://werd.io/indieauth/auth">
<link rel="token_endpoint" href="https://werd.io/indieauth/token">
<link rel="micropub" href="https://werd.io/micropub/endpoint"><link rel="stylesheet" href="{{asset('ben')}}/leaflet.css">
<!--[if lte IE 8]>
<link rel="stylesheet" href="https://werd.io/IdnoPlugins/Checkin/external/leaflet/leaflet.ie.css"/>
<![endif]-->
<script type="text/javascript" src="{{asset('ben')}}/leaflet.js"></script>
<script type="text/javascript" src="{{asset('ben')}}/tile.stamen.js"></script>
<link href="{{asset('ben')}}/default.min(1).css" rel="stylesheet">
<style>
    body {
        background-position: center;
        background-attachment: fixed;
        background-size: cover;
    }
</style>
<link rel="stylesheet" href="{{asset('ben')}}/style.site.css">
<style type="text/css">#mc_embed_signup input.mce_inline_error { border-color:#6B0505; } #mc_embed_signup div.mce_inline_error { margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff; }</style></head>

<body class="idno_pages_entity_view page-2018 page-2018-a-day-in-the-life-of-an-engineer-turned-investor logged-out" data-gr-c-s-loaded="true" style="zoom: 1;" cz-shortcut-listen="true">

    <a href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor#maincontent" style="display:none">Skip to main content</a>
    <div class="">
        <div class="container page-body">
            <div id="page-messages">

    </div>            <a name="pagecontent"></a>
                        <div class="row idno-entry idno-entry-entry">
                <div class="col-md-8 col-md-offset-2 h-entry idno-posts idno-object idno-content">
                    <div>

                        <div class="break">&nbsp;</div>
                    </div>
                    <div class="datestamp">
                        <p>
                            <a class="u-url url" href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor" rel="permalink">
                                <time class="dt-published" datetime="2018-03-14T17:03:55+00:00" title="March 14, 2018">March 14, 2018</time>
                            </a>
                        </p>
                    </div>
                                        <div class="idno-body">
                      
        <h2 class="p-name"><a href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor">A day in the life of an engineer turned investor</a>
        </h2>
            <p class="reading">
            <span class="vague">4 min read </span>
        </p>
    <div class="e-content entry-content">
<p>When I talk to former colleagues about my life at <a href="https://matter.vc/">Matter</a>, and in particular how much of my day I spend <em>talking</em> to people. As an engineer, maybe I had three meetings a week; these days it's often&nbsp;eight a day. And I love it:&nbsp;as a former founder, I'm excited&nbsp;to meet with hundreds of people who are all working on things they care deeply about - and I'm excited to&nbsp;find them.</p><p>This is what yesterday looked like for me:</p><p>7am: I finished a blog post draft that will be published on Thursday. I'm excited about intelligent assistants and the shift to ambient computing, and I&nbsp;was able to&nbsp;back up my piece with sources from&nbsp;an internal investment trend document I wrote.</p><p>8am: Headed into work, listening to <a href="https://www.wnycstudios.org/shows/otm/">On the Media</a>, my favorite podcast.</p><p>9am:&nbsp;Caught up with email.&nbsp;I'm still figuring out a process for this: I get more than I can really handle, and I don't feel good about sending one-line responses.</p><p>9:30am: A standup with the team,&nbsp;talking about the day, and any new developments.</p><p>10am: I welcomed a group of&nbsp;foreign journalists who were interested in Matter. We talked for an hour about new trends, how we think about&nbsp;products vs teams (hint: <a href="https://matter.vc/apply">we invest in teams</a>), and whether there's still a future for print.</p><p>11am and 11:30am: I jumped on the phone with some founders&nbsp;who wanted to learn more about Matter, and whether it would be a good fit for their companies.</p><p>12pm:&nbsp;More email, including outreach to some startups that I'm hoping will apply. There are a lot of people out there who don't think of themselves as working on a media startup, but&nbsp;who are&nbsp;exactly what we're looking for, and who could be substantially helped <a href="https://matter.vc/program">by the Matter program</a>.</p><p>1pm: I joined in on a workshop with <a href="https://medium.com/matter-driven-narrative/meet-matter-eight-f3910567407e">our Matter Eight teams</a>, thinking about how to pin down the top-down trends that make their startups good investments. Key question: why is&nbsp;<em>now </em>the right time for this&nbsp;venture? Our Directors of Program are, frankly, geniuses at&nbsp;helping people think their way through these kinds of&nbsp;questions, and I'm always excited to learn from them.</p><p>2pm: I sat down with&nbsp;the CEOs of one of our portfolio companies to give them some feedback on how they're describing their venture to investors.</p><p>3pm: I spoke to another&nbsp;founder who&nbsp;didn't join Matter, but wanted to give me an update about where they were. It's always exciting to hear about how a team has progressed.</p><p>3:30pm: I took an audit of our application process on the web. Some applicants drop off while they're filling in the form, and I wanted to know where that might be happening. At the same time,&nbsp;I&nbsp;did some SEO&nbsp;work on the website. (SEO work follows me in every role, wherever I go.)</p><p>4pm: I&nbsp;have a personal goal of reaching out to at least five startups a day - so&nbsp;I spent more time&nbsp;doing research and uncovering both communities to visit and&nbsp;events to attend, as well as individual startups that I would love to see join the program.</p><p>5pm: Facilitated introductions for some portfolio founders who wanted to meet certain investors. I always do double blind introductions, asking the investors first if they want to connect. Then I turned to going over our applicants, reading through their decks, and doing some research on their markets and founders.</p><p>7pm: I went home to eat.</p><p>8pm:&nbsp;I caught up on my RSS subscriptions, reading about the various industries and founders I'm interested in.</p><p>There's no time for coding anymore - but there's a lot to do, and I couldn't be happier to support these amazing founders. If that's you,&nbsp;<a href="https://matter.vc/apply">applications are open now</a>.</p></div>
                    </div>
                    <div class="footer">
                        <div class="permalink">
    <p>
                            </p>
</div>
<div class="interactions">
	<span class="annotate-icon">
                    <a class="stars" href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor#comments"><i class="fa fa-star-o"></i> 0 stars</a></span>
            	    <span class="annotate-icon"><a class="comments" href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor#comments"><i class="fa fa-comments"></i> 0 comments</a></span>
    <a class="shares" href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor#comments"></a>
    <a class="rsvps" href="https://werd.io/2018/a-day-in-the-life-of-an-engineer-turned-investor#comments"></a>
</div>
<br class="clearall">
      <div class="posse">
    <a name="posse"></a>

    <p>
        Also on:
        <a href="https://twitter.com/benwerd/status/973967976696336385" rel="syndication" class="u-syndication twitter"><i class="fa fa-twitter"></i>
 @benwerd</a>    </p>
</div>
                    </div>
                </div>

            </div>

                </div>
    </div>

        
<!-- Flexible media player -->
<script src="{{asset('ben')}}/mediaelement-and-player.min.js"></script>
<link rel="stylesheet" href="{{asset('ben')}}/mediaelementplayer.min.css">
<!-- Video shim -->
<script src="{{asset('ben')}}/jquery.fitvids.min.js"></script>
<!-- Placed at the end of the document so the pages load faster -->


<script src="{{asset('ben')}}/jquery.timeago.min.js"></script>
<script src="{{asset('ben')}}/underscore-min.js" type="text/javascript"></script>
<!--<script src="https://werd.io/external/mention/bootstrap-typeahead.js"
        type="text/javascript"></script>
<script src="https://werd.io/external/mention/mention.js"
        type="text/javascript"></script> -->

<!-- Begin asset javascript -->            <script src="{{asset('ben')}}/image.min.js"></script>
                        <script src="{{asset('ben')}}/exif.min.js"></script>
            <!-- End asset javascript -->
<!-- HTML5 form element support for legacy browsers -->
<script src="{{asset('ben')}}/h5f.min.js"></script>

<script src="{{asset('ben')}}/shell.min.js"></script>
<script src="{{asset('ben')}}/embeds.min.js"></script>
<script>
    $('video').mediaelementplayer({
        features: ['playpause','progress','current','tracks','volume','fullscreen'],
        alwaysShowControls: false,
        pauseOtherPlayers: true,
        enableAutosize: false,
        mediaWidth: -1,
        mediaHeight: -1
    });
    $('audio').mediaelementplayer({
        features: ['playpause','progress','current','tracks','volume','fullscreen'],
        alwaysShowControls: false,
        pauseOtherPlayers: true,
        enableAutosize: false,
        mediaWidth: -1,
        mediaHeight: -1
    });
</script>
<style>
    .known-media-element {
        width: 100%;
        height: 100%;
    }
</style><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56674978-1', 'auto');
  ga('send', 'pageview');

</script>
<div class="blank-footer">    


</div>

</body></html>