<!doctype html>
<!--{/*
@info
Generated on: Thu, 10 May 2018 13:48:12 GMT
Initiator: ROBOAMP AMP Generator
Generator version: 0.9.6
*/}-->
<html ⚡ lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>Matter. | Changing Media For Good</title>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <script src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js" async="async" custom-element="amp-analytics"></script>
        <script src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async="async" custom-element="amp-accordion"></script>
        <script src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async="async" custom-element="amp-sidebar"></script>
        <script src="https://cdn.ampproject.org/v0/amp-vimeo-0.1.js" async="async" custom-element="amp-vimeo"></script>
        <link rel="canonical" href="https://matter.vc/">
        <meta name="generator" content="https://generator.rabbit.gomobile.jp">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta name="robots" content="index, follow">
        <meta name="description" content="We support media entrepreneurs building a more informed, inclusive, and empathetic society through our 5-month startup accelerator program in San Francisco and New York City.">
        <meta name="msapplication-TileImage" content="https://matter.vc/wp-content/uploads/2018/03/cropped-2ce73553b6dc1c1dad2b4e3fa4da3b9b_400x400-1-270x270.jpeg">
        <meta property="og:locale" content="en_US">
        <meta property="og:site_name" content="Matter.">
        <meta property="og:title" content="Matter.">
        <meta property="og:url" content="http://matter.vc/">
        <meta property="og:type" content="website">
        <meta property="og:description" content="We support entrepreneurs building a more informed, inclusive, and empathetic society.">
        <meta property="og:image" content="http://matter.vc/wp-content/uploads/2017/02/1-WCiaytkit5BT7jueZsT9OQ.jpeg">
        <meta property="article:publisher" content="https://facebook.com/mattervc">
        <meta itemprop="name" content="Matter.">
        <meta itemprop="headline" content="Matter.">
        <meta itemprop="description" content="We support entrepreneurs building a more informed, inclusive, and empathetic society.">
        <meta itemprop="image" content="http://matter.vc/wp-content/uploads/2017/02/1-WCiaytkit5BT7jueZsT9OQ.jpeg">
        <meta name="twitter:title" content="Matter.">
        <meta name="twitter:url" content="http://matter.vc/">
        <meta name="twitter:description" content="We support entrepreneurs building a more informed, inclusive, and empathetic society.">
        <meta name="twitter:image" content="http://matter.vc/wp-content/uploads/2017/02/1-WCiaytkit5BT7jueZsT9OQ.jpeg">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@mattervc">
        <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript data-amp-spec=""><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter%7COpen+Sans:400,600">
        <style amp-custom="">html {
  min-width: 910px
}
.container {
  position: relative;
  width: 100%;
  margin: 0 auto;
  padding: 0 50px;
  clear: both
}
.inner-container {
  position: relative;
  height: 100%;
  width: 100%
}
.container_wrap {
  clear: both;
  position: relative;
  border-top-style: solid;
  border-top-width: 1px
}
.units {
  float: left;
  display: inline;
  margin-left: 50px;
  position: relative;
  z-index: 1;
  min-height: 1px
}
#wrap_all {
  width: 100%;
  position: static;
  z-index: 2;
  overflow: hidden
}
body .units.alpha,
body div .first {
  margin-left: 0;
  clear: left
}
body .units.alpha {
  width: 100%
}
.container .av-content-full.units {
  width: 100%
}
.container {
  max-width: 1010px
}
.container:after {
  content: "\0020";
  display: block;
  height: 0;
  clear: both;
  visibility: hidden
}
.clearfix:after,
.clearfix:before,
.flex_column:after,
.flex_column:before,
.widget:after,
.widget:before {
  content: '\0020';
  display: block;
  overflow: hidden;
  visibility: hidden;
  width: 0;
  height: 0
}
.clearfix:after,
.flex_column:after {
  clear: both
}
.clearfix {
  zoom: 1
}
body div .first,
body div .no_margin {
  margin-left: 0
}
div .flex_column {
  z-index: 1;
  float: left;
  position: relative;
  min-height: 1px;
  width: 100%
}
div .av_one_fifth {
  margin-left: 6%;
  width: 15.2%
}
div .av_one_fourth {
  margin-left: 6%;
  width: 20.5%
}
div .av_two_fifth {
  margin-left: 6%;
  width: 36.4%
}
div .av_one_half {
  margin-left: 6%;
  width: 47%
}
#top div .no_margin {
  margin-left: 0;
  margin-top: 0
}
#top .no_margin.av_one_fourth {
  width: 25%
}
a,
amp-img,
b,
body,
div,
form,
h1,
h2,
h3,
header,
html,
iframe,
li,
nav,
p,
section,
span,
strong,
ul {
  margin: 0;
  border: 0
}
header,
nav,
section {
  display: block
}
body {
  line-height: 1
}
ul {
  list-style: none
}
* {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box
}
body {
  font: 13px/1.65em HelveticaNeue,"Helvetica Neue",Helvetica,Arial,sans-serif;
  color: #444;
  -webkit-text-size-adjust: 100%
}
h1,
h2,
h3 {
  font-weight: 600
}
h1 {
  font-size: 34px;
  line-height: 1.1em;
  margin-bottom: 14px
}
h2 {
  font-size: 28px;
  line-height: 1.1em;
  margin-bottom: 10px
}
h3 {
  font-size: 20px;
  line-height: 1.1em;
  margin-bottom: 8px
}
p {
  margin: .85em 0
}
b,
strong {
  font-weight: 700
}
a {
  text-decoration: none;
  outline: 0;
  max-width: 100%
}
a:focus,
a:hover,
a:visited {
  outline: 0;
  text-decoration: underline
}
p a,
p a:visited {
  line-height: inherit
}
#top .avia_hidden_link_text {
  display: none
}
ul {
  margin-bottom: 20px
}
ul {
  list-style: none outside;
  margin-left: 7px
}
a amp-img,
amp-img {
  border: none;
  margin: 0;
  max-width: 100%;
  height: auto
}
.button,
input[type=submit] {
  padding: 9px 22px;
  cursor: pointer;
  border: none;
  -webkit-appearance: none;
  border-radius: 0
}
[data-av_icon]:before {
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-weight: 400;
  content: attr(data-av_icon);
  speak: none
}
#top form {
  margin-bottom: 20px
}
#top input[type=email] {
  -webkit-appearance: none;
  border: 1px solid #e1e1e1;
  padding: 8px 6px;
  outline: 0;
  font: 13px HelveticaNeue,"Helvetica Neue",Helvetica,Arial,sans-serif;
  color: #777;
  margin: 0;
  width: 100%;
  display: block;
  margin-bottom: 20px;
  background: #fff;
  border-radius: 0
}
#top input[type=email]:focus {
  box-shadow: 0 0 2px 0 rgba(0,0,0,.2);
  color: #555
}
iframe {
  max-width: 100%
}
#header {
  position: relative;
  z-index: 501;
  width: 100%;
  background: 0 0
}
#header_main .container,
.main_menu ul:first-child > li a {
  height: 88px;
  line-height: 88px
}
#header_main {
  border-bottom-width: 1px;
  border-bottom-style: solid;
  z-index: 1
}
.header_bg {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: .95;
  z-index: 0;
  -webkit-perspective: 1000px;
  -webkit-backface-visibility: hidden
}
div .logo {
  float: left;
  position: absolute;
  left: 0;
  z-index: 1
}
.logo,
.logo a {
  overflow: hidden;
  position: relative;
  display: block;
  height: 100%
}
.logo amp-img {
  width: auto;
  height: auto;
  max-height: 100%
}
.main_menu {
  clear: none;
  position: absolute;
  z-index: 100;
  line-height: 30px;
  height: 100%;
  margin: 0;
  right: 0
}
.av-main-nav-wrap {
  float: left;
  position: relative;
  z-index: 3
}
.av-main-nav-wrap ul {
  margin: 0;
  padding: 0
}
.av-main-nav li {
  float: left;
  position: relative;
  z-index: 20
}
.av-main-nav li:hover {
  z-index: 100
}
.av-main-nav > li {
  line-height: 30px
}
.av-main-nav li a {
  max-width: none
}
.av-main-nav > li > a {
  display: block;
  text-decoration: none;
  padding: 0 13px;
  font-weight: 400;
  font-size: 12px;
  font-weight: 600;
  font-size: 13px
}
.avia-menu-fx {
  position: absolute;
  bottom: -1px;
  height: 2px;
  z-index: 10;
  width: 100%;
  left: 0;
  opacity: 1;
  visibility: hidden
}
.av-main-nav li:hover .avia-menu-fx,
.current-menu-item > a > .avia-menu-fx {
  opacity: 1;
  visibility: visible
}
.avia-menu-fx .avia-arrow-wrap {
  height: 10px;
  width: 10px;
  position: absolute;
  top: -10px;
  left: 50%;
  margin-left: -5px;
  overflow: hidden;
  display: none;
  visibility: hidden
}
.current-menu-item > a > .avia-menu-fx > .avia-arrow-wrap,
.current_page_item > a > .avia-menu-fx > .avia-arrow-wrap {
  display: block
}
.avia-menu-fx .avia-arrow-wrap .avia-arrow {
  top: 10px
}
.main_menu .avia-bullet {
  display: none
}
.avia-bullet {
  display: block;
  position: absolute;
  height: 0;
  width: 0;
  top: 51%;
  margin-top: -3px;
  left: -3px;
  border-top: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-left: 3px solid green
}
.avia-arrow {
  height: 10px;
  width: 10px;
  position: absolute;
  top: -6px;
  left: 50%;
  margin-left: -5px;
  -webkit-transform: rotate(45deg);
  transform: rotate(45deg);
  border-width: 1px;
  border-style: solid
}
.content {
  padding-top: 50px;
  padding-bottom: 50px;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  min-height: 1px;
  z-index: 1
}
.content:hover {
  z-index: 1
}
.content {
  border-right-style: solid;
  border-right-width: 1px;
  margin-right: -1px
}
.content .entry-content-wrapper {
  padding-right: 50px
}
.fullsize .content {
  margin: 0;
  border: none
}
.fullsize .content .entry-content-wrapper {
  padding-right: 0
}
.template-page .entry-content-wrapper h1 {
  text-transform: uppercase;
  letter-spacing: 1px
}
.widgettitle {
  font-weight: 600;
  text-transform: uppercase;
  letter-spacing: 1px;
  font-size: 1.1em
}
.widget {
  clear: both;
  position: relative;
  padding: 30px 0 30px 0;
  float: none
}
#footer .widget {
  padding: 0;
  margin: 30px 0 30px 0;
  overflow: hidden
}
#top .widget ul {
  padding: 0;
  margin: 0;
  width: 100%;
  float: left
}
#top #footer .widget ul {
  float: none
}
.widget li {
  clear: both
}
.widget_nav_menu a {
  display: block;
  padding: 4px 0 5px 0;
  text-decoration: none
}
div .widget_nav_menu {
  padding-bottom: 24px
}
#top .widget_nav_menu ul {
  margin: 0;
  padding: 0;
  float: none;
  list-style-type: none
}
#top .widget_nav_menu li {
  position: relative;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  clear: both
}
#top #footer .widget_nav_menu li {
  background-color: transparent
}
.widget_nav_menu .current-menu-item > a,
.widget_nav_menu .current_page_item > a {
  font-weight: 700
}
.widget_nav_menu ul:first-child > .current-menu-item > a,
.widget_nav_menu ul:first-child > .current_page_item > a {
  border: none;
  padding: 6px 7px 7px 0
}
#footer {
  padding: 15px 0 30px 0;
  z-index: 1
}
#scroll-top-link {
  position: fixed;
  border-radius: 2px;
  height: 50px;
  width: 50px;
  line-height: 50px;
  text-decoration: none;
  text-align: center;
  opacity: 1;
  right: 50px;
  bottom: 50px;
  z-index: 1030;
  visibility: hidden
}
.widget li {
  line-height: 1.8em;
  font-size: 15px
}
#advanced_menu_hide,
#advanced_menu_toggle {
  position: absolute;
  border-radius: 2px;
  height: 46px;
  width: 46px;
  line-height: 46px;
  text-decoration: none;
  text-align: center;
  right: 0;
  top: 50%;
  margin-top: -23px;
  z-index: 10000;
  border-style: solid;
  border-width: 1px;
  font-size: 30px;
  display: none
}
#advanced_menu_hide {
  z-index: 10001;
  visibility: hidden;
  opacity: 1;
  top: 44px;
  right: 23px
}
.main_menu .avia-menu {
  display: none
}
#advanced_menu_hide {
  display: block
}
.entry-content-wrapper:empty,
p:empty {
  display: none
}
.avia-shadow {
  box-shadow: inset 0 1px 3px rgba(0,0,0,.1)
}
body .container_wrap .avia-builder-el-no-sibling {
  margin-top: 0;
  margin-bottom: 0
}
body .container_wrap .avia-builder-el-last {
  margin-bottom: 0
}
body .container_wrap .avia-builder-el-first {
  margin-top: 0
}
#top .av_inherit_color * {
  color: inherit
}
.avia_textblock {
  clear: both;
  line-height: 1.65em
}
.av-special-heading {
  width: 100%;
  clear: both;
  display: block;
  margin-top: 50px;
  overflow: hidden;
  position: relative
}
body .av-special-heading .av-special-heading-tag {
  padding: 0;
  margin: 0;
  float: left
}
.special-heading-border {
  position: relative;
  overflow: hidden
}
.av-special-heading-h1 .special-heading-border {
  height: 3.4em
}
.special-heading-inner-border {
  display: block;
  width: 100%;
  margin-left: 15px;
  border-top-style: solid;
  border-top-width: 1px;
  position: relative;
  top: 50%;
  opacity: .15
}
.modern-quote .av-special-heading-tag {
  font-weight: 300
}
body .av-special-heading.modern-centered {
  text-align: center
}
body .av-special-heading.blockquote > * {
  white-space: normal;
  float: none
}
body .av-special-heading.blockquote .special-heading-border {
  display: none
}
.hr {
  clear: both;
  display: block;
  width: 100%;
  height: 25px;
  line-height: 25px;
  position: relative;
  margin: 30px 0;
  float: left
}
.hr-inner {
  width: 100%;
  position: absolute;
  height: 1px;
  left: 0;
  top: 50%;
  width: 100%;
  margin-top: -1px;
  border-top-width: 1px;
  border-top-style: solid
}
#top .hr-invisible,
.hr-invisible .hr-inner {
  margin: 0;
  border: none
}
.hr-invisible {
  float: none
}
body .container_wrap .hr.avia-builder-el-first,
body .container_wrap .hr.avia-builder-el-last {
  margin: 30px 0
}
.avia-button {
  color: #777;
  border-color: #e1e1e1;
  background-color: #f8f8f8
}
body div .avia-button {
  border-radius: 3px;
  padding: 10px;
  font-size: 12px;
  text-decoration: none;
  display: inline-block;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  margin: 3px 0;
  line-height: 1.2em;
  position: relative;
  font-weight: 400;
  text-align: center;
  max-width: 100%
}
.avia-button:hover {
  opacity: .9
}
.avia-button:active {
  border-bottom-width: 0;
  border-top-width: 1px;
  border-top-style: solid
}
.avia-button.avia-color-red,
.avia-button.avia-color-red:hover {
  background-color: #b02b2c;
  border-color: #8b2121;
  color: #fff
}
.avia-button.avia-color-blue,
.avia-button.avia-color-blue:hover {
  background-color: #7bb0e7;
  border-color: #6693c2;
  color: #fff
}
#top .av-custom-form-color ::-webkit-input-placeholder {
  color: inherit;
  opacity: .8
}
#top .av-custom-form-color ::-moz-placeholder {
  color: inherit;
  opacity: .8
}
#top .av-custom-form-color :-ms-input-placeholder {
  color: inherit;
  opacity: .8
}
.flex_column .widget:first-child {
  padding-top: 0;
  border-top: none
}
.flex_column .widget .widgettitle {
  margin-top: .85em
}
.avia-image-container {
  display: block;
  position: relative;
  max-width: 100%
}
.av-image-caption-overlay,
.avia-image-container-inner,
.avia_image {
  border-radius: 3px;
  display: block;
  position: relative;
  max-width: 100%
}
.avia-image-container.avia-align-center {
  display: block;
  margin: 0 auto 10px auto;
  text-align: center;
  clear: both
}
.avia-image-container.avia-align-center .avia-image-container-inner {
  margin: 0 auto;
  display: inline-block;
  vertical-align: bottom
}
.avia-image-container.avia-align-center.avia-builder-el-no-sibling {
  margin-bottom: 0;
  margin-top: 0
}
.av-styling-no-styling .avia-image-container-inner,
.av-styling-no-styling .avia_image {
  border-radius: 0;
  border: none
}
.av-image-caption-overlay {
  position: absolute;
  height: 100%;
  width: 100%;
  z-index: 10;
  text-align: center
}
.av-image-caption-overlay-position {
  display: table;
  width: 100%;
  height: 100%;
  position: relative
}
.av-image-caption-overlay-center {
  display: table-cell;
  vertical-align: middle;
  font-size: 1.3em;
  line-height: 1.5em;
  padding: 0 1.5em
}
.av-image-caption-overlay-center p:first-child {
  margin-top: 0
}
.av-image-caption-overlay-center p:last-child {
  margin-bottom: 0
}
.av-caption-image-overlay-bg {
  position: absolute;
  height: 100%;
  width: 100%
}
.av-overlay-hover-deactivate:hover .av-caption-image-overlay-bg {
  opacity: 0
}
.avia-iframe-wrap,
.avia-video {
  clear: both;
  position: relative;
  margin-bottom: 20px
}
.avia-video iframe,
div .avia-video .avia-iframe-wrap {
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  padding: 0
}
.avia-video-16-9 {
  padding-bottom: 56.25%;
  height: 0
}
.av-hover-grow {
  overflow: hidden
}
.avia-slideshow {
  position: relative;
  margin: 50px 0;
  width: 100%;
  clear: both;
  overflow: hidden
}
.avia-slideshow-inner {
  padding: 0;
  margin: 0;
  position: relative;
  overflow: hidden;
  width: 100%
}
#top .av-default-height-applied .avia-slideshow-inner {
  height: 0
}
.avia-slideshow li {
  padding: 0;
  margin: 0;
  list-style-type: none;
  list-style-position: outside;
  position: absolute;
  visibility: hidden;
  z-index: 1;
  top: 0;
  left: 0;
  width: 100%;
  clear: both;
  opacity: 1
}
.avia-slideshow li:first-child {
  position: relative;
  visibility: visible;
  z-index: 3
}
.avia-slideshow li amp-img {
  width: 100%;
  margin: 0 auto;
  border-radius: 3px
}
#top .avia-slideshow-arrows a {
  display: block;
  text-decoration: none;
  color: #fff;
  visibility: visible;
  position: absolute;
  width: 60px;
  text-align: center;
  height: 60px;
  line-height: 62px;
  font-size: 25px;
  top: 50%;
  margin: -30px 15px 0;
  z-index: 99;
  overflow: hidden;
  text-indent: -600%
}
#top .avia-slideshow-arrows a.next-slide {
  right: 0
}
.avia-slideshow-arrows a:before {
  visibility: visible;
  display: block;
  position: absolute;
  z-index: 100;
  background: #aaa;
  background: rgba(0,0,0,.3);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  border-radius: 3px;
  text-align: center;
  line-height: 62px;
  color: inherit
}
.prev-slide:before {
  text-indent: -2px;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px
}
.next-slide:before {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
  text-indent: 0
}
.avia-slideshow-dots {
  position: relative;
  z-index: 200;
  width: 100%;
  bottom: 0;
  text-align: center;
  left: 0;
  height: 0
}
.avia-slideshow-dots a {
  display: inline-block;
  height: 13.5px;
  width: 13.5px;
  border-radius: 14px;
  background: #000;
  opacity: .6;
  text-indent: 100px;
  overflow: hidden;
  margin: 0 1px;
  padding: 7px;
  position: relative;
  bottom: 33px
}
.avia-slideshow-dots a.active,
.avia-slideshow-dots a:hover {
  opacity: .8;
  background: #fff
}
#top .av-control-hidden .avia-slideshow-controls {
  display: none
}
#top .av-video-slide,
#top .av-video-slide .avia-slide-wrap {
  width: 100%;
  height: 100%;
  position: absolute;
  overflow: hidden
}
#top .av-video-slide .caption_fullwidth {
  top: 0;
  left: 0;
  right: 0;
  bottom: 40px
}
#top .av-video-slide.av-hide-video-controls .caption_fullwidth {
  bottom: 0
}
.av-video-slide .mejs-mediaelement {
  height: 100%
}
#top .av-video-slide .avia-slide-wrap {
  background: #000 url(https://matter.vc/wp-content/themes/enfold/images/layout/preload-dark-grey-big.gif) no-repeat center center
}
#top .av-video-slide.av-video-service-youtube .avia-slide-wrap {
  background: #000
}
.av-click-overlay {
  content: ".";
  position: absolute;
  text-indent: -200px;
  overflow: hidden;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 11;
  opacity: 1;
  background: #000
}
.av-click-overlay {
  z-index: 9;
  bottom: 30px
}
.av-hide-video-controls .av-click-overlay {
  bottom: 0
}
.av-video-service-youtube .av-click-overlay {
  bottom: 36px
}
.av-video-stretch.av-video-service-youtube .av-click-overlay {
  bottom: 0
}
.avia-slideshow-inner li {
  -webkit-transform-style: preserve-3d
}
.avia-fullwidth-slider {
  border-top-style: solid;
  border-top-width: 1px
}
.avia-fullwidth-slider.avia-builder-el-0 {
  border-top: none
}
.avia-slide-wrap {
  position: relative;
  display: block
}
.av_slideshow_full.avia-slideshow {
  margin: 0
}
#top .av_slideshow_full .avia-slideshow-arrows a:hover {
  opacity: 1
}
.av_slideshow_full li amp-img {
  border-radius: 0
}
.caption_fullwidth {
  position: absolute;
  right: 0;
  bottom: 0;
  left: 0;
  top: 0;
  z-index: 10
}
.slideshow_caption {
  z-index: 100;
  width: 42%;
  position: absolute;
  bottom: 0;
  top: auto;
  height: 100%;
  display: block;
  text-decoration: none;
  padding: 50px
}
div .slideshow_caption h2 {
  text-transform: uppercase
}
div .slideshow_caption,
div .slideshow_caption a,
div .slideshow_caption a:hover,
div .slideshow_caption h2 {
  color: #fff
}
.slideshow_inner_caption {
  position: relative;
  display: table;
  height: 100%;
  width: 100%
}
.slideshow_align_caption {
  display: table-cell;
  vertical-align: middle;
  position: relative
}
.av_slideshow_full .container.caption_container {
  position: relative;
  top: 0;
  z-index: 5;
  height: 100%;
  left: 0;
  overflow: visible
}
#top div .caption_center .slideshow_caption {
  left: 0;
  width: 100%;
  text-align: center
}
.avia-caption-content {
  line-height: 1.3em
}
.caption_framed .slideshow_caption .avia-caption-content p,
.caption_framed .slideshow_caption .avia-caption-title {
  background: #000;
  background: rgba(0,0,0,.5);
  display: inline-block;
  margin: 0 0 1px 0;
  padding: 10px 15px
}
#top .avia-slideshow-button {
  border-radius: 3px;
  text-transform: uppercase;
  padding: 15px 16px;
  display: inline-block;
  margin-top: 20px;
  text-decoration: none;
  font-weight: 700
}
.av-layout-grid-container {
  width: 100%;
  table-layout: fixed;
  display: table;
  border-top-stye: none;
  border-top-width: 0
}
.flex_cell {
  display: table-cell;
  padding: 30px;
  vertical-align: top
}
.flex_cell_inner {
  display: block;
  position: relative
}
#top .av-rotator-container {
  position: relative
}
#top .av-rotator-container-inner {
  position: relative;
  line-height: 1.1em;
  margin: 0;
  font-weight: 300;
  text-transform: none;
  color: inherit;
  font-size: inherit
}
.av-rotator-text,
.av-rotator-text-single {
  position: relative;
  display: inline
}
.av-rotator-text-single {
  display: none;
  top: 0
}
.av-rotator-text-single:first-child {
  display: inline
}
button::-moz-focus-inner {
  padding: 0;
  border: 0
}
.mejs-mediaelement {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%
}
div .mejs-controls .mejs-loop-on::hover button {
  background-position: -64px -32px
}
::-moz-selection {
  background-color: #4ecac2;
  color: #fff
}
body {
  font-size: 15px
}
.socket_color ::-webkit-input-placeholder {
  color: #555
}
.socket_color ::-moz-placeholder {
  color: #555;
  opacity: 1
}
.socket_color :-ms-input-placeholder {
  color: #555
}
#scroll-top-link,
html {
  background-color: #333
}
#scroll-top-link {
  color: #eee;
  border: 1px solid #444
}
.footer_color,
.footer_color a,
.footer_color div,
.footer_color form,
.footer_color h3,
.footer_color li,
.footer_color p,
.footer_color section,
.footer_color span,
.footer_color ul {
  border-color: #444
}
.footer_color {
  background-color: #222;
  color: #ddd
}
.footer_color h3 {
  color: #555
}
.footer_color a {
  color: #fff
}
.footer_color a:hover {
  color: #aaa
}
.footer_color input[type=submit],
div .footer_color .button {
  background-color: #fff;
  color: #222;
  border-color: #ddd
}
.footer_color .button:hover,
.footer_color input[type=submit]:hover {
  background-color: #aaa;
  color: #222;
  border-color: #888
}
.footer_color .widget_nav_menu ul:first-child > .current-menu-item,
.footer_color .widget_nav_menu ul:first-child > .current_page_item {
  background: #333
}
.footer_color ::-webkit-input-placeholder {
  color: #555
}
.footer_color ::-moz-placeholder {
  color: #555;
  opacity: 1
}
.footer_color :-ms-input-placeholder {
  color: #555
}
#top .footer_color input[type=email] {
  border-color: #444;
  background-color: #333;
  color: #555
}
.footer_color .required {
  color: #fff
}
.alternate_color ::-webkit-input-placeholder {
  color: #ababab
}
.alternate_color ::-moz-placeholder {
  color: #ababab;
  opacity: 1
}
.alternate_color :-ms-input-placeholder {
  color: #ababab
}
.main_color {
  background-color: #fff;
  color: #9d9d9d
}
.main_color h1,
.main_color h2,
.main_color h3 {
  color: #222
}
.main_color .special-heading-inner-border {
  border-color: #9d9d9d
}
.main_color a,
.main_color b,
.main_color strong {
  color: #4ecac2
}
.main_color a:hover {
  color: #38b5ad
}
.main_color ::-webkit-input-placeholder {
  color: #ababab
}
.main_color ::-moz-placeholder {
  color: #ababab;
  opacity: 1
}
.main_color :-ms-input-placeholder {
  color: #ababab
}
#main {
  border-color: #e1e1e1
}
#scroll-top-link:hover {
  background-color: #fcfcfc;
  color: #4ecac2;
  border: 1px solid #e1e1e1
}
.header_color {
  background-color: #fff;
  color: #121212
}
.header_color a,
.header_color strong,
.header_color strong a {
  color: #121212
}
.header_color a:hover {
  color: #7a7a7a
}
.header_color .avia-menu-fx,
.header_color .avia-menu-fx .avia-arrow {
  background-color: #121212;
  color: #fff;
  border-color: #000
}
.header_color .avia-bullet {
  border-color: #121212
}
.header_color ::-webkit-input-placeholder {
  color: #7a7a7a
}
.header_color ::-moz-placeholder {
  color: #7a7a7a;
  opacity: 1
}
.header_color :-ms-input-placeholder {
  color: #7a7a7a
}
.header_color .header_bg {
  background-color: #fff;
  color: #7a7a7a
}
.header_color .main_menu ul:first-child > li > a {
  color: #7a7a7a
}
.header_color .main_menu ul:first-child > li a:hover,
.header_color .main_menu ul:first-child > li.current-menu-item > a,
.header_color .main_menu ul:first-child > li.current_page_item > a {
  color: #121212
}
#main {
  background-color: #fff
}
#advanced_menu_hide,
#advanced_menu_toggle {
  background-color: #fff;
  color: #121212;
  border-color: #fff
}
.header_color .avia-arrow {
  background-color: #fff
}
.header_color .avia-bullet {
  border-color: #7a7a7a
}
#top .header_color a:hover .avia-bullet {
  border-color: #7a7a7a
}
h1,
h2,
h3 {
  font-family: Bitter,HelveticaNeue,'Helvetica Neue',Helvetica,Arial,sans-serif
}
body {
  font-family: 'Open Sans',HelveticaNeue,'Helvetica Neue',Helvetica,Arial,sans-serif
}
.container {
  width: 100%
}
.logo {
  padding-top: 10px
}
.template-page .entry-content-wrapper h1 {
  text-transform: none
}
amp-img {
  max-width: 2000px
}
h3 {
  line-height: 1.5em
}
#full_slider_1 {
  display: none
}
@font-face {
  font-family: entypo-fontello;
  font-weight: 400;
  font-style: normal;
  src: url(https://matter.vc/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.eot?v=3);
  src: url(https://matter.vc/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.eot?v=3#iefix) format('embedded-opentype'),url(https://matter.vc/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.woff?v=3) format('woff'),url(https://matter.vc/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.ttf?v=3) format('truetype'),url(https://matter.vc/wp-content/themes/enfold/config-templatebuilder/avia-template-builder/assets/fonts/entypo-fontello.svg?v=3#entypo-fontello) format('svg')
}
html body [data-av_iconfont=entypo-fontello]:before {
  font-family: entypo-fontello
}
.rbt-inline-0 {
  padding-bottom: 28.6666666667%
}
.rbt-inline-1 {
  font-size: 42px
}
.rbt-inline-2 {
  font-size: 17px
}
.rbt-inline-7 {
  font-size: 32px
}
.rbt-inline-8 {
  font-size: 15px
}
.rbt-inline-9 {
  padding-bottom: 20px
}
.rbt-inline-10 {
  height: 1px;
  margin-top: -40px
}
.rbt-inline-11 {
  vertical-align: top;
  padding: 30px
}
.rbt-inline-12 {
  opacity: .5;
  background-color: #000
}
.rbt-inline-13 {
  color: #fff;
  font-size: 23px
}
.rbt-inline-18 {
  opacity: .4;
  background-color: #000
}
.rbt-inline-24 {
  height: 30px
}
.rbt-inline-25 {
  font-size: 14px;
  color: #141414
}
.rbt-inline-26 {
  height: 25px
}
.rbt-inline-27 {
  font-size: 14px;
  color: #0a0a0a
}
.rbt-inline-30 {
  height: 1px;
  margin-top: -30px
}
.rbt-inline-31 {
  font-size: 30px;
  color: #0a0a0a
}
.rbt-inline-32 {
  text-align: center
}
.rbt-inline-33 {
  height: 1px;
  margin-top: -50px
}
.rbt-inline-36 {
  height: 20px
}
.rbt-inline-38 {
  height: 100px
}
.rbt-inline-39 {
  text-align: left;
  font-size: 30px
}
.rbt-inline-40 {
  height: 50px
}
.rbt-inline-43 {
  height: 15px
}
.rbt-inline-47 {
  height: 10px
}
#rbt-sidebar {
  width: 80%;
  background: #eee
}
#rbt-sidebar ul li {
  display: block
}
#rbt-sidebar-openner {
  font-size: 22px;
  line-height: 34px;
  color: #000;
  text-align: center;
  background: #fff;
  border: 1px solid #000;
  border-radius: 6px;
  position: absolute;
  top: 10px;
  right: 10px;
  display: block;
  width: 36px;
  height: 36px;
  overflow: hidden;
  cursor: pointer;
  z-index: 10001
}
#rbt-sidebar > nav {
  position: static
}
#rbt-sidebar > nav > div {
  display: block
}
section.rbt-accordion-section > header {
  background-color: transparent;
  padding: 0;
  margin: 0;
  border: 0
}</style>
    </head>
    <body id="top" class="home page-template-default page page-id-27850 stretched bitter open_sans " itemscope="itemscope" itemtype="https://schema.org/WebPage">
        <amp-sidebar id="rbt-sidebar" layout="nodisplay">
            <nav class="main_menu" data-selectname="Select a page" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
                <div class="avia-menu av-main-nav-wrap">
                    <ul id="avia-menu" class="menu av-main-nav">
                        <li id="menu-item-27955" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-top-level menu-item-top-level-1"><a href="https://matter.vc/"><span class="avia-bullet"></span><span class="avia-menu-text">Home</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                        <li id="menu-item-19089" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-2"><a href="https://matter.vc/program/"><span class="avia-bullet"></span><span class="avia-menu-text">Program</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                        <li id="menu-item-1359" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-3"><a href="https://matter.vc/mission/"><span class="avia-bullet"></span><span class="avia-menu-text">Mission</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                        <li id="menu-item-1361" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-4"><a href="https://matter.vc/community/"><span class="avia-bullet"></span><span class="avia-menu-text">Community</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                        <li id="menu-item-29047" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-top-level menu-item-top-level-5"><a href="https://medium.com/matter-driven-narrative"><span class="avia-bullet"></span><span class="avia-menu-text">News</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                        <li id="menu-item-24795" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-6"><a href="https://matter.vc/jobs/"><span class="avia-bullet"></span><span class="avia-menu-text">Jobs</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                        <li id="menu-item-30223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-7"><a href="https://matter.vc/apply/"><span class="avia-bullet"></span><span class="avia-menu-text">Apply</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
                    </ul>
                </div>
            </nav>
        </amp-sidebar>
        <amp-analytics type="googleanalytics" id="rbt-ga-1">
            <script type="application/json">
                {
                    "vars": {
                        "account": "UA-72097030-1"
                    },
                    "triggers": {
                        "trackPageview": {
                            "on": "visible",
                            "request": "pageview"
                        }
                    }
                }
            </script>
        </amp-analytics>

        <div id="wrap_all">

            <header id="header" class=" header_color light_bg_color  av_header_top av_logo_left av_main_nav_header av_menu_right av_slim av_header_sticky av_header_shrinking_disabled av_header_stretch_disabled av_mobile_menu_tablet av_header_searchicon_disabled av_header_unstick_top_disabled av_bottom_nav_disabled  av_alternate_logo_active av_header_border_disabled" role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader"><span id="rbt-sidebar-openner" on="tap:rbt-sidebar.toggle" role="button" tabindex="-1">☰</span>

                <a id="advanced_menu_toggle" href="#" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello"></a><a id="advanced_menu_hide" href="#" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello"></a>

                <div id="header_main" class="container_wrap container_wrap_logo">

                    <div class="container">

                        <div class="inner-container">
                            <strong class="logo"><a href="https://matter.vc/"><amp-img src="https://matter.vc/wp-content/uploads/2016/04/small-matter-logo-for-website.jpg" alt="Matter." width="300" height="100" layout="fixed"></amp-img></a></strong>
                            <!--{/* @notice */}-->
                            <!--{/* The following code was moved to amp-sidebar. */}-->
                            <!-- <nav class="main_menu" data-selectname="Select a page" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement"><div class="avia-menu av-main-nav-wrap"><ul id="avia-menu" class="menu av-main-nav"><li id="menu-item-27955" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-top-level menu-item-top-level-1"><a href="https://matter.vc/"><span class="avia-bullet"></span><span class="avia-menu-text">Home</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li id="menu-item-19089" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-2"><a href="https://matter.vc/program/"><span class="avia-bullet"></span><span class="avia-menu-text">Program</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li id="menu-item-1359" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-3"><a href="https://matter.vc/mission/"><span class="avia-bullet"></span><span class="avia-menu-text">Mission</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li id="menu-item-1361" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-4"><a href="https://matter.vc/community/"><span class="avia-bullet"></span><span class="avia-menu-text">Community</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li id="menu-item-29047" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-top-level menu-item-top-level-5"><a href="https://medium.com/matter-driven-narrative"><span class="avia-bullet"></span><span class="avia-menu-text">News</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li id="menu-item-24795" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-6"><a href="https://matter.vc/jobs/"><span class="avia-bullet"></span><span class="avia-menu-text">Jobs</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
<li id="menu-item-30223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-top-level menu-item-top-level-7"><a href="https://matter.vc/apply/"><span class="avia-bullet"></span><span class="avia-menu-text">Apply</span><span class="avia-menu-fx"><span class="avia-arrow-wrap"><span class="avia-arrow"></span></span></span></a></li>
</ul></div></nav> -->

                        </div>

                    </div>

                </div>

                <div class="header_bg"></div>

            </header>

            <div id="main" data-scroll-offset="88">

                <div id="full_slider_1" class="avia-fullwidth-slider main_color avia-shadow  avia-builder-el-0  el_before_av_heading  avia-builder-el-first  container_wrap fullsize">
                    <div data-size="featured" data-lightbox_size="large" data-animation="slide" data-ids="29268,27476,20375," data-video_counter="1" data-autoplay="true" data-bg_slider="false" data-slide_height="" data-handle="av_slideshow_full" data-interval="10" data-class=" " data-css_id="" data-scroll_down="" data-control_layout="av-control-hidden" data-custom_markup="" data-perma_caption="" data-autoplay_stopper="" data-src="" data-position="top left" data-repeat="no-repeat" data-attach="scroll" data-stretch="" data-default-height="28.6666666667" class="avia-slideshow avia-slideshow-1  av-control-hidden av-default-height-applied avia-slideshow-featured av_slideshow_full   avia-slide-slider " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                        <ul class="avia-slideshow-inner rbt-inline-0">
                            <li class=" slide-1 ">
                                <div data-rel="slideshow-1" class="avia-slide-wrap">
                                    <div class="caption_fullwidth av-slideshow-caption caption_center caption_center_framed caption_framed">
                                        <div class="container caption_container">
                                            <div class="slideshow_caption">
                                                <div class="slideshow_inner_caption">
                                                    <div class="slideshow_align_caption">
                                                        <h2 class="avia-caption-title rbt-inline-1" itemprop="name">IT’S YOUR TURN TO MATTER.</h2>
                                                        <div class="avia-caption-content av_inherit_color rbt-inline-2" itemprop="description">
                                                            <p>We&#8217;re looking for scrappy entrepreneurs who are building the media platforms of the future.<br> We&#8217;ll support them with investment and a five-month in-person accelerator program.<br> Applications for Matter Nine are open now.</p>
                                                        </div><a href="https://matter.vc/apply/" class="avia-slideshow-button avia-button avia-color-red " data-duration="800" data-easing="easeInOutQuad">APPLY</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <amp-img src="https://matter.vc/wp-content/uploads/2017/02/M6-BC-Day1-3-1500x430.jpg" alt="" width="1500" height="430" layout="responsive"></amp-img>
                                </div>
                            </li>
                            <li class=" slide-2 ">
                                <div data-rel="slideshow-1" class="avia-slide-wrap">
                                    <div class="caption_fullwidth av-slideshow-caption caption_center caption_center_framed caption_framed">
                                        <div class="container caption_container">
                                            <div class="slideshow_caption">
                                                <div class="slideshow_inner_caption">
                                                    <div class="slideshow_align_caption">
                                                        <h2 class="avia-caption-title rbt-inline-1" itemprop="name">Local news bootcamps in four regions</h2>
                                                        <div class="avia-caption-content av_inherit_color rbt-inline-2" itemprop="description">
                                                            <p>We&#8217;re launching local news bootcamps in partnership with<br> Google News Lab, News Media Alliance, and 4 top journalism schools.</p>
                                                        </div><a href="https://medium.com/matter-driven-narrative/a-lot-more-experiments-open-matter-e3088db96fb" target="_blank" class="avia-slideshow-button avia-button avia-color-red " data-duration="800" data-easing="easeInOutQuad">Apply to Open Matter</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <amp-img src="https://matter.vc/wp-content/uploads/2016/03/Screenshot-2016-12-16-10.54.33-1500x430.png" alt="" width="1500" height="430" layout="responsive"></amp-img>
                                </div>
                            </li>
                            <li class=" slide-3 ">
                                <div data-rel="slideshow-1" class="avia-slide-wrap">
                                    <div class="caption_fullwidth av-slideshow-caption caption_center caption_center_framed caption_framed">
                                        <div class="container caption_container">
                                            <div class="slideshow_caption">
                                                <div class="slideshow_inner_caption">
                                                    <div class="slideshow_align_caption">
                                                        <h2 class="avia-caption-title rbt-inline-1" itemprop="name">CREATING THE FUTURE OF MEDIA</h2>
                                                        <div class="avia-caption-content av_inherit_color rbt-inline-2" itemprop="description">
                                                            <p>We support media entrepreneurs building a more informed, connected, and empowered society<br> through our startup accelerator based in San Francisco and New York City.</p>
                                                        </div><a href="http://matter.vc/program" class="avia-slideshow-button avia-button avia-color-red " data-duration="800" data-easing="easeInOutQuad">LEARN MORE</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <amp-img src="https://matter.vc/wp-content/uploads/2016/02/Corey-Ford-candid-photo-2-1500x430.jpg" alt="" width="1500" height="430" layout="responsive"></amp-img>
                                </div>
                            </li>
                            <li data-controls="aviaTBaviaTBvideo_controls" data-mute="" data-loop="aviaTBaviaTBvideo_loop" data-disable-autoplay="" data-mobile-img="https://matter.vc/wp-content/uploads/2016/02/Cb1l5caWAAAiE_Q-800x430.jpg" data-video-height="197" data-video-toppos="-48.5" data-video-ratio="3.48837209302" class=" av-video-slide  av-video-service-youtube  av-hide-video-controls av-loop-video av-mobile-fallback-image av-video-stretch slide-4 ">
                                <div data-rel="slideshow-1" class="avia-slide-wrap">
                                    <div class="caption_fullwidth av-slideshow-caption caption_center caption_center_framed caption_framed">
                                        <div class="container caption_container">
                                            <div class="slideshow_caption">
                                                <div class="slideshow_inner_caption">
                                                    <div class="slideshow_align_caption">
                                                        <h2 class="avia-caption-title rbt-inline-7" itemprop="name">MEDIA + DESIGN THINKING + ENTREPRENEURSHIP</h2>
                                                        <div class="avia-caption-content av_inherit_color rbt-inline-8" itemprop="description">
                                                            <p>Our 20-week program focuses on building scalable media ventures<br> with a human-centered, prototype-driven design process.</p>
                                                        </div><a href="http://matter.vc/program" class="avia-slideshow-button avia-button avia-color-blue " data-duration="800" data-easing="easeInOutQuad">LEARN MORE</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="av-click-overlay"></div>
                                    <div class="mejs-mediaelement">
                                        <div class="av_youtube_frame" id="player_27850_735523050_1698096918" data-autoplay="1" data-videoid="ZdRlKOObMQw" data-hd="1" data-rel="0" data-wmode="opaque" data-playlist="player_27850_735523050_1698096918" data-loop="1" data-version="3" data-autohide="1" data-color="white" data-controls="0" data-showinfo="0" data-iv_load_policy="3"></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="avia-slideshow-arrows avia-slideshow-controls"><a href="#prev" class="prev-slide" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello">Previous</a><a href="#next" class="next-slide" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello">Next</a></div>
                        <div class="avia-slideshow-dots avia-slideshow-controls"><a href="#1" class="goto-slide active">1</a><a href="#2" class="goto-slide ">2</a><a href="#3" class="goto-slide ">3</a><a href="#4" class="goto-slide ">4</a></div>
                    </div>
                </div>
                <div id="after_full_slider_1" class="main_color container_wrap fullsize">
                    <div class="container">
                        <div class="template-page content  av-content-full alpha units">
                            <div class="post-entry post-entry-type-page post-entry-27850">
                                <div class="entry-content-wrapper clearfix">
                                    <div class="av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-1  el_after_av_slideshow_full  el_before_av_hr  avia-builder-el-first   rbt-inline-9" data-rbt-accordion-index="0" data-rbt-accordion-section-index="0">
                                        <h1 class="av-special-heading-tag" itemprop="headline">The Latest News</h1>
                                        <div class="special-heading-border">
                                            <div class="special-heading-inner-border"></div>
                                        </div>
                                    </div>
                                    <div class="hr hr-invisible  avia-builder-el-2  el_after_av_heading  el_before_av_layout_row  avia-builder-el-last  rbt-inline-10"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="av-layout-grid-1" class="av-layout-grid-container main_color av-flex-cells  avia-builder-el-3  el_after_av_hr  el_before_av_heading  submenu-not-first container_wrap fullsize">
                    <div class="flex_cell no_margin av_one_fourth  avia-builder-el-4  el_before_av_cell_one_fourth  avia-builder-el-first    rbt-inline-11">
                        <div class="flex_cell_inner">
                            <div class="avia-image-container  av-styling-  noHover  av-overlay-hover-deactivate  avia-builder-el-5  avia-builder-el-no-sibling  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                <div class="avia-image-container-inner">
                                    <a href="https://medium.com/@benwerd/build-the-media-platform-of-tomorrow-49e00e246d20" class="avia_image" target="_blank">
                                        <div class="av-image-caption-overlay">
                                            <div class="av-caption-image-overlay-bg rbt-inline-12"></div>
                                            <div class="av-image-caption-overlay-position">
                                                <div class="av-image-caption-overlay-center rbt-inline-13">
                                                    <p>Apply to Matter Nine</p>
                                                </div>
                                            </div>
                                        </div>
                                        <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2018/03/Image-uploaded-from-iOS-17-300x200.jpg" alt="" width="2000" height="1333.3" layout="fixed"></amp-img>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex_cell no_margin av_one_fourth  avia-builder-el-6  el_after_av_cell_one_fourth  el_before_av_cell_one_fourth    rbt-inline-11">
                        <div class="flex_cell_inner">
                            <div class="avia-image-container  av-styling- av-hover-grow noHover  av-overlay-hover-deactivate  avia-builder-el-7  avia-builder-el-no-sibling  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                <div class="avia-image-container-inner">
                                    <a href="https://medium.com/matter-driven-narrative/a-lot-more-experiments-open-matter-e3088db96fb" class="avia_image" target="_blank">
                                        <div class="av-image-caption-overlay">
                                            <div class="av-caption-image-overlay-bg rbt-inline-12"></div>
                                            <div class="av-image-caption-overlay-position">
                                                <div class="av-image-caption-overlay-center rbt-inline-13">
                                                    <p>Open Matter: A Lot More Experiments</p>
                                                </div>
                                            </div>
                                        </div>
                                        <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2018/03/Matter1-300x200.jpeg" alt="" width="2000" height="1333.3" layout="fixed"></amp-img>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex_cell no_margin av_one_fourth  avia-builder-el-8  el_after_av_cell_one_fourth  el_before_av_cell_one_fourth    rbt-inline-11">
                        <div class="flex_cell_inner">
                            <div class="avia-image-container  av-styling-  noHover  av-overlay-hover-deactivate  avia-builder-el-9  avia-builder-el-no-sibling  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                <div class="avia-image-container-inner">
                                    <a href="https://medium.com/matter-driven-narrative/meet-matter-eight-f3910567407e" class="avia_image">
                                        <div class="av-image-caption-overlay">
                                            <div class="av-caption-image-overlay-bg rbt-inline-18"></div>
                                            <div class="av-image-caption-overlay-position">
                                                <div class="av-image-caption-overlay-center rbt-inline-13">
                                                    <p>Meet Matter Eight!</p>
                                                </div>
                                            </div>
                                        </div>
                                        <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2018/03/1nYQue0dzCGtrudm2jAFjMg-300x200.jpeg" alt="" width="2000" height="1333.3" layout="fixed"></amp-img>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex_cell no_margin av_one_fourth  avia-builder-el-10  el_after_av_cell_one_fourth  avia-builder-el-last    rbt-inline-11">
                        <div class="flex_cell_inner">
                            <div class="avia-image-container  av-styling- av-hover-grow noHover  av-overlay-hover-deactivate  avia-builder-el-11  avia-builder-el-no-sibling  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                <div class="avia-image-container-inner">
                                    <a href="https://medium.com/matter-driven-narrative/matter-startup-huzza-is-acquired-by-kickstarter-841c9b11d0bf#.r4bpz4kvo" class="avia_image" target="_blank">
                                        <div class="av-image-caption-overlay">
                                            <div class="av-caption-image-overlay-bg rbt-inline-18"></div>
                                            <div class="av-image-caption-overlay-position">
                                                <div class="av-image-caption-overlay-center rbt-inline-13">
                                                    <p>Matter startup Huzza is acquired by Kickstarter</p>
                                                </div>
                                            </div>
                                        </div>
                                        <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2016/02/huzza-laughing-crop-300x200.jpg" alt="" width="2000" height="1333.3" layout="fixed"></amp-img>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="after_submenu" class="main_color container_wrap fullsize">
                    <div class="container">
                        <div class="template-page content  av-content-full alpha units">
                            <div class="post-entry post-entry-type-page post-entry-27850">
                                <div class="entry-content-wrapper clearfix">
                                    <div class="av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-12  el_after_av_layout_row  el_before_av_hr  avia-builder-el-first   rbt-inline-9" data-rbt-accordion-index="1" data-rbt-accordion-section-index="0">
                                        <h1 class="av-special-heading-tag" itemprop="headline">Our Entrepreneurs</h1>
                                        <div class="special-heading-border">
                                            <div class="special-heading-inner-border"></div>
                                        </div>
                                    </div>
                                    <p></p>
                                    <div class="hr hr-invisible  avia-builder-el-13  el_after_av_heading  el_before_av_one_fifth  rbt-inline-24"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                    </div>
                                    <div class="flex_column av_one_fifth first  avia-builder-el-14  el_after_av_hr  el_before_av_one_fourth  ">
                                        <div class="avia-image-container  av-styling-   avia-builder-el-15  el_before_av_image  avia-builder-el-first  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                            <div class="avia-image-container-inner">
                                                <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2016/02/ceci-crop-3-300x219.jpg" alt="" width="2000" height="1460" layout="responsive"></amp-img>
                                            </div>
                                        </div>
                                        <div class="avia-image-container  av-styling-   avia-builder-el-16  el_after_av_image  el_before_av_image  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                            <div class="avia-image-container-inner">
                                                <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2016/02/nick-crop1-300x200.jpg" alt="" width="2000" height="1333.3" layout="responsive"></amp-img>
                                            </div>
                                        </div>
                                        <div class="avia-image-container  av-styling-   avia-builder-el-17  el_after_av_image  avia-builder-el-last  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                            <div class="avia-image-container-inner">
                                                <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2017/02/nadya_matterfrontpage_colorgraded-300x190.jpg" alt="" width="2000" height="1266.7" layout="responsive"></amp-img>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex_column av_one_fourth   avia-builder-el-18  el_after_av_one_fifth  el_before_av_one_half  ">
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-25" itemprop="text">
                                                <p>&#8220;I&#8217;ve learned such a huge amount in terms of how to build a team, iterate, and fail forward.&#8221;<br>
                                                    <strong>Ceci Mourkogiannis, CEO, </strong><strong>Pie</strong></p>
                                            </div>
                                        </section>
                                        <div class="hr hr-invisible  avia-builder-el-20  el_after_av_textblock  el_before_av_textblock  rbt-inline-26"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-27" itemprop="text">
                                                <p>&#8220;I feel like I&#8217;m armed with the tools to take on the future of our business.&#8221;<br>
                                                    <strong>Nick Smit, Co-Founder, </strong><strong>Huzza</strong></p>
                                            </div>
                                        </section>
                                        <div class="hr hr-invisible  avia-builder-el-22  el_after_av_textblock  el_before_av_textblock  rbt-inline-26"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-27" itemprop="text">
                                                <p>&#8220;The greatest gift Matter has given us is a rigorous adherence to design thinking. It taught us to embrace failures as learning opportunities.&#8221;<br>
                                                    <strong>Nadya Lev, CEO, </strong><b>Aconite</b></p>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="flex_column av_one_half   avia-builder-el-24  el_after_av_one_fourth  el_before_av_hr  ">
                                        <div class="avia-video avia-video-16-9  " itemprop="video" itemtype="https://schema.org/VideoObject">
                                            <div class="avia-iframe-wrap">
                                                <amp-vimeo layout="responsive" width="1280" height="720" data-videoid="188045969"></amp-vimeo>
                                            </div>
                                        </div>
                                        <div class="hr hr-invisible  avia-builder-el-26  el_after_av_video  el_before_av_textblock  rbt-inline-30"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-31" itemprop="text">
                                                <p class="rbt-inline-32">
                                                </p>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="hr hr-invisible  avia-builder-el-28  el_after_av_one_half  el_before_av_heading  rbt-inline-33"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                    </div>
                                    <amp-accordion>
                                        <section class="av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-29  el_after_av_hr  el_before_av_textblock   rbt-inline-9">
                                            <!-- <div style="padding-bottom:20px;" class="av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-29  el_after_av_hr  el_before_av_textblock   rbt-inline-9"> -->
                                            <header>
                                                <h1 class="av-special-heading-tag" itemprop="headline">2 Locations. 1 Program.</h1>
                                            </header>
                                            <div class="special-heading-border">
                                                <div class="special-heading-inner-border"></div>
                                            </div>
                                            <!-- </div> -->
                                        </section>
                                    </amp-accordion>
                                    <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                        <div class="avia_textblock  av_inherit_color rbt-inline-27" itemprop="text">
                                            <p>Our interwoven program exposes both our San Francisco-based and New York-based teams to the Innovation Capital of the World and the Media Capital of the World. Throughout the 20 weeks, we create a structured and supportive environment to accelerate their path to product-market fit.</p>
                                        </div>
                                    </section>
                                    <div class="hr hr-invisible  avia-builder-el-31  el_after_av_textblock  el_before_av_image  rbt-inline-36"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                    </div>
                                    <div class="avia-image-container  av-styling-   avia-builder-el-32  el_after_av_hr  el_before_av_hr  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                        <div class="avia-image-container-inner">
                                            <a href="http://matter.vc/program/" class="avia_image" target="_blank">
                                                <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2018/03/Screen-Shot-2018-03-13-at-5.23.27-PM.png" alt="" width="2000" height="668.1" layout="responsive"></amp-img>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="hr hr-invisible  avia-builder-el-33  el_after_av_image  el_before_av_one_half  rbt-inline-24"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                    </div>
                                    <div class="flex_column av_one_half first  avia-builder-el-34  el_after_av_hr  el_before_av_two_fifth  ">
                                        <div class="avia-image-container  av-styling-no-styling   avia-builder-el-35  avia-builder-el-no-sibling  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                            <div class="avia-image-container-inner">
                                                <a href="http://matter.vc/program" class="avia_image">
                                                    <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2016/02/Screenshot-2016-03-01-16.45.56.png" alt="" width="2000" height="1396.4" layout="responsive"></amp-img>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex_column av_two_fifth   avia-builder-el-36  el_after_av_one_half  el_before_av_hr  ">
                                        <div class="hr hr-invisible  avia-builder-el-37  el_before_av_headline_rotator  avia-builder-el-first  rbt-inline-38"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <div class="av-rotator-container avia-builder-el-38  el_after_av_hr  el_before_av_hr  rbt-inline-39" data-interval="6" data-animation="1">
                                            <h3 class="av-rotator-container-inner">Matter is<span class="av-rotator-text av-rotator-multiline-off">
<span class="av-rotator-text-single av-rotator-text-single-1">design thinking as applied to early-stage entrepreneurship as applied to the future of media that matters.</span>
                                                <span class="av-rotator-text-single av-rotator-text-single-2">a 20-week accelerator program based on human-centered, prototype-driven design.</span>
                                                <span class="av-rotator-text-single av-rotator-text-single-3">a community of more than 100 entrepreneurs, 49 portfolio companies, 12 partner organizations, and 150+ mentors.</span>
                                                <span class="av-rotator-text-single av-rotator-text-single-4">home to a collaborative culture of experimentation designed to help you fail quickly to succeed sooner.</span>
                                                <span class="av-rotator-text-single av-rotator-text-single-5">your chance to receive $50K in investment, a space in San Francisco or New York City, and a tight-knit cohort of fellow entrepreneurs.</span>
                                                </span>
                                            </h3>
                                        </div>
                                        <div class="hr hr-invisible  avia-builder-el-39  el_after_av_headline_rotator  avia-builder-el-last  rbt-inline-40"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                    </div>
                                    <div class="hr hr-invisible  avia-builder-el-40  el_after_av_two_fifth  el_before_av_heading  rbt-inline-40"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                    </div>
                                    <amp-accordion>
                                        <section class="av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-41  el_after_av_hr  el_before_av_one_fifth   rbt-inline-9">
                                            <!-- <div style="padding-bottom:20px;" class="av-special-heading av-special-heading-h1  blockquote modern-quote modern-centered  avia-builder-el-41  el_after_av_hr  el_before_av_one_fifth   rbt-inline-9"> -->
                                            <header>
                                                <h1 class="av-special-heading-tag" itemprop="headline">Our Partners</h1>
                                            </header>
                                            <div class="special-heading-border">
                                                <div class="special-heading-inner-border"></div>
                                            </div>
                                            <!-- </div> -->
                                        </section>
                                    </amp-accordion>
                                    <div class="flex_column av_one_fifth first  avia-builder-el-42  el_after_av_heading  el_before_av_one_fourth  ">
                                        <div class="hr hr-invisible  avia-builder-el-43  el_before_av_image  avia-builder-el-first  rbt-inline-43"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <div class="avia-image-container  av-styling-   avia-builder-el-44  el_after_av_hr  el_before_av_hr  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                            <div class="avia-image-container-inner">
                                                <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2016/02/Tim-Olson1.jpg" alt="" width="2000" height="2000" layout="fixed"></amp-img>
                                            </div>
                                        </div>
                                        <div class="hr hr-invisible  avia-builder-el-45  el_after_av_image  el_before_av_image  rbt-inline-43"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <div class="avia-image-container  av-styling-   avia-builder-el-46  el_after_av_hr  el_before_av_hr  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                            <div class="avia-image-container-inner">
                                                <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2016/02/Jim-Kennedy-smaller.jpg" alt="" width="2000" height="2000" layout="fixed"></amp-img>
                                            </div>
                                        </div>
                                        <div class="hr hr-invisible  avia-builder-el-47  el_after_av_image  el_before_av_image  rbt-inline-43"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <div class="avia-image-container  av-styling-   avia-builder-el-48  el_after_av_hr  avia-builder-el-last  avia-align-center " itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                            <div class="avia-image-container-inner">
                                                <amp-img class="avia_image " src="https://matter.vc/wp-content/uploads/2016/02/the-history-project_niles.jpg" alt="" width="2000" height="2000" layout="fixed"></amp-img>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex_column av_one_fourth   avia-builder-el-49  el_after_av_one_fifth  el_before_av_one_half  ">
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-27" itemprop="text">
                                                <p>&#8220;Matter is a big part of KQED&#8217;s “outside in” innovation strategy. We leverage Matter’s design thinking and connections to entrepreneurs.&#8221;<br>
                                                    <strong>Tim Olson, VP Digital &#038; Education</strong><br>
                                                    <strong>KQED</strong></p>
                                            </div>
                                        </section>
                                        <div class="hr hr-invisible  avia-builder-el-51  el_after_av_textblock  el_before_av_textblock  rbt-inline-47"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-27" itemprop="text">
                                                <p>&#8220;Matter is a source of innovation for our company and a training camp for our people, all rolled into one.&#8221;<br>
                                                    <strong>Jim Kennedy, SVP Strategic Planning</strong><br>
                                                    <strong>AP</strong></p>
                                            </div>
                                        </section>
                                        <div class="hr hr-invisible  avia-builder-el-53  el_after_av_textblock  el_before_av_textblock  rbt-inline-47"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-27" itemprop="text">
                                                <p>&#8220;When Matter connected us to its media partners, we got an incredible launchpad to help accelerate our thinking, test our products in market, and gain access to investment.&#8221;<br>
                                                    <strong>Niles Lichtenstein, CEO</strong><br>
                                                    <strong>The History Project</strong></p>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="flex_column av_one_half   avia-builder-el-55  el_after_av_one_fourth  avia-builder-el-last  ">
                                        <div class="avia-video avia-video-16-9  " itemprop="video" itemtype="https://schema.org/VideoObject">
                                            <div class="avia-iframe-wrap">
                                                <amp-vimeo layout="responsive" width="1280" height="720" data-videoid="188360471"></amp-vimeo>
                                            </div>
                                        </div>
                                        <div class="hr hr-invisible  avia-builder-el-57  el_after_av_video  el_before_av_textblock  rbt-inline-30"><span class="hr-inner "><span class="hr-inner-style"></span></span>
                                        </div>
                                        <section class="av_textblock_section" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                                            <div class="avia_textblock  av_inherit_color rbt-inline-31" itemprop="text">
                                                <p class="rbt-inline-32"><strong> </strong></p>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container_wrap footer_color" id="footer">

                    <div class="container">

                        <div class="flex_column av_one_fourth  first el_before_av_one_fourth">
                            <section id="text-4" class="widget clearfix widget_text">
                                <h3 class="widgettitle">Matter</h3>
                                <div class="textwidget">
                                    <p>We support media entrepreneurs building a more informed, connected, and empowered society through our start-up accelerator in San Francisco and New York City.</p>
                                    <div id="mc_embed_signup">
                                        <!--{/* @notice */}-->
                                        <!--{/* form[method=POST] is not supported in AMP Generator. */}-->
                                        <!-- <form action="http://matter.us6.list-manage2.com/subscribe/post?u=3a28bb6e5760bd84fa88e6d32&amp;id=1701e22b7b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">

	<div class="mc-field-group">
		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Join our mailing list">
	</div>
	<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
	</form> -->
                                    </div>
                                </div>
                                <span class="seperator extralight-border"></span></section>
                        </div>
                        <div class="flex_column av_one_fourth  el_after_av_one_fourth  el_before_av_one_fourth ">
                            <section id="text-3" class="widget clearfix widget_text">
                                <h3 class="widgettitle">Contact</h3>
                                <div class="textwidget">
                                    <p>421 Bryant St.<br> San Francisco, CA 94107</p>
                                    <p><a href="mailto:hello@matter.vc">hello@matter.vc</a></p>
                                </div>
                                <span class="seperator extralight-border"></span></section>
                        </div>
                        <div class="flex_column av_one_fourth  el_after_av_one_fourth  el_before_av_one_fourth ">
                            <section id="nav_menu-2" class="widget clearfix widget_nav_menu">
                                <h3 class="widgettitle">Menu</h3>
                                <div class="menu-main-menu-container">
                                    <ul id="menu-main-menu" class="menu">
                                        <li id="menu-item-27955" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-27955"><a href="https://matter.vc/">Home</a></li>
                                        <li id="menu-item-19089" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19089"><a href="https://matter.vc/program/">Program</a></li>
                                        <li id="menu-item-1359" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1359"><a href="https://matter.vc/mission/">Mission</a></li>
                                        <li id="menu-item-1361" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1361"><a href="https://matter.vc/community/">Community</a></li>
                                        <li id="menu-item-29047" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-29047"><a href="https://medium.com/matter-driven-narrative">News</a></li>
                                        <li id="menu-item-24795" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24795"><a href="https://matter.vc/jobs/">Jobs</a></li>
                                        <li id="menu-item-30223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30223"><a href="https://matter.vc/apply/">Apply</a></li>
                                    </ul>
                                </div><span class="seperator extralight-border"></span></section>
                        </div>
                        <div class="flex_column av_one_fourth  el_after_av_one_fourth  el_before_av_one_fourth ">
                            <section id="text-6" class="widget clearfix widget_text">
                                <h3 class="widgettitle">Connect</h3>
                                <div class="textwidget"><a href="https://twitter.com/mattervc">Twitter</a><br>
                                    <a href="https://www.facebook.com/mattervc">Facebook</a><br>
                                    <a href="https://medium.com/matter-driven-narrative">Medium</a><br>
                                    <a href="https://instagram.com/mattervc">Instagram</a><br>
                                    <a href="https://angel.co/matter-ventures">AngelList</a><br></div>
                                <span class="seperator extralight-border"></span></section>
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <a href="#top" title="Scroll to top" id="scroll-top-link" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello"><span class="avia_hidden_link_text">Scroll to top</span></a>

        <div id="fb-root"></div>

    </body>
</html>