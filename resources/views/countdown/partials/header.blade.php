<header id="site-header">
    <h1 id="site-logo">

        @if($type=='amp')
            <amp-img src="{{asset('img/countdown/logo.png')}}"
                     alt="ROBOAMP Logo"
                     width="300"
                     height="240"
                     layout="responsive">
            </amp-img>
        @else
        <img src="{{asset('img/countdown/logo.png')}}" alt=""></h1>
        @endif

    <h3 id="site-slogan">We make the web lightning fast</h3>
</header>