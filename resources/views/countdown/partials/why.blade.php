<div class="section-content">
    <h2 class="section-title">Why AMP is important<!-- Here you can change section title --></h2>
    <div class="services">
        <div class="service-item">
            <i class="service-icon-1"></i>
            <h4 class="service-title">Conversion</h4>
            <div class="service-content">
                Accelerated Mobile Pages (AMP) increase your buyer conversion rate and engagement, thus increasing revenue and performance for your brand.
            </div>
        </div>
        <div class="service-item">
            <i class="service-icon-2"></i>
            <h4 class="service-title">lightning Speed</h4>
            <div class="service-content">
                RoboAMP optimizes your mobile website by eliminating inefficiencies to deliver a remarkable user experience with load times up to 5X faster than non-AMP sites.							</div>
        </div>
        <div class="service-item">
            <i class="service-icon-3"></i>
            <h4 class="service-title">SEO</h4>
            <div class="service-content">
                We know how important SEO is for your business. AMP compatible websites have significantly better website rankings on Google with increased SEO.
            </div>
        </div>
    </div>
</div>