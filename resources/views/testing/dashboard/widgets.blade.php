@extends ('testing/dashboard.show')
@section ('title')
    <title>Index</title>
@endsection
@section ('page_specific_css')

    <link href="{{asset('css/test_widgets/jquery.easy-pie-chart.css')}}" rel="stylesheet" type="text/css" media="screen"/>

@endsection
@section ('content')

<div class="row state-overview">
    @foreach($counters as $obj)
        <div class="col-lg-4 col-sm-4">
            {!!$obj['html']!!}
        </div>
    @endforeach
</div>

<div class="row">
    @foreach($widgets as $obj)
        <div class="col-lg-4">
            {!!$obj['html']!!}
        </div>
    @endforeach
</div>

@endsection
@section ('page_specific_scripts')
    <script src="{{asset('js/test_widgets/jquery.easy-pie-chart.js')}}"></script>
    <script src="{{asset('js/test_widgets/easy-pie-chart.js')}}"></script>
    <script src="{{asset('js/test_widgets/jquery.customSelect.min.js')}}"></script>
    <script src="{{asset('js/test_widgets/jquery.sparkline.js')}}"></script>
    <script src="{{asset('js/test_widgets/sparkline-chart.js')}}"></script>   
    <script src="{{asset('js/owl.carousel.js')}}" ></script>
    @foreach($counters as $obj)
        {!!$obj['js']!!}
    @endforeach

    @foreach($widgets as $obj)
        {!!$obj['js']!!}
    @endforeach
        
    <script src="{{asset('js/test_widgets/count.js')}}"></script>
    
    <script>

        //owl carousel
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                navigation : true,
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem : true,
                autoPlay:true
            });

                //custom select box
                $(function(){
                    $('select.styled').customSelect();
                    
                });

            setInterval(function() {
                var data = { "_token": "{{ csrf_token() }}", 'widgets[]': {!!$widgets_name!!}, 'counters[]': {!!$counters_name!!} };
                $.ajax({
                    type: 'POST',
                    url: '/data',
                    data: data,
                    success:(response) => {

                        var obj = $.parseJSON(response);
                        console.log(obj);

                        @foreach($counters as $obj)

                            {!! $obj['triggers'] !!}

                        @endforeach

                        @foreach($widgets as $obj)

                            {!! $obj['triggers'] !!}

                        @endforeach

                    }
                });

            }, 5 * 1000); // 60 * 1000 milsec

            setInterval(function() {
                console.log('GENERATING DATA');
                $.ajax({
                    type: 'GET',
                    url: '/generate/property'
                });
                $.ajax({
                    type: 'GET',
                    url: '/generate/user'
                });
                $.ajax({
                    type: 'GET',
                    url: '/generate/customer'
                });

            }, 10 * 10000); // 60 * 1000 milsec

        });



 
    </script>
@endsection