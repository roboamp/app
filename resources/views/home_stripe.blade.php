<html>
<head>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

</head>
<body>
@foreach ($plans as $plan)

    <button id="{{$plan['stripe_id']}}">{{$plan['nickname']}}</button>

@endforeach



<script>
    $(document).ready()
    {
        var handler = StripeCheckout.configure({
            key: 'pk_test_Jng3zo76N0269r614I9P8TQL',
            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            token: function(token) {
                // You can access the token ID with `token.id`.
                // Get the token ID to your server-side code for use.
            }
        });

@foreach ($plans as $plan)

    @include('admin.includes.stripe_buttons_script',$plan)
@endforeach






        // Close Checkout on page navigation:
        window.addEventListener('popstate', function () {
            handler.close();
        });
    }


</script>
</body>
</html>
