<!doctype html>
<html amp lang="en">
  <head>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
        <title>
</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">

<link rel="canonical" href="/fvr/kreativewebworks/">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

<style amp-custom>
  @font-face{font-family: 'Open Sans', sans-serif ;src: url(https://fonts.googleapis.com/css?family=Open+Sans);}
*{padding:0;margin:0;transition: all 0s ease-in-out;transition-timing-function: cubic-bezier(0.22, .25, .27, .3);}li{list-style:none}input{outline: none;}
*:focus {outline: none;}
html{font-size:62.5%;overflow-x: hidden;}
body{overflow-x: hidden;font-size:16px;font-weight: normal;font-family: 'Open Sans', sans-serif;color:#666;}
p{text-align: justify;}
img{object-fit:cover}
a,a:focus,a:hover,a:visited,button{color:#fff;background:none;border:0;outline:0;text-decoration:none;cursor:pointer}
.left-img img {object-fit: contain;object-position: left;}
.mb1{margin-bottom: 1rem;}.m1{margin: 1rem}.m2{margin: 2rem;}.m20{margin: 2rem 0}.mhalf{margin: 0.5rem}.m1a{margin: 1rem auto;}.m4a{margin: 4rem auto;}.m10{margin: 1rem 0;}.mh0{margin: 0.5rem 0;}.m0h{margin: 0 0.5rem;}.m12{margin: 1rem 2rem;}.mt4{margin-top: 4rem;}.mt2{margin-top: 2rem;}
.fs1{font-size: 1rem}.fs18{font-size: 1.8rem;}.fs16{font-size: 1.6rem;}.fs14{font-size: 1.4rem;}.fs13{font-size: 1.3rem;}.fs12{font-size: 1.2rem;}.fs11{font-size: 1.1rem;}.fs3{font-size: 3rem;}.fs4{font-size: 4rem;}.fs26{font-size: 2.6rem;}.fs2{font-size: 2rem;}
.fw4{font-weight: 400;}.fw5{font-weight: 500;}
.ucase{text-transform: uppercase;}.tdul,.tdhul:hover{text-decoration: underline;}.dimc {color: #707070;}
.br3r{border-radius: 0.3rem;-webkit-border-radius: 0.3rem;}
.p1{padding: 1rem;}.p2{padding: 2rem;}.lp1{padding-left: 1rem; }.p40{padding: 4rem 0;}.p80{padding: 8rem 0;}
.rc {color: #ce2b00;}.gryc{color: #333334;}.gbg{background: #f1f1f2;}
.rbtn{color: #fff;background-color: #ed1c24;border-color: #ed1c24;}
.dfx{display: flex;align-items: center;}.jcsb{justify-content: space-between;}.jcse{justify-content: space-evenly;}.jcsa {justify-content: space-around;}
.flxwrp{flex-wrap: wrap;}.jcc{justify-content: center;}.baseline{align-items: baseline;}.flend{align-items: flex-end;}.flstart{align-items: flex-start;}
.ftcnt{width: fit-content;}.text-center{text-align: center;}.posrel {position: relative;}
.cw, a.cw{color: #fff;}.wbg{background: #fff}
a[href^=tel]{ color:#F00; text-decoration:none;}


h1,h2,h3,h4,h5,h6 {
  font-weight: 500;
}
.wrapper {
  width: 96%;
  margin:auto;
}

.logo{max-width:60vw;width: 30rem;height:5rem;}.logo img{object-fit:contain;}
.mobile-toggler {
  display: none;
}
#home-slider {
  min-height: 40rem;
}
#home-slider .slide-img img {
  object-fit: cover;
}
#home-slider .amp-carousel-button {
display: none;}

.slide-title {
  text-shadow: 1px 1px #000;
  font-size: 6.8rem
}
.menubarc {
  background: rgba(0,0,0,.5);
  min-height: 11rem;
  max-height: 11rem;
  overflow: visible;
  padding: 1rem 10%;
  position: absolute;
  top: 0;
  z-index: 99999;
  width: 80%;
}
.menu-items {
  min-height: 5rem;
  max-width: 60rem;
}
a.menu-link {
    color: #fff;
}
.contact-btn {
  border-radius: 0.8rem;
  margin-left: 0.3rem;
}
body > div:last-child {
  display: none;
}
.sidebar{
  top: calc(0px);
  float: right;
  right: -265px;
  max-width: 260px;
  width: 80%;
  z-index: 9999999;
  position: fixed;
  padding: 0;
  background-color: #EB1C24;
  height: 100%;
  top: 0;
  margin-top: 0;
  padding-bottom: 50px;
  overflow-x: hidden;
}
.sidebar ul li a {
  font-size: 24px;
  width: 100%;
  padding: 20px 0 20px 20px;
  box-sizing: border-box;
  border-bottom: 1px solid rgba(0,0,0,.1);
}
.slider-content {
  position: absolute;
  bottom: 10rem;
  max-width: 70rem;
}
.sidebar .hlg a {
  color: #fff;
}
.sidebar li a:hover {
  color: #EB1C24;
  background-color: #FFF;
}
.industries {z-index: 2}

.w25p {
  min-width: 25%;
  z-index: 1;
  width: 25%;
  position: relative;
  height: 6rem;
  max-height: 6rem;
  top: 0;
  overflow: hidden;
}
.menu {
  width: 100%;
  height: 60px;
  display: inline-block;
  transition: .7s all ease-in-out;
  overflow: hidden;
  position: relative;
}
.industries .w25p:hover {
  overflow: visible;
}
.industries .w25p .menu {
  height: 6rem;
  position: absolute;
  transition: 1s all ease;
}
.industries .w25p:hover .menu {
  height: 30rem;
  position: absolute;
  transform: translateY(-24rem);
  transition: 1s all ease;
}
#menu-1 {
  white-space: normal;
  background-color: #018580;
}
#menu-2 {
  white-space: normal;
  background-color: #ca215e;
}
#menu-3 {
  white-space: normal;
  background-color: #016092;
}
#menu-4 {
  white-space: normal;
  background-color: #d24400;
}

@-webkit-keyframes slide {
  from { background-position: 0 0; }
  to { background-position: -400px 0; }
}
#menu-1:hover {
  background-image: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/industries/schools.jpg')}});
  background-repeat: no-repeat;
  position: relative;
  animation: slide 50s ease-out;
  -moz-animation: slide 50s ease-out;
  -webkit-animation: slide 50s ease-out;

}
#menu-2:hover {
  background-image: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/industries/glass-industries.jpg')}});
  background-repeat: no-repeat;
  position: relative;
  animation: slide 50s ease-out;
  -moz-animation: slide 50s ease-out;
  -webkit-animation: slide 50s ease-out;

}
#menu-3:hover {
  background-image: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/industries/small-businesses.jpg')}});
  background-repeat: no-repeat;
  position: relative;
  animation: slide 50s ease-out;
  -moz-animation: slide 50s ease-out;
  -webkit-animation: slide 50s ease-out;

}
#menu-4:hover {
  background-image: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/industries/long-sales-cycles.jpg')}});
  background-repeat: no-repeat;
  position: relative;
  animation: slide 50s ease-out;
  -moz-animation: slide 50s ease-out;
  -webkit-animation: slide 50s ease-out;
}

#menu-1, #menu-2, #menu-3, #menu-4, .btn-nav2 {
  color: #fff;
}


.reviews-section, .services-section {
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
.services-section {
  background: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/services-bg.jpg')}}) center center no-repeat fixed;
}
.reviews-section {
  background: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/reviews-bg.jpg')}}) center center no-repeat fixed;
}

.services-head {
  color:#889aa7;
  font-size:6.4rem;
  line-height:6.8rem;
  max-width: 96vw;
  overflow: hidden;
}
.larger-text {
  font-size: 22px;
  line-height: 36px;
}

.serviceBox {
  border: 1px solid #ededed;
  transition: all .5s ease 0s;
  min-height: 15rem;
  background-color: rgba(255,255,255,.75);
  padding: 1.9rem;
  margin: auto;
  margin-bottom: 3rem;
}
.serviceBox .title * {
  font-size: 18px;
  font-weight: 700;
  color: #F61A24;
  margin-bottom: 15px;
}
.serviceBox .service-icon {
  display: inline-block;
  top: 33%;
  left: 23px;
  font-size: 45px;
  color: #F61A24;
  opacity: .3;
  transition: all .5s ease-in 0s;
  margin-right: 1rem;
}
.service-Content {
  max-width: 65%;
}
.service-Content .title {
  margin-bottom: 2rem;
}

.serviceBox:hover {
  border-color: #F61A24;
}
.serviceBox:hover .service-icon {
  opacity: 1;
}
.serviceBox:before {
  top: -3px;
}
.serviceBox:after {
  bottom: -3px;
}
.serviceBox:after, .serviceBox:before {
  content: "";
  display: block;
  width: 50px;
  height: 5px;
  background: #F61A24;
  position: absolute;
  left: 0;
  opacity: 0;
  transition: all .5s ease 0s;
}
.serviceBox:hover:after, .serviceBox:hover:before {
  left: 40px;
  opacity: 1;
}

.flpBox {
  width: 30rem;
  height: 30rem;
  min-width: 30rem;
  min-height: 30rem;
  max-width: 90vw;
}
.rb-btn {
  background: #F61A24;
  transition-timing-function: ease-out;
  transform: perspective(1px) translateZ(0);
  transition-duration: .3s;
  transition-property: color;
  color: #fff
}
.rb-btn:hover {
  background: #000;
  transition-timing-function: ease-out;
  transform: perspective(1px) translateZ(0);
  transition-duration: .3s;
  transition-property: color;
  vertical-align: middle;
  color: #fff;
}


.wb-btn {
  background: #fff;
  transition-timing-function: ease-out;
  transform: perspective(1px) translateZ(0);
  transition-duration: .3s;
  transition-property: background;
}
.wb-btn:hover {
  background: #000;
  transition-timing-function: ease-out;
  transform: perspective(1px) translateZ(0);
  transition-duration: .3s;
  transition-property: background;
  vertical-align: middle;
}
.book-img {
  min-width: 100%;
  /* max-width: 90vw; */
  /* margin: auto; */
}
.client-logo {
  width: 18.5rem;
  max-width: 20rem;
}
.client-logo img, .book-img img {
  object-fit: contain;
}

.imgborder {
  display: inline-block;
  position: relative;
  border: 1px solid #ccc;
  padding: 15px;
  background: #f2f2f2;
  margin-bottom: 30px;
}
.imgborder p {
  font-size: 1.8rem;
  line-height: 1.5;
}
.review-box {
  width: 20%;
  min-width: 26rem;
  margin: 0 auto;
}

.toggle {
  font-weight: 700;
  margin: 0 auto;
  display: block;
  background-color: #F61A24;
  padding: 20px;
}
a.toggle {
  color: #fff;
  text-align: center;
}

.footer {
  background-color: #fff;
  color: #515151;
  padding: 40px
}
.toggle,
a.bottom-foot,
a.toggle {
  color: #fff
}
a.toggle {
  text-decoration: none
}
.toggle {
  font-weight: 700;
  margin: 0 auto;
  display: block;
  background-color: #f61a24;
  padding: 20px
}
hr {
  display: block;
  height: 1px;
  border: 0;
  border-top: 1px solid #ccc;
  margin: 1em 0;
  padding: 0
}
a.bottom-foot:link,
a.bottom-foot:visited {
  text-decoration: none;
  color: #fff
}
a.bottom-foot:hover {
  text-decoration: none;
  color: #f61a24
}
a.bottom-foot:active {
  text-decoration: none;
  color: #fff
}
.flpBox {
  background-color: transparent;
  min-width: 100%;
  height: 320px;
  padding: 1rem 0;
  perspective: 1000px;
}
.flipper {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.8s;
  transform-style: preserve-3d;
  min-height: 20rem;
}
.flip-container {
  padding: 1.5rem;
}
.flip-container:hover .flipper {
  transform: rotateY(180deg);
}
.front, .back {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
  height: 320px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.front > div, .back > div {
  padding: 1rem;
}
.front {
  background-color: #bbb;
  color: black;
}
.back {
  background-color: dodgerblue;
  color: white;
  transform: rotateY(180deg);
}
.flip-container p {
  text-align: center;
}




.hvr-shutter-in-horizontal {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: perspective(1px) translateZ(0);
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
  position: relative;
  background: #000;
  -webkit-transition-property: color;
  transition-property: color;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
}
.hvr-shutter-in-horizontal:before {
  content: "";
  position: absolute;
  z-index: -1;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: #F61A24;
  -webkit-transform: scaleX(1);
  transform: scaleX(1);
  -webkit-transform-origin: 50%;
  transform-origin: 50%;
  -webkit-transition-property: transform;
  transition-property: transform;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-timing-function: ease-out;
  transition-timing-function: ease-out;
}
.hvr-shutter-in-horizontal:hover, .hvr-shutter-in-horizontal:focus, .hvr-shutter-in-horizontal:active {
  color: white;
}
.hvr-shutter-in-horizontal:hover:before, .hvr-shutter-in-horizontal:focus:before, .hvr-shutter-in-horizontal:active:before {
  -webkit-transform: scaleX(0);
  transform: scaleX(0);
}


@media(min-width: 768px) {
    .flpBox {
      min-width: 300px;
      height: 320px;
    }
}

@media(min-width: 992px) {
    .container {
        width: 970px
    }
}

@media(min-width: 1200px) {
    .container {
        width: 1170px
    }
}
@media screen and (min-width: 1192px) {
  .hlg {
    display: none;
  }

}
@media screen and (max-width: 1191px) {
  .mobile-toggler {
    display: block;
  }
  .hmd {
    display:none;
  }
  .w25p {
    width:50%;
  }
  .industries .menu:hover {
    height: 4rem;
    top: 0;
    transition: 1s height ease;
  }
}

@media screen and (max-width: 800px) {
  .slide-title {
    font-size: 4rem;
  }
  .see-reviews-btn.wb-btn {
    display: none;
  }
  .slider-content {
    bottom: 3rem;
  }
}

@media screen and (max-width: 768px) {
  .menubarc {
    background: #000;
  }
  .w25p {
    width:100%;
    overflow: hidden;
  }
  .industries .menu:hover {
    /* height: 4rem;
    top: 0;
    transition: 1s height ease; */
  }
  .slider-content {
    text-align: left;
    margin-left: 1rem;
  }

  .industries .w25p:hover .menu {
    height: 6rem;
    position: absolute;
    transform: translateY(0);
    transition: 1s all ease;
  }

}
.col-lg-1,
.col-lg-10,
.col-lg-11,
.col-lg-12,
.col-lg-2,
.col-lg-3,
.col-lg-4,
.col-lg-5,
.col-lg-6,
.col-lg-7,
.col-lg-8,
.col-lg-9,
.col-md-1,
.col-md-10,
.col-md-11,
.col-md-12,
.col-md-2,
.col-md-3,
.col-md-4,
.col-md-5,
.col-md-6,
.col-md-7,
.col-md-8,
.col-md-9,
.col-sm-1,
.col-sm-10,
.col-sm-11,
.col-sm-12,
.col-sm-2,
.col-sm-3,
.col-sm-4,
.col-sm-5,
.col-sm-6,
.col-sm-7,
.col-sm-8,
.col-sm-9,
.col-xs-1,
.col-xs-10,
.col-xs-11,
.col-xs-12,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9 {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px
}

.col-xs-1,
.col-xs-10,
.col-xs-11,
.col-xs-12,
.col-xs-2,
.col-xs-3,
.col-xs-4,
.col-xs-5,
.col-xs-6,
.col-xs-7,
.col-xs-8,
.col-xs-9 {
    float: left
}

.col-xs-12 {
    width: 100%;
}

.col-xs-11 {
    width: 91.66666667%;
}

.col-xs-10 {
    width: 83.33333333%;
}

.col-xs-9 {
    width: 75%;
}

.col-xs-8 {
    width: 66.66666667%;
}

.col-xs-7 {
    width: 58.33333333%;
}

.col-xs-6 {
    width: 50%;
}

.col-xs-5 {
    width: 41.66666667%;
}

.col-xs-4 {
    width: 33.33333333%;
}

.col-xs-3 {
    width: 25%;
}

.col-xs-2 {
    width: 16.66666667%;
}

.col-xs-1 {
    width: 8.33333333%;
}

@media(min-width: 768px) {
    .col-sm-1,
    .col-sm-10,
    .col-sm-11,
    .col-sm-12,
    .col-sm-2,
    .col-sm-3,
    .col-sm-4,
    .col-sm-5,
    .col-sm-6,
    .col-sm-7,
    .col-sm-8,
    .col-sm-9 {
        float: left
    }
    .col-sm-12 {
        width: 100%;
    }
    .col-sm-11 {
        width: 91.66666667%;
    }
    .col-sm-10 {
        width: 83.33333333%;
    }
    .col-sm-9 {
        width: 75%;
    }
    .col-sm-8 {
        width: 66.66666667%;
    }
    .col-sm-7 {
        width: 58.33333333%;
    }
    .col-sm-6 {
        width: 50%;
    }
    .col-sm-5 {
        width: 41.66666667%;
    }
    .col-sm-4 {
        width: 33.33333333%;
    }
    .col-sm-3 {
        width: 25%;
    }
    .col-sm-2 {
        width: 16.66666667%;
    }
    .col-sm-1 {
        width: 8.33333333%;
    }
}

@media(min-width: 992px) {
    .col-md-1,
    .col-md-10,
    .col-md-11,
    .col-md-12,
    .col-md-2,
    .col-md-3,
    .col-md-4,
    .col-md-5,
    .col-md-6,
    .col-md-7,
    .col-md-8,
    .col-md-9 {
        float: left
    }
    .col-md-12 {
        width: 100%;
    }
    .col-md-11 {
        width: 91.66666667%;
    }
    .col-md-10 {
        width: 83.33333333%;
    }
    .col-md-9 {
        width: 75%;
    }
    .col-md-8 {
        width: 66.66666667%;
    }
    .col-md-7 {
        width: 58.33333333%;
    }
    .col-md-6 {
        width: 50%;
    }
    .col-md-5 {
        width: 41.66666667%;
    }
    .col-md-4 {
        width: 33.33333333%;
    }
    .col-md-3 {
        width: 25%;
    }
    .col-md-2 {
        width: 16.66666667%;
    }
    .col-md-1 {
        width: 8.33333333%;
    }
}
@media(min-width: 1200px) {
    .col-lg-1,
    .col-lg-10,
    .col-lg-11,
    .col-lg-12,
    .col-lg-2,
    .col-lg-3,
    .col-lg-4,
    .col-lg-5,
    .col-lg-6,
    .col-lg-7,
    .col-lg-8,
    .col-lg-9 {
        float: left
    }
    .col-lg-12 {
        width: 100%;
    }
    .col-lg-11 {
        width: 91.66666667%;
    }
    .col-lg-10 {
        width: 83.33333333%;
    }
    .col-lg-9 {
        width: 75%;
    }
    .col-lg-8 {
        width: 66.66666667%;
    }
    .col-lg-7 {
        width: 58.33333333%;
    }
    .col-lg-6 {
        width: 50%;
    }
    .col-lg-5 {
        width: 41.66666667%;
    }
    .col-lg-4 {
        width: 33.33333333%;
    }
    .col-lg-3 {
        width: 25%;
    }
    .col-lg-2 {
        width: 16.66666667%;
    }
    .col-lg-1 {
        width: 8.33333333%;
    }
}
.container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto
}
@media(max-width: 336px) {
    .container {
        width: 90%;
    }
}
@media(min-width: 768px) {
    .container {
        width: 750px
    }
}
@media(min-width: 992px) {
    .container {
        width: 970px
    }
}
@media(min-width: 1200px) {
    .container {
        width: 1170px
    }
}
</style>
</head>
<body>
  <amp-sidebar id="sidebar-right"
  class="sidebar"
  layout="nodisplay"
  side="right">
  <button on="tap:sidebar-right.close" class="m2"><i class="fa fa-close fs4 cw"></i></button>
  <nav toolbar="(min-width: 1192px)"
    toolbar-target="target-element-right">
    <ul class="menu-items">
              <li class="menu-item dfx fs18 p1">
          <a class="menu-link" href="https://www.kreativewebworks.com/about-us/">About US</a>
        </li>
              <li class="menu-item dfx fs18 p1">
          <a class="menu-link" href="https://www.kreativewebworks.com/about-you/">About You</a>
        </li>
              <li class="menu-item dfx fs18 p1">
          <a class="menu-link" href="https://www.kreativewebworks.com/our-services/">Our Services</a>
        </li>
              <li class="menu-item dfx fs18 p1">
          <a class="menu-link" href="https://www.kreativewebworks.com/portfolio/">Our Work</a>
        </li>
              <li class="menu-item dfx fs18 p1">
          <a class="menu-link" href="https://blog.kreativewebworks.com">Trends</a>
        </li>
            <li class="menu-item hlg dfx fs18 p1"><a class="menu-link" href="/contact/">Contact</a></li>
      <li class="hmd rbtn p1 dfx contact-btn"><a class="" href="/contact/"><span style="color:#fff;">Contact Us</span></a></li>
       <li class="hmd"><a href="tel:+19492766063" class="menu-link"><span style="font-weight:bold;font-size:24px;">(949) 276-6063</span></a></li>
    </ul>
  </nav>
</amp-sidebar>
<div class="menubarc dfx">
  <div class="wrapper">
    <div class="menubar dfx jcsb">
      <div class="logoc">
        <amp-img itemprop="logo" class="logo" src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/logo.png')}}" alt="logo" height="1" width="1" layout="responsive"></amp-img>
      </div>
      <button on="tap:sidebar-right.toggle" class="mobile-toggler mt2"><svg enable-background="new 0 0 512 512" height="50px" id="Layer_1" version="1.1" viewBox="0 0 512 512" width="60px" xml:space="preserve" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
        <g>
          <rect style="fill:#fff;stroke:none" id="rect2985" y="0.52245784" x="0.45167819" height="53.861767" width="511.68677"></rect>
        <rect style="fill:#fff;stroke:none" id="rect2985-6-6" transform="scale(1,-1)" y="-376.41687" x="0.45167819" height="53.861767" width="511.68677"></rect>
         <rect style="fill:#fff;stroke:none" id="rect2985-6" transform="scale(1,-1)" y="-215.40071" x="0.45167819" height="53.861767" width="511.68677"></rect>
        </g>
      </svg></button>
      <div id="target-element-right" class="mt4">
      </div>
    </div>
  </div>
</div>
<amp-carousel
  id="home-slider"
  width="940"
  height="400"
  layout="responsive"
  type="slides"
  autoplay
  delay="2000">
      <div style="background: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/slider/slide-1.jpg')}}) ;background-size: cover;background-position: center;">
      <div class="dfx text-center flxwrp jcc">
        <div class="cw fw5 slider-content">
          <h2 class="slide-title">Digital Inbound Marketing</h2>
          <h2 class="white">Measurable Results... Because Data Beats Opinions</h2>
          <div class="mt4">
          <a href="https://www.customerlobby.com/reviews/4677/kreative-webworks-inc/" target="_blank" class="hvr-shutter-in-vertical wb-btn see-reviews-btn" style="padding: 20px;">
            <span style="color:#ED1B23; font-weight:bold;">
              SEE OUR REVIEWS
            </span>
          </a>
          </div>
        </div>
      </div>
    </div>
      <div style="background: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/slider/slide-2.jpg')}}) ;background-size: cover;background-position: center;">
      <div class="dfx text-center flxwrp jcc">
        <div class="cw fw5 slider-content">
          <h2 class="slide-title">Digital Inbound Marketing</h2>
          <h2 class="white">Measurable Results... Because Data Beats Opinions</h2>
          <div class="mt4">
          <a href="https://www.customerlobby.com/reviews/4677/kreative-webworks-inc/" target="_blank" class="hvr-shutter-in-vertical wb-btn see-reviews-btn" style="padding: 20px;">
            <span style="color:#ED1B23; font-weight:bold;">
              SEE OUR REVIEWS
            </span>
          </a>
          </div>
        </div>
      </div>
    </div>
      <div style="background: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/slider/slide-3.jpg')}}) ;background-size: cover;background-position: center;">
      <div class="dfx text-center flxwrp jcc">
        <div class="cw fw5 slider-content">
          <h2 class="slide-title">Digital Inbound Marketing</h2>
          <h2 class="white">Measurable Results... Because Data Beats Opinions</h2>
          <div class="mt4">
          <a href="https://www.customerlobby.com/reviews/4677/kreative-webworks-inc/" target="_blank" class="hvr-shutter-in-vertical wb-btn see-reviews-btn" style="padding: 20px;">
            <span style="color:#ED1B23; font-weight:bold;">
              SEE OUR REVIEWS
            </span>
          </a>
          </div>
        </div>
      </div>
    </div>
      <div style="background: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/slider/slide-4.jpg')}}) ;background-size: cover;background-position: center;">
      <div class="dfx text-center flxwrp jcc">
        <div class="cw fw5 slider-content">
          <h2 class="slide-title">Digital Inbound Marketing</h2>
          <h2 class="white">Measurable Results... Because Data Beats Opinions</h2>
          <div class="mt4">
          <a href="https://www.customerlobby.com/reviews/4677/kreative-webworks-inc/" target="_blank" class="hvr-shutter-in-vertical wb-btn see-reviews-btn" style="padding: 20px;">
            <span style="color:#ED1B23; font-weight:bold;">
              SEE OUR REVIEWS
            </span>
          </a>
          </div>
        </div>
      </div>
    </div>
      <div style="background: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/slider/slide-5.jpg')}}) ;background-size: cover;background-position: center;">
      <div class="dfx text-center flxwrp jcc">
        <div class="cw fw5 slider-content">
          <h2 class="slide-title">Digital Inbound Marketing</h2>
          <h2 class="white">Measurable Results... Because Data Beats Opinions</h2>
          <div class="mt4">
          <a href="https://www.customerlobby.com/reviews/4677/kreative-webworks-inc/" target="_blank" class="hvr-shutter-in-vertical wb-btn see-reviews-btn" style="padding: 20px;">
            <span style="color:#ED1B23; font-weight:bold;">
              SEE OUR REVIEWS
            </span>
          </a>
          </div>
        </div>
      </div>
    </div>
  </amp-carousel>
<div class="industries dfx flxwrp" style="z-index: 1">
  <div class="w25p" style="z-index: 111;">

        <div class="menu" id="menu-1">

            <h2 class="text-center" style="padding-top:10px;">Schools &amp; Education</h2>
            <p class="vertical-markets" style="padding:20px;">Proven strategies for enrolling more students.</p>
          <p class="text-center"><a href="https://www.kreativewebworks.com/vertical-markets/schools-and-education/" class="cw">- Learn
            <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e')}}/img/kww_cog.png" class="cog-spin" alt="Schools and Education" width="1" height="1"></amp-img> More -</a></p>

      </div>

  </div>

    <div class="w25p" style="z-index: 111;">

        <div class="menu" id="menu-2">

            <h2 class="text-center" style="padding-top:10px;">The Glass Industries</h2>
            <p style="padding:20px;">Glass &amp; Mirror, Auto Glass, Windows, and Window Film</p>
            <p class="text-center"><a href="https://www.kreativewebworks.com/vertical-markets/glass-industries/" class="cw">- Learn
              <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e')}}/img/kww_cog.png" class="cog-spin" alt="Glass and Mirror" width="1" height="1"></amp-img> More -</a></p>

        </div>

  </div>

    <div class="w25p" style="z-index: 111;">

        <div class="menu" id="menu-3">

            <h2 class="text-center" style="padding-top:10px;">Small Businesses</h2>
            <p style="padding:20px;">Have BIG ambitions and a moderate budget?</p>
            <p class="text-center"><a href="https://www.kreativewebworks.com/vertical-markets/small-business/" class="cw">- Learn
              <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e')}}/img/kww_cog.png" class="cog-spin" alt="Small Businesses" width="1" height="1"></amp-img> More -</a></p>

        </div>

  </div>

    <div class="w25p" style="z-index: 111;">

        <div class="menu" id="menu-4">

            <h2 class="text-center" style="padding-top:10px;">Lead Nurturing</h2>
            <p style="padding:20px;">Strategies for industries with complex buying decisions.</p>
            <p class="text-center"><a href="https://www.kreativewebworks.com/vertical-markets/long-sales-cycles/" class="cw">- Learn
              <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e')}}/img/kww_cog.png" class="cog-spin" alt="Long Sales Cycles" width="1" height="1"></amp-img> More -</a></p>

        </div>

    </div>

</div>
<div class="row full-padding" style="z-index: auto">
  <div class="container text-center p40">
    <h2 class="services-head fw5">A Professional Digital Marketing Agency</h2>
     <p class="larger-text text-center">Driven by Data, Kreative by Nature</p>
  </div>
</div>


<div class="services-section p40" id="services">

  <div class="container dfx flxwrp baseline jcse">
    <div class="col-md-4 col-sm-6 col-xs-12 ">
      <div class="serviceBox posrel dfx jcse">
        <div class="service-icon">
            <i class="fa fa-bullseye" aria-hidden="true"></i>
        </div>
        <div class="service-Content">
            <h3 class="title"><a href="#" class="services-link" data-toggle="modal" data-target="#myModal1">Inbound Marketing Strategies</a></h3>
            <p class="description" style="color:#000;">
                No one size fits all strategies. This one’s for you.
            </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12 ">
      <div class="serviceBox posrel dfx jcse">
        <div class="service-icon">
            <i class="fa fa-globe" aria-hidden="true"></i>
        </div>
        <div class="service-Content">
            <h3 class="title"><a href="#" class="services-link" data-toggle="modal" data-target="#myModal2">Website Design &amp; Maintenance</a></h3>
            <p class="description" style="color:#000;">
                Growth driven design to scale with your business.
            </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12 ">
      <div class="serviceBox posrel dfx jcse">
        <div class="service-icon">
          <i class="fa fa-search" aria-hidden="true"></i>
        </div>
        <div class="service-Content">
          <h3 class="title"><a href="#" class="services-link" data-toggle="modal" data-target="#myModal3">Search Engine Optimization</a></h3>
          <p class="description" style="color:#000;">
              No promises…just results. We’re very good at this.
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12 ">
      <div class="serviceBox posrel dfx jcse">
        <div class="service-icon">
            <i class="fa fa-area-chart" aria-hidden="true"></i>
        </div>
        <div class="service-Content">
            <h3 class="title"><a href="#" class="services-link" data-toggle="modal" data-target="#myModal4">Digital Advertising Management</a></h3>
            <p class="description" style="color:#000;">
                Google, Bing, Facebook advertising…we manage it all.
            </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12 ">
      <div class="serviceBox posrel dfx jcse">
        <div class="service-icon">
            <i class="fa fa-comments" aria-hidden="true"></i>
        </div>
        <div class="service-Content">
          <h3 class="title"><a href="#" class="services-link" data-toggle="modal" data-target="#myModal5">Social Media Management</a></h3>
          <p class="description" style="color:#000;">
              We create, we publish, we brand…
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12 ">
      <div class="serviceBox posrel dfx jcse">
        <div class="service-icon">
          <i class="fa fa-user-plus" aria-hidden="true"></i>
        </div>
        <div class="service-Content">
          <h3 class="title"><a href="#" class="services-link" data-toggle="modal" data-target="#myModal6">PartnerPlus Program</a></h3>
          <p class="description" style="color:#000;">
              We become your Digital Marketing department.

          </p>
        </div>
      </div>
    </div>

        <div class="col-xs-12 text-center"><a href="/our-services/" class="hvr-shutter-in-horizontal rb-btn fs2 p2">See Each of our Digital Marketing Services
      </a>
    </div>
  </div>
</div>
<div class="p40">

  <div class="container flip-boxes dfx flxwrp jcsa">

    <div class="col-md-4 col-sm-6 col-xs-12 flpBox text-center baseline">

      <div class="flip-container">

        <div class="flipper square-box">

          <div class="front" style="background-color:#BD3D24;">
            <div>
              <h2 style="font-size:64px; color:#FFF;">93%</h2>
              <p style="font-size:30px; font-weight: lighter; color:#FFF;">Of Buying Starts With Online Search</p>
            </div>

          </div>

          <div class="back" style="background-color:#000;">

            <p style="font-size:20px; font-weight: lighter; color:#FFF; padding:10px;">SEO is vitally important for businesses looking to get found online. If you don’t rank well in search, it’s hard for people to find you. (<a href="https://www.marketo.com/resources/" target="_blank">Marketo</a>)</p>

          </div>

        </div>

      </div>
    </div>


    <div class="col-md-4 col-sm-6 col-xs-12 flpBox text-center baseline">

      <div class="flip-container">

        <div class="flipper square-box">

          <div class="front" style="background-color:#0C4359;">
            <div>
              <h2 style="font-size:64px; color:#FFF;">451%</h2>
              <p style="font-size:30px; font-weight: lighter; color:#FFF;">Increase via Marketing Automation</p>
            </div>


          </div>

          <div class="back" style="background-color:#000;">

            <p style="font-size:20px; font-weight: lighter; color:#FFF; padding:10px;">Companies that use marketing automation to nurture prospects experience a 451% increase in qualified leads (Salesforce)</p>

          </div>

        </div>

      </div>
    </div>




    <div class="col-md-4 col-sm-6 col-xs-12 flpBox text-center baseline">

      <div class="flip-container">

        <div class="flipper square-box">

          <div class="front" style="background-color:#330066;">
            <div>
              <h2 style="font-size:64px; color:#FFF;">62%</h2>
              <p style="font-size:30px; font-weight: lighter; color:#fff;">Less Per Lead With Inbound</p>
            </div>


          </div>

          <div class="back" style="background-color:#000;">

            <p style="font-size:20px; font-weight: lighter; color:#FFF; padding:10px;">Inbound marketing costs 62% less per lead than traditional marketing such as radio, TV, magazines, direct-mail, etc. (<a href="https://www.hubspot.com/marketing-statistics" target="_blank">Hubspot</a>)</p>

          </div>

        </div>

      </div>
    </div>



    <div class="col-md-4 col-sm-6 col-xs-12 flpBox text-center baseline">

      <div class="flip-container">

        <div class="flipper square-box">

          <div class="front" style="background-color:#640534;">
            <div>
              <h2 style="font-size:64px; color:#FFF;">61%</h2>
              <p style="font-size:30px; font-weight: lighter; color:#fff;">Of Consumers Want Custom Content</p>
            </div>


          </div>

          <div class="back" style="background-color:#000;">

            <p style="font-size:20px; font-weight: lighter; color:#FFF; padding:10px;">61% of consumers say they feel better about a company that delivers custom content and are also more likely to buy from that company. (Custom Content Council).</p>

          </div>

        </div>

      </div>
    </div>



    <div class="col-md-4 col-sm-6 col-xs-12 flpBox text-center baseline">

      <div class="flip-container">

        <div class="flipper square-box">

          <div class="front" style="background-color:#006699;">
            <div>
              <h2 style="font-size:64px; color:#FFF;">82%</h2>
              <p style="font-size:30px; font-weight: lighter; color:#FFF;">Of Facebook Traffic Links to Longer Stories</p>
            </div>


          </div>

          <div class="back" style="background-color:#000;">

            <p style="font-size:20px; font-weight: lighter; color:#FFF; padding:10px;">Facebook sends 82% of social media traffic to longer stories and 84% of social traffic to shorter news articles. (Pew Research Center, 2016)</p>

          </div>

        </div>

      </div>
    </div>



    <div class="col-md-4 col-sm-6 col-xs-12 flpBox text-center baseline">

      <div class="flip-container">

        <div class="flipper square-box">

          <div class="front" style="background-color:#989113;">
            <div>
              <h2 style="font-size:64px; color:#FFF;">64%</h2>
              <p style="font-size:30px; font-weight: lighter; color:#FFF;">Of People Click on Google Ads</p>
            </div>


          </div>

          <div class="back" style="background-color:#000;">

            <p style="font-size:20px; font-weight: lighter; color:#FFF; padding:10px;">64% of people click on Google Ads when they are looking to buy. The average conversion rate across all industries is 2.7% (WordStream, 2016)</p>

          </div>

        </div>

      </div>
    </div>

  </div>

</div>
<div class="row light-padded">
  <div class="container">
   <div class="clearfix">&nbsp;</div>
      <div class="col-xs-12">
        <h2 class="text-center">From Our Resources Library</h2>
      </div>

      <div class="books-container flxwrp jcse m4a med-padded">

      <div class="col-md-3">
        <a href="/25-website-must-haves/" target="_blank">
          <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/books/essential-website-tips.png')}}" alt="Drive-Traffic-Leads-and-sales" class="book-img" width="500" height="645" layout="responsive"></amp-img>
        </a>
      </div>

      <div class="col-md-3">
        <a href="/the-digital-marketing-smell-test/" target="_blank">
          <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/books/digital-marketing-smell-test.png')}}" alt="How-To-" class="book-img" width="500" height="645" layout="responsive"></amp-img>
        </a>
      </div>

      <div class="col-md-3">
        <a href="/landing-page-optimization/" target="_blank">
          <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/books/landing-page-optimization.png')}}" alt="Landing-Page-Optimization" class="book-img" width="500" height="645" layout="responsive"></amp-img>
        </a>
      </div>

      <div class="col-md-3">
        <a href="/guide-to-enrolling-students/" target="_blank">
          <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/books/charter-school-marketing-guide.png')}}" alt="Charter-School-Masters-Guide" class="book-img" width="500" height="645" layout="responsive"></amp-img>
        </a>
      </div>
    </div>
  </div>

</div>
<div class="reviews-section p40" id="reviews">
  <div class="container dfx flstart flxwrp jcse">
    <div class="col-md-3 col-sm-6 col-xs-12 review-box">
      <div class=" imgborder">
        <h3 style="font-size:26px;">Excellent!!</h3>
        <p class="">My rating: <span class="rating-input" style="color:#E1CD07"><span data-value="0" class="fa fa-star"></span><span data-value="1" class="fa fa-star"></span><span data-value="2" class="fa fa-star"></span><span data-value="3" class="fa fa-star"></span><span data-value="4" class="fa fa-star"></span></span></p>
        <p class="m20">There are web designers and SEO specialists on every corner in America. Finally, we have found in Kreative Webworks a company who achieved results quickly and very cost effective. In an industry where there are mostly pretenders, Chuck and his staff stand clear of their competition as the real deal.</p>
        <p> <strong>Thomas M.</strong><br><em>Newport Beach, CA</em> </p>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 review-box">
      <div class=" imgborder">
        <h3 style="font-size:26px;">Great Service and Results!</h3>
        <p class="">My rating: <span class="rating-input" style="color:#E1CD07"><span data-value="0" class="fa fa-star"></span><span data-value="1" class="fa fa-star"></span><span data-value="2" class="fa fa-star"></span><span data-value="3" class="fa fa-star"></span><span data-value="4" class="fa fa-star"></span></span></p>
        <p class="m20">Chuck and his wonderful team have been handling all of my internet needs for my company for the last 5 or 6 years. They are always very responsive to all of my requests and they stay on top of all of the changing trends as they relate to internet search and website design. I have seen my business and my online presence continue to grow since I hired Kreative Webworks!</p>
        <p> <strong>Cory W.</strong><br><em>Dallas, TX</em> </p>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 review-box">
      <div class=" imgborder">
        <h3 style="font-size:26px;">Amazing</h3>
        <p class="">My rating: <span class="rating-input" style="color:#E1CD07"><span data-value="0" class="fa fa-star"></span><span data-value="1" class="fa fa-star"></span><span data-value="2" class="fa fa-star"></span><span data-value="3" class="fa fa-star"></span><span data-value="4" class="fa fa-star"></span></span></p>
        <p class="m20">We are an independent school. After extensive research we found Kreative Webworks. All I can say is AMAZING. This has been a great partnership. It's as if we got a brand new employee who is a top level digital marketer. This is far more than an SEO company. This is a social media, website, ad campaign powerhouse! Great customer service. Many in the school wanted to go with "tradition" private school providers. These guys turned out to be much better.</p>
        <p> <strong>Adam G.</strong><br><em>Orange Park, FL</em> </p>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 review-box">
      <div class=" imgborder">
        <h3 style="font-size:26px;">Very responsive and great results!</h3>
        <p class="">My rating: <span class="rating-input" style="color:#E1CD07"><span data-value="0" class="fa fa-star"></span><span data-value="1" class="fa fa-star"></span><span data-value="2" class="fa fa-star"></span><span data-value="3" class="fa fa-star"></span><span data-value="4" class="fa fa-star"></span></span></p>
        <p class="m20">They ALWAYS follow through with what they say they will do. They said they would get us a lot of new customers, and that’s just what they constantly do. I highly recommend them for any business that relies on the Internet to get new customers!</p>
        <p> <strong>Debbie K.</strong><br><em>Anaheim, CA</em> </p>
      </div>
    </div>
    <div class="col-xs-12 text-center"><a href="https://www.customerlobby.com/reviews/4677/kreative-webworks-inc/" target="_blank" class="hvr-shutter-in-horizontal rb-btn fs2 p2">See All Independent Third-Party Reviews</a> </div>
  </div>
</div>
<div class="p80">
  <div class="container">
    <div class="dfx flxwrp jcc">
      <div class="m1">
        <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/clients/HubSpot-Logo.png')}}'" alt="Hubspot" class="client-logo" width="1" height="1" layout="responsive"></amp-img>
      </div>

      <div class="m1">
         <a href="https://upcity.com/local-marketing-agencies/lists/content-agencies-in-irvine" target="_blank">
          <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/clients/2017_content_national.png')}}" alt="Top Content Marketing Agency 2017" class="client-logo" width="1" height="1" layout="responsive"></amp-img>
        </a>
       </div>

      <div class="m1">
        <a href="https://www.wsiworld.com/about-us/leadership/" target="_blank">
          <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/clients/GlobalAgencyNetwork_Seal_A.png')}}" alt="WSI Certified Agency" class="client-logo" width="1" height="1" layout="responsive"></amp-img>
        </a>
      </div>

      <div class="m1">
        <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/clients/sempo_membership_logo_platin.jpg')}}" alt="HTML5" class="client-logo" width="1" height="1" layout="responsive"></amp-img>
      </div>

      <div class="m1">
        <a href="https://sharpspring.com/" target="_blank">
          <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/clients/emblem.png')}}" alt="Sharpspring" class="client-logo" width="1" height="1" layout="responsive"></amp-img>
          </a>
      </div>

    </div>
  </div>

</div>
<div class="footer-container">

  <a href="#" class="toggle">WANT A BIGGER FOOTER?</a>
    <div class="footer" style="display: none">

     <div class="row">

        <div class="container">

          <div class="col-sm-4 col-xs-12 text-center">
              <p>
                <amp-img src="{{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e/img/logo.png')}}'" alt="Kreative-Webworks" style="padding-bottom:10px; width:100%; max-width:300px;" width="6" height="1" layout="responsive"></amp-img>

26961 Camino De Estrella, Suite 300<br>
Capistrano Beach, CA 92624<br>
Phone: 949-276-6063</p>
<div class="hidden-lg hidden-md hidden-sm"><span style="color:#000;"><hr></span></div>
          </div>

                <div class="col-sm-4 col-xs-12 text-center">
                <h3><i class="fa fa-globe" aria-hidden="true"></i>
 How's Your Web Presence?</h3>
<p><a href="/webscan-for-business/" target="_blank">Webscan™ Website Audit <br>
(Promotional Offer of the Month)</a></p>
<div class="hidden-lg hidden-md hidden-sm"><span style="color:#000;"><hr></span></div>
                </div>

                <div class="col-sm-4 col-xs-12 text-center">
                    <h3>Connect With Us</h3>
                    <div class="social">
                        <ul>
                            <li><a href="https://www.facebook.com/KreativeWebworks" target="_blank"><i class="fa fa-lg fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/KreativeWebwrks" target="_blank"><i class="fa fa-lg fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://plus.google.com/+Kreativewebworks" target="_blank"><i class="fa fa-lg fa-google-plus" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/chuckbankoff" target="_blank"><i class="fa fa-lg fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>

                   </div>
                </div>

          </div>

      </div>

    </div>

    <a href="#" class="toggle" style="display: none;">STILL NOT BIG ENOUGH?</a>
    <div class="footer" style="background-image: url({{asset('properties/c5c30070-ce72-11e7-acde-9b4153c2a21e')}}/img/media/website_designs/3/foot-bg.gif); color: rgb(255, 255, 255); display: none;">
      <div class="container">
            <div class="col-sm-4 col-xs-12 med-padded">

                <h3>Services</h3>

                <p style="line-height:28px;">


                    <a href="/inbound-marketing-strategies/" class="bottom-foot"><i class="fa fa-bullseye" aria-hidden="true"></i> Inbound Marketing Strategies</a><br>

                    <a href="/our-services/orange-county-web-design/" class="bottom-foot"><i class="fa fa-globe" aria-hidden="true"></i> Website Design &amp; Maintenance</a><br>

                    <a href="/our-services/seo/" class="bottom-foot"><i class="fa fa-search" aria-hidden="true"></i> Search Engine Optimization</a><br>

                    <a href="/digital-advertising-management/" class="bottom-foot"><i class="fa fa-area-chart" aria-hidden="true"></i> Digital Advertising Management</a><br>

                    <a href="/our-services/social-media-management/" class="bottom-foot"><i class="fa fa-comments" aria-hidden="true"></i> Social Media Management</a><br>

                    <a href="/our-services/partner-plus-program/" class="bottom-foot"><i class="fa fa-user-plus" aria-hidden="true"></i> Partner Plus Program</a>

                </p>
            </div>

            <div class="col-sm-4 col-xs-12 med-padded">

            <h3>Additional Links</h3>
             <p style="line-height:28px;">
       <a href="https://www.kreativewebworks.com/about-us/" class="bottom-foot"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
 About Us</a><br>
            <a href="https://www.kreativewebworks.com/about-you/" class="bottom-foot"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
 About You</a><br>
             <a href="https://www.kreativewebworks.com/portfolio/" class="bottom-foot"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
 Our Work</a><br>
             <a href="https://www.blog.kreativewebworks.com/" class="bottom-foot"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
 Trends</a>
       </p>
       <form action="https://www.kreativewebworks.com/search/" target="_top" style="color:#666;">
        <input name="query">
        <input type="submit" value="Search">
      </form>
       </div>



            <div class="col-sm-4 col-xs-12 med-padded">

            <h3>HOW CAN WE HELP?</h3>
            <p style="line-height:28px;">
                <a href="/contact/" class="bottom-foot"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
 Contact Us</a><br>
          <a href="/sitemap/" class="bottom-foot"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
 Site Map</a><br>
          <a href="/privacy-policy/" class="bottom-foot"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
 Privacy Policy</a></p>


            </div>


    </div>

        </div>

        <a href="#" class="toggle" style="display: none;">STILL NOT IMPRESSED?</a>
    <div class="footer text-center" style="display: none;">
      <div class="med-padding">
      <p style="font-size:40px; line-height:60px;">IMPRESSED NOW?</p>

        <a href="/contact/" class="hvr-shutter-in-horizontal" style="margin-top: 30px;">Get Your Free Consultation</a>

    </div>
    <div class="col-xs-12 text-center" style="padding-top:50px;">
  <p>©
  2019
 Kreative Webworks All Rights Reserved</p></div>

    </div>

</div>

</body>
</html>
