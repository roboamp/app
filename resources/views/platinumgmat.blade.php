<!doctype html>
<!--{/*
@info
Generated on: Wed, 23 May 2018 16:50:02 GMT
Initiator: ROBOAMP AMP Generator
Generator version: 0.9.6
*/}-->
<html ⚡ lang="en">
    <head>
        <meta charset="utf-8">
        <title>GMAT Prep From Platinum GMAT - Premier GMAT Prep Resources</title>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <script src="https://cdn.ampproject.org/v0/amp-ad-0.1.js" async="async" custom-element="amp-ad"></script>
        <script src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js" async="async" custom-element="amp-analytics"></script>
        <script src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async="async" custom-element="amp-accordion"></script>
        <link rel="canonical" href="http://www.platinumgmat.com/">
        <meta name="generator" content="https://generator.rabbit.gomobile.jp">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta name="Description" content="Find world-class free GMAT prep resources including practice questions with thorough answers, study guides, strategy tips, and detailed analyses of the GMAT.">
        <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript data-amp-spec=""><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:500">
        <style amp-custom="">body {
  font-family: Verdana,Tahoma,Arial,Helvetica,sans-serif
}
a amp-img {
  border: 0
}
#footer {
  margin-top: 2em;
  border-top: 0 solid gray;
  padding-top: .5em;
  font-size: .8em;
  color: #333
}
label {
  cursor: pointer
}
.inline-question {
  margin: .5em .1em;
  border: 1px solid #000;
  padding: .1em
}
.inline-question table {
  empty-cells: show;
  border-collapse: collapse
}
.inline-question table > tr > td {
  border: 1px solid #000;
  padding: 2px 3px
}
.inline-question-title {
  font-weight: 700;
  text-decoration: underline
}
.inline-question-answers {
  margin: 1em 1em 1em 2em
}
.inline-question-answers > tr > td {
  border: none
}
.inline-question-answer-letter {
  width: 3em;
  vertical-align: top
}
.inline-question-answer-contents {
  vertical-align: top
}
#member-tab {
  display: inline;
  float: right;
  color: #fff;
  padding-right: 2.5em
}
#member-signin-anchor,
#member-signout-anchor,
#member-signup-anchor {
  color: #fff;
  font-size: 2em;
  text-decoration: none;
  line-height: 60px;
  text-decoration: underline;
  font-size: 100%;
  display: inline;
  border: 0;
  margin: 0;
  padding: 0
}
.member-link-login-component-enabled {
  display: inline
}
.member-link-login-component-disabled {
  display: none
}
.homepage-subheader {
  margin-bottom: .25em
}
table {
  border-collapse: collapse;
  empty-cells: show
}
.underline {
  text-decoration: underline
}
.upper-alpha {
  list-style-type: upper-alpha
}
#content h1#title {
  font-family: Verdana,Arial,Helvetica,sans-serif;
  font-size: 1.65em;
  font-weight: 700;
  margin: 0;
  margin-bottom: 8px;
  padding-top: 3px;
  padding-bottom: 1px;
  color: #103f60;
  line-height: 125%;
  border-bottom: 0 solid #3a495c
}
#content h3 {
  margin: 1em 0;
  font-family: Verdana,Arial,Helvetica,sans-serif;
  font-size: 1.05em;
  font-weight: 700;
  color: #103f60
}
body {
  border: none;
  margin: 0;
  padding: 0
}
#header {
  background: 0 0;
  border-bottom: 0 solid green
}
#header-gutter {
  width: row;
  margin: 0 auto;
  background: #103f60;
  height: 70px
}
#logo {
  padding-left: 1px;
  width: row
}
html > body #logo {
  padding-left: 0;
  width: row
}
#logo-a {
  color: #fff;
  font-size: 2em;
  text-decoration: none;
  line-height: 60px;
  padding-left: 0
}
#myNavbar li a:hover,
#myNavbar li.ancestor a,
#myNavbar li.current a {
  color: #fff;
  background: #c0202a
}
#content-wrapper {
  width: row;
  margin: 0 auto;
  padding-left: 1px
}
html > body #content-wrapper {
  padding-left: 0
}
#content-main-wide {
  background: #fff
}
#content {
  font-size: 1em;
  font-family: Arial,Helvetica,sans-serif;
  line-height: 1.5em
}
.content-main a {
  color: #103f60;
  border-bottom-width: 1px;
  border-bottom-style: solid;
  border-bottom-color: #002bb8;
  text-decoration: none
}
html > body .content-main a {
  border-bottom-width: 0;
  text-decoration: underline
}
.content-main a:active {
  color: #ff9934;
  border-bottom-width: 2px;
  border-bottom-color: #ff9934
}
.content-main a:hover {
  color: #c0202a
}
#footer-links {
  text-align: center;
  margin-bottom: .5em
}
html {
  font-family: sans-serif;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%
}
body {
  margin: 0
}
nav {
  display: block
}
a {
  background-color: transparent
}
a:active,
a:hover {
  outline: 0
}
b,
strong {
  font-weight: 700
}
h1 {
  margin: .67em 0;
  font-size: 2em
}
amp-img {
  border: 0
}
button,
input {
  margin: 0;
  font: inherit;
  color: inherit
}
button {
  overflow: visible
}
button {
  text-transform: none
}
button {
  -webkit-appearance: button;
  cursor: pointer
}
button::-moz-focus-inner,
input::-moz-focus-inner {
  padding: 0;
  border: 0
}
input {
  line-height: normal
}
input[type=radio] {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  padding: 0
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  height: auto
}
input[type=search]::-webkit-search-cancel-button,
input[type=search]::-webkit-search-decoration {
  -webkit-appearance: none
}
table {
  border-spacing: 0;
  border-collapse: collapse
}
td {
  padding: 0
}
@font-face {
  font-family: 'Glyphicons Halflings';
  src: url(http://www.platinumgmat.com/views/contentPage/fonts/glyphicons-halflings-regular.eot);
  src: url(http://www.platinumgmat.com/views/contentPage/fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(http://www.platinumgmat.com/views/contentPage/fonts/glyphicons-halflings-regular.woff2) format('woff2'),url(http://www.platinumgmat.com/views/contentPage/fonts/glyphicons-halflings-regular.woff) format('woff'),url(http://www.platinumgmat.com/views/contentPage/fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(http://www.platinumgmat.com/views/contentPage/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')
}
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
html {
  font-size: 10px;
  -webkit-tap-highlight-color: transparent
}
body {
  font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
  font-size: 14px;
  line-height: 2.5;
  color: #333;
  background-color: #60a9c7
}
button,
input {
  font-family: inherit;
  font-size: inherit;
  line-height: inherit
}
a {
  color: #337ab7;
  text-decoration: none
}
a:focus,
a:hover {
  color: #23527c;
  text-decoration: underline
}
a:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
h1,
h3 {
  font-family: inherit;
  font-weight: 500;
  line-height: 1.1;
  color: inherit
}
h1,
h3 {
  margin-top: 20px;
  margin-bottom: 10px
}
h1 {
  font-size: 36px
}
h3 {
  font-size: 24px
}
p {
  margin: 0 0 10px
}
ol,
ul {
  margin-top: 0;
  margin-bottom: 10px
}
.container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto
}
.row {
  margin-right: 0;
  margin-left: 0
}
.col-lg-10,
.col-lg-12,
.col-lg-8,
.col-sm-12,
.col-sm-6,
.col-xs-3,
.col-xs-4,
.col-xs-6,
.col-xs-7 {
  position: relative;
  min-height: 1px;
  padding-right: 5px;
  padding-left: 5px
}
.col-xs-3,
.col-xs-4,
.col-xs-6,
.col-xs-7 {
  float: left
}
.col-xs-7 {
  width: 58.33333333%
}
.col-xs-6 {
  width: 50%
}
.col-xs-4 {
  width: 33.33333333%
}
.col-xs-3 {
  width: 25%
}
.col-xs-offset-1 {
  margin-left: 8.33333333%
}
table {
  background-color: transparent
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px
}
input[type=radio] {
  margin: 4px 0 0;
  line-height: normal
}
input[type=radio]:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
.form-control::-moz-placeholder {
  color: #999;
  opacity: 1
}
.form-control:-ms-input-placeholder {
  color: #999
}
.form-control::-webkit-input-placeholder {
  color: #999
}
.collapse {
  display: none
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none
}
.nav > li {
  position: relative;
  display: block
}
.nav > li > a {
  position: relative;
  display: block;
  padding: 10px 10px
}
.nav > li > a:focus,
.nav > li > a:hover {
  text-decoration: none;
  background-color: #eee
}
.navbar {
  position: relative;
  min-height: 50px;
  margin-bottom: 1px;
  border: 1px solid transparent
}
.navbar-collapse {
  padding-right: 15px;
  padding-left: 15px;
  overflow-x: visible;
  -webkit-overflow-scrolling: touch;
  border-top: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.1);
  box-shadow: inset 0 1px 0 rgba(255,255,255,.1)
}
.navbar-brand {
  float: left;
  height: 50px;
  padding: 15px 15px;
  font-size: 18px;
  line-height: 20px
}
.navbar-brand:focus,
.navbar-brand:hover {
  text-decoration: none
}
.navbar-toggle {
  position: relative;
  float: right;
  padding: 9px 10px;
  margin-top: 8px;
  margin-right: 15px;
  margin-bottom: 8px;
  background-color: #60a9c7;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px
}
.navbar-toggle:focus {
  outline: 0
}
.navbar-toggle .icon-bar {
  display: block;
  width: 22px;
  height: 2px;
  border-radius: 1px
}
.navbar-toggle .icon-bar + .icon-bar {
  margin-top: 4px
}
.navbar-nav {
  margin: 7.5px -15px
}
.navbar-nav > li > a {
  padding-top: 10px;
  padding-bottom: 10px;
  line-height: 20px
}
.navbar-inverse {
  background-color: #60a9c7;
  border-color: #60a9c7
}
.navbar-inverse .navbar-brand {
  color: #9d9d9d
}
.navbar-inverse .navbar-brand:focus,
.navbar-inverse .navbar-brand:hover {
  color: #fff;
  background-color: transparent
}
.navbar-inverse .navbar-nav > li > a {
  color: #fff
}
.navbar-inverse .navbar-nav > li > a:focus,
.navbar-inverse .navbar-nav > li > a:hover {
  color: #fff;
  background-color: #c0202a
}
.navbar-inverse .navbar-toggle {
  border-color: #60a9c7
}
.navbar-inverse .navbar-toggle:focus,
.navbar-inverse .navbar-toggle:hover {
  background-color: #60a9c7
}
.navbar-inverse .navbar-toggle .icon-bar {
  background-color: #fff
}
.navbar-inverse .navbar-collapse {
  border-color: #101010
}
.container-fluid:after,
.container-fluid:before,
.nav:after,
.nav:before,
.navbar-collapse:after,
.navbar-collapse:before,
.navbar-header:after,
.navbar-header:before,
.navbar:after,
.navbar:before,
.row:after,
.row:before {
  display: table;
  content: " "
}
.container-fluid:after,
.nav:after,
.navbar-collapse:after,
.navbar-header:after,
.navbar:after,
.row:after {
  clear: both
}
.rbt-inline-0 {
  font-family: Raleway
}
#member-tab {
  align: right
}
#content {
  background: #fff
}
.rbt-inline-3 {
  padding-bottom: 10px
}
.rbt-inline-11 {
  border: 0 none;
  float: right;
  text-decoration: none
}
.rbt-inline-12 {
  display: block
}
section.rbt-accordion-section > header {
  background-color: transparent;
  padding: 0;
  margin: 0;
  border: 0
}</style>
    </head>
    <body class="wide-content">
        <amp-analytics type="googleanalytics" id="rbt-ga-1">
            <script type="application/json">
                {
                    "vars": {
                        "account": "UA-5765867-1"
                    },
                    "triggers": {
                        "trackPageview": {
                            "on": "visible",
                            "request": "pageview"
                        }
                    }
                }
            </script>
        </amp-analytics>
        <div id="header">
            <div class="row" id="header-gutter">
                <div class="col-xs-7 col-xs-offset-1" id="logo"><a href="http://www.platinumgmat.com/" title="GMAT Prep" id="logo-a"><span class="rbt-inline-0">PlatinumGMAT</span></a></div>
                <div class="col-xs-4" id="member-tab"><a href="http://www.platinumgmat.com/memberSignup" id="member-signup-anchor" class="member-link-login-component-enabled">Register</a><span id="member-link-divider" class="member-link-login-component-enabled">&nbsp;&middot;&nbsp;</span><a href="http://www.platinumgmat.com/memberSignin" id="member-signin-anchor" class="member-link-login-component-enabled">Login</a><a href="http://www.platinumgmat.com/memberSignin?state=doSignout&amp;mode=html-form" id="member-signout-anchor" class="member-link-login-component-disabled">Logout</a></div>
            </div>
        </div>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
                        <a class="navbar-brand" href="#"></a>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li class="ancestor current"><a href="http://www.platinumgmat.com/">GMAT Prep</a></li>
                                <li><a href="http://www.platinumgmat.com/gmat-practice-test/">GMAT Practice Test</a></li>
                                <li><a href="http://www.platinumgmat.com/practice_gmat_questions/">GMAT Practice Questions</a></li>
                                <li><a href="http://www.platinumgmat.com/about_gmat/">About GMAT</a></li>
                                <li><a href="http://www.platinumgmat.com/gmat_study_guide/">GMAT Study Guide</a></li>
                                <li><a href="http://www.platinumgmat.com/mba_admissions/">MBA Admissions</a></li>
                                <li><a href="http://www.platinumgmat.com/blog/">GMAT Blog</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <amp-accordion>
            <section class="col-lg-offset-1" id="content-wrapper">
                <!-- <div class="col-lg-offset-1" id="content-wrapper"> -->
                <header>
                    <div class="content-main" id="content-main-wide">
                        <div class="col-lg-8 col-lg-offset-2" id="content">
                            <h1 id="title">GMAT Prep From Platinum GMAT</h1>

                            <div class="row">
                                <div class="col-sm-6">
                                    <h3>GMAT Prep Materials</h3>
                                    <div class="row" id="Links">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p class="homepage-subheader rbt-inline-3"><b><a>GMAT Practice Questions</a></b></p>
                                            </div>
                                            <div class="col-sm-12 rbt-inline-3">
                                                <div class="col-xs-4"><a>Critical Reasoning</a></div>
                                                <div class="col-xs-4"><a>Sentence Correction</a>&nbsp;</div>
                                                <div class="col-xs-4"><a>Reading Comprehension</a></div>
                                            </div>
                                            <div class="col-sm-12 rbt-inline-3">
                                                <div class="col-xs-4"><a>Problem Solving</a></div>
                                                <div class="col-xs-4"><a>Data Sufficiency</a></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p class="homepage-subheader rbt-inline-3"><b><a>GMAT Study Guide</a></b></p>
                                            </div>
                                            <div class="col-sm-12 rbt-inline-3">
                                                <div class="col-xs-6"><a>Number Properties</a></div>
                                                <div class="col-xs-6"><a>Combinatorics</a></div>
                                            </div>
                                            <div class="col-sm-12 rbt-inline-3">
                                                <div class="col-xs-6"><a>AWA Essay Template</a></div>
                                                <div class="col-xs-6"><a>GMAT Idioms</a></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12 rbt-inline-3">
                                                <p class="homepage-subheader"><b><a>MBA Admissions</a></b></p>
                                            </div>
                                            <div class="col-sm-12 rbt-inline-3">
                                                <div class="col-xs-3"><a>Average Scores &amp; GPAs</a></div>
                                                <div class="col-xs-3"><a>Rankings</a></div>
                                                <div class="col-xs-3"><a>School Profiles</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <h3>Announcement</h3>
                                        <p>Check out our latest blog post about <a>The Best GMAT Books for Studying on Your Own</a>.</p>
                                    </div>
                                    <div class="row">
                                        <h3>About Us</h3>
                                        <p>Platinum GMAT Prep provides the best GMAT preparation materials available anywhere, enabling individuals to master the GMAT and gain admission to any MBA program. We also provide hundreds of pages of free GMAT prep content, including practice questions, study guides, and test overviews.</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="inline-question">
                                        <div class="inline-question-title">
                                            <a class="rbt-inline-11">
                                                <amp-img src="http://www.platinumgmat.com/global/images/icons/feed.gif" alt="rss" width="16" height="16" layout="fixed"></amp-img>
                                            </a> GMAT Practice Question (One of Hundreds)</div>
                                        <div class="inline-question-contents">Two leagues exist in major league baseball, the National League and <span class="underline">the American League; differing slightly in rules, they have the same basic game play.</span></div>
                                        <table class="inline-question-answers">
                                            <tr class="inline-question-answer">
                                                <td class="inline-question-answer-letter"><label for="q0_ansA"><input type="radio" name="q0_ans" value="A" id="q0_ansA">A) </label></td>
                                                <td class="inline-question-answer-contents"><label for="q0_ansA">the American League; differing slightly in rules, they have the same basic game play.</label></td>
                                            </tr>
                                            <tr class="inline-question-answer">
                                                <td class="inline-question-answer-letter"><label for="q0_ansB"><input type="radio" name="q0_ans" value="B" id="q0_ansB">B) </label></td>
                                                <td class="inline-question-answer-contents"><label for="q0_ansB">the American League; although the two leagues are the same in basic game play, the two leagues have slightly different rules.</label></td>
                                            </tr>
                                            <tr class="inline-question-answer">
                                                <td class="inline-question-answer-letter"><label for="q0_ansC"><input type="radio" name="q0_ans" value="C" id="q0_ansC">C) </label></td>
                                                <td class="inline-question-answer-contents"><label for="q0_ansC">the American League; the two leagues have the same basic game play, and they have slightly different rules.</label></td>
                                            </tr>
                                            <tr class="inline-question-answer" id="inline-question-correct-answer-0">
                                                <td class="inline-question-answer-letter"><label for="q0_ansD"><input type="radio" name="q0_ans" value="D" id="q0_ansD">D) </label></td>
                                                <td class="inline-question-answer-contents"><label for="q0_ansD">the American League; although the same in basic game play, the two leagues have slightly different rules.</label></td>
                                            </tr>
                                            <tr class="inline-question-answer">
                                                <td class="inline-question-answer-letter"><label for="q0_ansE"><input type="radio" name="q0_ans" value="E" id="q0_ansE">E) </label></td>
                                                <td class="inline-question-answer-contents"><label for="q0_ansE">the American league, having the same basic game play and different rules.</label></td>
                                            </tr>
                                        </table>

                                        <div class="inline-question-answer" id="inline-question-answer-0">Correct Answer: <strong>D</strong></div>

                                        <div class="inline-question-hint" id="inline-question-hint-0">A pronoun must have a clear antecedent. Participial phrases at the beginning of a sentence modify the word that immediately follows them, and participial phrases in the middle of a sentence modify the word that immediately precedes them. State the information in the most concise manner possible.</div>

                                        <div class="inline-question-exp" id="inline-question-exp-0">
                                            <div>
                                                <p>This sentence, although grammatically correct, is not as clear as it could be. <i>They</i> is vague pronoun, and its antecedent is not clear. <i>They</i> should be replaced with a noun and the proper modifiers, such as <i>the two leagues</i>.</p>
                                                <ol class="upper-alpha">
                                                    <li><i>They</i> is vague pronoun, and its antecedent is not clear.</li>
                                                    <li>This sentence is wordy. Using <i>the two leagues</i> twice makes the sentence longer and more wordy than necessary.</li>
                                                    <li>This sentence is wordy. Using the pronoun <i>they</i> is unnecessary and makes the sentence longer and more wordy than necessary.</li>
                                                    <li>This sentence relates the material in the most concise manner without the use of vague pronouns.</li>
                                                    <li><i>Having the same basic game play and different rules</i> incorrectly modifies league. This causes confusion.</li>
                                                </ol>
                                            </div>
                                            <div class="inline_question_explanation_feedback" id="inline_question_explanation_feedback_27">

                                            </div>
                                        </div>

                                        <div class="inline-question-footer-link"><a>View More GMAT Practice Questions</a></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <amp-ad type="adsense" data-ad-client="ca-pub-4367824193171189" data-ad-slot="9731365553" width="728" height="190"></amp-ad>
                                </div>
                            </div>

                            <div class="row">

                            </div>
                            <div class="col-lg-12" id="footer">
                                <div id="footer-links"><a>Terms of Use</a> | <a>Privacy Policy</a> | <a>Contact Us</a> | <a><i class="fa fa-twitter-square"></i></a> | <a><i class="fa fa-facebook-square"></i></a></div>
                                &copy; 2015 Lighthouse Prep, LLC. GMAT&trade; is a registered trademark of The Graduate Management Admission Council&trade; (GMAC), which does not endorse nor is affiliated in any way with the owner of this website or any content contained herein. All content (including practice questions and study guides) is written by PlatinumPrep, LLC not GMAC.</div>
                        </div>
                    </div>
                </header>

                <!-- </div> -->
            </section>
        </amp-accordion>
    </body>
</html>