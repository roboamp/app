:root {
--blue: #007bff;
--indigo: #6610f2;
--purple: #6f42c1;
--pink: #e83e8c;
--red: #dc3545;
--orange: #fd7e14;
--yellow: #ffc107;
--green: #28a745;
--teal: #20c997;
--cyan: #17a2b8;
--white: #fff;
--gray: #6c757d;
--gray-dark: #343a40;
--primary: #007bff;
--secondary: #6c757d;
--success: #28a745;
--info: #17a2b8;
--warning: #ffc107;
--danger: #dc3545;
--light: #f8f9fa;
--dark: #343a40;
--breakpoint-xs: 0;
--breakpoint-sm: 576px;
--breakpoint-md: 768px;
--breakpoint-lg: 992px;
--breakpoint-xl: 1200px;
--font-family-sans-serif: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
--font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace
}

.header:before {
content: "";
position: absolute;
width: 100%;
height: 30px;
right: 0;
top: 0;
background: -moz-linear-gradient(left, rgba(76, 76, 76, 0) 0, #282828 100%);
background: -webkit-linear-gradient(left, rgba(76, 76, 76, 0) 0, #282828 100%);
background: linear-gradient(to right, rgba(76, 76, 76, 0) 0, #282828 100%)
}

a,
body,
div,
figcaption,
figure,
footer,
form,
h1,
h2,
h3,
h4,
header,
html,
i,
label,
li,
main,
nav,
p,
section,
small,
span,
strong,
u,
ul {
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline
}

html {
font-size: 63%
}

@media (max-width:767px) {
html {
font-size: 60%
}
}

.out {
overflow: hidden;
position: fixed
}

*,
::after,
::before {
box-sizing: border-box
}

footer,
header,
main,
nav,
section {
display: block
}

body {
margin: 0;
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
font-size: 1rem;
font-weight: 400;
line-height: 1.5;
color: #212529;
text-align: left;
background-color: #fff
}

[tabindex="-1"]:focus {
outline: 0
}

h2,
h3 {
margin-top: 0;
margin-bottom: .5rem
}

p {
margin-top: 0;
margin-bottom: 1rem
}

ul {
margin-top: 0;
margin-bottom: 1rem
}

ul ul {
margin-bottom: 0
}

strong {
font-weight: bolder
}

a {
color: #007bff;
text-decoration: none;
background-color: transparent
}

a:hover {
color: #0056b3;
text-decoration: underline
}

label {
display: inline-block;
margin-bottom: .5rem
}

button {
border-radius: 0
}

button:focus {
outline: 1px dotted;
outline: 5px auto -webkit-focus-ring-color
}

button,
input {
margin: 0;
font-family: inherit;
font-size: inherit;
line-height: inherit
}

button,
input {
overflow: visible
}

button {
text-transform: none
}

[type=button],
[type=submit],
button {
-webkit-appearance: button
}

[type=button]::-moz-focus-inner,
[type=submit]::-moz-focus-inner,
button::-moz-focus-inner {
padding: 0;
border-style: none
}

input[type=checkbox] {
box-sizing: border-box;
padding: 0
}

.h2,
.h3,
.h5,
h2,
h3 {
margin-bottom: .5rem;
font-weight: 500;
line-height: 1.2
}

.h2,
h2 {
font-size: 2rem
}

.h3,
h3 {
font-size: 1.75rem
}

.h5 {
font-size: 1.25rem
}

.container {
width: 100%;
padding-right: 15px;
padding-left: 15px;
margin-right: auto;
margin-left: auto
}

@media (min-width:576px) {
.container {
max-width: 540px
}
}

@media (min-width:768px) {
.container {
max-width: 720px
}
}

@media (min-width:992px) {
.container {
max-width: 960px
}
}

@media (min-width:1200px) {
.container {
max-width: 1140px
}
}

.container-fluid {
width: 100%;
padding-right: 15px;
padding-left: 15px;
margin-right: auto;
margin-left: auto
}

.row {
display: flex;
flex-wrap: wrap;
margin-right: -15px;
margin-left: -15px
}

.no-gutters {
margin-right: 0;
margin-left: 0
}

.no-gutters>[class*=col-] {
padding-right: 0;
padding-left: 0
}

.col,
.col-12,
.col-3,
.col-4,
.col-5,
.col-7,
.col-8,
.col-9,
.col-auto,
.col-lg-11,
.col-lg-2,
.col-lg-3,
.col-lg-4,
.col-lg-6,
.col-lg-9,
.col-md-4,
.col-md-6,
.col-md-8,
.col-sm-12,
.col-xl-7 {
position: relative;
width: 100%;
padding-right: 15px;
padding-left: 15px
}

.col {
flex-basis: 0;
flex-grow: 1;
max-width: 100%
}

.col-auto {
flex: 0 0 auto;
width: auto;
max-width: 100%
}

.col-3 {
flex: 0 0 25%;
max-width: 25%
}

.col-4 {
flex: 0 0 33.3333333333%;
max-width: 33.3333333333%
}

.col-5 {
flex: 0 0 41.6666666667%;
max-width: 41.6666666667%
}

.col-7 {
flex: 0 0 58.3333333333%;
max-width: 58.3333333333%
}

.col-8 {
flex: 0 0 66.6666666667%;
max-width: 66.6666666667%
}

.col-9 {
flex: 0 0 75%;
max-width: 75%
}

.col-12 {
flex: 0 0 100%;
max-width: 100%
}

.order-1 {
order: 1
}

.order-2 {
order: 2
}

.order-3 {
order: 3
}

@media (min-width:576px) {
.col-sm-12 {
flex: 0 0 100%;
max-width: 100%
}
}

@media (min-width:768px) {
.col-md-4 {
flex: 0 0 33.3333333333%;
max-width: 33.3333333333%
}
.col-md-6 {
flex: 0 0 50%;
max-width: 50%
}
.col-md-8 {
flex: 0 0 66.6666666667%;
max-width: 66.6666666667%
}
}

@media (min-width:992px) {
.col-lg-2 {
flex: 0 0 16.6666666667%;
max-width: 16.6666666667%
}
.col-lg-3 {
flex: 0 0 25%;
max-width: 25%
}
.col-lg-4 {
flex: 0 0 33.3333333333%;
max-width: 33.3333333333%
}
.col-lg-6 {
flex: 0 0 50%;
max-width: 50%
}
.col-lg-9 {
flex: 0 0 75%;
max-width: 75%
}
.col-lg-11 {
flex: 0 0 91.6666666667%;
max-width: 91.6666666667%
}
.order-lg-2 {
order: 2
}
.order-lg-3 {
order: 3
}
}

@media (min-width:1200px) {
.col-xl-7 {
flex: 0 0 58.3333333333%;
max-width: 58.3333333333%
}
}

.form-control {
display: block;
width: 100%;
height: calc(1.5em + .75rem + 2px);
padding: .375rem .75rem;
font-size: 1rem;
font-weight: 400;
line-height: 1.5;
color: #495057;
background-color: #fff;
background-clip: padding-box;
border: 1px solid #ced4da;
border-radius: .25rem;
transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
.form-control {
transition: none
}
}

.form-control::-ms-expand {
background-color: transparent;
border: 0
}

.form-control:focus {
color: #495057;
background-color: #fff;
border-color: #80bdff;
outline: 0;
box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25)
}

.form-control:disabled {
background-color: #e9ecef;
opacity: 1
}

.form-group {
margin-bottom: 1rem
}

.btn {
display: inline-block;
font-weight: 400;
color: #212529;
text-align: center;
vertical-align: middle;
user-select: none;
background-color: transparent;
border: 1px solid transparent;
padding: .375rem .75rem;
font-size: 1rem;
line-height: 1.5;
border-radius: .25rem;
transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
.btn {
transition: none
}
}

.btn:hover {
color: #212529;
text-decoration: none
}

.btn:focus {
outline: 0;
box-shadow: 0 0 0 .2rem rgba(0, 123, 255, .25)
}

.btn:disabled {
opacity: .65
}

.btn-primary {
color: #fff;
background-color: #007bff;
border-color: #007bff
}

.btn-primary:hover {
color: #fff;
background-color: #0069d9;
border-color: #0062cc
}

.btn-primary:focus {
box-shadow: 0 0 0 .2rem rgba(38, 143, 255, .5)
}

.btn-primary:disabled {
color: #fff;
background-color: #007bff;
border-color: #007bff
}

.btn-secondary,
.btn.btn-secondary_transparent {
color: #fff;
background-color: #6c757d;
border-color: #6c757d
}

.btn-secondary:hover,
.btn.btn-secondary_transparent:hover {
color: #fff;
background-color: #5a6268;
border-color: #545b62
}

.btn-secondary:focus,
.btn.btn-secondary_transparent:focus {
box-shadow: 0 0 0 .2rem rgba(130, 138, 145, .5)
}

.btn-secondary:disabled,
.btn.btn-secondary_transparent:disabled {
color: #fff;
background-color: #6c757d;
border-color: #6c757d
}

.btn-link {
font-weight: 400;
color: #007bff;
text-decoration: none
}

.btn-link:hover {
color: #0056b3;
text-decoration: underline
}

.btn-link:focus {
text-decoration: underline;
box-shadow: none
}

.btn-link:disabled {
color: #6c757d;
pointer-events: none
}

.btn-sm {
padding: .25rem .5rem;
font-size: .875rem;
line-height: 1.5;
border-radius: .2rem
}

.fade {
transition: opacity .15s linear
}

@media (prefers-reduced-motion:reduce) {
.fade {
transition: none
}
}

.fade:not(.show) {
opacity: 0
}

.collapse:not(.show) {
display: none
}

.dropdown {
position: relative
}

.dropdown-toggle {
white-space: nowrap
}

.dropdown-toggle::after {
display: inline-block;
margin-left: .255em;
vertical-align: .255em;
content: "";
border-top: .3em solid;
border-right: .3em solid transparent;
border-bottom: 0;
border-left: .3em solid transparent
}

.dropdown-menu {
position: absolute;
top: 100%;
left: 0;
z-index: 1000;
display: none;
float: left;
min-width: 10rem;
padding: .5rem 0;
margin: .125rem 0 0;
font-size: 1rem;
color: #212529;
text-align: left;
list-style: none;
background-color: #fff;
background-clip: padding-box;
border: 1px solid rgba(0, 0, 0, .15);
border-radius: .25rem
}

.dropdown-menu-right {
right: 0;
left: auto
}

.dropdown-item {
display: block;
width: 100%;
padding: .25rem 1.5rem;
clear: both;
font-weight: 400;
color: #212529;
text-align: inherit;
white-space: nowrap;
background-color: transparent;
border: 0
}

.dropdown-item:focus,
.dropdown-item:hover {
color: #16181b;
text-decoration: none;
background-color: #f8f9fa
}

.dropdown-item:active {
color: #fff;
text-decoration: none;
background-color: #007bff
}

.dropdown-item:disabled {
color: #6c757d;
pointer-events: none;
background-color: transparent
}

.custom-control {
position: relative;
display: block;
min-height: 1.5rem;
padding-left: 1.5rem
}

.custom-control-input {
position: absolute;
z-index: -1;
opacity: 0
}

.card {
position: relative;
display: flex;
flex-direction: column;
min-width: 0;
word-wrap: break-word;
background-color: #fff;
background-clip: border-box;
border: 1px solid rgba(0, 0, 0, .125);
border-radius: .25rem
}

.close {
float: right;
font-size: 1.5rem;
font-weight: 700;
line-height: 1;
color: #000;
text-shadow: 0 1px 0 #fff;
opacity: .5
}

.close:hover {
color: #000;
text-decoration: none
}

button.close {
padding: 0;
background-color: transparent;
border: 0;
appearance: none
}

.modal {
position: fixed;
top: 0;
left: 0;
z-index: 1050;
display: none;
width: 100%;
height: 100%;
overflow: hidden;
outline: 0
}

.modal-dialog {
position: relative;
width: auto;
margin: .5rem;
pointer-events: none
}

.modal.fade .modal-dialog {
transition: transform .3s ease-out;
transform: translate(0, -50px)
}

@media (prefers-reduced-motion:reduce) {
.modal.fade .modal-dialog {
transition: none
}
}

.modal-dialog-centered {
display: flex;
align-items: center;
min-height: calc(100% - 1rem)
}

.modal-dialog-centered::before {
display: block;
height: calc(100vh - 1rem);
content: ""
}

.modal-content {
position: relative;
display: flex;
flex-direction: column;
width: 100%;
pointer-events: auto;
background-color: #fff;
background-clip: padding-box;
border: 1px solid rgba(0, 0, 0, .2);
border-radius: .3rem;
outline: 0
}

.modal-header {
display: flex;
align-items: flex-start;
justify-content: space-between;
padding: 1rem 1rem;
border-bottom: 1px solid #dee2e6;
border-top-left-radius: .3rem;
border-top-right-radius: .3rem
}

.modal-header .close {
padding: 1rem 1rem;
margin: -1rem -1rem -1rem auto
}

.modal-title {
margin-bottom: 0;
line-height: 1.5
}

.modal-body {
position: relative;
flex: 1 1 auto;
padding: 1rem
}

@media (min-width:576px) {
.modal-dialog {
max-width: 500px;
margin: 1.75rem auto
}
.modal-dialog-centered {
min-height: calc(100% - 3.5rem)
}
.modal-dialog-centered::before {
height: calc(100vh - 3.5rem)
}
}

@media (min-width:992px) {
.modal-lg {
max-width: 800px
}
}

.bg-white {
background-color: #fff
}

.card::after,
.clearfix::after,
.header::after {
display: block;
clear: both;
content: ""
}

.flex-column {
flex-direction: column
}

.logos-rewards-container {
flex-wrap: wrap
}

.justify-content-end {
justify-content: flex-end
}

.justify-content-center {
justify-content: center
}

.justify-content-between {
justify-content: space-between
}

.align-items-center,
.logos-rewards-container {
align-items: center
}

@media (min-width:992px) {
.flex-lg-row {
flex-direction: row
}
}

.float-right {
float: right
}

.position-relative {
position: relative
}

.position-absolute {
position: absolute
}

.sr-only {
position: absolute;
width: 1px;
height: 1px;
padding: 0;
overflow: hidden;
clip: rect(0, 0, 0, 0);
white-space: nowrap;
border: 0
}

.sr-only-focusable:active,
.sr-only-focusable:focus {
position: static;
width: auto;
height: auto;
overflow: visible;
clip: auto;
white-space: normal
}

.card {
box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15)
}

.w-100 {
width: 100%
}

.m-0 {
margin: 0
}

.mb-1 {
margin-bottom: .25rem
}

.ml-1 {
margin-left: .25rem
}

.mx-2 {
margin-right: .5rem
}

.mb-2 {
margin-bottom: .5rem
}

.ml-2,
.mx-2 {
margin-left: .5rem
}

.mb-3 {
margin-bottom: 1rem
}

.mb-4 {
margin-bottom: 1.5rem
}

.mt-5 {
margin-top: 3rem
}

.mb-5 {
margin-bottom: 3rem
}

.ml-5 {
margin-left: 3rem
}

.p-0 {
padding: 0
}

.pr-0,
.px-0 {
padding-right: 0
}

.px-0 {
padding-left: 0
}

.py-3 {
padding-top: 1rem
}

.pb-3,
.py-3 {
padding-bottom: 1rem
}

.px-4 {
padding-right: 1.5rem
}

.py-4 {
padding-bottom: 1.5rem
}

.px-4 {
padding-left: 1.5rem
}

.m-auto {
margin: auto
}

.mx-auto {
margin-right: auto
}

.mx-auto {
margin-left: auto
}

@media (min-width:992px) {
.mb-lg-0 {
margin-bottom: 0
}
.mb-lg-3 {
margin-bottom: 1rem
}
.ml-lg-5 {
margin-left: 3rem
}
.pr-lg-4 {
padding-right: 1.5rem
}
}

.text-left {
text-align: left
}

.text-right {
text-align: right
}

.text-center {
text-align: center
}

.text-uppercase {
text-transform: uppercase
}

.text-white {
color: #fff
}

@media print {
*,
::after,
::before {
text-shadow: none;
box-shadow: none
}
a:not(.btn) {
text-decoration: underline
}
h2,
h3,
p {
orphans: 3;
widows: 3
}
h2,
h3 {
page-break-after: avoid
}
body {
min-width: 992px
}
.container {
min-width: 992px
}
}

.slick-slider {
position: relative;
display: block;
box-sizing: border-box;
-webkit-touch-callout: none;
-webkit-user-select: none;
-khtml-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
-ms-touch-action: pan-y;
touch-action: pan-y;
-webkit-tap-highlight-color: transparent
}

a,
body,
div,
footer,
form,
h2,
h3,
header,
i,
label,
li,
main,
nav,
p,
section,
span,
strong,
u,
ul {
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline
}

footer,
header,
main,
nav,
section {
display: block
}

body {
line-height: 1
}

ul {
list-style: none
}

.color-primary.color-primary {
color: #c20808;
fill: #c20808
}

.bg-color-white.bg-color-white {
background: #fff
}

.color-white.color-white {
color: #fff;
fill: #fff
}

.color-black.color-black {
color: #000;
fill: #000
}

.color-grey-cool.color-grey-cool {
color: #686868;
fill: #686868
}

.color-gunmetal.color-gunmetal {
color: #53565a;
fill: #53565a
}

.color-lima.color-lima {
color: #7cb41b;
fill: #7cb41b
}

[class*=" icon-"]:before,
[class^=icon-]:before {
font-family: font-icons;
display: inline-block;
line-height: 1;
font-weight: 400;
font-style: normal;
speak: none;
text-decoration: inherit;
text-transform: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale
}

.icon-arrow-down:before {
content: "\f10c"
}

.icon-arrow-right:before {
content: "\f10f"
}

.icon-best-price-guaranteed:before {
content: "\f120"
}

.icon-bookings:before {
content: "\f127"
}

.icon-card-related:before {
content: "\f13e"
}

.icon-check:before {
content: "\f145"
}

.icon-close-small:before {
content: "\f14a"
}

.icon-error:before {
content: "\f177"
}

.icon-exclusive-discounts:before {
content: "\f179"
}

.icon-facebook:before {
content: "\f17d"
}

.icon-free-award-nights:before {
content: "\f18a"
}

.icon-hotel-icon:before {
content: "\f1a6"
}

.icon-in-room-welcome-gift:before {
content: "\f1ab"
}

.icon-info:before {
content: "\f1af"
}

.icon-instagram:before {
content: "\f1b0"
}

.icon-logout:before {
content: "\f1c1"
}

.icon-members-only-rate:before {
content: "\f1cd"
}

.icon-phone:before {
content: "\f1ec"
}

.icon-pinterest:before {
content: "\f1ef"
}

.icon-see-password:before {
content: "\f216"
}

.icon-skip-to-content:before {
content: "\f224"
}

.icon-twitter:before {
content: "\f243"
}

.icon-user:before {
content: "\f248"
}

.icon-youtube:before {
content: "\f260"
}

.wrapper {
position: relative
}

[class*=col-] {
float: left
}

::-moz-focus-inner {
border: 0;
padding: 0
}

input {
-webkit-appearance: none;
-moz-appearance: none
}

* {
-webkit-tap-highlight-color: transparent
}

.text-bold,
strong {
font-weight: 700
}

.text-left {
text-align: left
}

.text-center {
text-align: center
}

.text-right {
text-align: right
}

.text-underline {
text-decoration: underline
}

.card:after,
.card:before,
.clearfix:after,
.clearfix:before,
.header:after,
.header:before {
content: " "
}

.card:after,
.clearfix:after,
.header:after {
clear: both
}

.hidden.hidden {
display: none
}

.sr-only {
position: absolute;
width: 1px;
height: 1px;
padding: 0;
margin: -1px;
overflow: hidden;
clip: rect(0, 0, 0, 0);
border: 0
}

.mb-8 {
margin-bottom: 6rem
}

.position-inherit {
position: inherit
}

.img-responsive {
width: 100%;
max-width: 100%;
height: auto
}

.row [class*=col-] {
flex: 0 0 auto
}

::-webkit-scrollbar {
width: 6px;
height: 6px
}

::-webkit-scrollbar-track {
background: #eee
}

::-webkit-scrollbar-thumb {
background: #888;
opacity: .3
}

::-webkit-scrollbar-thumb:hover {
opacity: .8
}

.mb-5 {
margin-bottom: 5rem
}

.mb-25 {
margin-bottom: 25px
}

.mt-50 {
margin-top: 50px
}

.mt-5 {
margin-top: 5rem
}

.no-border {
border: 0
}

.collapse,
.lazy {
transition: all .8s ease
}

.canvas-wrap,
.navbar-toggle .hamburger-menu span {
transition: all .6s ease-out
}

.card {
box-shadow: 0 1px 4px 0 rgba(177, 179, 179, .3)
}

.off-canvas,
button.navbar-toggle {
top: 0
}

@media (min-width:992px) {
.off-canvas,
button.navbar-toggle {
display: none
}
}

.off-canvas .content-off-canvas {
padding: 2rem;
padding-right: 0
}

.off-canvas .content-off-canvas .navigation {
overflow-x: hidden;
height: calc(100vh - 9rem);
padding-right: 2rem
}

.off-canvas .content-off-canvas .header__inner {
padding-left: 0;
margin-bottom: 1.5rem
}

.off-canvas .content-off-canvas .list--inline {
width: 100%;
display: block
}

.off-canvas .content-off-canvas .list--inline li {
border-bottom: 1px solid #dedede;
padding-top: 1.7rem;
padding-bottom: 1.7rem;
width: 100%;
text-align: left
}

.off-canvas .content-off-canvas .list--inline li.reservation-link.loyalty-visibility {
display: none
}

.off-canvas .content-off-canvas .list--inline li.reservation-link.loyalty-visibility.show {
display: inline-block;
padding-bottom: 1.7rem
}

.off-canvas .content-off-canvas .list--inline li:before {
display: none
}

.off-canvas .content-off-canvas .list--inline li.dropdown-customer .btn.btn-link,
.off-canvas .content-off-canvas .list--inline li.dropdown-language .btn.btn-link {
width: 100%;
line-height: 2.2rem
}

@media (min-width:400px) {
.off-canvas .content-off-canvas .list--inline li.dropdown-customer .btn.btn-link,
.off-canvas .content-off-canvas .list--inline li.dropdown-language .btn.btn-link {
width: calc(100% - 12rem);
float: right;
text-align: right;
padding-right: .5rem
}
}

.off-canvas .content-off-canvas .list--inline li.dropdown-customer .dropdown-customer--text,
.off-canvas .content-off-canvas .list--inline li.dropdown-language .dropdown-language__text {
line-height: 2rem;
max-width: 12rem;
color: #53565a;
text-transform: uppercase;
font-size: 1.4rem
}

.off-canvas .content-off-canvas .list--inline .btn.btn-link {
color: #53565a;
font-size: 1.4rem;
text-transform: uppercase
}

.off-canvas .content-off-canvas .list--inline .btn.btn-link:hover {
text-decoration: none
}

.off-canvas .content-off-canvas .list--inline a {
color: #53565a;
text-align: right;
text-transform: uppercase
}

.off-canvas .content-off-canvas .list--inline .btn-link {
color: #53565a;
font-size: 1.4rem;
left: 0;
top: auto;
width: 100%;
text-align: left
}

.off-canvas .content-off-canvas .list--inline .btn-link .icon-arrow-down {
top: .3rem;
color: #686868;
float: right
}

@media (max-width:1199.98px) {
.off-canvas .content-off-canvas .list--inline .dropdown-menu {
background: #f3f3f2;
margin-top: 1.5rem
}
.off-canvas .content-off-canvas .list--inline .dropdown-menu:not(#selectBrand) {
position: absolute;
width: 90%;
left: 5%;
top: 3.5rem
}
}

@media (max-width:1199.98px) and (orientation:landscape) {
.off-canvas .content-off-canvas .list--inline .dropdown-menu:not(#selectBrand) {
width: 60%;
left: 20%
}
}

.d-none {
display: none
}

.d-inline-block {
display: inline-block
}

.d-block {
display: block
}

.d-inline-flex {
display: inline-flex
}

@media (min-width:768px) {
.d-md-flex {
display: flex
}
}

@media (max-width:1199.98px) {
.off-canvas .content-off-canvas .list--inline .dropdown-menu#selectBrand {
position: relative
}
.off-canvas .content-off-canvas .list--inline .dropdown-menu li {
padding-top: .5rem;
padding-bottom: .5rem;
border: 0
}
.off-canvas .content-off-canvas .list--inline .dropdown-menu li:last-child {
border-bottom: 0
}
.off-canvas .content-off-canvas .list--inline .dropdown-menu li a {
text-align: left
}
}

@media (max-width:991px) and (orientation:portrait) {
.off-canvas .content-off-canvas .blank-space {
height: 27.5rem
}
}

.navbar-toggle {
display: block;
z-index: 1039
}

.navbar-toggle[data-side=right] {
right: 3%
}

.navbar-toggle .hamburger-menu {
width: 2.5rem;
height: 2.5rem;
position: relative;
transform: rotate(0);
transition: .5s ease-in-out;
cursor: pointer
}

.navbar-toggle .hamburger-menu span {
border-radius: 0;
width: 2.5rem;
height: 3px;
display: block;
position: absolute;
background: #53565a;
opacity: 1;
left: 0;
transform: rotate(0);
transition: .25s ease-in-out
}

.navbar-toggle .hamburger-menu span:nth-child(1) {
top: 0
}

.navbar-toggle .hamburger-menu span:nth-child(2) {
top: .9rem
}

.navbar-toggle .hamburger-menu span:nth-child(3) {
top: 1.8rem
}

.out .hamburger-menu span:nth-child(1) {
top: .8rem;
left: -5px;
transform: rotate(135deg)
}

.out .hamburger-menu span {
background: #fff
}

.out .hamburger-menu span:nth-child(2) {
opacity: 0;
left: -1rem
}

.out .hamburger-menu span:nth-child(3) {
top: .8rem;
left: -.5rem;
transform: rotate(-135deg);
opacity: 1
}

.out .hamburger-menu span {
width: 2.5rem;
height: .2rem;
border-radius: 0
}

.off-canvas {
width: 25rem;
width: 80%;
height: 100%;
left: -80%;
position: fixed;
z-index: 1039;
overflow: auto;
-webkit-overflow-scrolling: touch
}

@media (max-width:767.98px) {
.off-canvas {
width: 80%;
left: -80%
}
}

.off-canvas[data-side=right] {
left: auto;
right: -80%
}

@media (max-width:767.98px) {
.off-canvas[data-side=right] {
width: 80%;
left: auto;
right: -80%
}
}

.canvas-wrap {
opacity: 0;
top: 0;
left: 0;
pointer-events: none;
background: rgba(0, 0, 0, .8);
z-index: 1038;
display: block;
position: fixed;
width: 100vw;
height: 100vh
}

.slick-slider--banner {
display: block;
width: 100%
}

.slick-slider .slick-dots {
position: relative;
display: block;
width: 100%;
padding: 0;
list-style: none;
text-align: center;
margin-bottom: 2rem
}

.slick-slider .slick-dots li {
position: relative;
display: inline-block;
margin: 0 5px;
padding: 0;
cursor: pointer
}

.slick-slider .slick-dots li button {
font-size: 0;
line-height: 0;
display: block;
width: 7.5px;
height: 7.5px;
padding: 0;
cursor: pointer;
color: transparent;
border: 0;
outline: 0;
border-radius: 100%;
opacity: 1;
background: #ccc;
width: 7.5px;
height: 7.5px;
padding: 0
}

.slick-slider .slick-dots li.slick-active {
opacity: 1
}

.slick-slider .slick-dots li.slick-active button {
opacity: 1;
background: #c20808;
width: 10px;
height: 10px
}

::-moz-selection {
color: #fff;
background: #979899
}

::selection {
color: #fff;
background: #979899
}

body {
margin: 0;
font-size: 1.4rem;
line-height: 2rem;
font-family: Gotham;
color: #202020
}

h3 {
font-size: inherit;
margin: 0;
width: auto;
color: inherit
}

.h2,
.h3,
.h5 {
color: inherit;
display: block;
font-size: inherit;
font-weight: 300;
margin: 0;
text-decoration: none;
width: auto
}

a {
color: #c20808
}

a:hover {
color: #c20808;
text-decoration: none
}

.text-12 {
font-size: 1.2rem
}

.form-group .form-control,
.text-14 {
font-size: 1.4rem
}

p {
font-size: 1.4rem;
line-height: 2rem
}

.text-grey {
color: #979899
}

.title-section {
font-size: 2.2rem;
line-height: 2.6rem;
margin-bottom: 2.2rem;
font-weight: 700;
color: #53565a
}

.title-section--center {
font-size: 2.8rem;
font-weight: 400;
display: inline-block;
position: relative;
margin-bottom: 4rem
}

.title-section--center::before {
content: "";
display: block;
background: #202020;
height: 1px;
width: 35%;
position: absolute;
display: block;
margin: auto;
bottom: -70%;
left: 0;
right: 0
}

i {
font-size: 1.2rem;
line-height: 1.6rem;
margin-bottom: 15px;
font-style: italic;
font-weight: 400
}

p {
font-weight: 400;
font-size: 1.4rem;
line-height: 2rem;
margin-bottom: 15px
}

.h3 {
font-weight: 700;
font-size: 2rem
}

strong {
font-weight: 700
}

.font-bold {
font-weight: 700
}

.font-medium {
font-weight: 300
}

.font-light {
font-weight: 100
}

i[class*=icon-] {
background-size: contain;
vertical-align: middle;
width: 48px;
height: 48px;
font-size: 48px;
display: block;
position: relative;
margin: 0
}

i[class*=icon-]::before {
vertical-align: baseline
}

i[class*=icon-].xxs {
display: inline-block;
width: 8px;
height: 15px;
font-size: 8px
}

i[class*=icon-].xs {
display: inline-block;
width: 16px;
height: 17px;
font-size: 16px
}

i[class*=icon-].sm {
display: inline-block;
width: 24px;
height: 24px;
font-size: 24px
}

i[class*=icon-].md {
display: inline-block;
width: 40px;
height: 40px;
font-size: 40px
}

i[class*=icon-].md:after {
font-size: 2.4rem
}

i[class*=icon-].lg {
display: inline-block;
width: 48px;
height: 48px;
font-size: 48px
}

button:focus {
box-shadow: 0 0 0 3px rgba(194, 8, 8, .25)
}

[role=button] {
cursor: pointer
}

[data-toggle=modal] {
cursor: pointer
}

.btn {
transition: all .3s ease;
font-size: 1.4rem;
font-weight: 400;
border-radius: .4rem;
white-space: inherit
}

.btn+.btn {
margin-left: 15px
}

.btn [class*=icon-] {
vertical-align: middle;
margin-right: .5rem
}

.btn:disabled:not(.m-progress) {
opacity: .55
}

.btn:focus {
outline-width: .1rem;
outline-style: dotted
}

.btn.btn-primary {
padding: 2rem 2.5rem;
line-height: 2rem;
background-color: #c20808;
border: none
}

.btn.btn-primary:disabled:not(.m-progress) {
background-color: #686868
}

.btn.btn-primary:focus {
box-shadow: 0 0 0 3px rgba(194, 8, 8, .25)
}

.btn.btn-secondary,
.btn.btn-secondary_transparent {
padding: 2rem 2.5rem;
line-height: 2rem;
background-color: #fff;
color: #c20808;
border-color: #c20808;
border: 1px solid
}

.btn.btn-secondary_transparent {
background-color: transparent;
border: 1px solid #c20808
}

.btn.btn-secondary:disabled:hover:not(.m-progress),
.btn.btn-secondary_transparent:disabled:hover:not(.m-progress) {
background-color: #fff;
color: #c20808;
border-color: #c20808
}

.btn.btn-secondary:focus,
.btn.btn-secondary_transparent:focus {
box-shadow: 0 0 0 3px rgba(194, 8, 8, .25)
}

.btn.btn-transparent {
padding: 2rem 2.5rem;
line-height: 2rem;
background-color: transparent;
color: #fff;
border-color: #fff;
border: 1px solid
}

.btn.btn-transparent:disabled:hover:not(.m-progress) {
background-color: transparent;
color: #fff;
border-color: #fff
}

.btn[class*=btn-link] {
background-color: transparent;
padding: 0;
text-decoration: none;
font-weight: 300;
border: 0;
padding: 0;
position: relative;
text-align: justify
}

.btn[class*=btn-link] i {
transition: .5s;
transform: rotate(0)
}

.btn[class*=btn-link].text-underline {
text-decoration: underline
}

.btn.btn-link {
color: #c20808
}

.btn.btn-link--inline {
font-size: inherit;
font-weight: 300;
padding: 0;
line-height: inherit;
vertical-align: inherit;
margin: 0
}

.btn.btn-link--color {
font-weight: inherit
}

.btn.btn-pill {
background: #fff
}

.btn.btn-pill--transparent {
background: 0 0
}

.btn.btn-sm {
font-size: 1.2rem;
padding: 1rem 1.5rem;
line-height: 1.2rem;
font-weight: 300
}

.btn-icon-link {
color: #000
}

.btn-icon-link:hover {
color: #741619
}

.card {
height: 100%;
border-radius: .4rem;
border: 1px solid rgba(0, 0, 0, .08)
}

.card__body {
width: 100%;
max-width: 90%;
display: block;
margin: auto;
flex: 1 1 auto;
padding: 1.25rem
}

@media (max-width:767.98px) {
.card__body {
height: auto
}
}

.card__title {
color: rgba(0, 0, 0, .7);
font-size: 1.8rem;
font-weight: 700;
line-height: 2.2rem;
margin-bottom: 10px
}

.card__text {
color: #686868;
font-size: 1.4rem;
line-height: 2rem;
margin-bottom: 25px
}

.card__text+.btn {
margin-bottom: 4rem
}

.card>[class*=icon-] {
font-size: 60px;
width: 60px;
height: 60px;
display: block;
margin: auto;
margin-bottom: 20px;
margin-top: 35px
}

.dropdown .dropdown-menu {
box-shadow: 0 12px 12px 0 rgba(0, 0, 0, .12);
background: #fff;
margin: 0;
padding: 0;
z-index: 2
}

.dropdown .dropdown-menu .dropdown-item {
font-size: 1.2rem;
line-height: 1.3rem;
padding: 1.5rem;
border-top: 1px solid #d8d8d8;
white-space: normal
}

.dropdown .dropdown-menu .dropdown-item:first-child {
border-top: none
}

.dropdown .dropdown-menu .dropdown-item:active {
font-weight: 700;
background: #f3f3f2;
color: #000;
opacity: 1
}

.dropdown .dropdown-menu-icon {
max-height: 210px;
overflow-y: auto
}

.list li:last-child {
margin-bottom: 0
}

.list--inline {
display: inline-block
}

.list--inline li {
display: inline-block;
margin-bottom: 0
}

.list--inline li:before {
content: " | ";
color: #979797
}

.list--inline li:first-child:before {
content: "";
display: none
}

.list--nav li {
list-style: none;
border-bottom: 1px solid #979797
}

.list--nav li .nav-item {
width: 100%;
padding: 2rem 3rem;
font-size: 1.4rem;
display: block
}

.list--nav li .nav-item i {
margin-right: 1.5rem
}

.custom-control {
cursor: pointer
}

.custom-control .custom-control-input {
-webkit-appearance: checkbox;
-moz-appearance: checkbox;
margin-right: 5px;
background-color: #fff
}

.custom-control .custom-control-input:checked~.custom-control-indicator i {
opacity: 1
}

.custom-control .custom-control-input:disabled~.custom-control-indicator {
opacity: .55
}

.custom-control .custom-control-input:focus~.custom-control-indicator {
outline-color: #c20808;
outline-width: .1rem;
outline-style: dotted
}

.custom-control .custom-control-indicator {
display: flex;
justify-content: center;
align-items: center;
position: absolute;
top: 0;
left: 0;
width: 16px;
height: 16px;
border: 1px solid #c1c1c1;
background: #fff
}

.custom-control .custom-control-indicator i {
display: flex;
justify-content: center;
align-items: center;
opacity: 0;
transition: .2s;
color: #53565a
}

.custom-control .custom-control-indicator i::before {
font-size: 78%
}

form:not(.form-inline) .label-up {
white-space: nowrap
}

form .form__title {
font-size: 1.8rem;
line-height: 2.2rem;
margin-bottom: 2.2rem;
font-weight: 100
}

form .icon-info--form {
margin: .5rem 0 0 .5rem;
color: #53565a
}

form .form-group-with-icon {
display: flex
}

form .form-group-with-icon .form-group {
flex-grow: 1
}

.form-control-icon {
display: inline-block;
border: 1px solid transparent;
background-color: transparent;
vertical-align: middle;
outline: 0;
z-index: 1;
pointer-events: auto;
cursor: pointer;
position: absolute;
right: 0;
top: 0;
padding: .2rem 0 .2rem .5rem
}

.form-control-icon i {
color: #6c6c6c
}

::-ms-clear {
display: none
}

.form-bg-dark {
color: #fff
}

.form-bg-dark .checkbox+p,
.form-bg-dark input[type=checkbox]~span {
color: #fff
}

.form-bg-dark label.label-up {
color: #dfdfdf
}

.form-bg-dark .form__title {
color: #fff
}

.form-bg-dark .form-control-icon i {
color: #fff
}

.form-bg-dark .icon-info--form {
color: #fff
}

.form-bg-dark .form-group .form-control:focus {
color: #fff
}

.form-bg-dark .form-group .form-control:focus~.label-up {
color: #dfdfdf
}

.form-bg-dark .form-group .form-control:focus {
border-bottom: 1px solid #fff
}

.form-group {
padding: 0;
margin-bottom: 2.8rem;
position: relative
}

.form-group .custom-control {
position: relative;
pointer-events: auto;
width: 100%
}

.form-group .form-control {
border: 0;
border-bottom: 1px solid #dedede;
width: 100%;
padding: .375rem .75rem .375rem 0;
border-radius: 0;
min-height: 28px;
background-color: transparent
}

.form-group .form-control:focus {
border-bottom: 1px solid #dedede;
color: #000;
-webkit-box-shadow: none;
box-shadow: none
}

.form-group .form-control:focus~.label-up {
top: -15px;
font-size: 1.2rem;
color: #53565a
}

.form-group .form-control:focus {
border-bottom: 1px solid #000
}

.form-group .form-control::-moz-placeholder {
color: transparent
}

.form-group .form-control:-ms-input-placeholder {
color: transparent
}

.form-group .form-control:focus::-moz-placeholder {
color: #6c6c6c
}

.form-group .form-control:disabled {
opacity: 1;
color: #6c6c6c;
cursor: not-allowed
}

.form-group .form-control:disabled~label {
color: #6c6c6c
}

@media (max-width:767.98px) {
.form-group .form-control-icon {
right: 0
}
}

.form-group label {
display: block;
color: #53565a;
line-height: 1.8rem;
font-size: 1.2rem
}

.form-group .label-up {
font-size: 1.4rem;
margin-bottom: 5px;
transition: .2s ease all;
-moz-transition: .2s ease all;
-webkit-transition: .2s ease all;
position: absolute;
pointer-events: none;
left: 0;
top: 6px;
overflow-x: hidden;
text-overflow: ellipsis;
width: 100%
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.form-group .label-up {
top: -15px;
font-size: 1.2rem;
color: #53565a
}
.form-group .form-control:-ms-input-placeholder {
text-indent: inherit;
color: #6c6c6c
}
}

input[type=text]::-ms-clear {
display: none;
width: 0;
height: 0
}

input[type=text]::-ms-reveal {
display: none;
width: 0;
height: 0
}

.collapse {
display: block;
max-height: 0;
overflow: hidden
}

.modal {
z-index: 1041
}

.modal .modal-header {
border: none;
min-height: 50px;
align-items: center;
padding-left: 0;
padding-right: 0
}

.modal .close {
opacity: 1;
position: absolute;
top: 7px;
right: 0;
width: 20px;
height: 25px;
margin: 0;
padding: 5px 0 5px 0
}

.modal .close span {
background: #fff;
display: block;
width: 20px;
height: 1px;
transform: rotate(45deg);
position: relative
}

.modal .close span+span {
transform: rotate(-45deg);
top: -1px
}

.modal .modal-title {
color: #fff;
font-size: 2rem
}

.modal .modal-content {
background: 0 0;
border: 0
}

.modal .modal-body {
padding: 0
}

.modal .modal-body--form {
background: #fff;
position: relative;
border-radius: .4rem;
padding: 7rem 5rem
}

.modal .modal-body--alert {
background: #fff;
position: relative;
border-radius: .4rem;
padding: 5rem 4rem;
text-align: center;
color: #53565a
}

.modal .modal-body--alert .modal-body__icon {
margin-bottom: 3rem
}

.modal .modal-body--alert .modal-body__icon.lg {
width: 65px;
height: 65px;
font-size: 65px;
margin-bottom: 4rem
}

.modal .modal-body--alert .modal-body__title {
font-size: 18px;
font-weight: 700;
margin-bottom: 1rem;
display: block
}

.modal .modal-body--alert .modal-body__subtitle {
font-size: 18px;
font-weight: 100;
display: block;
margin-bottom: 4rem
}

@media (min-width:992px) {
.modal .modal-body--alert .modal-body__subtitle {
width: 70%;
margin-left: auto;
margin-right: auto
}
}

.entity-modal .modal-body {
height: auto;
max-height: 88vh;
overflow-y: auto;
overflow-x: hidden
}

.lazy {
opacity: 0
}

.header {
width: 100%;
z-index: 600;
position: relative;
top: 0;
font-size: 1.4rem;
background: rgba(65, 65, 65, .6);
transition: all .6s ease-out
}

.header a.skip-main {
background: #53565a;
color: #fff;
width: 0;
height: 0;
position: absolute;
top: auto;
overflow: hidden;
padding: 0;
z-index: -999
}

.header a.skip-main:active,
.header a.skip-main:focus {
top: 10px;
left: 10px;
width: auto;
height: auto;
overflow: auto;
padding: 10px;
z-index: 999
}

.header__body {
height: 80px
}

@media (max-width:991.98px) {
.header__body {
height: 60px
}
}

.header__body button.btn.btn-link:active {
color: #fff
}

.header__body .btn.btn-link,
.header__body .btn.btn-link:active,
.header__body .btn.btn-link:focus,
.header__body .btn.btn-link:hover,
.header__body .dropdown-customer--text,
.header__body a,
.header__body a:hover,
.header__body a:link {
color: #fff;
font-size: 1.2rem;
text-transform: uppercase;
font-weight: 500
}

.header__body .btn.btn-link:active:hover,
.header__body .btn.btn-link:focus:hover,
.header__body .btn.btn-link:hover,
.header__body .btn.btn-link:hover:hover,
.header__body .dropdown-customer--text:hover,
.header__body a:hover,
.header__body a:hover:hover,
.header__body a:link:hover {
color: #fff;
text-decoration: none
}

.header__body a.dropdown-item,
.header__body a:hover.dropdown-item,
.header__body a:link.dropdown-item {
color: #686868;
line-height: 2rem
}

.header__body a.dropdown-item:active,
.header__body a:hover.dropdown-item:active,
.header__body a:link.dropdown-item:active {
background: #fae5e8;
font-weight: 700
}

.header__body .btn.btn-link:active:focus,
.header__body .btn.btn-link:focus,
.header__body .btn.btn-link:focus:focus,
.header__body .btn.btn-link:hover:focus,
.header__body .dropdown-customer--text:focus,
.header__body a:focus,
.header__body a:hover:focus,
.header__body a:link:focus {
outline-color: #fff;
outline-width: .1rem;
outline-style: dotted
}

@media (min-width:992px) {
.header__body .dropdown-language a.dropdown-item {
color: #53565a;
line-height: 3em;
border-top: 1px solid #b1b3b3
}
.header__body .dropdown-language .dropdown-menu {
padding: 0
}
.header__body .dropdown-language [aria-label=language] a.dropdown-item {
line-height: 1.4em
}
}

.header .container-fluid {
max-width: 1440px
}

@media (min-width:992px) {
.header .container-fluid>.row {
margin-left: 1.5rem;
margin-right: 1.5rem
}
}

@media (max-width:991.98px) {
.header {
background: #fff
}
}

.header__logo {
max-width: 150px;
padding: 1rem 0
}

@media (max-width:991.98px) {
.header__logo {
height: 50px
}
}

.header:before {
content: "";
position: absolute;
width: 100%;
height: 30px;
right: 0;
top: 0;
background: -moz-linear-gradient(left, rgba(76, 76, 76, 0) 0, #282828 100%);
background: -webkit-linear-gradient(left, rgba(76, 76, 76, 0) 0, #282828 100%);
background: linear-gradient(to right, rgba(76, 76, 76, 0) 0, #282828 100%)
}

.header .customer-navigation {
position: relative
}

@media (min-width:768px) {
.header .customer-navigation {
padding-bottom: 1.7rem
}
}

.header .customer-navigation li {
margin-right: 1.5rem
}

.header .customer-navigation li.dropdown-language {
margin-right: 0
}

.header .customer-navigation li.reservation-link.loyalty-visibility {
display: none
}

.header .gradient-nav {
height: 40px;
position: absolute;
right: 0;
width: 100%;
top: 0;
overflow: hidden
}

.header .gradient-nav:after {
position: absolute;
top: 0;
z-index: -1;
right: -80px;
height: 42px;
width: 100%;
max-width: 800px;
content: "";
display: block;
opacity: .7;
background-image: linear-gradient(to right, rgba(0, 0, 0, 0) 0, #000 100%);
background-repeat: repeat-x
}

.header .gradient-nav:before {
position: absolute;
top: 0;
z-index: -1;
content: "";
display: block;
opacity: .7;
background-color: #000;
right: -980px;
height: 42px;
width: 900px
}

.header:before {
height: 0
}

.header .list--inline li::before {
display: none
}

.header__inner {
display: flex;
padding-right: 3em
}

@media (max-width:991.98px) {
.header__inner {
padding-right: 0
}
}

@media (max-width:991.98px) {
.customer-navigation .dropdown-menu {
border-top: 2px solid #686868
}
}

@media (max-width:991.98px) {
.hotel-navigation .btn.btn-link {
color: #686868;
font-weight: 400
}
.hotel-navigation .btn.btn-link:active,
.hotel-navigation .btn.btn-link:focus {
color: #686868;
outline: 0
}
}

.hotel-navigation li {
margin-right: 1.5rem
}

@media (max-width:991.98px) {
.hotel-navigation li {
margin-right: 0
}
}

.hotel-navigation li a {
text-transform: uppercase
}

.hotel-navigation li:last-child {
margin-right: 0
}

.hotel-navigation li:last-child a {
padding-right: 0
}

.hotel-navigation .dropdown-menu {
width: 100%;
padding-top: 4rem;
padding-bottom: 4rem;
margin-top: 12px;
opacity: 0;
display: flex;
pointer-events: none;
transform: translate3d(0, 69px, 0);
top: 0;
border-top: 2px solid #686868
}

@media (max-width:1199.98px) {
.hotel-navigation .dropdown-menu {
padding-top: 2rem;
padding-bottom: 2rem
}
}

.hotel-navigation .dropdown-menu ul {
max-width: 1199px;
display: flex;
align-items: center;
justify-content: space-around
}

@media (max-width:1199.98px) {
.hotel-navigation .dropdown-menu ul {
max-width: 990px
}
}

.hotel-navigation .dropdown-menu ul li {
display: block;
width: 100%;
min-width: 150px;
text-align: center;
margin: 0
}

@media (min-width:992px) {
.hotel-navigation .dropdown-menu ul li {
min-width: 130px
}
}

@media (max-width:1199.98px) {
.hotel-navigation .dropdown-menu ul li {
min-width: 110px
}
}

.hotel-navigation .dropdown-menu ul li a {
display: block;
width: 100%;
padding: 0
}

.hotel-navigation .dropdown-menu ul li a:focus {
border: 1px solid red
}

.card.card__featured-icon {
border: none;
box-shadow: none
}

@media (min-width:992px) {
.card.card__featured-icon::before {
content: "";
display: block;
position: absolute;
left: 0;
top: 0;
bottom: 0;
border-right: 1px solid #d8d8d8;
height: 60%;
margin: auto
}
}

.card.card__featured-icon.no-border:before {
border: none
}

.card.card__featured-icon .card__title {
font-weight: 400
}

.card.card__featured-icon .card__icon {
color: #53565a
}

.entity-hero {
position: relative;
height: auto;
z-index: 101
}

.entity-hero:after,
.entity-hero:before {
content: " "
}

.entity-hero:after {
clear: both
}

.logo-rewards {
background: url(https://roberto.dev/img/logos/radisson-rewards.svg) no-repeat center center;
background-size: contain
}

.banner-app {
position: relative;
width: 100%;
left: 0;
top: 0;
z-index: 900;
background: #f3f3f2
}

.banner-app .radisson-logo {
padding: 5px;
background: #fff;
display: inline-block;
border-radius: 5px;
width: 35px;
height: 35px
}

@media (max-width:350px) {
.banner-app .caption {
max-width: 100px
}
}

.banner-app .caption p {
margin-bottom: 0;
line-height: 1.6rem
}

.banner-app .btn-transparent {
color: #53565a
}

.banner-app .close {
opacity: 1;
top: 1.5rem;
left: .5rem;
width: 30px;
height: 30px;
margin: 0;
padding: 10px 0 5px 0
}

.banner-app .close span {
background: #53565a;
display: block;
width: 20px;
height: 2px;
transform: rotate(45deg);
position: relative
}

.banner-app .close span+span {
transform: rotate(-45deg);
top: -2px
}

.banner-app .close span.sr-only {
background: 0 0
}

.loyalty-visibility {
opacity: 0;
display: none;
transition: all .6s ease-out
}

.loyalty-visibility.show {
opacity: 1;
display: block
}

.entity-loyalty .logo-rewards {
display: block;
margin: 0 auto 20px;
width: 150px;
height: 70px;
position: relative;
margin-top: 30px;
background: 0 0
}

@media (max-width:991.98px) {
.entity-loyalty .logo-rewards {
margin-top: 20px
}
}

.entity-loyalty .logo-rewards::after {
content: "";
border-bottom: 1px solid #000;
bottom: 0;
display: block;
position: absolute;
left: 45px;
width: 60px
}

.entity-loyalty .loyalty__title {
font-size: 1.8rem;
text-align: center;
color: #000;
max-width: 150px;
display: block;
margin: auto;
margin-bottom: 60px
}

.entity-loyalty .loyalty__subtitle {
text-align: center;
color: #686868;
max-width: 610px;
display: block;
margin: auto
}

.entity-loyalty .cards {
display: block;
max-width: 800px;
width: 100%;
display: flex;
margin: auto;
position: relative;
justify-content: center;
padding-bottom: 40px
}

@media (min-width:992px) {
.entity-loyalty .cards {
min-height: 270px
}
}

@media (max-width:991.98px) {
.entity-loyalty .cards {
min-height: 240px
}
}

@media (max-width:767.98px) {
.entity-loyalty .cards {
min-height: 170px
}
}

.entity-loyalty .btn {
padding: 1.5rem 5.8rem;
margin: 20px auto;
text-transform: uppercase
}

@media (min-width:992px) {
.entity-loyalty {
display: block
}
}

.entity-slider-special {
position: relative
}

.entity-slider-special .slider-nav {
margin-top: -70px
}

.entity-slider-special .slider-nav .slick-dots {
top: -30px
}

@media (max-width:991.98px) {
.entity-slider-special .slider-nav .slick-dots {
top: 70px
}
}

.hero {
position: relative;
width: 100%;
max-height: 770px;
height: 100vh;
transition: all 1.2s ease;
overflow: hidden
}

@media (min-width:992px) {
.hero {
margin-top: -130px
}
}

@media (max-width:1199.98px) {
.hero {
max-height: 450px
}
}

.hero--slider {
position: relative;
margin-top: -130px;
width: 100%;
max-height: 770px;
height: 100vh;
opacity: 0;
overflow: hidden;
visibility: hidden;
transition: all 1.2s ease
}

@media (max-width:991.98px) {
.hero--slider {
margin-top: -95px
}
}

@media (max-width:767.98px) {
.hero--slider {
max-height: 450px
}
}

.wrap-loyalty-bars {
height: 50px
}

@media (max-width:991.98px) {
.wrap-loyalty-bars {
height: 35px
}
}

.loyalty-bar {
background: #53565a;
font-size: 1.4rem;
font-weight: 100;
position: relative;
align-items: center;
line-height: 1.4rem;
opacity: 0;
transition: all .5s ease;
display: flex;
height: 0;
overflow: hidden
}

.loyalty-bar__logo {
width: 77px;
height: 24px
}

.loyalty-bar.show {
opacity: .95;
visibility: visible;
height: 50px;
overflow: visible
}

.loyalty-bar .btn {
font-size: inherit
}

@media (max-width:991.98px) {
.loyalty-bar {
font-size: 1.2rem
}
.loyalty-bar.show {
opacity: 1;
height: 35px
}
}

.loyalty-bar__body {
display: flex;
align-items: center;
flex-direction: row;
justify-content: space-between;
width: 100%
}

.loyalty-bar__body a {
color: #fff
}

.loyalty-bar__menu-left {
color: #fff
}

.loyalty-bar__menu-right {
display: flex;
align-items: center;
justify-content: flex-end
}

@media (max-width:991.98px) {
.loyalty-bar__menu-right {
position: inherit
}
}

.loyalty-bar__menu-right .btn-link {
color: #fff;
font-size: 1.1rem
}

.loyalty-bar__menu-right .btn-link:not(.btn-link--inline) {
padding: .8rem
}

.loyalty-bar__menu-right .menu-item>.btn-link {
padding: .8rem
}

.loyalty-bar__menu-right .menu-item>.btn-link::before {
content: "·";
display: inline-block;
padding-right: 1.5rem
}

.loyalty-bar__menu-right .menu-item:first-child a::before {
display: none
}

.loyalty-bar .dropdown-loyalty .dropdown-menu {
width: 320px;
background: #45494d;
opacity: 1;
padding: 2.5rem;
border-radius: 4px;
border-top-left-radius: 0;
border-top-right-radius: 0;
margin-top: 7px;
font-size: 1.2rem
}

.loyalty-bar .dropdown-loyalty.sm {
position: inherit
}

.loyalty-bar .dropdown-loyalty.sm .collapse-menu .collapse__body {
padding: 2.5rem
}

.loyalty-bar--logged {
box-shadow: 0 3px 0
}

.loyalty-bar--logged .user-information .tier::before {
content: "";
margin-right: .6rem;
background: #000;
width: 7px;
height: 7px;
display: inline-block;
border-radius: 100%
}

.loyalty-bar--logged .user-information .points::before {
content: "\2192";
display: inline-block;
padding-left: .5rem;
padding-right: .5rem;
opacity: .6
}

.loyalty-bar--logged .loyalty-bar__menu-right .menu-item .btn-link {
font-size: 1.2rem;
font-weight: 400
}

.loyalty-bar--logged .dropdown-loyalty.sm .collapse-menu .collapse__body {
padding: 3rem
}

.loyalty-bar--logged .dropdown-loyalty.sm .btn-logout {
bottom: 3rem;
left: 3rem
}

footer {
background-color: #f5f5f5;
font-size: 1.2rem;
overflow: hidden
}

footer .copyright {
background-color: #f3f3f3;
padding: .7rem 0;
text-align: center;
border-top: 1px solid #e3e3e3
}

footer .entity-footer-destinations .accordion,
footer .footer-links .accordion {
padding: 15px 0;
display: flex;
justify-content: space-between;
align-items: center;
position: relative
}

@media (min-width:992px) {
footer .entity-footer-destinations .accordion,
footer .footer-links .accordion {
padding-top: 40px;
border-top: none
}
}

footer .entity-footer-destinations .accordion:hover,
footer .footer-links .accordion:hover {
cursor: pointer
}

@media (min-width:992px) {
footer .entity-footer-destinations .accordion:hover,
footer .footer-links .accordion:hover {
cursor: inherit
}
}

@media (min-width:992px) {
footer .entity-footer-destinations .accordion .icon-arrow-down,
footer .footer-links .accordion .icon-arrow-down {
display: none
}
}

footer .entity-footer-destinations .collapse,
footer .footer-links .collapse {
padding: 0 0 4rem
}

@media (min-width:992px) {
footer .entity-footer-destinations .collapse,
footer .footer-links .collapse {
display: block;
overflow: visible;
max-height: inherit;
transition: 0s
}
}

.entity-footer-contact {
background-color: #f3f3f3;
text-align: center
}

.entity-footer-contact .contact-us {
padding: 3rem 0
}

.entity-footer-contact .contact-us:before {
content: "";
background: #d8d8d8;
position: absolute;
top: 0;
width: 50px;
height: 1px;
left: calc(50% - 25px)
}

.entity-footer-contact .contact-us:first-child:before {
content: none
}

@media (min-width:992px) {
.entity-footer-contact .contact-us {
padding: 4rem 0
}
.entity-footer-contact .contact-us:before {
top: calc(50% - 38px);
width: 1px;
height: 76px;
left: 0
}
}

.entity-footer-contact .contact-us__title {
font-size: 1.8rem;
color: #000;
margin: 0 auto 1rem
}

@media (min-width:992px) {
.entity-footer-contact .contact-us__title {
margin-bottom: 2rem;
max-width: 200px
}
}

.entity-footer-contact .contact-us__social {
display: flex;
justify-content: center
}

.entity-footer-contact .contact-us__social--network {
align-content: center;
justify-content: center;
padding: 5px
}

.entity-footer-contact .contact-us__email {
display: flex;
justify-content: center;
padding: 5px;
align-items: baseline
}

.entity-footer-contact .contact-us__email .form-control {
width: 170px;
margin: 0 8px 0 0;
background: #f3f3f3
}

.entity-footer-contact .contact-us__email .label-up {
text-align: left
}

.entity-footer-contact .contact-us__email .help-block {
text-align: left
}

.entity-footer-destinations {
background-color: #fff;
margin-top: 2px;
border-top: 1px solid #e3e3e3
}

.entity-footer-destinations .hot-destinations__title {
font-size: 1rem;
text-transform: uppercase;
color: #152935;
font-weight: 700
}

.entity-footer-destinations .hot-destinations__tags {
margin: 0 -10px;
overflow: hidden;
flex-wrap: wrap;
justify-content: start;
display: flex
}

.entity-footer-destinations .tag {
border-color: #b1b3b3;
color: #b1b3b3;
margin: 5px 10px
}

.entity-footer-destinations .tag:hover {
border-color: #c20808;
color: #c20808
}

@media (min-width:992px) {
.footer-links {
border-top: 1px solid #b1b3b3
}
}

.footer-links__title {
font-size: 1rem;
text-transform: uppercase;
color: #152935;
font-weight: 700
}

.footer-links__items {
color: #979899
}

.footer-links .item-link {
line-height: 2.2rem
}

.footer-links .item-link:hover {
text-decoration: underline
}

@media (min-width:992px) {
.footer-links .col-lg-2 {
max-width: 20%
}
}

.footer-links .accordion:before {
content: "";
width: 100%;
height: 1px;
background-color: #b1b3b3;
position: absolute;
top: 0
}

.entity-footer-logos {
background-color: #fff;
border-top: 1px solid #b1b3b3
}

.entity-footer-logos .footer-logos-container {
width: 100%;
position: relative;
max-width: 850px;
margin: auto;
padding-top: 35px;
padding-bottom: 35px
}

.entity-footer-logos .footer-logos-container:after {
content: "";
width: 1px;
height: 117px;
background-color: #b1b3b3;
position: absolute;
left: 26%;
top: 0;
bottom: 0;
display: block;
margin: auto
}

@media (max-width:991.98px) {
.entity-footer-logos .footer-logos-container:after {
display: none
}
}

@media (max-width:991.98px) {
.entity-footer-logos .logo-container:before {
content: "";
position: absolute;
width: calc(100% - 30px);
height: 1px;
left: 15px;
background: #b1b3b3;
bottom: 0
}
}

@media (min-width:992px) {
.entity-footer-logos .logo-container {
border: 0
}
}

.entity-footer-logos .main-logo {
max-width: 150px
}

@media (max-width:991.98px) {
.entity-footer-logos .main-logo {
max-width: 217px;
width: 100%;
margin-bottom: 49px;
margin-top: 49px
}
}

.entity-footer-logos .list-logos-radisson {
position: relative
}

@media (max-width:991.98px) {
.entity-footer-logos .list-logos-radisson__hotelscom {
margin-top: 15px
}
}

.entity-footer-logos .logo-brand-footer.rco {
width: 61px;
margin-right: 17px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rco {
height: 39px
}
}

.entity-footer-logos .logo-brand-footer.rdb {
width: 85px;
margin-right: 17px
}

@media (min-width:992px) {
.entity-footer-logos .logo-brand-footer.rdb {
margin-top: 4px
}
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rdb {
height: 26px
}
}

.entity-footer-logos .logo-brand-footer.rad {
width: 67px;
margin-right: 19px
}

@media (min-width:992px) {
.entity-footer-logos .logo-brand-footer.rad {
margin-top: 2px
}
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rad {
height: 26px
}
}

.entity-footer-logos .logo-brand-footer.rdr {
width: 39px;
margin-right: 22px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rdr {
height: 39px
}
}

.entity-footer-logos .logo-brand-footer.pph {
width: 40px;
margin-right: 20px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.pph {
height: 43px
}
}

.entity-footer-logos .logo-brand-footer.pii {
width: 62px;
margin-right: 20px
}

@media (min-width:992px) {
.entity-footer-logos .logo-brand-footer.pii {
margin-top: 8px
}
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.pii {
height: 22px
}
}

.entity-footer-logos .logo-brand-footer.cis {
width: 76px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.cis {
height: 38px
}
}

@media (max-width:991.98px) {
.entity-footer-logos .logo-brand-footer {
margin: 4rem
}
.entity-footer-logos .logo-brand-footer.rco {
width: 100%;
max-width: 121px;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.rdb {
max-width: 155px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.rad {
max-width: 124px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.rdr {
max-width: 75px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.pph {
width: 100%;
max-width: 83px;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.pii {
max-width: 114px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.cis {
max-width: 159px;
width: 100%;
margin-left: 0;
margin-right: 0
}
}

.list-logos-brands {
display: inline-block;
text-align: center;
margin-top: 30px
}

@media (min-width:992px) {
.list-logos-brands {
display: flex;
justify-content: center;
align-items: center
}
.list-logos-brands li {
display: inline-block;
vertical-align: middle;
margin-bottom: 18px
}
}

@media (max-width:991.98px) {
.list-logos-brands {
display: flex;
align-items: center;
flex-wrap: wrap
}
.list-logos-brands li {
width: 50%;
text-align: center
}
.list-logos-brands li:last-child {
width: 100%
}
}

.logos-rewards-container {
display: flex;
justify-content: space-around
}

@media (min-width:992px) {
.logos-rewards-container {
justify-content: center
}
}

@media (max-width:991.98px) {
.list-logos-radisson {
padding-top: 32px
}
.list-logos-radisson:before {
content: "";
position: absolute;
width: calc(100% - 30px);
height: 1px;
left: 15px;
background: #b1b3b3;
top: 0
}
}

@media (max-width:991.98px) {
.list-logos-radisson [class*=list-logos-radisson] {
display: inline-block;
text-align: center;
width: 50%;
margin-bottom: 4rem
}
}

.list-logos-radisson [class*=list-logos-radisson] {
position: relative;
height: 38px;
margin-bottom: 56px
}

@media (min-width:992px) {
.list-logos-radisson [class*=list-logos-radisson]:before {
position: absolute;
width: 110%;
top: -5px;
content: "";
height: 1px;
background: #b1b3b3
}
}
/*! CSS Used fontfaces */

@font-face {
font-family: font-icons;
src: url(fonts/font-icons.eot?ba1a20208770ab9d95c3ab1d032c57cf);
src: url(fonts/font-icons.eot?ba1a20208770ab9d95c3ab1d032c57cf#iefix) format("embedded-opentype"), url(fonts/font-icons.woff?ba1a20208770ab9d95c3ab1d032c57cf) format("woff"), url(fonts/font-icons.ttf?ba1a20208770ab9d95c3ab1d032c57cf) format("truetype");
font-weight: 400;
font-style: normal
}

@font-face {
font-family: font-icons;
src: url(fonts/font-icons.eot?ba1a20208770ab9d95c3ab1d032c57cf);
src: url(fonts/font-icons.eot?ba1a20208770ab9d95c3ab1d032c57cf#iefix) format("embedded-opentype"), url(fonts/font-icons.woff?ba1a20208770ab9d95c3ab1d032c57cf) format("woff"), url(fonts/font-icons.ttf?ba1a20208770ab9d95c3ab1d032c57cf) format("truetype");
font-weight: 400;
font-style: normal
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Book.woff2) format("woff2"), url(fonts/Gotham-Book.woff) format("woff");
font-weight: 400;
font-display: auto
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Light.woff2) format("woff2"), url(fonts/Gotham-Light.woff) format("woff");
font-weight: 100;
font-display: auto
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Medium.woff2) format("woff2"), url(fonts/Gotham-Medium.woff) format("woff");
font-weight: 300;
font-display: auto
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Bold.woff2) format("woff2"), url(fonts/Gotham-Bold.woff) format("woff");
font-weight: 700;
font-display: auto
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Book.woff2) format("woff2"), url(fonts/Gotham-Book.woff) format("woff");
font-weight: 400;
font-display: auto
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Light.woff2) format("woff2"), url(fonts/Gotham-Light.woff) format("woff");
font-weight: 100;
font-display: auto
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Medium.woff2) format("woff2"), url(fonts/Gotham-Medium.woff) format("woff");
font-weight: 300;
font-display: auto
}

@font-face {
font-family: Gotham;
src: url(fonts/Gotham-Bold.woff2) format("woff2"), url(fonts/Gotham-Bold.woff) format("woff");
font-weight: 700;
font-display: auto
}
/*! CSS Used from: https://assets.radissonhotels.net/main/css/styles.min.css?v=42bae66861db1acc6407d25373d6d02457846b11 */

*,
::after,
::before {
box-sizing: border-box
}

ul {
margin-top: 0;
margin-bottom: 1rem
}

a {
color: #007bff;
text-decoration: none;
background-color: transparent
}

a:hover {
color: #0056b3;
text-decoration: underline
}

.col-12,
.col-lg-3,
.col-lg-9 {
position: relative;
width: 100%;
padding-right: 15px;
padding-left: 15px
}

.col-12 {
flex: 0 0 100%;
max-width: 100%
}

.order-1 {
order: 1
}

.order-2 {
order: 2
}

.order-3 {
order: 3
}

@media (min-width:992px) {
.col-lg-3 {
flex: 0 0 25%;
max-width: 25%
}
.col-lg-9 {
flex: 0 0 75%;
max-width: 75%
}
.order-lg-2 {
order: 2
}
.order-lg-3 {
order: 3
}
}

@media (min-width:992px) {
.d-lg-flex {
display: flex
}
}

.flex-column {
flex-direction: column
}

.logos-rewards-container {
flex-wrap: wrap
}

.justify-content-center {
justify-content: center
}

.align-items-center,
.logos-rewards-container {
align-items: center
}

.w-100 {
width: 100%
}

.p-0 {
padding: 0
}

.pt-4,
.py-4 {
padding-top: 1.5rem
}

.py-4 {
padding-bottom: 1.5rem
}

.text-center {
text-align: center
}

@media print {
*,
::after,
::before {
text-shadow: none;
box-shadow: none
}
a:not(.btn) {
text-decoration: underline
}
}

a,
div,
li,
ul {
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline
}

ul {
list-style: none
}

[class*=col-] {
float: left
}

::-moz-focus-inner {
border: 0;
padding: 0
}

* {
-webkit-tap-highlight-color: transparent
}

.text-center {
text-align: center
}

.row [class*=col-] {
flex: 0 0 auto
}

::-webkit-scrollbar {
width: 6px;
height: 6px
}

::-webkit-scrollbar-track {
background: #eee
}

::-webkit-scrollbar-thumb {
background: #888;
opacity: .3
}

::-webkit-scrollbar-thumb:hover {
opacity: .8
}

.lazy {
transition: all .8s ease
}

::-moz-selection {
color: #fff;
background: #979899
}

::selection {
color: #fff;
background: #979899
}

a {
color: #c20808
}

a:hover {
color: #c20808;
text-decoration: none
}

::-ms-clear {
display: none
}

.lazy {
opacity: 0
}

.entity-footer-logos .footer-logos-container {
width: 100%;
position: relative;
max-width: 850px;
margin: auto;
padding-top: 35px;
padding-bottom: 35px
}

.entity-footer-logos .footer-logos-container:after {
content: "";
width: 1px;
height: 117px;
background-color: #b1b3b3;
position: absolute;
left: 26%;
top: 0;
bottom: 0;
display: block;
margin: auto
}

@media (max-width:991.98px) {
.entity-footer-logos .footer-logos-container:after {
display: none
}
}

@media (max-width:991.98px) {
.entity-footer-logos .logo-container:before {
content: "";
position: absolute;
width: calc(100% - 30px);
height: 1px;
left: 15px;
background: #b1b3b3;
bottom: 0
}
}

@media (min-width:992px) {
.entity-footer-logos .logo-container {
border: 0
}
}

.entity-footer-logos .main-logo {
max-width: 150px
}

@media (max-width:991.98px) {
.entity-footer-logos .main-logo {
max-width: 217px;
width: 100%;
margin-bottom: 49px;
margin-top: 49px;
margin-left: auto;
margin-right: auto
}
}

.entity-footer-logos .list-logos-radisson {
position: relative
}

@media (max-width:991.98px) {
.entity-footer-logos .list-logos-radisson__hotelscom {
margin-top: 15px
}
}

.entity-footer-logos .logo-brand-footer.rco {
width: 61px;
margin-right: 17px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rco {
height: 39px
}
}

.entity-footer-logos .logo-brand-footer.rdb {
width: 85px;
margin-right: 17px
}

@media (min-width:992px) {
.entity-footer-logos .logo-brand-footer.rdb {
margin-top: 4px
}
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rdb {
height: 26px
}
}

.entity-footer-logos .logo-brand-footer.rad {
width: 67px;
margin-right: 19px
}

@media (min-width:992px) {
.entity-footer-logos .logo-brand-footer.rad {
margin-top: 2px
}
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rad {
height: 26px
}
}

.entity-footer-logos .logo-brand-footer.rdr {
width: 39px;
margin-right: 22px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.rdr {
height: 39px
}
}

.entity-footer-logos .logo-brand-footer.pph {
width: 40px;
margin-right: 20px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.pph {
height: 43px
}
}

.entity-footer-logos .logo-brand-footer.pii {
width: 62px;
margin-right: 20px
}

@media (min-width:992px) {
.entity-footer-logos .logo-brand-footer.pii {
margin-top: 8px
}
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.pii {
height: 22px
}
}

.entity-footer-logos .logo-brand-footer.cis {
width: 76px
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.entity-footer-logos .logo-brand-footer.cis {
height: 38px
}
}

@media (max-width:991.98px) {
.entity-footer-logos .logo-brand-footer {
margin: 4rem
}
.entity-footer-logos .logo-brand-footer.rco {
width: 100%;
max-width: 121px;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.rdb {
max-width: 155px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.rad {
max-width: 124px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.rdr {
max-width: 75px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.pph {
width: 100%;
max-width: 83px;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.pii {
max-width: 114px;
width: 100%;
margin-left: 0;
margin-right: 0
}
.entity-footer-logos .logo-brand-footer.cis {
max-width: 159px;
width: 100%;
margin-left: 0;
margin-right: 0
}
}

.list-logos-brands {
display: inline-block;
text-align: center;
margin-top: 30px
}

@media (min-width:992px) {
.list-logos-brands {
display: flex;
justify-content: center;
align-items: center
}
.list-logos-brands li {
display: inline-block;
vertical-align: middle
}
}

@media (max-width:991.98px) {
.list-logos-brands {
display: flex;
align-items: center;
flex-wrap: wrap
}
.list-logos-brands li {
width: 50%;
text-align: center
}
.list-logos-brands li:last-child {
width: 100%
}
}

.logos-rewards-container {
display: flex;
justify-content: space-around
}

@media (min-width:992px) {
.logos-rewards-container {
justify-content: center
}
}

@media (max-width:991.98px) {
.list-logos-radisson {
padding-top: 32px
}
.list-logos-radisson:before {
content: "";
position: absolute;
width: calc(100% - 30px);
height: 1px;
left: 15px;
background: #b1b3b3;
top: 0
}
}

@media (max-width:991.98px) {
.list-logos-radisson [class*=list-logos-radisson] {
display: inline-block;
text-align: center;
width: 50%;
margin-bottom: 4rem
}
}

.list-logos-radisson [class*=list-logos-radisson] {
position: relative;
height: 38px;
margin-bottom: 56px
}

@media (min-width:992px) {
.list-logos-radisson [class*=list-logos-radisson]:before {
position: absolute;
width: 110%;
top: -5px;
content: "";
height: 1px;
background: #b1b3b3
}
}

.dropdown .dropdown-menu-icon {
max-height: 210px;
overflow-y: auto
}

.lazy.loaded {
opacity: 1
}

.hero--slider.slick-initialized {
opacity: 1;
visibility: visible
}

@media (max-width:992px) {
.loyalty-bar .dropdown-loyalty.sm .dropdown-toggle {
color: #fff;
display: flex;
align-items: center;
padding: .6rem 1rem .6rem 2rem;
border-radius: 0
}
.loyalty-bar .dropdown-loyalty.sm .dropdown-toggle .logo {
height: 21px
}
.loyalty-bar .dropdown-loyalty.sm .collapse-menu {
position: absolute;
width: 100vw;
top: 34px;
left: 0;
z-index: 900;
background: #45494d
}
}

.header .customer-navigation li.reservation-link.loyalty-visibility.show {
display: inline-block
}

@media (min-width:992px) {
footer .footer-links button.accordion {
display: none
}
}

.hero .caption__title {
font-size: 6rem;
font-stretch: normal;
line-height: 1;
letter-spacing: -2px;
text-shadow: 0 2px 4px rgba(0, 0, 0, .81);
color: #fff;
margin-bottom: 10px
}

@media (min-width:992px) {
.hero .caption .caption__inner {
width: 60%
}
.header__body .dropdown-customer .dropdown-menu,
.header__body .dropdown-language .dropdown-menu {
padding: 0
}
.slick-slider .slick-dots.d-lg-none {
display: none
}
}

.hero--slider .caption {
opacity: 1;
-webkit-transform: translateY(-51%);
transform: translateY(-51%);
transition: all .7s linear(.32, .34, 0, 1.62) .6s
}

.hero .caption--right {
display: flex;
justify-content: flex-end
}

.hero .caption {
position: absolute;
font-weight: 700;
top: 50%;
left: 0;
right: 0;
-webkit-transform: translateY(-51%);
transform: translateY(-51%);
transition: all .7s linear(.32, .34, 0, 1.62) .6s
}

.slick-slider .caption {
position: absolute;
text-align: left;
padding: 15px;
color: #fff;
font-size: 40px;
font-weight: 700;
letter-spacing: .02em;
opacity: 0;
z-index: 1;
transition: all .3s ease;
top: 50%;
left: 0;
right: 0;
display: block;
margin: auto;
-webkit-backface-visibility: hidden;
backface-visibility: hidden
}

.slick-slider .caption,
.slick-slider .slick-slide,
.slick-slider .slick-slide::before {
-webkit-backface-visibility: hidden;
backface-visibility: hidden
}

.hero .caption__subtitle {
font-size: 2.4rem;
line-height: 3rem;
letter-spacing: -.8px;
font-weight: 150;
text-shadow: 0 2px 4px rgba(0, 0, 0, .81);
color: #fff;
margin-bottom: 20px
}

.banner-image {
position: relative;
z-index: 1
}

.banner-image .caption {
z-index: 1
}

.banner-image__title {
font-size: 2.8rem;
line-height: 3.2rem;
color: #fff;
margin-bottom: 1rem
}

@media (max-width:767.98px) {
.banner-image__title {
font-size: 3.2rem
}
}

.py-5 {
padding-bottom: 3rem
}

.py-5 {
padding-top: 3rem
}

.hero--slider .amp-carousel-button:after,
.hero--slider .amp-carousel-button:before {
color: #c20808;
font-size: 25px;
font-family: font-icons;
line-height: 1;
font-weight: 400;
font-style: normal;
speak: none;
text-decoration: inherit;
text-transform: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased
}

.hero--slider .amp-carousel-button:after,
.hero--slider .amp-carousel-button:before {
font-size: 35px;
color: #fff
}

.hero--slider .amp-carousel-button-prev:before {
content: "\f10e"
}

.hero--slider .amp-carousel-button-next:before {
content: "\f10f"
}

.hero .caption__title {
font-size: 6rem;
font-stretch: normal;
line-height: 1;
letter-spacing: -2px;
text-shadow: 0 2px 4px rgba(0, 0, 0, .81);
color: #fff;
margin-bottom: 10px
}

@media (min-width:992px) {
.hero .caption .caption__inner {
width: 60%
}
#raddison--highlight,
.slick-slider .slick-dots.d-lg-none {
display: none
}
}

.hero--slider .caption {
opacity: 1;
-webkit-transform: translateY(-51%);
transform: translateY(-51%);
transition: all .7s linear(.32, .34, 0, 1.62) .6s
}

.hero .caption--right {
display: flex;
justify-content: flex-end
}

.hero .caption {
position: absolute;
font-weight: 700;
top: 50%;
left: 0;
right: 0;
-webkit-transform: translateY(-51%);
transform: translateY(-51%);
transition: all .7s linear(.32, .34, 0, 1.62) .6s
}

.hero .caption__subtitle {
font-size: 2.4rem;
line-height: 3rem;
letter-spacing: -.8px;
font-weight: 150;
text-shadow: 0 2px 4px rgba(0, 0, 0, .81);
color: #fff;
margin-bottom: 20px
}

.banner-image {
position: relative;
z-index: 1
}

.banner-image:before {
top: 0;
width: 100%;
height: 100%;
position: absolute;
display: block;
content: "";
transition: all .5s ease;
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .3) 70%, rgba(0, 0, 0, 0) 100%)
}

.banner-image__text {
width: 45%;
min-height: 300px;
color: #fff;
font-weight: 400
}

.banner-image__title {
font-size: 2.8rem;
line-height: 3.2rem;
color: #fff;
margin-bottom: 1rem
}

@media (max-width:767.98px) {
.banner-image__title {
font-size: 3.2rem
}
}

@media (max-width:991.98px) {
.banner-image__text {
min-height: 200px;
width: 100%
}
}

.offers-- .amp-carousel-button {
display: none
}

.py-5 {
padding-bottom: 3rem
}

.py-5 {
padding-top: 3rem
}

.card-city .location {
position: absolute;
top: 25px;
left: 25px;
z-index: 1
}

.slick-track {
position: relative;
left: 0;
top: 0;
display: block
}

.slick-slider .slick-list .slick-track {
display: flex
}

.slick-slider .slick-list .slick-track .slick-slide {
position: relative;
height: auto;
outline: 0
}

.slick-slide {
float: left;
height: 100%;
min-height: 1px
}

.slick-initialized .slick-slide {
display: block
}

.entity-slider-special .slider-nav .slick-list .slick-slide {
padding: 5px;
padding-bottom: 30px;
margin: 0 6px;
max-height: 177px
}

.slick-slider .slick-list .slick-track .slick-slide.slick-active {
z-index: 1;
border-radius: 6px;
padding: 0;
height: 141px;
width: 100%;
margin: 0 1rem
}

.slick-slider .slick-list .slick-track .slick-slide.slick-active.slick-current {
height: 171px;
transition: all .5s ease
}

.card-city:before {
cursor: pointer;
pointer-events: none;
position: absolute;
width: calc(100% - 20px);
height: calc(100% - 20px);
display: block;
content: "";
opacity: 1;
display: block;
margin: auto;
border-radius: 6px;
transition: all .5s ease;
background: linear-gradient(90deg, #000 20%, #312f2f 50%, #969393 100%);
opacity: .6
}

.entity-slider-special .slider-nav .slick-list .slick-slide:before {
width: 100%;
height: 100%
}

.entity-slider-special .slider-nav .slick-list .slick-slide:before {
background: linear-gradient(25deg, #000 0, rgba(41, 137, 216, .5) 70%, rgba(4, 5, 2, 0) 100%);
background: #ffffffc2;
opacity: .5
}

.entity-slider-special .slider-nav .slick-list .slick-slide:before {
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .2) 70%, rgba(0, 0, 0, 0) 100%);
opacity: .5;
z-index: 1
}

.entity-slider-special .slider-nav .slick-list .slick-slide:hover:before {
background: #fff;
opacity: 1;
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .2) 70%, rgba(0, 0, 0, 0) 100%)
}

.slick-list {
position: relative;
overflow: hidden;
display: block;
margin: 0;
padding: 0
}

.slick-slider .slick-list,
.slick-slider .slick-track {
-webkit-transform: translate3d(0, 0, 0);
-moz-transform: translate3d(0, 0, 0);
-ms-transform: translate3d(0, 0, 0);
-o-transform: translate3d(0, 0, 0);
transform: translate3d(0, 0, 0)
}

.slick-slider .slick-list {
margin-bottom: 2.5rem
}

.entity-slider-special .slider-nav .slick-list {
padding: 0;
width: calc(100% - 50px);
margin-left: 30px;
margin-right: 30px
}

.slick-track:after,
.slick-track:before {
content: "";
display: table
}

.banner-image__title,
.card-city .location__title,
.title-section {
font-size: 2.2rem;
line-height: 2.6rem;
margin-bottom: 2.2rem;
font-weight: 700;
color: #53565a
}

.card-city .location small,
.card-city .location__title {
text-shadow: 0 1px 1px rgba(0, 0, 0, .3);
color: #fff
}

.card-city .location__title {
margin-bottom: 0;
width: 100%;
font-weight: 300
}

.entity-slider-special .slider-nav .slick-list .slick-slide .location__title {
margin-bottom: 0;
font-weight: 100
}

.card-city .location small {
text-transform: uppercase;
font-weight: 100
}

.entity-slider-special .slider-nav .slick-list .slick-slide .location small {
font-weight: 300
}

.card-city .location small {
font-size: 1rem
}

.banner-image .caption {
z-index: 1
}

.banner-image__title {
font-size: 2.8rem;
line-height: 3.2rem;
color: #fff;
margin-bottom: 1rem
}

.slick-slider .caption {
opacity: 1;
-webkit-transform: translateY(-51%);
transform: translateY(-51%);
transition: all .7s linear(.32, .34, 0, 1.62) .6s
}

.entity-slider-special .slider-nav .slick-list .slick-slide.slick-current::before {
height: 100%;
width: 100%
}

.entity-slider-special .slider-nav .slick-list .slick-slide.slick-current::before {
background: #fff;
opacity: 1;
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .2) 70%, rgba(0, 0, 0, 0) 100%)
}

.slick-dots {
margin-top: 40px;
margin-bottom: 0
}

.marginauto {
margin: auto
}

.card {
height: 100%;
border-radius: .4rem;
border: 1px solid rgba(0, 0, 0, .08)
}

.card {
box-shadow: 0 1px 4px 0 rgba(177, 179, 179, .3)
}

.icon-gift-shop:before {
content: ""
}

[class*=" icon-"]:before,
[class^=icon-]:before {
font-family: font-icons;
display: inline-block;
line-height: 1;
font-weight: 400;
font-style: normal;
speak: none;
text-decoration: inherit;
text-transform: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale
}

.col-md-4 .card__title {
color: rgba(0, 0, 0, .7);
font-size: 1.7rem;
font-weight: 400;
line-height: 2.2rem;
margin-bottom: 10px;
margin-top: -14%
}

#main>section.entity-card-list--slider-image-icon>div>div>div>div>div:nth-child(1)>a>div>div>h3 {
width: 82%;
margin-left: 9%
}

#main>section.entity-card-list--slider-image-icon>div>div>div>div>div:nth-child(1)>a>div>div>h3>div {
font-weight: 400;
width: 146%;
margin-left: -21%;
margin-top: 10%
}

@media (max-width:767.98px) {
.slick-slider .slick-list {
margin-bottom: 1.5rem
}
}

@media (max-width:991.98px) {
.visible--desktop {
display: none
}
}

.col-md-4 {
height: 192px;
width: 228px
}

.dropdown-menu.show {
display: block
}

.ModelLayer {
position: fixed;
top: 0;
bottom: 0;
right: 0;
left: 0;
z-index: 1
}

.hotel-navigation .dropdown-menu.show {
display: flex;
justify-content: center;
opacity: 1;
pointer-events: all
}

.hotel-navigation .dropdown-menu ul {
max-width: 1199px;
display: flex;
align-items: center;
justify-content: space-around
}

.hotel-navigation .dropdown-menu ul li {
display: block;
width: 100%;
min-width: 150px;
text-align: center;
margin: 0
}

@media (min-width:992px) {
.hotel-navigation .dropdown-menu ul li {
min-width: 130px
}
}

.hotel-navigation .dropdown-menu ul li a amp-img {
display: block;
margin: auto;
max-height: 50px;
max-width: 80px
}

.amp-carousel-button {
background: unset
}

.position-static {
position: static
}

.in,
.out {
overflow: hidden;
position: fixed
}

.bg-white {
background-color: #fff
}

.off-canvas,
button.navbar-toggle {
top: 0
}

.off-canvas {
overflow: visible
}

.off-canvas {
width: 25rem;
width: 80%;
height: 100%;
left: -80%;
position: fixed;
z-index: 1039;
overflow: auto;
-webkit-overflow-scrolling: touch
}

.animation,
.canvas-wrap,
.in .off-canvas,
.navbar-toggle .hamburger-menu span {
transition: all .6s ease-out
}

.off-canvas[data-side=right] {
left: auto;
right: -80%
}

.in .off-canvas.out {
right: 0
}

[type=button]:not(:disabled),
[type=reset]:not(:disabled),
[type=submit]:not(:disabled),
button:not(:disabled) {
cursor: pointer
}

.navbar-toggle[data-side=right] {
right: 3%
}

.in .off-canvas.out [data-toggle=offcanvas] {
top: 1.5rem;
left: -12%;
background: 0 0;
position: absolute
}

.in .canvas-wrap,
.out .canvas-wrap {
opacity: 1;
pointer-events: all
}

@media (max-width:991.98px) {
.loyalty-bar .dropdown-loyalty.sm .collapse-menu.show {
height: calc(100vh - 95px)
}
}

.collapse.in,
.collapse.show {
max-height: inherit
}

.btn.btn-cvent,
.btn.btn-cvent [class*=speedrfp_button],
.card-box--collapse [data-toggle=collapse] .icon-arrow-down,
.card-box--collapse [data-toggle=collapse] .icon-arrow-up,
.collapse,
.hotel-navigation .dropdown-menu.show,
.lazy,
.nav-tabs.default .nav-item,
.transition,
[data-toggle=collapse] .icon-arrow-down,
[data-toggle=collapse] .icon-arrow-down::before,
[data-toggle=collapse] .icon-arrow-up,
[data-toggle=collapse] .icon-arrow-up::before {
transition: all .8s ease
}

.hotel-navigation .dropdown-menu ul li a amp-img {
display: block;
margin: auto;
max-height: 50px;
max-width: 80px
}

.hotel-navigation li.show::after {
content: "";
border-style: solid;
border-width: 0 7.5px 7.5px 7.5px;
border-color: transparent transparent #686868 transparent;
display: block;
width: 15px;
height: 15px;
position: relative;
margin: auto;
margin-top: -15px;
top: 10px
}

@media (max-width:1199.98px) {
.off-canvas .content-off-canvas .list--inline .dropdown-menu.show {
transform: translate3d(0, 0, 0)
}
.hotel-navigation .dropdown-menu ul {
max-width: 990px
}
.hotel-navigation .dropdown-menu ul li a amp-img {
max-width: 70px
}
}

@media (max-width:991.98px) {
.entity-footer-logos .logo-brand-footer.loaded {
margin: auto;
margin-top: 4rem;
margin-bottom: 4rem
}
.customer-navigation .dropdown-menu.show::before {
content: "";
border-style: solid;
border-width: 0 7.5px 7.5px 7.5px;
border-color: transparent transparent #686868 transparent;
display: block;
width: 15px;
height: 15px;
position: absolute;
margin: auto;
top: -15px;
margin: auto;
left: 0;
right: 0
}
.hotel-navigation .dropdown-menu {
display: none;
border-top: 2px solid #686868
}
.hotel-navigation .dropdown-menu.show ul {
display: inline-block;
width: 100%
}
.hotel-navigation .dropdown-menu.show ul li {
width: 32%;
display: inline-table;
vertical-align: middle;
min-width: inherit;
border-bottom: 0;
margin-bottom: 15px
}
.hotel-navigation .dropdown-menu.show a {
display: block;
width: 100%;
padding: 0
}
.hotel-navigation li.show:after {
top: 15px
}
}

.off-canvas {
overflow: visible
}

.form-group .form-control:focus,
.form-group .form-control:not(:placeholder-shown) {
border-bottom: 1px solid #dedede;
color: #000;
-webkit-box-shadow: none;
box-shadow: none
}

.form-group .form-control:focus~.label-up,
.form-group .form-control:not(:placeholder-shown)~.label-up {
top: -15px;
font-size: 1.2rem;
color: #53565a
}

.form-group .form-control:focus::placeholder,
.form-group .form-control:not(:placeholder-shown)::placeholder {
text-indent: inherit
}

.form-group .form-control:focus {
border-bottom: 1px solid #000
}

.form-group .form-control::placeholder {
color: #6c6c6c;
text-indent: -9999px
}

@media (min-width:992px) {
footer .d-flex.flex-column.d-lg-block {
display: block
}
i[class*=icon-].xxs.d-lg-none {
display: none
}
.d-lg-none {
display: none
}
.d-lg-inline-block {
display: inline-block
}
.d-lg-block {
display: block
}
.d-lg-flex {
display: flex
}
}

.banner-image:before {
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .01) 55%, rgba(0, 0, 0, 0) 80%);
z-index: 1
}

@media (min-width:992px) {
.entity-footer-logos .list-logos-radisson__rewards amp-img {
max-width: 86px;
margin-right: 36px
}
.entity-footer-logos .list-logos-radisson amp-img {
margin-top: 14px
}
.entity-footer-logos .list-logos-radisson__hotelscom amp-img {
max-width: 194px
}
.entity-footer-logos .list-logos-radisson__metings amp-img {
max-width: 86px;
margin-left: 20px
}
.entity-footer-logos .list-logos-radisson__metings:before {
width: 90%
}
.list-logos-radisson__rewards {
margin-left: 30px
}
}

@media (max-width:991.98px) {
.list-logos-radisson [class*=list-logos-radisson].order-3 {
display: flex
}
.entity-footer-logos .list-logos-radisson__hotelscom amp-img {
width: 150%;
height: 20px;
max-width: 299px
}
#hero-carousal .amp-carousel-button {
display: none
}
}

@media (max-width:767.98px) {
.banner-image__title {
font-size: 3.2rem
}
}

.title-section--center {
font-size: 2.8rem;
font-weight: 400;
display: inline-block;
position: relative;
margin-bottom: 4rem
}

.radisson-special {
position: relative;
width: 496px;
height: 368px
}

amp-img.contain {
object-fit: contain
}

@media (max-width:991.98px) {
.hero .caption__title {
font-size: 3.6rem;
line-height: 3.4rem;
letter-spacing: -1.8px;
margin-bottom: 8px
}
.hero .caption__subtitle {
font-size: 1.9rem;
line-height: 2.2rem;
letter-spacing: -.6px;
margin-bottom: 10px
}
}

.overflow-text.autoheight .content-overflowed:after {
background: unset;
}

ul {
list-style: none
}

.color-white.color-white {
color: #fff;
fill: #fff
}

.btn.btn-link--white {
color: #fff
}

.color-black.color-black {
color: #000;
fill: #000
}

.bg-color-grey-soft.bg-color-grey-soft {
background: #f3f3f2
}

.bg-color-gunmetal.bg-color-gunmetal {
background: #53565a
}

.color-gunmetal.color-gunmetal {
color: #53565a;
fill: #53565a
}

.bg-color-gallery.bg-color-gallery {
background: #eaeaea
}

.bg-color-facebook.bg-color-facebook {
background: #43619c
}

.bg-color-twitter.bg-color-twitter {
background: #24a9e6
}

.bg-color-instagram.bg-color-instagram {
background: #6a453b
}

[class*=" icon-"]:before,
[class^=icon-]:before {
font-family: font-icons;
display: inline-block;
line-height: 1;
font-weight: 400;
font-style: normal;
speak: none;
text-decoration: inherit;
text-transform: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale
}

.icon-arrow-down:before {
content: "\f10c"
}

.entity-breadcrumb .list.list--inline li:before {
content: "\f10f"
}

.icon-concierge:before {
content: "\f156"
}

.icon-facebook:before {
content: "\f17d"
}

.icon-free-award-nights:before {
content: "\f18a"
}

.icon-instagram:before {
content: "\f1b0"
}

.icon-partner-redemption-offers:before {
content: "\f1e7"
}

.icon-pin:before {
content: "\f1ee"
}

.icon-twitter:before {
content: "\f243"
}

.wrapper {
position: relative
}

[class*=col-] {
float: left
}

::-moz-focus-inner {
border: 0;
padding: 0
}

* {
-webkit-tap-highlight-color: transparent
}

.text-bold {
font-weight: 700
}

.text-left {
text-align: left
}

.text-center {
text-align: center
}

.text-right {
text-align: right
}

.text-underline {
text-decoration: underline
}

.card:after,
.card:before {
content: " "
}

.card:after {
clear: both
}

.hidden.hidden {
display: none
}

.sr-only {
position: absolute;
width: 1px;
height: 1px;
padding: 0;
margin: -1px;
overflow: hidden;
clip: rect(0, 0, 0, 0);
border: 0
}

.mb-8 {
margin-bottom: 6rem
}

.py-8 {
padding-bottom: 6rem
}

.py-8 {
padding-top: 6rem
}

.background-soft-grey {
background: #fafafa
}

.img-responsive {
width: 100%;
max-width: 100%;
height: auto
}

.row [class*=col-] {
flex: 0 0 auto
}

::-webkit-scrollbar {
width: 6px;
height: 6px
}

::-webkit-scrollbar-track {
background: #eee
}

::-webkit-scrollbar-thumb {
background: #888;
opacity: .3
}

::-webkit-scrollbar-thumb:hover {
opacity: .8
}

.mb-5 {
margin-bottom: 5rem
}

.mb-25 {
margin-bottom: 25px
}

.mt-50 {
margin-top: 50px
}

.mt-5 {
margin-top: 5rem
}

.px-6 {
padding-right: 4rem;
padding-left: 4rem
}

.collapse,
.lazy,
[data-toggle=collapse] .icon-arrow-down,
[data-toggle=collapse] .icon-arrow-down::before {
transition: all .8s ease
}

.card {
box-shadow: 0 1px 4px 0 rgba(177, 179, 179, .3)
}

.overflow-text {
width: 100%;
overflow: hidden;
display: block;
transition: 1s ease-out all;
position: relative;
margin-bottom: 10px;
color: #53565a
}

.overflow-text .content-overflowed:after {
content: "";
display: block;
width: 100%;
height: 2rem;
position: absolute;
bottom: 0;
background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 0) 1%, #fff 100%)
}

.bg-color-grey-totally-soft .overflow-text .content-overflowed:after {
background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 0, rgba(255, 255, 255, 0) 1%, #fafafa 100%)
}

.overflow-text.all-visible .content-overflowed:after {
display: none
}

.overflow-text+.btn {
display: none
}

.overflow-text+.btn.visible {
display: block
}

.slick-slider .slick-list {
margin-bottom: 2.5rem
}

@media (max-width:767.98px) {
.slick-slider .slick-list {
margin-bottom: 1.5rem
}
}

.slick-slider .slick-list .slick-track {
display: flex
}

.slick-slider .slick-list .slick-track .slick-slide {
position: relative;
height: auto;
outline: 0
}

@media (min-width:992px) {
.slick-slider .slick-list .slick-track .slick-slide[class*=col-] {
max-width: initial
}
}

.slick-slider .slick-list .slick-track .slick-slide.slick-active {
z-index: 1
}

.slick-slider .slick-list .slick-track .slick-slide.slick-active .caption {
opacity: 1;
-webkit-transform: translateY(-51%);
transform: translateY(-51%);
transition: all .7s linear(.32, .34, 0, 1.62) .6s
}

.slick-slider--banner {
display: block;
width: 100%
}

.slick-slider--banner .slick-list .slick-slide.banner-image {
height: 420px
}

@media (max-width:991.98px) {
.slick-slider--banner .slick-list .slick-slide.banner-image {
height: 360px
}
}

.slick-slider .slick-dots {
position: relative;
display: block;
width: 100%;
padding: 0;
list-style: none;
text-align: center;
margin-bottom: 2rem
}

.slick-slider .slick-dots li {
position: relative;
display: inline-block;
margin: 0 5px;
padding: 0;
cursor: pointer
}

.slick-slider .slick-dots li button {
font-size: 0;
line-height: 0;
display: block;
width: 7.5px;
height: 7.5px;
padding: 0;
cursor: pointer;
color: transparent;
border: 0;
outline: 0;
border-radius: 100%;
opacity: 1;
background: #ccc;
width: 7.5px;
height: 7.5px;
padding: 0
}

.slick-slider .slick-dots li.slick-active {
opacity: 1
}

.slick-slider .slick-dots li.slick-active button {
opacity: 1;
background: #c20808;
width: 10px;
height: 10px
}

.slick-slider .caption {
position: absolute;
text-align: left;
padding: 15px;
color: #fff;
font-size: 40px;
font-weight: 700;
letter-spacing: .02em;
opacity: 0;
z-index: 1;
transition: all .3s ease;
top: 50%;
left: 0;
right: 0;
display: block;
margin: auto;
-webkit-backface-visibility: hidden;
backface-visibility: hidden
}

.slick-slider .caption,
.slick-slider .slick-slide,
.slick-slider .slick-slide::before {
-webkit-backface-visibility: hidden;
backface-visibility: hidden
}

::-moz-selection {
color: #fff;
background: #979899
}

::selection {
color: #fff;
background: #979899
}

h3 {
font-size: inherit;
margin: 0;
width: auto;
color: inherit
}

.h1,
.h2,
.h3 {
color: inherit;
display: block;
font-size: inherit;
font-weight: 300;
margin: 0;
text-decoration: none;
width: auto
}

a {
color: #c20808
}

a:hover {
color: #c20808;
text-decoration: none
}

.h1 {
font-weight: 700;
font-size: 3.6rem
}

.text-12 {
font-size: 1.2rem
}

.overflow-text,
.text-14 {
font-size: 1.4rem
}

.card--hotel .card__title,
.subtitle-section {
font-size: 1.8rem;
line-height: 2.2rem
}

.text-28 {
font-size: 2.8rem;
line-height: 2.8rem
}

.card {
height: 100%;
border-radius: .4rem;
border: 1px solid rgba(0, 0, 0, .08)
}

.card__body {
width: 100%;
max-width: 90%;
display: block;
margin: auto;
flex: 1 1 auto;
padding: 1.25rem
}

@media (max-width:767.98px) {
.card__body {
height: auto
}
}

.card__title {
color: rgba(0, 0, 0, .7);
font-size: 1.8rem;
font-weight: 700;
line-height: 2.2rem;
margin-bottom: 10px
}

.card__text {
color: #686868;
font-size: 1.4rem;
line-height: 2rem;
margin-bottom: 25px
}

.card__text+.btn {
margin-bottom: 4rem
}

.card__image {
margin-bottom: 20px;
min-height: 1px;
height: 200px;
width: 100%;
object-fit: cover
}

@media (max-width:991.98px) {
.card__image {
height: 400px
}
}

@media (max-width:767.98px) {
.card__image {
height: 280px
}
}

@media (max-width:575.98px) {
.card__image {
height: 190px
}
}

.card>[class*=icon-] {
font-size: 60px;
width: 60px;
height: 60px;
display: block;
margin: auto;
margin-bottom: 20px;
margin-top: 35px
}

@media (max-width:991.98px) {
.slick-cards .slick-track {
display: flex
}
.slick-cards .slick-track .slick-slide {
height: auto
}
.slick-cards .slick-track .slick-slide[class*=col-] {
padding-right: 10px;
padding-left: 10px
}
.slick-cards .slick-list {
padding: 0 5% 0 5%
}
}

.dropdown .dropdown-menu {
box-shadow: 0 12px 12px 0 rgba(0, 0, 0, .12);
background: #fff;
margin: 0;
padding: 0;
z-index: 2
}

.list li:last-child {
margin-bottom: 0
}

.list--inline {
display: inline-block
}

.list--inline li {
display: inline-block;
margin-bottom: 0
}

.list--inline li:before {
content: " | ";
color: #979797
}

.list--inline li:first-child:before {
content: "";
display: none
}

.navbar.navbar-light {
border-bottom: 1px solid #d8d8d8;
margin: 0;
padding: 0
}

@media (min-width:992px) {
.navbar.navbar-light .navbar-collapse {
max-height: inherit;
height: inherit
}
}

.navbar.navbar-light .nav-item {
margin-bottom: 0;
text-align: center;
position: relative;
margin: 0
}

.navbar.navbar-light .nav-item::before {
background: #c20808;
content: "";
position: absolute;
bottom: 0;
width: 0;
height: 2px;
display: block;
transition: all .4s ease;
left: 0;
right: 0;
margin: auto
}

.navbar.navbar-light .nav-item:hover {
color: #45494d
}

.navbar.navbar-light .nav-item:hover::before {
width: 100%
}

.navbar.navbar-light .nav-item .nav-link {
border: 0;
position: relative;
font-size: 1.3rem;
padding: 1.8rem 1.8rem;
color: #53565a;
text-align: center;
transition: all .4s ease;
height: 100%;
white-space: nowrap
}

.navbar.navbar-light .nav-item .nav-link:hover {
color: #000
}

.navbar.navbar-light .nav-item .nav-link.active {
font-weight: 700;
color: #000
}

.navbar.navbar-light .nav-item .nav-link.active::before {
background: #c20808;
content: "";
position: absolute;
bottom: 0;
width: 100%;
height: 2px;
display: block;
left: 0;
right: 0;
margin: auto
}

.navbar.navbar-light .nav-item .nav-link:focus {
outline-color: #c20808;
outline-width: .1rem;
outline-style: dotted
}

@media (max-width:991.98px) {
.navbar.navbar-light[data-toggle=collapse] {
border-radius: 4px;
border: solid 1px #bdbdbd;
cursor: pointer
}
.navbar.navbar-light[data-toggle=collapse] .label-item {
padding: 2rem;
max-height: 50px;
line-height: 1.19rem;
width: 100%
}
.navbar.navbar-light[data-toggle=collapse] .icon-arrow-down {
width: 10px;
float: right
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link {
text-align: left;
border-bottom: 1px solid #d8d8d8;
cursor: pointer
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link:hover {
color: #000
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link.active {
font-weight: 700;
background: #f3f3f2;
color: #000
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link.active::before {
background: 0 0
}
}

::-ms-clear {
display: none
}

[data-toggle=collapse] [class*=icon-] {
vertical-align: text-top
}

[data-toggle=collapse] .icon-arrow-down {
position: relative;
margin-left: .2rem
}

.collapse {
display: block;
max-height: 0;
overflow: hidden
}

.banner-image {
position: relative;
z-index: 1
}

.banner-image:before {
top: 0;
width: 100%;
height: 100%;
position: absolute;
display: block;
content: "";
transition: all .5s ease;
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .3) 70%, rgba(0, 0, 0, 0) 100%)
}

.banner-image .caption {
z-index: 1
}

.banner-image__text {
width: 45%;
min-height: 300px;
color: #fff;
font-weight: 400
}

@media (max-width:991.98px) {
.banner-image__text {
min-height: 200px;
width: 100%
}
}

.banner-image__title {
font-size: 2.8rem;
line-height: 3.2rem;
color: #fff;
margin-bottom: 1rem
}

@media (max-width:767.98px) {
.banner-image__title {
font-size: 3.2rem
}
}

.overflow-text ul,
.text-rich ul {
list-style: inherit;
padding: 1rem 0 1rem 0
}

.overflow-text ul li,
.text-rich ul li {
margin-left: 1.5rem;
padding-left: 1.5rem
}

.lazy {
opacity: 0
}

.lazy.loaded,
.lazy[data-was-processed=true] {
opacity: 1
}

.entity-we-nav,
.menu-main {
margin-bottom: 1rem
}

@media (min-width:992px) {
.entity-we-nav,
.menu-main {
margin-bottom: 3rem
}
}

@media (max-width:991.98px) {
.entity-we-nav,
.menu-main {
top: 0;
z-index: 100;
background-color: #fff;
width: 100%;
transition: top .2s ease-in-out
}
.entity-we-nav.nav-show,
.menu-main.nav-show {
position: -webkit-sticky;
position: -moz-sticky;
position: -ms-sticky;
position: -o-sticky;
position: sticky
}
.entity-we-nav .navbar,
.menu-main .navbar {
border-bottom: 1px solid #bdbdbd
}
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link,
.menu-main .nav-item .dropdown-toggle.nav-link {
color: #c20808
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link::after,
.menu-main .nav-item .dropdown-toggle.nav-link::after {
display: none
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link .icon,
.menu-main .nav-item .dropdown-toggle.nav-link .icon {
vertical-align: middle;
padding: 0 0 .2rem 0
}

@media (min-width:992px) {
.entity-we-nav #navbar-we-menu.collapse,
.menu-main #navbar-we-menu.collapse {
overflow: visible
}
.entity-we-nav .nav-link:hover,
.menu-main .nav-link:hover {
cursor: pointer
}
}

@media (max-width:1199.98px) {
.entity-we-nav #navbar-we-menu,
.menu-main #navbar-we-menu {
max-height: 45vh;
overflow: auto
}
}

@media (max-width:1199.98px) and (orientation:portrait) {
.entity-we-nav #navbar-we-menu,
.menu-main #navbar-we-menu {
max-height: 59vh
}
}

.card--hotel {
border: 0;
font-size: 1.2rem;
width: 100%
}

.card--hotel .hotel-name {
text-align: left;
margin-bottom: 5px
}

@media (max-width:991.98px) {
.card--hotel .hotel-name {
height: auto
}
}

.card--hotel .logo-brand {
display: block;
width: 70px;
height: 100%;
float: right;
max-width: 100%
}

.card--hotel .hotel-reviews {
display: flex;
flex-direction: column;
justify-content: flex-end;
align-items: flex-start;
font-size: 1.2rem
}

.card--hotel .hotel-reviews img {
height: 13.5px;
display: block
}

@media (max-width:991.98px) {
.card--hotel .hotel-reviews {
margin-bottom: 15px
}
}

@media (max-width:991.98px) {
.card--hotel {
margin-bottom: 15px
}
}

.card--hotel .card__body {
max-width: 100%;
display: block;
margin: auto;
padding-top: 2rem;
justify-content: space-between;
flex-direction: column;
display: flex
}

.card--hotel .card__body small {
font-size: 1rem;
display: inline-block;
max-width: 90%
}

.card--hotel .card__title {
font-weight: 700;
line-height: 2.4rem
}

.card--hotel .card__image {
height: auto;
min-height: 150px;
width: 100%;
margin-bottom: 0;
max-height: fit-content
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.card--hotel .card__image {
height: 0
}
}

.card--hotel .btn {
min-width: 100%;
width: 100%;
padding-left: 0;
padding-right: 0
}

.entity-hero {
position: relative;
height: auto;
z-index: 101
}

.entity-hero:after,
.entity-hero:before {
content: " "
}

.entity-hero:after {
clear: both
}

.logo-brand {
display: block;
width: 100%;
height: 60px;
max-width: 100px;
float: right
}

.logo-brand.rco {
background: url(https://assets.radissonhotels.net/main/img/logos/rco.svg) no-repeat center center;
background-size: contain
}

.logo-brand.rdr {
background: url(https://assets.radissonhotels.net/main/img/logos/rdr.svg) no-repeat center center;
background-size: contain
}

.logo-brand.art {
background: url(https://assets.radissonhotels.net/main/img/logos/art.svg) no-repeat center center;
background-size: contain
}

.logo-rewards {
background: url(https://assets.radissonhotels.net/main/img/logos/radisson-rewards.svg) no-repeat center center;
background-size: contain
}

.entity-multimedia-text ul {
list-style: inherit
}

.entity-multimedia-text ul li {
margin-left: 50px
}

.loyalty-visibility {
opacity: 0;
display: none;
transition: all .6s ease-out
}

.loyalty-visibility.show {
opacity: 1;
display: block
}

@media (min-width:992px) {
.slick-cards .slick-track {
width: 100%
}
}

.breadcrumb-floating .entity-breadcrumb {
position: absolute;
top: 0;
width: 100%;
padding-left: 15px;
padding-right: 15px;
max-width: 1440px;
z-index: 101
}

@media (min-width:992px) {
.breadcrumb-floating .entity-breadcrumb {
padding-left: 45px;
padding-right: 45px;
margin-top: 10px
}
}

.breadcrumb-floating .entity-breadcrumb a,
.breadcrumb-floating .entity-breadcrumb li {
color: #fff
}

.breadcrumb-floating .entity-breadcrumb a:before,
.breadcrumb-floating .entity-breadcrumb li:before {
color: #fff
}

.breadcrumb-floating .entity-breadcrumb li a:focus {
text-decoration: underline;
outline: 0;
background-color: rgba(0, 0, 0, .08)
}

.breadcrumb-floating .entity-breadcrumb li a:hover {
text-decoration: underline
}

.entity-breadcrumb {
max-width: 1440px;
display: block;
margin: auto;
left: 0;
right: 0
}

.entity-breadcrumb li {
font-size: 1rem;
color: #53565a;
text-transform: uppercase;
overflow: hidden;
transition: all 0s linear;
height: 15px;
white-space: nowrap;
text-overflow: ellipsis;
font-weight: 100
}

@media (min-width:768px) {
.entity-breadcrumb li a {
color: #53565a
}
}

.entity-breadcrumb li a:focus {
text-decoration: underline;
outline: 0
}

.entity-breadcrumb li.active {
max-width: 500px;
font-weight: 700
}

.entity-breadcrumb li:before {
margin-right: .5rem;
margin-left: .5rem;
font-size: .5rem;
vertical-align: middle
}

@media (max-width:767.98px) {
.entity-breadcrumb li:before {
color: #fff
}
}

.entity-breadcrumb .list.list--inline li:before {
font-family: font-icons
}

.entity-breadcrumb__standard {
padding-left: 15px;
padding-right: 15px;
width: 100%;
max-width: 1299px
}

@media (max-width:767.98px) {
.entity-breadcrumb__standard {
padding-top: 5px;
background-color: rgba(0, 0, 0, .4)
}
}

@media (min-width:768px) {
.entity-breadcrumb__standard {
margin-top: 15px
}
}

@media (min-width:992px) {
.entity-breadcrumb__standard {
padding-left: 45px;
padding-right: 45px
}
}

@media (max-width:767.98px) {
.entity-breadcrumb__standard li a {
color: #fff
}
}

.hero {
position: relative;
width: 100%;
max-height: 770px;
height: 100vh;
transition: all 1.2s ease;
overflow: hidden
}

@media (min-width:992px) {
.hero {
margin-top: -130px
}
}

@media (max-width:1199.98px) {
.hero {
max-height: 450px
}
}

.hero figure {
position: relative;
height: 100%
}

.hero figure.image {
z-index: 0
}

.hero .slide-media {
-webkit-animation: slideOut .4s cubic-bezier(.4, .29, .01, 1);
animation: slideOut .4s cubic-bezier(.4, .29, .01, 1)
}

.hero .slide-image {
height: 100%;
background-size: cover;
background-position: center;
transition: all .8s ease
}

.hero .slide-image .image-entity {
width: 100%;
height: 100%;
object-fit: cover
}

@media screen and (-ms-high-contrast:active),
(-ms-high-contrast:none) {
.hero [style^=background-image] img.image-entity {
display: none
}
}

.hero .caption {
position: absolute;
font-weight: 700;
top: 50%;
left: 0;
right: 0;
-webkit-transform: translateY(-51%);
transform: translateY(-51%);
transition: all .7s linear(.32, .34, 0, 1.62) .6s
}

@media (max-width:991.98px) {
.hero .caption {
top: 54%
}
}

@media (min-width:992px) {
.hero .caption .caption__inner {
width: 60%
}
}

.hero .caption__title {
font-size: 6rem;
font-stretch: normal;
line-height: 1;
letter-spacing: -2px;
text-shadow: 0 2px 4px rgba(0, 0, 0, .81);
color: #fff;
margin-bottom: 10px
}

@media (max-width:991.98px) {
.hero .caption__title {
font-size: 3.6rem;
line-height: 3.4rem;
letter-spacing: -1.8px;
margin-bottom: 8px
}
}

@media (max-width:991px) and (orientation:landscape),
(max-height:600px) {
.hero .caption__title {
font-size: 3.6rem;
line-height: 3.4rem;
letter-spacing: -1.8px
}
}

.hero--common {
max-height: 300px
}

@media (min-width:992px) {
.hero--common {
margin-top: 0
}
}

.hero--common .slide-media {
z-index: 1;
position: relative
}

.hero--common .slide-media:before {
top: 0;
z-index: -1;
width: 100%;
height: 100%;
position: absolute;
display: block;
content: "";
transition: all .5s ease;
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .3) 70%, rgba(0, 0, 0, 0) 100%)
}

.hero--common .caption {
z-index: 2
}

@media (min-width:992px) {
.hero--common .caption .caption__inner {
width: 45%
}
}

.hero--common .caption .caption__inner .caption__title {
font-size: 4.5rem;
line-height: 4.5rem;
letter-spacing: -2.3px
}

.hero--common .caption .caption__inner .caption__description {
color: #fff;
font-weight: 400;
white-space: pre-line
}

.hero--common .caption .caption__inner .caption__cta {
padding: 1.5rem 5rem
}

.entity-slider-full-width .banner-image__text {
line-height: normal
}

@media (min-width:768px) {
.entity-slider-full-width .banner-image__text {
width: 55%
}
}

.entity-slider-full-width .banner-image__text .banner-image__title {
width: 60%
}

.loyalty-login {
padding-top: 4rem;
padding-bottom: 4rem
}

.loyalty-login .logo-rewards {
width: 12.5rem;
margin-right: 5rem;
position: relative
}

.loyalty-login .logo-rewards:before {
content: "";
background: #fff;
width: 1px;
height: 100%;
display: block;
position: absolute;
right: -2.5rem;
top: 0;
opacity: .3
}

@media (max-width:991.98px) {
.loyalty-login .logo-rewards:before {
display: none
}
}

.loyalty-login .logo-rewards amp-img {
width: 100%;
position: absolute;
top: 0;
bottom: 0;
display: block;
margin: auto;
max-width: 12rem
}

@media (max-width:991.98px) {
.loyalty-login .logo-rewards amp-img {
position: relative;
top: auto;
bottom: auto;
margin-bottom: 1.5rem
}
}

.loyalty-login .content_loyalty--text {
max-width: 60%
}

@media (max-width:991.98px) {
.loyalty-login .content_loyalty--text {
max-width: 100%
}
}

@media (max-width:991.98px) {
.loyalty-login .content_loyalty--text .btn-join {
margin-bottom: 1.5rem
}
.loyalty-login .content_loyalty--text .btn-join+span {
display: block
}
}

@media (max-width:767.98px) {
[class*="__social-buttons"] {
flex-wrap: wrap;
align-items: center
}
}

@media (max-width:767.98px) {
[class*="__social-buttons"] [class*="__items"] {
padding: 0 .35rem .5rem
}
}

@media (min-width:992px) and (max-width:1199.98px) {
[class*="__social-buttons"] [class*="__items"] {
margin: 0 1.8rem
}
}

@media (min-width:1200px) {
[class*="__social-buttons"] [class*="__items"] {
margin: 0 2.5rem
}
}

[class*="__social-buttons"] [class*="__items"] .social-network {
width: 4rem;
height: 4rem;
border-radius: 50%
}

[class*="__social-buttons"] [class*="__items"] .social-network:hover {
filter: brightness(80%)
}

@media (max-width:767.98px) {
[class*="__social-buttons"] [class*="__items"] .social-network {
width: 3.3rem;
height: 3.3rem;
border-radius: 2rem
}
}

@media (max-width:767.98px) {
[class*="__social-buttons"] i[class*=icon-].sm {
width: 17px;
height: 17px;
font-size: 18px
}
}

.entity-social-media__content {
width: 100%
}

.entity-social-media__content>div {
width: 100%;
align-items: center
}

.entity-social-media__logo img {
width: 112px
}

.entity-social-media__text {
font-size: 1.5rem
}

@media (min-width:768px) {
.entity-social-media__text {
font-size: 1.8rem
}
}

@media screen and (-ms-high-contrast:active),
(-ms-high-contrast:none) {
.entity-hero .slide-image:before {
display: none
}
}
/*! CSS Used from: Embedded */

.banner-image:before {
background: linear-gradient(25deg, #000 0, rgba(0, 0, 0, .01) 55%, rgba(0, 0, 0, 0) 80%)
}

.slick-current {
padding-bottom: 2px
}

.slick-dots {
margin-top: 40px;
margin-bottom: 0
}

.title-section {
font-size: 2.2rem;
line-height: 2.6rem;
margin-bottom: 2.2rem;
font-weight: 700;
color: #53565a
}

.hero--common .slide-media:before {
background: unset
}

.col-md-9 {
position: relative;
width: 100%;
padding-right: 15px;
padding-left: 15px
}

.slick-cards .slick-slide {
height: auto
}

.col-md-4 .card__title {
font-weight: 700;
margin-top: .2rem
}

@media (min-width:768px) {
.col-md-9 {
flex: 0 0 75%;
max-width: 75%
}
}

.title-section.h2 {
font-size: 2.2rem;
line-height: 2.6rem;
margin-bottom: 2.2rem;
font-weight: 700;
color: #53565a
}

.slick-slider .slick-slide.slick-active .caption {
opacity: 1;
transform: translateY(-51%)
}

.line-both,
.line-left {
border-left: 1px solid #b1b3b3
}

.btn.btn-secondary.btn-block.text-uppercase.font-medium {
padding-left: 0;
padding-right: 0
}

@media (min-width:992px) {
.justify-content-lg-end {
justify-content: flex-end
}
.search-box--date .checkout-label.label-up {
padding-left: 94px
}
.block-search-container .block-search-bar .autocomplete-block .label-up.checkin-label.d-flex,
.block-search-container .block-search-bar .autocomplete-block .label-up.checkout-label.d-flex {
left: 34px
}
}
/*! CSS Used from: Embedded */

*,
::after,
::before {
box-sizing: border-box
}

ul {
margin-top: 0;
margin-bottom: 1rem
}

a {
color: #007bff;
text-decoration: none;
background-color: transparent
}

.dropdown {
position: relative
}

.dropdown-toggle {
white-space: nowrap
}

.dropdown-toggle::after {
display: inline-block;
margin-left: .255em;
vertical-align: .255em;
content: "";
border-top: .3em solid;
border-right: .3em solid transparent;
border-bottom: 0;
border-left: .3em solid transparent
}

.dropdown-menu {
position: absolute;
top: 100%;
left: 0;
z-index: 1000;
display: none;
float: left;
min-width: 10rem;
padding: .5rem 0;
margin: .125rem 0 0;
font-size: 1rem;
color: #212529;
text-align: left;
list-style: none;
background-color: #fff;
background-clip: padding-box;
border: 1px solid rgba(0, 0, 0, .15);
border-radius: .25rem
}

.nav-link {
display: block;
padding: .5rem 1rem
}

.navbar-nav {
display: flex;
flex-direction: column;
padding-left: 0;
margin-bottom: 0;
list-style: none
}

.navbar-nav .nav-link {
padding-right: 0;
padding-left: 0
}

.navbar-nav .dropdown-menu {
position: static;
float: none
}

@media (min-width:992px) {
.navbar-expand-lg .navbar-nav {
flex-direction: row
}
.navbar-expand-lg .navbar-nav .dropdown-menu {
position: absolute
}
.navbar-expand-lg .navbar-nav .nav-link {
padding-right: .5rem;
padding-left: .5rem
}
}

.navbar-light .navbar-nav .nav-link {
color: rgba(0, 0, 0, .5)
}

.d-none {
display: none
}

.mr-auto {
margin-right: auto
}

a,
div,
i,
li,
span,
ul {
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline
}

ul {
list-style: none
}

[class*=" icon-"]:before {
font-family: font-icons;
display: inline-block;
line-height: 1;
font-weight: 400;
font-style: normal;
speak: none;
text-decoration: inherit;
text-transform: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale
}

.icon-arrow-down:before {
content: "\f10c"
}

::-moz-focus-inner {
border: 0;
padding: 0
}

::-webkit-scrollbar {
width: 6px;
height: 6px
}

::-webkit-scrollbar-track {
background: #eee
}

::-webkit-scrollbar-thumb {
background: #888;
opacity: .3
}

::-webkit-scrollbar-thumb:hover {
opacity: .8
}

a {
color: #c20808
}

i {
font-size: 1.2rem;
line-height: 1.6rem;
margin-bottom: 15px;
font-style: italic;
font-weight: 400
}

i[class*=icon-] {
background-size: contain;
vertical-align: middle;
width: 48px;
height: 48px;
font-size: 48px;
display: block;
position: relative;
margin: 0
}

i[class*=icon-]::before {
vertical-align: baseline
}

i[class*=icon-].xxs {
display: inline-block;
width: 8px;
height: 15px;
font-size: 8px
}

.dropdown .dropdown-menu {
box-shadow: 0 12px 12px 0 rgba(0, 0, 0, .12);
background: #fff;
margin: 0;
padding: 0;
z-index: 2
}

.navbar.navbar-light .nav-item {
margin-bottom: 0;
text-align: center;
position: relative;
margin: 0
}

.navbar.navbar-light .nav-item::before {
background: #c20808;
content: "";
position: absolute;
bottom: 0;
width: 0;
height: 2px;
display: block;
left: 0;
right: 0;
margin: auto
}

.navbar.navbar-light .nav-item .nav-link {
border: 0;
position: relative;
font-size: 1.3rem;
padding: 1.8rem;
color: #53565a;
text-align: center;
height: 100%;
white-space: nowrap
}

@media (max-width:991.98px) {
.navbar.navbar-light[data-toggle=collapse] .icon-arrow-down {
width: 10px;
float: right
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link {
text-align: left;
border-bottom: 1px solid #d8d8d8
}
}

::-ms-clear {
display: none
}

[data-toggle=collapse] [class*=icon-] {
vertical-align: text-top
}

[data-toggle=collapse] .icon-arrow-down {
position: relative;
margin-left: .2rem
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link,
.menu-main .nav-item .dropdown-toggle.nav-link {
color: #c20808
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link::after,
.menu-main .nav-item .dropdown-toggle.nav-link::after {
display: none
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link .icon,
.menu-main .nav-item .dropdown-toggle.nav-link .icon {
vertical-align: middle;
padding: 0 0 .2rem
}
/*! CSS Used from: https://assets.radissonhotels.net/main/css/styles.min.css?v=06d990da0eea8012ebe1e4f70d9028f9fa827d3e */

*,
::after,
::before {
box-sizing: border-box
}

ul {
margin-top: 0;
margin-bottom: 1rem
}

a {
color: #007bff;
text-decoration: none;
background-color: transparent
}

a:hover {
color: #0056b3;
text-decoration: underline
}

.dropdown {
position: relative
}

.dropdown-toggle {
white-space: nowrap
}

.dropdown-toggle::after {
display: inline-block;
margin-left: .255em;
vertical-align: .255em;
content: "";
border-top: .3em solid;
border-right: .3em solid transparent;
border-bottom: 0;
border-left: .3em solid transparent
}

.dropdown-toggle:empty::after {
margin-left: 0
}

.dropdown-menu {
position: absolute;
top: 100%;
left: 0;
z-index: 1000;
display: none;
float: left;
min-width: 10rem;
padding: .5rem 0;
margin: .125rem 0 0;
font-size: 1rem;
color: #212529;
text-align: left;
list-style: none;
background-color: #fff;
background-clip: padding-box;
border: 1px solid rgba(0, 0, 0, .15);
border-radius: .25rem
}

.nav-link {
display: block;
padding: .5rem 1rem
}

.nav-link:focus,
.nav-link:hover {
text-decoration: none
}

.navbar-nav {
display: flex;
flex-direction: column;
padding-left: 0;
margin-bottom: 0;
list-style: none
}

.navbar-nav .nav-link {
padding-right: 0;
padding-left: 0
}

.navbar-nav .dropdown-menu {
position: static;
float: none
}

@media (min-width:992px) {
.navbar-expand-lg .navbar-nav {
flex-direction: row
}
.navbar-expand-lg .navbar-nav .dropdown-menu {
position: absolute
}
.navbar-expand-lg .navbar-nav .nav-link {
padding-right: .5rem;
padding-left: .5rem
}
}

.navbar-light .navbar-nav .nav-link {
color: rgba(0, 0, 0, .5)
}

.navbar-light .navbar-nav .nav-link:focus,
.navbar-light .navbar-nav .nav-link:hover {
color: rgba(0, 0, 0, .7)
}

.navbar-light .navbar-nav .nav-link.active {
color: rgba(0, 0, 0, .9)
}

.d-none {
display: none
}

.mr-auto {
margin-right: auto
}

@media print {
*,
::after,
::before {
text-shadow: none;
box-shadow: none
}
a:not(.btn) {
text-decoration: underline
}
}

a,
div,
i,
li,
span,
ul {
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline
}

ul {
list-style: none
}

[class*=" icon-"]:before {
font-family: font-icons;
display: inline-block;
line-height: 1;
font-weight: 400;
font-style: normal;
speak: none;
text-decoration: inherit;
text-transform: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale
}

.icon-arrow-down:before {
content: "\f10c"
}

::-moz-focus-inner {
border: 0;
padding: 0
}

* {
-webkit-tap-highlight-color: transparent
}

::-webkit-scrollbar {
width: 6px;
height: 6px
}

::-webkit-scrollbar-track {
background: #eee
}

::-webkit-scrollbar-thumb {
background: #888;
opacity: .3
}

::-webkit-scrollbar-thumb:hover {
opacity: .8
}

[data-toggle=collapse] .icon-arrow-down,
[data-toggle=collapse] .icon-arrow-down::before {
transition: all .8s ease
}

::-moz-selection {
color: #fff;
background: #979899
}

::selection {
color: #fff;
background: #979899
}

a {
color: #c20808
}

a:hover {
color: #c20808;
text-decoration: none
}

i {
font-size: 1.2rem;
line-height: 1.6rem;
margin-bottom: 15px;
font-style: italic;
font-weight: 400
}

i[class*=icon-] {
background-size: contain;
vertical-align: middle;
width: 48px;
height: 48px;
font-size: 48px;
display: block;
position: relative;
margin: 0
}

i[class*=icon-]::before {
vertical-align: baseline
}

i[class*=icon-].xxs {
display: inline-block;
width: 8px;
height: 15px;
font-size: 8px
}

.dropdown .dropdown-menu {
box-shadow: 0 12px 12px 0 rgba(0, 0, 0, .12);
background: #fff;
margin: 0;
padding: 0;
z-index: 2
}

.navbar.navbar-light .nav-item {
margin-bottom: 0;
text-align: center;
position: relative;
margin: 0
}

.navbar.navbar-light .nav-item::before {
background: #c20808;
content: "";
position: absolute;
bottom: 0;
width: 0;
height: 2px;
display: block;
transition: all .4s ease;
left: 0;
right: 0;
margin: auto
}

.navbar.navbar-light .nav-item:hover {
color: #45494d
}

.navbar.navbar-light .nav-item:hover::before {
width: 100%
}

.navbar.navbar-light .nav-item .nav-link {
border: 0;
position: relative;
font-size: 1.3rem;
padding: 1.8rem 1.8rem;
color: #53565a;
text-align: center;
transition: all .4s ease;
height: 100%;
white-space: nowrap
}

.navbar.navbar-light .nav-item .nav-link:hover {
color: #000
}

.navbar.navbar-light .nav-item .nav-link.active {
font-weight: 700;
color: #000
}

.navbar.navbar-light .nav-item .nav-link.active::before {
background: #c20808;
content: "";
position: absolute;
bottom: 0;
width: 100%;
height: 2px;
display: block;
left: 0;
right: 0;
margin: auto
}

.navbar.navbar-light .nav-item .nav-link:focus {
outline-color: #c20808;
outline-width: .1rem;
outline-style: dotted
}

@media (max-width:991.98px) {
.navbar.navbar-light[data-toggle=collapse] .icon-arrow-down {
width: 10px;
float: right
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link {
text-align: left;
border-bottom: 1px solid #d8d8d8;
cursor: pointer
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link:hover {
color: #000
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link.active {
font-weight: 700;
background: #f3f3f2;
color: #000
}
.navbar.navbar-light[data-toggle=collapse] .nav-item .nav-link.active::before {
background: 0 0
}
}

::-ms-clear {
display: none
}

[data-toggle=collapse] [class*=icon-] {
vertical-align: text-top
}

[data-toggle=collapse] .icon-arrow-down {
position: relative;
margin-left: .2rem
}

[data-toggle=collapse]:not(.collapsed) .icon-arrow-down::before {
transform: rotate(180deg)
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link,
.menu-main .nav-item .dropdown-toggle.nav-link {
color: #c20808
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link::after,
.menu-main .nav-item .dropdown-toggle.nav-link::after {
display: none
}

.entity-we-nav .nav-item .dropdown-toggle.nav-link .icon,
.menu-main .nav-item .dropdown-toggle.nav-link .icon {
vertical-align: middle;
padding: 0 0 .2rem 0
}

@media (min-width:992px) {
.entity-we-nav .nav-link:hover,
.menu-main .nav-link:hover {
cursor: pointer
}
}

.btn.font-medium {
font-weight: 300;
}

@media (max-width:992px) {
.justify-content-around {
justify-content: space-around
}
}

@media (min-width:992px) {
.d-lg-none {
display: none
}
.d-lg-block {
display: block
}
}

.flex-column {
flex-direction: column
}

.flex-wrap {
flex-wrap: wrap
}

.flex-nowrap {
flex-wrap: nowrap
}

.justify-content-center {
justify-content: center
}

.justify-content-between {
justify-content: space-between
}

.align-items-center {
align-items: center
}

.align-items-stretch {
align-items: stretch
}

@media (min-width:992px) {
.flex-lg-row {
flex-direction: row
}
.justify-content-lg-center {
justify-content: center
}
}

.position-relative {
position: relative
}

.sr-only {
position: absolute;
width: 1px;
height: 1px;
padding: 0;
overflow: hidden;
clip: rect(0, 0, 0, 0);
white-space: nowrap;
border: 0
}

.sr-only-focusable:active,
.sr-only-focusable:focus {
position: static;
width: auto;
height: auto;
overflow: visible;
clip: auto;
white-space: normal
}

.w-50 {
width: 50%
}

.w-100 {
width: 100%
}

.m-0 {
margin: 0
}

.mr-0 {
margin-right: 0
}

.mb-0 {
margin-bottom: 0
}

.mb-1 {
margin-bottom: .25rem
}

.mb-2 {
margin-bottom: .5rem
}

.mt-3,
.my-3 {
margin-top: 1rem
}

.mb-3,
.my-3 {
margin-bottom: 1rem
}

.mb-4 {
margin-bottom: 1.5rem
}

.mt-5,
.my-5 {
margin-top: 3rem
}

.mb-5,
.my-5 {
margin-bottom: 3rem
}

.p-0 {
padding: 0
}

.px-2 {
padding-right: .5rem
}

.px-2 {
padding-left: .5rem
}

.p-3 {
padding: 1rem
}

.py-3 {
padding-top: 1rem
}

.py-3 {
padding-bottom: 1rem
}

.py-4 {
padding-top: 1.5rem
}

.py-4 {
padding-bottom: 1.5rem
}

.p-5 {
padding: 3rem
}

.pt-5 {
padding-top: 3rem
}

.px-5 {
padding-right: 3rem
}

.px-5 {
padding-left: 3rem
}

.m-auto {
margin: auto
}

@media (min-width:768px) {
.p-md-5 {
padding: 3rem
}
}

@media (min-width:992px) {
.p-lg-0 {
padding: 0
}
}

.text-left {
text-align: left
}

.text-center {
text-align: center
}

.text-uppercase {
text-transform: uppercase
}

@media print {
*,
::after,
::before {
text-shadow: none;
box-shadow: none
}
p {
orphans: 3;
widows: 3
}
.container {
min-width: 992px
}
}

a,
div,
form,
h5,
header,
i,
label,
p,
section,
span {
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline
}

header,
section {
display: block
}

.color-primary.color-primary {
color: #c20808;
fill: #c20808
}

.border-color-primary.border-color-primary {
border: 1px solid #c20808
}

.color-black.color-black {
color: #000;
fill: #000
}

.color-grey-cool-two.color-grey-cool-two {
color: #b1b3b3;
fill: #b1b3b3
}

.bg-color-grey-soft.bg-color-grey-soft {
background: #f3f3f2
}

.color-gunmetal.color-gunmetal {
color: #53565a;
fill: #53565a
}

[class*=" icon-"]:before,
[class^=icon-]:before {
font-family: font-icons;
display: inline-block;
line-height: 1;
font-weight: 400;
font-style: normal;
speak: none;
text-decoration: inherit;
text-transform: none;
text-rendering: auto;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale
}

.icon-arrow-left:before {
content: "\f10e"
}

.icon-calendar:before {
content: "\f139"
}

.icon-call-center:before {
content: "\f13a"
}

.icon-flipchart-markers:before {
content: "\f186"
}

.icon-location:before {
content: "\f1c0"
}

.icon-search:before {
content: "\f214"
}

[class*=col-] {
float: left
}

::-moz-focus-inner {
border: 0;
padding: 0
}

input {
-webkit-appearance: none;
-moz-appearance: none
}

* {
-webkit-tap-highlight-color: transparent
}

.text-left {
text-align: left
}

.text-center {
text-align: center
}

.clearfix:after,
.clearfix:before {
content: " "
}

.clearfix:after {
clear: both
}

.sr-only {
position: absolute;
width: 1px;
height: 1px;
padding: 0;
margin: -1px;
overflow: hidden;
clip: rect(0, 0, 0, 0);
border: 0
}

.absolute {
position: absolute
}

.row [class*=col-] {
flex: 0 0 auto
}

::-webkit-scrollbar {
width: 6px;
height: 6px
}

::-webkit-scrollbar-track {
background: #eee
}

::-webkit-scrollbar-thumb {
background: #888;
opacity: .3
}

::-webkit-scrollbar-thumb:hover {
opacity: .8
}

.mb-5 {
margin-bottom: 5rem
}

.mt-5 {
margin-top: 5rem
}

@media (max-width:991.98px) {
.no-border__md {
border: 0
}
}

::-moz-selection {
color: #fff;
background: #979899
}

::selection {
color: #fff;
background: #979899
}

.h2 {
color: inherit;
display: block;
font-size: inherit;
font-weight: 300;
margin: 0;
text-decoration: none;
width: auto
}

a {
color: #c20808
}

a:hover {
color: #c20808;
text-decoration: none
}

.text-12 {
font-size: 1.2rem
}

.form-group .form-control,
.options-group .options-content .option-header {
font-size: 1.4rem
}

p {
font-size: 1.4rem;
line-height: 2rem
}

.text-18 {
font-size: 1.8rem;
line-height: 2.2rem
}

i {
font-size: 1.2rem;
line-height: 1.6rem;
margin-bottom: 15px;
font-style: italic;
font-weight: 400
}

p {
font-weight: 400;
font-size: 1.4rem;
line-height: 2rem;
margin-bottom: 15px
}

.font-bold {
font-weight: 700
}

.font-medium {
font-weight: 300
}

.datepicker-wrapper .vc-container {
font-family: Gotham
}

.datepicker-wrapper .vc-container .vc-day-content {
font-family: Gotham
}

i[class*=icon-] {
background-size: contain;
vertical-align: middle;
width: 48px;
height: 48px;
font-size: 48px;
display: block;
position: relative;
margin: 0
}

i[class*=icon-]::before {
vertical-align: baseline
}

i[class*=icon-].xs {
display: inline-block;
width: 16px;
height: 17px;
font-size: 16px
}

i[class*=icon-].sm {
display: inline-block;
width: 24px;
height: 24px;
font-size: 24px
}

i[class*=icon-].xl {
display: inline-block;
width: 60px;
height: 60px;
font-size: 60px
}

button:focus {
box-shadow: 0 0 0 3px rgba(194, 8, 8, .25)
}

[role=button] {
cursor: pointer
}

.btn {
transition: all .3s ease;
font-size: 1.4rem;
font-weight: 400;
border-radius: .4rem;
white-space: inherit
}

.btn+.btn {
margin-left: 15px
}

.btn [class*=icon-] {
vertical-align: middle;
margin-right: .5rem
}

.btn:disabled:not(.m-progress) {
opacity: .55
}

.btn:focus {
outline-width: .1rem;
outline-style: dotted
}

.btn.btn-primary {
padding: 2rem 2.5rem;
line-height: 2rem;
background-color: #c20808;
border: none
}

.btn.btn-primary:disabled:not(.m-progress) {
background-color: #686868
}

.btn.btn-primary:focus {
box-shadow: 0 0 0 3px rgba(194, 8, 8, .25)
}

.btn.btn-secondary {
padding: 2rem 2.5rem;
line-height: 2rem;
background-color: #fff;
color: #c20808;
border-color: #c20808;
border: 1px solid
}

.btn.btn-secondary:disabled:hover:not(.m-progress) {
background-color: #fff;
color: #c20808;
border-color: #c20808
}

.btn.btn-secondary:focus {
box-shadow: 0 0 0 3px rgba(194, 8, 8, .25)
}

.btn.btn-sm {
font-size: 1.2rem;
padding: 1rem 1.5rem;
line-height: 1.2rem;
font-weight: 300
}

.options-group {
box-shadow: 0 2px 4px rgba(8, 21, 37, .12);
position: absolute;
transform: translate(-50%, 0);
left: 50%;
background: #fff;
min-width: max-content;
min-width: -moz-max-content;
min-width: -webkit-max-content;
min-width: -o-max-content;
margin-top: 20px;
border-top: 2px solid #6c6c6c;
z-index: 900;
width: 100%;
display: table
}

@media (max-width:991.98px) {
.options-group {
min-width: 100%
}
}

.options-group:before {
content: "";
width: 0;
height: 0;
border-style: solid;
border-width: 0 7.5px 7px 7.5px;
border-color: transparent transparent #6c6c6c transparent;
position: absolute;
left: 0;
right: 0;
margin: auto;
top: -9px
}

@media (max-width:991.98px) {
.options-group {
z-index: 9;
position: absolute
}
.options-group [class*=col-] {
margin-bottom: 0
}
}

.options-group .options-content {
position: relative;
max-height: 320px;
overflow-y: auto;
overflow-x: hidden;
transition: all 2s ease-in-out;
scroll-behavior: smooth
}

@media (max-width:991.98px) {
.options-group .options-content {
max-height: 300px
}
}

.options-group .options-content .option-header {
margin: 0;
color: #53565a;
border: 0;
display: block;
width: 100%;
padding: .85rem 1.6rem;
background: #f3f3f2;
text-transform: capitalize
}

form:not(.form-inline) .label-up {
white-space: nowrap
}

::-ms-clear {
display: none
}

.form-group {
padding: 0;
margin-bottom: 2.8rem;
position: relative
}

.form-group .form-control {
border: 0;
border-bottom: 1px solid #dedede;
width: 100%;
padding: .375rem .75rem .375rem 0;
border-radius: 0;
min-height: 28px;
background-color: transparent
}

.form-group .form-control:focus,
.form-group .form-control:not(:placeholder-shown) {
border-bottom: 1px solid #dedede;
color: #000;
-webkit-box-shadow: none;
box-shadow: none
}

.form-group .form-control:focus~.label-up,
.form-group .form-control:not(:placeholder-shown)~.label-up {
top: -15px;
font-size: 1.2rem;
color: #53565a
}

.form-group .form-control:focus::placeholder,
.form-group .form-control:not(:placeholder-shown)::placeholder {
text-indent: inherit
}

.form-group .form-control:focus {
border-bottom: 1px solid #000
}

.form-group .form-control::placeholder {
color: #6c6c6c;
text-indent: -9999px
}

.form-group .form-control::-moz-placeholder {
color: transparent
}

.form-group .form-control:-ms-input-placeholder {
color: transparent
}

.form-group .form-control:focus::-moz-placeholder {
color: #6c6c6c
}

.form-group .form-control:disabled {
opacity: 1;
color: #6c6c6c;
cursor: not-allowed
}

.form-group .form-control:disabled~label {
color: #6c6c6c
}

.form-group label {
display: block;
color: #53565a;
line-height: 1.8rem;
font-size: 1.2rem
}

.form-group .label-up {
font-size: 1.4rem;
margin-bottom: 5px;
transition: .2s ease all;
-moz-transition: .2s ease all;
-webkit-transition: .2s ease all;
position: absolute;
pointer-events: none;
left: 0;
top: 6px;
overflow-x: hidden;
text-overflow: ellipsis;
width: 100%
}

.form-group .read-only-arrow {
background-color: transparent;
background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkNhcGFfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiCgkgdmlld0JveD0iMCAwIDggNSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgOCA1OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6I0MwMEQwRDt9Cjwvc3R5bGU+CjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0wLDFjMCwwLjEsMCwwLjEsMC4xLDAuMWwzLjgsMy44YzAsMCwwLDAsMCwwQzQsNSw0LjEsNSw0LjIsNC45bDMuOC0zLjhDOCwxLjEsOCwxLjEsOCwxQzgsMSw4LDAuOSw3LjksMC45CglMNy4xLDBDNy4xLDAsNywwLDcsMGMwLDAsMCwwLDAsMEw0LDNjMCwwLDAsMCwwLDBMMSwwYzAsMC0wLjEsMC0wLjEsMGMwLDAsMCwwLDAsMEwwLjEsMC45QzAsMC45LDAsMC45LDAsMUMwLDAuOSwwLDAuOSwwLDEKCUMwLDEsMCwxLDAsMXoiLz4KPC9zdmc+Cg==);
background-size: 9px 9px;
background-repeat: no-repeat;
background-position: 99%;
cursor: pointer
}

@media all and (-ms-high-contrast:none),
(-ms-high-contrast:active) {
.form-group .label-up {
top: -15px;
font-size: 1.2rem;
color: #53565a
}
.form-group .form-control:-ms-input-placeholder {
text-indent: inherit;
color: #6c6c6c
}
}

input[type=text]::-ms-clear {
display: none;
width: 0;
height: 0
}

input[type=text]::-ms-reveal {
display: none;
width: 0;
height: 0
}

.modal {
z-index: 1041
}

.modal .modal-header {
border: none;
min-height: 50px;
align-items: center;
padding-left: 0;
padding-right: 0
}

.modal .close {
opacity: 1;
position: absolute;
top: 7px;
right: 0;
width: 20px;
height: 25px;
margin: 0;
padding: 5px 0 5px 0
}

.modal .close span {
background: #fff;
display: block;
width: 20px;
height: 1px;
transform: rotate(45deg);
position: relative
}

.modal .close span+span {
transform: rotate(-45deg);
top: -1px
}

.modal .close span.sr-only {
background: 0 0
}

.modal .modal-title {
color: #fff;
font-size: 2rem
}

.modal .modal-content {
background: 0 0;
border: 0
}

.modal .modal-body {
padding: 0
}
/*! CSS Used from: https://assets.radissonhotels.net/searchbar/searchbar.css?v=c9665bb5c4436f9841f8dbb954c9f1ab31dcb5de */

.rna-calendar .vc-day-disabled {
text-decoration: line-through
}

.search-box--date .calendar__icon {
padding-right: 13px
}

.search-box--date .checkin-label {
padding-right: 141px
}

.search-box--date .checkout-label {
padding-left: 54px
}

@media (min-width:992px)and (max-width:1199.98px) {
.search-box--date .checkin-label {
padding-right: 106px
}
.search-box--date .checkout-label {
padding-left: 84px
}
}

@media (max-width:991.98px) {
.search-box--date .checkin-label {
margin-left: 30px;
width: auto
}
.search-box--date .checkout-label {
margin-left: 65px;
width: auto
}
}

.datepicker-wrapper__full-title {
margin: 1rem 0;
text-align: center;
font-size: 1.5rem;
color: #686868;
font-weight: 700
}

.datepicker-wrapper__full-title .back-icon {
padding-left: 1rem;
float: left
}

.datepicker-wrapper__full-title .calendar-icon {
padding-right: 3rem
}

@media (min-width:768px) {
.datepicker-wrapper__full-title {
display: none
}
}

.datepicker-wrapper .vc-title-wrapper {
pointer-events: none
}

.datepicker-wrapper .vc-title {
font-size: 1.4rem
}

.datepicker-wrapper .vc-weekday {
font-size: 1.2rem
}

.datepicker-wrapper .vc-weeks {
-ms-grid-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr
}

.datepicker-wrapper .date-picker-footer {
background: #f3f3f2;
-webkit-box-shadow: 0 2px 1px #dee2e6;
box-shadow: 0 2px 1px #dee2e6;
width: 100%;
display: block;
padding: 14px 10px;
text-align: center;
font-weight: 700;
position: relative;
margin-top: -5px
}

.datepicker-wrapper .vc-header {
padding: 10px;
height: 4.8rem
}

.datepicker-wrapper .vc-day.in-next-month,
.datepicker-wrapper .vc-day.in-prev-month {
visibility: hidden
}

.datepicker-wrapper .vc-day-layer .vc-day-box-center-center .vc-highlight {
background-color: #fff
}

.datepicker-wrapper .vc-day-layer .vc-day-box-left-center .vc-highlight,
.datepicker-wrapper .vc-day-layer .vc-day-box-right-center .vc-highlight {
border-width: 0;
background: #fae5e8
}

.datepicker-wrapper .vc-day-content {
font-weight: 400
}

.datepicker-wrapper .vc-day-disabled {
cursor: not-allowed;
pointer-events: none;
font-weight: 400
}

.datepicker-wrapper .vc-day-disabled:focus,
.datepicker-wrapper .vc-day-disabled:hover {
background: 0 0
}

.datepicker-wrapper .vc-border {
border-width: 0
}

.datepicker-wrapper .vc-popover-content-wrapper {
-webkit-transform: translate3d(0, 30px, 0);
transform: translate3d(0, 30px, 0)
}

.datepicker-wrapper .datepicker-footer-close-button:disabled {
background-color: #fff;
color: #c20808;
opacity: 1;
border: 1px solid #c20808
}

.datepicker-wrapper [class*=vc-grid-cell-col--] {
padding: 0
}

.datepicker-wrapper .vc-highlights~.vc-day-disabled {
text-decoration: none;
color: #686868;
font-weight: 500
}

@media (max-width:991.98px) {
.datepicker-wrapper .vc-pane {
overflow: auto
}
.datepicker-wrapper .vc-popover-content-wrapper:empty {
display: none
}
.datepicker-wrapper.calendar-dropdown {
position: fixed;
top: 0;
bottom: 0;
right: 0;
left: 0;
-webkit-transform: none;
transform: none;
overflow-y: scroll;
will-change: auto;
background-color: #fff;
z-index: 9999
}
.datepicker-wrapper.options-group {
display: block
}
.datepicker-wrapper .datePicker {
width: 100%
}
.datepicker-wrapper .date-picker-footer {
bottom: 0;
z-index: 1;
padding: 2rem
}
.datepicker-wrapper .datepicker-wrapper__full-footer {
position: fixed
}
.datepicker-wrapper .vc-arrows-container {
display: none
}
.datepicker-wrapper .vc-grid-container {
-ms-grid-columns: 100%
}
.datepicker-wrapper .vc-weeks {
-ms-grid-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr
}
}

.datepicker-wrapper--inline .vc-container {
width: 100%
}

@media (min-width:768px) {
.datepicker-wrapper--inline .vc-grid-cell-col-1 .vc-pane {
border-right: 1px solid #b1b3b3
}
}

.datepicker-wrapper--inline .vc-arrows-container {
display: -webkit-box;
display: -ms-flexbox;
display: flex
}

.datepicker-wrapper--inline .vc-grid-container {
-ms-grid-columns: 1fr 1fr
}

.datepicker-wrapper--inline .vc-grid-container .vc-weeks {
-ms-grid-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr
}

@media (max-width:991.98px) {
.datepicker-wrapper--inline .vc-grid-container {
-ms-grid-columns: 1fr
}
}

.datepicker-wrapper--inline .date-picker-footer {
-webkit-box-shadow: none;
box-shadow: none
}

.btn-occupancy[data-v-0c2dc47b] {
width: 30px;
height: 30px;
background-color: #dcdcdc;
border-radius: 50%;
position: relative
}

.btn-occupancy[data-v-0c2dc47b]:enabled {
background: #fae5e8;
cursor: pointer;
opacity: 1
}

.btn-occupancy.minus[data-v-0c2dc47b]:before,
.btn-occupancy.plus[data-v-0c2dc47b]:before {
content: "";
display: block;
width: 12px;
height: 2px;
background: #c20808;
position: absolute;
left: 0;
right: 0;
top: 0;
bottom: 0;
margin: auto
}

.btn-occupancy.plus[data-v-0c2dc47b]:after {
content: "";
display: block;
height: 12px;
width: 2px;
background: #c20808;
position: absolute;
left: 0;
right: 0;
top: 0;
bottom: 0;
margin: auto
}

.btn-occupancy[data-v-0c2dc47b]:hover:enabled {
background: #c20808;
color: #fae5e8
}

.btn-occupancy.minus[data-v-0c2dc47b]:hover:enabled:before,
.btn-occupancy.plus[data-v-0c2dc47b]:hover:enabled:after,
.btn-occupancy.plus[data-v-0c2dc47b]:hover:enabled:before {
background: #fae5e8
}

.occupancy-number[data-v-0c2dc47b] {
font-size: 2rem;
text-align: center
}

.sticky-room[data-v-4672e241] {
overflow-y: auto;
overflow-x: hidden;
max-height: 230px
}

.sticky-room .room-occupancy[data-v-4672e241] {
border-top: 1px solid #dcdcdc
}

.sticky-room .room-occupancy[data-v-4672e241]:first-of-type {
margin-top: 4.6rem
}

.sticky-room .room-occupancy--item[data-v-4672e241] {
padding-left: 6px;
padding-right: 6px
}

@media (max-width:991.98px) {
#offers-modal .modal-dialog {
max-width: 620px
}
}

@media (max-width:767.98px) {
#offers-modal .modal-dialog {
max-width: 580px
}
}

.form-group--autocomplete .search {
position: relative;
width: 100%
}

.form-group--autocomplete .input-search-autocomplete.main {
margin-left: 15px
}

.block-search-bar form .autocomplete-block .label-up.main {
left: 35px;
top: 1px
}

@media (max-width:991.98px) {
.block-search-bar form .autocomplete-block .label-up.main {
left: 30px;
width: auto
}
.calendar__icon {
padding-right: 15px
}
}

.form-group .form-control:focus~.label-up.main,
.form-group .form-control:not(:placeholder-shown)~.label-up.main {
top: -15px
}

.icon-calendar,
.icon-search {
opacity: .6;
zoom: .9
}

@media (min-width:992px) {
.icon-calendar,
.icon-search {
padding: 0 2px
}
}

@media (-ms-high-contrast:none),
screen and (-ms-high-contrast:active) {
.block-search-bar form .autocomplete-block .label-up.main {
top: -15px
}
}

.block-search-container .searchbar--modal .search-box[data-v-173139d7] {
border: none
}

.block-search-container .searchbar--modal .datepicker-wrapper--inline[data-v-173139d7] {
border: 1px solid #d8d8d8
}

@media (min-width:992px) {
.block-search-container .searchbar--modal .form-group--last .search-box[data-v-173139d7] {
border-right: 1px solid #d8d8d8
}
}

.error-message {
color: #f84c4c;
height: 12px;
margin: .2rem 0 0 3.5rem;
font-weight: 300;
font-style: italic;
font-size: .8rem;
text-transform: uppercase;
line-height: 1.3rem
}

.search-box {
position: relative;
height: 100%;
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: column
}

.form-group .label-up {
left: unset
}

.options-group {
z-index: 1;
top: 70px;
left: 0;
-webkit-transform: none;
transform: none;
border-top: 2px solid #b1b3b3;
-webkit-box-shadow: 0 2px 4px #b1b3b3;
box-shadow: 0 2px 4px #b1b3b3;
margin: 0 0 3rem 0
}

.options-group:before {
left: 30px;
right: auto;
border-color: transparent transparent #b1b3b3 transparent
}

.options-group .options-content .option-header {
display: -webkit-box;
display: -ms-flexbox;
display: flex;
padding: .9rem 1.6rem
}

.options-group .options-content .fixed-option-header {
position: absolute;
z-index: 1
}

.block-search-container .search {
height: 100%
}

.block-search-container {
-webkit-transition: all .6s ease-out;
transition: all .6s ease-out;
width: 1100px;
display: block;
margin: auto;
position: absolute;
bottom: 8%;
right: 0;
left: 0;
z-index: 601
}

.block-search-container:after {
display: none;
content: ""
}

.block-search-container .form-control {
max-height: 2rem
}

.block-search-container .submit-search {
min-height: 50px;
height: 100%;
width: 100%
}

.block-search-container .block-search-bar {
position: relative
}

.block-search-container .block-search-bar .form-group {
margin-bottom: 1rem;
position: relative
}

.block-search-container .block-search-bar .form-group[class*=col] label {
left: 6px
}

.block-search-container .block-search-bar .form-group[class*=col]:last-child {
position: relative
}

.block-search-container .block-search-bar .form-group .form-control {
letter-spacing: -.5px;
overflow: hidden;
text-overflow: ellipsis
}

.block-search-container .block-search-bar .form-group .form-control:focus {
-webkit-box-shadow: none;
box-shadow: none
}

.block-search-container .block-search-bar .form-group .location-control {
position: absolute;
right: 6px;
top: 0
}

@media (min-width:992px) {
.block-search-container .block-search-bar .autocomplete-block {
-webkit-box-flex: 1;
-ms-flex: 1 1 0px;
flex: 1 1 0
}
}

@media (max-width:991.98px) {
.block-search-container .block-search-bar .autocomplete-block {
margin-right: 0;
width: 100%
}
}

@media (max-width:991.98px) {
.block-search-container .block-search-bar .autocomplete-block--button {
padding: 1.5rem 0;
margin: 20px auto
}
}

@media (min-width:992px) {
.block-search-container .block-search-bar .autocomplete-block--button {
-webkit-box-flex: 0;
-ms-flex: 0 0 100px;
flex: 0 0 100px
}
}

.block-search-container .block-search-bar .autocomplete-block .form-group {
margin-bottom: 0
}

@media (max-width:991.98px) {
.block-search-container .block-search-bar .autocomplete-block .form-group--with-placeholder {
border-bottom: 1px solid #d8d8d8
}
}

.block-search-container .block-search-bar .autocomplete-block .label-up {
left: unset
}

@media (min-width:992px) {
.block-search-container .block-search-bar .autocomplete-block .label-up.checkin-label,
.block-search-container .block-search-bar .autocomplete-block .label-up.checkout-label {
left: 14px
}
}

.block-search-container .block-search-bar .autocomplete-block .datepicker-col-right {
padding: 0 5px
}

@media (max-width:991.98px) {
.block-search-container .block-search-bar .autocomplete-block .datepicker-col-right {
padding: 0
}
}

@media (max-width:991.98px) {
.block-search-container .block-search-bar .autocomplete-block .datepicker-col-right {
padding-right: .6rem
}
}

.block-search-container .block-search-bar .align-items-stretch .autocomplete-block {
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-align: stretch;
-ms-flex-align: stretch;
align-items: stretch
}

.block-search-container .block-search-bar .align-items-stretch .form-group {
width: 100%
}

.block-search-container .block-search-bar .row {
-webkit-box-align: center;
-ms-flex-align: center;
align-items: center
}

@media (max-width:991.98px) {
.block-search-container .block-search-bar {
left: 0;
width: 100%;
background: #fff;
border-radius: 0;
max-height: 0;
overflow: hidden
}
}

@media (max-width:991.98px) {
.block-search-container .block-search-bar form {
left: 0;
width: 100%;
background: #fff;
padding: 30px;
padding-bottom: 80px;
border-radius: 4px
}
.block-search-container .block-search-bar form .close {
top: 0;
right: 0;
position: absolute;
font-size: 2.5rem;
padding: 20px;
color: #c20808;
display: none
}
}

@media (min-width:992px) {
.block-search-container .block-search-bar {
background: #fff;
position: relative;
border-radius: 6px;
padding-top: 5px;
padding-bottom: 5px;
padding-left: 0;
padding-right: 0
}
.block-search-container .block-search-bar .close {
display: none
}
}

@media (max-width:1199.98px) {
.block-search-container {
max-width: 991px
}
}

@media (max-width:991.98px) {
.block-search-container {
-webkit-transition: width 0s ease-out;
transition: width 0s ease-out;
max-width: calc(100% - 30px);
border-radius: 6px
}
}

.component-selector {
width: 100%
}

.block-search-container.searchbar--modify {
z-index: 102;
position: relative;
max-width: inherit;
width: inherit
}

@media (max-width:1199.98px) {
.block-search-container.searchbar--modify {
margin: 0
}
}

@media (max-width:991.98px) {
.block-search-container.searchbar--modify .searchbar-modify {
height: 90vh
}
.block-search-container.searchbar--modify .searchbar-modify__container {
height: 100%
}
}

.block-search-container.searchbar--modify .search-info {
background-color: #fff;
margin: auto;
width: 100%;
margin-left: 0
}

@media (max-width:1199.98px) {
.block-search-container.searchbar--modify .search-info {
font-size: .9em
}
}

@media (max-width:991.98px) {
.block-search-container.searchbar--modify .search-info {
max-width: inherit;
padding: 1rem 3rem 1.5rem 0
}
}

.block-search-container.searchbar--modify .search-info span {
white-space: nowrap
}

.block-search-container.searchbar--modify .search-info div {
padding: 0 1.5rem 0 4rem;
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: column;
-webkit-box-pack: center;
-ms-flex-pack: center;
justify-content: center
}

@media (min-width:1200px) {
.block-search-container.searchbar--modify .search-info div {
padding: 0 2rem
}
}

@media (max-width:991.98px) {
.block-search-container.searchbar--modify .search-info div:last-child {
padding: 0;
margin-left: auto
}
}

.block-search-container.searchbar--modify .search-info div:last-child button {
opacity: 1;
position: inherit;
width: 100%;
bottom: 0;
left: 0;
padding: .8rem 1.9rem;
max-width: 20rem
}

.block-search-container.searchbar--modify .search-info div:last-child button span {
white-space: normal
}

@media (min-width:992px) {
.block-search-container.searchbar--modify .search-info div:last-child button {
padding: 1rem .8rem
}
}

@media (min-width:1200px) {
.block-search-container.searchbar--modify .search-info div:last-child button {
padding: 1rem 2rem
}
}

.block-search-container.searchbar--modify .search-box {
border-left: 1px solid #d8d8d8;
border-right: 1px solid #fff;
-webkit-box-flex: 1;
-ms-flex-positive: 1;
flex-grow: 1;
padding: 2rem .5rem 1rem
}

@media (max-width:991.98px) {
.block-search-container.searchbar--modify .search-box {
border: 1px solid #d8d8d8;
border-right: none;
border-left: none;
padding: 2.2rem .5rem 1.2rem;
border-bottom: 0
}
}

.block-search-container.searchbar--modify .block-search-bar {
border-radius: 0;
-webkit-box-shadow: 0 1px 4px #dee2e6;
box-shadow: 0 1px 4px #dee2e6
}

@media (min-width:992px) {
.block-search-container.searchbar--modify .block-search-bar {
padding: 5px 0;
max-height: inherit
}
}

@media (min-width:992px) {
.block-search-container.searchbar--modify .block-search-bar .autocomplete-block--button {
border-left: 1px solid #d8d8d8;
padding-left: 1rem
}
}

.block-search-container.searchbar--modify .block-search-bar .autocomplete-block--button .submit-search {
padding: 1.1rem 2.5rem;
margin-bottom: .2rem
}

@media (max-width:991.98px) {
.block-search-container.searchbar--modify .block-search-bar .modal-dialog {
max-width: 620px
}
}

@media (max-width:767.98px) {
.block-search-container.searchbar--modify .block-search-bar .modal-dialog {
max-width: 580px
}
}

.block-search-container.searchbar--modify .block-search-bar form {
min-width: 0
}

@media (max-width:1199.98px) {
.block-search-container.searchbar--modify .block-search-bar form {
padding: 0 15px
}
}

@media (max-width:575.98px) {
.block-search-container.searchbar--modify .block-search-bar form {
padding: 0
}
}

.block-search-container.searchbar--modify .block-search-bar form [class*=col-] {
margin-bottom: 0
}

.block-search-container.searchbar--modify .block-search-bar .submit-search:disabled {
opacity: 1
}

.block-search-container.searchbar--modify .block-search-bar .submit-search:disabled.btn-primary {
border: 1px solid #c20808
}

@media (max-width:991.98px) {
.block-search-container.searchbar--modify .block-search-bar [class*=col-]:last-child button {
opacity: 1;
position: static;
-webkit-transition-delay: initial;
transition-delay: 0s
}
.block-search-container.searchbar--modify .block-search-bar [class*=col-]:last-child button.btn-occupancy {
position: relative;
min-width: 30px;
min-height: 30px
}
}
/*! CSS Used from: https://assets.radissonhotels.net/search-map-destinations/search-map-destinations.css?v=bd59e928a1f253c100948466bfb4e7c735f6d265 */

.vc-day.in-next-month {
visibility: hidden
}

.vc-day-disabled:focus {
background: 0 0
}

.vc-day-layer .vc-day-box-right-center .vc-highlight {
border-width: 0;
background: #fae5e8
}
/*! CSS Used from: Embedded */

.vc-popover-content-wrapper[data-v-9dcbc3ae] {
position: absolute;
display: block;
outline: 0;
z-index: 10
}

.vc-popover-content-wrapper[data-v-9dcbc3ae]:not(.is-interactive) {
pointer-events: none
}
/*! CSS Used from: Embedded */

.vc-grid-container[data-v-3ca35a05] {
position: relative;
flex-shrink: 1;
display: -ms-grid;
display: grid;
overflow: auto;
-webkit-overflow-scrolling: touch
}

.vc-grid-cell[data-v-3ca35a05] {
display: flex;
justify-content: center;
align-items: center
}
/*! CSS Used from: Embedded */

.vc-svg-icon[data-v-5572e632] {
display: inline-block;
stroke: currentColor;
stroke-width: 0
}

.vc-svg-icon path[data-v-5572e632] {
fill: currentColor
}
/*! CSS Used from: Embedded */

.vc-day[data-v-21e64d49] {
position: relative;
min-height: 28px;
width: 100%;
height: 100%;
z-index: 1
}

.vc-day-layer[data-v-21e64d49] {
position: absolute;
left: 0;
right: 0;
top: 0;
bottom: 0;
pointer-events: none
}

.vc-day-box-center-center[data-v-21e64d49] {
display: flex;
justify-content: center;
align-items: center;
height: 100%;
transform-origin: 50% 50%
}

.vc-day-box-left-center[data-v-21e64d49] {
display: flex;
justify-content: flex-start;
align-items: center;
height: 100%;
transform-origin: 0 50%
}

.vc-day-box-right-center[data-v-21e64d49] {
display: flex;
justify-content: flex-end;
align-items: center;
height: 100%;
transform-origin: 100% 50%
}

.vc-day-content[data-v-21e64d49] {
display: flex;
justify-content: center;
align-items: center;
width: 28px;
height: 28px;
margin: 1.6px auto;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none
}

.vc-day-content[data-v-21e64d49]:hover {
background-color: rgba(204, 214, 224, .3)
}

.vc-day-content[data-v-21e64d49]:focus {
background-color: rgba(204, 214, 224, .4)
}

.vc-highlights[data-v-21e64d49] {
overflow: hidden;
pointer-events: none;
z-index: -1
}

.vc-highlight[data-v-21e64d49] {
width: 28px;
height: 28px
}

.vc-highlight.vc-highlight-base-start[data-v-21e64d49] {
width: 50%;
border-radius: 0;
border-right-width: 0
}

.vc-highlight.vc-highlight-base-end[data-v-21e64d49] {
width: 50%;
border-radius: 0;
border-left-width: 0
}
/*! CSS Used from: Embedded */

.vc-pane[data-v-4d247649] {
flex-grow: 1;
flex-shrink: 1;
display: flex;
flex-direction: column;
justify-content: center;
align-items: stretch
}

.vc-header[data-v-4d247649] {
flex-shrink: 0;
display: flex;
align-items: stretch;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
padding: 10px 10px 0 10px
}

.vc-title-layout[data-v-4d247649] {
display: flex;
justify-content: center;
align-items: center;
flex-grow: 1
}

.vc-title-wrapper[data-v-4d247649] {
position: relative
}

.vc-title[data-v-4d247649] {
cursor: pointer;
white-space: nowrap;
padding: 0 8px
}

.vc-title[data-v-4d247649],
.vc-weekday[data-v-4d247649] {
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none
}

.vc-weekday[data-v-4d247649] {
display: flex;
justify-content: center;
align-items: center;
flex: 1;
padding: 5px 0;
cursor: default
}

.vc-weeks[data-v-4d247649] {
flex-shrink: 1;
flex-grow: 1;
padding: 5px 6px 7px 6px
}
/*! CSS Used from: Embedded */

.vc-reset,
.vc-reset * {
line-height: 1.5;
box-sizing: border-box
}

.vc-reset:focus {
outline: 0
}

.vc-reset [role=button] {
cursor: pointer
}

.vc-border {
border-style: solid
}

.vc-bg-white {
background-color: #fff
}

.vc-border-transparent {
border-color: transparent
}

.vc-border-gray-400 {
border-color: #cbd5e0
}

.vc-rounded-full {
border-radius: 9999px
}

.vc-border {
border-width: 1px
}

.vc-cursor-pointer {
cursor: pointer
}

.vc-cursor-not-allowed {
cursor: not-allowed
}

.vc-flex {
display: flex
}

.vc-items-center {
align-items: center
}

.vc-justify-center {
justify-content: center
}

.vc-font-bold {
font-weight: 700
}

.vc-h-full {
height: 100%
}

.vc-opacity-0 {
opacity: 0
}

.vc-opacity-25 {
opacity: .25
}

.hover\:vc-opacity-50:hover {
opacity: .5
}

.vc-pointer-events-none {
pointer-events: none
}

.vc-pointer-events-auto {
pointer-events: auto
}

.vc-relative {
position: relative
}

.vc-text-white {
color: #fff
}

.vc-text-sm {
font-size: 14px
}

.vc-select-none {
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none
}

.vc-w-full {
width: 100%
}
/*! CSS Used from: Embedded */

.vc-container {
font-family: BlinkMacSystemFont, -apple-system, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
width: -webkit-max-content;
width: max-content
}

.vc-arrows-container {
width: 100%;
position: absolute;
top: 0;
display: flex;
justify-content: space-between;
padding: 8px 10px;
pointer-events: none
}

#block-search-container.affix {
left: 0;
position: fixed;
top: 0;
margin-top: 0;
width: 100%;
max-width: 100%;
border-radius: 0;
bottom: auto
}

.block-search-container.searchbar--modify {
z-index: 1022;
position: relative;
max-width: inherit;
width: inherit
}

.block-search-container {
-webkit-transition: all .6s ease-out;
transition: all .6s ease-out;
width: 1100px;
display: block;
margin: auto;
position: absolute;
bottom: 8%;
right: 0;
left: 0;
z-index: 601
}

.block-search-container.searchbar--modify .search-box.selected {
border-color: #c20808
}

@media (max-width:991.98px) {
#block-search-container.affix:not(.searchbar--hotel) .block-search-bar.visible {
height: 100%;
overflow-x: auto
}
.block-search-container.searchbar--modify .search-info {
max-width: inherit;
padding: 1rem 3rem 1.5rem 0
}
#block-search-container.affix {
overflow: visible;
overflow-y: unset
}
.block-search-container .block-search-bar.visible {
max-height: 90vh;
overflow: visible;
margin-top: 0
}
.block-search-container.searchbar--modify .block-search-bar.visible {
min-height: 100vh
}
.visible {
visibility: visible
}
.block-search-container.searchbar--modify {
margin: 0
}
.block-search-container {
-webkit-transition: width 0s ease-out;
transition: width 0s ease-out;
max-width: calc(100% - 30px);
border-radius: 6px
}
.block-search-container.searchbar--modify .search-box.selected {
outline: 1px solid #c20808;
outline-offset: -1px
}
.block-search-container.searchbar--modify .block-search-bar form.p-3 {
padding: 1rem
}
}

.form-group--autocomplete .options-group .options-content {
-webkit-transition: none;
transition: none
}

.form-group--autocomplete .options-group--suggestions {
min-width: 330px
}

.options-group {
z-index: 1;
top: 70px;
left: 0;
-webkit-transform: none;
transform: none;
border-top: 2px solid #b1b3b3;
-webkit-box-shadow: 0 2px 4px #b1b3b3;
box-shadow: 0 2px 4px #b1b3b3;
margin: 0 0 3rem 0
}

.options-group:before {
left: 30px;
right: auto;
border-color: transparent transparent #b1b3b3 transparent
}

.options-group .dropdown-option:focus {
background: #fae5e8;
font-weight: 700;
outline: 0
}

input[type=radio][name=option] {
margin: 0;
padding: 0;
-webkit-appearance: none;
-moz-appearance: none;
appearance: none;
width: 0;
height: 0;
border: 0
}

.block-search-container .block-search-bar .row {
-webkit-box-align: center;
-ms-flex-align: center;
align-items: center
}

.options-group .options-content li.hotel-option-cities {
padding: 1rem 2.5rem
}

.options-group li div {
padding: 0 6px
}

.options-group .options-content li {
padding: .8rem 2.5rem;
font-size: 1.2rem;
line-height: 1.6rem;
border-top: 1px solid #d8d8d8;
color: #53565a
}

.header.light {
background: #fff
}

.header.light .gradient-nav:after {
background-image: linear-gradient(to right, rgba(243, 243, 242, 0) 0, #f3f3f2 100%);
background-repeat: repeat-x
}

.header.light .gradient-nav:before {
background-color: #f3f3f2
}

.header.light .header__body .dropdown-customer--text {
color: #000
}

.header.light .header__body a,
.header.light .header__body a:link {
color: #000
}

.header.light .header__body a:hover,
.header.light .header__body a:link:hover {
color: #686868
}

.header.light .header__body a:focus,
.header.light .header__body a:link:focus {
outline-color: #c20808;
outline-width: .1rem;
outline-style: dotted
}

.header.light .header__body button.btn.btn-link {
color: #000
}

.header.light .header__body button.btn.btn-link:hover {
color: #686868
}

.header.light .header__body .btn.btn-link,
.header.light .header__body button,
.header.light .header__body i {
color: #000
}

.header.light .header__body .btn.btn-link:hover,
.header.light .header__body button:hover,
.header.light .header__body i:hover {
color: #686868
}

.header.light .header__body .btn.btn-link:focus,
.header.light .header__body button:focus,
.header.light .header__body i:focus {
outline-color: #c20808;
outline-width: .1rem;
outline-style: dotted
}

.position-inherit {
position: inherit
}

.card-city button span,
.text-12 {
font-size: 1.2rem
}

.d-flex {
display: flex
}

.d-none {
display: none
}

.btn.btn-link.text-bold,
strong {
font-weight: 700
}

@media (min-width:992px) {
.d-none.d-lg-flex {
display: flex
}
.d-none.d-lg-block {
display: block
}
#navigation-desktop {
display: flex
}
.d-lg-none {
display: none
}
.navbar-collapse.show-desktop.collapse:not(.show) {
display: block
}
}

.overflow-text.autoheight {
transition: .5s ease-in all;
max-height: 8000px;
}

.overflow-text {
max-height: 80px;
transition: .5s ease-in all;
}

[data-max-lines] {
line-height: 20px;
}

.off-canvas .content-off-canvas .list--inline .btn.btn-link.text-bold,
strong {
font-weight: bold;
}

.off-canvas .content-off-canvas .list--inline .btn.btn-link.text-12 {
font-size: 1.2rem;
}

.btn.btn-secondary:not(:disabled):not(.disabled):hover,
.btn.btn-cvent:not(:disabled):not(.disabled):hover[class*="speedrfp_button"],
.btn.btn-secondary_transparent:not(:disabled):not(.disabled):hover,
.btn.btn-gallery-icon:not(:disabled):not(.disabled):hover {
background-color: #c20808;
color: #fff;
}

.btn.btn-transparent:not(:disabled):not(.disabled):hover {
background-color: rgba(255, 255, 255, 0.3);
color: #fff;
}

.btn.btn-primary:not(:disabled):not(.disabled):hover {
background-color: #790505;
}