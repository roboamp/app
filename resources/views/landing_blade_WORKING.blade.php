<!DOCTYPE html>
<html lang="en">
@include ('includes.landing-styles')

<body>

    <!-- Header -->
    @include('includes.landing-nav')

    <!-- Features -->
    @include ('includes.landing-features')

    <!-- Banner -->
    


    <!-- About & Count -->

    <!-- How it works -->
    @include ('includes.landing-how-it-works')


    <!-- Screenshot carousel -->
    <div class="screenshot-area section section-padding" id="success_stories">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-xs-12">
                    <div class="section-header text-center">
                        <h3 class="section-title">Tier one brands are already using AMP</h3>
                        <!-- <p class="section-text">They've already converted!</p> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="screenshot-carousel-1" class="card-container grid screenshot-carousel-1 owl-carousel owl-theme">
                    @foreach ($case_studies as $case_study)
                        @include ('includes.landing-screenshot-carousel', $case_study)
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Pricing Table -->
    <div class="pricing-area primary-gradient-bg section section-padding bottom-0" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <div class="section-header text-center">
                        <h3 class="section-title text-white">Instant speed. Zero programming.</h3>
                        <p class="section-text text-white"><strong>Close more deals. Make more sales.</strong></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="pricing-packages">
                    @foreach ($plans as $plan)
                        @include ('includes.landing-pricing-table', $plan)
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Testimonial -->
    <div class="testimonial section section-padding bottom-0" id="testimonial">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-xs-12">
                    <div class="section-header text-center">
                        <h3 class="section-title">People are Saying</h3>
                        <p class="section-text">Folks love the results of AMP!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="testimonials-wrap">
                    <div class="testimonial-carousel-1 owl-carousel" id="testimonial-carousel">
                        @foreach ($carousel_testimonials as $testimonial)
                            @include ('includes.landing-testimonials', $testimonial)
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team -->
    <div class="team-area section section-padding" id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-xs-12">
                    <div class="section-header text-center">
                        <h3 class="section-title">ROBOAMP Crew</h3>
                        <p class="section-text"></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="team-items">
                    @foreach($team_members as $member)
                        @if (count($team_members) == 2)
                            <div class="col-md-6  col-sm-6">
                        @else
                            <div class="col-md-3 col-sm-6">
                        @endif
                            @include ('includes.landing-team-members', $member)
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Download area -->
    <!-- @include ('includes.landing-download-area') -->

    <!-- Subscribe box -->
    @include ('includes.landing-subscribe-box')

    <!-- contact section -->
    @include('includes.landing-contact-section')

    <!-- Map area-->
    <!-- @include('includes.landing-map-area') -->

    <!-- Footer -->
    @include ('includes.landing-footer')

    <!-- Script -->

</body>

@include('includes.landing-scripts')

</html>
