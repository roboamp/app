@extends('spark::...layouts.app')


@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="{{asset('css/dashboard/style.css')}}" rel="stylesheet">

    <home :customer="user" inline-template>

        <div class="container">
            <!-- Application Dashboard -->
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row state-overview user-margin">
                        @if(isset($counters))
                            @foreach($counters as $obj)
                                <div class="col-lg-6 col-sm-6">
                                    {!!$obj['html']!!}
                                </div>
                            @endforeach
                        @endif

                    </div>
                            @include('...includes.flash_notifications')

                    @include('...users.includes.blocks.amp_errors',array('errors'=>$errors))


                </div>
            </div>
        </div>
    </home>

@yield('js_files')






@endsection
