@extends('spark::layouts.app')

@section('content')

<?php
	if(!is_null($property)){
        $txt_hidden_property='<input type="hidden" id="txt_property_id" name="txt_property_id" value="'.$property->id.'">';
        $txt_hidden_property.='<input type="hidden"  name="txt_renew_subscription" value="yes">';

        $txt_property_placeholder="";
        $txt_property_readonly="readonly";
        $txt_property_value=$property->url;
	}else{
        $txt_hidden_property="";
        $txt_property_placeholder="https://";
        $txt_property_readonly="";
        $txt_property_value="https://";
	}

    if(!is_null($customer)){
        $txt_hidden='<input type="hidden" id="edit_customer_id" name="edit_customer_id" value="'.$customer['id'].'">';
        $txt_name_placeholder="";
        $txt_name_readonly="readonly";
        $txt_name_value=$customer->name;
        if(!is_null($property)){
            $btn_submit_value="Restore Subscription";
        }else{
        	$btn_submit_value="Add Website";
        }
    }else{
		$txt_hidden="";
		$txt_name_placeholder="Customer Name";
		$txt_name_readonly="";
		$txt_name_value="";
		$btn_submit_value="Add Customer";
    }

?>



	<head>
		<script src="https://checkout.stripe.com/checkout.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://use.typekit.net/cbe3rku.js"></script>
		<script>try { Typekit.load({ async: true }); } catch (e) {}</script>
		<link href='//fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' id='main-style-css'  href="{{asset('css/plans.css')}}" type='text/css' media='all' />
		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>

	</head>

	<home :customer="user" inline-template>
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-header">
						{{$btn_submit_value}}
					</div>
				</div>
				<div class="panel-body">
					<form id="status_form" method="POST" class="form-group" action="{{route('users.add.customer')}}">
						{{ csrf_field() }}
						<input type="hidden" name="stripeToken" id="stripeToken">
                        <input type="hidden" name="stripeEmail" id="stripeEmail">
                        <div class="form-group">
							<h3 class="text-center"><label for="name">STEP 1: Enter Customer Name </label></h3>
                                {!! $txt_hidden !!}
                                <input type="name" id="name"class="form-control" name="name" {{$txt_name_readonly}} value="{{$txt_name_value}}" placeholder="{{$txt_name_placeholder}}" autofocus required/>
                            <br>
							<h3 class="text-center"><label for="name">STEP 2: Enter Customer URL </label></h3>

							{!! $txt_hidden_property !!}
							<input type="text"  id="url" class="form-control" name="url" {{$txt_property_readonly}} value="{{$txt_property_value}}" placeholder="{{$txt_property_placeholder}}" autofocus required onfocus="var temp_value=this.value; this.value=''; this.value=temp_value"/>

							<br>
							<h3 class="text-center"><label for="plan_stripe_id">STEP 3: Select a plan!</label></h3>
							<p id="plan"></p>
						</div>
						<div class="row">
							<body id="homepage">
								<section id="products">
									<div class="center container pt2">
										<div class="cf">
											@foreach($plans as $myplans)
	                                            @if($myplans->active)
												<div class="col-md-6 col-sm-12">
													@include('layouts.add_customer',$myplans)
												</div>
	                                            @endif
											@endforeach
										</div>
									</div>
								</section>
							</body>
						</div>
						<div class="form-group text-center submit-button">
                            <input disabled type="submit" class="form-control" id="btn_submit" name="submit" value="{{$btn_submit_value}}">
						</div>
                        <input id="txt_status_form" type="hidden" name="plan_stripe_id" value="">
					</form>
				</div>
			</div>
		</div>
	</home>

	@include('includes.stop-form-submit')
@include('users.js.add',array('process_stripe_url'=>route($callback),'redirect_url'=>route('users.dashboard')))

@endsection