<div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-header">Current Customers
                                <a href="{{route('users.add.customer')}}"><button id="test" class="btn btn-default add_customer">Add Customer</button></a>
                            </div>
                        </div>
<div class="panel-body">
                            <table class="table table striped">
                                <thead>
                                <th style="width: 20%">Name</th>
                                <th style="width: 20%">Main Domain</th>
                                <th style="width: 5%">#</th>
                                <th>Actions</th>
                                </thead>
                                <tbody id="customer_table">
                                @if(isset($customers))
                                @forelse ($customers as $customer)
                                 <?php   if ($customer->all_accounts_suspended() == true  ){
                                    $td_id="suspended_customer_";
                                    $td_class="suspended_customer";
                                    $class="website_suspended";
                                    $btn_id="activate_";
                                    $btn_class="btn btn-success";
                                    $btn_caption="Activate";

                                    }else{
                                    $td_id="active_customer_";
                                    $td_class="active_customer";
                                    $class="";
                                    $btn_id="suspend_";
                                    $btn_class="btn btn-danger";
                                    $btn_caption="Suspend";

                                    } ?>
                                    <tr data-url="/users/{{ $customer->id }}" id="row_{{$customer->id}}" class="{{$class}}">
                                        <td>{{ $customer->name }}</td>
                                        <td id="main_website_{{$customer->id}}"><a href="http://{{ $customer->main_website() }}" target="_blank"> {{ $customer->main_website() }}</a></td>
                                        <td id="counter_{{$customer->id}}">{{ $customer->properties->count() }}</td>
                                        <td id="{{ $td_id.$customer->id}}" class="{{$td_class}}">

                                        @if($customer->more_than_one_property()==false)
                                        <!-- <button id="changePlan_{{ $customer->id }}" value="{{ $customer->id }}" class="btn btn-success">Change Plan</button> -->
                                        @endif
                                        <button id="addWebsite_{{ $customer->id }}" value="{{ $customer->id }}" class="btn btn-default">Add Website</button>
                                        @if(count($customer->properties)!=0)
                                                <!-- <button id="update_{{ $customer->id }}" value="{{ $customer->id }}" class="btn btn-primary">Update</button> -->
                                                <button id="{{ $btn_id.$customer->id }}" value="{{ $customer->id }}" class="{{$btn_class}}">{{$btn_caption}}</button>
                                        @endif
                                        </td>
                                    </tr>
                                    @empty
                                    <p>No Customers</p>
                                @endforelse
                                    @endif
                                </tbody>
                            </table>
                            @if(isset($customers))
                            <div class="text-center">
                                {{ $customers->links() }}
                            </div>
                            @endif
                        </div>
                    </div>

@section('extra_js')

 $('#customer_table').on('mouseenter', 'tr', function() {
                $(this).addClass('hover');
            });
            $('#customer_table').on('mouseleave', 'tr', function() {
                $(this).removeClass('hover');
            });
@endsection