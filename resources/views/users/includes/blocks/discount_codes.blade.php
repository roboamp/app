<div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-header">Available Coupons
                                <a href="{{route('users.coupons.add')}}"><button id="test" class="btn btn-default add_customer">Add Coupon</button></a>
                            </div>
                        </div>
<div class="panel-body">
                            <table class="table table striped" id="coupons_table">
                                <thead>
                                <th style="width: 20%">Coupon ID</th>
                                <th style="width: 20%">Percent Off</th>
                                <th style="width: 20%">Redemptions</th>
                                <th style="width: 40%">Actions</th>
                                </thead>
                                <tbody id="customer_table">
                                    @if(!is_null($coupons))
                                        @foreach($coupons as $coupon)
                                        <tr data-url="{{$coupon->id}}" id="row_{{$coupon->id}}" class="">
                                            <td>{{$coupon->id}}</td>
                                            <td id="main_website_">{{$coupon->percent_off}}%</td>
                                            <td id="counter_">{{$coupon->times_redeemed."/".$coupon->max_redemptions}}</td>
                                            <td id="" class="">


                                            <button id="delete_coupon_{{$coupon->id}}" value="{{$coupon->id}}" class="btn btn-danger">Delete</button>

                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <p>No Coupons</p>
                                    @endif
                                </tbody>
                            </table>
                            @if(isset($customers))
                            <div class="text-center">
                                {{ $customers->links() }}
                            </div>
                            @endif
                        </div>
                        </div>
@section ('js_files')
    <script src="{{asset('js/bootstrap/util.js')}}"></script>
    <script src="{{asset('js/bootstrap/alert.js')}}"></script>
@endsection

@section('extra_js_coupons')
$("#coupons_table").on("click", ".btn", function () {

    //All button IDs begin with one word and camel cased followed by a unique ID #

    var action = ($(this)[0].id).split('_');
    var coupon_id = ($(this)[0].value);
    var button = action[1];
    var $action = action[0];



    switch($action){

            case "delete":
                    swal({
                        title: 'Are you sure?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Yes, Delete Coupon!'
                    }).then((result) => {

                        if (result.value) {
                        $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: 'POST',
                                url: '{{route('users.coupons.delete.post')}}/'+ coupon_id,
                                data: $('form').serialize(),
                                success:(response) => {
                                var arr = $.parseJSON(response);
                        if (arr.error === 0){
                            swal({
                                type: 'success',
                                title: 'Coupon has been deleted.',
                                timer: 2000,
                            });
                            $('#row_'+coupon_id).hide();
                        }
                        else {
                            swal({
                                type: 'error',
                                title: 'Sorry',
                                text: 'Something went wrong!',
                                timer: 5000,
                            })
                        }
                    }
                    });
                    }
                })

            break;
        }

    event.stopPropagation();
    });

    $('#coupons_table').on('mouseenter', 'tr', function() {
        $(this).addClass('hover');
    });

    $('#coupons_table').on('mouseleave', 'tr', function() {
        $(this).removeClass('hover');
    });
@endsection