<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-header">Available Error Pages
           <!-- <a href="{{route('users.coupons.add')}}"><button id="test" class="btn btn-default add_customer">Add Coupon</button></a>-->
        </div>
    </div>
<div class="panel-body">
                            <table class="table table striped" id="reports_table">
                                <thead>
                                <th style="width: 20%">URL</th>
                                <th style="width: 20%">Reported By</th>
                                <th style="width: 20%">Reported on</th>
                                <th style="width: 20%">Action</th>
                                </thead>
                                <tbody id="customer_table">
                                    @if(isset($errors) && !is_null($errors))
                                        @foreach($errors as $error)
                                        <tr data-url="{{$error->id}}" id="row_{{$error->id}}" class="">
                                            <td><a href="{{$error->url}}">{{$error->url}}</a></td>
                                            <td id="main_website_">{{$error->user->name}}</td>
                                            <td id="counter_">{{$error->created_at_for_humans()}}</td>
                                            <td id="" class="">


                                            <!--<button id="delete_coupon_" value="" class="btn btn-danger">Delete</button>-->

                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <p>No Errors</p>
                                    @endif
                                </tbody>
                            </table>
                            @if(isset($customers))
                            <div class="text-center">
                                {{ $customers->links() }}
                            </div>
                            @endif
                        </div>
                        </div>
@section ('js_files')
    <script src="{{asset('js/bootstrap/util.js')}}"></script>
    <script src="{{asset('js/bootstrap/alert.js')}}"></script>
@endsection

