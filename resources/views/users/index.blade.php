@extends('spark::layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-header">
                    {{ $customer->name }}
                    <button id="add_site" class="btn btn-default add_customer">Add Website</button>
                    <!-- <a href="/users/edit/{{$customer->id}}"><button id="edit_customer" value="{{ $customer->id }}" class="btn btn-primary">Edit Customer</button></a> -->
                </div>
            </div>

            <div class="panel-body">
                <table class="table table striped">
                    <thead>
                        <th style="width: 60%">Website</th>
                        <th style="width: 5%">Actions</th><th></th>
                        <th style="width: 35%">Status</th>
                    </thead>
                    <tbody id="website_table">
                        @foreach ($sites as $site)
                            @php $class=($site->status_id==3)?"website_suspended":"" @endphp
                                <tr id="row_{{$site->id}}" class="{{$class}}">
                                <td><a href="http://{{ $site->url }}" target="_blank"> {{ $site->url }}</a></td>
                                <?php
                                    $id=($site->status_id == 3)?'activate':'suspend';
                                    $class=($site->status_id == 3)?'btn-success':'btn-danger';
                                ?>

                                    <td id="action_{{$site->id}}">
                                        <button id="{{$id}}" value="{{ $site->id }}" class="btn {{$class}}">{{ucfirst($id)}}</button>
                                    </td>
                                    <td id="action_{{$site->id}}"><button id="show_code"  value="{{ $site->id }}" class="btn btn-primary">Show Code</button></td>

                                <td id="status_{{$site->id}}">{{ $site->status->status }}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $sites->links() }}
            </div>
        </div>
    </div>
</div>
<script type="">
    $( document ).ready(function() {
        $obj_url="{{$testing['url']}}";
        $obj_name="{{$testing['name']}}";
        $(".panel-header").on("click", "#add_site", function () {
            window.location.href = '/users/add/{{$customer->id}}';
        })

        $("#website_table").on("click", ".btn", function () {

            var action = ($(this)[0].id);
            var website_id = ($(this)[0].value);
            var update_id="";

            if(action=='show_code'){
                window.location.href="/users/show/code/"+website_id;
            }

            if(action == 'suspend'){
                // console.log('I am in the suspend method');
                swal({
                    title: 'Are you sure you want to suspend this site?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, suspend it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '/properties/'+ website_id + '/' + 3,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'This website has been suspended.',
                                        timer: 3000,
                                    });
                                    $obj1= $('#action_'+ website_id);
                                    $obj1.html('<td id="action_' + website_id + '"><button id="activate" value="'+ website_id +'" class="btn btn-success">Activate</button></td>');

                                    $obj2= $('#status_' + website_id);
                                    $obj2.html('Suspended');

                                    $('#row_' + website_id).addClass('website_suspended');

                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })
            }

            if(action == 'activate'){
                // console.log('I am in the suspend method');
                swal({
                    title: 'Are you sure you want to activate this site?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, activate it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        window.location="/properties/"+website_id;
                        aler("hi!")
                    }
                })
            }

        })

        


    });
</script>
@endsection 