

@extends('spark::layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-header">
                	Edit Customer - {{$customer->name}}
               	</div>
            </div>
            <div class="panel-body">
                <form id="status_form">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="name">Name: </label>
					<input type="name" name="name" value="{{$customer->name}}"/>
					<br>
					<label for="plan_id">Current Plan: </label>
				    <select name="plan_id" id="plan_id">

						<!-- grab all the different status
						//if the id matches the users' status id
						//then I select them, otherwise I just list it -->
						@foreach($plans as $plan)
							@if($plan->id == $customer->plan_id)
								<option value="{{$plan->id}}" selected>{{$plan->name}}</option>
							@else
								<option value="{{$plan->id}}">{{$plan->name}}</option>
							@endif
						@endforeach

						<!--BUILD FUNCTION THAT RETURNS THE BELOW

							//name of the object to be rendered
							//caption state
							//$array
							//ID

						A::B("the_name","select something", $banana,$user->status->id)
						-->
					</select>
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Update">
					<!-- <button type="submit" name="submit" class="btn btn-primary">Update</button> -->
				</div>
			</form>
            </div>
        </div>
    </div>
</div>
<script>
	$( document ).ready(function() {
    	$(function () {

	        $('form').on('submit', function (e) {
    	    	e.preventDefault();

        		$.ajax({
          			type: 'POST',
          			url: '/customers/{{$customer->id}}',
          			data: $('form').serialize(),
          			success: function () {
            			swal({
                            type: 'success',
                            title: 'Customer info has been updated!',
                            timer: 3000,
                        });
            		}
            	});
        	});
      	});
     });
    </script>
@endsection