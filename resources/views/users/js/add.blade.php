<script>
    $obj=Array();
    $( document ).ready(function() {
        $target=0;
        $beta="{{$default_plan}}";
        $('#btn_submit').prop('disabled',true);
        disable_submit_button();



        $('.cta.mt3.bfree').on('click',function(e){
            e.preventDefault();
        })




        var handler = StripeCheckout.configure({
            key: '{{$stripe_key}}',
            image: '{{$stripe_form_image}}',
            locale: 'auto',

            token: function(token) {
                $("#stripeEmail").val(token.email);
                $("#stripeToken").val(token.id);
                $.ajax({
                    type: 'POST',
                    url: '{{$process_stripe_url}}',
                    data: $('form').serialize(),
                    success:(response)=> {
                        var arr = $.parseJSON(response);
                        if (arr.error === 0){

                            $('form')[0].reset();
                            $('#btn_submit').prop('disabled',true);

                            swal({
                                type: 'success',
                                title: arr.success,
                                timer: 3000,
                                onClose(){
                                    redirect(arr.redirect);

                                }
                            });
                        }
                        else{
                            swal({
                                type: 'error',
                                title: 'Sorry',
                                text: arr.error_message,
                                timer: 5000,
                            })
                        }
                    },
                    error: function(data){
                        error_message(data,"You have not been Charged.")
                    }
                });
            }
        });

        @foreach ($plans as $plan)
            $tmp_obj=Array();
            $tmp_obj['plan_stripe_id']="{{$plan->stripe_id}}";
            $tmp_obj['plan_nickname']="{{$plan->nickname}}";
            $tmp_obj['plan_amount']={{$plan->amount}};
            @if($plan->default==1)
                $target="{{$plan->stripe_id}}";
            $("#txt_status_form").val($target);


            @endif
$obj.push($tmp_obj);
        @endforeach

$("#btn_submit").on("click",function(e){
            e.preventDefault();
            $status=$('#txt_status_form').val();

            $obj.forEach(function($e){
                if($e['plan_stripe_id']==$status.toString()){
                    if($status==$beta){


                        $.ajax({
                        headers: {
                                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  },
                          type: "POST",
                          url: "{{route("customers.process_beta_property")}}",
                          data: $('form').serialize(),
                          success:function (data){
                              success_message(data)
                          },
                            error: function(data){
                                error_message(data)
                            }


                        });

                        return true;


                    }else{
                        handler.open({
                            name: $e['plan_nickname'],
                            amount: $e['plan_amount'],
                            email: "{{$stripe_email}}"
                        })
                    }
                }
            })
        })

        $("#status_form").on('Submit',function(e){
            return 0;
            e.preventDefault();
        })

        //This function will find the plan selected
        $('.box').on('click', function($el) {


            $current_target=$el.currentTarget.id;
            $target=$("#"+$current_target).attr('class').split(' ')[0];

            $('.selected_plan').removeClass('selected_plan');

            $("."+$target).addClass('selected_plan');

            $("#txt_status_form").val($target);
        })
    });

    function disable_submit_button(){

        @if(isset($testing))
            $('#btn_submit').prop('disabled',false);
        @endif
        $('#url').keyup(function(){

            if(ValidURL($('#url').val())){
                $('#btn_submit').prop('disabled',false);
            }else{
                $('#btn_submit').prop('disabled',true);
            }


        });
        /////////////


    }
    function error_message(data,$message){

            var errors="";
            if(data!=null){
                 errors = data.error_message;

            }

        swal({
            type: 'error',
            title: $message,
            text: errors,
            timer: 5000,
            onClose(){
                history.go(-1);

            }
        });

    }

    function success_message($data,$message){

        var arr = $.parseJSON($data);
        if (arr.error === 0){

            $('form')[0].reset();
            $('#btn_submit').prop('disabled',true);

            swal({
                type: 'success',
                title: arr.success,
                timer: 3000,
                onClose(){
                redirect(arr.redirect);

                }
            });
        }else{
            error_message(null, arr.error);

        }
    }


    function redirect($url){
        if (typeof $url !== 'undefined') {
            window.location.href = $url;
        }else{
            window.location.href="{{$redirect_url}}"
        }
    }
    function ValidURL(str) {
  var pattern = new RegExp('^(https?:\/\/)?'+ // protocol
    '((([a-z\d]([a-z\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locater      if(!pattern.test(str)) {
     if(!pattern.test(str)) {

        return false;
      } else {
        return true;
      }
    }

</script>