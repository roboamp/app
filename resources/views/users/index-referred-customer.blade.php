@extends('spark::layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<home :customer="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-header">Welcome, {{$customer->name}} 
                        </div>
                        <div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table striped">
                            <tr>

                            </tr>
                        </table>
                        <div class="text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>
<script>
    $( document ).ready(function() {
        $("body").on("click", "#txt_banana", function () {
            alert('Hey');

        });
        $("#customer_table").on("click", ".btn", function () {

            //All button IDs begin with one word and camel cased followed by a unique ID #

            var action = ($(this)[0].id).split('_');
            var customer_id = ($(this)[0].value);
            var update_id="";
            // console.log(action);
            var button = action[1];
            var action = action[0];

            // console.log(action);
            // console.log(button);
            console.log(customer_id);


            
            event.stopPropagation();
            // console.log($(this).parent());

            if(action == 'changePlan'){
                console.log('Inside the change_plan method');
                swal({
                    title: 'Are you sure?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    text: "This will change plan to #3",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, change plan!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'PUT',
                            url: '/customers/'+ customer_id,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                console.log(arr);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'Your plan has been changed.',
                                        timer: 2000,
                                    });
                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })

            }
            if(action == 'update'){
                console.log('Inside the update method');
                swal({
                    type: 'info',
                    title: 'Update button placeholder!',
                    text: 'I will close in 5 seconds.',
                    timer: 5000,
                    onOpen: () => {
                        swal.showLoading()
                    }
                })
            }
            if(action == 'addWebsite'){
                console.log('Inside the add_website method');
                swal({
                    title: 'Add a site',
                    html:
                        '<form id="add_site_form">' +
                        '{{ csrf_field() }}' +
                        '<input id="url" class="swal2-input" name="url" placeholder="URL" value="'+$obj_url+'">' +
                        '</form>',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        return new Promise((resolve) =>{
                            setTimeout(() => {
                            if ($('#url').val() === '') {
                                swal.showValidationError(
                                'Url must be filled out'
                              )
                            }
                            resolve()
                            }, 2000)
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'POST',
                            url: '/properties/'+ customer_id,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                console.log(arr);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'Website has been added!',
                                        timer: 2000,
                                    });

                                if(arr.flag == true){
                                    console.log(arr);
                                    $obj_url=(arr.testing.url);
                                    $obj_name=(arr.testing.name);
                                    $obj= $('#main_website_'+customer_id);
                                    $obj.html('<a href=' + arr.url + '>' + arr.url + '</a>');
                                }

                                    console.log(arr);
                                    $obj_url=(arr.testing.url);
                                    $obj_name=(arr.testing.name);
                                    $obj= $('#counter_'+customer_id);
                                    $obj.html(arr.count);

                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })
            }
            if(action == 'suspend'){
                console.log('Inside the suspend method');
                swal({
                    title: 'Are you sure?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, suspend it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'PATCH',
                            url: '/customers/'+ customer_id + '/' + 3,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                console.log(arr);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'This customer has been suspended.',
                                        timer: 3000,
                                    });

                                    $obj1 = $('#active_customer_'+ customer_id);
                                    $obj1.html(
                                                '<td id="suspended_customer_'+ customer_id +'">\
                                                <button id="changePlan_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-success">Change Plan</button>\
                                                <button id="update_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-primary">Update</button>\
                                                <button id="addWebsite_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-default">Add Website</button>\
                                                <button id="activate_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-success">Activate</button>\
                                                </td>'
                                            );

                                    $('#row_' + customer_id).addClass('website_suspended');
                                    
                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })
            }

            if(action == 'activate'){
                console.log('Inside the activate method');
                swal({
                    title: 'Are you sure?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, activate it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'PATCH',
                            url: '/customers/'+ customer_id + '/' + 2,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                console.log(arr);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'This customer has been activated.',
                                        timer: 3000,
                                    });

                                    $obj1 = $('#suspended_customer_'+ customer_id);
                                    $obj1.html(
                                                '<td id="active_customer_'+ customer_id +'">\
                                                <button id="changePlan_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-success">Change Plan</button>\
                                                <button id="update_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-primary">Update</button>\
                                                <button id="addWebsite_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-default">Add Website</button>\
                                                <button id="suspend_'+ customer_id +'" value="'+ customer_id +'" class="btn btn-danger">Suspend</button>\
                                                </td>'
                                            );

                                    $('#row_' + customer_id).removeClass('website_suspended');
                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })
            }
        });


        $("#test").on("click", ".swal2-confirm swal2-styled", function () {
            alert('Hey');

        }
    );

        

        

        $('#customer_table').on('mouseenter', 'tr', function() {
            $(this).addClass('hover');
        });
        $('#customer_table').on('mouseleave', 'tr', function() {
            $(this).removeClass('hover');
        });


        //DISABLES BUTTONS ---- NEED TO FIND SOLUTION
        $('#customer_table').on('click', 'tr', function(e) {
            var elementType = $(e.target).prop('nodeName');
            // console.log(elementType);
            if (elementType == 'TD') {
                window.location.href = $(this).data('url');
            } else {

                // console.log(elementType);
            };
        })



    });
</script>
@endsection
