<script>
    var aa{{rand()}} = function () {

    // easy pie chart
        
        $('.{{$widget_name}}').easyPieChart({
            animate: 1000,
            size: 135,
            barColor: '{{$widget_color}}'
        });
        $('.{{$widget_name}}-light').easyPieChart({
            barColor: function(percent) {
                percent /= 100;
                return "rgb(" + Math.round(255 * (1-percent)) + ", " + Math.round(255 * percent) + ", 0)";
            },
            trackColor: '{{$widget_color}}',
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: 15,
            animate: 1000
        });

       

    }();
</script>