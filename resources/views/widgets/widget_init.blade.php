<section class="panel">
    <div class="revenue-head" style="background: {{$color}};">
        <span style="background: {{$color}};">
            <i class="fa fa-bar-chart-o"></i>
        </span>
        <h3>{{$label}}</h3>
        <span class="rev-combo pull-right" style="background: {{$color}};">
            {{$current_month}} {{$year=2018}}
        </span>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6 col-sm-6 text-center">
                <div class="easy-pie-chart">
                    <div class="{{$widget_name}}" data-percent="{{$progress}}"><span>{{$progress}}</span>%</div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="chart-info chart-position">
                    <span class="increase" style="background: {{$color}}"></span>
                    <span>Goal </span>
                </div>
                <div class="chart-info">
                    <span class="decrease"></span>
                    <span>Remaining </span>
                </div>
            </div>
        </div>
    </div>
</section>