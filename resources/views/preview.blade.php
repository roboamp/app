<!doctype html>
<html lang="en">
<head>
    <script type="text/javascript" src="js/biomp.js"></script>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div id="preview"></div>
<script>
    bioMp(document.getElementById('preview'), {
        url: 'http://127.0.0.1:8000/techstars',
        view: 'front',
        image: 'images/iphone6_front_gold.png'
    });
</script>
</body>
</html>