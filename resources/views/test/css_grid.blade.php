<!
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
    <style>
        .container{
            display: grid;
            width: 500px;
            margin: 100px auto;
            height:400px;
            background-color: #1abc9c;
            grid-template-columns: auto auto auto;

        }
        .item{
            border: 5px solid blue;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="item">1</div>
        <div class="item">2</div>
        <div class="item">3</div>
        <div class="item">4</div>
        <div class="item">5</div>
        <div class="item">6</div>
    </div>
</body>
</html>