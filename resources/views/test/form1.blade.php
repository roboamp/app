
<html>
<header>
    <style>
        /*! CSS Used from: http://fellowfellow.com/wp-content/plugins/wp-minify/min/?f=wp-content/themes/ffblog/style.css,wp-content/plugins/pinterest-rss-widget/style.css,wp-content/plugins/related-posts-thumbnails/assets/css/front.css&m=1523427416 ; media=screen */
        @media screen{
            section{display:block;}
            input,textarea{font-family:sans-serif;}
            a{outline:0;}
            h2{margin:0;padding:0;font-weight:normal;}
            ul{margin:0;padding:0;}
            form{margin:0;}
            input,textarea{font-size:100%;margin:0;vertical-align:baseline;*vertical-align:middle;}
            input{line-height:normal;}
            input[type="submit"]{cursor:pointer;-webkit-appearance:button;*overflow:visible;}
            input::-moz-focus-inner{border:0;padding:0;}
            textarea{overflow:auto;vertical-align:top;}
            section{width:586px;padding:13px 0 30px 30px;float:left;}
            h2{font-family:"Calibri",Arial,Helvetica,sans-serif;}
            h2.pagetitle{margin:50px 0 32px 0;font-weight:normal;font-size:30px;color:#868686;}
            .entry a{color:#919F97;text-decoration:underline;}
            .entry a:hover{text-decoration:none;color:#A39024;}
            .entry ul{list-style:none;margin:0 0 15px 15px;}
            .entry ul li{background:url(http://fellowfellow.com/wp-content/themes/ffblog/images/qd_bullet.gif) no-repeat 0 13px;padding:3px 0 3px 17px;}
            input{font:14px Arial, Helvetica, sans-serif;color:#c6c6c5;line-height:normal;}
            .gform_wrapper{background:#eff7f2 url(http://fellowfellow.com/wp-content/themes/ffblog/images/form-bg.gif) top center no-repeat;padding:50px 20px 20px 20px;margin:-20px 0 0 0;overflow:auto;border:1px solid #FFF;}
            .gform_heading{margin:16px 0;}
            .entry .gform_wrapper ul,.entry .gform_wrapper li{padding-left:0;margin:0;}
            label.gfield_label{color:#6f6f6e;display:block;margin-bottom:3px;}
            .gform_wrapper input,.gform_wrapper textarea{background:#FFF;color:#6f6f6e;font-family:Arial,Helvetica,sans-serif;border:0;padding:6px;margin:0 0 10px 0;font-size:1em;width:97%;}
            .gform_wrapper input.button{font-family:"MuseoSans-300",Arial,Helvetica,sans-serif;display:block;font-size:15px;padding:3px;width:115px;background:#f8b085;color:#fff;border:0;text-align:center;cursor:pointer;}
            .gform_wrapper input.button:hover{background:#f7c4a6;}
            .gform_validation_container{display:none;}
        }
        /*! CSS Used fontfaces */
        @font-face{font-family:'MuseoSans-300';src:url('http://fellowfellow.com/wp-content/themes/ffblog/webfonts/269341_0_0.eot');src:url('http://fellowfellow.com/wp-content/themes/ffblog/webfonts/269341_0_0.eot#iefix') format('embedded-opentype'),url('http://fellowfellow.com/wp-content/themes/ffblog/webfonts/269341_0_0.woff') format('woff'),url('http://fellowfellow.com/wp-content/themes/ffblog/webfonts/269341_0_0.ttf') format('truetype'),url('http://fellowfellow.com/wp-content/themes/ffblog/webfonts/269341_0_0.svg#wf') format('svg');}
    </style>
    <body>
    <section id="main-content"><h2 class="pagetitle">Contact me</h2><div class="entry"><div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1"><form method="post" enctype="multipart/form-data" id="gform_1" action="/contact/"><div class="gform_heading"> <span class="gform_description">If you’d like to get in touch, I’d love to hear from you! Feel free to fill in this form, or send me an email at: <a href="mailto:mail@fellowfellow.com">mail@fellowfellow.com</a>. Thank you! :)</span></div><div class="gform_body"><ul id="gform_fields_1" class="gform_fields top_label form_sublabel_below description_below"><li id="field_1_1" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label gfield_label_before_complex" for="input_1_1">Name<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_name"> <input name="input_1" id="input_1_1" type="text" value="" class="medium" tabindex="1" aria-required="true" aria-invalid="false"></div></li><li id="field_1_2" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_2">Email<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_email"> <input name="input_2" id="input_1_2" type="email" value="" class="medium" tabindex="2" aria-required="true" aria-invalid="false"></div></li><li id="field_1_3" class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_3">Message<span class="gfield_required">*</span></label><div class="ginput_container ginput_container_textarea"><textarea name="input_3" id="input_1_3" class="textarea medium" tabindex="3" aria-required="true" aria-invalid="false" rows="10" cols="50"></textarea></div></li><li id="field_1_4" class="gfield gform_validation_container field_sublabel_below field_description_below gfield_visibility_visible"><label class="gfield_label" for="input_1_4">Phone</label><div class="ginput_container"><input name="input_4" id="input_1_4" type="text" value="" autocomplete="off"></div><div class="gfield_description">This field is for validation purposes and should be left unchanged.</div></li></ul></div><div class="gform_footer top_label"> <input type="submit" id="gform_submit_button_1" class="gform_button button" value="Submit" tabindex="4" onclick="if(window[&quot;gf_submitting_1&quot;]){return false;}  if( !jQuery(&quot;#gform_1&quot;)[0].checkValidity || jQuery(&quot;#gform_1&quot;)[0].checkValidity()){window[&quot;gf_submitting_1&quot;]=true;}  " onkeypress="if( event.keyCode == 13 ){ if(window[&quot;gf_submitting_1&quot;]){return false;} if( !jQuery(&quot;#gform_1&quot;)[0].checkValidity || jQuery(&quot;#gform_1&quot;)[0].checkValidity()){window[&quot;gf_submitting_1&quot;]=true;}  jQuery(&quot;#gform_1&quot;).trigger(&quot;submit&quot;,[true]); }"> <input type="hidden" class="gform_hidden" name="is_submit_1" value="1"> <input type="hidden" class="gform_hidden" name="gform_submit" value="1"> <input type="hidden" class="gform_hidden" name="gform_unique_id" value=""> <input type="hidden" class="gform_hidden" name="state_1" value="WyJbXSIsIjAyNDgxNjkxYTBjNzM5Nzc4ZWE4NmM0OWEwZmMxMWZhIl0="> <input type="hidden" class="gform_hidden" name="gform_target_page_number_1" id="gform_target_page_number_1" value="0"> <input type="hidden" class="gform_hidden" name="gform_source_page_number_1" id="gform_source_page_number_1" value="1"> <input type="hidden" name="gform_field_values" value=""></div></form></div><script type="text/javascript">jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 1) {} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type="text/javascript">jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [1, 1]) } );</script> </div> </section>
    </body>
</header>
</html>

