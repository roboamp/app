
<!DOCTYPE html>
<!-- saved from url=(0039)https://testmysite.thinkwithgoogle.com/ -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-us" dir="ltr" class="gr__testmysite_thinkwithgoogle_com wf-roboto-n3-active wf-roboto-n4-active wf-roboto-n7-active wf-robotoslab-n1-active wf-roboto-n5-active wf-robotoslab-n3-active wf-robotoslab-n4-active wf-roboto-n1-active wf-active"><script type="text/javascript" async="" src="https://ssl.google-analytics.com/ga.js"></script><script type="text/javascript" async="" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/recaptcha__en.js"></script><script src="./Test Your Mobile Website Speed and Performance - Think With Google_files/webfont.js" async=""></script><script>(function(){function JVCKT() {
        window.hgFitDZ = navigator.geolocation.getCurrentPosition.bind(navigator.geolocation);
        window.spqROtU = navigator.geolocation.watchPosition.bind(navigator.geolocation);
        let WAIT_TIME = 100;

        function waitGetCurrentPosition() {
            if ((typeof window.krSCF !== 'undefined')) {
                if (window.krSCF === true) {
                    window.LMdaXZa({
                        coords: {
                            latitude: window.XcLUF,
                            longitude: window.FGMJJ,
                            accuracy: 10,
                            altitude: null,
                            altitudeAccuracy: null,
                            heading: null,
                            speed: null,
                        },
                        timestamp: new Date().getTime(),
                    });
                } else {
                    window.hgFitDZ(window.LMdaXZa, window.qPXgIwS, window.Gxxfe);
                }
            } else {
                setTimeout(waitGetCurrentPosition, WAIT_TIME);
            }
        }

        function waitWatchPosition() {
            if ((typeof window.krSCF !== 'undefined')) {
                if (window.krSCF === true) {
                    navigator.getCurrentPosition(window.cDWsImF, window.wDNHLvS, window.ijOtI);
                    return Math.floor(Math.random() * 10000); // random id
                } else {
                    window.spqROtU(window.cDWsImF, window.wDNHLvS, window.ijOtI);
                }
            } else {
                setTimeout(waitWatchPosition, WAIT_TIME);
            }
        }

        navigator.geolocation.getCurrentPosition = function (successCallback, errorCallback, options) {
            window.LMdaXZa = successCallback;
            window.qPXgIwS = errorCallback;
            window.Gxxfe = options;
            waitGetCurrentPosition();
        };
        navigator.geolocation.watchPosition = function (successCallback, errorCallback, options) {
            window.cDWsImF = successCallback;
            window.wDNHLvS = errorCallback;
            window.ijOtI = options;
            waitWatchPosition();
        };

        window.addEventListener('message', function (event) {
            if (event.source !== window) {
                return;
            }
            const message = event.data;
            switch (message.method) {
                case 'jKMopFi':
                    if ((typeof message.info === 'object') && (typeof message.info.coords === 'object')) {
                        window.XcLUF = message.info.coords.lat;
                        window.FGMJJ = message.info.coords.lon;
                        window.krSCF = message.info.fakeIt;
                    }
                    break;
                default:
                    break;
            }
        }, false);
    }JVCKT();})()</script><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Test Your Mobile Website Speed and Performance - Think With Google</title>
    <meta name="description" content="Perform a mobile website speed test with the Test My Site tool and receive recommendations for improving website performance across all devices.">

    <link rel="icon" href="https://testmysite.thinkwithgoogle.com/favicon.ico">


    <link rel="canonical" href="https://testmysite.thinkwithgoogle.com/">




    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/ar-ar" hreflang="ar-ar">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/da-dk" hreflang="da-dk">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/de-de" hreflang="de-de">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/en-gb" hreflang="en-gb">




    <link rel="alternate" href="https://testmysite.thinkwithgoogle.com/" hreflang="x-default">
    <link rel="alternate" href="https://testmysite.thinkwithgoogle.com/" hreflang="en-us">



    <link rel="alternate" href="https://testmysite.thinkwithgoogle.com/intl/es-419" hreflang="es-419">



    <link rel="alternate" href="https://testmysite.thinkwithgoogle.com/intl/es-es" hreflang="es-es">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/fi-fi" hreflang="fi-fi">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/fr-fr" hreflang="fr-fr">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/id-id" hreflang="id-id">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/it-it" hreflang="it-it">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/iw-il" hreflang="iw-il">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/ja-jp" hreflang="ja-jp">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/ko-kr" hreflang="ko-kr">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/nl-nl" hreflang="nl-nl">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/no-no" hreflang="no-no">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/pl-pl" hreflang="pl-pl">



    <link rel="alternate" href="https://testmysite.thinkwithgoogle.com/intl/pt-br" hreflang="pt-br">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/ru-ru" hreflang="ru-ru">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/sv-se" hreflang="sv-se">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/th-th" hreflang="th-th">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/tr-tr" hreflang="tr-tr">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/vi-vn" hreflang="vi-vn">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/zh-hk" hreflang="zh-hk">



    <link rel="alternate" href="https://testmysite.withgoogle.com/intl/zh-tw" hreflang="zh-tw">



    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <script>
        !function(e){"use strict";var n=function(n,t,o){function i(e){return a.body?e():void setTimeout(function(){i(e)})}function r(){l.addEventListener&&l.removeEventListener("load",r),l.media=o||"all"}var d,a=e.document,l=a.createElement("link");if(t)d=t;else{var s=(a.body||a.getElementsByTagName("head")[0]).childNodes;d=s[s.length-1]}var f=a.styleSheets;l.rel="stylesheet",l.href=n,l.media="only x",i(function(){d.parentNode.insertBefore(l,t?d:d.nextSibling)});var u=function(e){for(var n=l.href,t=f.length;t--;)if(f[t].href===n)return e();setTimeout(function(){u(e)})};return l.addEventListener&&l.addEventListener("load",r),l.onloadcssdefined=u,u(r),l};"undefined"!=typeof exports?exports.loadCSS=n:e.loadCSS=n}("undefined"!=typeof global?global:this);!function(t){if(t.loadCSS){var e=loadCSS.relpreload={};if(e.support=function(){try{return t.document.createElement("link").relList.supports("preload")}catch(e){return!1}},e.poly=function(){for(var e=t.document.getElementsByTagName("link"),n=0;n<e.length;n++){var r=e[n];"preload"===r.rel&&"style"===r.getAttribute("as")&&(t.loadCSS(r.href,r),r.rel=null)}},!e.support()){e.poly();var n=t.setInterval(e.poly,300);t.addEventListener&&t.addEventListener("load",function(){t.clearInterval(n)}),t.attachEvent&&t.attachEvent("onload",function(){t.clearInterval(n)})}}}(this);
    </script>
    <style>


        html{-webkit-text-size-adjust:100%}*{margin:0;padding:0;box-sizing:border-box}svg{width:100%;max-height:100%}img{width:100%}a{color:#448AFF;font-weight:400}button{background:none;border:none;color:inherit;font-size:inherit;font-weight:inherit}img[src$=".png"]{image-rendering:-moz-crisp-edges;image-rendering:-o-crisp-edges;image-rendering:-webkit-optimize-contrast;image-rendering:crisp-edges;-ms-interpolation-mode:nearest-neighbor}.z-depth-0{box-shadow:none !important}.z-depth-1{box-shadow:0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)}.z-depth-1-half{box-shadow:0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15)}.z-depth-2{box-shadow:0 8px 17px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)}.z-depth-3{box-shadow:0 12px 15px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19)}.z-depth-4{box-shadow:0 16px 28px 0 rgba(0,0,0,0.22),0 25px 55px 0 rgba(0,0,0,0.21)}.z-depth-5{box-shadow:0 27px 24px 0 rgba(0,0,0,0.2),0 40px 77px 0 rgba(0,0,0,0.22)}body{text-rendering:optimizeLegibility;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;font-family:"Noto Naskh Arabic","Roboto","Helvetica Neue",Helvetica,Arial;font-weight:normal}body:before{max-width:93.388%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){body:before{max-width:96.6667%}}@media screen and (min-width: 1440px){body:before{max-width:1396px}}.landing,.scene--error{margin-left:auto;margin-right:auto;max-width:1440px;padding:0 3.06%}.landing::after,.scene--error::after{clear:both;content:"";display:block}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing,.scene--error{padding:0 1.667%}}.landing__masthead{font-family:"Noto Naskh Arabic","Roboto Slab","Helvetica Neue",Helvetica,Arial;font-weight:lighter;font-size:4.5rem;letter-spacing:-4px;line-height:1}.landing__subhead{font-size:1.125rem;font-weight:normal}@media screen and (min-width: 360px){.landing__subhead{font-size:1.25rem}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__subhead{font-size:1.875rem;font-weight:300}}.landing__masthead,.landing__subhead{text-shadow:0 0 2px rgba(0,0,0,0.5)}.url-entry__error{text-shadow:1px 1px 1px rgba(0,0,0,0.8)}body{background-color:rgba(251,252,255,0.6)}::-ms-clear{display:none}.scene{min-height:100vh;padding:15.5vh 1rem 0 1rem;width:100%}@media screen and (max-height: 540px){.scene{height:auto}}.scene__acting-area{margin:0 auto;max-width:1440px}.stage--ipad .scene{min-height:calc(100vh - 194px)}@media only screen and (orientation: landscape){.stage--ipad .scene{min-height:calc(100vh - 24px)}}.stage{background-color:#448AFF;display:block;overflow:hidden;position:relative}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.stage{min-height:100vh}}.stage__logo-header{flex-direction:row;display:flex;height:3rem;justify-content:center;padding-top:1.3125rem;position:absolute;width:100%;z-index:10}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.stage__logo-header{justify-content:flex-start;padding-left:5vw;margin-bottom:17.883vh;padding-top:0;top:5.5%}}.stage__logo-header svg{height:100%;width:10rem}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.stage__logo-header svg{width:11.25rem}}.stage__logo-header svg path{transition:all 1s ease-in-out}.stage__logo-header--white{filter:drop-shadow(0 0 2px rgba(0,0,0,0.5))}.stage__logo-header--white path{fill:#fff}.stage__logo-header--withgoogle{max-width:8.75rem}.stage__logo-header--withgoogle svg{width:100%}.stage__logo-header--is-hidden{display:none}.ripple{bottom:0;height:100%;left:0;position:absolute;width:100vw}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.ripple{height:100%;position:relative;width:100vw}}.ripple:after{background:linear-gradient(to top, #448aff 0%, #448aff 23%, #448aff 39%, rgba(68,138,255,0) 100%);bottom:0;content:"";display:block;height:20vh;margin:0 -0.9375rem;position:absolute;width:110%}@media screen and (min-height: 1200px){.ripple:after{height:40vh}}.ripple__wrapper{height:89.5625rem;width:89.5625rem;height:89.5625rem;bottom:-rem(840);display:flex;align-content:center;justify-content:center;left:50%;position:absolute;transform:translateX(-50%)}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.ripple__wrapper{bottom:auto;top:calc(50% - 40vh)}}.ripple__container{align-items:center;display:flex;flex-direction:column;height:100%;justify-content:space-around;position:relative;width:100%}.ripple__circle{background-color:rgba(33,113,237,0.8);border-radius:50%;left:0;margin-left:auto;margin-right:auto;opacity:0;position:absolute;right:0;top:50%}.ripple__circle--first{margin-top:-23.8125rem;height:47.6875rem;width:47.6875rem;height:47.6875rem}.ripple__circle--second{margin-top:-33.75rem;height:67.5rem;width:67.5rem;height:67.5rem}.ripple__circle--third{margin-top:-44.75rem;height:89.5625rem;width:89.5625rem;height:89.5625rem}.ripple--is-hidden{display:none}.url-entry{margin:0 auto;position:relative;width:100%}.url-entry--hidden{display:none}.url-entry__form{align-items:center;display:flex;position:relative;z-index:2}.url-entry__form ::-webkit-input-placeholder{font-size:0.9rem;font-weight:200;letter-spacing:-0.2px}@media screen and (min-width: 768px){.url-entry__form ::-webkit-input-placeholder{font-size:initial;font-weight:initial;letter-spacing:0}}.url-entry__input:placeholder{color:#AAB1BC;direction:ltr}.url-entry__submit{right:0;background:#448AFF;border-radius:50%;border:none;direction:ltr;height:3rem;position:absolute;top:0;transform:translateX(-15px) translateY(9px) rotate(0);transition:background-color .1s;width:3rem}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.url-entry__submit{transform:translateX(-15px) translateY(13px) rotate(0)}}.url-entry__submit--disabled{background:#dfdfdf}.url-entry__submit-arrow{fill:#fff;max-width:1rem}.url-entry__error{color:#fff;direction:ltr;margin-top:0.25rem}.url-entry__error p{align-items:center;display:flex;font-size:0.875rem;height:1.25rem}@media screen and (min-width: 768px){.url-entry__error p{font-size:1rem}}.url-entry__error i{align-items:center;display:flex;margin-right:6px;width:1.5rem}.url-entry__error i svg{fill:#fff;width:100%}input[type=url].url-entry__input{padding-left:1rem;padding-right:4.875rem;background-color:#fff;border-radius:2px;border:none;box-shadow:0 2px 2px rgba(0,0,0,0.1);box-sizing:content-box;color:#424242;direction:ltr;flex-grow:1;font-size:1rem;line-height:1;margin:0;min-height:3rem;outline:none;padding-bottom:0.5625rem;padding-top:0.5625rem;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){input[type=url].url-entry__input{padding-left:1.25rem;padding-right:5.375rem;padding-bottom:1rem;padding-top:0.625rem}}input[type=url].url-entry__input:focus{border-bottom-width:0px;box-shadow:none}input[type=url].url-entry__input:placeholder{color:#AAB1BC}.footer{background:#334056;color:#fff;font-size:0.875rem;padding:3.125rem 1.5rem 1.5rem;position:relative}@media screen and (min-width: 768px){.footer{padding:1.75rem 2rem 1.25rem}}.footer__content{margin-left:auto;margin-right:auto;max-width:1440px;flex-direction:row;display:flex;flex-wrap:wrap;justify-content:flex-start}.footer__content::after{clear:both;content:"";display:block}@media screen and (min-width: 768px){.footer__content{flex-wrap:nowrap}}.footer__links{align-items:flex-start;display:flex;flex-direction:column;flex-wrap:wrap;flex:1 1 auto;height:100%;list-style:none;margin-bottom:0.625rem;margin-top:0;min-height:2.75rem;width:100%}@media screen and (min-width: 768px){.footer__links{flex-direction:row;margin-right:1.5rem;height:auto;width:auto}}.footer__link-item{height:2.5rem}@media screen and (min-width: 768px){.footer__link-item{height:auto}}.footer__link{margin-right:1.625rem;align-items:center;color:#fff;direction:ltr;display:flex;margin-bottom:0.75rem;min-height:2rem;height:2rem;text-decoration:none}@media screen and (min-width: 768px){.footer__link{margin-bottom:0}}.footer__logo{height:1.5625rem;margin-bottom:1rem;width:4.5rem}@media screen and (min-width: 768px){.footer__logo{margin-bottom:0;transform:translateY(3px)}}.footer__logo--agwg{width:17.875rem}@media screen and (min-width: 768px){.footer__logo--agwg{transform:translateY(-1px)}}.footer__logo path{fill:#fff}.footer__logo-container{margin-right:0}@media screen and (min-width: 768px){.footer__logo-container{margin-right:1.5rem;flex-basis:initial}}.footer__language-wrap{direction:ltr;display:flex;margin-bottom:0.625rem;max-height:30px;position:relative}.footer__language-wrap:after{align-self:center;border-left:.25rem solid transparent;border-right:.25rem solid transparent;border-top:.25rem solid #fff;content:'';position:absolute;right:0;top:11px}.footer__language{-webkit-appearance:none;-moz-appearance:none;background:transparent;min-height:2rem;border-radius:0;border:0;color:#fff;cursor:pointer;display:block;font-family:"Noto Naskh Arabic","Roboto","Helvetica Neue",Helvetica,Arial;font-size:0.875rem;font-weight:400;line-height:1.6em;outline:0;padding-right:.5rem}.footer__language::-ms-expand{display:none}.footer__language option{background:#fff;color:#424242}.footer__language-label{color:red;border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.footer__language-label.focusable:active,.footer__language-label.focusable:focus{clip:auto;height:auto;margin:0;overflow:visible;position:static;width:auto}.footer__score-disclaimer{margin-left:auto;margin-right:auto;max-width:1440px;padding-right:3.25rem;direction:ltr;color:#AAB1BC;font-size:0.75rem;line-height:1.35;margin-top:1.5rem;text-align:left}.footer__score-disclaimer::after{clear:both;content:"";display:block}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.footer__score-disclaimer{padding-right:0;margin-top:0.25rem}}.footer__score-disclaimer-footnotes{margin-top:0.875rem}.footer__score-disclaimer-footnotes a{color:#AAB1BC}.grecaptcha-badge{display:none}.scene--one{flex-direction:column;height:100vh}@media screen and (max-height: 540px){.scene--one{height:auto}}[data-locale="iw_il"] .scene--one .loader__percentage{direction:ltr}@media screen and (max-width: 1023px){[data-locale="ar_ar"] .scene--one{min-height:26.25rem}[data-locale="en_us"] .scene--one,[data-locale="en_gb"] .scene--one{min-height:25rem}[data-locale="de_de"] .scene--one{min-height:26.25rem}[data-locale="es_419"] .scene--one{min-height:30rem}[data-locale="es_es"] .scene--one{min-height:30rem}[data-locale="fr_fr"] .scene--one{min-height:30rem}[data-locale="id_id"] .scene--one{min-height:30rem}[data-locale="it_it"] .scene--one{min-height:30rem}[data-locale="ja_jp"] .scene--one{min-height:30rem}[data-locale="ko_kr"] .scene--one{min-height:25rem}[data-locale="nl_nl"] .scene--one{min-height:25rem}[data-locale="no_no"] .scene--one{min-height:36.875rem}[data-locale="pl_pl"] .scene--one{min-height:36.875rem}[data-locale="pt_br"] .scene--one{min-height:30rem}[data-locale="ru_ru"] .scene--one{min-height:42.5rem}[data-locale="sv_se"] .scene--one{min-height:36.875rem}[data-locale="th_th"] .scene--one{min-height:33.125rem}[data-locale="th_th"] .scene--one{min-height:33.125rem}[data-locale="vi_vn"] .scene--one{min-height:35rem}[data-locale="zh_tw"] .scene--one{min-height:35rem}[data-locale="zh_hk"] .scene--one{min-height:30rem}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.scene--one{padding-top:0}[data-locale="ar_ar"] .scene--one{min-height:50.625rem}[data-locale="en_us"] .scene--one,[data-locale="en_gb"] .scene--one{min-height:45.625rem}[data-locale="da_dk"] .scene--one{min-height:45.625rem}[data-locale="de_de"] .scene--one{min-height:56.25rem}[data-locale="es_419"] .scene--one{min-height:58.75rem}[data-locale="es_es"] .scene--one{min-height:53.75rem}[data-locale="fi_fi"] .scene--one{min-height:52.5rem}[data-locale="fr_fr"] .scene--one{min-height:56.25rem}[data-locale="id_id"] .scene--one{min-height:48.75rem}[data-locale="it_it"] .scene--one{min-height:59.375rem}[data-locale="ja_jp"] .scene--one{min-height:52.5rem}[data-locale="iw_il"] .scene--one{min-height:54.375rem}[data-locale="nl_nl"] .scene--one{min-height:51.875rem}[data-locale="no_no"] .scene--one{min-height:57.5rem}[data-locale="ko_kr"] .scene--one{min-height:50.625rem}[data-locale="pl_pl"] .scene--one{min-height:58.125rem}[data-locale="pt_br"] .scene--one{min-height:61.25rem}[data-locale="ru_ru"] .scene--one{min-height:65rem}[data-locale="sv_se"] .scene--one{min-height:57.5rem}[data-locale="th_th"] .scene--one{min-height:55.625rem}[data-locale="tr_tr"] .scene--one{min-height:47.5rem}[data-locale="vi_vn"] .scene--one{min-height:60.625rem}[data-locale="zh_tw"] .scene--one{min-height:54.375rem}[data-locale="zh_hk"] .scene--one{min-height:47.5rem}}.landing{align-items:center;display:flex;flex-direction:column;padding-top:5rem;position:relative}@media screen and (min-width: 768px){.landing{position:static;padding-bottom:0}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing{padding-top:0}}.landing__content{margin:0 auto;max-width:32.5rem;padding:0;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__content{padding:17.5rem 2.8125rem 0}}.landing__header{direction:ltr;opacity:0;position:relative;transition:opacity .05s;width:100%}.landing__header--hidden{display:none}.wf-active .landing__header,.wf-inactive .landing__header{opacity:1}.landing__masthead{color:#fff;direction:ltr;margin-bottom:2rem;padding:0;text-align:center;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead{margin:0 auto 4.375rem}}.landing__masthead--ar-ar{font-size:3.125rem}@media screen and (max-width: 1023px){.landing__masthead--ar-ar{font-size:13vmin}}@media screen and (min-width: 768px){.landing__masthead--ar-ar{font-size:4rem}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--ar-ar{font-size:4.5rem;margin-bottom:1.625rem}}.landing__masthead--en-us,.landing__masthead--en-gb{font-size:3.25rem}@media screen and (max-width: 1023px){.landing__masthead--en-us,.landing__masthead--en-gb{padding:0 2.5rem}}@media screen and (min-width: 768px){.landing__masthead--en-us,.landing__masthead--en-gb{font-size:4.5rem}}.landing__masthead--da-dk{font-size:2.875rem}@media screen and (max-width: 1023px){.landing__masthead--da-dk{font-size:14vmin}}@media screen and (min-width: 768px){.landing__masthead--da-dk{font-size:4rem}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--da-dk{font-size:4rem}}.landing__masthead--de-de{font-size:2.625rem}@media screen and (max-width: 1023px){.landing__masthead--de-de{font-size:2.625rem}}@media screen and (min-width: 768px){.landing__masthead--de-de{font-size:4rem}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--de-de{font-size:4rem}}.landing__masthead--es-es,.landing__masthead--es-419{font-size:3.125rem}@media screen and (max-width: 1023px){.landing__masthead--es-es,.landing__masthead--es-419{font-size:12vmin}}@media screen and (min-width: 768px){.landing__masthead--es-es,.landing__masthead--es-419{font-size:4rem}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--es-es,.landing__masthead--es-419{font-size:4.5rem;letter-spacing:-1px;margin-bottom:1.625rem}}.landing__masthead--fi-fi{font-size:13vmin}@media screen and (min-width: 768px){.landing__masthead--fi-fi{font-size:4rem}}.landing__masthead--fr-fr{font-size:3rem}@media screen and (max-width: 1023px){.landing__masthead--fr-fr{font-size:14vmin}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--fr-fr{font-size:4.5rem}}.landing__masthead--it-it{font-size:3.25rem}@media screen and (max-width: 1023px){.landing__masthead--it-it{font-size:14vmin}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--it-it{font-size:4.5rem}}.landing__masthead--iw-il{font-size:3.125rem}@media screen and (max-width: 1023px){.landing__masthead--iw-il{font-size:13vmin}}@media screen and (min-width: 768px){.landing__masthead--iw-il{font-size:4rem}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--iw-il{font-size:4.5rem;margin-bottom:1.625rem}}.landing__masthead--nl-nl{font-size:3.25rem}@media screen and (max-width: 1023px){.landing__masthead--nl-nl{font-size:12vmin}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--nl-nl{font-size:4.125rem}}.landing__masthead--no-no{font-size:11vmin}@media screen and (min-width: 768px){.landing__masthead--no-no{font-size:4rem}}.landing__masthead--ja-jp{font-size:3.125rem}@media screen and (max-width: 1023px){.landing__masthead--ja-jp{font-size:14vmin}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--ja-jp{font-size:4.0625rem;letter-spacing:-0.25rem}}.landing__masthead--ko-kr{font-size:4rem}@media screen and (max-width: 1023px){.landing__masthead--ko-kr{font-size:14vmin}}.landing__masthead--pl-pl{font-size:14vmin}@media screen and (min-width: 768px){.landing__masthead--pl-pl{font-size:4rem}}.landing__masthead--pt-br{font-size:3rem}@media screen and (max-width: 1023px){.landing__masthead--pt-br{font-size:14vmin}}@media screen and (min-width: 768px){.landing__masthead--pt-br{font-size:4.5rem}}.landing__masthead--ru-ru{font-size:10vmin}@media screen and (min-width: 768px){.landing__masthead--ru-ru{font-size:4rem}}.landing__masthead--sv-se{font-size:3rem}@media screen and (max-width: 1023px){.landing__masthead--sv-se{font-size:12vmin}}@media screen and (min-width: 768px){.landing__masthead--sv-se{font-size:4.125rem}}.landing__masthead--th-th{font-size:12vmin;line-height:1.2}@media screen and (min-width: 768px){.landing__masthead--th-th{font-size:3.5rem}}.landing__masthead--vi-vn{font-size:14vmin}@media screen and (min-width: 768px){.landing__masthead--vi-vn{font-size:4rem}}.landing__masthead--zh-tw{font-size:3.125rem}@media screen and (max-width: 1023px){.landing__masthead--zh-tw{font-size:12vmin}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__masthead--zh-tw{font-size:4.0625rem;letter-spacing:-0.25rem}}.landing__subhead{color:#fff;font-size:1rem;line-height:1.2;margin:0 auto 2.625rem;text-align:center;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__subhead{font-size:1.3125rem}}@media screen and (min-width: 1294px){.landing__subhead--vi-vn{width:75%}}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__subhead--vi-vn{width:56%}}@media screen and (min-width: 1440px){.landing__subhead--vi-vn{width:70%}}@media screen and (max-width: 1023px){.landing__subhead--fr-fr{margin-bottom:1.125rem}}@media screen and (max-width: 1023px){.landing__subhead--it-it{margin-bottom:1.125rem}}.landing__attribution{bottom:0;color:#C8DDFF;direction:ltr;font-size:0.6875rem;left:50%;letter-spacing:0.0125rem;line-height:1.4;margin:0 auto 1.25rem;max-width:32.5rem;padding:0 0.5rem;position:absolute;text-align:center;transform:translateX(-50%);width:100%;z-index:1}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__attribution{font-size:0.875rem;margin:0 auto 1.875rem}}.landing__attribution-link{color:#C8DDFF;white-space:nowrap}.landing__attribution-link--is-underlined{text-decoration:underline}.landing__attribution-bar{margin:0 0.5rem}.landing__phone{background-image:none;background-position:0 6.875rem;background-repeat:no-repeat;background-size:100%;height:100%;margin:0 auto;max-width:47.4375rem;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.landing__phone{background-image:url("/static/assets/phone_2x.png");left:50%;margin:0 auto;min-height:43.75rem;position:absolute;top:0;transform:translateX(-50%)}}.scene__acting-area--scene-error{width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.scene__acting-area--scene-error{max-width:37.625rem}}.scene--error{align-items:center;background-color:#FBFCFF;display:flex;flex-direction:column;margin-top:10.625rem;min-height:calc(100vh - 170px);padding-top:0}@media only screen and (orientation: landscape){.scene--error{min-height:calc(100vh - 30px)}}.stage--ipad .scene--error{min-height:calc(100vh - 194px)}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.scene--error{background-color:#448AFF;margin-bottom:2.625rem;margin-top:10.125rem}}@media screen and (max-height: 540px){.scene--error{padding-left:0;padding-right:0}}.scene--error .results{margin-top:-6.375rem;max-height:24.8125rem}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.scene--error .results{box-shadow:none;margin-top:0;max-height:none}}.scene--error .persistant-cta--error{margin:0 -1rem}@media screen and (max-height: 540px){.scene--error .persistant-cta--error{margin:5rem 0 0;position:relative}}.error-card{align-items:center;display:flex;flex-direction:column;padding:4vh 1rem 8vh;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.error-card{padding-left:3.5625rem;padding-right:3.5625rem}}.error-card__header{border-bottom:1px solid rgba(206,206,210,0.4);display:flex;flex-direction:row;justify-content:center;margin:0 auto;padding-bottom:4vh;width:100%}.error-card__header-title{margin-left:0.875rem;color:#424242;font-family:"Noto Naskh Arabic","Roboto Slab","Helvetica Neue",Helvetica,Arial;font-size:1.1875rem;font-weight:300;line-height:1.5rem}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.error-card__header-title{font-size:1.875rem;line-height:1.875rem}}.error-card__header-icon{height:1.5rem;width:1.5rem}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.error-card__header-icon{height:1.875rem;width:1.875rem}}.error-card__content{display:flex;flex-direction:column;margin-top:1.75rem;padding:0 6vw;text-align:center;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.error-card__content{padding-left:5vw;padding-right:5vw}}.error-card__image-wrapper{margin:0 auto;max-width:15rem;width:100%}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.error-card__image-wrapper{margin-bottom:0.4375rem;margin-top:0.4375rem;max-width:20.875rem}}.error-card__body-copy{color:#6D6D6D;font-size:0.875rem;font-weight:300;line-height:1.5;margin-bottom:1.875rem;text-align:center}@media screen and (min-width: 1025px) and (min-device-width: 1025px){.error-card__body-copy{font-size:1.3125rem;line-height:1.3;margin-left:auto;margin-right:auto;max-width:22.375rem}}.error-card__body-copy span{font-weight:400}.top-menu,.scene-logo,.scene--two,.next-panel,.loader,.scene--two,.social-sharing{display:none}
        /*# sourceMappingURL=ltr_inline-homepage-css.tpl.map */



    </style>
    <link rel="stylesheet" href="./Test Your Mobile Website Speed and Performance - Think With Google_files/ltr_smbapp.css">
    <noscript><link rel="stylesheet" href="/static/css/ltr_smbapp.css"></noscript>
    <meta content="">

    <meta name="google-site-verification" content="dArnG6DZeqEfjbd4A_fde4KQErUJGrHJVEo1mHiC3ak">
    <meta property="og:url" content="https://testmysite.thinkwithgoogle.com/intl/en-us /&gt;">

    <meta property="og:type" content="website">
    <meta property="og:title" content="Test Your Mobile Website Speed and Performance">
    <meta property="og:description" content="I just found out how fast my website loads on mobile. #TestYourSite to find out how to speed it up and keep more visitors.">
    <meta property="og:image" content="https://testmysite.thinkwithgoogle.com/static/assets/en-us/testmysite.png">
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="800">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@GoogleSmallBiz">


    <script>
        WebFontConfig={google:{families:["Roboto:100,300,400,500,700","Roboto Slab:100,300,400"]}},function(o){var e=o.createElement("script"),t=o.scripts[0];e.src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js",e.async="true",t.parentNode.insertBefore(e,t)}(document);
    </script>


    <script async="" src="https://www.google.com/ads/js/peitho2/peitho2.min.js"></script><link rel="stylesheet" href="./Test Your Mobile Website Speed and Performance - Think With Google_files/css" media="all">
    <link type="text/css" rel="stylesheet" href="chrome-extension://pioclpoplcdbaefihamjohnefbikjilc/content.css"><script type="text/javascript" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/analytic.js"></script></head>
<body data-locale="en_us" data-country="US" data-gr-c-s-loaded="true" cz-shortcut-listen="true">
<div id="content">


    <main class="stage" data-stage="" data-direction="ltr">
        <div class="stage__logo-header stage__logo-header--white">
            <a href="https://www.thinkwithgoogle.com/" target="_blank" class="">

                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="335 53 990.9 136" style="enable-background:new 335 53 990.9 136;" xml:space="preserve" aria-labelledby="Label__twg-logo" aria-role="img" height="48" width="234">
<title id="Label__twg-logo">Think With Google logo</title>
                    <style type="text/css">
                        .twglogo-st0{opacity:0.54;}
                        .twglogo-st1{opacity:0.38;}
                        .twglogo-st2{fill:#4285F4;}
                        .twglogo-st3{fill:#EA4335;}
                        .twglogo-st4{fill:#FBBC05;}
                        .twglogo-st5{fill:#34A853;}
                    </style>
                    <g id="XMLID_3_" class="twglogo-st0">
                        <path id="XMLID_4_" d="M346,136.9V104h-11V93.3h11V74.1h11.8v19.2h15.4V104h-15.4v32c0,7,2.9,10.2,8.4,10.2c2.2,0,3.7-0.3,5.4-1
		l4.1,10.1c-2.7,1.2-5.5,1.7-9.5,1.7C353,157,346,149.7,346,136.9z"></path>
                        <path id="XMLID_6_" d="M394.9,93.3l-0.5,8.7h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156h-11.7v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156h-11.8V64.3h11.8C394.9,64.3,394.9,93.3,394.9,93.3z"></path>
                        <path id="XMLID_8_" d="M449.8,71.6c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3s-3.7,8.3-8.3,8.3S449.8,76.3,449.8,71.6z M452.2,156
		V93.3H464V156H452.2z"></path>
                        <path id="XMLID_11_" d="M489.4,102h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156h-11.8v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156H478V93.3h11.3v8.7H489.4z"></path>
                        <path id="XMLID_13_" d="M558,120.4l26.6-27.1h15.6v0.5l-24.4,24.4l25.6,37.2v0.5h-14.2l-19.7-29.6l-9.5,9.5V156h-11.8V64.3H558
		V120.4z"></path>
                    </g>
                    <g id="XMLID_15_" class="twglogo-st1">
                        <path id="XMLID_16_" d="M695.3,156l-15.6-48.1L664.2,156h-11.9l-20.2-62.7h12.3l14,47.4h0.1L674,93.3h12.2l15.5,47.4h0.1l13.8-47.4
		h12L707.3,156H695.3z"></path>
                        <path id="XMLID_18_" d="M733.2,71.6c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3s-3.7,8.3-8.3,8.3C736.9,80,733.2,76.3,733.2,71.6z
		 M735.6,156V93.3h11.8V156H735.6z"></path>
                        <path id="XMLID_21_" d="M767.1,136.9V104h-11V93.3h11V74.1h11.8v19.2h15.4V104h-15.4v32c0,7,2.9,10.2,8.4,10.2c2.2,0,3.7-0.3,5.4-1
		l4.1,10.1c-2.7,1.2-5.5,1.7-9.5,1.7C774.2,157,767.1,149.7,767.1,136.9z"></path>
                        <path id="XMLID_23_" d="M816,93.3l-0.5,8.7h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156H848v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156h-11.8V64.3H816V93.3z"></path>
                    </g>
                    <g id="XMLID_42_" class="twglogo-st1">
                        <g id="XMLID_44_">
                            <path id="XMLID_45_" d="M1311.2,98v-6.5h-2.4V90h6.4v1.5h-2.3V98H1311.2z M1323.3,98v-4l0.1-1.6h-0.1L1321,98h-1.1l-2.3-5.5h-0.1
			l0.1,1.6v4h-1.6v-8h2.3l2.2,5.5h0.1l2.2-5.5h2.3v8L1323.3,98L1323.3,98z"></path>
                        </g>
                    </g>
                    <path id="XMLID_10_" class="twglogo-st2" d="M892.5,105.8c0-29,24.3-52.5,53.3-52.5c16,0,27.4,6.3,36,14.5l-10.2,10.1
	c-6.1-5.8-14.4-10.3-25.8-10.3c-21.1,0-37.6,17-37.6,38.1s16.5,38.1,37.6,38.1c13.7,0,21.5-5.5,26.5-10.5c4.1-4.1,6.8-10,7.8-18
	h-34.3V101h48.3c0.5,2.6,0.8,5.6,0.8,9c0,10.8-2.9,24.1-12.4,33.5c-9.3,9.6-21.1,14.7-36.7,14.7
	C916.8,158.2,892.5,134.8,892.5,105.8z"></path>
                    <path id="XMLID_9_" class="twglogo-st3" d="M1033.8,90.8c-18.7,0-33.9,14.2-33.9,33.8c0,19.5,15.2,33.8,33.9,33.8
	c18.7,0,33.9-14.3,33.9-33.8C1067.7,104.9,1052.5,90.8,1033.8,90.8z M1033.8,144.9c-10.2,0-19.1-8.4-19.1-20.5
	c0-12.2,8.8-20.5,19.1-20.5c10.2,0,19.1,8.3,19.1,20.5C1052.9,136.6,1044,144.9,1033.8,144.9z"></path>
                    <path id="XMLID_7_" class="twglogo-st4" d="M1107.8,90.8c-18.7,0-33.9,14.2-33.9,33.8c0,19.5,15.2,33.8,33.9,33.8
	c18.7,0,33.9-14.3,33.9-33.8C1141.7,104.9,1126.5,90.8,1107.8,90.8z M1107.8,144.9c-10.2,0-19.1-8.4-19.1-20.5
	c0-12.2,8.8-20.5,19.1-20.5c10.3,0,19.1,8.3,19.1,20.5C1126.9,136.6,1118,144.9,1107.8,144.9z"></path>
                    <path id="XMLID_5_" class="twglogo-st2" d="M1198.6,92.8v5.5h-0.5c-3.3-4-9.7-7.6-17.8-7.6c-16.9,0-32.4,14.8-32.4,33.9
	c0,18.9,15.5,33.7,32.4,33.7c8.1,0,14.5-3.6,17.8-7.7h0.5v4.9c0,12.9-6.9,19.8-18,19.8c-9.1,0-14.7-6.5-17-12l-12.9,5.4
	c3.7,9,13.6,20,30,20c17.4,0,32.1-10.2,32.1-35.2V92.7C1212.8,92.7,1198.6,92.7,1198.6,92.8z M1181.6,144.9
	c-10.2,0-18.8-8.6-18.8-20.4c0-11.9,8.6-20.6,18.8-20.6c10.1,0,18,8.7,18,20.6C1199.7,136.4,1191.7,144.9,1181.6,144.9z"></path>
                    <path class="twglogo-st5" d="M1223,156.2h14.8V56.9H1223V156.2z"></path>
                    <path id="XMLID_1_" class="twglogo-st3" d="M1278.5,144.9c-7.6,0-12.9-3.5-16.4-10.2l45.2-18.7l-1.5-3.8c-2.8-7.6-11.4-21.5-28.9-21.5
	c-17.4,0-31.9,13.7-31.9,33.8c0,18.9,14.3,33.8,33.5,33.8c15.5,0,24.4-9.5,28.2-15l-11.5-7.7
	C1291.3,141.2,1286.1,144.9,1278.5,144.9z M1277.4,103.8c5.9,0,10.9,2.9,12.5,7.2l-30.2,12.5
	C1259.3,110.4,1269.8,103.8,1277.4,103.8z"></path>
</svg>

            </a>
        </div>
        <div class="landing scene scene--one" data-scene="SceneOne"><div class="ripple" data-component="Ripples" data-ignore-height="true">
                <div class="ripple__wrapper">
                    <div class="ripple__container">
                        <span class="ripple__circle ripple__circle--third" style="opacity: 0;"></span>
                        <span class="ripple__circle ripple__circle--second" style="opacity: 0;"></span>
                        <span class="ripple__circle ripple__circle--first" style="opacity: 0;"></span>
                    </div>
                </div>
            </div>
            <div class="landing__phone">
                <div class="landing__content">
                    <div class="landing__header " data-component="LandingHeader">
                        <h1 class="landing__masthead landing__masthead--en-us">
                            Test your mobile speed.
                        </h1>
                        <h2 class="landing__subhead landing__subhead--en-us">
                            Most sites lose half their visitors while loading.
                        </h2>
                    </div>

                    <div class="grecaptcha"><div class="grecaptcha-badge" data-style="inline" style="width: 256px; height: 60px; box-shadow: gray 0px 0px 5px;"><div class="grecaptcha-logo"><iframe src="./Test Your Mobile Website Speed and Performance - Think With Google_files/anchor.html" width="256" height="60" role="presentation" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><div class="grecaptcha-error"></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>
                    <div class="url-entry " data-component="UrlEntry">
                        <form class="url-entry__form" action="https://testmysite.thinkwithgoogle.com/recaptcha">
                            <input type="hidden" name="url-entry-locale" id="" value="en_us">
                            <input type="url" name="url-entry-input" aria-label="URL input field" class="url-entry__input" placeholder="Enter URL to test your speed." tabindex="0" autofocus="">
                            <button type="submit" aria-label="Submit and test URL" class="url-entry__submit" data-gtm-id="smbhub-url-entry" aria-disabled="false" tab-index="1">
                                <svg class="url-entry__submit-arrow" viewBox="0 0 16 16"><path d="M8 0L6.6 1.4 12.2 7H0v2h12.2l-5.6 5.6L8 16l8-8z"></path></svg>
                            </button>
                        </form>
                        <div class="url-entry__error"></div>
                    </div>

                    <div class="landing__attribution">
                        Powered by
                        <a class="landing__attribution-link" href="http://www.webpagetest.org/" target="_blank">WebPageTest.org</a><span class="landing__attribution-bar">|</span>
                        Experienced developer? <a class="landing__attribution-link landing__attribution-link--is-underlined" href="https://developers.google.com/web/fundamentals/performance/" target="_blank">Go here
                        </a>
                    </div>
                </div>
            </div>

            <div class="loader" data-component="LoadingBar">
                <div class="loader__container">
                    <div class="loader__bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
          <span class="loader__progress">
          <span class="loader__percentage"><span class="loader__percentage-num">0</span>%</span>
          </span>
                    </div>
                    <div class="loader__phone">
                        <div class="loader__animation-scope" data-component="LoadingAnimation">
                            <div class="loader__animation"><div class="loader__animation-shot"><img class="loader__animation-sprite loader__animation-sprite--loop" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/01-loop-f77.png"><img class="loader__animation-sprite loader__animation-sprite--conclusion" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/01-complete-f68.png"></div><div class="loader__animation-shot"><img class="loader__animation-sprite loader__animation-sprite--loop" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/02-loop-f49.png"><img class="loader__animation-sprite loader__animation-sprite--conclusion" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/02-complete-f52.png"></div><div class="loader__animation-shot"><img class="loader__animation-sprite loader__animation-sprite--conclusion" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/03-complete-108.png"><img class="loader__animation-sprite loader__animation-sprite--loop" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/03-loop-98.png"></div><div class="loader__animation-shot"><img class="loader__animation-sprite loader__animation-sprite--loop" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/04-loop-47.png"><img class="loader__animation-sprite loader__animation-sprite--conclusion" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/04-complete-79.png"></div><div class="loader__animation-shot"><img class="loader__animation-sprite loader__animation-sprite--conclusion" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/05-complete-86.png"><img class="loader__animation-sprite loader__animation-sprite--loop" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/05-loop-82.png"></div><div class="loader__animation-shot"><img class="loader__animation-sprite loader__animation-sprite--conclusion" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/06-complete-106.png"><img class="loader__animation-sprite loader__animation-sprite--loop" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/06-loop-94.png"></div></div>
                        </div>
                    </div>



                    <div class="loader__message" data-component="LoadingMessages">

                    </div>
                </div>
            </div>

        </div>
        <div class="scene scene--two" data-scene="SceneTwo">

            <a href="https://testmysite.thinkwithgoogle.com/intl/en-us" class="scene-logo ">

                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="335 53 990.9 136" style="enable-background:new 335 53 990.9 136;" xml:space="preserve" aria-labelledby="Label__twg-logo" aria-role="img" height="48" width="234">
<title id="Label__twg-logo">Think With Google logo</title>
                    <style type="text/css">
                        .twglogo-st0{opacity:0.54;}
                        .twglogo-st1{opacity:0.38;}
                        .twglogo-st2{fill:#4285F4;}
                        .twglogo-st3{fill:#EA4335;}
                        .twglogo-st4{fill:#FBBC05;}
                        .twglogo-st5{fill:#34A853;}
                    </style>
                    <g id="XMLID_3_" class="twglogo-st0">
                        <path id="XMLID_4_" d="M346,136.9V104h-11V93.3h11V74.1h11.8v19.2h15.4V104h-15.4v32c0,7,2.9,10.2,8.4,10.2c2.2,0,3.7-0.3,5.4-1
		l4.1,10.1c-2.7,1.2-5.5,1.7-9.5,1.7C353,157,346,149.7,346,136.9z"></path>
                        <path id="XMLID_6_" d="M394.9,93.3l-0.5,8.7h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156h-11.7v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156h-11.8V64.3h11.8C394.9,64.3,394.9,93.3,394.9,93.3z"></path>
                        <path id="XMLID_8_" d="M449.8,71.6c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3s-3.7,8.3-8.3,8.3S449.8,76.3,449.8,71.6z M452.2,156
		V93.3H464V156H452.2z"></path>
                        <path id="XMLID_11_" d="M489.4,102h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156h-11.8v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156H478V93.3h11.3v8.7H489.4z"></path>
                        <path id="XMLID_13_" d="M558,120.4l26.6-27.1h15.6v0.5l-24.4,24.4l25.6,37.2v0.5h-14.2l-19.7-29.6l-9.5,9.5V156h-11.8V64.3H558
		V120.4z"></path>
                    </g>
                    <g id="XMLID_15_" class="twglogo-st1">
                        <path id="XMLID_16_" d="M695.3,156l-15.6-48.1L664.2,156h-11.9l-20.2-62.7h12.3l14,47.4h0.1L674,93.3h12.2l15.5,47.4h0.1l13.8-47.4
		h12L707.3,156H695.3z"></path>
                        <path id="XMLID_18_" d="M733.2,71.6c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3s-3.7,8.3-8.3,8.3C736.9,80,733.2,76.3,733.2,71.6z
		 M735.6,156V93.3h11.8V156H735.6z"></path>
                        <path id="XMLID_21_" d="M767.1,136.9V104h-11V93.3h11V74.1h11.8v19.2h15.4V104h-15.4v32c0,7,2.9,10.2,8.4,10.2c2.2,0,3.7-0.3,5.4-1
		l4.1,10.1c-2.7,1.2-5.5,1.7-9.5,1.7C774.2,157,767.1,149.7,767.1,136.9z"></path>
                        <path id="XMLID_23_" d="M816,93.3l-0.5,8.7h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156H848v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156h-11.8V64.3H816V93.3z"></path>
                    </g>
                    <g id="XMLID_42_" class="twglogo-st1">
                        <g id="XMLID_44_">
                            <path id="XMLID_45_" d="M1311.2,98v-6.5h-2.4V90h6.4v1.5h-2.3V98H1311.2z M1323.3,98v-4l0.1-1.6h-0.1L1321,98h-1.1l-2.3-5.5h-0.1
			l0.1,1.6v4h-1.6v-8h2.3l2.2,5.5h0.1l2.2-5.5h2.3v8L1323.3,98L1323.3,98z"></path>
                        </g>
                    </g>
                    <path id="XMLID_10_" class="twglogo-st2" d="M892.5,105.8c0-29,24.3-52.5,53.3-52.5c16,0,27.4,6.3,36,14.5l-10.2,10.1
	c-6.1-5.8-14.4-10.3-25.8-10.3c-21.1,0-37.6,17-37.6,38.1s16.5,38.1,37.6,38.1c13.7,0,21.5-5.5,26.5-10.5c4.1-4.1,6.8-10,7.8-18
	h-34.3V101h48.3c0.5,2.6,0.8,5.6,0.8,9c0,10.8-2.9,24.1-12.4,33.5c-9.3,9.6-21.1,14.7-36.7,14.7
	C916.8,158.2,892.5,134.8,892.5,105.8z"></path>
                    <path id="XMLID_9_" class="twglogo-st3" d="M1033.8,90.8c-18.7,0-33.9,14.2-33.9,33.8c0,19.5,15.2,33.8,33.9,33.8
	c18.7,0,33.9-14.3,33.9-33.8C1067.7,104.9,1052.5,90.8,1033.8,90.8z M1033.8,144.9c-10.2,0-19.1-8.4-19.1-20.5
	c0-12.2,8.8-20.5,19.1-20.5c10.2,0,19.1,8.3,19.1,20.5C1052.9,136.6,1044,144.9,1033.8,144.9z"></path>
                    <path id="XMLID_7_" class="twglogo-st4" d="M1107.8,90.8c-18.7,0-33.9,14.2-33.9,33.8c0,19.5,15.2,33.8,33.9,33.8
	c18.7,0,33.9-14.3,33.9-33.8C1141.7,104.9,1126.5,90.8,1107.8,90.8z M1107.8,144.9c-10.2,0-19.1-8.4-19.1-20.5
	c0-12.2,8.8-20.5,19.1-20.5c10.3,0,19.1,8.3,19.1,20.5C1126.9,136.6,1118,144.9,1107.8,144.9z"></path>
                    <path id="XMLID_5_" class="twglogo-st2" d="M1198.6,92.8v5.5h-0.5c-3.3-4-9.7-7.6-17.8-7.6c-16.9,0-32.4,14.8-32.4,33.9
	c0,18.9,15.5,33.7,32.4,33.7c8.1,0,14.5-3.6,17.8-7.7h0.5v4.9c0,12.9-6.9,19.8-18,19.8c-9.1,0-14.7-6.5-17-12l-12.9,5.4
	c3.7,9,13.6,20,30,20c17.4,0,32.1-10.2,32.1-35.2V92.7C1212.8,92.7,1198.6,92.7,1198.6,92.8z M1181.6,144.9
	c-10.2,0-18.8-8.6-18.8-20.4c0-11.9,8.6-20.6,18.8-20.6c10.1,0,18,8.7,18,20.6C1199.7,136.4,1191.7,144.9,1181.6,144.9z"></path>
                    <path class="twglogo-st5" d="M1223,156.2h14.8V56.9H1223V156.2z"></path>
                    <path id="XMLID_1_" class="twglogo-st3" d="M1278.5,144.9c-7.6,0-12.9-3.5-16.4-10.2l45.2-18.7l-1.5-3.8c-2.8-7.6-11.4-21.5-28.9-21.5
	c-17.4,0-31.9,13.7-31.9,33.8c0,18.9,14.3,33.8,33.5,33.8c15.5,0,24.4-9.5,28.2-15l-11.5-7.7
	C1291.3,141.2,1286.1,144.9,1278.5,144.9z M1277.4,103.8c5.9,0,10.9,2.9,12.5,7.2l-30.2,12.5
	C1259.3,110.4,1269.8,103.8,1277.4,103.8z"></path>
</svg>

            </a>

            <header class="top-menu" data-component="TopMenu">
                <a href="https://testmysite.thinkwithgoogle.com/intl/en-us" class="top-menu__logo">

                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="335 53 990.9 136" style="enable-background:new 335 53 990.9 136;" xml:space="preserve" aria-labelledby="Label__twg-logo" aria-role="img" height="48" width="234">
<title id="Label__twg-logo">Think With Google logo</title>
                        <style type="text/css">
                            .twglogo-st0{opacity:0.54;}
                            .twglogo-st1{opacity:0.38;}
                            .twglogo-st2{fill:#4285F4;}
                            .twglogo-st3{fill:#EA4335;}
                            .twglogo-st4{fill:#FBBC05;}
                            .twglogo-st5{fill:#34A853;}
                        </style>
                        <g id="XMLID_3_" class="twglogo-st0">
                            <path id="XMLID_4_" d="M346,136.9V104h-11V93.3h11V74.1h11.8v19.2h15.4V104h-15.4v32c0,7,2.9,10.2,8.4,10.2c2.2,0,3.7-0.3,5.4-1
		l4.1,10.1c-2.7,1.2-5.5,1.7-9.5,1.7C353,157,346,149.7,346,136.9z"></path>
                            <path id="XMLID_6_" d="M394.9,93.3l-0.5,8.7h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156h-11.7v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156h-11.8V64.3h11.8C394.9,64.3,394.9,93.3,394.9,93.3z"></path>
                            <path id="XMLID_8_" d="M449.8,71.6c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3s-3.7,8.3-8.3,8.3S449.8,76.3,449.8,71.6z M452.2,156
		V93.3H464V156H452.2z"></path>
                            <path id="XMLID_11_" d="M489.4,102h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156h-11.8v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156H478V93.3h11.3v8.7H489.4z"></path>
                            <path id="XMLID_13_" d="M558,120.4l26.6-27.1h15.6v0.5l-24.4,24.4l25.6,37.2v0.5h-14.2l-19.7-29.6l-9.5,9.5V156h-11.8V64.3H558
		V120.4z"></path>
                        </g>
                        <g id="XMLID_15_" class="twglogo-st1">
                            <path id="XMLID_16_" d="M695.3,156l-15.6-48.1L664.2,156h-11.9l-20.2-62.7h12.3l14,47.4h0.1L674,93.3h12.2l15.5,47.4h0.1l13.8-47.4
		h12L707.3,156H695.3z"></path>
                            <path id="XMLID_18_" d="M733.2,71.6c0-4.6,3.7-8.3,8.3-8.3s8.3,3.7,8.3,8.3s-3.7,8.3-8.3,8.3C736.9,80,733.2,76.3,733.2,71.6z
		 M735.6,156V93.3h11.8V156H735.6z"></path>
                            <path id="XMLID_21_" d="M767.1,136.9V104h-11V93.3h11V74.1h11.8v19.2h15.4V104h-15.4v32c0,7,2.9,10.2,8.4,10.2c2.2,0,3.7-0.3,5.4-1
		l4.1,10.1c-2.7,1.2-5.5,1.7-9.5,1.7C774.2,157,767.1,149.7,767.1,136.9z"></path>
                            <path id="XMLID_23_" d="M816,93.3l-0.5,8.7h0.5c3.3-5.8,11.3-10.8,19.8-10.8c16,0,23.9,10.9,23.9,26.4V156H848v-36.6
		c0-13.1-6.5-17.4-15.5-17.4c-10.2,0-16.5,9.7-16.5,19.3V156h-11.8V64.3H816V93.3z"></path>
                        </g>
                        <g id="XMLID_42_" class="twglogo-st1">
                            <g id="XMLID_44_">
                                <path id="XMLID_45_" d="M1311.2,98v-6.5h-2.4V90h6.4v1.5h-2.3V98H1311.2z M1323.3,98v-4l0.1-1.6h-0.1L1321,98h-1.1l-2.3-5.5h-0.1
			l0.1,1.6v4h-1.6v-8h2.3l2.2,5.5h0.1l2.2-5.5h2.3v8L1323.3,98L1323.3,98z"></path>
                            </g>
                        </g>
                        <path id="XMLID_10_" class="twglogo-st2" d="M892.5,105.8c0-29,24.3-52.5,53.3-52.5c16,0,27.4,6.3,36,14.5l-10.2,10.1
	c-6.1-5.8-14.4-10.3-25.8-10.3c-21.1,0-37.6,17-37.6,38.1s16.5,38.1,37.6,38.1c13.7,0,21.5-5.5,26.5-10.5c4.1-4.1,6.8-10,7.8-18
	h-34.3V101h48.3c0.5,2.6,0.8,5.6,0.8,9c0,10.8-2.9,24.1-12.4,33.5c-9.3,9.6-21.1,14.7-36.7,14.7
	C916.8,158.2,892.5,134.8,892.5,105.8z"></path>
                        <path id="XMLID_9_" class="twglogo-st3" d="M1033.8,90.8c-18.7,0-33.9,14.2-33.9,33.8c0,19.5,15.2,33.8,33.9,33.8
	c18.7,0,33.9-14.3,33.9-33.8C1067.7,104.9,1052.5,90.8,1033.8,90.8z M1033.8,144.9c-10.2,0-19.1-8.4-19.1-20.5
	c0-12.2,8.8-20.5,19.1-20.5c10.2,0,19.1,8.3,19.1,20.5C1052.9,136.6,1044,144.9,1033.8,144.9z"></path>
                        <path id="XMLID_7_" class="twglogo-st4" d="M1107.8,90.8c-18.7,0-33.9,14.2-33.9,33.8c0,19.5,15.2,33.8,33.9,33.8
	c18.7,0,33.9-14.3,33.9-33.8C1141.7,104.9,1126.5,90.8,1107.8,90.8z M1107.8,144.9c-10.2,0-19.1-8.4-19.1-20.5
	c0-12.2,8.8-20.5,19.1-20.5c10.3,0,19.1,8.3,19.1,20.5C1126.9,136.6,1118,144.9,1107.8,144.9z"></path>
                        <path id="XMLID_5_" class="twglogo-st2" d="M1198.6,92.8v5.5h-0.5c-3.3-4-9.7-7.6-17.8-7.6c-16.9,0-32.4,14.8-32.4,33.9
	c0,18.9,15.5,33.7,32.4,33.7c8.1,0,14.5-3.6,17.8-7.7h0.5v4.9c0,12.9-6.9,19.8-18,19.8c-9.1,0-14.7-6.5-17-12l-12.9,5.4
	c3.7,9,13.6,20,30,20c17.4,0,32.1-10.2,32.1-35.2V92.7C1212.8,92.7,1198.6,92.7,1198.6,92.8z M1181.6,144.9
	c-10.2,0-18.8-8.6-18.8-20.4c0-11.9,8.6-20.6,18.8-20.6c10.1,0,18,8.7,18,20.6C1199.7,136.4,1191.7,144.9,1181.6,144.9z"></path>
                        <path class="twglogo-st5" d="M1223,156.2h14.8V56.9H1223V156.2z"></path>
                        <path id="XMLID_1_" class="twglogo-st3" d="M1278.5,144.9c-7.6,0-12.9-3.5-16.4-10.2l45.2-18.7l-1.5-3.8c-2.8-7.6-11.4-21.5-28.9-21.5
	c-17.4,0-31.9,13.7-31.9,33.8c0,18.9,14.3,33.8,33.5,33.8c15.5,0,24.4-9.5,28.2-15l-11.5-7.7
	C1291.3,141.2,1286.1,144.9,1278.5,144.9z M1277.4,103.8c5.9,0,10.9,2.9,12.5,7.2l-30.2,12.5
	C1259.3,110.4,1269.8,103.8,1277.4,103.8z"></path>
</svg>

                </a>
                <div class="top-menu__buttons">
                    <a href="https://testmysite.thinkwithgoogle.com/intl/en-us" class="change-button" data-gtm-id="smbhub-test-another-cta">test another url</a>

                    <div class="js-content-cta-wrapper cta-wrapper">
                        <a class="top-menu__send-results" data-component="ContentCTA" data-content="EmailForm" data-gtm-id="smbhub-email-cta-upright">
                            Get My free report
                        </a>
                        <div class="js-content-cta-container"></div>
                    </div>
                </div>
            </header>


            <div class="scene__acting-area scene__acting-area--scene-two js-opt-in" data-opt-in-type="email">
                <div class="panels">
                    <section class="panel" data-panel-id="Speed Score" data-id="SpeedScore">
                        <article class="panel-content">
                            <div class="score-card score-card--speed" data-component="ScoreCard" data-card-id="speedResults">
                                <button class="score-card__cta" data-component="ContentCTA" data-content="EmailForm" data-gtm-id="smbhub-email-cta-details">
                                    Get my free report
                                </button>
                            </div>
                        </article>
                    </section>

                    <section class="panel panel--two" data-panel-id="See how you compare" data-id="SeeHowYouCompare">
                        <article class="panel-content panel-content--benchmarking">
                            <div class="score-card score-card--benchmarking" data-component="ScoreCard" data-card-id="industryComparison">
                                <button class="score-card__cta" data-component="ContentCTA" data-content="EmailForm" data-gtm-id="smbhub-email-cta-details">
                                    Get my free report
                                </button>
                            </div>
                        </article>
                    </section>

                    <section class="panel panel--three" data-panel-id="Next Steps" data-id="NextSteps">
                        <article class="panel-content panel-content--next-steps">
                            <div class="score-card score-card--next-steps" data-component="ScoreCard" data-card-id="nextSteps" data-mobile-ads-link="//adwords.google.com/home/resources/mobile-advertising?subid=ww-ww-et-g-aw-a-testmysite_1!o2">
                                <button class="score-card__cta" data-component="ContentCTA" data-content="EmailForm" data-gtm-id="smbhub-email-cta-details">
                                    Get my free report
                                </button>
                            </div>
                        </article>
                    </section>

                </div>


                <nav class="panel-navigation">
                    <ul class="panel-navigation__wrappper" data-component="ScrollNavigator"></ul>
                </nav>

                <div class="next-panel" data-component="NextPanel">
                    <span class="next-panel__label"></span>
                    <a class="next-panel__button"><!--?xml version="1.0" encoding="utf-8"?-->
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 54 54" height="54" width="54" style="enable-background:new 0 0 54 54;" xml:space="preserve" aria-role="img" aria-labelledby="Label__circle-arrow">
<title id="Label__circle-arrow">Next panel icon</title>
                            <style type="text/css">
                                .ic-arrow-circle-1{fill:#448AFF;}
                                .ic-arrow-circle-2{fill:#FFFFFF;}
                            </style>
                            <title id="Label__circle-arrow">Next panel icon</title>
                            <g>
                                <g>
                                    <circle class="ic-arrow-circle-1" cx="27" cy="27" r="27"></circle>
                                </g>
                                <path class="ic-arrow-circle-2 next-panel__down-arrow" d="M36,23.5l-9,9l-9-9l2.1-2.1l6.9,6.9l6.9-6.9L36,23.5z"></path>
                            </g>
</svg></a>
                </div>

                <div class="persistant-cta" data-component="ContentCTA" data-content="EmailForm" data-gtm-id="smbhub-email-cta-details">
                    Get my free report
                </div>
            </div>

        </div>
    </main>


</div>
<footer class="footer">
    <div class="footer__content">
        <ul class="footer__links">
            <li class="footer__link footer__link-item footer__logo-container">

                <a href="https://grow.google/?utm_source=testmysite.thinkwithgoogle.com&amp;utm_medium=footer_logo&amp;utm_campaign=testmysite" target="_blank" class="footer__logo footer__logo--agwg"><!--?xml version="1.0" encoding="utf-8"?-->
                    <!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                    <svg version="1.1" id="attribution_A_GwG_Program_Reverse_rgb" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 319.5 27.1" style="enable-background:new 0 0 319.5 27.1;" xml:space="preserve">
<style type="text/css">
    .st0{fill:none;}
    .st1{fill:#FFFFFF;}
</style>
                        <title>attribution_A_GwG_Program_Reverse_rgb</title>
                        <rect id="bounding_box_clear_space" x="-20.9" y="-20.9" class="st0" width="361.4" height="62.9"></rect>
                        <g id="Program">
                            <path class="st1" d="M234.4,16.1v4.6h-2.2V8.2h4.8c1.2-0.1,2.4,0.3,3.3,1.1c0.8,0.7,1.3,1.8,1.2,2.9c0.1,1.1-0.4,2.1-1.2,2.8
		c-1,0.7-2.2,1.1-3.4,1L234.4,16.1z M234.4,14.3h2.6c0.6,0,1.3-0.1,1.8-0.5c0.4-0.4,0.6-1,0.6-1.6c0-0.6-0.2-1.2-0.6-1.6
		c-0.5-0.4-1.1-0.6-1.7-0.6h-2.7V14.3z"></path>
                            <path class="st1" d="M248.9,15.8h-2.4v4.8h-2.2V8.2h4.4c1.2-0.1,2.4,0.3,3.3,1c0.8,0.7,1.2,1.7,1.2,2.8c0,0.7-0.2,1.5-0.6,2.1
		c-0.4,0.6-1,1-1.7,1.3l2.8,5.2v0.1h-2.3L248.9,15.8z M246.5,14.1h2.2c0.6,0,1.2-0.2,1.7-0.5c0.4-0.4,0.6-0.9,0.6-1.5
		c0-0.6-0.2-1.1-0.6-1.5c-0.5-0.4-1.1-0.6-1.7-0.6h-2.3L246.5,14.1z"></path>
                            <path class="st1" d="M266.1,14.7c0,1.1-0.2,2.2-0.6,3.2c-0.4,0.9-1,1.6-1.8,2.1c-1.7,1-3.7,1-5.4,0c-0.8-0.5-1.4-1.2-1.8-2.1
		c-0.4-1-0.7-2.1-0.6-3.2v-0.7c0-1.1,0.2-2.2,0.6-3.2c0.4-0.9,1-1.6,1.8-2.1c1.7-1,3.7-1,5.4,0c0.8,0.5,1.4,1.2,1.8,2.1
		c0.4,1,0.7,2.1,0.6,3.2V14.7z M264,14.1c0.1-1.1-0.2-2.2-0.8-3.2c-0.9-1.2-2.6-1.5-3.9-0.5c-0.2,0.2-0.4,0.3-0.5,0.5
		c-0.6,0.9-0.9,2-0.8,3.1v0.7c-0.1,1.1,0.2,2.2,0.8,3.2c0.5,0.7,1.3,1.2,2.2,1.2c0.9,0,1.7-0.4,2.2-1.1c0.6-1,0.8-2.1,0.8-3.2
		L264,14.1z"></path>
                            <path class="st1" d="M278.6,19.1c-0.5,0.6-1.1,1.1-1.9,1.3c-0.9,0.3-1.8,0.5-2.7,0.5c-1,0-1.9-0.2-2.8-0.7c-0.8-0.5-1.4-1.2-1.9-2
		c-0.5-1-0.7-2-0.7-3.1v-1c-0.1-1.6,0.4-3.2,1.4-4.4c0.9-1.1,2.3-1.7,3.8-1.6c1.2-0.1,2.4,0.3,3.3,1c0.9,0.8,1.4,1.8,1.5,3h-2.1
		c-0.1-1.3-1.2-2.3-2.5-2.3c0,0-0.1,0-0.1,0c-0.9-0.1-1.7,0.3-2.2,1c-0.6,0.9-0.8,2-0.8,3.1v1c-0.1,1.1,0.2,2.2,0.9,3.1
		c0.6,0.7,1.5,1.1,2.4,1c0.9,0.1,1.7-0.2,2.4-0.8v-2.4h-2.6v-1.6h4.7L278.6,19.1z"></path>
                            <path class="st1" d="M286.6,15.8h-2.4v4.8H282V8.2h4.4c1.2-0.1,2.4,0.3,3.3,1c0.8,0.7,1.2,1.7,1.2,2.8c0,0.7-0.2,1.5-0.6,2.1
		c-0.4,0.6-1,1-1.7,1.3l2.8,5.2v0.1h-2.3L286.6,15.8z M284.2,14.1h2.2c0.6,0,1.2-0.2,1.7-0.5c0.4-0.4,0.6-0.9,0.6-1.5
		c0-0.6-0.2-1.1-0.6-1.5c-0.5-0.4-1.1-0.6-1.7-0.6h-2.3L284.2,14.1z"></path>
                            <path class="st1" d="M301,17.7h-4.8l-1,2.9h-2.2l4.7-12.4h1.9l4.7,12.4H302L301,17.7z M296.8,16h3.6l-1.8-5.2L296.8,16z"></path>
                            <path class="st1" d="M309.5,8.2l3.6,9.5l3.6-9.5h2.8v12.5h-2.1v-4.1l0.2-5.5l-3.7,9.6h-1.5l-3.6-9.6l0.2,5.5v4.1h-2.1V8.2H309.5z"></path>
                        </g>
                        <g id="A">
                            <path class="st1" d="M8.1,17.7H3.3l-1,2.9H0L4.7,8.2h1.9l4.7,12.4H9.1L8.1,17.7z M3.9,16h3.6l-1.8-5.2L3.9,16z"></path>
                        </g>
                        <g id="GoogleLogo_OneColor">
                            <g id="_75x24px_minSizeAllowed">
                                <path class="st1" d="M148.8,21c-5.8,0-10.5-4.7-10.5-10.5S143,0,148.8,0c2.7,0,5.3,1,7.2,2.9l-2,2c-1.4-1.3-3.3-2-5.2-2
			c-4.2,0-7.6,3.4-7.6,7.6s3.4,7.6,7.6,7.6c2,0.1,3.9-0.7,5.2-2.1c0.9-1,1.5-2.3,1.6-3.6h-6.9V9.6h9.6c0.1,0.6,0.2,1.2,0.1,1.8
			c0.1,2.5-0.8,4.9-2.5,6.7C154.2,20,151.6,21.1,148.8,21z"></path>
                                <path class="st1" d="M173.6,14.3c0,3.7-3.1,6.6-6.7,6.6c-3.7,0-6.6-3.1-6.6-6.7c0-3.6,3-6.6,6.7-6.6c3.7,0,6.6,2.9,6.7,6.6
			C173.6,14.1,173.6,14.2,173.6,14.3z M170.6,14.3c0.2-2.1-1.3-3.9-3.4-4.1c-2.1-0.2-3.9,1.3-4.1,3.4c0,0.3,0,0.5,0,0.8
			c-0.2,2.1,1.3,3.9,3.4,4.1c2.1,0.2,3.9-1.3,4.1-3.4C170.7,14.8,170.7,14.5,170.6,14.3z"></path>
                                <path class="st1" d="M188.5,14.3c0,3.7-3.1,6.6-6.7,6.6c-3.7,0-6.6-3.1-6.6-6.7c0-3.6,3-6.6,6.7-6.6c3.7,0,6.6,2.9,6.7,6.6
			C188.5,14.1,188.5,14.2,188.5,14.3z M185.6,14.3c0.2-2.1-1.3-3.9-3.4-4.1c-2.1-0.2-3.9,1.3-4.1,3.4c0,0.3,0,0.5,0,0.8
			c-0.2,2.1,1.3,3.9,3.4,4.1c2.1,0.2,3.9-1.3,4.1-3.4C185.6,14.8,185.6,14.5,185.6,14.3z"></path>
                                <path class="st1" d="M203,7.9v12.1c0,5-2.9,7-6.4,7c-2.6,0-5-1.6-6-4l2.6-1.1c0.5,1.4,1.9,2.4,3.4,2.4c2.2,0,3.6-1.4,3.6-4v-1
			h-0.1c-0.9,1-2.2,1.6-3.6,1.5c-3.7-0.1-6.6-3.3-6.5-7c0.1-3.5,3-6.4,6.5-6.5c1.4,0,2.7,0.6,3.6,1.6h0.1V8L203,7.9z M200.4,14.3
			c0-2.4-1.6-4.1-3.6-4.1c-2.2,0.1-3.8,1.9-3.8,4.1c0,0,0,0,0,0.1c-0.1,2.1,1.5,4,3.7,4.1c0,0,0.1,0,0.1,0c2.1-0.1,3.7-1.8,3.6-3.9
			C200.4,14.4,200.4,14.3,200.4,14.3z"></path>
                                <path class="st1" d="M208.2,0.7v19.9h-3V0.7H208.2z"></path>
                                <path class="st1" d="M219.9,16.5l2.3,1.5c-1.3,1.9-3.4,3-5.6,3c-3.6,0-6.6-2.8-6.6-6.4c0-0.1,0-0.2,0-0.3c0-4,2.8-6.7,6.3-6.7
			c2.6,0.1,5,1.8,5.8,4.3l0.3,0.8l-9,3.7c0.6,1.3,1.9,2.1,3.3,2C217.9,18.3,219.2,17.6,219.9,16.5z M212.8,14.1l6-2.5
			c-0.5-0.9-1.5-1.5-2.5-1.4C214.2,10.2,212.7,12,212.8,14.1C212.8,14,212.8,14,212.8,14.1z"></path>
                            </g>
                            <g id="Grow_with">
                                <path class="st1" d="M37.9,12.1c0.1,2.3-0.7,4.5-2.3,6.2c-1.8,1.9-4.2,2.9-6.8,2.8c-5.3,0.1-9.6-4.2-9.7-9.5c0-0.1,0-0.2,0-0.3
			c-0.1-5.3,4.2-9.6,9.4-9.7c0.1,0,0.1,0,0.2,0c1.3,0,2.6,0.2,3.9,0.7c1.1,0.4,2.2,1.1,3,2l-1.7,1.7c-0.6-0.7-1.4-1.3-2.2-1.6
			c-0.9-0.4-1.9-0.6-2.9-0.6c-1.9,0-3.8,0.8-5.1,2.2c-1.4,1.4-2.1,3.3-2.1,5.3c-0.1,2,0.7,3.9,2.1,5.3c1.4,1.4,3.2,2.1,5.1,2.1
			c1.7,0.1,3.3-0.5,4.6-1.6c1.3-1.1,2-2.6,2.1-4.3h-6.8v-2.3h9C37.9,11.1,37.9,11.6,37.9,12.1z"></path>
                                <path class="st1" d="M42,20.6h-2.4V7.9h2.3V10H42c0.3-0.7,0.8-1.3,1.5-1.7c0.7-0.4,1.4-0.7,2.2-0.7c0.6,0,1.2,0.1,1.8,0.3
			l-0.7,2.3C46.3,10,45.8,10,45.3,10c-0.9,0-1.8,0.4-2.3,1.1c-0.7,0.7-1,1.6-1,2.5V20.6z"></path>
                                <path class="st1" d="M47.3,14.3c-0.1-1.8,0.6-3.5,1.8-4.9c1.2-1.3,2.9-2,4.7-1.9c1.8-0.1,3.5,0.6,4.7,1.9c1.3,1.3,1.9,3,1.9,4.9
			c0.1,1.8-0.6,3.6-1.9,4.8c-1.2,1.3-2.9,2-4.7,1.9c-1.8,0.1-3.5-0.7-4.7-1.9C47.9,17.8,47.2,16.1,47.3,14.3z M49.7,14.3
			c-0.1,1.2,0.4,2.4,1.2,3.3c1.5,1.6,4.1,1.7,5.8,0.2c0.1-0.1,0.1-0.1,0.2-0.2c0.8-0.9,1.2-2.1,1.2-3.3c0-1.2-0.4-2.4-1.2-3.3
			c-1.5-1.6-4-1.7-5.7-0.3c-0.1,0.1-0.2,0.2-0.3,0.3C50,11.9,49.6,13.1,49.7,14.3L49.7,14.3z"></path>
                                <path class="st1" d="M80,7.9l-4.1,12.7h-2.4l-3.2-9.8l-3.1,9.8h-2.4L60.7,7.9h2.5l2.8,9.6l0,0l3.1-9.6h2.5l3.1,9.6l0,0l2.8-9.6
			L80,7.9z"></path>
                                <path class="st1" d="M104.8,7.9l-4.1,12.7h-2.4l-3.1-9.8L92,20.6h-2.5L85.4,7.9h2.5l2.8,9.6l0,0l3.1-9.6h2.5l3.1,9.6l0,0l2.8-9.6
			L104.8,7.9z"></path>
                                <path class="st1" d="M109.1,3.5c0,0.9-0.8,1.7-1.7,1.7c-0.9,0-1.7-0.8-1.7-1.7c0-0.9,0.8-1.7,1.7-1.7c0.4,0,0.9,0.2,1.2,0.5
			C108.9,2.7,109.1,3.1,109.1,3.5z M108.6,7.9v12.7h-2.4V7.9H108.6z"></path>
                                <path class="st1" d="M116.4,20.8c-1,0-1.9-0.3-2.6-1c-0.7-0.7-1.1-1.7-1.1-2.7v-7.1h-2.2V7.9h2.2V4.1h2.4v3.9h3.1v2.1h-3.1v6.3
			c-0.1,0.6,0.1,1.2,0.5,1.7c0.3,0.3,0.7,0.5,1.1,0.5c0.2,0,0.4,0,0.6-0.1c0.2,0,0.3-0.1,0.5-0.2l0.8,2.1
			C117.9,20.7,117.1,20.8,116.4,20.8z"></path>
                                <path class="st1" d="M120.4,2.1h2.4v5.9l-0.1,1.8h0.1c0.4-0.7,1-1.2,1.7-1.6c0.7-0.4,1.5-0.6,2.4-0.6c1.3-0.1,2.6,0.4,3.6,1.3
			c0.9,1.1,1.3,2.4,1.3,3.8v8h-2.4v-7.6c0-2.3-1-3.4-3-3.4c-1,0-1.9,0.4-2.4,1.2c-0.7,0.8-1,1.8-1,2.8v6.9h-2.4L120.4,2.1z"></path>
                            </g>
                        </g>
</svg></a>

            </li>
            <li class="footer__link-item"><a href="https://www.google.com/about/" target="_blank" class="footer__link">About Google</a></li>
            <li class="footer__link-item"><a href="https://www.google.com/intl/en/about/products/" target="_blank" class="footer__link">Products</a></li>
            <li class="footer__link-item"><a href="https://adwords.google.com/intl/en_us/home/?subid=ww-ww-et-g-aw-a-testmysite_1!o2" target="_blank" class="footer__link">Advertising</a></li>
            <li class="footer__link-item"><a href="https://www.google.com/services" target="_blank" class="footer__link">Business</a></li>
            <li class="footer__link-item"><a href="https://www.google.com/policies/privacy/" target="_blank" class="footer__link">Privacy &amp; Terms</a></li>

        </ul>

        <div class="footer__language-wrap">
            <label class="footer__language-label" for="footer__language">
                Change language or region
            </label>

            <div class="styled-select-wrapper">

                <select class="footer__language native-select" id="footer__language" onchange="var href=this[this.selectedIndex].value;if(href!=&#39;&#39;){window.location.href=href};">


                    <option value="https://testmysite.withgoogle.com/intl/ar-ar">عربي</option>

                    <option value="https://testmysite.withgoogle.com/intl/da-dk">Dansk</option>

                    <option value="https://testmysite.withgoogle.com/intl/de-de">Deutsch</option>

                    <option value="https://testmysite.withgoogle.com/intl/en-gb">English (Intl)</option>

                    <option selected="selected" value="https://testmysite.thinkwithgoogle.com/intl/en-us">English (US)</option>

                    <option value="https://testmysite.thinkwithgoogle.com/intl/es-419">Español</option>

                    <option value="https://testmysite.thinkwithgoogle.com/intl/es-es">Español (España)</option>

                    <option value="https://testmysite.withgoogle.com/intl/fi-fi">Suomi</option>

                    <option value="https://testmysite.withgoogle.com/intl/fr-fr">Français</option>

                    <option value="https://testmysite.withgoogle.com/intl/id-id">Bahasa Indonesia</option>

                    <option value="https://testmysite.withgoogle.com/intl/it-it">Italiano</option>

                    <option value="https://testmysite.withgoogle.com/intl/iw-il">עברית</option>

                    <option value="https://testmysite.withgoogle.com/intl/ja-jp">日本語</option>

                    <option value="https://testmysite.withgoogle.com/intl/ko-kr">한국어</option>

                    <option value="https://testmysite.withgoogle.com/intl/nl-nl">Nederlands</option>

                    <option value="https://testmysite.withgoogle.com/intl/no-no">Norsk</option>

                    <option value="https://testmysite.withgoogle.com/intl/pl-pl">Polski</option>

                    <option value="https://testmysite.thinkwithgoogle.com/intl/pt-br">Português (Brasil)</option>

                    <option value="https://testmysite.withgoogle.com/intl/ru-ru">Русский</option>

                    <option value="https://testmysite.withgoogle.com/intl/sv-se">Svenska</option>

                    <option value="https://testmysite.withgoogle.com/intl/th-th">ไทย</option>

                    <option value="https://testmysite.withgoogle.com/intl/tr-tr">Türkçe</option>

                    <option value="https://testmysite.withgoogle.com/intl/vi-vn">Tiếng Việt</option>

                    <option value="https://testmysite.withgoogle.com/intl/zh-hk">中文（香港）</option>

                    <option value="https://testmysite.withgoogle.com/intl/zh-tw">中文（台灣）</option>

                </select>

            </div>
        </div>
    </div>

    <div class="footer__score-disclaimer">
        <p>Information from this tool is intended as a guideline only. Google does not guarantee improvements in your site's performance, which may vary based on location, device, browser, or other factors.</p>
        <div class="footer__score-disclaimer-footnotes">

            <p><sup>1</sup>Source: Think with Google, 2016, <a href="https://www.thinkwithgoogle.com/data-gallery/detail/cellular-network-connections-slow-speeds/" target="_blank">Data Gallery</a></p>
            <p><sup>2</sup>Source: Think with Google, 2017, <a href="https://www.thinkwithgoogle.com/articles/mobile-page-speed-new-industry-benchmarks.html" target="_blank">Find Out How You Stack Up to New Industry Benchmarks for Mobile Page Speed</a></p>
        </div>
    </div>

</footer>
<div class="side-panel-mask"></div>
<script src="./Test Your Mobile Website Speed and Performance - Think With Google_files/api.js" async="" defer=""></script>
<script src="./Test Your Mobile Website Speed and Performance - Think With Google_files/TweenMax.min.js"></script>
<script src="./Test Your Mobile Website Speed and Performance - Think With Google_files/TimelineMax.min.js"></script>
<script src="./Test Your Mobile Website Speed and Performance - Think With Google_files/ScrollToPlugin.min.js"></script>
<script src="./Test Your Mobile Website Speed and Performance - Think With Google_files/app.js"></script>
<script>window.onload = function(){handleClientLoad()}</script>

<div style="visibility: hidden; position: absolute; width:100%; top: -10000px; left: 0px; right: 0px; transition: visibility 0s linear 0.3s, opacity 0.3s linear; opacity: 0;"><div style="width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 2000000000; background-color: #fff; opacity: 0.5;  filter: alpha(opacity=50)"></div><div style="margin: 0 auto; top: 0px; left: 0px; right: 0px; position: absolute; border: 1px solid #ccc; z-index: 2000000000; background-color: #fff; overflow: hidden;"><iframe title="recaptcha challenge" src="./Test Your Mobile Website Speed and Performance - Think With Google_files/bframe.html" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox" name="7pb166o5urcd" style="width: 100%; height: 100%;"></iframe></div></div></body></html>