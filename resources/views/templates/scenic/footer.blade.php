<nav class="ampstart-footer-nav">
    <ul class="list-reset flex flex-wrap mb3">
        @foreach($data as $link)
        <li class="px1">
            <a class="text-decoration-none caps h5" href="{{$link['url']}}">{{$link['caption']}}</a>
        </li>
        @endforeach
    </ul>
</nav>