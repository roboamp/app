<div submit-success>
    <template type="amp-mustache">
        Success! Thanks {!!{{name}}!!} , we'll get back to you shortly.
    </template>
</div>
<div submit-error>
    <template type="amp-mustache">
        Error! {{message}}
    </template>
</div>