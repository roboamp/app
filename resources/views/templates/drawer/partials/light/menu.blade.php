<div id="menu-sidebar" class="menu menu-sidebar">
    <div class="menu-scroll">
        <div class="menu-clear"></div><!-- Use this on pages that don't have an opened submenu-->
        <div class="submenu-item">
            <input type="checkbox" data-submenu-items="9" class="toggle-submenu" id="toggle-1">
            <label class="menu-item" for="toggle-1"><i class="fa fa-cog active-icon"></i><strong>Home</strong></label>
            <div class="submenu-wrapper">
                <a class="menu-item" href="index-classic.html"><i class="fa fa-angle-right"></i>Classic Home</a>
                <a class="menu-item" href="index-splash.html"><i class="fa fa-angle-right"></i>Splash Screen</a>
                <a class="menu-item" href="index-landing.html"><i class="fa fa-angle-right"></i>Landing Page</a>
                <a class="menu-item" href="index-news.html"><i class="fa fa-angle-right"></i>News Page</a>
                <a class="menu-item" href="cover-1"><i class="fa fa-angle-right"></i>
                    <em>Cover Style 1</em>
                </a>
                <a class="menu-item" href="cover-2"><i class="fa fa-angle-right"></i>Cover Style 2</a>
                <a class="menu-item" href="cover-3"><i class="fa fa-angle-right"></i>Cover Style 3</a>
                <a class="menu-item" href="cover-4"><i class="fa fa-angle-right"></i>Cover Style 4</a>
                <a class="menu-item" href="cover-5"><i class="fa fa-angle-right"></i>Cover Style 5</a>
            </div>
        </div>
        <div class="submenu-item">
            <input type="checkbox" data-submenu-items="7" class="toggle-submenu" id="toggle-4a">
            <label class="menu-item" for="toggle-4a"><i class="fa fa-power-off"></i><span>Features</span></label>
            <div class="submenu-wrapper">
                <a class="menu-item" href="features-type.html"><i class="fa fa-angle-right"></i>Type</a>
                <a class="menu-item" href="features-columns.html"><i class="fa fa-angle-right"></i>Columns</a>
                <a class="menu-item" href="features-buttons.html"><i class="fa fa-angle-right"></i>Buttons</a>
                <a class="menu-item" href="features-inputs.html"><i class="fa fa-angle-right"></i>Inputs</a>
                <a class="menu-item" href="features-quotes.html"><i class="fa fa-angle-right"></i>Quotes</a>
                <a class="menu-item" href="features-dividers.html"><i class="fa fa-angle-right"></i>Dividers</a>
                <a class="menu-item" href="features-headings.html"><i class="fa fa-angle-right"></i>Headings</a>
            </div>
        </div>
        <a class="menu-item" href="amp-features.html"><i class="fa fa-bolt"></i><span>AMP's</span></a>
        <div class="submenu-item">
            <input type="checkbox" data-submenu-items="7" class="toggle-submenu" id="toggle-4">
            <label class="menu-item" for="toggle-4"><i class="fa fa-file-o"></i><span>Pages</span></label>
            <div class="submenu-wrapper">
                <a class="menu-item" href="page-404.html"><i class="fa fa-angle-right"></i>Page Error</a>
                <a class="menu-item" href="page-soon.html"><i class="fa fa-angle-right"></i>Page Soon</a>
                <a class="menu-item" href="page-profile-1.html"><i class="fa fa-angle-right"></i>Profile 1</a>
                <a class="menu-item" href="page-profile-2.html"><i class="fa fa-angle-right"></i>Profile 2</a>
                <a class="menu-item" href="page-timeline-1.html"><i class="fa fa-angle-right"></i>Timeline 1</a>
                <a class="menu-item" href="page-timeline-2.html"><i class="fa fa-angle-right"></i>Timeline 2</a>
                <a class="menu-item" href="page-testimonials.html"><i class="fa fa-angle-right"></i>Testimonials</a>
            </div>
        </div>
        <div class="submenu-item">
            <input type="checkbox" data-submenu-items="6" class="toggle-submenu" id="toggle-5">
            <label class="menu-item" for="toggle-5"><i class="fa fa-camera"></i><span>Media</span></label>
            <div class="submenu-wrapper">
                <a class="menu-item" href="gallery-round.html"><i class="fa fa-angle-right"></i>Gallery Round</a>
                <a class="menu-item" href="gallery-square.html"><i class="fa fa-angle-right"></i>Gallery Square</a>
                <a class="menu-item" href="gallery-wide.html"><i class="fa fa-angle-right"></i>Gallery Wide</a>
                <a class="menu-item" href="portfolio-1.html"><i class="fa fa-angle-right"></i>Portfolio One</a>
                <a class="menu-item" href="portfolio-2.html"><i class="fa fa-angle-right"></i>Portfolio Two</a>
                <a class="menu-item" href="portfolio-item.html"><i class="fa fa-angle-right"></i>Portfolio Selected</a>

            </div>
        </div>
        <div class="submenu-item">
            <input type="checkbox" data-submenu-items="5" class="toggle-submenu" id="toggle-7">
            <label class="menu-item" for="toggle-7"><i class="fa fa-newspaper-o"></i><span>News</span></label>
            <div class="submenu-wrapper">
                <a class="menu-item" href="news-list-1.html"><i class="fa fa-angle-right"></i>News List</a>
                <a class="menu-item" href="news-list-2.html"><i class="fa fa-angle-right"></i>News List 2</a>
                <a class="menu-item" href="news-cards.html"><i class="fa fa-angle-right"></i>News Cards</a>
                <a class="menu-item" href="news-post-1.html"><i class="fa fa-angle-right"></i>News Post 1</a>
                <a class="menu-item" href="news-post-2.html"><i class="fa fa-angle-right"></i>News Post 2</a>
            </div>
        </div>
        <a class="menu-item" href="contact.html"><i class="fa fa-envelope-o"></i><span>Contact</span></a>
    </div>
</div>
