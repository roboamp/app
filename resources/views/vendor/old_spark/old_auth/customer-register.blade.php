<!DOCTYPE html>
<html lang="en">
    <head>
        @include('.........includes.landing.analytics')

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Mosaddek">
        <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <link rel="shortcut icon" href="{{asset('img/logo-1.png')}}">
        <title>ROBOAMP - Customer Registration</title>
        <!-- Bootstrap core CSS -->
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/dashboard/bootstrap-reset.css')}}" rel="stylesheet">
        <!--external css-->
        <link href="{{asset('css/dashboard/font-awesome.css')}}" rel="stylesheet" />
        <!-- Custom styles for this template -->
        <link href="{{asset('css/dashboard/style.css')}}" rel="stylesheet">
        <link href="{{asset('css/dashboard/style-responsive.css')}}" rel="stylesheet" />
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-body">
        <div class="container">
            <form class="form-signin" action="{{ route('customers.register.post') }}" method="POST">
                {{ csrf_field() }}
                <h2 class="form-signin-heading">customer registration</h2>
                <div class="login-wrap">
                    <p> Enter your account details below</p>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @include('debug.customers.register',['testing'=>$testing])


                    <button class="btn btn-lg btn-login btn-block" type="submit">Submit</button>
                    <div class="registration">
                        Already Registered.
                        <a class="" href="{{ route('customers.login') }}">
                            Login
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>