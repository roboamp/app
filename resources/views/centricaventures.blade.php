<!doctype html>
<!--{/*
@info
Generated on: Fri, 23 Feb 2018 21:32:23 GMT
Initiator: ROBOAMP AMP Generator
Generator version: 0.9.6
*/}-->
<html ⚡ lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome to Centrica Ventures</title>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <link rel="canonical" href="http://centricaventures.com/">
        <meta name="generator" content="https://generator.rabbit.gomobile.jp">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript data-amp-spec=""><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700">
        <style amp-custom="">html {
  font-family: sans-serif;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%
}
body {
  margin: 0
}
figure,
footer,
header,
nav,
section {
  display: block
}
a {
  background-color: transparent
}
a:active,
a:hover {
  outline: 0
}
amp-img {
  border: 0
}
figure {
  margin: 1em 40px
}
button,
input,
textarea {
  margin: 0;
  font: inherit;
  color: inherit
}
button {
  overflow: visible
}
button {
  text-transform: none
}
button {
  -webkit-appearance: button;
  cursor: pointer
}
button::-moz-focus-inner,
input::-moz-focus-inner {
  padding: 0;
  border: 0
}
input {
  line-height: normal
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  height: auto
}
input[type=search]::-webkit-search-cancel-button,
input[type=search]::-webkit-search-decoration {
  -webkit-appearance: none
}
textarea {
  overflow: none
}
@font-face {
  font-family: 'Glyphicons Halflings';
  src: url(http://centricaventures.com/fonts/glyphicons-halflings-regular.eot);
  src: url(http://centricaventures.com/fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(http://centricaventures.com/fonts/glyphicons-halflings-regular.woff2) format('woff2'),url(http://centricaventures.com/fonts/glyphicons-halflings-regular.woff) format('woff'),url(http://centricaventures.com/fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(http://centricaventures.com/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')
}
.glyphicon {
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale
}
.glyphicon-chevron-left:before {
  content: "\e079"
}
.glyphicon-chevron-right:before {
  content: "\e080"
}
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
html {
  font-size: 10px;
  -webkit-tap-highlight-color: transparent
}
body {
  font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
  font-size: 14px;
  line-height: 1.42857143;
  color: #333;
  background-color: #fff
}
button,
input,
textarea {
  font-family: inherit;
  font-size: inherit;
  line-height: inherit
}
a {
  color: #337ab7;
  text-decoration: none
}
a:focus,
a:hover {
  color: #23527c;
  text-decoration: underline
}
a:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
figure {
  margin: 0
}
.carousel-inner > .item amp-img,
.img-responsive {
  max-width: 100%;
  height: auto
}
.sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0
}
[role=button] {
  cursor: pointer
}
h2,
h3,
h4,
h5 {
  font-family: inherit;
  font-weight: 500;
  line-height: 1.1;
  color: inherit
}
h2,
h3 {
  margin-top: 155px;
  margin-bottom: 10px
}
h4,
h5 {
  margin-top: 10px;
  margin-bottom: 10px
}
h2 {
  font-size: 30px
}
h3 {
  font-size: 24px
}
h4 {
  font-size: 18px
}
h5 {
  font-size: 14px
}
p {
  margin: 0 0 10px
}
.text-center {
  text-align: center
}
ol,
ul {
  margin-top: 0;
  margin-bottom: 10px
}
ul ul {
  margin-bottom: 0
}
.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto
}
.row {
  margin-right: -15px;
  margin-left: -15px
}
.col-lg-12,
.col-lg-2,
.col-md-10,
.col-md-12,
.col-md-2,
.col-md-3,
.col-md-4,
.col-md-6,
.col-md-9,
.col-sm-12,
.col-sm-6 {
  position: relative;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: 700
}
.form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075)
}
.form-control:focus {
  border-color: #66afe9;
  outline: 0;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
}
.form-control::-moz-placeholder {
  color: #999;
  opacity: 1
}
.form-control:-ms-input-placeholder {
  color: #999
}
.form-control::-webkit-input-placeholder {
  color: #999
}
.form-control::-ms-expand {
  background-color: transparent;
  border: 0
}
.form-group {
  margin-bottom: 15px
}
.btn {
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px
}
.btn:active:focus,
.btn:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
.btn:focus,
.btn:hover {
  color: #333;
  text-decoration: none
}
.btn:active {
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
  box-shadow: inset 0 3px 5px rgba(0,0,0,.125)
}
.btn-default {
  color: #333;
  background-color: #fff;
  border-color: #ccc
}
.btn-default:focus {
  color: #333;
  background-color: #e6e6e6;
  border-color: #8c8c8c
}
.btn-default:hover {
  color: #333;
  background-color: #e6e6e6;
  border-color: #adadad
}
.btn-default:active {
  color: #333;
  background-color: #e6e6e6;
  border-color: #adadad
}
.btn-default:active:focus,
.btn-default:active:hover {
  color: #333;
  background-color: #d4d4d4;
  border-color: #8c8c8c
}
.btn-default:active {
  background-image: none
}
.collapse {
  display: none
}
.dropdown-menu {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 160px;
  padding: 5px 0;
  margin: 2px 0 0;
  font-size: 14px;
  text-align: left;
  list-style: none;
  background-color: #fff;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0,0,0,.15);
  border-radius: 4px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175)
}
.dropdown-menu > li > a {
  display: block;
  padding: 3px 20px;
  clear: both;
  font-weight: 400;
  line-height: 1.42857143;
  color: #333;
  white-space: nowrap
}
.dropdown-menu > li > a:focus,
.dropdown-menu > li > a:hover {
  color: #262626;
  text-decoration: none;
  background-color: #f5f5f5
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none
}
.nav > li {
  position: relative;
  display: block
}
.nav > li > a {
  position: relative;
  display: block;
  padding: 10px 15px
}
.nav > li > a:focus,
.nav > li > a:hover {
  text-decoration: none;
  background-color: #eee
}
.navbar {
  position: relative;
  min-height: 50px;
  margin-bottom: 20px;
  border: 1px solid transparent
}
.navbar-collapse {
  padding-right: 15px;
  padding-left: 15px;
  overflow-x: visible;
  -webkit-overflow-scrolling: touch;
  border-top: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.1);
  box-shadow: inset 0 1px 0 rgba(255,255,255,.1)
}
.navbar-fixed-top .navbar-collapse {
  max-height: 340px
}
.navbar-fixed-top {
  position: fixed;
  right: 0;
  left: 0;
  z-index: 1030
}
.navbar-fixed-top {
  top: 0;
  border-width: 0 0 1px
}
.navbar-toggle {
  position: relative;
  float: right;
  padding: 9px 10px;
  margin-top: 8px;
  margin-right: 15px;
  margin-bottom: 8px;
  background-color: transparent;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px
}
.navbar-toggle:focus {
  outline: 0
}
.navbar-toggle .icon-bar {
  display: block;
  width: 22px;
  height: 2px;
  border-radius: 1px
}
.navbar-toggle .icon-bar + .icon-bar {
  margin-top: 4px
}
.navbar-nav {
  margin: 7.5px -15px
}
.navbar-nav > li > a {
  padding-top: 10px;
  padding-bottom: 10px;
  line-height: 20px
}
.navbar-default {
  background-color: #f8f8f8;
  border-color: #e7e7e7
}
.navbar-default .navbar-nav > li > a {
  color: #777
}
.navbar-default .navbar-nav > li > a:focus,
.navbar-default .navbar-nav > li > a:hover {
  color: #333;
  background-color: transparent
}
.navbar-default .navbar-toggle {
  border-color: #ddd
}
.navbar-default .navbar-toggle:focus,
.navbar-default .navbar-toggle:hover {
  background-color: #ddd
}
.navbar-default .navbar-toggle .icon-bar {
  background-color: #888
}
.navbar-default .navbar-collapse {
  border-color: #e7e7e7
}
.carousel {
  position: relative
}
.carousel-inner {
  position: relative;
  width: 100%;
  overflow: hidden
}
.carousel-inner > .item {
  position: relative;
  display: none
}
.carousel-inner > .active {
  display: block
}
.carousel-inner > .active {
  left: 0
}
.carousel-control {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  width: 15%;
  font-size: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0,0,0,.6);
  background-color: rgba(0,0,0,0);
  opacity: .5
}
.carousel-control.left {
  background-image: -webkit-linear-gradient(left,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%);
  background-image: -o-linear-gradient(left,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%);
  background-image: -webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.5)),to(rgba(0,0,0,.0001)));
  background-image: linear-gradient(to right,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%);
  background-repeat: repeat-x
}
.carousel-control.right {
  right: 0;
  left: auto;
  background-image: -webkit-linear-gradient(left,rgba(0,0,0,.0001) 0,rgba(0,0,0,.5) 100%);
  background-image: -o-linear-gradient(left,rgba(0,0,0,.0001) 0,rgba(0,0,0,.5) 100%);
  background-image: -webkit-gradient(linear,left top,right top,from(rgba(0,0,0,.0001)),to(rgba(0,0,0,.5)));
  background-image: linear-gradient(to right,rgba(0,0,0,.0001) 0,rgba(0,0,0,.5) 100%);
  background-repeat: repeat-x
}
.carousel-control:focus,
.carousel-control:hover {
  color: #fff;
  text-decoration: none;
  outline: 0;
  opacity: .9
}
.carousel-control .glyphicon-chevron-left,
.carousel-control .glyphicon-chevron-right {
  position: absolute;
  top: 50%;
  z-index: 5;
  display: inline-block;
  margin-top: -10px
}
.carousel-control .glyphicon-chevron-left {
  left: 50%;
  margin-left: -10px
}
.carousel-control .glyphicon-chevron-right {
  right: 50%;
  margin-right: -10px
}
.carousel-indicators {
  position: absolute;
  bottom: 10px;
  left: 50%;
  z-index: 15;
  width: 60%;
  padding-left: 0;
  margin-left: -30%;
  text-align: center;
  list-style: none
}
.carousel-indicators li {
  display: inline-block;
  width: 10px;
  height: 10px;
  margin: 1px;
  text-indent: -999px;
  cursor: pointer;
  background-color: rgba(0,0,0,0);
  border: 1px solid #fff;
  border-radius: 10px
}
.carousel-indicators .active {
  width: 12px;
  height: 12px;
  margin: 0;
  background-color: #fff
}
.carousel-caption {
  position: absolute;
  right: 15%;
  bottom: 20px;
  left: 15%;
  z-index: 10;
  padding-top: 20px;
  padding-bottom: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0,0,0,.6)
}
.clearfix:after,
.clearfix:before,
.container:after,
.container:before,
.nav:after,
.nav:before,
.navbar-collapse:after,
.navbar-collapse:before,
.navbar-header:after,
.navbar-header:before,
.navbar:after,
.navbar:before,
.row:after,
.row:before {
  display: table;
  content: " "
}
.clearfix:after,
.container:after,
.nav:after,
.navbar-collapse:after,
.navbar-header:after,
.navbar:after,
.row:after {
  clear: both
}
.center-block {
  display: block;
  margin-right: auto;
  margin-left: auto
}
.pull-left {
  float: left
}
::-moz-selection {
  background: #008ed6 none repeat scroll 0 0;
  color: #fff
}
::-moz-selection {
  background: #64bb67;
  color: #fff;
  text-shadow: none
}
body {
  background: #fff;
  font-family: 'Open Sans',sans-serif;
  font-size: 16px;
  line-height: 30px;
  overflow-x: hidden;
  position: relative;
  font-weight: 300
}
section {
  clear: both;
  padding-bottom: 0;
  padding-left: 0;
  padding-right: 0;
  padding-top: 0
}
h2,
h3,
h4,
h5 {
  font-family: 'Roboto Slab',serif
}
h2 {
  font-size: 46px
}
h3 {
  font-size: 36px
}
h4 {
  font-size: 32px
}
h5 {
  font-size: 26px;
  text-align: center;
  color: #008ed6
}
p {
  background-color: hsl(0,0%,0,0);
  color: #343943;
  font-size: 16px;
  line-height: 27px;
  padding-bottom: 2px;
  padding-left: 12px;
  padding-top: 2px;
  text-align: center
}
.secondary-bg {
  background-color: #010a1c
}
.primary-bg {
  background-color: #fff
}
.third-bg {
  background-color: #000711
}
.fourth-bg {
  background-color: #000205
}
a {
  transition-duration: .3s;
  -moz-transition-duration: .3s;
  -o-transition-duration: .3s;
  -webkit-transition-duration: .3s;
  -ms-transition-duration: .3s;
  text-decoration: none
}
a:hover {
  text-decoration: none
}
.padding_bottom_none {
  padding-bottom: 0
}
amp-img {
  max-width: 168%
}
@font-face {
  font-family: FontAwesome;
  src: url(http://centricaventures.com/fonts/font-awesome/fontawesome-webfont.eot?v=4.4.0);
  src: url(http://centricaventures.com/fonts/font-awesome/fontawesome-webfont.eot?#iefix&v=4.4.0) format('embedded-opentype'),url(http://centricaventures.com/fonts/font-awesome/fontawesome-webfont.woff2?v=4.4.0) format('woff2'),url(http://centricaventures.com/fonts/font-awesome/fontawesome-webfont.woff?v=4.4.0) format('woff'),url(http://centricaventures.com/fonts/font-awesome/fontawesome-webfont.ttf?v=4.4.0) format('truetype'),url(http://centricaventures.com/fonts/font-awesome/fontawesome-webfont.svg?v=4.4.0#fontawesomeregular) format('svg');
  font-weight: 400;
  font-style: normal
}
.fa {
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  font-size: inherit;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale
}
.pull-left {
  float: left
}
.fa-cog:before {
  content: "\f013"
}
.fa-home:before {
  content: "\f015"
}
.fa-bar-chart:before {
  content: "\f080"
}
.fa-phone:before {
  content: "\f095"
}
.fa-twitter:before {
  content: "\f099"
}
.fa-facebook:before {
  content: "\f09a"
}
.fa-link:before {
  content: "\f0c1"
}
.fa-google-plus:before {
  content: "\f0d5"
}
.fa-money:before {
  content: "\f0d6"
}
.fa-linkedin:before {
  content: "\f0e1"
}
.fa-youtube-square:before {
  content: "\f166"
}
.fa-instagram:before {
  content: "\f16d"
}
.carousel .carousel-control {
  opacity: 1
}
.carousel .carousel-control.left {
  background-image: linear-gradient(to right,#000 0,rgba(0,0,0,0) 100%)
}
.carousel .carousel-control.right {
  background-image: linear-gradient(to right,rgba(0,0,0,0) 0,#000 100%)
}
.item {
  position: relative
}
.item:before {
  content: '';
  background: rgba(0,0,0,.2);
  width: 100%;
  height: 100%;
  display: block;
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 5
}
.rbt-inline-0 {
  width: 100%
}
.rbt-inline-1 {
  padding: 0
}
.rbt-inline-2 {
  padding-bottom: 20%
}
.rbt-inline-7 {
  text-align: center
}
.rbt-inline-8 {
  margin-left: 8%
}
section.rbt-accordion-section > header {
  background-color: transparent;
  padding: 0;
  margin: 0;
  border: 0
}</style>
    </head>
    <body>

        <header class="navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="header_top">
                        <div class="col-md-2">
                            <div class="logo_img">
                                <a href="http://www.centricaventures.com/">
                                    <amp-img src="http://centricaventures.com/images/logo.png" alt="logoimage" width="556.1" height="75.6" layout="fixed"></amp-img>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-10">
                            <div class="menu_bar">
                                <nav role="navigation" class="navbar navbar-default" data-rbt-accordion-index="0" data-rbt-accordion-section-index="0">
                                    <div class="navbar-header">
                                        <button id="menu_slide" aria-controls="navbar" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                    </div>

                                    <div class="collapse navbar-collapse" id="navbar">

                                        <ul class="nav navbar-nav">
                                            <li><a href="#home" class="js-target-scroll">Home</a></li>

                                            <li><a href="#aboutus" class="js-target-scroll">About Us </a></li>
                                            <li><a href="#whyus" class="js-target-scroll"> Why Us </a></li>
                                            <li><a href="#oursolutions" class="js-target-scroll">Solutions </a></li>
                                            <li><a href="#services" class="js-target-scroll">Services </a></li>

                                            <li><a href="#contact" class="js-target-scroll"> Contact Us </a></li>

                                            <ul class="dropdown-menu">
                                                <li><a href="#">List  Width</a></li>
                                                <li><a href="#">List  Sidebar</a></li>
                                                <li><a href="#">List  Sidebar</a></li>
                                                <li><a href="#">List Sidebar</a></li>
                                            </ul>

                                        </ul>
                                    </div>

                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>
        <section id="home" class="top_banner_bg secondary-bg">
            <div class="container rbt-inline-0">
                <div class="row">
                    <div class="col-md-12 rbt-inline-1">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">

                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                                <li data-target="#myCarousel" data-slide-to="4"></li>
                            </ol>

                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <amp-img src="http://centricaventures.com/images/slider1.jpg" class="center-block img-responsive" alt="" width="3225.6" height="1512" layout="responsive"></amp-img>
                                    <div class="carousel-caption text-center rbt-inline-2">

                                        <h3>Enabling Global Growth as a Service </h3>

                                    </div>
                                </div>

                                <div class="item">
                                    <amp-img src="http://centricaventures.com/images/slider2.jpg" class="center-block img-responsive" alt="" width="3225.6" height="1512" layout="responsive"></amp-img>
                                    <div class="carousel-caption text-center rbt-inline-2">
                                        <h3>Growth Catalysts helping clients to build and scale their business globally </h3>

                                    </div>
                                </div>

                                <div class="item">
                                    <amp-img src="http://centricaventures.com/images/slider3.jpg" class="center-block img-responsive" alt="" width="3225.6" height="1512" layout="responsive"></amp-img>
                                    <div class="carousel-caption text-center rbt-inline-2">
                                        <h3>Expertise in International business expansions, cross-border investments, Joint Ventures and Partnerships

                                        </h3>

                                    </div>
                                </div>

                                <div class="item">
                                    <amp-img src="http://centricaventures.com/images/slider4.jpg" class="center-block img-responsive" alt="" width="3225.6" height="1512" layout="responsive"></amp-img>
                                    <div class="carousel-caption text-center rbt-inline-2">
                                        <h3>The Art of Alliances : Building productive strategic business partnerships between US and Asian businesses </h3>

                                    </div>
                                </div>

                                <div class="item">
                                    <amp-img src="http://centricaventures.com/images/slider5.jpg" class="center-block img-responsive" alt="" width="3225.6" height="1512" layout="responsive"></amp-img>
                                    <div class="carousel-caption text-center rbt-inline-2">
                                        <h3>Our mission is to help you expand and scale your business to global markets</h3>

                                    </div>
                                </div>

                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="aboutus" class="primary-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div>

                            <div class="section_heading">
                                <h2> About us</h2>

                                <p> Our primary goal is to provide the best tools and services to help your business expand and scale globally. Our core focus is to enable cross boarder investments and business expansion between Asia and North America.
                                </p>
                                <p>Our vision is to act as an International Growth Catalyst for our clients. We help Companies at Every Stage of International Growth. We delivers support and expertise through our experienced advisors and partners distributed across the globe.
                                </p>
                                <p>Our Certified CentriPartners™ have been reviewed and tested by our technology experts to provide maximum value for our corporate clients. With multiple CentriPartners™ across a variety of technology segments, we have a powerful line up of solutions to offer our clients.
                                </p>
                                <p>
                                    Centrica Venture clients in the USA, Asia and the Middle East are looking for revenue and operation ideas that will drive corporate results. Their Innovation Teams regularly meet with our senior partners regarding their needs and system requirements.</p>
                                <p>Matching Certified CentriPartners™ to our Centrica Venture clients is our passion!</p>

                            </div>

                            <div class="col-md-6">
                                <div class="features_detail">
                                    <ul>
                                        <li class="">

                                            <i class="" aria-hidden="true"></i>
                                            <h5>Integrity</h5>
                                            We do the right thing and we represent our capabilities honestly.

                                        </li>
                                        <li><i class="" aria-hidden="true"></i>
                                            <h5>Client First </h5>
                                            Our success is measured by your success. Committed to creating a long lasting and visible value to you
                                        </li>
                                        <li>
                                            <i class="" aria-hidden="true"></i>
                                            <h5>Partnership</h5>
                                            Partnerships make huge differences. We believe, appreciate, respect and celebrate partnerships.
                                        </li>

                                        <li>
                                            <i class="" aria-hidden="true"></i>
                                            <h5>Think Big</h5>
                                            Think Big. Think differently .
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="features_img pull-left">
                                    <amp-img src="http://centricaventures.com/images/features_img.png" class="img-responsive" alt="image" width="860.2" height="942.5" layout="responsive"></amp-img>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="whyus" class="primary-bg">
            <div class="container">

                <div class="row">
                    <div class="col-md-12"></div>

                    <div class="section_heading">
                        <h2> Why Us </h2>

                        <p> Innovations are our focus! . Senior Consultants from CentricaVentures connect the most appropriate technologies and services to corporate customers around the world. Using our proprietary CentriSolution™ Process, our teams research, align and integrate the right innovation into the right business condition.
                        </p>
                        <p>We connect Fortune 500 Corporations to CentriPartners™ from the hottest innovation spots in the USA, Asia and the Middle East.</p>
                        <p>In the ever-changing business world where things are unprecedented and disruptive , we add value by exploring new ideas, learning and reviewing alternative ways and providing solutions that matter the most to our clients.</p>
                        <p>We have a proprietary process of building the most advance CentriPartner™ platforms that will maximize our innovative solution platform.</p>
                    </div>
                </div>
            </div>
        </section>

        <div id="oursolutions" class="section_heading">
            <h2>Our Solutions</h2>
            <section class="our-blog">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 blog-w3ls2">
                            <div class="blog-agile">
                                <a href="http://centricaventures.com/blogpost.html" class="blogimg-w3ls">
                                    <div class="hover01 column">
                                        <div>
                                            <figure>
                                                <!--{/* @notice */}-->
                                                <!--{/* The image has no src attribute. */}-->
                                                <!-- <amp-img src="" alt=" " class=" " width="200" height="150" layout="responsive"></amp-img> -->
                                            </figure>
                                        </div>
                                    </div>
                                </a>

                                <p class="padding10 blog-p2">Our Growth as a Service Platform powered by a five stage process model,ensures that CentriPartner™ solutions are easily implemented with our Global Corporate Clients. Each stage of our process enables quick adoption of the technology and seamless integration into the ongoing corporate infrastructure.</p>

                            </div>
                        </div>
                        <div class="rbt-inline-7">
                            <div class="col-lg-2 col-md-3 col-sm-6 blog-w3ls2 rbt-inline-8">
                                <div class="blog-agile blog-agile-fix-height">
                                    <a href="#" class="blogimg-w3ls">
                                        <div class="hover01 column">
                                            <div>
                                                <figure>
                                                    <amp-img src="http://centricaventures.com/images/blog-img1.jpg" alt="w3layouts" class="img-responsive" width="504" height="504" layout="responsive"></amp-img>
                                                </figure>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="http://centricaventures.com/blogpost.html" class="blogimg-w3lh">
                                        <!--{/* @notice */}-->
                                        <!--{/* Tag h7 isn't supported in AMP. */}-->
                                        <!-- <h7> -->CentriResearch™
                                        <!-- </h7> -->
                                    </a>
                                    <p class="blog-p2">Intelligence on category application to targeted customer groups..</p>

                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6 blog-w3ls2">
                                <div class="blog-agile blog-agile-fix-height">
                                    <a href="#" class="blogimg-w3ls">
                                        <div class="hover01 column">
                                            <div>
                                                <figure>
                                                    <amp-img src="http://centricaventures.com/images/blog-img2.jpg" alt="w3layouts" class="img-responsive" width="504" height="504" layout="responsive"></amp-img>
                                                </figure>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="http://centricaventures.com/blogpost.html" class="blogimg-w3lh">
                                        <!--{/* @notice */}-->
                                        <!--{/* Tag h7 isn't supported in AMP. */}-->
                                        <!-- <h7> -->CentriStrat™
                                        <!-- </h7> -->
                                    </a>
                                    <p class="blog-p2">Finding your Focal Point on the client’s needs.</p>

                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6 blog-w3ls2">
                                <div class="blog-agile blog-agile-fix-height">
                                    <a href="#" class="blogimg-w3ls">
                                        <div class="hover01 column">
                                            <div>
                                                <figure>
                                                    <amp-img src="http://centricaventures.com/images/blog-img3.jpg" alt="w3layouts" class="img-responsive" width="504" height="504" layout="responsive"></amp-img>
                                                </figure>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="http://centricaventures.com/blogpost.html" class="blogimg-w3lh">
                                        <!--{/* @notice */}-->
                                        <!--{/* Tag h7 isn't supported in AMP. */}-->
                                        <!-- <h7> -->CentriCRM™
                                        <!-- </h7> -->
                                    </a>
                                    <p class="blog-p2">Local marketing support for innovation launch.<br></p>

                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6 blog-w3ls2">
                                <div class="blog-agile blog-agile-fix-height">
                                    <a href="#" class="blogimg-w3ls">
                                        <div class="hover01 column">
                                            <div>
                                                <figure>
                                                    <amp-img src="http://centricaventures.com/images/blog-img5.jpg" alt="w3layouts" class="img-responsive" width="504" height="504" layout="responsive"></amp-img>
                                                </figure>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="blogimg-w3lh">
                                        <!--{/* @notice */}-->
                                        <!--{/* Tag h7 isn't supported in AMP. */}-->
                                        <!-- <h7> -->CentriDev™
                                        <!-- </h7> -->
                                    </a>
                                    <p class="blog-p2">Account Management and Selling.</p>

                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-6 blog-w3ls2">
                                <div class="blog-agile blog-agile-fix-height">
                                    <a href="#" class="blogimg-w3ls">
                                        <div class="hover01 column">
                                            <div>
                                                <figure>
                                                    <amp-img src="http://centricaventures.com/images/blog-img4.jpg" alt="w3layouts" class="img-responsive" width="504" height="504" layout="responsive"></amp-img>
                                                </figure>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="blogimg-w3lh">
                                        <!--{/* @notice */}-->
                                        <!--{/* Tag h7 isn't supported in AMP. */}-->
                                        <!-- <h7> -->CentriOps™
                                        <!-- </h7> -->
                                    </a>
                                    <p class="blog-p2">Contracts to Next Step Operational Meetings.</p>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 blog-w3ls2">
                            <div class="blog-agile">

                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>

        <section id="services" class="padding_bottom_none our_service_bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section_heading section_heading_2">
                            <h2 class="restrict-bottom-margin"> Our Services </h2>
                        </div>

                        <div class="col-md-9 pull-left">
                            <div class="services_detail">
                                <ul>
                                    <li>
                                        <a href="#"><span><i class="fa fa-bar-chart" aria-hidden="true"></i></span> 
								<h5> Global Growth as a Service </h5></a>
                                        <p> New models has been disrupting the global business world. We work with you to sense the new opportunity and help in evaluating, acquiring and development of innovative business models that allow you to expand and scale to new markets.</p>
                                    </li>

                                    <li>
                                        <a href="#"><span><i class="fa fa-link " aria-hidden="true"></i></span> 
								<h5>Strategic Alliances & Partnerships</h5></a>
                                        <p> Our trusted business partners across the global work with you to develop and grow products, markets, projects etc through strategic alliances & partnerships.</p>
                                    </li>

                                    <li>
                                        <a href="#"><span><i class="fa fa-money" aria-hidden="true"></i></span> 
								<h5>Cross Boarder Investments & Capital Help</h5></a>
                                        <p> We help our clients to secure Angel, Private Equity/Venture Capital to enable International expansion. We alsoprovide financial advisory services.</p>
                                    </li>

                                    <li>
                                        <a href="#"><span><i class="fa fa-cog" aria-hidden="true"></i></span> 
								<h5>Management & Technology Consulting</h5></a>
                                        <p> We have expertise in managing international, strategic consulting experience, M&A expertise , technology consulting and industry insight across multiple sectors that enables us to support your business across the entire lifecycle.</p>
                                    </li>

                                </ul>

                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="services_img" id="img_position">
                                <amp-img src="http://centricaventures.com/images/services_img1.png" alt="image-1" width="413.3" height="937.5" layout="responsive"></amp-img>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section id="contact" class="contact_bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section_heading section_heading_2">
                            <h2> Contact Us </h2>

                            <h4>Want to know how we can be of help with your global growth plans? .</h4>
                        </div>
                        <div class="seo">

                            <div class="container">
                                <div class="seo-grids">
                                    <div class="col-md-4 seo-grid">
                                        <div class="seo-one"></div>
                                        <h5>Drop Us a Line</h5>

                                        <p class="white-text">Contact us and let us<br>know more about you</p>
                                    </div>
                                    <div class="col-md-4 seo-grid">
                                        <div class="seo-two"></div>
                                        <h5>Direct Access to Experts
                                            <h5>
                                                <p class="white-text">One of our Growth Catalysts<br> will be reaching out to you</p>
                                            </h5>
                                        </h5>
                                    </div>
                                    <div class="col-md-4 seo-grid">
                                        <div class="seo-three"></div>
                                        <h5>Build and Execute a Plan that work for you</h5>
                                        <p class="white-text">Our experts are committed to collaborate with one goal in mind - to get your business where you wanted it to be.</p>

                                    </div>
                                    <h5>Drop a line to us and we will be in touch.
                                        <h5>
                                            <div class="clearfix"></div>
                                        </h5>
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="contact_form">
                                <!--{/* @notice */}-->
                                <!--{/* form[method=POST] is not supported in AMP Generator. */}-->
                                <!-- <form method="post" action="contactform2.php">
							<div class="form-group">
								<label>Full Name : <span> *</span></label>
								<input type="text" name="name" class="form-control" id="exampleInputEmail1">
							</div>
							  
							<div class="form-group">
								<label>Email Address : <span> *</span></label>
								<input type="email" class="form-control" name="email" id="exampleInputPassword1">
							
							</div>
							  
							<div class="form-group">
								<label>Message <span> *</span></label>
								<textarea name="message" class="form-textarea" rows="3"></textarea>
							</div>
						  
							<div class="section_sub_btn">
								<button class="btn btn-default" type="submit">  Send Message</button>	
							</div>
						</form> -->
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="contact_text">
                                <ul>
                                    <li>
                                        <span><i class="fa fa-home" aria-hidden="true"></i></span>
                                        <h5>311 N Market St #200, Dallas, TX 75202</h5>
                                    </li>

                                    <li>
                                        <span><i class="fa fa-phone" aria-hidden="true"></i></span>
                                        <h5> +1 (501)765-4761</h5>
                                    </li>

                                </ul>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section id="contact" class="contact_bg">

            <footer class="third-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="footer_top">
                                <h4> </h4>

                                <ul>
                                    <li> <a href="http://facebook.com/"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                                    <li> <a href="http://twitter.com/"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                                    <li> <a href="http://linkedin.com/"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
                                    <li> <a href="http://google.com/"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                                    <li> <a href="http://youtu.be/"> <i class="fa fa-youtube-square" aria-hidden="true"></i> </a> </li>
                                    <li> <a href="https://www.instagram.com/"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="footer_bottom fourth-bg">

                    <p> 2017 &copy; Copyright Centrica venture. All rights Reserved. Powered By <a href="http://centricaventures.com/">Centrica</a> </p>
                    <a href="#" class="backtop"> ^ </a>
                </div>

            </footer>

        </section>
    </body>
</html>