<link rel="shortcut icon" href="img/favicon.png">
<!-- Bootstrap core CSS -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('css/dashboard/bootstrap-reset.css')}}" rel="stylesheet">
<!--external css-->
<link href="{{asset('fonts/fontawesome/css/all.css')}}" rel="stylesheet" />

<!--right slidebar-->
<link href="{{asset('css/dashboard/slidebars.css')}}" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="{{asset('css/dashboard/style.css')}}" rel="stylesheet">
<link href="{{asset('css/dashboard/style-responsive.css')}}" rel="stylesheet" />