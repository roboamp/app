    <tr>
        <td>{{$property->url}}</td>
        <td>
            <select name="{{$property->id}}" id="selected_platform">
                @foreach($platforms as $platform)

                    <option value="{!!$property->platform_link($platform)!!}"><?=ucfirst($platform->name)?></option>
                @endforeach
            </select>
        </td>
        <td>
            <pre>
                <div class="{{$property->id}}">{{$property->platform_link($default_platform)}}</div>
                <input type="hidden" value="{{$property->platform_link($default_platform)}}" id="{{$property->id}}">
            </pre>
        </td>

        <td><button onclick="copyLink('<?php echo $property->id ;?>')" class="btn" id="amp_button" title="Copy and Paste this code into the head section of your html">Copy to clipboard</button></td>
    </tr>
