@foreach($customer->properties as $property)
    <?php
        if($customer->properties->first()->id==$property->id){
            $active_class='alert-success';
        }else{
            $active_class='';
        }
    ?>
    <form id="form_{{$property->id}}" onsubmit="event.preventDefault();">
    {{ csrf_field() }}

    <tr id="tr_{{$property->id}}">
        <td class="{{$active_class}}">{{$property->url}}</td>
        <td class="{{$active_class}}">
            <select name="{{$property->id}}" id="{{$property->id}}">

                @foreach($platforms[$property->id] as $platform)
                    <option {{$platform->selected}} value="{!!$property->platform_link($platform,$property->id)!!}||{{$platform->id}}"><?=ucfirst($platform->name)?></option>
                @endforeach
            </select>
        </td>
        <td class="{{$active_class}}">
            <pre>
                <div class="div_{{$property->id}}">{{$platforms[$property->id]->default_code}}</div>
                <input type="hidden" value="{{$platforms[$property->id]->default_code}}" name="platform_code_{{$property->id}}" id="platform_code_{{$property->id}}">
                <input type="hidden" name="platform_id_{{$property->id}}" id="platform_id_{{$property->id}}">
                <input type="hidden" name="change_from_div_{{$property->id}}" id="change_from_div_{{$property->id}}">

            </pre>
        </td>

        <td class="{{$active_class}}"><button onclick="copyLink('platform_code_{{$property->id}}')" class="btn" id="amp_button" title="Copy and Paste this code into the head section of your html">Copy to clipboard</button></td>
    </tr>
    </form>
@endforeach