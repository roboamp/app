<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{asset('js/dashboard/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('js/dashboard/slidebars.min.js')}}"></script>
<script src="{{asset('js/dashboard/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{asset('js/dashboard/respond.min.js')}}" ></script>
<script src="{{asset('/js/sweetalert2.all.min.js')}}"></script>    

<!--common script for all pages-->
<script src="{{asset('js/dashboard/common-scripts.js')}}"></script>