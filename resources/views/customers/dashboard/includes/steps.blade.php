<section class="panel">
    <header class="panel-heading">
    </header>
    <table class="table table-striped table-advance table-hover steps">
        <thead>
        <tr>
            <th style="width: 25%;text-align: center">
                <i class="fa fa-bullhorn"></i>
                Please Follow these Steps
            </th>
        </tr>
        </thead>
            {!!$steps!!}
        <tfoot>

        </tfoot>
    </table>
</section>