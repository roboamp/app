<section class="panel">
    <header class="panel-heading">
        @if($login_type=='customers')Current Websites @endif
            <a href="{{$add_website_url}}" name="add_website"><button class="btn add_customer" id="amp_button">Add Website</button></a>
    </header>
    <table class="table table-striped table-advance table-hover">
        <thead>
        <tr>
            <th style="width: 25%"><i class="fa fa-bullhorn"></i> Domain</th>
            <th style="width: 10%">Platform</th>
            <th style="width: 50%"><i class="fa fa-bookmark"></i> ROBOAMP Code</th>
            <th style="width: 15%"><i class="fa fa-edit"></i> Copy</th>
        </tr>
        </thead>
        <tbody>

        @include('customers.dashboard.includes.code_selection.'.$login_type,array('property'=>$property,'platforms'=>$platforms))


        </tbody>
        <tfoot>
        <tr>
            <td colspan='100%' class="text-center">ROBOAMP</td>
        </tr>
        </tfoot>
    </table>
</section>