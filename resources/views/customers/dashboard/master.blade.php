<!DOCTYPE html>
<html>
	<head>
    	<link rel="icon" href="{{asset('img/logo-1.png')}}">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	    <meta name="csrf-token" content="{{ csrf_token() }}" />
	    @include ('customers/dashboard/includes.meta')
	    @include ('customers/dashboard/includes.css')
	    @yield ('page_specific_css')
		@yield('page_header_specific_scripts')
	    @yield ('title')
    </head>
	<body>
		@include ('customers/dashboard/includes.nav')
		@include ('customers/dashboard/includes.sidebar')
	  	<section id="container" class="">
	     	<section id="main-content">
	        	<section class="wrapper site-min-height">
					@yield ('content')
				</section>
			</section>
		</section>
	</body>
	@include ('customers/dashboard/includes.footer')
	@include ('customers/dashboard/includes.scripts')
	@yield ('page_specific_scripts')

</html>