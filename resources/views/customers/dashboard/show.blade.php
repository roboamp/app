@extends ('customers/dashboard.master')
@section ('title')
<title>ROBOAMP Dashboard</title>
@endsection
@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://unpkg.com/tippy.js@2.5.0/dist/tippy.all.min.js"></script>


<div class="row">
    <div class="col-md-10 col-md-offset-1 platform">
        @include('customers.dashboard.includes.select_platform',array('platforms'=>$platforms,'login_type'=>$login_type))

    </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        @include('customers.dashboard.includes.steps',array('steps'=>$steps))

    </div>
</div>


@include('customers.js.select_platform_js')
@endsection