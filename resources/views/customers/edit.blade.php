

@extends('spark::layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-header">
                	Edit Customer
               	</div>
            </div>
            <div class="panel-body">
                <form id="status_form" class="form-group text-center">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="name">Name </label>
					<input type="name" name="name" value="{{$customer->name}}" class="form-control text-center" required/>
				</div>
				<div class="form-group">
					<input class="form-control btn-primary" type="submit" name="submit" value="Update">
				</div>
			</form>
            </div>
        </div>
    </div>
</div>
<script>
	$( document ).ready(function() {
    	$(function () {

	        $('form').on('submit', function (e) {
    	    	e.preventDefault();

        		$.ajax({
          			type: 'POST',
          			url: '/customers/{{$customer->id}}',
          			data: $('form').serialize(),
          			success:(response) => {
                        var arr = $.parseJSON(response);
                        if (arr.error == 0){
	            			swal({
	                            type: 'success',
	                            title: 'Customer info has been updated!',
	                            timer: 3000,
	                            onClose(){
	                        		window.location.href = arr.redirect;
	                            }
	                        });
                        }
            		}
            	});
        	});
      	});
     });
    </script>
@endsection