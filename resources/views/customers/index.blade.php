@extends('spark::layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-header">
                    {{ $customer->name }}
                    <button id="add_site" class="btn btn-default add_customer">Add Site</button>
                    <a href="/customers/edit/{{$customer->id}}"><button id="edit_customer" value="{{ $customer->id }}" class="btn btn-primary">Edit Customer</button></a>
                </div>
            </div>

            <div class="panel-body">
                <table class="table table striped">
                    <thead>
                        <th style="width: 30%">Website</th>
                        <th style="width: 15%">Status</th>
                        <th style="width: 55%">Actions</th>
                    </thead>
                    <tbody id="website_table">
                        @foreach ($sites as $site)
                            @php $class=($site->status_id==3)?"website_suspended":"" @endphp
                            @php $class=($customer->main_website()==$site->url)?"main_website_row":"" @endphp
                                <tr id="row_{{$site->id}}" class="{{$class}}">

                                <td><a href="https://{{ $site->url }}"> @php $url = new App\MyClasses\URL; echo $url->shorten_url($site->url, 30) @endphp </a></td>
                                <td id="status_{{$site->id}}">{{ $site->status->status }}</a></td>
                                <td id="action_{{$site->id}}">
                                    <a href="/properties/{{$site->id}}"><button id="change_plan" value="{{ $site->id }}" class="btn btn-success">Change Plan</button></a>
                                @if ($site->status_id == 3)
                                    <button id="activate" value="{{ $site->id }}" class="btn btn-success">Activate</button>
                                </td>
                                @else
                                    <button id="suspend" value="{{ $site->id }}" class="btn btn-danger">Suspend</button>
                                    @if ($customer->main_website() != $site->url)
                                        <button id="default" value="{{ $site->id }}" class="btn btn-primary">Make Default</button>
                                    @endif
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $sites->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="">
    $( document ).ready(function() {
        $obj_url="{{$testing['url']}}";
        $obj_name="{{$testing['name']}}";
        $(".panel-header").on("click", "#add_site", function () {
            swal({
                title: 'Add a site',
                html:
                    '<form id="add_site_form">' +
                    '{{ csrf_field() }}' +
                    '<input id="url" class="swal2-input" name="url" placeholder="URL" value="'+$obj_url+'">' +
                    '</form>',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return new Promise((resolve) =>{
                        setTimeout(() => {
                        if ($('#url').val() === '') {
                            swal.showValidationError(
                            'Url must be filled out'
                          )
                        }
                        resolve()
                        }, 2000)
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        url: '/properties/{{$customer->id}}',
                        data: $('form').serialize(),
                        success:(response) => {
                            var arr = $.parseJSON(response);
                            console.log(arr);
                            if (arr.error === 0){
                                appendData(arr);
                                swal({
                                    type: 'success',
                                    title: 'Website has been added!',
                                    timer: 2000,
                                });

                                $obj_url=(arr.testing.url);
                                $obj_name=(arr.testing.name);
                            }
                            else {
                                swal({
                                    type: 'error',
                                    title: 'Sorry',
                                    text: 'Something went wrong!',
                                    timer: 5000,
                                })
                            }
                        }
                    });

                    function appendData(data){
                        $('#website_table').append('\
                            <tr id="row_'+ data.property_id +'" class="">\
                                <td><a href="https://' + data.url + '">' + data.url + '</a></td>\
                                <td id="status_'+ data.property_id +'">Pending</td>\
                                <td id="action_'+ data.property_id +'"><a href="/properties/'+ data.property_id+'"><button id="change_plan" value="'+ data.property_id +'" class="btn btn-success">Change Plan</button></a>\
                                <button id="suspend" value="' + data.property_id + '" class="btn btn-danger">Suspend</button>\
                                <button id="default" value="' + data.property_id + '" class="btn btn-primary">Make Default</button></td>\
                            </tr>'
                        )   
                    }
                }
            })
        })

        $("#website_table").on("click", ".btn", function () {
            var action = ($(this)[0].id);
            var website_id = ($(this)[0].value);
            var update_id="";
            console.log(website_id);
            // console.log($(this)[0]);
            // console.log(customer_id);

            if(action == 'suspend'){
                // console.log('I am in the suspend method');
                swal({
                    title: 'Are you sure you want to suspend this site?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, suspend it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'PATCH',
                            url: '/properties/'+ website_id + '/' + 3,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                console.log(arr);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'This website has been suspended.',
                                        timer: 3000,
                                    });
                                    $obj1= $('#action_'+ website_id);
                                    $obj1.html('<td id="action_' + website_id + '">\
                                                <button id="change_plan" value="'+ website_id +'" class="btn btn-success">Change Plan</button>\
                                                <button id="activate" value="'+ website_id +'" class="btn btn-success">Activate</button></td>');

                                    $obj2= $('#status_' + website_id);
                                    $obj2.html('Suspended');

                                    $('#row_' + website_id).addClass('website_suspended');

                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })
            }

            if(action == 'activate'){
                // console.log('I am in the suspend method');
                swal({
                    title: 'Are you sure you want to activate this site?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, activate it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                        reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'PATCH',
                            url: '/properties/'+ website_id + '/' + 2,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                console.log(arr);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'This website has been activated.',
                                        text: 'Head to the Checkout to fully activate website.',
                                        timer: 3000,
                                    });
                                    $obj1= $('#action_'+ website_id);
                                    $obj1.html('<td id="action_' + website_id + '">\
                                                <button id="change_plan" value="'+ website_id +'" class="btn btn-success">Change Plan</button>\
                                                <button id="suspend" value="'+ website_id +'" class="btn btn-danger">Suspend</button></td>');
                                    $obj2= $('#status_' + website_id);
                                    $obj2.html('Pending');

                                    $('#row_' + website_id).removeClass('website_suspended');

                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })
            }

            if(action == 'default'){
                // console.log('I am in the suspend method');
                swal({
                    title: 'Are you sure you want to make this the default url?',
                    html: '<form>' +
                          '{{ csrf_field()}}'+
                          '</form>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, make default!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false,
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: 'PUT',
                            url: '/properties/'+ website_id,
                            data: $('form').serialize(),
                            success:(response) => {
                                var arr = $.parseJSON(response);
                                console.log(arr);
                                if (arr.error === 0){
                                    swal({
                                        type: 'success',
                                        title: 'This website has been set as default.',
                                        timer: 3000,
                                    });

                                    console.log(arr.old_main_website.id);

                                    $obj1= $('#action_'+ website_id);
                                    $obj1.html('<td id="action_' + website_id + '">\
                                                <a href="/properties/'+ website_id +'"><button id="change_plan" value="'+ website_id +'" class="btn btn-success">Change Plan</button></a>\
                                                <button id="activate" value="'+ website_id +'" class="btn btn-danger">Suspend</button></td>');


                                    $obj2 = $('#row_'+ arr.old_main_website.id);
                                    $obj2.html('<td><a href="https://' + arr.old_main_website.url + '">' + arr.old_main_website.url + '</a></td>\
                                                <td id="status_'+ arr.old_main_website.id +'">Pending</td>\
                                                <td id="action_'+ arr.old_main_website.id +'"><a href="/properties/'+ arr.old_main_website.id +'"><button id="change_plan" value="'+ arr.old_main_website.id +'" class="btn btn-success">Change Plan</button></a>\
                                                <button id="activate" value="'+ arr.old_main_website.id +'" class="btn btn-danger">Suspend</button>\
                                                <button id="default" value="' + arr.old_main_website.id + '" class="btn btn-primary">Make Default</button></td>');


                                    $('#website_table .main_website_row').removeClass('main_website_row');
                                    $('#row_' + website_id).addClass('main_website_row');


                                }
                                else {
                                    swal({
                                        type: 'error',
                                        title: 'Sorry',
                                        text: 'Something went wrong!',
                                        timer: 5000,
                                    })
                                }
                            }
                        });
                    }
                })
            }
        })
    });
</script>
@endsection 