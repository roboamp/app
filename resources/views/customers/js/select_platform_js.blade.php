<script type="text/javascript">
    $(document).ready(function(){
        $index="";

        @foreach($customer->properties as $property)

            $(document).on('change', '#{{$property->id}}', function() {
                    changed_platform($(this))
            });
            $(document).on('click', '#tr_{{$property->id}}', function() {
                if($('#tr_{{$property->id}}  > td').first().hasClass('alert-success')==false){

                    changed_platform_div('{{$property->id}}');
                }
                remove_all_trs_classes();

                $('#tr_{{$property->id}}  > td').toggleClass('alert-success');

            });

        @endforeach
        function changed_platform_div($obj){
            changed_platform($('#'+$obj),"yes");
        }
        function remove_all_trs_classes(){
            $('.platform td').removeClass('alert-success');
        }
        $(document).on('click','.btn.confirm',function(){
            $property_id=$(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{route("customers.property.update.confirm")}}",
                data: {"property_id":$property_id},
                success:function ($data){
                    $steps_table=$('.table.steps');
                    $('.table.steps  tbody').empty();
                    $steps_table.append($data);
                }

            });
            swal({type: 'success',
                title: 'Thank you! We will take over from here :)',
                timer: 2500,
            });


        });

        function changed_platform($obj,$div){
            $option="Select your platform||1";
            //if($obj.val()!=$option){
                var platform= $obj.val().split("||");
                var platform_link=platform[0];
                var platform_id=platform[1];
                var property_id = $obj.attr('id');


                $div_content="";
                $div_content=$('.div_' + property_id);
                $div_content.text(platform_link);
                $div_content.val(platform_link);

                $('#change_from_div_'+property_id).val($div);
                $('#platform_id_'+property_id).val(platform_id);
                $('#platform_code_'+property_id).val(platform_link);

                if($div!="yes")$('#'+property_id+' option[value="'+$option+'"]').remove();



                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{route("customers.property.update.platform")}}",
                    data: $('#form_'+property_id).serialize(),
                    success:function ($data){
                        $steps_table=$('.table.steps');
                        $('.table.steps  tbody').empty();
                        $steps_table.append($data);

                    }
                });



            //}
        }

        tippy('.btn', {
            size: 'large'
        });

    });
    function hide_all_trs(){
       return true;
        $('table.steps tbody').hide();
    }
    function info_to_success($row){
        $row.addClass('alert-success');
        $row.removeClass('alert-info');
    }
    function copyLink(property_id) {
        var copyText = document.getElementById(property_id);
        copyText.select();
        var tempInput = document.createElement("input");
        tempInput.style = "position: absolute; left: -1000px; top: -1000px";
        tempInput.value = copyText.value;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
        swal({
            type: 'success',
            title: 'Link has been copied to your clipboard.',
            timer: 2500,
        });
    }


</script>