      <div id="film-list">
          <h2 id="showtimes-header">Showtimes BATMAN</h2>
          <div class="film-listing-container-desktop">
            <div class="film-image" style="float:left; padding:0 1em;">
              <a href="#" data-toggle="modal" data-target="#basicModal">
              <amp-img id=574 class="modal-thumb" src=https://theroxy.org/appimages/us/how%20to%20train%20your%20dragon%20the%20hidden%20world.jpg data-film-name="" data-film-rating="PG" data-film-runtime="106" data-film-synopsis="Its been five years since everything was awesome and the citizens are facing a huge new threat: LEGO DUPLO'® invaders from outer space, wrecking everything faster than they can rebuild." data-playing-status="Now Showing" data-performance-count="12" data-single-performance="" width="200" height="300"></amp-img>
            </a></div>
            <div class="movie-name"> <span class="tk-sorts-mill-goudy" style="color:#e5ae47; font-size:180%; text-transform: uppercase;"><strong>HOW TO TRAIN YOUR DRAGON: THE HIDDEN WORLD</strong></span> </div>
            <div class="movie-rating-runtime"> <span style="color:#fff;font-size:120%">PG, 104 Minutes</span> </div>
            <br/>
        <div style="float:left; display:inline-block;"><div class="film-performance-time" style="float:left;">
        <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50891">
            <span id="How To Train Your Dragon: The Hidden World->2019-03-14->3:30pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">3:30pm</span>
        </a>
        </div><div class="film-performance-time" style="float:left;">
        <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50889">
            <span id="How To Train Your Dragon: The Hidden World->2019-03-14->6:30pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">6:30pm</span>
        </a>
        </div><div class="film-performance-time" style="float:left;">
        <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50890">
            <span id="How To Train Your Dragon: The Hidden World->2019-03-14->9:15pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">9:15pm</span>
        </a>
        </div></div>

            <div class="film-button-container" style="display:block; float:right; margin-right:1em;">
              <div style="padding:1em 0;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/start" class="button tk-sorts-mill-goudy ticketing-btn pulse-button btn-buy-tickets" style="width:200px"> <span class="button-text btn-buy-tickets">Buy Tickets</span> </a> </div>
              <div style="padding-top:1em;"> <a href="#" class="button tk-sorts-mill-goudy video-btn btn-view-trailer" style="width:200px" data-toggle="modal" data-src="cksYkEzUa7k" data-banner="https://theroxy.org/titleimages/the%20lego%20movie%202%20the%20second%20part.jpg" data-target="#trailerModal" data-playing-status="Now Showing" data-performance-count="12" data-single-performance=""> <span class="button-text btn-view-trailer">View Trailer</span> </a> </div>
            </div>
          </div>
          <div class="film-listing-container-desktop">
            <div class="film-image" style="float:left; padding:0 1em;"><a href="#" data-toggle="modal" data-target="#basicModal">
              <amp-img id=575 class="modal-thumb" src=https://theroxy.org/appimages/us/custom/MINARC/2019%20oscar%20shorts:%20animated.jpg data-film-name="2019 Oscar Shorts: Animated" data-film-rating="NR" data-film-runtime="90" data-film-synopsis="Don't miss your chance to experience this year's best short films the way they were meant to be seen – on the big screen – “The short films … offer a vision of what the Academy Awards should and could be but very rarely are: eclectic, cosmopolitan, scrappy and surprising.” – A.O. Scott, New York Times" data-playing-status="Now Showing" data-performance-count="3" data-single-performance="" width="200" height="300"></amp-img>
            </a></div>
            <div class="movie-name"> <span class="tk-sorts-mill-goudy" style="color:#e5ae47; font-size:180%; text-transform: uppercase;"><strong>2019 Oscar Shorts: Animated</strong></span> </div>
            <div class="movie-rating-runtime"> <span style="color:#fff;font-size:120%">NR, 90 Minutes</span> </div>
            <br/>
            <div class="movie-times" style="float:left; display:grid;">
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Mon (02/18):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50523"> <span id="2019 Oscar Shorts: Animated->2019-02-18->5:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">5:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Tue (02/19):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50534"> <span id="2019 Oscar Shorts: Animated->2019-02-19->5:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">5:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Wed (02/20):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50551"> <span id="2019 Oscar Shorts: Animated->2019-02-20->2:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">2:00 pm</span> </a> </div>
                </div>
              </div>
            </div>
            <div class="film-button-container" style="display:block; float:right; margin-right:1em;">
              <div style="padding:1em 0;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/start" class="button tk-sorts-mill-goudy ticketing-btn pulse-button btn-buy-tickets" style="width:200px"> <span class="button-text btn-buy-tickets">Buy Tickets</span> </a> </div>
            </div>
          </div>
          <div class="film-listing-container-desktop">
            <div class="film-image" style="float:left; padding:0 1em;"><a href="#" data-toggle="modal" data-target="#basicModal">
              <amp-img id=576 class="modal-thumb" src=https://theroxy.org/appimages/us/custom/MINARC/2019%20oscar%20shorts:%20live%20action.jpg data-film-name="2019 Oscar Shorts: Live Action" data-film-rating="NR" data-film-runtime="109" data-film-synopsis="Don't miss your chance to experience this year's best short films the way they were meant to be seen – on the big screen – “The short films … offer a vision of what the Academy Awards should and could be but very rarely are: eclectic, cosmopolitan, scrappy and surprising.” – A.O. Scott, New York Times" data-playing-status="Now Showing" data-performance-count="4" data-single-performance="" width="200" height="300"></amp-img>
            </a></div>
            <div class="movie-name"> <span class="tk-sorts-mill-goudy" style="color:#e5ae47; font-size:180%; text-transform: uppercase;"><strong>2019 Oscar Shorts: Live Action</strong></span> </div>
            <div class="movie-rating-runtime"> <span style="color:#fff;font-size:120%">NR, 109 Minutes</span> </div>
            <br/>
            <div class="movie-times" style="float:left; display:grid;">
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Mon (02/18):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50525"> <span id="2019 Oscar Shorts: Live Action->2019-02-18->2:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">2:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Tue (02/19):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50532"> <span id="2019 Oscar Shorts: Live Action->2019-02-19->2:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">2:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Wed (02/20):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50550"> <span id="2019 Oscar Shorts: Live Action->2019-02-20->8:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">8:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Thu (02/21):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50489"> <span id="2019 Oscar Shorts: Live Action->2019-02-21->3:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">3:00 pm</span> </a> </div>
                </div>
              </div>
            </div>
            <div class="film-button-container" style="display:block; float:right; margin-right:1em;">
              <div style="padding:1em 0;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/start" class="button tk-sorts-mill-goudy ticketing-btn pulse-button btn-buy-tickets" style="width:200px"> <span class="button-text btn-buy-tickets">Buy Tickets</span> </a> </div>
            </div>
          </div>
          <div class="film-listing-container-desktop">
            <div class="film-image" style="float:left; padding:0 1em;"><a href="#" data-toggle="modal" data-target="#basicModal">
              <amp-img id=577 class="modal-thumb" src=https://theroxy.org/appimages/us/custom/MINARC/2019%20oscar%20shorts:%20documentary.jpg data-film-name="2019 Oscar Shorts: Documentary" data-film-rating="NR" data-film-runtime="143" data-film-synopsis="Don't miss your chance to experience this year's best short films the way they were meant to be seen – on the big screen – “The short films … offer a vision of what the Academy Awards should and could be but very rarely are: eclectic, cosmopolitan, scrappy and surprising.” – A.O. Scott, New York Times" data-playing-status="Now Showing" data-performance-count="3" data-single-performance="" width="200" height="300"></amp-img>
            </a></div>
            <div class="movie-name"> <span class="tk-sorts-mill-goudy" style="color:#e5ae47; font-size:180%; text-transform: uppercase;"><strong>2019 Oscar Shorts: Documentary</strong></span> </div>
            <div class="movie-rating-runtime"> <span style="color:#fff;font-size:120%">NR, 143 Minutes</span> </div>
            <br/>
            <div class="movie-times" style="float:left; display:grid;">
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Mon (02/18):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50524"> <span id="2019 Oscar Shorts: Documentary->2019-02-18->7:30pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">7:30 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Tue (02/19):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50533"> <span id="2019 Oscar Shorts: Documentary->2019-02-19->7:30pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">7:30 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Wed (02/20):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50548"> <span id="2019 Oscar Shorts: Documentary->2019-02-20->4:30pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">4:30 pm</span> </a> </div>
                </div>
              </div>
            </div>
            <div class="film-button-container" style="display:block; float:right; margin-right:1em;">
              <div style="padding:1em 0;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/start" class="button tk-sorts-mill-goudy ticketing-btn pulse-button btn-buy-tickets" style="width:200px"> <span class="button-text btn-buy-tickets">Buy Tickets</span> </a> </div>
            </div>
          </div>
          <div class="film-listing-container-desktop">
            <div class="film-image" style="float:left; padding:0 1em;"><a href="#" data-toggle="modal" data-target="#basicModal">
              <amp-img id=578 class="modal-thumb" src=https://theroxy.org/appimages/us/alita%20battle%20angel.jpg data-film-name="Alita: Battle Angel" data-film-rating="PG-13" data-film-runtime="122" data-film-synopsis="An action-packed story of one young womans journey to discover the truth of who she is and her fight to change the world." data-playing-status="New Release" data-performance-count="12" data-single-performance="" width="200" height="300"></amp-img>
            </a></div>
            <div class="movie-name"> <span class="tk-sorts-mill-goudy" style="color:#e5ae47; font-size:180%; text-transform: uppercase;"><strong>Alita: Battle Angel</strong></span> </div>
            <div class="movie-rating-runtime"> <span style="color:#fff;font-size:120%">PG-13, 122 Minutes</span> </div>
            <br/>
            <div class="movie-times" style="float:left; display:grid;">
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Mon (02/18):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50515"> <span id="Alita: Battle Angel->2019-02-18->3:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">3:00 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50513"> <span id="Alita: Battle Angel->2019-02-18->6:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">6:00 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50514"> <span id="Alita: Battle Angel->2019-02-18->9:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">9:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Tue (02/19):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50528"> <span id="Alita: Battle Angel->2019-02-19->3:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">3:00 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50526"> <span id="Alita: Battle Angel->2019-02-19->6:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">6:00 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50527"> <span id="Alita: Battle Angel->2019-02-19->9:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">9:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Wed (02/20):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50537"> <span id="Alita: Battle Angel->2019-02-20->3:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">3:00 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50535"> <span id="Alita: Battle Angel->2019-02-20->6:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">6:00 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50536"> <span id="Alita: Battle Angel->2019-02-20->9:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">9:00 pm</span> </a> </div>
                </div>
              </div>
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Thu (02/21):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50483"> <span id="Alita: Battle Angel->2019-02-21->3:30pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">3:30 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50491"> <span id="Alita: Battle Angel->2019-02-21->6:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">6:00 pm</span> </a> </div>
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50492"> <span id="Alita: Battle Angel->2019-02-21->9:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">9:00 pm</span> </a> </div>
                </div>
              </div>
            </div>
            <div class="film-button-container" style="display:block; float:right; margin-right:1em;">
              <div style="padding:1em 0;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/start" class="button tk-sorts-mill-goudy ticketing-btn pulse-button btn-buy-tickets" style="width:200px"> <span class="button-text btn-buy-tickets">Buy Tickets</span> </a> </div>
              <div style="padding-top:1em;"> <a href="#" class="button tk-sorts-mill-goudy video-btn btn-view-trailer" style="width:200px" data-toggle="modal" data-src="w7pYhpJaJW8" data-banner="https://theroxy.org/titleimages/alita%20battle%20angel.jpg" data-target="#trailerModal" data-playing-status="New Release" data-performance-count="12" data-single-performance=""> <span class="button-text btn-view-trailer">View Trailer</span> </a> </div>
            </div>
          </div>
          <div class="film-listing-container-desktop">
            <div class="film-image" style="float:left; padding:0 1em;"><a href="#" data-toggle="modal" data-target="#basicModal">
              <amp-img id=579 class="modal-thumb" src=https://theroxy.org/appimages/us/custom/MINARC/the%20insult%20(2017).jpg data-film-name="The Insult (2017)" data-film-rating="R" data-film-runtime="113" data-film-synopsis="2018 Oscar Nominated Lebonese film The Insult will presented with Q&A from lead actor Kamel El Basha, who won Best Actor at the Venice Film Fest for his turn in this thorny morality play that thoughtfully transcends borders, cultures, and religions. Don't miss this One Night Only special event." data-playing-status="Coming Soon" data-performance-count="1" data-single-performance="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50461" width="200" height="300"></amp-img>
            </a></div>
            <div class="movie-name"> <span class="tk-sorts-mill-goudy" style="color:#e5ae47; font-size:180%; text-transform: uppercase;"><strong>The Insult (2017)</strong></span> </div>
            <div class="movie-rating-runtime"> <span style="color:#fff;font-size:120%">R, 113 Minutes</span> </div>
            <br/>
            <div class="movie-times" style="float:left; display:grid;">
              <div class="performance-container">
                <div class="film-performance-date-row" style="float:left; width:120px;"> <span style="font-size:130%">Thu (02/21):</span> </div>
                <div style="float:left; display:inline-block;">
                  <div class="film-performance-time" style="float:left;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50461"> <span id="The Insult (2017)->2019-02-21->7:00pm" class="ticketing-link" style="font-size:130%; text-decoration:underline; margin-right:20px;">7:00 pm</span> </a> </div>
                </div>
              </div>
            </div>
            <div class="film-button-container" style="display:block; float:right; margin-right:1em;">
              <div style="padding:1em 0;"> <a href="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50461" class="button tk-sorts-mill-goudy ticketing-btn pulse-button btn-buy-tickets" style="width:200px"> <span class="button-text btn-buy-tickets">Buy Tickets</span> </a> </div>
              <div style="padding-top:1em;"> <a href="#" class="button tk-sorts-mill-goudy video-btn btn-view-trailer" style="width:200px" data-toggle="modal" data-src="8l3z84kehg0" data-banner="the%20insult.jpg" data-target="#trailerModal" data-playing-status="Coming Soon" data-performance-count="1" data-single-performance="https://www.internet-ticketing.com/websales/sales/main/minarc/actual_book?perfcode=50461"> <span class="button-text btn-view-trailer">View Trailer</span> </a> </div>
            </div>
          </div>
</div>
