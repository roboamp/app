@extends ('customers/dashboard.master')

@section('page_header_specific_scripts')
		<link href="/css/app.css" rel="stylesheet">

		<script src="https://checkout.stripe.com/checkout.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://use.typekit.net/cbe3rku.js"></script>
		<script>try { Typekit.load({ async: true }); } catch (e) {}</script>
		<link href='//fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' id='main-style-css'  href='{{asset('css/plans.css')}}' type='text/css' media='all' />
		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
@endsection
@section('content')
<?php
    if(!is_null($customer['name'])){
        $txt_hidden='<input type="hidden" name="edit_customer_id" value="'.$customer['id'].'">';
        $txt_name_placeholder="";
        $txt_name_readonly="readonly";
        $txt_name_value=$customer['name'];
        $btn_submit_value="Add Website";
    }else{
        if($login_type=='customers'){
		}else{
			$txt_hidden="";
			$txt_name_placeholder="Customer Name";
			$txt_name_readonly="";
			$txt_name_value="";
			$btn_submit_value="Add Customer";
        }
    }

?>

	<home :customer="user" inline-template>
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-header">
						{{$btn_submit_value}}
					</div>
				</div>
				<div class="panel-body">
					<form id="status_form" method="POST" class="form-group" action="{{route('customers.add.property.submit')}}">
						{{ csrf_field() }}
						<input type="hidden" name="stripeToken" id="stripeToken">
                        <input type="hidden" name="stripeEmail" id="stripeEmail">
                        <div class="form-group">
							<h3 class="text-center"><label for="name">STEP 1: Enter URL for this Website </label></h3>
							<input type="text" id="url" class="form-control" name="url" value="{{$url}}" autofocus required onfocus="var temp_value=this.value; this.value=''; this.value=temp_value"/>
							<br>
							<h3 class="text-center"><label for="plan_stripe_id">STEP 2: Select a plan!</label></h3>
							<p id="plan"></p>
						</div>
						<div class="row">
							<body id="homepage">
							<section id="products">
								<div class="center container pt2">
									<div class="cf">

										@foreach($plans as $myplans)
											<div class="{{(count($plans)==3?"col-md-4":"col-md-6")}}">
												@include('layouts.add_customer',$myplans)
											</div>
										@endforeach
									</div>
								</div>
							</section>
							</body>
						</div>
						<div class="form-group text-center submit-button">
                            <input disabled type="submit" class="form-control btn_submit" id="btn_submit" name="submit" value="{{$btn_submit_value}}">
						</div>
                        <input id="txt_status_form" type="hidden" name="plan_stripe_id" value="">
					</form>
				</div>
			</div>
		</div>
	</home>

	@include('includes.stop-form-submit')

    @include('users.js.add',array('process_stripe_url'=>route($callback),'redirect_url'=>route('customers.dashboard')))
@endsection