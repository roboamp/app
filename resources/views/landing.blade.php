<!DOCTYPE html>
<html lang="en">
@include ('includes.landing.landing-styles')

<body>

    <!-- Header -->
    @include('includes.landing.landing-nav')

    <!-- Features -->
    @include ('includes.landing.landing-features')

    <!-- Banner -->
    


    <!-- About & Count -->

    <!-- How it works -->
    @include ('includes.landing.landing-how-it-works')

    @include('includes.landing.sections.case_studies',['case_studies'=>$case_studies])
    @include('includes.landing.sections.plans',['plans'=>$plans])
    @include('includes.landing.sections.carousel',['carousel_testimonials'=>$carousel_testimonials])
    @include('includes.landing.sections.team',['team_members'=>$team_members])




    <!-- Download area -->
    <!-- @include ('includes.landing.landing-download-area') -->

    <!-- Subscribe box -->
    @include ('includes.landing.landing-subscribe-box')

    <!-- contact section -->
    @include('includes.landing.landing-contact-section')

    <!-- Map area-->
    <!-- @include('includes.landing.landing-map-area') -->

    <!-- Footer -->
    @include ('includes.landing.landing-footer')

    <!-- Script -->

</body>

@include('includes.landing.landing-scripts')

</html>
