

<?php

$url=rtrim(urlencode(get_the_permalink()),"/");
$array=[
    "https%3A%2F%2Fwww.ioogo.com",
    "https%3A%2F%2Fwww.ioogo.com%2Findividuals",
    "https%3A%2F%2Fwww.ioogo.com%2Fbusinesses",
    "https%3A%2F%2Fblog.ioogo.com"
];

function validate_subdomain($url){
    $parsedUrl = parse_url(urldecode($url));
    $host = explode('.', $parsedUrl['host']);
    return (isset($host && $host[0]=="blog")?true:false);
}


function check_for_string_in_array($url,$array){
    foreach ($array as $element) {
        if ($url=== $element)  {
            return true;
        }
    }
    if(validate_subdomain($url)){return true;}

    return null;
}


if(check_for_string_in_array($url,$array)){
    echo "<link rel='amphtml' href='some_url&page=".$url."'>";
}

echo $url;

?>

/*
function get_encoded_urls(){
    $arr=[
        "https://www.ioogo.com/",
        "https://www.ioogo.com/individuals/",
        "https://www.ioogo.com/businesses/",
        "https://blog.ioogo.com/",
    ];

    foreach ($arr as $url){
        echo urlencode(rtrim($url,"/"))."<br>";
    }

    die();
};

get_encoded_urls();

function validate_subdomain($url){

    $parsedUrl = parse_url(urldecode($url));
    $host = explode('.', $parsedUrl['host']);
    return (isset($host && $host[0]=="blog")?true:false);
}


validate_subdomain('https%3A%2F%2Fblog.taytus.com%2F2020%2F01%2F29%2Fhello-world%2F');


$array=[
    "https%3A%2F%2Fwww.ioogo.com",
    "https%3A%2F%2Fwww.ioogo.com%2Findividuals",
    "https%3A%2F%2Fwww.ioogo.com%2Fbusinesses",
    "https%3A%2F%2Fblog.ioogo.com"
];

function check_for_string_in_array($url,$array){
    foreach ($array as $element) {
        if ($url=== $element)  {
            return true;
        }
    }
    if(validate_subdomain($url)){return true;}

    return null;
}


if(check_for_string_in_array($url,$array)){
    echo ("<link rel='amphtml' href='some_url&page=".$url."'>");
}
*/