<!doctype html>
<!--{/*
@info
Generated on: Fri, 09 Feb 2018 11:26:57 GMT
Initiator: ROBOAMP AMP Generator
Generator version: 0.9.6
*/}-->
<html ⚡ lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>HOME - NōD Coworking</title>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <script src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js" async="async" custom-element="amp-analytics"></script>
        <script src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async="async" custom-element="amp-sidebar"></script>
        <link rel="canonical" href="http://www.noddfw.com/">
        <meta name="generator" content="https://roboamp.com">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta property="og:locale" content="en_US">
        <meta property="og:type" content="website">
        <meta property="og:title" content="HOME - NōD Coworking">
        <meta property="og:url" content="http://www.noddfw.com/">
        <meta property="og:site_name" content="NōD Coworking">
        <meta name="twitter:card" content="summary">
        <meta name="twitter:title" content="HOME - NōD Coworking">
        <meta name="twitter:site" content="@noddfw">
        <meta name="twitter:creator" content="@noddfw">
        <meta property="fb:app_id" content="760014087422302">
        <meta property="fb:admins" content="100002975475925,825550647">
        <meta name="msapplication-TileImage" content="http://www.noddfw.com/wp-content/uploads/2016/04/cropped-Screen-Shot-2016-04-22-at-11.21.58-PM-270x270.png">
        <style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript data-amp-spec=""><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=EB+Garamond%7CSpinnaker&#038;ver=3.0.1">
        <style amp-custom="">@font-face {
  font-family: dt-twitter;
  src: url(http://www.noddfw.com/wp-content/plugins/digicution-simple-twitter-feed/fonts/dt-twitter.eot);
  src: url(http://www.noddfw.com/wp-content/plugins/digicution-simple-twitter-feed/fonts/dt-twitter.eot?#iefix) format('embedded-opentype'),url(http://www.noddfw.com/wp-content/plugins/digicution-simple-twitter-feed/fonts/dt-twitter.ttf) format('truetype'),url(http://www.noddfw.com/wp-content/plugins/digicution-simple-twitter-feed/fonts/dt-twitter.woff) format('woff'),url(http://www.noddfw.com/wp-content/plugins/digicution-simple-twitter-feed/fonts/dt-twitter.svg#dt-twitter) format('svg');
  font-weight: 400;
  font-style: normal
}
footer,
header,
main,
nav,
section {
  display: block
}
html {
  font-family: sans-serif;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%
}
body {
  margin: 0
}
a:focus {
  outline: thin dotted
}
a:active,
a:hover {
  outline: 0
}
amp-img {
  border: 0
}
svg:not(:root) {
  overflow: hidden
}
input[type=search]::-webkit-search-cancel-button,
input[type=search]::-webkit-search-decoration {
  -webkit-appearance: none
}
button::-moz-focus-inner,
input::-moz-focus-inner {
  border: 0;
  padding: 0
}
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
.footer-widgets:before,
.site-container:before,
.site-footer:before,
.site-header:before,
.site-inner:before,
.widget:before,
.wrap:before {
  content: " ";
  display: table
}
.footer-widgets:after,
.site-container:after,
.site-footer:after,
.site-header:after,
.site-inner:after,
.widget:after,
.wrap:after {
  clear: both;
  content: " ";
  display: table
}
body {
  background-color: #d7c603;
  color: #666;
  font-family: 'Open Sans',serif;
  font-size: 18px;
  font-weight: 400;
  line-height: 1.625
}
::-moz-selection {
  background-color: #333;
  color: #fff
}
a {
  color: #f07802;
  text-decoration: none
}
a amp-img {
  margin-bottom: -4px
}
a:hover {
  color: #333
}
p {
  margin: 0 0 24px;
  padding: 0
}
ul {
  margin: 0;
  padding: 0
}
h4 {
  font-family: Spinnaker,sans-serif;
  font-weight: 400;
  line-height: 1.2;
  margin: 0 0 16px
}
h4 {
  font-size: 20px
}
amp-img {
  max-width: 100%
}
amp-img {
  height: auto
}
::-moz-placeholder {
  color: #999
}
::-webkit-input-placeholder {
  color: #999
}
.button {
  background-color: #f07802;
  border-radius: 3px;
  border: none;
  box-shadow: none;
  color: #fff;
  cursor: pointer;
  font-family: Montserrat,sans-serif;
  padding: 10px 15px;
  text-transform: uppercase;
  width: auto
}
.button {
  display: inline-block
}
.button:hover {
  background-color: #d7c603;
  color: #fff
}
input[type=search]::-webkit-search-cancel-button,
input[type=search]::-webkit-search-results-button {
  display: none
}
.site-container {
  padding-top: 61px
}
.site-inner,
.wrap {
  margin: 0 auto;
  max-width: 1140px
}
.site-inner {
  clear: both;
  padding: 5% 0
}
.agency-pro-home .site-inner {
  padding: 0
}
.content {
  float: right;
  width: 740px
}
.full-width-content .content {
  width: 100%
}
.alignleft {
  float: left;
  text-align: left
}
.widget-title {
  color: #333;
  font-size: 16px;
  margin-bottom: 16px;
  text-transform: uppercase
}
.widget {
  word-wrap: break-word
}
.widget li {
  list-style-type: none;
  margin-bottom: 6px
}
.widget li li {
  margin: 0;
  padding: 0 0 0 20px
}
.site-header {
  background-color: #333;
  left: 0;
  min-height: 100px;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 999
}
.title-area {
  float: left;
  padding: 16px 0;
  width: 242px
}
.site-title a {
  color: #fff
}
.site-title a:hover {
  color: #d7c603
}
.header-image .site-title a {
  display: block;
  text-indent: -9999px
}
.header-image .site-header .wrap {
  padding: 0
}
.header-image .site-title a {
  float: left;
  min-height: 100px;
  width: 242px
}
.site-header .widget-area {
  float: right;
  text-align: right;
  width: 850px
}
.site-header .widget-area li {
  margin: 0;
  padding: 0
}
.genesis-nav-menu {
  clear: both;
  color: #fff;
  font-family: Spinnaker,sans-serif;
  font-size: 14px;
  line-height: 1.5;
  padding-top: 20px;
  text-align: left;
  width: 550px
}
.genesis-nav-menu .menu-item {
  display: inline-block;
  text-align: left
}
.genesis-nav-menu a {
  border-top: 2px solid transparent;
  color: #fff;
  display: block;
  padding: 10px;
  position: relative
}
.genesis-nav-menu a:hover {
  background-color: #fff;
  border-color: #d7c603;
  color: #d7c603
}
.genesis-nav-menu .sub-menu {
  left: -9999px;
  opacity: 1;
  position: absolute;
  width: 180px;
  z-index: 99
}
.genesis-nav-menu .sub-menu a {
  background-color: #fff;
  border: none;
  font-size: 12px;
  padding: 16px;
  position: relative;
  width: 180px
}
.genesis-nav-menu .sub-menu a:hover {
  background-color: #333
}
.genesis-nav-menu .menu-item:hover {
  position: static
}
.genesis-nav-menu .menu-item:hover > .sub-menu {
  left: auto;
  opacity: 1
}
.site-header .genesis-nav-menu {
  float: left;
  width: 500px
}
.home-top .wrap {
  color: #fff;
  font-family: 'Open Sans',sans-serif;
  padding: 15% 0;
  text-align: center
}
.home-top .button {
  background-color: #fff;
  border-radius: 50px;
  color: #333;
  text-transform: uppercase
}
.home-top .widget .button:hover {
  background-color: #333;
  color: #fff
}
.footer-widgets {
  background-color: #fff;
  border-top: 1px solid #ececec;
  color: #999;
  clear: both;
  padding: 40px 0 0
}
.footer-widgets-1,
.footer-widgets-3 {
  width: 350px
}
.footer-widgets-2 {
  width: 360px
}
.footer-widgets-1 {
  margin-right: 40px
}
.footer-widgets-1,
.footer-widgets-2 {
  float: left
}
.footer-widgets-3 {
  float: right
}
.footer-widgets .widget {
  margin-bottom: 24px
}
.footer-widgets li {
  list-style-type: none;
  margin-bottom: 6px
}
.site-footer {
  background-color: #f5f5f5;
  color: #999;
  font-family: Spinnaker,sans-serif;
  font-size: 10px;
  letter-spacing: 2px;
  padding: 20px 0;
  text-align: center;
  text-transform: uppercase
}
.site-footer a {
  color: #999
}
.site-footer a:hover {
  color: #333
}
.site-footer p {
  margin-bottom: 0
}
.agency-pro-orange .genesis-nav-menu a:hover,
.agency-pro-orange .site-title a:hover,
.agency-pro-orange a {
  color: #f07802
}
.agency-pro-orange .genesis-nav-menu a,
.agency-pro-orange .home-top a:hover,
.agency-pro-orange .site-title a,
.agency-pro-orange a.button,
.agency-pro-orange a:hover.button {
  color: #333
}
.agency-pro-orange .site-footer a {
  color: #999
}
.agency-pro-orange .home-top a,
.agency-pro-orange .site-footer a:hover,
.agency-pro-orange a:hover {
  color: #f07802
}
.agency-pro-orange .genesis-nav-menu a:hover {
  border-color: #f07802
}
.agency-pro-orange a:hover.button,
body.agency-pro-orange {
  background-color: #f07802
}
.site-inner,
.wrap {
  max-width: 960px
}
.content {
  width: 620px
}
.site-header .widget-area {
  width: 660px
}
.footer-widgets-1,
.footer-widgets-2,
.footer-widgets-3 {
  width: 300px
}
.site-inner,
.wrap {
  max-width: 760px
}
.content,
.footer-widgets-1,
.footer-widgets-2,
.footer-widgets-3,
.site-header .widget-area,
.title-area {
  width: 100%
}
.site-container {
  padding-top: 105px
}
.header-image .site-title a {
  background-position: center;
  margin: 0 0 6px
}
.genesis-nav-menu li,
.site-header ul.genesis-nav-menu {
  float: none
}
.genesis-nav-menu,
.site-header .title-area,
.site-title {
  text-align: center
}
.genesis-nav-menu a {
  padding: 20px 16px
}
.site-footer {
  padding: 24px 0
}
.footer-widgets {
  padding: 40px 5% 16px
}
.footer-widgets-1 {
  margin: 0
}
.genesis-nav-menu a {
  padding: 10px 12px 12px
}
.genesis-nav-menu .sub-menu a {
  padding: 12px
}
.agency-pro-home .wrap {
  max-width: 640px
}
.site-inner {
  padding: 5%
}
.site-container {
  padding: 0
}
.site-header {
  position: static
}
.wrap {
  width: 90%
}
.site-header {
  background: url(http://www.northdallascoworking.com/wp-content/uploads/2014/03/cubes.png) repeat scroll 0 0 rgba(0,0,0,0);
  border-bottom: 1px solid #d2d2d2;
  position: fixed;
  width: 100%;
  z-index: 999
}
#text-2 a.button {
  color: #fff
}
header.site-header {
  display: none
}
@font-face {
  font-family: job-manager;
  src: url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/job-manager.eot?4963673);
  src: url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/job-manager.eot?4963673#iefix) format('embedded-opentype'),url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/job-manager.woff?4963673) format('woff'),url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/job-manager.ttf?4963673) format('truetype'),url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/job-manager.svg?4963673#job-manager) format('svg');
  font-weight: 400;
  font-style: normal
}
@font-face {
  font-family: jm-logo;
  src: url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/jm-logo/jm.eot?ycsbky);
  src: url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/jm-logo/jm.eot?#iefixycsbky) format('embedded-opentype'),url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/jm-logo/jm.woff?ycsbky) format('woff'),url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/jm-logo/jm.ttf?ycsbky) format('truetype'),url(http://www.noddfw.com/wp-content/plugins/wp-job-manager/assets/font/jm-logo/jm.svg?ycsbky#icomoon) format('svg');
  font-weight: 400;
  font-style: normal
}
.hamburger {
  padding: 15px 15px;
  display: inline-block;
  cursor: pointer;
  transition-property: opacity,filter;
  transition-duration: .15s;
  transition-timing-function: linear;
  font: inherit;
  color: inherit;
  text-transform: none;
  background-color: transparent;
  border: 0;
  margin: 0;
  overflow: visible
}
.hamburger-box {
  width: 30px;
  height: 24px;
  display: inline-block;
  position: relative
}
.hamburger-inner {
  display: block;
  top: 50%;
  margin-top: -2px
}
.hamburger-inner,
.hamburger-inner::after,
.hamburger-inner::before {
  width: 30px;
  height: 4px;
  background-color: #000;
  border-radius: 4px;
  position: absolute;
  transition-property: transform;
  transition-duration: .15s;
  transition-timing-function: ease
}
.hamburger-inner::after,
.hamburger-inner::before {
  content: "";
  display: block
}
.hamburger-inner::before {
  top: -10px
}
.hamburger-inner::after {
  bottom: -10px
}
.hamburger--slider .hamburger-inner {
  top: 2px
}
.hamburger--slider .hamburger-inner::before {
  top: 10px;
  transition-property: transform,opacity;
  transition-timing-function: ease;
  transition-duration: .15s
}
.hamburger--slider .hamburger-inner::after {
  top: 20px
}
@font-face {
  font-family: wprmenu;
  src: url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/fonts/wprmenu.eot?p8o4s0);
  src: url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/fonts/wprmenu.eot?p8o4s0#iefix) format('embedded-opentype'),url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/fonts/wprmenu.ttf?p8o4s0) format('truetype'),url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/fonts/wprmenu.woff?p8o4s0) format('woff'),url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/fonts/wprmenu.svg?p8o4s0#wprmenu) format('svg');
  font-weight: 400;
  font-style: normal
}
#wprmenu_bar,
#wprmenu_bar * {
  margin: 0;
  padding: 0;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  font-family: inherit;
  list-style: none;
  text-align: left;
  font-size: 100%;
  font-weight: 400
}
#wprmenu_bar {
  position: fixed;
  top: 0;
  left: 0;
  background: #4c656c;
  padding: 10px 16px;
  width: 100%;
  cursor: pointer;
  overflow: hidden;
  height: 42px;
  display: none;
  z-index: 99999
}
#wprmenu_bar .menu_title {
  font-weight: 400;
  font-size: 20px;
  line-height: 35px;
  position: relative;
  top: -6px
}
#wprmenu_bar div.hamburger {
  margin-top: -3px
}
body {
  position: relative
}
#wprmenu_menu_ul .sub-menu {
  list-style: none;
  padding: 0
}
#mg-wprm-wrap {
  display: none
}
#mg-wprm-wrap ul li {
  margin: 0
}
#mg-wprm-wrap ul li ul.sub-menu li {
  border: none;
  position: relative
}
#mg-wprm-wrap {
  z-index: 9999
}
#mg-wprm-wrap ul li a {
  text-decoration: none;
  z-index: 9999
}
#mg-wprm-wrap li.menu-item-has-children {
  position: relative
}
#wprmenu_menu_ul {
  padding: 0 0 50px;
  margin: 0;
  list-style: none;
  overflow: hidden
}
#wprmenu_menu_ul ul.sub-menu {
  margin-left: 0
}
#mg-wprm-wrap li.menu-item a {
  padding: 13px 14px
}
.cbp-spmenu {
  position: fixed;
  overflow: none;
  height: 100%;
  z-index: 9999
}
.cbp-spmenu a {
  padding: 1em;
  display: block
}
#mg-wprm-wrap.cbp-spmenu-left {
  top: 42px
}
.cbp-spmenu-left {
  left: -100%
}
#mg-wprm-wrap ul li {
  border-top: solid 1px #474747;
  border-bottom: solid 1px #131212
}
#wprmenu_bar {
  background-color: #0d0d0d
}
#wprmenu_bar .menu_title {
  color: #f2f2f2
}
#wprmenu_bar .menu_title {
  font-size: 20px
}
#mg-wprm-wrap li.menu-item a {
  font-size: px
}
#mg-wprm-wrap li.menu-item-has-children ul.sub-menu a {
  font-size: px
}
#mg-wprm-wrap {
  background-color: #2e2e2e
}
#mg-wprm-wrap.cbp-spmenu-left,
.cbp-spmenu-vertical {
  width: 80%;
  max-width: px
}
#mg-wprm-wrap ul#wprmenu_menu_ul li.menu-item a {
  color: #cfcfcf
}
#mg-wprm-wrap ul#wprmenu_menu_ul li.menu-item a:hover {
  color: #606060
}
div.wprmenu_bar div.hamburger {
  padding-right: 6px
}
.wprmenu_bar .hamburger {
  float: left
}
html {
  padding-top: 42px
}
#mg-wprm-wrap,
#wprmenu_bar {
  display: block
}
@font-face {
  font-family: icomoon;
  src: url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/inc/icons/fonts/icomoon.eot?5ujmx2);
  src: url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/inc/icons/fonts/icomoon.eot?5ujmx2#iefix) format('embedded-opentype'),url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/inc/icons/fonts/icomoon.ttf?5ujmx2) format('truetype'),url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/inc/icons/fonts/icomoon.woff?5ujmx2) format('woff'),url(http://www.noddfw.com/wp-content/plugins/wp-responsive-menu/inc/icons/fonts/icomoon.svg?5ujmx2#icomoon) format('svg');
  font-weight: 400;
  font-style: normal
}
.simple-social-icons svg[class^=social-] {
  display: inline-block;
  width: 1em;
  height: 1em;
  stroke-width: 0;
  stroke: currentColor;
  fill: currentColor
}
.simple-social-icons {
  overflow: hidden
}
.simple-social-icons ul {
  margin: 0;
  padding: 0
}
.simple-social-icons ul li {
  background: 0 0;
  border: none;
  float: left;
  list-style-type: none;
  margin: 0 6px 12px;
  padding: 0
}
.simple-social-icons ul li a {
  border: none;
  -moz-box-sizing: content-box;
  -webkit-box-sizing: content-box;
  box-sizing: content-box;
  display: inline-block;
  font-style: normal;
  font-variant: normal;
  font-weight: 400;
  height: 1em;
  line-height: 1em;
  text-align: center;
  text-decoration: none;
  text-transform: none;
  width: 1em
}
@font-face {
  font-family: TablePress;
  src: url(http://www.noddfw.com/wp-content/plugins/tablepress/css/tablepress.eot);
  src: url(http://www.noddfw.com/wp-content/plugins/tablepress/css/tablepress.eot?#ie) format('embedded-opentype'),url(data:application/x-font-woff;base64,d09GRgABAAAAAAXYAAwAAAAACXwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAABHAAAABwAAAAcZInHOk9TLzIAAAE4AAAAPgAAAGB7NXPDY21hcAAAAXgAAABMAAABUvD45QVnYXNwAAABxAAAAAgAAAAIAAAAEGdseWYAAAHMAAABKQAAAgjYVHFyaGVhZAAAAvgAAAAvAAAANv2jaBNoaGVhAAADKAAAAB0AAAAkCk0F6GhtdHgAAANIAAAAIAAAACgoAADqbG9jYQAAA2gAAAAWAAAAFgJWAbxtYXhwAAADgAAAAB0AAAAgAE4AH25hbWUAAAOgAAAB3AAAA43ZugaUcG9zdAAABXwAAABZAAAAgeNVfAkAAAABAAAAAMwXcmMAAAAAy1XFoAAAAADNHI82eJxjYGbexjiBgZWBhaWHxZiBgaENQjMVM0SB+ThBQWVRMYPDh+APd9gY/gP5bAyMykCKEUmJAgMjAC56Ct8AAHicY2BgYGaAYBkGRgYQ8AHyGMF8FgYDIM0BhEwMDB9CPtz6cOf/fyAr+MN1EIt/A78evy5UFxgwsjHAuYxAPSB9KICRYdgDAL67D8kAAQAB//8AD3icXU/NTsJAEJ5ZFggxxmCbkiwHAzLTowlu4ejeyxPwCOBR7r3rE/geeubuG3i3TyGts12MtT18ndnd7w8QsvpdX/Q/IAboI9s1rpY3mMTDkcAVDuaqPD0RqRfaU05UGSxJhj2p5wDNUplqyjsZQT6EQ/2mc9GcyBIPReQOObPrkccHMZhgonMS3R1tiLCsDNFm8bgQF3W06ekgqqnN5Aa//D9nlrOgDeg09Aq4lK3RTiX16n621BBVLrIe2PQK09s2c4THyPIvF4TrAndwO5uHvhPUwOa7MNw8FsCtZfZDJBKB66BQhW58cRwHW09OVMEtI+3aOhEHX+E63crM2dh6X+WazMFLvf6F57OvAqiPGvo+87Tb+LrT4X9/7lT6bKXs9vsByDlkyAAAAHicY2BkYGAA4mO9Sxji+W2+MnCzM4DAWZl+bwT9fyuLN+tpIJeDgQkkCgAbqgoOAHicY2BkYGBj+HeXgYGdAQRYvBkYGVABFwA+hgJEAAAAeJxjZ4AAdiBmaWAwBeJSFhAbiJkaGByAGMwGADa+AxEAAAAAAAAAAAAqAFgAdgCUALIA0AEEAAB4nGNgZGBg4GKQYWBiAAEQycgAEnMA8xkABjEAbgAAAHicjZJBaxNBFMf/u0mbJoig5KC3QcRblt2tFCm9lEp701iKIHiZJLPJ0mSnzE5S8h38BB79Nn4ab94E/zN9JVB7MCEzv/3Pm/f+720ADPEbCe4/qXCCAV4Kp+jhrXAHGb4Ld/Ecv4T38Cx5I7yPQfJBuIdPyR/hAzxJa+E+Buk3Zku6fal8xwn9dIRTPMVr4Q6+4kS4S/Wn8B5eJanwPobJO+EefiRfhA/wIv0o3Mcw3YRyV9CYYAmDMRzXll9c6cnSjJ1pyZcU51gzRDMAl2a+XmrCOSwa+Lg7RhgolJxKzv2Yv8cSqwfZ7iILHGHEdcT7OfdDJreNP7dublSZ5epY7QwpMUCxOBoVozIvDh/v4mEx/JsE+MwQx+A6dhO8hA7y2EvwUjLEuLa2jSqyPFcsV/7H0MbxUFMOYwkhVSykODAb10U8OePTDbY8qel0QV3FO1PSvaUqDnh3p5LBB8WxxozqKnZ4TU1T9THfhJ3vsjTcw9M02s1ocWl0a5QzlXHKW+UXRp3Zm62r5wuvWjP1oenKunhS8YUo7/TMrLS7Vtp7V0/WMaSxvp6aNoP8FYKzU9zGOpbOTHidzqvTW9PaFZ/e0+QmtnJBatjAlqLeGHWhm9kWfwGPR6C4eJxjYGIAg/9VDFEM2AAXEDMyMDEyMTIzsjCyMrIxsjNy8CRnpJYV5efF56SmlfDCOEWZ6RklXMmJRakl8Sn55XkcEGZpAVQMpJgbwgQrZSnOLyoBACnGHqgAAAA=) format('woff'),url(http://www.noddfw.com/wp-content/plugins/tablepress/css/tablepress.ttf) format('truetype'),url(http://www.noddfw.com/wp-content/plugins/tablepress/css/tablepress.svg#TablePress) format('svg');
  font-weight: 400;
  font-style: normal
}
.widget-meetup-event-list-day .wpm-number-display:afer {
  content: " "
}
.site-title a {
  background: url(http://www.noddfw.com/wp-content/uploads/2014/04/logo.png) no-repeat
}
.simple-social-icons ul li a,
.simple-social-icons ul li a:focus,
.simple-social-icons ul li a:hover {
  background-color: #999;
  border-radius: 36px;
  color: #fff;
  border: 0 #fff solid;
  font-size: 18px;
  padding: 9px
}
.simple-social-icons ul li a:focus,
.simple-social-icons ul li a:hover {
  background-color: #f07802;
  border-color: #fff;
  color: #fff
}
.simple-social-icons ul li a:focus {
  outline: 1px dotted #f07802
}
.rbt-inline-0 {
  list-style-type: none;
  margin: 0;
  padding: 0;
  width: %
}
#dmagazinelogo_img {
  width: 50px;
  height: auto
}
#launchedindfw_img {
  width: 50px;
  height: auto
}
#rbt-sidebar {
  width: 80%;
  background: #eee
}
#rbt-sidebar ul li {
  display: block
}</style>
    </head>
    <body class="home page-template-default page page-id-746 custom-background custom-header header-image full-width-content agency-pro-orange agency-pro-theme agency-pro-home" itemscope="" itemtype="https://schema.org/WebPage">
        <amp-sidebar id="rbt-sidebar" layout="nodisplay">
            <section id="nav_menu-2" class="widget widget_nav_menu">
                <div class="widget-wrap">
                    <nav class="nav-header" itemscope="" itemtype="https://schema.org/SiteNavigationElement">
                        <ul id="menu-main" class="menu genesis-nav-menu">
                            <li id="menu-item-1342" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1342"><a href="http://www.noddfw.com/our-space/" itemprop="url"><span itemprop="name">SPACE</span></a>
                                <ul class="sub-menu">
                                    <li id="menu-item-1141" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1141"><a title="Coworking Space" href="http://www.noddfw.com/our-space/coworking-lounge/" itemprop="url"><span itemprop="name">COWORKING</span></a></li>
                                    <li id="menu-item-1182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1182"><a title="Dedicated Desks &#038; Offices" href="http://www.noddfw.com/our-space/offices/" itemprop="url"><span itemprop="name">PRIVATE OFFICES</span></a></li>
                                    <li id="menu-item-1140" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1140"><a title="Meeting &#038; Conference Rooms" href="http://www.noddfw.com/our-space/meeting-and-conference-rooms/" itemprop="url"><span itemprop="name">CONFERENCE ROOMS</span></a></li>
                                </ul>
                            </li>
                            <li id="menu-item-3639" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3639"><a href="https://twitter.com/nodDFW/lists/n-d-members/members" itemprop="url"><span itemprop="name">STARTUPS</span></a></li>
                            <li id="menu-item-975" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-975"><a title="Sponsors" href="http://www.noddfw.com/members/partners/" itemprop="url"><span itemprop="name">PARTNERS</span></a></li>
                            <li id="menu-item-3641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3641"><a href="https://angel.co/nod-coworking/activity#press" itemprop="url"><span itemprop="name">PRESS</span></a></li>
                        </ul>
                    </nav>
                </div>
            </section>
        </amp-sidebar>
        <script type="application/ld+json">
            {
                "@context": "http:\/\/schema.org",
                "@type": "WebSite",
                "@id": "#website",
                "url": "http:\/\/www.noddfw.com\/",
                "name": "N\u014dD Coworking",
                "potentialAction": {
                    "@type": "SearchAction",
                    "target": "http:\/\/www.noddfw.com\/?s={search_term_string}",
                    "query-input": "required name=search_term_string"
                }
            }
        </script>
        <script type="application/ld+json">
            {
                "@context": "http:\/\/schema.org",
                "@type": "Organization",
                "url": "http:\/\/www.noddfw.com\/",
                "sameAs": ["https:\/\/www.facebook.com\/noddfw", "https:\/\/www.linkedin.com\/company\/north-dallas-coworking", "https:\/\/twitter.com\/noddfw"],
                "@id": "#organization",
                "name": "NoD Coworking",
                "logo": "http:\/\/www.noddfw.com\/wp-content\/uploads\/2016\/04\/NoD-Logo.png"
            }
        </script>
        <amp-analytics type="googleanalytics" id="rbt-ga-1">
            <script type="application/json">
                {
                    "vars": {
                        "account": "UA-49693332-1"
                    },
                    "triggers": {
                        "trackPageview": {
                            "on": "visible",
                            "request": "pageview"
                        }
                    }
                }
            </script>
        </amp-analytics>
        <header class="site-header" itemscope="" itemtype="https://schema.org/WPHeader">
            <div class="wrap">
                <div class="title-area">
                    <p class="site-title" itemprop="headline"><a href="http://www.noddfw.com/">NōD Coworking</a></p>
                </div>
                <div class="widget-area header-widget-area">
                    <!--{/* @notice */}-->
                    <!--{/* The following code was moved to amp-sidebar. */}-->
                    <!-- <section id="nav_menu-2" class="widget widget_nav_menu"><div class="widget-wrap"><nav class="nav-header" itemscope="" itemtype="https://schema.org/SiteNavigationElement"><ul id="menu-main" class="menu genesis-nav-menu"><li id="menu-item-1342" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1342"><a href="http://www.noddfw.com/our-space/" itemprop="url"><span itemprop="name">SPACE</span></a>
<ul class="sub-menu">
	<li id="menu-item-1141" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1141"><a title="Coworking Space" href="http://www.noddfw.com/our-space/coworking-lounge/" itemprop="url"><span itemprop="name">COWORKING</span></a></li>
	<li id="menu-item-1182" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1182"><a title="Dedicated Desks &#038; Offices" href="http://www.noddfw.com/our-space/offices/" itemprop="url"><span itemprop="name">PRIVATE OFFICES</span></a></li>
	<li id="menu-item-1140" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1140"><a title="Meeting &#038; Conference Rooms" href="http://www.noddfw.com/our-space/meeting-and-conference-rooms/" itemprop="url"><span itemprop="name">CONFERENCE ROOMS</span></a></li>
</ul>
</li>
<li id="menu-item-3639" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3639"><a href="https://twitter.com/nodDFW/lists/n-d-members/members" itemprop="url"><span itemprop="name">STARTUPS</span></a></li>
<li id="menu-item-975" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-975"><a title="Sponsors" href="http://www.noddfw.com/members/partners/" itemprop="url"><span itemprop="name">PARTNERS</span></a></li>
<li id="menu-item-3641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3641"><a href="https://angel.co/nod-coworking/activity#press" itemprop="url"><span itemprop="name">PRESS</span></a></li>
</ul></nav></div></section> -->
                    <section id="text-2" class="widget widget_text">
                        <div class="widget-wrap">
                            <div class="textwidget"><a class="button" href="http://www.noddfw.com/membership/">Coworking Membership</a>

                                <a class="button" href="http://www.noddfw.com/our-space/event-spaces/">Room Rental</a></div>
                        </div>
                    </section>
                </div>
            </div>
        </header>
        <div class="site-container">
            <div id="home-top" class="home-top widget-area">
                <div class="wrap">
                    <section id="text-3" class="widget widget_text">
                        <div class="widget-wrap">
                            <div class="textwidget">
                                <p>

                                    <a class="button" href="https://calendly.com/nod_tour" target="_blank">Schedule a Tour</a>
                                </p>

                                <p>
                                    <a class="button" href="https://www.youtube.com/watch?v=6LxU7H71qR0&list=PLmCaX77KMaotkc9RtvHlValjiZODzgQK9&index=1" target="_blank">Watch Our Video</a>
                                </p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="site-inner">
                <div class="content-sidebar-wrap">
                    <main class="content"></main>
                </div>
            </div>
            <div class="footer-widgets">
                <div class="wrap">
                    <div class="widget-area footer-widgets-1 footer-widget-area">
                        <section id="text-4" class="widget widget_text">
                            <div class="widget-wrap">
                                <h4 class="widget-title widgettitle">Contact Us</h4>
                                <div class="textwidget">
                                    <p>
                                        <!--{/* @notice */}-->
                                        <!--{/* Tag font isn't supported in AMP. */}-->
                                        <!-- <font color="#E58C29"> -->
                                        <a href="https://goo.gl/maps/4dDqB" title="Driving directions to NōD Coworking">17290 Preston Rd #300<br>
Dallas, TX 75252</a>
                                        <!-- </font> -->
                                    </p>

                                    <a href="mailto:info@noddfw.com">
info@nodDFW.com</a><br>
                                    <!--{/* @notice */}-->
                                    <!--{/* Tag font isn't supported in AMP. */}-->
                                    <!-- <font color="#E58C29"> -->(972) 590-0225
                                    <!-- </font> -->
                                </div>
                            </div>
                        </section>
                        <section id="simple-social-icons-5" class="widget simple-social-icons">
                            <div class="widget-wrap">
                                <ul class="alignleft">
                                    <li class="ssi-facebook"><a href="https://www.facebook.com/noddfw"><svg role="img" class="social-facebook" aria-labelledby="social-facebook"><title id="social-facebook">Facebook</title><use xlink:href="http://www.noddfw.com/wp-content/plugins/simple-social-icons/symbol-defs.svg#social-facebook"/></svg></a></li>
                                    <li class="ssi-gplus"><a href="https://plus.google.com/105399389006611117165/posts"><svg role="img" class="social-gplus" aria-labelledby="social-gplus"><title id="social-gplus">Google+</title><use xlink:href="http://www.noddfw.com/wp-content/plugins/simple-social-icons/symbol-defs.svg#social-gplus"/></svg></a></li>
                                    <li class="ssi-linkedin"><a href="https://www.linkedin.com/company/north-dallas-coworking"><svg role="img" class="social-linkedin" aria-labelledby="social-linkedin"><title id="social-linkedin">Linkedin</title><use xlink:href="http://www.noddfw.com/wp-content/plugins/simple-social-icons/symbol-defs.svg#social-linkedin"/></svg></a></li>
                                    <li class="ssi-twitter"><a href="https://twitter.com/noddfw"><svg role="img" class="social-twitter" aria-labelledby="social-twitter"><title id="social-twitter">Twitter</title><use xlink:href="http://www.noddfw.com/wp-content/plugins/simple-social-icons/symbol-defs.svg#social-twitter"/></svg></a></li>
                                </ul>
                            </div>
                        </section>
                    </div>
                    <div class="widget-area footer-widgets-2 footer-widget-area">
                        <section id="text-5" class="widget widget_text">
                            <div class="widget-wrap">
                                <div class="textwidget">Join our local <a href="http://www.meetup.com/bigdocc/" title="Open Coffee Club" target="_blank">Open Coffee Club<br></a> Attend <a href="http://www.meetup.com/NorthDallasCoworking" title="NoD Meetup Group" target="_blank">Community Meetups<br></a> Read the <a href="http://www.noddfw.com/news" title="NoD Coworking Blog">Blog<br></a>

                                    <br>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="widget-area footer-widgets-3 footer-widget-area">
                        <section id="dt_twitter_widget-6" class="widget widget_dt_twitter_widget">
                            <div class="widget-wrap">
                                <div class="widget-text dt_twitter_plugin_box">
                                    <h4 class="widget-title widgettitle">Twitter</h4>
                                    <div class="dt-twitter-header"><a href="https://twitter.com/noddfw" class="twitter-follow-button" data-show-count="false" data-show-screen-name="true" data-dnt="true">Follow &#64;noddfw</a></div>
                                    <ul class="dt-twitter rbt-inline-0"></ul>
                                </div>
                            </div>
                        </section>
                        <section id="text-7" class="widget widget_text">
                            <div class="widget-wrap">
                                <div class="textwidget">Featured in:

                                    <a id="dmagazinelogo" target="_blank" href="https://dallasinnovates.com/founders-live-hold-monthly-live-streamed-pitches-nod-coworking/">
                                        <amp-img id="dmagazinelogo_img" src="http://www.dmagazine.com/wp-content/themes/dmagazine/dist/images/dmag-logo_180x164.png" width="50" height="45.6" layout="fixed"></amp-img>
                                    </a>
                                    <br> #LaunchedInDFW
                                    <a id="launchedindfw_a" target="_blank" href="http://launchdfw.com/2016/07/19/nod-coworking-expands-dallas-building/">
                                        <amp-img id="launchedindfw_img" src="http://www.launchedindfw.com/l.php?img=r&check=true" width="50" height="65.2" layout="responsive"></amp-img>
                                    </a>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <footer class="site-footer" itemscope="" itemtype="https://schema.org/WPFooter">
                <div class="wrap">
                    <div class="creds">
                        <p>Copyright &copy; 2018 &middot; nōd Coworking &middot; Site design by <a href="http://www.banyan360.com/" title="banyan360">banyan360</a></p>
                    </div>
                    <p></p>
                </div>
            </footer>
        </div>
        <div id="wprmenu_bar" class="wprmenu_bar  left" on="tap:rbt-sidebar.toggle" role="button" tabindex="-1">
            <div class="hamburger hamburger--slider">
                <span class="hamburger-box">
    						<span class="hamburger-inner"></span>
                </span>
            </div>
            <div class="menu_title">
                MENU </div>
        </div>

        <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left  " id="mg-wprm-wrap">
            <ul id="wprmenu_menu_ul">
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1342"><a href="http://www.noddfw.com/our-space/" itemprop="url">SPACE</a>
                    <ul class="sub-menu">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1141"><a title="Coworking Space" href="http://www.noddfw.com/our-space/coworking-lounge/" itemprop="url">COWORKING</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1182"><a title="Dedicated Desks &#038; Offices" href="http://www.noddfw.com/our-space/offices/" itemprop="url">PRIVATE OFFICES</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1140"><a title="Meeting &#038; Conference Rooms" href="http://www.noddfw.com/our-space/meeting-and-conference-rooms/" itemprop="url">CONFERENCE ROOMS</a></li>
                    </ul>
                </li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3639"><a href="https://twitter.com/nodDFW/lists/n-d-members/members" itemprop="url">STARTUPS</a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-975"><a title="Sponsors" href="http://www.noddfw.com/members/partners/" itemprop="url">PARTNERS</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3641"><a href="https://angel.co/nod-coworking/activity#press" itemprop="url">PRESS</a></li>

            </ul>
        </div>

        <div id="fb-root"></div>

    </body>
</html>