<!doctype html>
<html amp lang="en">
<head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <title>ROBOAMP - DEMO</title>
    <link rel="canonical" href="http://example.ampproject.org/article-metadata.html">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "website",
        "headline": "Interlock Partners",

      }
    </script>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <link rel="stylesheet" type="text/css" href="demo/interlock/css/bootstrap.min.css">


    <link rel="stylesheet" type="text/css" href="demo/interlock/css/style.css">
    <link rel="stylesheet" type="text/css" href="demo/interlock/css/responsive.css">

</head>
<body>
<div class="header-top fix">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <div class="logo">
                    <a href="#"><img src="demo/interlock/images/logo.png" alt="logo"></a>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-7 col-xs-6">
                <div class="logo-text">
                    <a href="https://secure.straitcapital.com/interlock/login.aspx">LP LOGIN</a>
                </div>
            </div>

        </div>
    </div>
</div>


<section class="slider-area bg">
    <div class="slider-text">
        <h1>Venture Collaboration.</h1>
        <p>PEOPLE • IDEAS • OUTCOMES</p>
    </div>
</section>



<section class="content-block">

    <div class="sinle-item">
        <div class="container">
            <div class="row">
                <div class="col-md-2  col-xs-12">
                    <img class="img-align" src="demo/interlock/images/right.png" alt="">
                </div>
                <div class="col-md-7 col-xs-12">
                    <div class="title">
                        <h2>COLLABORATION</h2>
                        <p>As implied by our name, we believe in the dynamic collaboration of people, ideas and resources with a shared vision of building great businesses. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sinle-item">
        <div class="container">
            <div class="row">
                <div class="col-md-2  col-xs-12">
                    <img class="img-align" src="demo/interlock/images/bottom.png" alt="">
                </div>
                <div class="col-md-7 col-xs-12">
                    <div class="title">
                        <h2>PEOPLE</h2>
                        <p>We may be highly selective of who we back, but that's only because we’re building real partnerships, not portfolios. That’s why we back innovative founders who are driven, versatile and creative.  Your energy and big ideas supported by our experience and resources can result in an exciting team. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sinle-item">
        <div class="container">
            <div class="row">
                <div class="col-md-2  col-xs-12">
                    <img class="img-align" src="demo/interlock/images/left.png" alt="">
                </div>
                <div class="col-md-7 col-xs-12">
                    <div class="title">
                        <h2>IDEAS</h2>
                        <p>We are interested in early stage (Series A) technology companies committed to solving significant business problems. There is a compelling opportunity to drive transformational change in the legacy enterprise technology stack fueled by artificial intelligence, machine learning, cloud platforms and other innovative solutions. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sinle-item">
        <div class="container">
            <div class="row">
                <div class="col-md-2  col-xs-12">
                    <img class="img-align" src="demo/interlock/images/top.png" alt="">
                </div>
                <div class="col-md-7 col-xs-12">
                    <div class="title">
                        <h2>RESOURCES</h2>
                        <p>Our partners and principals bring years of experience as successful entrepreneurs, executives, operators, investors and advisors.  We are complemented by very accomplished technology and top executives serving on our advisory boards and as a resource to the entrepreneurs and companies we partner with.  And yes, we have capital to invest, but our collaborative approach is intended to help you succeed. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="sinle-item">
        <div class="container">
            <div class="row">
                <div class="col-md-2  col-xs-12">
                    <img class="img-align" src="demo/interlock/images/right.png" alt="">
                </div>
                <div class="col-md-7 col-xs-12">
                    <div class="title">
                        <h2>GROWTH</h2>
                        <p>Entrepreneurs need more than capital to build a successful business. Real potential surfaces when entrepreneurs and experienced leaders collaborate closely to turn ideas into reality. From vision to strategy to operational execution, that’s where we excel. And that’s why we define VC as Venture Collaboration.  </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="empty empty1"></section>

<section class="team">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="team-content">
                    <amp-img height="233px" wight="233px" src="demo/interlock/images/1.jpg" alt="" >
                    <div class="team-text">
                        <h2 class="name">Jeff Williams</h2>
                        <h3>Partner</h3>
                        <p> <span> <a href="https://angel.co/jeff-williams-6">Angel.co</a></span> &#46; <span><a href="https://www.linkedin.com/in/jeff-williams-6899553/">LinkedIn</a></span> </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="team-content">
                    <amp-img height="233px" wight="233px" src="demo/interlock/images/2.jpg" alt="">
                    <div class="team-text">
                        <h2 class="name">Jason Story</h2>
                        <h3>Partner</h3>
                        <p> <span> <a href="https://angel.co/jasonstory">Angel.co</a></span> &#46; <span><a href="https://www.linkedin.com/in/jasonstory1/">LinkedIn</a></span> </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="team-content">
                    <amp-img height="233px" wight="233px"  src="demo/interlock/images/3.jpg" alt="">
                    <div class="team-text">
                        <h2 class="name">Harry Hawks</h2>
                        <h3>Partner</h3>
                        <p> <span> <a href="https://angel.co/harry-hawks">Angel.co</a></span> &#46; <span><a href="https://www.linkedin.com/in/harry-hawks-580a837/">LinkedIn</a></span> </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="team-content">
                    <amp-img height="233px" wight="233px" src="demo/interlock/images/4.jpg" alt="">
                    <div class="team-text">
                        <h2 class="name">Inobat Igamberdieva</h2>
                        <h3>Principal</h3>
                        <p> <span> <a href="https://angel.co/inobat-igamberdieva">Angel.co</a></span> &#46; <span><a href="https://www.linkedin.com/in/inobatigamberdieva/">LinkedIn</a></span> </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="empty empty1"></section>

<section id="office-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="office-title">
                    the offices
                </div>
            </div>
        </div>
    </div>

    <div class="container fix">
        <div class="row">
            <div class="col-md-6">
                <div class="top">
                    <div class="containers fix">
                        <div class="col-md-5 col-xs-5">DALLAS</div>
                        <div class="col-md-7 col-xs-7">
                            <div class="right-text">
                                <p>2109 Commerce Street</p>
                                <p>Dallas, TX 75201</p>
                                <a href="https://interlock.vc/dallas-map">Where to Park</a>
                            </div>
                        </div>
                    </div>
                </div>
                <img calss="sec" src="demo/interlock/images/sec1.jpg" alt="">
            </div>

            <div class="col-md-6">
                <div class="top">
                    <div class="containers fix">
                        <div class="col-md-5 col-xs-5">NEW YORK</div>
                        <div class="col-md-7 col-xs-7">
                            <div class="right-text">
                                <p>54 Thompson Street, 4th Floor </p>
                                <p>New York, NY 10012</p>
                                <a href="https://interlock.vc/dallas-map">Where to Park</a>
                            </div>
                        </div>
                    </div>
                </div>
                <img calss="sec" src="demo/interlock/images/sec2.jpg" alt="">
            </div>
        </div>
    </div>
</section>

<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <a href="">Info@interlock.vc</a>
            </div>
            <div class="col-md-6 col-sm-6 text-right">
                <img src="demo/interlock/images/footer.png" alt="">
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12 new">© 2017 Interlock Partners. All rights reserved.</div>
        </div>
    </div>
</section>

</body>
</body>
</html>