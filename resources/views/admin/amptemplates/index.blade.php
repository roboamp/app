@extends('admin.list')


@section('list')
<div class="clearfix">
    <a href="{{ route('admin.amptemplates.create') }}" class="btn btn-primary float-right">Create Template</a>
</div>
<div class="table-responsive mt-3">
    <table class="table table-striped" id="templates-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Component</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($templates as $template)
            <tr id="template-row-{{$template->id}}">
                <td>
                    {{ $template->name }}
                </td>
                <td>
                    {{ $template->component->name }}
                </td>
                <td>
                    <form id="edit-template-{{$template->id}}" method="POST" action="{{route('admin.amptemplates.edit')}}">
                        @csrf
                        <input type="hidden" value="{{$template->id}}" name="template" id="template">
                    </form>
                    <button type="button" class="btn btn-success btn-sm code" data-target="{{ $template->id }}" data-component="{{$template->component_id}}" data-template_id="{{$template->id}}" data-toggle="tooltip" data-placement="bottom" title="Generate Code"><i class="ti-shortcode"></i></button>
                    <button type="button" class="btn btn-info btn-sm edit" data-target="{{ $template->id }}" data-toggle="tooltip" data-placement="bottom" title="Edit Template"onclick="document.getElementById('edit-template-{{$template->id}}').submit();"><i class="ti-pencil-alt"></i></button>
                    <button type="button" class="btn btn-danger btn-sm destroy" data-target="{{ $template->id }}" data-toggle="tooltip" data-placement="bottom" title="Delete Template"><i class="ti-trash"></i></button>
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
    
</div>
<!-- Modal -->
<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Template</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            <form action="{{route('admin.amptemplates.destroy')}}" method="POST" id="destroyTemplate">
            @csrf
            @method("DELETE")
            <input type="hidden" id="deleteTemplateId" name="template" value="">
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary"  id="confirm-destroy">Understood, continue.</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();

        @if(count($templates) <= 10 )
        $('#templates-table').DataTable({
            searching: false,
            ordering:  false,
            paging: false
        });
        @else
        $('#templates-table').DataTable();
        @endif

        $('.alert').alert();

        $(".code").on("click", function(){
            $('#template-code').remove();
            let component = $(this).data("component");
            let template = $(this).data("template_id");
            let csrf_token = $('meta[name="csrf-token"]').attr('content');
            $.post('{{route("admin.amptemplates.code.index")}}', { template: template, component: component, _token: csrf_token }, function(data){
                $('#template-row-'+template).after('<tr id="template-code"><td colspan="4"><div class="alert alert-light" role="alert"><button type="button" class="close close-attr-alert" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h6>Template Code</h6><pre><code id="code-container"></code></pre></div></td></tr>');
                $('#code-container').text(data.code);
            })
        });

        $(document).on('click', '.close-attr-alert', function () {
            $('#template-code').remove();
        });

        $(document).on("click", ".destroy", function(){
            let target = $(this).data("target");
            $("#deleteTemplateId").val(target);
            $('#destroyModal').modal({
                keyboard: false
            })
        });

        $(document).on("click", "#confirm-destroy", destroy_template);

        function destroy_template(){
            let template = $("#deleteTemplateId").val();
            let csrf_token = $('meta[name="csrf-token"]').attr('content');
            let method = $('input[name="_method"]').val();
            $('#destroyModal').modal("hide");
            $.post( "{{route('admin.amptemplates.destroy')}}", { template: template, _token: csrf_token, _method: method }, function( data ) {
                if(data.success == true){
                    $('#templates-table').DataTable().row("#template-row-"+data.target).remove().draw("false");
                    $.toast({
                        heading: 'Template Deleted',
                        text: data.message,
                        position: 'top-right',
                        loaderBg:'#00c292',
                        icon: 'success',
                        hideAfter: 3000,
                        stack: false, 
                    });
                }else{
                    $.toast({
                        heading: 'Template Deleted',
                        text: data.message,
                        position: 'top-right',
                        loaderBg:'#e46a76',
                        icon: 'error',
                        hideAfter: 3000,
                        stack: false, 
                    });
                }
            });
        }
    </script>
@endsection