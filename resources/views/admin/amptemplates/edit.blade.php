@extends('admin.list')


@section('list')
<form method="POST" action="{{route('admin.amptemplates.update')}}" id="template-edit-form">
    @csrf
    <input type="hidden" name="template" id="template" value="{{$template->id}}"> 
    <div class="form-row mt-1">
        <div class="col-6">
            <label>Component</label>
            <h3 class="pl-3">{{$template->component->name}}</h3>
            <input type="hidden" name="component" id="component" value="{{$template->component->id}}">
        </div>
        <div class="col-6">
            <label>Name</label>
            <input type="text" class="form-control" placeholder="Name" name="name" id="name" required value="{{$template->name}}">
        </div>
    </div>
    <div class="form-row mt-1">
        <div class="col-12">
            <label>Description</label>
            <textarea class="form-control" placeholder="description" name="description" id="description">{{$template->description}}</textarea>
        </div>
    </div>
    <div class="form-row mt-3" id="update-component">
        <div class="col-12">
            <div class="alert alert-info alert-dismissible d-none" role="alert" id="alert-upgrade-component">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                There are a new attributes for this component. <a type="button" class="btn btn-primary" id="btn-upgrade-component">Clike here</a> to upgrade this template to use them.
            </div>
        </div>
    </div>
    <div class="form-row mt-3">
        <div class="col-12">
            <h4>Attributes</h4>
        </div>
    </div>
    <div class="form-row mt-1">
        <div class="col-3">
            <label>Name</label>
        </div>
        <div class="col-6">
            <label>Description</label>
        </div>
        <div class="col-3">
            <label>Value</label>
        </div>
    </div>
    <div id="attributes">

    </div>
    <div class="form-row mt-3">
        <div class="col-6 text-left pl-3"></div>
        <div class="col-6 text-right pr-3">
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
    </div>
</form>
<div class="alert alert-light" role="alert">
    <h6>Template Code</h6>
    <pre><code id="code-container"></code></pre>
</div>
<input type="hidden" id="inactive_attributes" value="">
@endsection



@section('scripts')
<script src="{{asset('js/dashboard/footable.min.js')}}"></script>
<script src="{{asset('dashboard_assets/js/pages/footable-init.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/moment/min/moment.min.js')}}"></script>
<script>


    $(document).ready(function(){
        searchAttributes();

        generating_code();

        $('.alert').alert();

        $(document).on("focus", ".code-attr", function(){
            let old = $(this).val();
            $(this).data("old", old);
        });

        $(document).on("change", ".code-attr", function(){
            let attr_name = $(this).data("name");
            let old_attr = attr_name+'="'+($(this).data("old"))+'"';
            let new_attr = attr_name+'="'+($(this).val())+'"';
            let code = $('#code-container').text();
            let new_code = code.replace(old_attr, new_attr);
            $('#code-container').text(new_code);
        });

        $("#btn-upgrade-component").on("click", function(){
            let template = $("#template").val();
            let attributes = $("#inactive_attributes").val();
            attributes = attributes.split(",");
            let csrf_token = $('meta[name="csrf-token"]').attr('content');
            $.post('{{route("admin.amptemplates.attributes.upgrade")}}', {template: template, attributes_inactive: attributes, _token: csrf_token }, function(data){
                if(data.success){
                    location.reload();
                }
            });
        });

        function generating_code(){
            let component = $("#component").val();
            let template = $("#template").val();
            let csrf_token = $('meta[name="csrf-token"]').attr('content');
            $.post('{{route("admin.amptemplates.code.index")}}', { template: template, component: component, _token: csrf_token }, function(data){
                $('#code-container').text(data.code);
            })
        }

        function searchAttributes(){
            let component = $('#component').val();
            let token = $('input[name ="_token"]').val();
            let template = $('#template').val();
            $.post('{{route('admin.amptemplates.attributes.index')}}', {component: component, _token: token, template: template }, function(data){
                $("#attributes").empty();
                let inactive_attributes = [];
                let active_attributes = [];
                jQuery.each(data.attributes, function(){
                    let id = this.id;
                    let name = this.name;
                    let description = this.description;
                    let value = JSON.parse(this.value);
                    let type = this.type_id;
                    let last_value;
                    let component = this.component_id;
                    if(type == 2){
                        last_value = value.default
                    }else{
                        last_value = value;
                    }
                    if(data.has_attr){
                        jQuery.each(data.templates_attr, function(){
                            if(this.attribute_id == id){
                                active_attributes.push(id);
                                let attr_id = this.id;
                                last_value = this.value;
                                let attr ='<div class="form-row mt-2">';
                                attr +='<div class="col-3">';
                                attr +='<label>'+ name +'</label>';
                                attr +='</div>';
                                attr +='<div class="col-6">';
                                attr +='<label>'+ description +'</label>';
                                attr +='</div>';
                                attr +='<div class="col-3">';
                                if(type == 1){
                                    attr +='<select name="value[]" id="value[]" class="form-control code-attr" data-old="" data-name="'+name+'">';
                                    jQuery.each(value.options, function(){
                                        if(this == last_value){
                                            attr += "<option value='"+ this + "' selected>"+ this + "</option>";    
                                        }else{
                                            attr += "<option value='"+ this + "' >"+ this + "</option>";
                                        }
                                    });
                                    attr += "</select>";
                                }else if(type == 2){
                                    attr += "<input type='text' name='value[]' id='value[]' value='" + last_value + "' class='form-control code-attr' data-old='' data-name='"+name+"'>";
                                }
                                attr +="<input type='hidden' name='attr_id[]' id='attr_id[]' value='" + attr_id + "'>";
                                attr +='</div>';
                                attr +='</div>';
                                $("#attributes").append(attr);
                            }else{
                                inactive_attributes.push(id);  
                            }
                        });
                    }
                });
                check_new_attributes(active_attributes, inactive_attributes)
            });
        }


        function check_new_attributes(actives, inactives){
            //eliminate duplicity in inactive attributes
            let in_attr = [...new Set(inactives)];
            //check both arrays ways to seach for uniques values
            let unique1 = in_attr.filter((o) => actives.indexOf(o) === -1);
            let unique2 = actives.filter((o) => in_attr.indexOf(o) === -1);
            //concatenate both arrays in one and only keep uique values
            let uniques = unique1.concat(unique2);

            if(uniques.length > 0){
                $("#inactive_attributes").val(uniques);
                $("#alert-upgrade-component").removeClass('d-none');
            }
        }


    });
</script>
@endsection