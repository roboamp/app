@extends('admin.list')


@section('list')
<form method="POST" action="{{route('admin.amptemplates.store')}}">
    @csrf
    <div class="form-row mt-1">
        <div class="col-6">
            <label>Component</label>
            <select name="component" id="component" class="form-control" required>
            @foreach($components as $component)
                <option value="{{$component->id}}">{{$component->name}}</option>
            @endforeach
            </select>
        </div>
        <div class="col-6">
            <label>Name</label>
            <input type="text" class="form-control" placeholder="Name" name="name" id="name" required>
        </div>
    </div>
    <div class="form-row mt-1">
        <div class="col-12">
            <label>Description</label>
            <textarea class="form-control" placeholder="description" name="description" id="description"></textarea>
        </div>
    </div>
    <div class="form-row mt-3">
        <div class="col-12">
            <h4>Attributes</h4>
        </div>
    </div>
    <div class="form-row mt-1">
        <div class="col-3">
            <label>Name</label>
        </div>
        <div class="col-6">
            <label>Description</label>
        </div>
        <div class="col-3">
            <label>Value</label>
        </div>
    </div>
    <div id="attributes">

    </div>
    <div class="form-row mt-3">
        <div class="col-12 text-center pr-3">
            <button type="submit" class="btn btn-primary">Create Template</button>
        </div>
    </div>
</form>
<!-- Modal -->
<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Feature</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            <form action="features/destroy" method="POST" id="destroyFeature">
            @csrf
            <input type="hidden" id="deleteFeatureId" name="feature" value="">
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="document.getElementById('destroyFeature').submit();">Understood, continue.</button>
        </div>
      </div>
    </div>
  </div>
@endsection



@section('scripts')
<script src="{{asset('js/dashboard/footable.min.js')}}"></script>
<script src="{{asset('dashboard_assets/js/pages/footable-init.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/moment/min/moment.min.js')}}"></script>
<script>

    $(document).ready(function(){
        searchAttributes();
        $('#component').on('change', searchAttributes);

        function searchAttributes(){
            let component = $('#component').val();
            let token = $('input[name ="_token"]').val();
            $.post('{{route('admin.amptemplates.attributes.index')}}', {component: component, _token: token }, function(data){
                $("#attributes").empty();
                jQuery.each(data.attributes, function(){
                    let id = this.id;
                    let name = this.name;
                    let description = this.description;
                    let value = JSON.parse(this.value);
                    let type = this.type_id;
                    let attr ='<div class="form-row mt-2">';
                    attr +='<div class="col-3">';
                    attr +='<label>'+ name +'</label>';
                    attr +='</div>';
                    attr +='<div class="col-6">';
                    attr +='<label>'+ description +'</label>';
                    attr +='</div>';
                    attr +='<div class="col-3">';
                    if(type == 1){
                        attr +='<select name="value[]" id="value[]" class="form-control">';
                        jQuery.each(value.options, function(){
                            attr += "<option value='"+ this + "'>"+ this + "</option>";
                        });
                        attr += "</select>";
                    }else if(type == 2){
                        attr += "<input type='text' name='value[]' id='value[]' value='" + value.default + "' class='form-control'>";
                    }
                    attr +="<input type='hidden' name='attr_id[]' id='attr_id[]' value='" + id + "'>";
                    attr +='</div>';
                    attr +='</div>';
                    $("#attributes").append(attr);
                });
            });
        }
    });
</script>
@endsection