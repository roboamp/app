@extends('admin.list')


@section('list')
<div class="form-row">
    <div class="col-6">
        <label>Template</label>
        <input type="text" class="form-control" placeholder="City" value="{{$template->name}}" disabled>
    </div>
    <div class="col-6">
        <label>Component</label>
        <input type="text" class="form-control" placeholder="City" value="{{$component->name}}" disabled>
    </div>
</div>

<pre>
    <code class="html">
        {{$code}}
    </code>
</pre>

@endsection

@section ('form_scripts_include')
    <script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('dashboard_assets/js/pages/templates/create.js')}}"></script>
@endsection


@section('scripts')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/highlight.min.js"></script>
<!-- and it's easy to individually load additional languages -->
<script charset="UTF-8" src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/10.1.2/languages/go.min.js"></script>
<script src="{{asset('js/dashboard/footable.min.js')}}"></script>
<script src="{{asset('dashboard_assets/js/pages/footable-init.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/moment/min/moment.min.js')}}"></script>
<script>hljs.initHighlightingOnLoad();</script>
<script>
    $('*[data-href]').on("click",function(){
        window.location = $(this).data('href');
        return false;
    });
    $("td > a").on("click",function(e){
        e.stopPropagation();
    });

</script>
@endsection