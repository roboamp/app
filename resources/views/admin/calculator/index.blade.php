@extends('admin.list')

@section('css_list')
    <link href="{{asset('dashboard_assets/dist/css/pages/bootstrap-switch.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.min.css')}}" rel="stylesheet">
    <style>
        .bootstrap-switch.bootstrap-switch-animate .bootstrap-switch-container {
            transition: margin-left .5s;
        }
        .tooltip-inner{min-width: 400px;}
    </style>
@endsection

@section('list')
<form method="POST" action="{{route('admin.calculator.downloadpdf')}}">
    @csrf
    <div class="form-row mb-3">
        <div class="col-3">
            <label>Customer's Name</label>
        </div>
        <div class="col-9 text-center">
            <input class="form-control" type="text" name="customer" id="customer" value="">
        </div>
    </div>
    @foreach ($features as $feature)
    <div class="form-row" data-toggle="tooltip" id="tooltip" data-placement="top" title="{{$feature->description}}" backup_title="{{$feature->description}}" data-trigger="hover">
        <div class="col-6 tooltip-container">
            <label>{{$feature->name}}</label>
        </div>
        <div class="col-4">
            <p class="text-center">{{$feature->price}}</p>
        </div>
        <div class="col-2 text-center">
            <input class="form-check-input feature" type="checkbox"  data-toggle="switch" data-on-color="primary" name="features[]" id="{{$feature->id}}" value="{{$feature->id}}" price="{{$feature->price}}">
        </div>
    </div>
    @endforeach
    <br>
    <div class="form-row">
        <div class="col-10">
            <label><b>Total</b></label>
        </div>
        <div class="col-2">
            <input class="form-control" type="text" name="total" id="total" readonly>
        </div>
    </div>
    <br>
    <div class="form-row mt-3">
        <div class="col-8">
            <label></label>
        </div>
        <div class="col-2">
            <input type="checkbox" name="add_description" id="add_description">
        </div>
        <div class="col-2">
            <button type="submit" class="btn btn-primary" id="downloadpdf">Download PDF</button>
        </div>
        @if(!$hide_tooltip_buttons ?? '')
        <div class="col-2">
            <button type="button"  id="disable_tooltip">Disable Tooltip</button>
        </div>
        <div class="col-2">
            <button type="button"  id="enable_tooltip">Reactivate Tooltip</button>
        </div>
        @endif
    </div>
</form>
@endsection

@section ('list_scripts_include')
    <script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/moment/min/moment.min.js')}}"></script>
@endsection


@section('list_script')

    $(function () {
        let $tooltip=$('[data-toggle="tooltip"]');
        $tooltip.tooltip();
        $('[data-toggle="switch"]').bootstrapSwitch();
        $('[data-toggle="switch"]').on('switchChange.bootstrapSwitch', function () {
            var total = 0;
            $('input:checkbox:checked').each(function(){
                total += isNaN(parseInt($(this).attr("price"))) ? 0 : parseInt($(this).attr("price"));
            });
            $("#total").val(total);
        });
    });

    function remove_tooltip(){
       $('[data-toggle="tooltip"]').removeAttr('data-original-title');
    }
    function reactivate_tooltip(){
        $('[data-toggle="tooltip"]').attr("data-original-title",function(e,v){

            let obj=$('[id="tooltip"]')[e];
            return $(obj).attr('backup_title');

        });
    };

    $('#disable_tooltip').on("click",function(){
        remove_tooltip();
    });
    $('#enable_tooltip').on("click",function(){
        reactivate_tooltip();
    });

    $('*[data-href]').on("click",function(){
    window.location = $(this).data('href');
    return false;
    });
    $("td > a").on("click",function(e){
        e.stopPropagation();
    });





@endsection