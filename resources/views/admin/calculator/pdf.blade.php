<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reports</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>.description{font-size:12px;}</style>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <body>
        <h1>{{ $customer }}</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="25%">Name</th>
                    <th width="60%">@if($description == "on")Description @endif</th>
                    <th width="15%" class="text-right">Price</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($features as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td class="description">@if($description == "on"){{ $item->description }}@endif</td>
                    <td class="text-right">{{ $item->price }}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2" class="text-right text-bold">TOTAL</td>
                    <td class="text-right">{{ $total }}.00</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>