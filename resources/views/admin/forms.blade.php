@extends('admin.dashboard')

@section ('css')
    <link href="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.min.css')}}" rel="stylesheet">
    @yield('css_form')
@endsection
@section('content')

                <div class="row">
                    @center_form($form_centered)
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">{{$form_title??''}}</h4>
                                <form class="needs-validation" novalidate id="uri" method="{{$form_method??"POST"}}" action="{{$form_action??"NULL"}}">
                                    @csrf
                                    @yield('form_fields')
                                    <div style="padding-top:20px" class="text-center">
                                        <button class="btn btn-primary" type="submit">{{$form_submit_button_title??'Submit Form'}}</button>
                                    </div>
                                </form>



                            </div>
                        </div>
                    @endcenter_form
                </div>
                @yield('new_row')


@endsection

@section ('scripts')

    <script src="{{asset('js/dashboard/bootstrapValidator.min.js')}}"></script>

    @yield('form_scripts_include')

    <script>
        (function() {

            $res=$('#uri').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    txt_url: {
                        validators: {
                            uri: {
                                message: 'The website address is not valid'

                            }
                        }
                    }
                }
            });

           'use strict';
            window.addEventListener('load', function() {

                @yield ('form_script')

                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();


    </script>
@endsection