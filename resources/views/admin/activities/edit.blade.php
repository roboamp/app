@extends('admin.forms')

@section('css_form')
    
@endsection


@section('form_fields')
<div class="form-row">
    <div class="col-6">
        <label>Name</label>
        <input type="text" class="form-control" placeholder="Name" name="name" id="name" value="{{$activity->name}}">
        <input type="hidden" name="id" id="id" value="{{$activity->id}}">
    </div>
    <div class="col-6">
        <label>Label</label>
        <input type="text" class="form-control" placeholder="Label" name="label" id="label" value="{{$activity->label}}">
    </div>                                                                                                                          
</div>
<div class="form-row">
    <div class="col-12">
        <label>Command</label>
        <input type="text" class="form-control" placeholder="Command" name="command" id="command" value="{{$activity->command}}">
    </div>                                                                                                                         
</div>
<div class="form-row">
    <div class="col-12">
        <label>Description</label>
        <textarea class="form-control" placeholder="Description" name="description" id="description">{{$activity->description}}</textarea>
    </div>                                                                                                                         
</div>
@endsection



@section('new_row')

@endsection


@section ('form_scripts_include')

@endsection



@section('form_script')
    
@endsection