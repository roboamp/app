@extends('admin.forms')

@section('css_form')
    
@endsection


@section('form_fields')
<div class="form-row">
    <div class="col-6">
        <label>Name</label>
        <input type="text" class="form-control" placeholder="Name" name="name" id="name">
    </div>
    <div class="col-6">
        <label>Label</label>
        <input type="text" class="form-control" placeholder="Label" name="label" id="label">
    </div>                                                                                                                          
</div>
<div class="form-row">
    <div class="col-12">
        <label>Command</label>
        <input type="text" class="form-control" placeholder="Command" name="command" id="command">
    </div>                                                                                                                         
</div>
<div class="form-row">
    <div class="col-12">
        <label>Description</label>
        <textarea class="form-control" placeholder="Description" name="description" id="description"></textarea>
    </div>                                                                                                                         
</div>
@endsection



@section('new_row')

@endsection


@section ('form_scripts_include')

@endsection



@section('form_script')
    
@endsection