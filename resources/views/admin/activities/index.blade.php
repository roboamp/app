@extends('admin.list')


@section('list')
<div class="clearfix">
    <a href="{{ route('admin.activities.create') }}" class="btn btn-primary float-right">Create Task</a>
</div>

<div class="table-responsive mt-3">
    <table class="table table-striped" id="activities-table">
        <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">Label</th>
                <th width="20%">Command</th>
                <th width="30%">Description</th>
                <th width="10%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($activities as $activity)
            <tr id="activity-row-{{ $activity->id }}">
                <td>
                    <span class="name-{{ $activity->id }}">{{ $activity->name }}</span>
                    <input type="text" class="form-control d-none" value="{{ $activity->name }}" name="name-{{ $activity->id }}" id="name-{{ $activity->id }}" data-old="{{ $activity->name }}">
                </td>
                <td>
                    <span class="label-{{ $activity->id }} text-break">{{ $activity->label }}</span>
                    <input type="text" class="form-control d-none" value="{{ $activity->label }}" name="label-{{ $activity->id }}" id="label-{{ $activity->id }}" data-old="{{ $activity->label }}">
                </td>
                <td>
                    <span class="command-{{ $activity->id }}">{{ $activity->command }}</span>
                    <input type="text" class="form-control d-none" value="{{ $activity->command }}" name="command-{{ $activity->id }}" id="command-{{ $activity->id }}" data-old="{{ $activity->command }}">
                </td>
                <td>
                    <span class="description-{{ $activity->id }}">{{ $activity->description }}</span>
                    <input type="text" class="form-control d-none" value="{{ $activity->description }}" name="description-{{ $activity->id }}" id="description-{{ $activity->id }}" data-old="{{ $activity->description }}">
                </td>
                <td>
                    <div class="actions-{{ $activity->id }}">
                        <form id="edit-activity-{{$activity->id}}" method="POST" action="{{route('admin.activities.edit')}}">
                            @csrf
                            <input type="hidden" value="{{$activity->id}}" name="id" id="id">
                        </form>
                        <button type="button" class="btn btn-info btn-sm edit" data-target="{{ $activity->id }}" onclick="document.getElementById('edit-activity-{{$activity->id}}').submit();"><i class="ti-pencil-alt"></i></button>
                        <button type="button" class="btn btn-danger btn-sm destroy" data-target="{{ $activity->id }}"><i class="ti-trash"></i></button>
                    </div>
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>


<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            @csrf
            @method("DELETE")
            <input type="hidden" id="deleteActivityId" name="activity" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="confirm-destroy">Understood, continue.</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();

    @if(count($activities) <= 10 )
    $('#activities-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#activities-table').DataTable();
    @endif

    $('.alert').alert();

    $(document).on("click", ".destroy", function(){
        let target = $(this).data("target");
        $("#deleteActivityId").val(target);
        $('#destroyModal').modal({
            keyboard: false
        })
    });

    
    $(document).on("click", "#confirm-destroy", destroy_activity);

    function destroy_activity(){
        let activity = $("#deleteActivityId").val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        let method = $('input[name="_method"]').val();
        $('#destroyModal').modal("hide");
        $.post( "{{route('admin.activities.destroy')}}", { activity: activity, _token: csrf_token, _method: method }, function( data ) {
            if(data.success == true){
                $('#activities-table').DataTable().row("#activity-row-"+data.target).remove().draw("false");
                $.toast({
                    heading: 'Task Deleted',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#00c292',
                    icon: 'success',
                    hideAfter: 3000,
                    stack: false, 
                });
            }else{
                $.toast({
                    heading: 'Task Deleted',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#e46a76',
                    icon: 'error',
                    hideAfter: 3000,
                    stack: false, 
                });
            }
        });
    }
    </script>
@endsection
