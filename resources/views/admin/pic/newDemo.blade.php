<!DOCTYPE html>
<!-- saved from url=(0039)http://mortgage.darkhorseanalytics.com/ -->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Performance Impact Calculator</title>
    <link rel="stylesheet" type="text/css" charset="utf-8"
          href="{{asset('performance_impact_calculator/stylesheet.css')}}">
    <meta id="viewport" name="viewport" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PIC</title>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <script>
        (function (doc) {
            var viewport = document.getElementById('viewport');
            if (screen.width < 768) {
                doc.getElementById("viewport").setAttribute("content", "width=420,maximum-scale=1.0");
            } else {
                doc.getElementById("viewport").setAttribute("content", "");
            }
        }(document));
    </script>

    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('performance_impact_calculator/mort.css')}}">
    <link rel="shortcut icon" href="http://mortgage.darkhorseanalytics.com/favicon.ico">

    <meta name="google" value="notranslate">
    <style>
        .iris {
            width: 400px;

            margin-left: 3px;
        }

        canvas {
            border: 1px dotted #93cddd;
        }

        .chart-container {
            position: relative;
            top: 30px;
            margin: auto 55%;
            height: 50vh;
            width: 35vw;
        }

    </style>
</head>
<body>


<div id="interact">


    <div id="headerDiv"><span class="header">A BETTER MORTGAGE AND LOAN CALCULATOR</span></div>
    <h1 id="titlePay" class="title">Performance Impact Calculator</h1>

    <div id="first">
        <div id="ctrlPrin" class="ctrl">
            <div class="label">Average Monthly Visitors:</div>
            <input id="average_monthly_visitors" class="slideRule" name="average_monthly_visitors" value="250000">
            <div class="label"></div>
            <div class="slider" id="intRateSlide">
                <div class="slideRule"></div>
            </div>


        </div>


        <div id="ctrlInt" class="ctrl">
            <div class="label">Conversion Rate:</div>
            <input id="conversion_rate" type="text" class="js-range-slider" name="conversion_rate" value="15"/>
            <div class="label"></div>
            <div class="slider" id="intRateSlide">
                <div class="slideRule"></div>
            </div>
        </div>
        <div id="ctrlTerm" class="ctrl">
            <div class="label">Average Order Value:</div>
            <input id="average_order_value" type="text" class="js-range-slider" name="average_order_value" value="100"/>
            <div class="label"></div>
            <div class="slider" id="intRateSlide">
                <div class="slideRule"></div>
            </div>

        </div>

        <div id="ctrlTerm" class="ctrl">
            <div class="label">Loading Time Speed:</div>
            <input id="loading_time_speed" type="text" class="js-range-slider" name="loading_time_speed" value="4"/>
            <div class="label"></div>
            <div class="slider" id="intRateSlide">
                <div class="slideRule"></div>
            </div>

        </div>

        <div id="ctrlTerm" class="ctrl">
            <div class="label">Potential Time Speed:</div>
            <input id="potential_time_speed" type="text" class="js-range-slider" name="potential_time_speed" value=""/>
            <div class="label"></div>
            <div class="slider" id="intRateSlide">
                <div class="slideRule"></div>
            </div>


        </div>

        <!--<div class="smallLink">A list of current Canadian interest rates can be found <a href="http://www.fiscalagents.com/rates/mor_clo_sort.shtml" target="_blank">here</a><br/>
          The rest of the world will have to rely on <a href="https://www.google.ca/search?q=mortgage+rates" target="_blank">google</a>
        </div>-->

    </div>

    <div id="second">
        <div id="outPrin" class="ctrl" style="display: none;">

        </div>
        <div id="outPay" class="ctrl">
            <div class="displayLabel">my ROI will be:</div>
            <div id="displayPay" class="result">$<span>3,216,743</span></div>
            <div id="displayPayFreq">monthly</div>
        </div>
        <div id="outTerm" class="ctrl" style="display: none;">
            <div class="displayLabel">I'll have it paid down in:</div>
            <div id="displayYrMonth">
                <div id="displayYr">YEARS</div>
                <div id="displayMonth"></div>
            </div>
            <div id="displayYrs">25</div>
        </div>

        <div class="smallPrint"><span id="interestNote">Calculations assume monthly</span>
            <br>
            <!--            <a id="canTogLink">Click here</a><span id="canOrNot"> if this is a Canadian fixed rate mortgage</span>-->
        </div>
    </div>

{{--    <div class="chart-container">--}}
{{--        <canvas id="myChart"><p>Hello Fallback World</p></canvas>--}}
{{--    </div>--}}

</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="{{asset('performance_impact_calculator/main.js')}}"></script>


</html>
