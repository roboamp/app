@extends('admin.list')

@section('css_list')

    <link rel="stylesheet" type="text/css" charset="utf-8"
          href="{{asset('performance_impact_calculator/stylesheet.css')}}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('performance_impact_calculator/adminMort.css')}}">

@endsection


@section('list')

        <div id="interact">

            <div id="headerDiv"><span class="header">A BETTER MORTGAGE AND LOAN CALCULATOR</span></div>
            <h1 id="titlePay" class="title">Performance Impact Calculator</h1>

            <div id="first">
                <div id="ctrlPrin" class="ctrl">
                    <div class="label">Average Monthly Visitors:</div>
                    <input id="average_monthly_visitors" class="slideRule" name="average_monthly_visitors" value="250000">
                    <div class="label"></div>
                    <div class="slider" id="intRateSlide">
                        <div class="slideRule"></div>
                    </div>


                </div>


                <div id="ctrlInt" class="ctrl">
                    <div class="label">Conversion Rate:</div>
                    <input id="conversion_rate" type="text" class="js-range-slider" name="conversion_rate" value="15"/>
                    <div class="label"></div>
                    <div class="slider" id="intRateSlide">
                        <div class="slideRule"></div>
                    </div>
                </div>
                <div id="ctrlTerm" class="ctrl">
                    <div class="label">Average Order Value:</div>
                    <input id="average_order_value" type="text" class="js-range-slider" name="average_order_value"
                           value="100"/>
                    <div class="label"></div>
                    <div class="slider" id="intRateSlide">
                        <div class="slideRule"></div>
                    </div>

                </div>

                <div id="ctrlTerm" class="ctrl">
                    <div class="label">Loading Time Speed:</div>
                    <input id="loading_time_speed" type="text" class="js-range-slider" name="loading_time_speed" value="4"/>
                    <div class="label"></div>
                    <div class="slider" id="intRateSlide">
                        <div class="slideRule"></div>
                    </div>

                </div>

                <div id="ctrlTerm" class="ctrl">
                    <div class="label">Potential Time Speed:</div>
                    <input id="potential_time_speed" type="text" class="js-range-slider" name="potential_time_speed"
                           value=""/>
                    <div class="label"></div>
                    <div class="slider" id="intRateSlide">
                        <div class="slideRule"></div>
                    </div>


                </div>

                <!--<div class="smallLink">A list of current Canadian interest rates can be found <a href="http://www.fiscalagents.com/rates/mor_clo_sort.shtml" target="_blank">here</a><br/>
                  The rest of the world will have to rely on <a href="https://www.google.ca/search?q=mortgage+rates" target="_blank">google</a>
                </div>-->

            </div>

            <div id="second">
                <div id="outPrin" class="ctrl" style="display: none;">

                </div>
                <div id="outPay" class="ctrl">
                    <div class="displayLabel">my ROI will be:</div>
                    <div id="displayPay" class="result">$<span>3,216,743</span></div>
                    <div id="displayPayFreq">monthly</div>
                </div>
                <div id="outTerm" class="ctrl" style="display: none;">
                    <div class="displayLabel">I'll have it paid down in:</div>
                    <div id="displayYrMonth">
                        <div id="displayYr">YEARS</div>
                        <div id="displayMonth"></div>
                    </div>
                    <div id="displayYrs">25</div>
                </div>

                <div class="smallPrint"><span id="interestNote">Calculations assume monthly</span>
                    <br>
                    <!--            <a id="canTogLink">Click here</a><span id="canOrNot"> if this is a Canadian fixed rate mortgage</span>-->
                </div>
            </div>
        </div>

@endsection

@section('list_child_script_include')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>
    <script src="{{asset('performance_impact_calculator/main.js')}}"></script>
@endsection
