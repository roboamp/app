<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>PIC</title>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container text-center p-5">
    <h1 class="result">Total: $<span></span></h1>
</div>

<div class="container shadow p-3 mb-5 bg-white rounded text-center">
    <p class="average_monthly_visitors font-bold">Average Monthly Visitors</p>
    <input id="average_monthly_visitors" type="text" class="js-range-slider"
           name="average_monthly_visitors" value=""/>
</div>

<div class="container shadow p-3 mb-5 bg-white rounded text-center">
    <p class="conversion_rate font-bold">Conversion Rate</p>
    <input id="conversion_rate" type="text" class="js-range-slider" name="conversion_rate" value=""/>
</div>

<div class="container shadow p-3 mb-5 bg-white rounded text-center">
    <p class="average_order_value font-bold">Average Order Value</p>
    <input id="average_order_value" type="text" class="js-range-slider" name="average_order_value"
           value=""/>
</div>

<div class="container shadow p-3 mb-5 bg-white rounded text-center">
    <p class="loading_time_speed font-bold">Loading Time Speed</p>
    <input id="loading_time_speed" type="text" class="js-range-slider" name="loading_time_speed"
           value=""/>
</div>

<div class="container shadow p-3 mb-5 bg-white rounded text-center">
    <p class="potential_time_speed font-bold">Potential Time Speed</p>
    <input id="potential_time_speed" type="text" class="js-range-slider" name="potential_time_speed"
           value=""/>
</div>

</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/js/ion.rangeSlider.min.js"></script>

<script>

    function sendRequest() {

        $amv = $("#average_monthly_visitors").val();
        $cr = $("#conversion_rate").val();
        $aov = $("#average_order_value").val();
        $lts = $("#loading_time_speed").val();
        $pts = $("#potential_time_speed").val();

        $token = $('input[name="_token"]').val();

        $.ajax({
            url: "/admin/pic/store",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            method: 'POST',
            data: {
                amv: $amv,
                cr: $cr,
                aov: $aov,
                lts: $lts,
                pts: $pts
            }
        })
            .done(function (data) {
                $(".result span").text(data.result);
            })
            .fail(function (data) {
                console.log('fail');
            });
    }


    $("#average_monthly_visitors").ionRangeSlider({

        skin: "round",
        min: 50000,
        max: 500000,
        step: 50000,
        grid: false,
        onChange: function (data) {
            sendRequest();
        }
    });

    $("#conversion_rate").ionRangeSlider({

        skin: "round",
        min: 5,
        max: 30,
        grid: false,
        step: 5,
        postfix: "%",
        onChange: function (data) {
            sendRequest();
        }

    });

    $("#average_order_value").ionRangeSlider({

        skin: "round",
        min: 50,
        max: 200,
        grid: false,
        step: 50,
        prefix: "$",
        onChange: function (data) {
            sendRequest();
        }

    });

    $("#loading_time_speed").ionRangeSlider({

        skin: "round",
        min: 2,
        max: 8,
        grid: false,
        onChange: function (data) {
            sendRequest();
            range_obj = $("#potential_time_speed");
            range = range_obj.data("ionRangeSlider");
            var o = {
                max: $lts
            };
            range.update(o);
        }
    });

    $("#potential_time_speed").ionRangeSlider({

        skin: "round",
        min: 0.6,
        max: 8,
        grid: false,
        step: 0.1,
        onChange: function (data) {
            sendRequest();
        }
    });

</script>
</html>
