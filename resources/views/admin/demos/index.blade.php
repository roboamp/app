@extends('admin.templates.list')

@section('content')
    @if(count($demos)>1)
        @foreach($demos as $item)
            @include('admin.demos.includes.demos_rows',['demo'=>$item])
        @endforeach
    @elseif (count($demos)>0)
            @include('admin.demos.includes.demo_row')
    @endif
@endsection

