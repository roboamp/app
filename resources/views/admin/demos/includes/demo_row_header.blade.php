<div class="card-header" id="heading">
    <div class="row">
        <div class="col-md-6"><h4> {{$demos[0]->name}}</h4></div>
        <div class="col-md-6">
            <h4>Status:
                <div style="margin-left: 30px" class="label {{$demos[0]->status->class}}">{{$demos[0]->status->name}}</div>
            </h4>
        </div>
    </div>
</div>