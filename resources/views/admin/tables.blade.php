@extends('admin.dashboard')
@section('css')
    <link href="{{asset('css/dashboard/footable.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard_assets/css/pages/footable-page.css')}}" rel="stylesheet">
    <link href="{{asset('dashboard_assets/css/pages/other-pages.css')}}" rel="stylesheet">
    @yield('css_table')
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{$card_title ?? ""}}</h4>
                    <h6 class="card-subtitle">{{$card_subtitle ?? ""}}</h6>
                    @yield('table_content')
                </div>
            </div>
       </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/dashboard/footable.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/js/pages/footable-init.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/moment/min/moment.min.js')}}"></script>
    <script>
    $('*[data-href]').on("click",function(){
        window.location = $(this).data('href');
        return false;
    });
    $("td > a").on("click",function(e){
        e.stopPropagation();
    });
    </script>
@endsection