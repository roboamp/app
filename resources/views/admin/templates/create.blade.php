@extends('admin.forms')

@section('css_form')
    <link href="{{asset('dashboard_assets/dist/css/pages/bootstrap-switch.css')}}" rel="stylesheet">
@endsection

@section('form_fields')
    <div class="form-row">

        <div class="col-md-6 mb-3">
            <label for="txt_name">Name</label>
            <input type="input" value='{{$dev_mode_data['name']??old("txt_name")}}' class="form-control @error('txt_name')alert-danger @enderror"
                   id="txt_name" name="txt_name" required placeholder="Descriptive name for the Template">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>


        <div class="col-md-6 mb-3">
            <label for="property_id">Property</label>
            <select class="custom-select" required id="property_id" name="property_id">
                <option value="">Select a Property</option>

                    @foreach($properties as $item)
                        <option value="{{$item->id}}" {{ old('property_id') == $item->id ? 'selected' : ''  }}>{{$item->url}}</option>
                    @endforeach
            </select>
            <div class="invalid-tooltip">Select a Property</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>

    <div class="form-row">

        <div class="col-md-6 mb-3">
            <label for="chk_dynamic_template">Dynamic Template</label>
            <br>
            <input type="checkbox" name="chk_dynamic_template" data-toggle="switch" data-on-color="success" checked data-inverse="true"  data-off-color="default" data-off-text="NO" data-on-text="Yes">
        </div>

        <div class="col-md-6 mb-3">
            <label for="txt_url">Target URL</label>
            <input type="url" placeholder="Enter the URL for the Template" value="{{$dev_mode_data['target_url']??old('txt_url')}}" class="form-control @error('txt_url')alert-danger @enderror" id="txt_url" name="txt_url" required>
            @error('txt_url')
            <p class="text-danger">{{$errors->first('txt_url')}}</p>
            @enderror
            <div class="invalid-tooltip">URL is Required</div>
            <div class="valid-tooltip">Looks good!</div>

        </div>




        <div class="col-md-6 mb-3">
            <label for="select_selector_type">Selector Type for the signature</label>
            <select class="custom-select" required id="selector_id" name="selector_id">
                <option value="">SELECTOR type</option>

                    @foreach($selectors as $item)
                        <option value="{{$item->id}}" {{ old('selector_id') == $item->id ? 'selected' : ''  }}>{{$item->name}}</option>
                    @endforeach
            </select>
            <div class="invalid-tooltip">Select a Selector</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>


<div class="form-row">
    <div class="col-md-12 mb-3">
        <label for="txt_signature">Signature</label>
        <textarea  class="form-control" rows="10" id="txt_signature" name="txt_signature" placeholder="HTML code we are going to search for" required>{{ $dev_mode_data['signature']??old('txt_signature')}}</textarea>
        <div class="invalid-tooltip">Please enter the HTML signature</div>
        <div class="valid-tooltip">Looks good!</div>
    </div>

</div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Code Sections</h4>
                    <div id="code_sections"></div>
                    <div class="row">

                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <input type="hidden" id="txth_selector" name="txth_selector" value='{!! $selector_type_options!!}'>
                                <input type="hidden" id="txth_content" name="txth_content" value='{!! $selector_content_type_options!!}'>

                                <select class="custom-select"  id="dd_code_block_selector_type" name="dd_code_block_selector_type[]">
                                    {!! $selector_type_options!!}
                                </select>

                            </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <select class="custom-select"  id="dd_content_type" name="dd_content_type[]">
                                    {!! $selector_content_type_options!!}
                                </select>                                </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <input type="text" class="form-control" id="txt_block_class" name="txt_block_class[]" value="" placeholder="Block Class">
                            </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="txt_block_value" name="txt_block_value[]" value="" placeholder="Block Value">

                                    <div class="input-group-append">
                                        <button class="btn btn-success" type="button" onclick="education_fields();">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('new_row')

@endsection
@section ('form_scripts_include')
    <script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('dashboard_assets/js/pages/templates/create.js')}}"></script>

@endsection
<!-- code executed during init() main thread -->
@section('form_script')
    $('[data-toggle="switch"]').bootstrapSwitch();



@endsection
