@extends ('admin.tables')
@section ('css_table')
    <style>
        *[data-href] {
            cursor: pointer;
        }
        td a {
            display:inline-block;
            min-height:100%;
            width:100%;
            padding: 10px; /* add your padding here */
        }
        td {
            padding:0;
        }
    </style>
@endsection
@section('table_content')
    <!-- Accordion -->
    <div class="accordion" id="accordionTable">

        @yield('content')

    </div>

@endsection