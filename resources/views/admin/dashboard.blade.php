<!DOCTYPE html>
<html lang="en">

@include('admin.includes.header')


<body class="skin-default fixed-layout">
    @include('admin.includes.preloader')
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        @include('admin.includes.top_bar_header',['messages'=>$messages ?? '','comments'=>$comments ?? ''])

        @include('admin.includes.left_side_panel',['user'=>$user])

        @include('admin.includes.page_wrapper')

        @include('admin.includes.footer')

    </div>


    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    @include('admin.includes.main_js')
    <!--BAT -->
    @yield('scripts')
    <!--MAN -->
</body>

</html>