@extends('admin.list')


@section('list')
<div class="clearfix">
    <a href="{{ route('admin.schedule.create') }}" class="btn btn-primary float-right">Create Scheduled Task</a>
</div>
<div class="table-responsive mt-3">
    <table class="table table-striped" id="tasks-table">
        <thead>
            <tr>
                <th width="15%">Activity</th>
                <th width="25%">Command</th>
                <th width="25%">Parameters</th>
                <th width="15%">Timeframe</th>
                <th width="10%">User</th>
                <th width="10%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tasks as $task)
            <tr id="task-row-{{$task->id}}">
                <td>
                    <span class="activity-{{ $task->activity->id }}">{{ $task->activity->name }}</span>
                </td>
                <td>
                    <span class="command-{{ $task->activity->id }}">{{ $task->activity->command }}</span>
                </td>
                <td>
                    <span class="user-{{ $task->parameters }}">{{ $task->parameters }}</span>
                </td>
                <td>
                    <span class="timeframe-{{ $task->timeframe->id }}">{{ $task->timeframe->name }}</span>
                </td>
                <td>
                    <span class="user-{{ $task->user->id }}">{{ $task->user->name }}</span>
                    <input type="hidden" value="{{ $task->user->name }}" name="user-{{ $task->user->id }}" id="user-{{ $task->user->id }}">
                </td>
                <td>
                    <div class="actions-{{ $task->id }}">
                        <form id="edit-task-{{$task->id}}" method="POST" action="{{route('admin.activities.edit')}}">
                            @csrf
                            <input type="hidden" value="{{$task->id}}" name="id" id="id">
                        </form>
                        <a class="btn btn-info btn-sm edit" href="/admin/schedule/edit/{{$task->id}}"><i class="ti-pencil-alt"></i></a>
                        <button type="button" class="btn btn-danger btn-sm destroy" data-target="{{ $task->id }}"><i class="ti-trash"></i></button>
                    </div>
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>


<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Schedelued Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            @csrf
            @method("DELETE")
            <input type="hidden" id="deleteTaskId" name="task" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="confirm-destroy">Understood, continue.</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();

    @if(count($tasks) <= 10 )
    $('#tasks-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#tasks-table').DataTable();
    @endif

    $('.alert').alert();

    $(document).on("click", ".destroy", function(){
        let target = $(this).data("target");
        $("#deleteTaskId").val(target);
        $('#destroyModal').modal({
            keyboard: false
        })
    });
    
    $(document).on("click", "#confirm-destroy", destroy_task);

    function destroy_task(){
        let task = $("#deleteTaskId").val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        let method = $('input[name="_method"]').val();
        $('#destroyModal').modal("hide");
        $.post( "{{route('admin.schedule.destroy')}}", { task: task, _token: csrf_token, _method: method }, function( data ) {
            if(data.success == true){
                $('#tasks-table').DataTable().row("#task-row-"+data.target).remove().draw("false");
                $.toast({
                    heading: 'Schedule Deleted',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#00c292',
                    icon: 'success',
                    hideAfter: 3000,
                    stack: false, 
                });
            }else{
                $.toast({
                    heading: 'Schedule Deleted',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#e46a76',
                    icon: 'error',
                    hideAfter: 3000,
                    stack: false, 
                });
            }
        });
    }
    </script>
@endsection