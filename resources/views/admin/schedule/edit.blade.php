@extends('admin.forms')

@section('css_form')
    
@endsection

@section('form_fields')
@method("PATCH")
<input type="hidden" id="user" name="user" value="{{$user->id}}">
<div class="form-row">
    <div class="col-6">
        <label>Activity</label>
        <select class="form-control" name="activity" id="activity">
            @foreach($activities as $activity)
            <option value="{{$activity->id}}" @if($activity->id == $task->activity_id) selected @endif>{{$activity->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-6">
        <label>Timeframe</label>
        <select class="form-control" name="timeframe" id="timeframe">
            @foreach($timeframes as $timeframe)
            <option value="{{$timeframe->id}}" @if($timeframe->id == $task->timeframe_id) selected @endif>{{$timeframe->name}}</option>
            @endforeach
        </select>
        <input type="hidden" name="old_timeframe" id="old_timeframe" value="{{ $task->timeframe_id }}">
    </div>                                                                                                                          
</div>
<div class="form-row mt-3">
    <div class="col-12">
        <label>Command</label>
        <div class="alert alert-info" role="alert" id="command" data-value="{{$task->activity->command}}">{{$task->activity->command}}</div>
    </div>                                                                                                                         
</div>
<div class="form-row mt-3">
    <div class="col-12">
        <label>Parameters</label>
        <input type="text" name="parameters" id="parameters" value="{{$task->parameters}}" class="form-control">
    </div>                                                                                                                         
</div>
@endsection


@section('form_fields')

@endsection



@section('new_row')

@endsection


@section ('form_scripts_include')

@endsection



@section('form_script')
$(document).ready(function(){
    updateCommand();
})
$("#activity").on("change", getCommand);

$("#parameters").on("change", updateCommand);

$(document).on("mouseover", "#timeframe", function(){
    let old = $(this).val(); 
    $("#old_timeframe").val(old);
});

function getCommand(){
    let activity = $("#activity").val();
    let csrf_token = $('meta[name="csrf-token"]').attr('content');
    $.post('{{route("admin.schedule.activities.command")}}', { _token: csrf_token, activity: activity }, function(data){
        if(data.success){
            $("#command").text(data.command).data("value", newCommand);
        }
    });
}

function updateCommand(){
    let parameters = $("#parameters").val();
    let command = $("#command").data("value");
    let newCommand = command+' '+parameters;
    $("#command").text(newCommand);
}
@endsection