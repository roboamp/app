@extends('admin.forms')

@section('css_form')
    
@endsection


@section('form_fields')
<input type="hidden" id="user" name="user" value="{{$user->id}}">
    <div class="form-row">
        <div class="col-6">
            <label>Activity</label>
            <select class="form-control" name="activity" id="activity">
                @foreach($activities as $activity)
                <option value="{{$activity->id}}" >{{$activity->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-6">
            <label>Timeframe</label>
            <select class="form-control" name="timeframe" id="timeframe">
                @foreach($timeframes as $timeframe)
                <option value="{{$timeframe->id}}">{{$timeframe->name}}</option>
                @endforeach
            </select>
        </div>                                                                                                                          
    </div>
    <div class="form-row mt-3">
        <div class="col-12">
            <label>Command</label>
            <div class="alert alert-info" role="alert" id="command" data-value=""></div>
        </div>                                                                                                                         
    </div>
    <div class="form-row mt-3">
        <div class="col-12">
            <label>Parameters</label>
            <input type="text" name="parameters" id="parameters" value="" class="form-control">
        </div>                                                                                                                         
    </div>
@endsection



@section('new_row')

@endsection


@section ('form_scripts_include')

@endsection



@section('form_script')
$(document).ready(function(){
    getCommand();
})
$("#activity").on("change", getCommand);

$(document).on("change", "#parameters", updateCommand);

function getCommand(){
    let activity = $("#activity").val();
    let csrf_token = $('meta[name="csrf-token"]').attr('content');
    $.post('{{route("admin.schedule.activities.command")}}', { _token: csrf_token, activity: activity }, function(data){
        if(data.success){
            $("#command").text(data.command).data("value", data.command);
        }
    });
}

function updateCommand(){
    let parameters = $("#parameters").val();
    let command = $("#command").data("value");
    let newCommand = command+' '+parameters;
    $("#command").text(newCommand);
}
@endsection