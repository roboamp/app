@extends('spark::layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    @include('spark::shared.errors')

                    @if(isset($coupon))
                        @include('admin.coupons.edit_coupon')

                    @else
                        @include('admin.coupons.create_coupon')
                    @endif

            </div>
        </div>
    </div>
@endsection

@section('extra_js')
    @yield('extra_js')

@endsection
