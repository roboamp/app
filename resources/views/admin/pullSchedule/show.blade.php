@extends('admin.list')

@section('list')
<div class="row">
    <div class="col-12">
        <h4>Property: {{$property->name}}</h4>
    </div>
    <div class="col-12">
        <h5>Pages</h5>
        <div class="table-responsive">
            <table id="pages-table" class="table table-hover table-selected">
                <thead>
                    <tr>
                        <th width="60%">URL</th>
                        <th width="15%">Schedule Active</th>
                        <th width="25%">Timeframe</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($property->pages as $page)
                    <tr>
                        <td>{{$page->url}}</td>
                        <td><input type="checkbox" id="active-{{$page->id}}" name="chk_dynamic_template" data-toggle="switch" data-on-color="success" data-size="mini" data-inverse="true"  data-off-color="default" data-off-text="Off" data-on-text="On" data-target="{{$page->id}}" @if($page->pullSchedule AND $page->pullSchedule->active == true) checked @else '' @endif></td>
                        <td>
                        <select class="form-control @if($page->pullSchedule AND $page->pullSchedule->active == true) '' @else d-none @endif timeframe" id="page-timeframe-{{$page->id}}" data-target="{{$page->id}}" data-old="@if($page->pullSchedule) {{$page->pullSchedule->timeframe_id}} @else 1 @endif">
                        @foreach($timeframes as $timeframe)
                            <option value="{{$timeframe->id}}" @if($page->pullSchedule AND $page->pullSchedule->timeframe_id == $timeframe->id) selected @endif>{{$timeframe->name}}</option>
                        @endforeach
                        </select>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>             
        </div>
    </div>
</div>
@endsection

@section('scripts')
<link href="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
<script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
<script>
$('[data-toggle="switch"]').bootstrapSwitch();

$(document).on('switchChange.bootstrapSwitch', '[data-toggle="switch"]', function () {
    saving_pull_schedule($(this).data("target"));
});

$(document).on("mouseover", ".timeframe", function(){
    let old = $(this).val(); 
    $(this).data("old", old);
});

$(document).on('change', '.timeframe', function(){
    saving_pull_schedule($(this).data("target"));
});

$(document).on('click', '.close-property-alert', function () {
    $('#property-more-info').remove();
});

function saving_pull_schedule(page){
    let timeframe = $("#page-timeframe-"+page).val();
    let old_timeframe = $("#page-timeframe-"+page).data("old");
    let csrf_token = $('meta[name="csrf-token"]').attr('content');
    let active;
    if($("#active-"+page).bootstrapSwitch('state') === true){
        active = 1;
        $("#page-timeframe-"+page).removeClass("d-none");
    }else{
        active = 0;
        $("#page-timeframe-"+page).addClass("d-none");
    }
    let user = {{Auth::User()->id}};
    $.post("{{route('admin.schedule.page')}}", { active: active, page: page, timeframe: timeframe, old_timeframe: old_timeframe, user: user, _token:csrf_token }, function(data){
        if(data.success == true){
            $.toast({
                heading: 'Section Updated',
                text: data.message,
                position: 'top-right',
                loaderBg:'#00c292',
                icon: 'success',
                hideAfter: 3000,
                stack: false, 
            });
        }else{
            $.toast({
                heading: 'Section Updated',
                text: data.message,
                position: 'top-right',
                loaderBg:'#e46a76',
                icon: 'error',
                hideAfter: 3000,
                stack: false, 
            });
        }
    });
}

@if(count($property->pages) <= 10 )
    $('#pages-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#pages-table').DataTable();
    @endif
</script>
@endsection