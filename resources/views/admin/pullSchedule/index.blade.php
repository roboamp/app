@extends('admin.list')

@section('list')
    @foreach($clients as $client)
    <div class="card shadow p-3 mb-5 bg-white rounded">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <h4>Client: {{$client->name}}</h4>
                </div>
                <div class="col-12">
                    <h5>Properties</h5>
                    <div class="table-responsive">
                        <table class="table table-hover table-stripped table-selected clickable-table" id="properties-table">
                            <thead>
                                <th>Property Name</th>
                                <th>Domain</th>
                                <th>Price Bucket</th>
                            </thead>
                            <tbody>
                                @foreach($client->properties as $property)
                                <tr id="property-{{$property->id}}" data-target="{{$property->id}}" class="collapsible">
                                    <td>{{$property->name}}</td>
                                    <td>{{$property->domain}}</td>
                                    <td>{{$property->bucket->name}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endsection

@section('scripts')
<link href="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.min.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
<script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
<script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
<script>
    $("#properties-table > tbody > tr").on("click",function(e){
        window.location = '/admin/pull-schedule/show/'+$(this).data('target');
    });
    
    @if(count($clients) <= 10 )
    $('#properties-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#properties-table').DataTable();
    @endif
</script>
@endsection