

<div class="panel-heading">Create a Coupon</div>

<div class="panel-body">
<form class="form-horizontal" role="form" method="POST" action="{{route('users.coupons.add.post')}}">
                        {{ csrf_field() }}

                        <!-- Coupon Name -->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Coupon ID</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="txt_coupon_id" value="{{Faker\Factory::create()->name()}}" autofocus>
                                </div>
                            </div>

                            <!-- Percent Off -->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Percent Off</label>

                                <div class="col-md-6">
                                   <input type="number" class="form-control" name="txt_percent_off" value="10" max="100" min="0">

                                </div>
                            </div>
                            <!-- Max Redemptions -->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Max Redemptions</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="txt_max_redemptions" value="10">
                                </div>
                            </div>
                            <!-- Max Redemptions -->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Duration</label>

                                <div class="col-md-6">
                                    <select name="cmb_duration">
                                        <option value="forever">Forever</option>
                                        <option value="once" selected="true">Once</option>
                                        <option value="repeating">Repeating</option>
                                    </select>
                                </div>
                            </div>



                            <!-- create Button -->
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa m-r-xs fa-sign-in"></i>Create Coupon
                                    </button>

                                </div>
                            </div>
                        </form>
                        </div>
                                        </div>