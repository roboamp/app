<div class="panel-heading">Detail View Coupon</div>

<div class="panel-body">
<form class="form-horizontal" role="form" id="form">
    {{ csrf_field() }}

    <!-- Coupon Name -->
        <div class="form-group">
            <label class="col-md-4 control-label">Coupon ID</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="txt_coupon_id" value="{{$coupon['id']}}" autofocus>
                <input type="hidden" class="form-control" name="txt_coupon_old_id" value="{{$coupon['id']}}" autofocus>

            </div>
        </div>

        <!-- Percent Off -->
        <div class="form-group">
            <label class="col-md-4 control-label">Percent Off</label>

            <div class="col-md-6">
               <input type="number" class="form-control" name="txt_percent_off" value="{{$coupon['percent_off']}}" max="100" min="0">

            </div>
        </div>
        <!-- Max Redemptions -->
        <div class="form-group">
            <label class="col-md-4 control-label">Max Redemptions</label>

            <div class="col-md-6">
                <input type="text" class="form-control" name="txt_max_redemptions" value="{{$coupon['max_redemptions']}}">
            </div>
        </div>



                            <!-- Close Windows Button -->
            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="button" class="btn btn-primary" id="btn_close">
                        <i class="fa m-r-xs fa-sign-in"></i>Close Window
                    </button>

                </div>
            </div>
        </form>
    </div>
</div>

@section('extra_js')
$("#form").submit(function(e){
    return false;
});


$('#btn_close').on('click',function(){
    window.open('','_parent','');
    window.close();
});
@endsection