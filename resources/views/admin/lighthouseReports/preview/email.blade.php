@extends('admin.list')

@section('list')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-10 col-md-8 col-lg-6">
            <p class="h2">URL: {{ $report->url }}</p>
            @foreach ($report->flags as $flag)
            <p class="h3">{{ $flag->name }}</p>
            @if($flag->pivot->score_normal > 0)
            @php $more = ((((float)$flag->pivot->score * 100)/(float)$flag->pivot->score_normal)-100); @endphp
            @endif
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th class="h5">Score without ROBOAMP</th>
                        <th class="h5">Score with ROBOAMP</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><img src="{{ asset('knobs/generated/knob-normal-'.($flag->pivot->score_normal*100).'.png') }}"/></td>
                        <td><img src="{{ asset('knobs/generated/knob-amp-'.($flag->pivot->score*100).'.png') }}"/></td>
                    </tr>
                    @if($flag->pivot->score_normal > 0)
                    <tr>
                        <td colspan="2"><p class="text-center h4"><span style="color: #01c0c8"><i class="ti-arrow-up"></i><em>{{ round($more, 2)}}% faster</em></span></p></td>
                    </tr>
                    @endif
                </tbody>
            </table>
            <p>{{ $flag->message }}</p>

            @endforeach
        </div>
    </div>
</div>
@endsection