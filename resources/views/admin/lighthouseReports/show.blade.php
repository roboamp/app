@extends('admin.list')

@section('list')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-body">
            <h4 class="card-title">Lighthouse Report</h4>
            <h5 class="card-subtitle">URL: <a href="{{$report->url}}" target="_blank">{{$report->url}}</a> </h5>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Status</label>
                        <input type="text" class="form-control text-{{$report->status->badge}}" value="{{$report->status->name}} - @if($report->status->id == 3) {{ $report->readed_on }}@endif" readonly data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{$report->status->description}}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="example-email">Last Generated Report</span></label>
                        <input type="text" class="form-control" value="{{$report->added_on}}" readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label>Category</label>
                        <input type="text" class="form-control" value="{{$report->category->name}}" readonly>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="example-email">Type</span></label>
                        <input type="text" class="form-control" value="{{$report->type}}" readonly>
                    </div>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Advantages of Roboamp</th>
                    <th scope="col">Score without ROBOAMP</th>
                    <th scope="col">Score with ROBOAMP</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($report->flags as $flag)
                    <tr>
                        <td>{{$flag->name}}</td>
                        <td class="pl-5"><img src="{{ asset('knobs/generated/knob-normal-'.($flag->pivot->score_normal*100).'.png') }}" width="65" alt="{{$flag->pivot->score_normal}}"/></td>
                        <td class="pl-5"><img src="{{ asset('knobs/generated/knob-amp-'.($flag->pivot->score*100).'.png') }}" width="65" alt="{{$flag->pivot->score}}"/></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="row">
                @if($report->lighthouse_status_id != 5)
                <h5 class="card-subtitle">Send report </h5>
                <div class="col">
                    <div class="form-group">
                        <form method="POST" action="{{route('admin.speedy.mailto')}}" class="form-inline">
                            @csrf
                            <div class="form-group mb-2">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                    <input type="hidden" value="{{ $report->id }}" name="report_id" id="report_id">
                                    <input type="hidden" value="3" name="status" id="status">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Send Mail</button>
                        </form>
                    </div>
                </div>
                @endif
                <div class="col">
                    <p class="text-center"><a href="{{url('admin/speedy/preview/email/'.$report->id)}}" class="btn btn-primary">Preview Email</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    </script>
@endsection