@extends('admin.list')

@section('list')
<div class="clearfix">
    <a href="{{ route('admin.speedy.create') }}" class="btn btn-primary float-right">Execute Test</a>
</div>
<div class="table-responsive mt-3">
    <table class="table table-striped clickable-table" id="reports-table">
        <thead>
            <tr>
                <th>URL</th>
                <th>Category</th>
                <th>Type</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reports as $report)
            <tr data-target="{{$report->id}}">
                <td><a href="{{$report->url}}" target="_blank">{{$report->url}}</a></td>
                <td>{{$report->category->name}}</td>
                <td>{{$report->type}}</td>
                <td>
                    <span class="label label-{{$report->status->badge}}" id="label-<?= $report->id?>">{{$report->status->name}}</span>
                    @if ($report->status->id == 1)
                    <div class="spinner-border ml-3 spinner-<?= $report->id?>" role="status"><span class="sr-only">Loading...</span></div>
                    @endif
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
    
</div>
@endsection

@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
    $("#reports-table > tbody > tr").on("click",function(e){
        window.location = '/admin/speedy/show/'+$(this).data('target');
    });

    @if(count($reports) <= 10 )
    $('#reports-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#reports-table').DataTable();
    @endif

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('42821be930c76bca6620', {
      cluster: 'us2'
    });

    var channel = pusher.subscribe('LighthouseReport');
    channel.bind('App\\Events\\LighthouseReportUpdated', function(data) {
        $(".spinner-"+data.data.report.id).addClass("d-none");
        $("#label-"+data.data.report.id).attr("class", "");
        $("#label-"+data.data.report.id).text("");
        $("#label-"+data.data.report.id).attr("class", "label label-"+data.data.status.badge);
        $("#label-"+data.data.report.id).text(data.data.status.name);
        if(data.data.success){
            $.toast({
                heading: 'Lighthouse Report Update',
                text: data.data.message,
                position: 'top-right',
                loaderBg:'#00c292',
                icon: 'success',
                hideAfter: 3000,
                stack: false, 
            });
        }else{
            $.toast({
                heading: 'Lighthouse Report Update',
                text: data.data.message,
                position: 'top-right',
                loaderBg:'#e46a76',
                icon: 'error',
                hideAfter: 3000,
                stack: false, 
            });
        }
    });
    </script>
@endsection