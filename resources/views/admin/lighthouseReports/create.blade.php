@extends('admin.forms')

@section('css_form')
    <link href="{{asset('dashboard_assets/dist/css/pages/bootstrap-switch.css')}}" rel="stylesheet">
@endsection

@section('form_fields')
    <div class="form-row">
        <div class="col-sm-12 col-lg-6">
            <div class="form-group">
                <label>Categories</label>
                <select class="custom-select" id="category" name="category" required>
                    <option value="">Choose...</option>
                @foreach ($categories as $category)
                    <option value="{{$category->id}}" {{ old('category') == $category->id ? 'selected' : ''  }}>{{$category->name}}</option>
                @endforeach
                </select>
                <div class="invalid-tooltip">Select a Category</div>
                <div class="valid-tooltip">Looks good!</div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col-sm-12">
            <label for="urls">Urls</label>
            <textarea rows="7" class="form-control @error('txt_name')alert-danger @enderror" id="url" name="url" rows="5" required>{{old('url')}}</textarea>
            @error('name')
            <p class="text-danger">The URLs are required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The URLs are required</div>
            <div class="valid-tooltip">Looks good!</div>
            
        </div>
    </div>
@endsection

@section('new_row')

@endsection
@section ('form_scripts_include')
    <script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('dashboard_assets/js/pages/templates/create.js')}}"></script>

@endsection
<!-- code executed during init() main thread -->
@section('form_script')
    $('[data-toggle="switch"]').bootstrapSwitch();



@endsection