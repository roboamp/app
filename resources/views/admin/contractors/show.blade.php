@extends('admin.forms')

@section('css_form')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.11.0/sweetalert2.css"/>
@endsection


@section('form_fields')
    @csrf
    @method('patch')
    <h1>Edit Contractor</h1>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Contractor Name</label>
            <input type="text" class="form-control" id="name" name="name" required value="{{$dev_contractor->name}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>

    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Contractor Email</label>
            <input type="text" class="form-control" id="email" name="email" required value="{{$dev_contractor->email}}">
            @error('email')
            <p class="text-danger">The email is required{{$errors->first('email')}}</p>
            @enderror
            <div class="invalid-tooltip">The email is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>

@endsection



