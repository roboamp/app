@extends('admin.forms')


@section('form_fields')
    <h1>Create Contractor</h1>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Contractor Name</label>
            <input type="text" class="form-control" id="name" name="name" required value="{{old("name")}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>

    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="description">Contractor Email</label>
            <input type="text" class="form-control" id="email" name="email" required value="{{old("email")}}">
            @error('email')
            <p class="text-danger">The Email is required{{$errors->first('email')}}</p>
            @enderror
            <div class="invalid-tooltip">The Email is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
@endsection
