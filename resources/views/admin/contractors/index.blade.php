@extends('admin.list')

@section('css')
    <style>
        #btn-delete, #btn-update {
            padding: 6px 32px;
        }
    </style>
@endsection

@section('list')

    <div class="messages"></div>
    <div class="cat-messages"></div>

    <h1 class="font-weight-bold pb-3 ml-2">Dev Contractors</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Contractor Name</th>
            <th scope="col">Contractor Email</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @if(count($dev_contractors) > 0)

            @foreach($dev_contractors as $contractor)

                @if($contractor->id != Session::get('active_id') )
                    <?php $class = ""?>

                @else
                    <?php $class = "alert-info"?>
                @endif

                <tr id="deleted-{{$contractor->id}}" class="{{$class ?? ""}}">
                    <td>{{$contractor->name}}</td>
                    <td>{{$contractor->email}}</td>

                    <td>
                        <form method="get" action="{{route('admin.contractors.show', $contractor->id)}}">
                            @csrf
                            <button type="submit" class="btn btn-info" id="btn-update"><i class="ti-pencil-alt"></i></button>
                        </form>
                    </td>
                    <td>
                        <label for="contractor" class="sr-only">Contractor</label>
                        <div class="delete-btn" data-id="{{$contractor->id}}">
                            <button class="btn btn-danger" id="btn-delete"><i class="ti-trash"></i>
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

@endsection

@section('list_child_script_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection

@section('list_child_script')
    $('.delete-btn').on("click", function (e) {
    Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {
    $id = $(this).attr('data-id');
    $token = $('input[name="_token"]').val();

    $.post("/admin/contractors/destroy/" + $id, {
    _token: $token,
    id: $id
    }).done(function (data) {
    var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
    $('.cat-messages').append($success);
    setTimeout(function () {
    $success.remove();
    }, 1800)
    $('#deleted-' + $id).remove();
    })
    .fail(function (data) {
    var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
    $('.cat-messages').append($fail);
    setTimeout(function () {
    $success.remove();
    }, 1800)
    })
    }
    })

    });
@endsection
