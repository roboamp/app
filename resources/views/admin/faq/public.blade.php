<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FAQ</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        .feedback {
            color: white;
            padding: 10px 20px;
            border-radius: 4px;
        }

        #btn-create-question {
            position: fixed;
            top: 20%;
            right: 35px;
        }

        .sidebar {
            position: fixed;
            top: 30%;
            right: 35px;
            width: 100%;
        }

        .button {
            color: #000;
        }


        .current {
            border-radius: 5px;
            background-color: #03a9f3;
            color: #ffffff;
        }

        .category {
            color: black;
        }

        .current a {
            color: #fff;
        }
    </style>
</head>
<body>

<div class="container">
    @if($errors->first('question'))
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong>{{$errors->first('question')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if(session('message'))
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong>{{session('message')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLongTitle">{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('admin.faq.store')}}">
                    @csrf
                    <label for="question" class="sr-only">Question</label>
                    <input type="text" class="form-control" name="question" id="question"
                           placeholder="{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}"
                           required>
                    <button class="btn btn-info mt-3" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="btn-create-question">
    <button type="button"
            class="btn btn-info mb-3 feedback"
            data-toggle="modal"
            data-target="#exampleModalCenter">{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}
    </button>
</div>

<h1 class="text-info font-weight-bold pb-3 ml-2">FAQ</h1>

<div class="d-flex">

    <div class="container col-sm-3 col-2 sidebar shadow-sm p-3 mb-5 bg-white rounded d-none d-lg-block">
        <h2 class="font-weight-bold">Categories</h2>
        <nav id="myScrollspy">
            <ul class="nav nav-pills flex-column">
                <li class="mb-3">
                    <form class="form-inline" method="get" action="{{route('admin.faq.public_list_search')}}">
                        <input class="form-control mr-sm-2" name="search" type="text" placeholder="Search"
                               aria-label="Search">
                        <button class="btn btn-info my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </li>
                @foreach($categories as $category)
                    <li class="nav-item  nav-categories p-2">
                        <a class="category" href="#{{$category->name}}">
                            {{$category->name}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>

    <div class="col-sm-6 col-8 main" data-spy="scroll" data-target="#myScrollspy" data-offset="0">
        @foreach($categories as $category)
            <div id="{{$category->name}}" class="sections">
                <h1 class="font-weight-bold pb-3">{{$category->name}}</h1>
                @foreach($questions as $item)
                    @foreach($item as $question)
                        @if($question->category_id==$category->id && $question->privacy == 'public')
                            <div class="card shadow-sm p-3 mb-5 bg-white rounded">
                                <div class="card-body">
                                    <h3 class="text-info card-title font-weight-bold">
                                        Q: {{ $question->question ?? "N/A"}}</h3>
                                    <h4 class="card-text">A: {{$question->answer??'N/A'}}</h4>
                                    <p class="card-subtitle">Asked by: {{$question->user->name??"N/A"}}</p>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endforeach
            </div>
        @endforeach
    </div>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLongTitle">{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('admin.faq.store')}}">
                    @csrf
                    <label for="question" class="sr-only">Question</label>
                    <input type="text" class="form-control" name="question" id="question"
                           placeholder="{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}"
                           required>
                    <button class="btn btn-info mt-3" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="btn-create-question">
    <button type="button"
            class="btn btn-info mb-3 feedback"
            data-toggle="modal"
            data-target="#exampleModalCenter">{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}
    </button>
</div>


</body>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>

<script>
    {{--    smooth scroll--}}
    $('a[href*="#"]')
        .click(function (event) {
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function () {
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) {
                            return false;
                        } else {
                            $target.focus();
                        }
                    });
                }
            }
        });

    {{--highlights current nav item--}}
    {{--    $('.nav').on('click', '.nav-item', function() {--}}
    {{--    $(this).toggleClass('current').siblings().removeClass('current');--}}
    {{--    });--}}

    var $sections = $('.sections'),
        $li = $('.nav-categories');

    $(window).on('scroll', function () {

        var scrollPos = $(window).scrollTop();

        console.log(scrollPos);

        $sections.each(function () {
            var top = $(this).offset().top - 90;
            if (scrollPos >= top) {
                var $target = $li.eq($(this).index());
                $li.not($target).removeClass('current');
                $target.addClass('current');
            }
        })
    });

    $('.alert').alert().delay(1800).fadeOut('slow');

</script>

</html>