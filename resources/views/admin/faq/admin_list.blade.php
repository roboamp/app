@extends('admin.list')

@section('css')
    <style>
        .selected button {
            background-color: orange;
        }

        .completed {
            background-color: #ebffeb;
        }

         #btn-delete, #btn-update {
             padding: 6px 32px;
         }
    </style>
@endsection

@section('list')
    @include('admin.faq.includes.messages')

    @include('admin.faq.includes.header')

    <div class="cat-messages"></div>

    <h1 class="font-weight-bold pb-3 ml-2">Questions</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Question</th>
            <th scope="col">Answer</th>
            <th scope="col">Name</th>
            <th scope="col">Privacy</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @if(count($questions) > 0)

            @foreach($questions as $question)
                @if($question->id != Session::get('active_id') )
                    <?php $class = ""?>

                @else
                    <?php $class = "alert-info"?>
                @endif

                <tr id="deleted-{{$question->id}}" class="privacy_{{$question->privacy}} {{$class ?? ""}}">
                    <td>{{$question->question}}</td>
                    <td>{{$question->answer}}</td>
                    <td>{{$question->user->name}}</td>
                    <td>{{$question->privacy}}</td>

                    <td>
                        <form method="get" action="{{route('admin.faq.show', $question->id)}}">
                            @csrf
                            <button type="submit" class="btn btn-info" id="btn-update"><i class="ti-pencil-alt"></i></button>
                        </form>
                    </td>
                    <td>
                        <label for="question" class="sr-only">Question</label>
                        <div class="delete-btn" data-id="{{$question->id}}">
                            <button class="btn btn-danger" id="btn-delete"><i class="ti-trash"></i></button>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    {{$questions->links()}}
@endsection

@section('list_child_script_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection

@section('list_child_script')
    $('.alert').delay(1800).fadeOut('slow');

    $('.delete-btn').on("click", function (e) {
    $id = $(this).attr('data-id');
    $token = $('input[name="_token"]').val();

    Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {
    $.ajax({
    url: "/admin/list/question/delete/" + $id,
    headers: {'X-CSRF-TOKEN': $token},
    method: 'DELETE'
    })
    .done(function (data) {
    var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
    $('.cat-messages').append($success);
    $('#deleted-' + $id).remove();
    setTimeout(function () {
    $success.remove();
    }, 1800)
    })
    .fail(function (data) {
    var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
    $('.cat-messages').append($fail);
    setTimeout(function () {
    $fail.remove();
    }, 1800)
    });
    }
    })

    });

    $("#btn_toggle_all").on("click", function () {
    $(".privacy_private").show();
    $(".privacy_public").show();
    $(".privacy_secret").show();
    $("#btn_toggle_all").toggleClass("btn-success");
    $("#btn_toggle_private").removeClass("btn-success");
    $("#btn_toggle_public").removeClass("btn-success");
    $("#btn_toggle_secret").removeClass("btn-success");
    });

    $("#btn_toggle_private").on("click", function () {
    $(".privacy_private").show();
    $(".privacy_public").hide();
    $(".privacy_secret").hide();
    $("#btn_toggle_private").toggleClass("btn-success");
    $("#btn_toggle_all").removeClass("btn-success");
    $("#btn_toggle_public").removeClass("btn-success");
    $("#btn_toggle_secret").removeClass("btn-success");
    });

    $("#btn_toggle_public").on("click", function () {
    $(".privacy_public").show();
    $(".privacy_private").hide();
    $(".privacy_secret").hide();
    $("#btn_toggle_public").toggleClass("btn-success");
    $("#btn_toggle_all").removeClass("btn-success");
    $("#btn_toggle_private").removeClass("btn-success");
    $("#btn_toggle_secret").removeClass("btn-success");
    });

    $("#btn_toggle_secret").on("click", function () {
    $(".privacy_secret").show();
    $(".privacy_public").hide();
    $(".privacy_private").hide();
    $("#btn_toggle_secret").toggleClass("btn-success");
    $("#btn_toggle_all").removeClass("btn-success");
    $("#btn_toggle_private").removeClass("btn-success");
    $("#btn_toggle_public").removeClass("btn-success");
    });
@endsection
