@extends('admin.forms')

@section('css_form')

@endsection

@section('form_fields')

    <form method="post" action="{{route('admin.faq.store')}}">
        @csrf
        <label for="question">Question: </label>
        <input id="question" name="question" type="text" value="{{old('question')}}">
        @error('question')
        <p id="errors">{{$errors->first('question')}}</p>
        @enderror
        <button class="btn btn-primary" type="submit">Submit</button>
    </form>
@endsection

@section('new_row')

@endsection
@section ('form_scripts_include')

@endsection
<!-- code executed during init() main thread -->
@section('form_script')

@endsection
