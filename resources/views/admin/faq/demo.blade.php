@extends('admin.forms')

@section('form_fields')


    <div class="form-row">

        <div class="col-md-12 mb-3">

            <label for="txt_url">Target URL</label>
            <input type="url" placeholder="Enter the URL" value="{{old('txt_url')}}" class="form-control @error('txt_url')alert-danger @enderror" id="txt_url" name="txt_url" required>

            @error('txt_url')
            <p class="text-danger">{{$errors->first('txt_url')}}</p>
            @enderror
            <div class="invalid-tooltip">URL is Required</div>
            <div class="valid-tooltip">Looks good!</div>

        </div>





    </div>



@endsection