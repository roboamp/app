{{-- FAQ modal--}}
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <form method="post" action="{{route('admin.faq.store')}}">
                    @csrf
                    <label for="question" class="sr-only">Question</label>
                    <input type="text" class="form-control" name="question" id="question"
                           placeholder="{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}"
                           required>
                    <button class="btn btn-info mt-3" type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="btn-create-question">
    <button type="button"
            class="btn btn-info mb-3 feedback"
            data-toggle="modal"
            data-target="#exampleModalCenter">{{Auth::user()->is_admin() ? 'Create a Question' : 'Ask a question'}}
    </button>
</div>