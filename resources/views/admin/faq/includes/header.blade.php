<div class="container d-inline-flex justify-content-between">
    <div>
        <h1 class="text-info font-weight-bold pb-3">FAQ</h1>
    </div>
        <div>
            <button id="btn_toggle_all" class="btn btn-secondary btn-success">All</button>
            <button id="btn_toggle_public" class="btn btn-secondary">Public</button>
            <button id="btn_toggle_private" class="btn btn-secondary">Private</button>
            <button id="btn_toggle_secret" class="btn btn-secondary">Secret</button>
        </div>
</div>
