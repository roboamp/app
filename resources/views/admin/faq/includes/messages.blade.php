<div class="container">
    @if($errors->first('question'))
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong>{{$errors->first('question')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if($errors->first('category_id'))
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong>{{$errors->first('category_id')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
        @if($errors->first('answer'))
            <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                <strong>{{$errors->first('answer')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    @if(session('message'))
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong>{{session('message')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
</div>
