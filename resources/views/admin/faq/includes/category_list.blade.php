<div class="container col-sm-3 col-2 sidebar shadow-sm p-3 mb-5 bg-white rounded d-none d-lg-block">
    <h2 class="font-weight-bold">Categories</h2>
    <nav id="myScrollspy">
        <ul class="nav nav-pills flex-column">
            <li class="mb-3">
                <form class="form-inline" method="get" action="{{route('admin.faq.search')}}">
                    <input class="form-control mr-sm-2" name="search" type="text" placeholder="Search"
                           aria-label="Search">
                    <button class="btn btn-info my-2 my-sm-0" type="submit">Search</button>
                </form>
            </li>
            @foreach($categories as $category)
                <li class="nav-item  nav-categories p-2">
                    <a class="category" href="#{{$category->name}}">
                        {{$category->name}}
                    </a>
                </li>
            @endforeach
        </ul>
    </nav>
</div>