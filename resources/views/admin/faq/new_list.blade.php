@extends ('admin.tables')

@section('table_content')
    <!-- Accordian -->
    <div class="accordion" id="accordionTable">
        <div class="card m-b-5">
            <div class="card-header" id="heading1">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#col1" aria-expanded="false" aria-controls="col1">
                        Collapsible Group Item #1
                    </button>
                </h5>
            </div>
            <div id="col1" class="collapse" aria-labelledby="heading1" data-parent="#accordionTable" style="">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-foo-accordion" class="table table-bordered m-b-0 toggle-arrow-tiny footable footable-2 footable-filtering footable-filtering-right footable-paging footable-paging-center breakpoint-md" data-filtering="true" data-paging="true" data-sorting="true" style="">
                            <thead><tr class="footable-filtering"><th colspan="5"><form><div class="input-group footable-filtering-search"><label class="sr-only">Search</label><div class="input-group"><input type="text" class="form-control" placeholder="Search"><div class="input-group-append"><button type="button" class="btn btn-primary"><span class="fas fa-search"></span></button><button type="button" class="btn btn-default dropdown-toggle"><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right"><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> First Name </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> Last Name </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> Job Title </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> DOB </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> Status </label></a></li></ul></div></div></div></form></th></tr>
                            <tr class="footable-filtering footable-header">





                                <th data-toggle="true" class="footable-sortable footable-first-visible" style="display: table-cell;"> First Name <span class="fas fa-sort"></span></th><th class="footable-sortable" style="display: table-cell;"> Last Name <span class="fas fa-sort"></span></th><th data-hide="phone" class="footable-sortable" style="display: table-cell;"> Job Title <span class="fas fa-sort"></span></th><th data-hide="all" class="footable-sortable" style="display: table-cell;"> DOB <span class="fas fa-sort"></span></th><th data-hide="all" class="footable-sortable footable-last-visible" style="display: table-cell;"> Status <span class="fas fa-sort"></span></th></tr>
                            </thead>
                            <tbody>






























                            <tr>





                                <td class="footable-first-visible" style="display: table-cell;">Isidra</td><td style="display: table-cell;">Boudreaux</td><td style="display: table-cell;">Traffic Court Referee</td><td style="display: table-cell;">22 Jun 1972</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-success">Active</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Shona</td><td style="display: table-cell;">Woldt</td><td style="display: table-cell;">Airline Transport Pilot</td><td style="display: table-cell;">3 Oct 1981</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-inverse">Disabled</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Granville</td><td style="display: table-cell;">Leonardo</td><td style="display: table-cell;">Business Services Sales Representative</td><td style="display: table-cell;">19 Apr 1969</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Easer</td><td style="display: table-cell;">Dragoo</td><td style="display: table-cell;">Drywall Stripper</td><td style="display: table-cell;">13 Dec 1977</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-success">Active</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Maple</td><td style="display: table-cell;">Halladay</td><td style="display: table-cell;">Aviation Tactical Readiness Officer</td><td style="display: table-cell;">30 Dec 1991</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Maxine</td><td style="display: table-cell;"><a href="javascript:void(0)">Woldt</a></td><td style="display: table-cell;"><a href="javascript:void(0)">Business Services Sales Representative</a></td><td style="display: table-cell;">17 Oct 1987</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-inverse">Disabled</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Lorraine</td><td style="display: table-cell;">Mcgaughy</td><td style="display: table-cell;">Hemodialysis Technician</td><td style="display: table-cell;">11 Nov 1983</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-inverse">Disabled</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Lizzee</td><td style="display: table-cell;"><a href="javascript:void(0)">Goodlow</a></td><td style="display: table-cell;">Technical Services Librarian</td><td style="display: table-cell;">1 Nov 1961</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Judi</td><td style="display: table-cell;">Badgett</td><td style="display: table-cell;">Electrical Lineworker</td><td style="display: table-cell;">23 Jun 1981</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-success">Active</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Lauri</td><td style="display: table-cell;">Hyland</td><td style="display: table-cell;">Blackjack Supervisor</td><td style="display: table-cell;">15 Nov 1985</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr></tbody>
                            <tfoot><tr class="footable-paging"><td colspan="5"><div class="footable-pagination-wrapper"><ul class="pagination justify-content-center"><li class="footable-page-nav disabled" data-page="first"><a class="footable-page-link" href="#">«</a></li><li class="footable-page-nav disabled" data-page="prev"><a class="footable-page-link" href="#">‹</a></li><li class="footable-page visible active" data-page="1"><a class="footable-page-link" href="#">1</a></li><li class="footable-page visible" data-page="2"><a class="footable-page-link" href="#">2</a></li><li class="footable-page visible" data-page="3"><a class="footable-page-link" href="#">3</a></li><li class="footable-page-nav" data-page="next"><a class="footable-page-link" href="#">›</a></li><li class="footable-page-nav" data-page="last"><a class="footable-page-link" href="#">»</a></li></ul><div class="divider"></div><span class="label label-primary">1 of 3</span></div></td></tr></tfoot></table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="heading2">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#col2" aria-expanded="false" aria-controls="col2">
                        Collapsible Group Item #2
                    </button>
                </h5>
            </div>
            <div id="col2" class="collapse" aria-labelledby="heading2" data-parent="#accordionTable">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="demo-foo-accordion2" class="table table-bordered m-b-0 toggle-arrow-tiny footable footable-3 footable-filtering footable-filtering-right footable-paging footable-paging-center breakpoint-md" data-filtering="true" data-paging="true" data-sorting="true" style="">
                            <thead><tr class="footable-filtering"><th colspan="5"><form><div class="input-group footable-filtering-search"><label class="sr-only">Search</label><div class="input-group"><input type="text" class="form-control" placeholder="Search"><div class="input-group-append"><button type="button" class="btn btn-primary"><span class="fas fa-search"></span></button><button type="button" class="btn btn-default dropdown-toggle"><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right"><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> First Name </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> Last Name </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> Job Title </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> DOB </label></a></li><li class="dropdown-item"><a class="checkbox"><label><input type="checkbox" checked="checked"> Status </label></a></li></ul></div></div></div></form></th></tr>
                            <tr class="footable-filtering footable-header">





                                <th data-toggle="true" class="footable-sortable footable-first-visible" style="display: table-cell;"> First Name <span class="fas fa-sort"></span></th><th class="footable-sortable" style="display: table-cell;"> Last Name <span class="fas fa-sort"></span></th><th data-hide="phone" class="footable-sortable" style="display: table-cell;"> Job Title <span class="fas fa-sort"></span></th><th data-hide="all" class="footable-sortable" style="display: table-cell;"> DOB <span class="fas fa-sort"></span></th><th data-hide="all" class="footable-sortable footable-last-visible" style="display: table-cell;"> Status <span class="fas fa-sort"></span></th></tr>
                            </thead>
                            <tbody>






























                            <tr>





                                <td class="footable-first-visible" style="display: table-cell;">Isidra</td><td style="display: table-cell;">Boudreaux</td><td style="display: table-cell;">Traffic Court Referee</td><td style="display: table-cell;">22 Jun 1972</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-success">Active</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Shona</td><td style="display: table-cell;">Woldt</td><td style="display: table-cell;">Airline Transport Pilot</td><td style="display: table-cell;">3 Oct 1981</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-inverse">Disabled</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Granville</td><td style="display: table-cell;">Leonardo</td><td style="display: table-cell;">Business Services Sales Representative</td><td style="display: table-cell;">19 Apr 1969</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Easer</td><td style="display: table-cell;">Dragoo</td><td style="display: table-cell;">Drywall Stripper</td><td style="display: table-cell;">13 Dec 1977</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-success">Active</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Maple</td><td style="display: table-cell;">Halladay</td><td style="display: table-cell;">Aviation Tactical Readiness Officer</td><td style="display: table-cell;">30 Dec 1991</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Maxine</td><td style="display: table-cell;"><a href="javascript:void(0)">Woldt</a></td><td style="display: table-cell;"><a href="javascript:void(0)">Business Services Sales Representative</a></td><td style="display: table-cell;">17 Oct 1987</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-inverse">Disabled</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Lorraine</td><td style="display: table-cell;">Mcgaughy</td><td style="display: table-cell;">Hemodialysis Technician</td><td style="display: table-cell;">11 Nov 1983</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-inverse">Disabled</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Lizzee</td><td style="display: table-cell;"><a href="javascript:void(0)">Goodlow</a></td><td style="display: table-cell;">Technical Services Librarian</td><td style="display: table-cell;">1 Nov 1961</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Judi</td><td style="display: table-cell;">Badgett</td><td style="display: table-cell;">Electrical Lineworker</td><td style="display: table-cell;">23 Jun 1981</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-success">Active</span></td></tr><tr>





                                <td class="footable-first-visible" style="display: table-cell;">Lauri</td><td style="display: table-cell;">Hyland</td><td style="display: table-cell;">Blackjack Supervisor</td><td style="display: table-cell;">15 Nov 1985</td><td class="footable-last-visible" style="display: table-cell;"><span class="label label-table label-danger">Suspended</span></td></tr></tbody>
                            <tfoot><tr class="footable-paging"><td colspan="5"><div class="footable-pagination-wrapper"><ul class="pagination justify-content-center"><li class="footable-page-nav disabled" data-page="first"><a class="footable-page-link" href="#">«</a></li><li class="footable-page-nav disabled" data-page="prev"><a class="footable-page-link" href="#">‹</a></li><li class="footable-page visible active" data-page="1"><a class="footable-page-link" href="#">1</a></li><li class="footable-page visible" data-page="2"><a class="footable-page-link" href="#">2</a></li><li class="footable-page visible" data-page="3"><a class="footable-page-link" href="#">3</a></li><li class="footable-page-nav" data-page="next"><a class="footable-page-link" href="#">›</a></li><li class="footable-page-nav" data-page="last"><a class="footable-page-link" href="#">»</a></li></ul><div class="divider"></div><span class="label label-primary">1 of 3</span></div></td></tr></tfoot></table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection