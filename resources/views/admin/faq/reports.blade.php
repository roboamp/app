@extends('admin.list')


@section('list')

    <h1 class="text-info font-weight-bold pb-3 ml-2">FAQ</h1>

    <h1 class="font-weight-bold pb-3 ml-2">Reports</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Term</th>
            <th scope="col">Frequency</th>
        </tr>
        </thead>
        <tbody>
        @if(count($search_logs) > 0)
            @foreach($search_logs as $term)
                <tr>
                    <td>{{$term->term}}
                    <td>{{$term->count}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

@endsection

