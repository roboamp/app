@extends('admin.list')

@section('css')
    <style>
        #btn-delete, #btn-update {
            padding: 6px 32px;
        }
    </style>
@endsection

@section('list')

    @include('admin.faq.includes.messages')

    <div class="cat-messages"></div>

    <h1 class="text-info font-weight-bold pb-3 ml-2">FAQ</h1>

    <div class="d-flex justify-content-between">
        <h1 class="font-weight-bold pb-3 ml-2">Categories</h1>
        <button type="button" class="btn btn-info mb-3 ml-2" data-toggle="modal" data-target="#exampleModalCenter">
            Add Category
        </button>
    </div>

    <br>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Category</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @if(count($categories) > 0)
            @foreach($categories as $c)
                <tr class="deleted-{{$c->id}}">
                    <td id="update-{{$c->id}}">{{$c->name}}</td>
                    <td>
                        <div class="d-flex form-group deleted-{{$c->id}}">
                            <label for="category" class="sr-only">Category</label>
                            <input id="old_name_{{$c->id}}" value="{{$c->name}}"
                                   hidden>
                            <input type="text" class="form-control mr-1" id="new_name_{{$c->id}}" value="{{$c->name}}"
                                   required>
                            <div class="edit-btn ml-2" data-id="{{$c->id}}">
                                <button class="btn btn-info edit" id="btn-update"><i class="ti-pencil-alt"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        @if(! $c->question_count > 0)
                            <label for="category" class="sr-only">Category</label>
                            <div class="delete-btn" data-id="{{$c->id}}">
                                <button class="btn btn-danger" id="btn-delete"><i class="ti-trash"></i>
                                </button>
                            </div>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    <!-- Add A New Category Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <label for="category" class="sr-only">Category</label>
                <div class="modal-body text-center" data-id="{{$c->id}}">
                    <input type="text" class="form-control" name="name" id="category" placeholder="Add Category"
                           required>
                    <button type="button" class="store-btn btn btn-info mt-3" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('list_child_script_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection

@section('list_child_script')

    $('#exampleModalCenter').on('shown.bs.modal', function () {
    $('#category').trigger('focus');
    });

    $('.alert').delay(1800).fadeOut('slow');

    $('.store-btn').on("click", function (e) {

    $id = $(this).attr('data-id');
    $token = $('input[name="_token"]').val();

    $name = $("#category").val();

    $.post("/admin/categories/category/store", {
    _token: $token,
    name: $name
    })
    .done(function (data) {
    var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
    var $addCategory = $(`<tr class="deleted-${data.id}"><td id="update-${data.id}">${$name}</td><td><div class="d-flex form-group deleted-${data.id}"><label for="category" class="sr-only">Category</label><input id="old_name_${data.id}" value="${data.name}" hidden><input type="text" class="form-control mr-1" id="new_name_${data.id}" value="${data.name}" required><div class="edit-btn ml-2" data-id="${data.id}"><button class="btn btn-info edit" id="btn-update"><i class="ti-pencil-alt"></i></button></div></div></td><td><label for="category" class="sr-only">Category</label><div class="delete-btn" data-id="${data.id}"><button class="btn btn-danger" id="btn-delete"><i class="ti-trash"></i></button></div></td></tr>`);
    $('.cat-messages').append($success);
    $('table tbody').append($addCategory);
    $("#category").val("");

    setTimeout(function () {
    $success.remove();
    }, 1800)
    })
    .fail(function (data) {
    var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.errors.name}</div>`);

    $('.cat-messages').append($fail);
    setTimeout(function () {
    $fail.remove();
    }, 1800)
    });
    });

    {{--  Edit Ajax Request  --}}
    $(document).on('click', '.edit-btn', function (e) {

    $id = $(this).attr('data-id');
    $token = $('input[name="_token"]').val();

    $name = $("#new_name_" + $id).val();
    $oldName = $("#old_name_" + $id).val();

    if($name !== $oldName){
    $.post("/admin/categories/category/" + $id, {
    _token: $token,
    name: $name
    })
    .done(function (data) {
    var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
    $('#update-' + $id).text($name);
    $('.cat-messages').append($success);
    setTimeout(function () {
    $success.remove();
    }, 1800)
    })
    .fail(function (data) {
    var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.errors.name}</div>`);

    $('.cat-messages').append($fail);
    setTimeout(function () {
    $fail.remove();
    }, 1800)
    });
    }
    });

    {{--  Delete Ajax Request  --}}
    $(document).on('click','.delete-btn', function (e) {
    $id = $(this).attr('data-id');
    $token = $('input[name="_token"]').val();

    Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {

    $.post("/admin/categories/category/delete/" + $id, {
    _token: $token,
    id: $id
    })
    .done(function (data) {
    $('.deleted-' + $id).remove();
    var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
    $('.cat-messages').append($success);
    setTimeout(function () {
    $success.remove();
    }, 1800)
    })
    .fail(function (data) {
    var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
    $('.cat-messages').append($fail);
    setTimeout(function () {
    $fail.remove();
    }, 1800)
    });
    }
    })

    });
@endsection




























