@extends('admin.list')

@section('css')
    <style>
        #btn-delete {
            padding: 6px 32px;
        }
    </style>
@endsection
@section('list')

    @include('admin.faq.includes.messages')

    <div class="cat-messages"></div>

    <h1 class="text-info font-weight-bold pb-3 ml-2">FAQ</h1>

    <h1 class="font-weight-bold pb-3 ml-2">Pending Questions</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Question</th>
            <th scope="col">Respond</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @if(count($questions) > 0)
            @foreach($questions as $question)
                <tr id="deleted-{{$question->id}}">
                    <td>{{$question->user->name}}</td>
                    <td>{{$question->user->email}}</td>
                    <td>{{$question->question}}</td>
                    <td>
                        <form method="get" action="{{route('admin.faq.show', $question->id)}}">
                            @csrf
                            <button class="btn btn-info" id="btn-edit">Respond</button>
                        </form>
                    </td>
                    <td>
                        <label for="question" class="sr-only">Question</label>
                        <div class="delete-btn" data-id="{{$question->id}}">
                            <button class="btn btn-danger" id="btn-delete"><i class="ti-trash"></i></button>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

@endsection

@section('list_child_script_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection

@section('list_child_script')
    $('.alert').delay(1800).fadeOut('slow');
        $('.delete-btn').on("click", function (e) {
            $id = $(this).attr('data-id');
            $token = $('input[name="_token"]').val();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "/admin/list/question/delete/" + $id,
                        headers: {'X-CSRF-TOKEN': $token},
                        method: 'DELETE'
                    })
                        .done(function (data) {
                        var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
                        $('.cat-messages').append($success);
                        $('#deleted-' + $id).remove();
                        setTimeout(function () {
                        $success.remove();
                        }, 1800)
                        })
                        .fail(function (data) {
                        var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
                        $('.cat-messages').append($fail);
                        setTimeout(function () {
                        $fail.remove();
                        }, 1800)
                        });
                }
            })

        });
@endsection
