@extends('admin.list')

@section('css')

@endsection

@section('list')

    @include('admin.faq.includes.messages')

    <div class="cat-messages"></div>

    <h1 class="text-info font-weight-bold pb-3 ml-2">FAQ</h1>

    <div class="d-flex justify-content-between">
        <h1 class="font-weight-bold pb-3 ml-2">Respond | Edit</h1>
        <form method="get" action="{{url()->previous()}}">
            @csrf
            <button class="btn btn-info mb-2 ml-1" id="btn-delete">
                <i class="fas fa-arrow-circle-left"></i> Back
            </button>
        </form>
    </div>

    <div class="container shadow-sm p-3 mb-5 bg-white rounded">
        <div class="card shadow-sm p-3 mb-5 bg-white rounded">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <h3 id="question"><span class="font-weight-bold pb-3">Q:</span> {{$question->question}}</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 mb-3">
                    <h4 id="answer"><span class="font-weight-bold pb-3">A:</span> {{$question->answer}}</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <p id="privacy"><span class="font-weight-bold pb-3">Privacy:</span> {{$question->privacy}}</p>
                </div>

                <div class="col-md-4">
                    <p><span class="font-weight-bold pb-3">Created By:</span> {{$question->user->email}}</p>
                </div>
                <div class="col-md-2">
                    @foreach($categories as $category)
                        @if($category->id === $question->category_id)
                            <p id="category"><span class="font-weight-bold pb-3">Category:</span> {{$category->name}}
                            </p>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>


        <form method="post" action="{{route('admin.faq.answer_store', $question->id)}}">
            @csrf
            @method('patch')
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="question"><span class="font-weight-bold pb-3">Question</span></label>
                    <input type="text" class="form-control" name="question" id="question"
                           value="{{$question->question}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="answer"><span class="font-weight-bold pb-3">Answer</span></label>
                    @if($question->answer != null)
                        <input type="text" class="form-control" name="answer" id="answer" value="{{$question->answer}}"
                               required>
                    @else
                        <input type="text" class="form-control" name="answer" id="answer" value="{{old('answer')}}"
                               required>
                    @endif
                    @error('answer')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="privacy"><span class="font-weight-bold pb-3">Privacy</span></label>
                    <select id="privacy" name="privacy" multiple class="form-control">
                        @foreach($privacy as $p)
                            <option value="{{$p}}">{{$p}}</option>
                        @endforeach
                    </select>
                    @error('privacy')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="category_id"><span class="font-weight-bold pb-3">Category</span></label>
                    <select name="category_id" id="category_id" multiple class="form-control">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" {{$category->id==$question->category_id? "selected" : ""}}>
                                {{$category->name}}
                            </option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="d-flex">
                <label for="category" class="sr-only">Category</label>
                <div class="delete-btn" data-id="{{$question->id}}">
                    <button type="button" data-id="{{$question->id}}" class="btn btn-danger pb-3 btn-delete btn-lg"
                            id="btn-delete"><i
                                class="ti-trash mt-2"></i></button>
                </div>
                <div class="ml-3">
                    <button id="btn-respond" type="submit" class="btn btn-info btn-lg">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('list_child_script_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection

@section('list_child_script')
    $('.alert').delay(1800).fadeOut('slow');

    $('.btn-delete').on("click", function (e) {
            $id = $(this).attr('data-id');
            $token = $('input[name="_token"]').val();

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "/admin/list/question/delete/" + $id,
                        headers: {'X-CSRF-TOKEN': $token},
                        method: 'DELETE'
                    }).done(function (data) {
                        window.location.href = "/admin/list/pending";
                        })
                        .fail(function (data) {
                        alert(data)
                        var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
                        $('.cat-messages').append($fail);
                        setTimeout(function () {
                        $fail.remove();
                        }, 1800)
                        });
                }
            })

        });
@endsection
