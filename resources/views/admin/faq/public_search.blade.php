<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FAQ Search Results</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<body>

<h1 class="text-info font-weight-bold pb-3">FAQ</h1>

<div class="d-flex justify-content-between">
    <h1 class="font-weight-bold pb-3 ml-2">Search Results</h1>
    <form method="get" action="{{url()->previous()}}">
        @csrf
        <button class="btn btn-info mb-2 ml-1" id="btn-delete">
            <i class="fas fa-arrow-circle-left"></i> Back
        </button>
    </form>
</div>
@if(count($questions) > 0)
    <div class="d-flex flex-wrap">
        @foreach($questions as $question)
            @if($question->answer != null && $question->privacy == 'public')
                <div class="col-sm-6 ">
                    <div class="card shadow-sm p-3 mb-5 bg-white rounded">
                        <div class="card-body">
                            <h5 class="text-info card-title">Q: {{$question->question}}</h5>
                            <p class="card-subtitle">A: {{$question->answer}}</p>
                            <p class="card-subtitle">Asked By: {{$question->user->name}}</p>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>

@else
    <div class="row">
        <div class="col-sm-6">
            <div class="card shadow-sm p-3 mb-5 bg-white rounded">
                <div class="card-body">
                    <h2 class="card-title text-info font-weight-bold pb-3">No Results.</h2>
                </div>
            </div>
        </div>
    </div>
@endif

</body>
</html>