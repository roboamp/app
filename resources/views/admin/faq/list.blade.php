@extends('admin.list')

@section('css')
    <style>
        .feedback {
            color: white;
            padding: 10px 20px;
            border-radius: 4px;
        }

        #btn-create-question {
            position: fixed;
            top: 20%;
            right: 15%;
        }

        .sidebar {
            position: fixed;
            top: 30%;
            right: 35px;
            width: 100%;
        }

        .button {
            color: #000;
        }


        .current {
            border-radius: 5px;
            background-color: #03a9f3;
            color: #ffffff;
        }

        .category {
            color: black;
        }

        .current a {
            color: #fff;
        }

    </style>
@endsection

@section('list')

    @include('admin.faq.includes.messages')

    <h1 class="text-info font-weight-bold pb-3 ml-2">FAQ</h1>

    <div class="d-flex">

        @include('admin.faq.includes.category_list')

        <div class="col-sm-6 col-8 main" data-spy="scroll" data-target="#myScrollspy" data-offset="0">
            @if(! Auth::user()->is_admin())
                @foreach($categories as $category)
                    <div id="{{$category->name}}" class="sections">
                        <h1 class="font-weight-bold pb-3">{{$category->name}}</h1>
                        @foreach($questions as $item)
                            @foreach($item as $question)
                                @if($question->category_id==$category->id && $question->privacy == 'public')
                                    <div class="card shadow-sm p-3 mb-5 bg-white rounded">
                                        <div class="card-body">
                                            <h3 class="text-info card-title font-weight-bold">
                                                Q: {{ $question->question ?? "N/A"}}</h3>
                                            <h4 class="card-text">A: {{$question->answer??'N/A'}}</h4>
                                            <p class="card-subtitle">Asked by: {{$question->user->name??"N/A"}}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                @endforeach
            @endif

            @if(Auth::user()->is_admin())
                @foreach($categories as $category)
                    <div id="{{$category->name}}" class="sections">
                        <h1 class="font-weight-bold pb-3">{{$category->name}}</h1>
                        @foreach($questions as $item)
                            @foreach($item as $question)
                                @if($question->category_id==$category->id)
                                    <div class="card shadow-sm p-3 mb-5 bg-white rounded">
                                        <div class="card-body">
                                            <h3 class="text-info card-title font-weight-bold">
                                                Q: {{ $question->question ?? "N/A"}}</h3>
                                            <h4 class="card-text">A: {{$question->answer??'N/A'}}</h4>
                                            <p class="card-subtitle">Asked by: {{$question->user->name??"N/A"}}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    @include('admin.faq.includes.faq_modal')

@endsection

@section('list_child_script')

    $('#exampleModalCenter').on('shown.bs.modal', function() {
    $('#question').trigger('focus');
    });
    {{--    smooth scroll--}}
    $('a[href*="#"]')
    .click(function (event) {
    if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
    &&
    location.hostname == this.hostname
    ) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
    event.preventDefault();
    $('html, body').animate({
    scrollTop: target.offset().top - 80
    }, 1000, function () {
    var $target = $(target);
    $target.focus();
    if ($target.is(":focus")) {
    return false;
    } else {
    $target.focus();
    }
    });
    }
    }
    });

{{--    highlights current nav item--}}
{{--        $('.nav').on('click', '.nav-item', function() {--}}
{{--        $(this).toggleClass('current').siblings().removeClass('current');--}}
{{--        });--}}

    var $sections = $('.sections'),
    $li = $('.nav-categories');

    $(window).on('scroll', function(){

    var scrollPos = $(window).scrollTop();

    $sections.each(function() {
    var top = $(this).offset().top - 90;
    if (scrollPos >= top) {
    var $target = $li.eq($(this).index());
    $li.not($target).removeClass('current');
    $target.addClass('current');
    }
    })
    });


    $('.alert').alert();
    $('.alert').delay(1800).fadeOut('slow');

@endsection
