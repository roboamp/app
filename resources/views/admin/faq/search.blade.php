@extends('admin.list')

@section('list')

    <h1 class="text-info font-weight-bold pb-3">FAQ</h1>

    <div class="d-flex justify-content-between">
        <h1 class="font-weight-bold pb-3 ml-2">Search Results</h1>
        <form method="get" action="{{url()->previous()}}">
            @csrf
            <button class="btn btn-info mb-2 ml-1" id="btn-delete">
                <i class="fas fa-arrow-circle-left"></i> Back
            </button>
        </form>
    </div>
    @if(count($questions) > 0)
        <div class="d-flex flex-wrap">
            @foreach($questions as $question)
                @if($question->answer != null && $question->privacy == 'public')
                    <div class="col-sm-6 ">
                        <div class="card shadow-sm p-3 mb-5 bg-white rounded">
                            <div class="card-body">
                                <h5 class="text-info card-title">Q: {{$question->question}}</h5>
                                <p class="card-subtitle">A: {{$question->answer}}</p>
                                <p class="card-subtitle">Asked By: {{$question->user->name}}</p>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>

    @else
        <div class="row">
            <div class="col-sm-6">
                <div class="card shadow-sm p-3 mb-5 bg-white rounded">
                    <div class="card-body">
                        <h2 class="card-title text-info font-weight-bold pb-3">No Results.</h2>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section ('form_scripts_include')
@endsection

@section('scripts')
@endsection