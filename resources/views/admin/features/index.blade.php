@extends('admin.list')


@section('list')
    <div class="clearfix mb-3">
        <a href="{{ route('admin.features.create') }}" class="btn btn-primary float-right">Add a Feature</a>
    </div> 
    <div class="table-responsive mt-3">
        <table class="table table-striped" id="features-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($features as $feature)
                <tr id="feature-row-{{$feature->id}}">
                    <td>
                        <span class="name-{{ $feature->id }}">{{ $feature->name }}</span>
                        <input type="text" class="form-control d-none" value="{{ $feature->name }}" name="name-{{ $feature->id }}" id="name-{{ $feature->id }}" data-old="{{ $feature->name }}">
                    </td>
                    <td>
                        <span class="price-{{ $feature->id }}">{{ $feature->price }}</span>
                        <input type="text" class="form-control d-none" value="{{ $feature->price }}" name="price-{{ $feature->id }}" id="price-{{ $feature->id }}" data-old="{{ $feature->price }}">
                    </td>
                    <td>
                        <div class="actions-{{ $feature->id }}">
                            <button type="button" class="btn btn-info btn-sm edit" data-target="{{ $feature->id }}"><i class="ti-pencil-alt"></i></button>
                            <button type="button" class="btn btn-danger btn-sm destroy" data-target="{{ $feature->id }}"><i class="ti-trash"></i></button>
                        </div>
                        <div class="form-actions-{{ $feature->id }} d-none">
                            <button type="button" class="btn btn-success btn-sm success" data-target="{{ $feature->id }}"><i class="ti-check"></i></button>
                            <button type="button" class="btn btn-danger btn-sm cancel" data-target="{{ $feature->id }}"><i class="ti-na"></i></button>
                        </div>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Delete Feature</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    This action cannot be undone. Are you sure you want to continue?
                    @csrf
                    @method("DELETE")
                    <input type="hidden" id="deleteFeatureId" name="feature" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="confirm-delete">Understood, continue.</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>

        @if(count($features) <= 10 )
        $('#features-table').DataTable({
            searching: false,
            ordering:  false,
            paging: false
        });
        @else
        $('#features-table').DataTable();
        @endif

        $(document).on("click", ".edit", function(){
            let target = $(this).data("target");
            prepare_edit_form(target);
        });
        $(document).on("click", ".success", function(){
            let target = $(this).data("target");
            send_edit_form(target);
        });
        $(document).on("click", ".cancel", function(){
            let target = $(this).data("target");
            return_old_values(target);
            dismiss_form(target);
        });

        $(document).on("click", ".destroy", function(){
            let target = $(this).data("target");
            $("#deleteFeatureId").val(target);
            $('#destroyModal').modal({
                keyboard: false
            })
        });

        $(document).on("click", "#confirm-delete", function(){
            let feature = $("#deleteFeatureId").val();
            let csrf_token = $('meta[name="csrf-token"]').attr('content');
            let method = $('input[name="_method"]').val();
            $.post("features/destroy", { feature: feature, _token: csrf_token, _method : method }, function(data){
                if(data.success == true){
                    $('#destroyModal').modal('hide');
                    $('#features-table').DataTable().row("#feature-row-"+feature).remove().draw("false");
                    $.toast({
                        heading: 'Feature Deleted',
                        text: data.message,
                        position: 'top-right',
                        loaderBg:'#00c292',
                        icon: 'success',
                        hideAfter: 3000,
                        stack: false, 
                    });
                }else{
                    return_old_values(data.target)
                    dismiss_form(data.target);
                    $.toast({
                        heading: 'Feature Deleted',
                        text: data.message,
                        position: 'top-right',
                        loaderBg:'#e46a76',
                        icon: 'error',
                        hideAfter: 3000,
                        stack: false, 
                    });
                }
            });
        });

        function prepare_edit_form(target){
            $(".actions-"+target).addClass("d-none");
            $(".form-actions-"+target).removeClass("d-none");
            $(".name-"+target).addClass("d-none");
            $("#name-"+target).removeClass("d-none");
            $(".price-"+target).addClass("d-none");
            $("#price-"+target).removeClass("d-none");
        }

        function send_edit_form(target){
            let name = $("#name-"+target).val();
            let price = $("#price-"+target).val();
            let csrf_token = $('meta[name="csrf-token"]').attr('content');
            $.post( "features/update", { id: target, name: name, price: price, _token: csrf_token }, function( data ) {
                if(data.success == true){
                    $(".name-"+data.target).text(data.name);
                    $(".price-"+data.target).text(data.price);
                    dismiss_form(data.target);
                    $.toast({
                        heading: 'Feature Updated',
                        text: data.message,
                        position: 'top-right',
                        loaderBg:'#00c292',
                        icon: 'success',
                        hideAfter: 3000,
                        stack: false, 
                    });
                }else{
                    return_old_values(data.target)
                    dismiss_form(data.target);
                    $.toast({
                        heading: 'Feature Updated',
                        text: data.message,
                        position: 'top-right',
                        loaderBg:'#e46a76',
                        icon: 'error',
                        hideAfter: 3000,
                        stack: false, 
                    });
                }
            });
        }

        function dismiss_form(target){
            $(".actions-"+target).removeClass("d-none");
            $(".form-actions-"+target).addClass("d-none");
            $(".name-"+target).removeClass("d-none");
            $("#name-"+target).addClass("d-none");
            $(".price-"+target).removeClass("d-none");
            $("#price-"+target).addClass("d-none");
        }

        function return_old_values(target){
            let name = $("#name-"+target).data("old");
            let price = $("#price-"+target).data("old");
            $(".name-"+target).text(name);
            $("#name-"+target).val(name);
            $(".price-"+target).text(price);
            $("#price-"+target).val(price);
        }
    </script>
@endsection