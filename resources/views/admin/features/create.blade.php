@extends('admin.forms')

@section('css_form')
    <link href="{{asset('dashboard_assets/dist/css/pages/bootstrap-switch.css')}}" rel="stylesheet">
@endsection


@section('form_fields')
    <div class="form-group row">
        <div class="col-sm-6">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" required value="{{old("name")}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
        <div class="col-sm-6">
            <label for="price">price</label>
            <input type="text" class="form-control" id="price" name="price" required>
            @error('price')
            <p class="text-danger">The Price is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Price is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="description">Descripton</label>
            <textarea class="form-control" id="description" name="description" required></textarea>
            @error('name')
            <p class="text-danger">The Description is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Description is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
@endsection

@section('new_row')

@endsection

@section ('form_scripts_include')
    <script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('dashboard_assets/js/pages/templates/create.js')}}"></script>

@endsection

@section('form_script')
    $('[data-toggle="switch"]').bootstrapSwitch();

@endsection