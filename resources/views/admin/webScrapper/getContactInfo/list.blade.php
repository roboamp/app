@extends('admin.list')

@section('list')
<div class="table-responsive mt-3">
    <table class="table table-striped" id="customers-table">
        <thead>
            <tr>
                <th>URL</th>
                <th>Phones</th>
                <th>Emails</th>
                <th width="75px">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customers as $customer)
            <tr id="customer-{{$customer->id}}">
                <td><a href="{{$customer->url}}" target="_blank">{{$customer->url}}</a></td>
                <td class="phones">
                    @foreach($customer->phones as $phone)
                    <p>{{ $phone->phone }}</p>
                    @endforeach
                </td>
                <td class="emails">
                    @foreach($customer->emails as $email)
                    <p>{{ $email->email }}</p>
                    @endforeach
                </td>
                <td></td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
    
</div>
@endsection

@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>
    $("#reports-table > tbody > tr").on("click",function(e){
        window.location = '/admin/speedy/show/'+$(this).data('target');
    });

    @if(count($customers) <= 10 )
    $('#customers-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#customers-table').DataTable();
    @endif

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('42821be930c76bca6620', {
      cluster: 'us2'
    });

    var channel = pusher.subscribe('PotentialCustomer');
    channel.bind('App\\Events\\PotentialCustomerInfoUpdated', function(data) {
        if(data.message.phones){
            $.each(data.message.phones, function(index, value){
                $("#customer-"+data.message.customer).find( ".phones" ).append('<p>'+value.phone+'</p>');
            });
        }
        if(data.message.mails){
            $.each(data.message.mails, function(index, value){
                $("#customer-"+data.message.customer).find( ".emails" ).append('<p>'+value.email+'</p>');
            });
        }
        $.toast({
            heading: 'Potential Customer Info Updated',
            text: data.message.message,
            position: 'top-right',
            loaderBg:'#00c292',
            icon: 'success',
            hideAfter: 3000,
            stack: false, 
        });
        //$("#customer-"+message.customer).find( ".phones" ).append()
    });
    </script>
@endsection