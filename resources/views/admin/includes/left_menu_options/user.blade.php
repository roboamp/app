<li class="nav-small-cap">--- EXTRA COMPONENTS</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-gallery"></i>
        <span class="hide-menu">Page Layout</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="layout-single-column.html">1 Column</a>
        </li>
        <li>
            <a href="layout-fix-header.html">Fix header</a>
        </li>
        <li>
            <a href="layout-fix-sidebar.html">Fix sidebar</a>
        </li>
        <li>
            <a href="layout-fix-header-sidebar.html">Fixe header &amp; Sidebar</a>
        </li>
        <li>
            <a href="layout-boxed.html">Boxed Layout</a>
        </li>
        <li>
            <a href="layout-logo-center.html">Logo in Center</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-files"></i>
        <span class="hide-menu">Sample Pages
                                    <span class="badge badge-pill badge-info">25</span>
                                </span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="starter-kit.html">Starter Kit</a>
        </li>
        <li>
            <a href="pages-blank.html">Blank page</a>
        </li>
        <li>
            <a href="javascript:void(0)" class="has-arrow">Authentication
                <span class="badge badge-pill badge-success pull-right">6</span>
            </a>
            <ul aria-expanded="false" class="collapse">
                <li>
                    <a href="pages-login.html">Login 1</a>
                </li>
                <li>
                    <a href="pages-login-2.html">Login 2</a>
                </li>
                <li>
                    <a href="pages-register.html">Register</a>
                </li>
                <li>
                    <a href="pages-register2.html">Register 2</a>
                </li>
                <li>
                    <a href="pages-register3.html">Register 3</a>
                </li>
                <li>
                    <a href="pages-lockscreen.html">Lockscreen</a>
                </li>
                <li>
                    <a href="pages-recover-password.html">Recover password</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="pages-profile.html">Profile page</a>
        </li>
        <li>
            <a href="pages-animation.html">Animation</a>
        </li>
        <li>
            <a href="pages-fix-innersidebar.html">Sticky Left sidebar</a>
        </li>
        <li>
            <a href="pages-fix-inner-right-sidebar.html">Sticky Right sidebar</a>
        </li>
        <li>
            <a href="pages-invoice.html">Invoice</a>
        </li>
        <li>
            <a href="pages-treeview.html">Treeview</a>
        </li>
        <li>
            <a href="pages-utility-classes.html">Helper Classes</a>
        </li>
        <li>
            <a href="pages-search-result.html">Search result</a>
        </li>
        <li>
            <a href="pages-scroll.html">Scrollbar</a>
        </li>
        <li>
            <a href="pages-pricing.html">Pricing</a>
        </li>
        <li>
            <a href="pages-lightbox-popup.html">Lighbox popup</a>
        </li>
        <li>
            <a href="pages-gallery.html">Gallery</a>
        </li>
        <li>
            <a href="pages-faq.html">Faqs</a>
        </li>
        <li>
            <a href="javascript:void(0)" class="has-arrow">Error Pages</a>
            <ul aria-expanded="false" class="collapse">
                <li>
                    <a href="pages-error-400.html">400</a>
                </li>
                <li>
                    <a href="pages-error-403.html">403</a>
                </li>
                <li>
                    <a href="pages-error-404.html">404</a>
                </li>
                <li>
                    <a href="pages-error-500.html">500</a>
                </li>
                <li>
                    <a href="pages-error-503.html">503</a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-pie-chart"></i>
        <span class="hide-menu">Charts</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="chart-morris.html">Morris Chart</a>
        </li>
        <li>
            <a href="chart-chartist.html">Chartis Chart</a>
        </li>
        <li>
            <a href="chart-echart.html">Echarts</a>
        </li>
        <li>
            <a href="chart-flot.html">Flot Chart</a>
        </li>
        <li>
            <a href="chart-knob.html">Knob Chart</a>
        </li>
        <li>
            <a href="chart-chart-js.html">Chartjs</a>
        </li>
        <li>
            <a href="chart-sparkline.html">Sparkline Chart</a>
        </li>
        <li>
            <a href="chart-extra-chart.html">Extra chart</a>
        </li>
        <li>
            <a href="chart-peity.html">Peity Charts</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-light-bulb"></i>
        <span class="hide-menu">Icons</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="icon-material.html">Material Icons</a>
        </li>
        <li>
            <a href="icon-fontawesome.html">Fontawesome Icons</a>
        </li>
        <li>
            <a href="icon-themify.html">Themify Icons</a>
        </li>
        <li>
            <a href="icon-weather.html">Weather Icons</a>
        </li>
        <li>
            <a href="icon-simple-lineicon.html">Simple Line icons</a>
        </li>
        <li>
            <a href="icon-flag.html">Flag Icons</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-location-pin"></i>
        <span class="hide-menu">Maps</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="map-google.html">Google Maps</a>
        </li>
        <li>
            <a href="map-vector.html">Vector Maps</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-align-left"></i>
        <span class="hide-menu">Multi level dd</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="javascript:void(0)">item 1.1</a>
        </li>
        <li>
            <a href="javascript:void(0)">item 1.2</a>
        </li>
        <li>
            <a class="has-arrow" href="javascript:void(0)" aria-expanded="false">Menu 1.3</a>
            <ul aria-expanded="false" class="collapse">
                <li>
                    <a href="javascript:void(0)">item 1.3.1</a>
                </li>
                <li>
                    <a href="javascript:void(0)">item 1.3.2</a>
                </li>
                <li>
                    <a href="javascript:void(0)">item 1.3.3</a>
                </li>
                <li>
                    <a href="javascript:void(0)">item 1.3.4</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0)">item 1.4</a>
        </li>
    </ul>
</li>
<li class="nav-small-cap">--- SUPPORT</li>
<li>
    <a class="waves-effect waves-dark" href="../documentation/documentation.html" aria-expanded="false">
        <i class="far fa-circle text-danger"></i>
        <span class="hide-menu">Documentation</span>
    </a>
</li>
<li>
    <a class="waves-effect waves-dark" href="pages-login.html" aria-expanded="false">
        <i class="far fa-circle text-success"></i>
        <span class="hide-menu">Log Out</span>
    </a>
</li>
<li>
    <a class="waves-effect waves-dark" href="pages-faq.html" aria-expanded="false">
        <i class="far fa-circle text-info"></i>
        <span class="hide-menu">FAQs</span>
    </a>
</li>