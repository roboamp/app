<li class="nav-small-cap"><div class="text-center">ADMIN CONTROL PANEL</div> </li>

<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-email"></i>
        <span class="hide-menu">Inbox</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="#">Create</a>

        </li>
        <li>
            <a href="app-email-detail.html">Mailbox Detail</a>
        </li>
        <li>
            <a href="app-compose.html">Compose Mail</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-cup"></i>
        <span class="hide-menu">Calculator</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="{{route('admin.calculator.engagement')}}">Engagement</a>
        </li>
        <li>
            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                <span>Features</span>
            </a>
            <ul aria-expanded="false" class="collapse">
                <li>
                    <a href="{{route('admin.features.index')}}">List</a>
                </li>
                <li>
                    <a href="{{route('admin.features.create')}}">Create</a>
                </li>
            </ul>
        </li>
    </ul>
</li>

<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="icon-speedometer"></i>
        <span class="hide-menu">Properties</span>
    </a>
    <ul aria-expanded="false" class="collapse">

        <li>
            <a href="{{route('admin.properties.create')}}">Create</a>
        </li>
        <li>
            <a href="{{route('admin.properties.list')}}">List</a>
        </li>

    </ul>
</li>


<li>
    <a class="has-arrow waves-effect waves-dark" aria-expanded="false">
        <i class="ti-layout-grid2"></i>
        <span class="hide-menu">Templates</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="{{route('admin.properties.templates.create')}}">Create</a>
        </li>
        <li>
            <a href="{{route('admin.properties.templates.list')}}">List</a>
        </li>

    </ul>
</li>

<!-- Speedy-->

<li>
    <a class="has-arrow waves-effect waves-dark" aria-expanded="false" href="javascript:void(0)">
        <i class="ti-dashboard"></i>
        <span class="hide-menu">Speedy</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="{{route('admin.speedy.create')}}">URL Test</a>
        </li>

        <li>
            <a href="{{route('admin.speedy.reports')}}">Reports List</a>
        </li>
    </ul>
</li>


<!-- FAQ -->
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-help"></i>
        <span class="hide-menu">FAQ</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="{{route('admin.faq.pending')}}">Pending</a>
        </li>
        <li>
            <a href="{{route('admin.faq.admin_list')}}">List</a>
        </li>
        <li>
            <a href="{{route('admin.category.index')}}">Categories</a>
        </li>
        <li>
            <a href="{{route('admin.faq.reports')}}">Reports</a>
        </li>
        <li>
            <a href="{{route('admin.faq.preview')}}">Preview</a>
        </li>
    </ul>
</li>


<!-- TIMELINE -->

<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-timer"></i>
        <span class="hide-menu">Timeline</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <!-- for testing only -->
        <li>
            <a href="{{route('admin.timeline.preview',4)}}">Cheating Preview</a>
        </li>
        <li>
            <a href="{{route('admin.timeline.list')}}">List</a>
        </li>
        <li>
            <a href="{{route('admin.timeline.create')}}">Create</a>
        </li>
        <li>
            <a href="{{route('admin.status.index')}}">Statuses</a>
        </li>
    </ul>
</li>

<!-- Schedule Task -->
<li>
    <a class="has-arrow waves-effect waves-dark" aria-expanded="false">
        <i class="ti-calendar"></i>
        <span class="hide-menu">Schedule Tasks</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="{{route('admin.schedule.index')}}">List</a>
        </li>
        <li>
            <a href="{{route('admin.schedule.create')}}">Create</a>
        </li>
        <li>
            <a class="has-arrow waves-effect waves-dark" aria-expanded="false">Tasks</a>
            <ul aria-expanded="false" class="collapse">
                <li>
                    <a href="{{route('admin.activities.index')}}">List</a>
                </li>
                <li>my-channel
                    <a href="{{route('admin.activities.create')}}">Create</a>
                </li>
            </ul>
        </li>
    </ul>
</li>

{{-- Utilities --}}

<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-settings"></i>
        <span class="hide-menu">Utilities</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a class="waves-effect waves-dark" href="{{route('admin.pic.index')}}"  aria-expanded="false">
                <i class="ti-money"></i>
                <span class="hide-menu">PIC</span>
            </a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="{{route('admin.url.index')}}"  aria-expanded="false">
                <i class="ti-world"></i>
                <span class="hide-menu">URL</span>
            </a>
        </li>
    </ul>
</li>

<!-- Pull Schedule -->

<li>
    <a class="waves-effect waves-dark" href="{{route('admin.pullSchedule.index')}}"  aria-expanded="false">
        <i class="ti-agenda"></i>
        <span class="hide-menu">Pull Schedule</span>
    </a>
</li>

<!-- Usage Reports -->
<li>
    <a class="has-arrow waves-effect waves-dark" aria-expanded="false">
        <i class="ti-bar-chart"></i>
        <span class="hide-menu">Usage Reports</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="{{route('admin.buckets.index')}}" class="waves-effect waves-dark" aria-expanded="false">Price Buckets</a>
        </li>
        <li>
            <a href="{{route('admin.clients.index')}}" class="waves-effect waves-dark" aria-expanded="false">Clients</a>
        </li>
        <li>
            <a href="{{route('admin.usage_reports.index')}}" class="waves-effect waves-dark" aria-expanded="false">Reports</a>
        </li>
    </ul>
</li>


<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-layout"></i>
        <span class="hide-menu">Amp Generator</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                <span>Components</span>
            </a>
            <ul aria-expanded="false" class="collapse">
                <li>
                    <a href="{{route('admin.ampcomponents.index')}}">List</a>
                </li>
                <li>
                    <a href="{{route('admin.ampcomponents.create')}}">Create</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                <span>Templates</span>
            </a>
            <ul aria-expanded="false" class="collapse">
                <li>
                    <a href="{{route('admin.amptemplates.index')}}">List</a>
                </li>
                <li>
                    <a href="{{route('admin.amptemplates.create')}}">Create</a>
                </li>
            </ul>
        </li>
    </ul>
</li>

<li>
    <a class="has-arrow waves-effect waves-dark" aria-expanded="false">
        <i class="ti-magnet"></i>
        <span class="hide-menu">Web Scrapper</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="{{route('admin.web_scrapper.get_contact_info')}}">Get Contact Info</a>
        </li>
        <li>
            <a href="{{route('admin.web_scrapper.potential_client_list')}}">Potential Client List</a>
        </li>
    </ul>
</li>