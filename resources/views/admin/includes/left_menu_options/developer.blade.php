<?php $notification_class='badge badge-pill badge-cyan ml-auto'; ?>

<li class="nav-small-cap">--- COMPONENTS</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="icon-speedometer"></i>
        <span class="hide-menu">Sliders
            <!-- add validation to show when there are new components
            for each section -->
            <span class="{{(isset($new_sliders)?$notification_class:'')}}">{{$new_sliders ?? ''}}</span>
        </span>
    </a>
    <ul aria-expanded="false" class="collapse">

        <li>
            <a href="index2.html">Analytical</a>
        </li>
        <li>
            <a href="index3.html">Demographical</a>
        </li>
        <li>
            <a href="index4.html">Modern</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-layout-grid2"></i>
        <span class="hide-menu">Apps</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="app-calendar.html">Calendar</a>
        </li>
        <li>
            <a href="app-chat.html">Chat app</a>
        </li>
        <li>
            <a href="app-ticket.html">Support Ticket</a>
        </li>
        <li>
            <a href="app-contact.html">Contact / Employee</a>
        </li>
        <li>
            <a href="app-contact2.html">Contact Grid</a>
        </li>
        <li>
            <a href="app-contact-detail.html">Contact Detail</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-email"></i>
        <span class="hide-menu">Inbox</span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="app-email.html">Mailbox</a>
        </li>
        <li>
            <a href="app-email-detail.html">Mailbox Detail</a>
        </li>
        <li>
            <a href="app-compose.html">Compose Mail</a>
        </li>
    </ul>
</li>
<li>
    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
        <i class="ti-palette"></i>
        <span class="hide-menu">Ui Elements
                                    <span class="badge badge-pill badge-primary text-white ml-auto">25</span>
                                </span>
    </a>
    <ul aria-expanded="false" class="collapse">
        <li>
            <a href="ui-cards.html">Cards</a>
        </li>
        <li>
            <a href="ui-user-card.html">User Cards</a>
        </li>
        <li>
            <a href="ui-buttons.html">Buttons</a>
        </li>
        <li>
            <a href="ui-modals.html">Modals</a>
        </li>
        <li>
            <a href="ui-tab.html">Tab</a>
        </li>
        <li>
            <a href="ui-tooltip-popover.html">Tooltip &amp; Popover</a>
        </li>
        <li>
            <a href="ui-tooltip-stylish.html">Tooltip stylish</a>
        </li>
        <li>
            <a href="ui-sweetalert.html">Sweet Alert</a>
        </li>
        <li>
            <a href="ui-notification.html">Notification</a>
        </li>
        <li>
            <a href="ui-progressbar.html">Progressbar</a>
        </li>
        <li>
            <a href="ui-nestable.html">Nestable</a>
        </li>
        <li>
            <a href="ui-range-slider.html">Range slider</a>
        </li>
        <li>
            <a href="ui-timeline.html">Timeline</a>
        </li>
        <li>
            <a href="ui-typography.html">Typography</a>
        </li>
        <li>
            <a href="ui-horizontal-timeline.html">Horizontal Timeline</a>
        </li>
        <li>
            <a href="ui-session-timeout.html">Session Timeout</a>
        </li>
        <li>
            <a href="ui-session-ideal-timeout.html">Session Ideal Timeout</a>
        </li>
        <li>
            <a href="ui-bootstrap.html">Bootstrap Ui</a>
        </li>
        <li>
            <a href="ui-breadcrumb.html">Breadcrumb</a>
        </li>
        <li>
            <a href="ui-bootstrap-switch.html">Bootstrap Switch</a>
        </li>
        <li>
            <a href="ui-list-media.html">List Media</a>
        </li>
        <li>
            <a href="ui-ribbons.html">Ribbons</a>
        </li>
        <li>
            <a href="ui-grid.html">Grid</a>
        </li>
        <li>
            <a href="ui-carousel.html">Carousel</a>
        </li>
        <li>
            <a href="ui-date-paginator.html">Date-paginator</a>
        </li>
        <li>
            <a href="ui-dragable-portlet.html">Dragable Portlet</a>
        </li>
        <li><a href="ui-spinner.html">Spinner</a></li>
        <li><a href="ui-scrollspy.html">Scrollspy</a></li>
        <li><a href="ui-toasts.html">Toasts</a></li>
    </ul>
</li>