<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div>
                    @if(!is_null($user->photo_url))
                    <img src="{{asset('Axton/assets/images/users').'/'.$user->photo_url}}" alt="user-img" class="img-circle">
                    @endif
                </div>
                <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">{{$user->name}}
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu animated flipInY">
                        <!-- text-->
                        <!--<a href="javascript:void(0)" class="dropdown-item">
                            <i class="ti-user"></i> My Profile</a>
                        <!--
                        <a href="javascript:void(0)" class="dropdown-item">
                            <i class="ti-wallet"></i> My Balance</a>
                        <!--
                        <a href="javascript:void(0)" class="dropdown-item">
                            <i class="ti-email"></i> Inbox</a>
                        <!--
                        <div class="dropdown-divider"></div>
                        <!--
                        <a href="javascript:void(0)" class="dropdown-item">
                            <i class="ti-settings"></i> Account Setting</a>
                        <!--
                        <div class="dropdown-divider"></div>
                        -->
                        <a href="{{ route('admin.logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> Logout</a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        @include('admin.includes.left_sidebar_nav',['menu'=>$user->user_role->name])

    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->