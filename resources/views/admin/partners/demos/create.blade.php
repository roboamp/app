@extends('admin.forms')

@section('form_fields')
    <div class="form-row">




        <div class="col-md-6 mb-3">
            <label for="name">Company's Name</label>
            <input type="input" value='{{old("name")}}' class="form-control @error('name')alert-danger @enderror"
                   id="name" name="name" required placeholder="Name of the Company">
            @error('name')
            <p class="text-danger">The Company's Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Company's Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>



    </div>

    <div class="form-row">
        <div class="col-md-6 mb-3">

            <label for="txt_url_1">Target URL</label>
            <input type="url" placeholder="Enter the URL" value="{{old('txt_url_1')}}" class="form-control @error('txt_url_1')alert-danger @enderror" id="txt_url_1" name="txt_url_1" required>

            @error('txt_url_1')
            <p class="text-danger">{{$errors->first('txt_url_1')}}</p>
            @enderror
            <div class="invalid-tooltip">URL is Required</div>
            <div class="valid-tooltip">Looks good!</div>

        </div>
        <div class="col-md-6 mb-3">

            <label for="txt_url_2">Target URL</label>
            <input type="url" placeholder="Enter the URL" value="{{old('txt_url_2')}}" class="form-control @error('txt_url_2')alert-danger @enderror" id="txt_url_2" name="txt_url_2">

            @error('txt_url_2')
            <p class="text-danger">{{$errors->first('txt_url_2')}}</p>
            @enderror
            <div class="invalid-tooltip">URL is Required</div>
            <div class="valid-tooltip">Looks good!</div>

        </div>

    </div>
    <div class="form-row">
        <div class="col-md-6 mb-3">

            <label for="txt_url_3">Target URL</label>
            <input type="url" placeholder="Enter the URL" value="{{old('txt_url_3')}}" class="form-control @error('txt_url_3')alert-danger @enderror" id="txt_url_3" name="txt_url_3" >

            @error('txt_url_3')
            <p class="text-danger">{{$errors->first('txt_url_3')}}</p>
            @enderror


        </div>
        <div class="col-md-6 mb-3">

            <label for="txt_url_4">Target URL</label>
            <input type="url" placeholder="Enter the URL" value="{{old('txt_url_4')}}" class="form-control @error('txt_url_4')alert-danger @enderror" id="txt_url_4" name="txt_url_4" >

            @error('txt_url_4')
            <p class="text-danger">{{$errors->first('txt_url_4')}}</p>
            @enderror

        </div>

    </div>


@endsection

{{-- @section ('new_row')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Dynamic Form Fields</h4>
                    <div id="education_fields"></div>
                    <div class="row">
                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <input type="text" class="form-control" id="Schoolname" name="Schoolname[]" value="" placeholder="School name">
                            </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <input type="text" class="form-control" id="Major" name="Major[]" value="" placeholder="Major">
                            </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <input type="text" class="form-control" id="Degree" name="Degree[]" value="" placeholder="Degree">
                            </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="form-group">
                                <div class="input-group">
                                    <select class="form-control" id="educationDate" name="educationDate[]">
                                        <option value="">Date</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="btn btn-success" type="button" onclick="education_fields();">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection --}}

@section ('scripts')
    <script src="{{asset('dashboard_assets')}}/node_modules/dff/dff.js" type="text/javascript"></script>

@endsection

