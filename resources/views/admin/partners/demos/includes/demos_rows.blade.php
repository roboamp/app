<div class="card m-b-5">
    <div class="card-header" id="{{($item->id==1?"nana":"heading")}}">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#col_{{$item->id}}}}" aria-expanded="false" aria-controls="{{($item->id==1?"col22":"col2")}}">
                {{$item->name}}
            </button>
        </h5>

    </div>
    <div id="col_{{$item->id}}}}" class="collapse" aria-labelledby="heading" data-parent="#accordionTable">
        <div class="card-body">
            <div class="card-body">
                <h4 class="card-title">{{$card_name ?? ""}}</h4>
                <h6 class="card-subtitle"></h6>
                @isset($add_new_button)
                    <button type="button" class="btn btn-info btn-rounded m-t-10 float-right" data-toggle="modal" data-target="#add-contact">Add New Contact</button>
                    <!-- Add Contact Popup Model -->
                    <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Add New Contact</h4> </div>
                                <div class="modal-body">
                                    <from class="form-horizontal form-material">
                                        <div class="form-group">
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Type name"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Email"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Phone"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Designation"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Age"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Date of joining"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Salary"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>Upload Contact Image</span>
                                                    <input type="file" class="upload"> </div>
                                            </div>
                                        </div>
                                    </from>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Save</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                @endisset
                <div class="table-responsive">
                    <table id="demo-foo-addrow" class="table table-bordered m-t-30 table-hover contact-list"  data-paging="{{$data_paging ?? "true"}}" data-paging-size="{{ $data_paging_size ?? 5 }}">
                        <thead>
                        <tr>
                            @foreach($table_headers as $header)
                                <th>{{$header}}</th>
                            @endforeach


                        </tr>
                        </thead>
                        <tbody>
                        @foreach($item->pages as $page)
                            <tr data-href="{{route('admin.demo.row.info',$page->id)}}">
                                <td><a href="{{route('admin.demo.row.info',$page->id)}}"> {{$page->name}}</a></td>
                                @if($user->is_admin())
                                    <td><a href="{{route('admin.demo.row.info',$page->id)}}">{{$page->local_url}}</a></td>
                                @else
                                    <td><a href="{{route('admin.demo.row.info',$page->id)}}">{{$page->testing_url}}</a></td>
                                @endif

                                <td>
                                    <a href="{{route('admin.demo.row.info',$page->id)}}">
                                        <span class="{{$page->status->class}}">{{$page->status->name}}</span>
                                    </a>
                                </td>
                                <td>
                                    {{$page->eta}}
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>