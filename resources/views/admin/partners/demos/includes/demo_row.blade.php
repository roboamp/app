<div class="card m-b-5">
    @include('admin.demos.includes.demo_row_header')
    <div class="table-responsive">
        <table id="demo-foo-addrow" class="table table-bordered m-t-30 table-hover contact-list"  data-paging="{{$data_paging ?? "true"}}" data-paging-size="{{ $data_paging_size ?? 5 }}">
            <thead>
            <tr>
                @foreach($table_headers as $header)
                    <th>{{$header}}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($demos[0]->pages as $page)
                <tr>
                    <td>{{$page->name}}</td>
                    @if($user->is_admin())
                    <td>{{$page->local_url}}</td>
                    @else
                    <td>{{$page->testing_url}}</td>
                    @endif

                    <td>
                        <span class="{{$page->status->class}}">{{$page->status->name}}</span>
                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>

</div>