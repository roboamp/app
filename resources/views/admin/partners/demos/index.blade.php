@extends('admin.templates.list')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{$card_title ?? ""}}</h4>
                    <h6 class="card-subtitle">{{$card_subtitle ?? ""}}</h6>
                   {{--}} @if(count($demos)>1)
                        @foreach($demos as $item)
                            @include('admin.partners.demos.includes.demos_rows',['demo'=>$item])
                        @endforeach
                    @elseif (count($demos)>0)
                        @include('admin.partners.demos.includes.demo_row')
                    @endif
                    --}}

                </div>
            </div>
        </div>
    </div>




@endsection

