@extends('admin.forms')

@section('css_form')
    <link href="{{asset('dashboard_assets/dist/css/pages/bootstrap-switch.css')}}" rel="stylesheet">
@endsection


@section('form_fields')
    <div class="form-row">
        <div class="col-sm-6">
            <label for="name">Name</label>
            <input type="text" class="form-control @error('name')alert-danger @enderror" id="name" name="name" required value="{{old('name')}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Description</label>
            <textarea class="form-control @error('description')alert-danger @enderror" id="description" name="description">{{old('description')}}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Codebase</label>
            <textarea rows="10" class="form-control @error('codebase')alert-danger @enderror" id="codebase" name="codebase" required>{{old('codebase')}}</textarea>
            @error('codebase')
            <p class="text-danger">The Codebase is required{{$errors->first('codebase')}}</p>
            @enderror
            <div class="invalid-tooltip">The Codebase is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">Attributes</label>
        <div class="col-sm-10 text-right">
            <button type="button" id="add-attribute" class="btn btn-info">Add Attribute</button>
        </div>
    </div>
    <div id="component_attributes" data-number="1">
        <div class="form-group row">
            <div class="col-sm-2">
                <label>Name</label>
                <input type="text" class="form-control" id="attr_name[]" name="attr_name[]" placeholder="name" required>
            </div>
            <div class="col-sm-3">
                <label>Description</label>
                <input type="text" class="form-control" id="attr_description[]" name="attr_description[]" placeholder="Description">
            </div>
            <div class="col-sm-2">
                <label>Type</label>
                <select class="form-control type" name="attr_type[]" id="attr_type[]" data-target="1" required>
                    @foreach($attr_types as $type)
                    <option value='{{ $type->id }}'>{{ $type->name }}</option>;
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4 td-1 row">
                <label class="col-sm-12">values</label>
                <input type='text' class='form-control col-sm-6 boolean' id='val_1_1' name='val_1_1' data-target='1' placeholder='Value 1' required>
                <input type='text' class='form-control col-sm-6 boolean' id='val_2_1' name='val_2_1' data-target='1' placeholder='Value 2' required>
                <input type='hidden' name='value[]' id='value_1' value=''>
            </div>
            <div class="col-sm-1">
                <div class="actions-1">
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('new_row')

@endsection
@section ('form_scripts_include')
    <script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('dashboard_assets/js/pages/templates/create.js')}}"></script>

@endsection
<!-- code executed during init() main thread -->
@section('form_script')
    $('[data-toggle="switch"]').bootstrapSwitch();

    $("td > a").on("click",function(e){
        e.stopPropagation();
    });
    $("#add-attribute").on("click", function(){
        let n = ($('#component_attributes').data("number"));
        let new_attr = '<div class="form-group row">';
            new_attr +='<div class="col-sm-2">';
            new_attr +='<label>Name</label>';
            new_attr +='<input type="text" class="form-control" id="attr_name[]" name="attr_name[]" placeholder="name"  required>';
            new_attr +='</div>';
            new_attr +='<div class="col-sm-3">';
            new_attr +='<label>Description</label>';
            new_attr +='<input type="text" class="form-control" id="attr_description[]" name="attr_description[]" placeholder="Description">';
            new_attr +='</div>';
            new_attr +='<div class="col-sm-2">';
            new_attr +='<label>Type</label>';
            new_attr +='<select class="form-control type" name="attr_type[]" id="attr_type[]" data-target="'+ (n+1) +'" required>';
            @foreach($attr_types as $type)
            new_attr +="<option value='{{ $type->id }}'>{{ $type->name }}</option>";
            @endforeach
            new_attr +='</select>';
            new_attr +='</div>';
            new_attr +="<div class='col-sm-4 td-"+(n+1)+" row'>";
            new_attr +='<label class="col-sm-12">Values</label>';
            new_attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_1_"+ (n+1) +"' name='val_1_"+ (n+1) +"' data-target='"+ (n+1) +"' placeholder='Value 1' required>";
            new_attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_2_"+ (n+1) +"' name='val_2_"+ (n+1) +"' data-target='"+ (n+1) +"' placeholder='Value 2' required>";
            new_attr +="<input type='hidden' name='value[]' id='value_"+ (n+1) +"' value=''>";
            new_attr +="</div>";
            new_attr +='<div class="col-sm-1">';
            new_attr +='<div class="actions-'+(n+1)+'">';
            new_attr +='<button type="button" class="btn btn-danger btn-sm destroy" data-target="'+(n+1)+'"><i class="ti-trash"></i></button>';
            new_attr +='</div>';
            new_attr +='</div>';
            new_attr +='</div>';

        $("#component_attributes").append(new_attr);
        $('#component_attributes').data("number", n+1);
    });

    $(document).on("click", "button.destroy" , function() {
        let toerase = $(this).parent().parent().parent();
        let n = ($('#component_attributes').data("number"));
        $('#component_attributes').data("number", n-1);
        toerase.remove();
    });

    $(document).on("change", "select.type", changeValueType);

    function changeValueType(){
        let target = $(this).data("target");
        let value = $(this).children(":selected").val();
        let attr = "";
        if( value == 1){
            attr +="<div class='col-sm-4 td-"+target+" row'>";
            attr +='<label class="col-sm-12">Value</label>';
            attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_1_"+ target +"' name='val_1_"+ target +"' data-target='"+ target +"' placeholder='Value 1' required>";
            attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_2_"+ target +"' name='val_2_"+ target +"' data-target='"+ target +"' placeholder='Value 2' required>";
            attr +="<input type='hidden' name='value[]' id='value_"+ target +"' value=''>";
            attr +="</div>";
        }
        if( value == 2 ){
            attr +="<div class='col-sm-4 td-"+target+" row'>";
            attr +='<label class="col-sm-12">Values</label>';
            attr +="<input type='text' class='form-control col-sm-12 string' name='val_"+ target +"' id='val_"+ target +"' data-target='"+ target +"' value='' placeholder='Default Value' required>";
            attr +="<input type='hidden' name='value[]' id='value_"+ target +"' value=''>";
            attr +="</div>";
        }

        $(".td-"+target).replaceWith(attr);
    }

    $(document).on("change", "input.string", updateStringValue);

    function updateStringValue(){
        let target = $(this).data("target");
        let val = $("#val_"+target).val();
        let new_val = '{ "default" : "'+val+'" }';
        $("#value_"+target).val(new_val);
    }

    $(document).on("change", "input.boolean", updateBooleanValue);

    function updateBooleanValue(){
        let target = $(this).data("target");
        let val_1 = $("#val_1_"+target).val();
        let val_2 = $("#val_2_"+target).val();
        let val = '{ "options" : ["'+val_1+'", "'+val_2+'"]}';
        $("#value_"+target).val(val);
    }
@endsection