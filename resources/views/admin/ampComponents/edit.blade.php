@extends('admin.forms')

@section('css_form')
    <link href="{{asset('dashboard_assets/dist/css/pages/bootstrap-switch.css')}}" rel="stylesheet">
@endsection


@section('form_fields')
    <div class="form-row">
        <div class="col-sm-12">
            <label for="name">Name</label>
            <input type="text" class="form-control @error('name')alert-danger @enderror" " id="name" name="name" value="{{$component->name}}" required>
            <input type="hidden" class="form-control" id="id" name="id" value="{{$component->id}}" required>
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Description</label>
            <textarea class="form-control @error('description')alert-danger @enderror" id="description" name="description">{{$component->description}}</textarea>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <textarea rows="10" class="form-control" id="codebase" name="codebase">{{$component->codebase}}</textarea>
            <label for="name">Codebase</label>
            @error('codebase')
            <p class="text-danger">The Codebase is required{{$errors->first('codebase')}}</p>
            @enderror
            <div class="invalid-tooltip">The Codebase is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">Attributes</label>
        <div class="col-sm-10 text-right">
            <button type="button" id="add-attribute" class="btn btn-info">Add Attribute</button>
        </div>
    </div>
    <div id="component_attributes" data-number="{{$component->attributes->count()}}">
        @foreach ($component->attributes as $index => $attribute)
        @php $value = json_decode($attribute->value, true);@endphp
        <div class="form-group row attr-{{$attribute->id}}">
            <div class="col-sm-2">
                <label>Name</label>
                <input type="text" class="form-control" id="attr_name[]" name="attr_name[]" placeholder="name" value="{{$attribute->name}}" required>
                <input type="hidden" id="attr_id[]" name="attr_id[]" value="{{$attribute->id}}">
            </div>
            <div class="col-sm-3">
                <label>Description</label>
                <input type="text" class="form-control" id="attr_description[]" name="attr_description[]" placeholder="Description" value="{{$attribute->description}}">
            </div>
            <div class="col-sm-2">
                <label>Type</label>
                <select class="form-control type" name="attr_type[]" id="attr_type[]" data-target="{{$index}}">
                    @foreach($attr_types as $type)
                    <option value='{{ $type->id }}' @if($attribute->type_id == $type->id) selected @endif>{{ $type->name }}</option>;
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4 td-{{$index}} row">
                @if($attribute->type_id == 1)
                <label class="col-sm-12">values</label>
                @foreach ($value["options"] as $i => $val)
                <input type='text' class='form-control col-sm-6 boolean' id='val_1_{{$i+1}}' name='val_1_{{$i+1}}' data-target='{{$index}}' placeholder='Value {{$i+1}}' value="{{$val}}" required>
                @endforeach
                <input type='hidden' name='value[]' id='value_{{$index}}' value='{{$attribute->value}}'>
                @elseif($attribute->type_id == 2)
                <label class="col-sm-12">value</label>
                <input type='text' class='form-control col-sm-12 string' name='val_{{$index}}' id='val_{{$index}}' data-target='{{$index}}' value='{{$value["default"]}}' placeholder='Default Value' required>
                <input type='hidden' name='value[]' id='value_{{$index}}' value='{{$attribute->value}}'>
                @endif
            </div>
            <div class="col-sm-1">
                <div class="actions-{{$index}}">
                    <button type="button" class="btn btn-danger btn-sm delete" data-target="{{$attribute->id}}"><i class="ti-trash"></i></button>
                </div>
            </div>
        </div>
        @endforeach
    </div>

<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Attribute</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            <input type="hidden" id="deleteAttributeId" name="attribute" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary delete_attribute">Understood, continue.</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('new_row')

@endsection
@section ('form_scripts_include')
    <script src="{{asset('dashboard_assets/node_modules/bootstrap-switch/bootstrap-switch.js')}}"></script>
    <script src="{{asset('dashboard_assets/js/pages/templates/create.js')}}"></script>

@endsection


@section('form_script')
    $('[data-toggle="switch"]').bootstrapSwitch();

    $('.alert').alert();

    $(".delete").on("click", function(){
        let target = $(this).data("target");
        $("#deleteAttributeId").val(target);
        $('#destroyModal').modal({
            keyboard: false
        });
        $(".attr-alert-row").remove();
    });

    $(".delete_attribute").on("click", function(){
        let attribute = $("#deleteAttributeId").val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        $.post("{{route('admin.ampcomponents.attributes.destroy')}}", { attribute: attribute, _token: csrf_token }, function( data ) {
            $('#destroyModal').modal('hide');
            if(data.success){
                $('.attr-'+attribute).remove();
            }else{
                $('.attr-'+attribute).after('<div class="form-group row attr-alert-row" id="attr-alert-'+attribute+'"><div class="col-sm-12 alert alert-danger attr-alert" role="alert">'+data.message+'<button type="button" class="close close-attr-alert" data-dismiss="alert" aria-label="Close" data-target="'+attribute+'"><span aria-hidden="true">&times;</span></button></div></div>');
            }
        });
    });

    $(document).on('click', '.close-attr-alert', function () {
        let target = $(this).data("target");
        $("#attr-alert-"+target).remove();
    });

    $("#add-attribute").on("click", function(){
        let n = ($('#component_attributes').data("number"));
        let new_attr = '<div class="form-group row">';
            new_attr +='<div class="col-sm-2">';
            new_attr +='<label>Name</label>';
            new_attr +='<input type="text" class="form-control" id="attr_name[]" name="attr_name[]" placeholder="name" value="" required>';
            new_attr +='<input type="hidden" id="attr_id[]" name="attr_id[]" value="">';
            new_attr +='</div>';
            new_attr +='<div class="col-sm-3">';
            new_attr +='<label>Description</label>';
            new_attr +='<input type="text" class="form-control" id="attr_description[]" name="attr_description[]" placeholder="Description">';
            new_attr +='</div>';
            new_attr +='<div class="col-sm-2">';
            new_attr +='<label>Type</label>';
            new_attr +='<select class="form-control type" name="attr_type[]" id="attr_type[]" data-target="'+ (n+1) +'">';
            @foreach($attr_types as $type)
            new_attr +="<option value='{{ $type->id }}'>{{ $type->name }}</option>";
            @endforeach
            new_attr +='</select>';
            new_attr +='</div>';
            new_attr +="<div class='col-sm-4 td-"+(n+1)+" row'>";
            new_attr +='<label class="col-sm-12">Values</label>';
            new_attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_1_"+ (n+1) +"' name='val_1_"+ (n+1) +"' data-target='"+ (n+1) +"' placeholder='Value 1' required>";
            new_attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_2_"+ (n+1) +"' name='val_2_"+ (n+1) +"' data-target='"+ (n+1) +"' placeholder='Value 2' required>";
            new_attr +="<input type='hidden' name='value[]' id='value_"+ (n+1) +"' value=''>";
            new_attr +="</div>";
            new_attr +='<div class="col-sm-1">';
            new_attr +='<div class="actions-'+ (n+1) +'">';
            new_attr +='<button type="button" class="btn btn-danger btn-sm destroy" data-target="'+ (n+1) +'"><i class="ti-trash"></i></button>';
            new_attr +='</div>';
            new_attr +='</div>';
            new_attr +='</div>';

        $("#component_attributes").append(new_attr);
        $('#component_attributes').data("number", n+1);
    });

    $(document).on("click", "button.destroy" , function() {
        let toerase = $(this).parent().parent().parent();
        let n = ($('#component_attributes').data("number"));
        $('#component_attributes').data("number", n-1);
        toerase.remove();
    });

    $(document).on("change", "select.type", changeValueType);

    function changeValueType(){
        let target = $(this).data("target");
        let value = $(this).children(":selected").val();
        let attr = "";
        if( value == 1){
            attr +="<div class='col-sm-4 td-"+target+" row'>";
            attr +='<label class="col-sm-12">Values</label>';
            attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_1_"+ target +"' name='val_1_"+ target +"' data-target='"+ target +"' placeholder='Value 1' required>";
            attr +="<input type='text' class='form-control col-sm-6 boolean' id='val_2_"+ target +"' name='val_2_"+ target +"' data-target='"+ target +"' placeholder='Value 2' required>";
            attr +="<input type='hidden' name='value[]' id='value_"+ target +"' value=''>";
            attr +="</div>";
        }
        if( value == 2 ){
            attr +="<div class='col-sm-4 td-"+target+" row'>";
            attr +='<label class="col-sm-12">Value</label>';
            attr +="<input type='text' class='form-control col-sm-12 string' name='val_"+ target +"' id='val_"+ target +"' data-target='"+ target +"' value='' placeholder='Default Value' required>";
            attr +="<input type='hidden' name='value[]' id='value_"+ target +"' value=''>";
            attr +="</div>";
        }

        $(".td-"+target).replaceWith(attr);
    }

    $(document).on("change", "input.string", updateStringValue);

    function updateStringValue(){
        let target = $(this).data("target");
        let val = $("#val_"+target).val();
        let new_val = '{ "default" : "'+val+'" }';
        $("#value_"+target).val(new_val);
    }

    $(document).on("change", "input.boolean", updateBooleanValue);

    function updateBooleanValue(){
        let target = $(this).data("target");
        let val_1 = $("#val_1_"+target).val();
        let val_2 = $("#val_2_"+target).val();
        let val = '{ "options" : ["'+val_1+'", "'+val_2+'"]}';
        $("#value_"+target).val(val);
    }
@endsection