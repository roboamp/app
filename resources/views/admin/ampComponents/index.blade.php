@extends('admin.list')


@section('list')
<div class="clearfix">
    <a href="{{ route('admin.ampcomponents.create') }}" class="btn btn-primary float-right">Add a Component</a>
</div>
<div class="table-responsive mt-3">
    <table class="table table-striped" id="components-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($components as $component)
            <tr id="component-row-{{ $component->id }}">
                <td>
                    <span class="name-{{ $component->id }}">{{ $component->name }}</span>
                    <input type="text" class="form-control d-none" value="{{ $component->name }}" name="name-{{ $component->id }}" id="name-{{ $component->id }}" data-old="{{ $component->name }}">
                </td>
                <td>
                    <span class="price-{{ $component->id }}">{{ $component->description }}</span>
                    <input type="text" class="form-control d-none" value="{{ $component->description }}" name="description-{{ $component->id }}" id="description-{{ $component->id }}" data-old="{{ $component->description }}">
                </td>
                <td>
                    <div class="actions-{{ $component->id }}">
                        <form id="edit-component-{{$component->id}}" method="POST" action="{{route('admin.ampcomponents.edit')}}">
                            @csrf
                            <input type="hidden" value="{{$component->id}}" name="id" id="id">
                        </form>
                        <button type="button" class="btn btn-info btn-sm edit" data-target="{{ $component->id }}" onclick="document.getElementById('edit-component-{{$component->id}}').submit();"><i class="ti-pencil-alt"></i></button>
                        <button type="button" class="btn btn-danger btn-sm destroy" data-target="{{ $component->id }}"><i class="ti-trash"></i></button>
                    </div>
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>


<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Component</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            @csrf
            @method("DELETE")
            <input type="hidden" id="deleteComponentId" name="component" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="confirm-destroy">Understood, continue.</button>
        </div>
      </div>
    </div>
  </div>
@endsection


@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>

    @if(count($components) <= 10 )
    $('#components-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#components-table').DataTable();
    @endif

    $(document).on("click", ".destroy", function(){
        let target = $(this).data("target");
        $("#deleteComponentId").val(target);
        $('#destroyModal').modal({
            keyboard: false
        })
    });

    $(document).on("click", "#confirm-destroy", destroy_component);

    function destroy_component(){
        let component = $("#deleteComponentId").val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        let method = $('input[name="_method"]').val();
        $('#destroyModal').modal("hide");
        $.post( "{{route('admin.ampcomponents.destroy')}}", { component: component, _token: csrf_token, _method: method }, function( data ) {
            if(data.success == true){
                $('#components-table').DataTable().row("#component-row-"+data.target).remove().draw("false");
                $.toast({
                    heading: 'Component Deleted',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#00c292',
                    icon: 'success',
                    hideAfter: 3000,
                    stack: false, 
                });
            }else{
                $.toast({
                    heading: 'Component Deleted',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#e46a76',
                    icon: 'error',
                    hideAfter: 3000,
                    stack: false, 
                });
            }
        });
    }
    </script>
@endsection