@extends('admin.list')

@section('css')
    <style>
        .url-link {
            cursor: pointer;
        }

        .url-link:hover {
            background-color: #e6fcff;
        }
    </style>
@endsection

@section('list')

    <div class="messages"></div>
    <div class="cat-messages"></div>

    <h1 class="font-weight-bold pb-3 ml-2">URL's</h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">URL</th>
            <th scope="col">Customer Name</th>
        </tr>
        </thead>
        <tbody>
        @if(count($properties) > 0)

            @foreach($properties as $property)

                <tr class="url-link" data-href='{{route('admin.url.show', $property->id)}}'>
                    <td>
                        <a href="{{route('admin.url.show', $property->id)}}">{{$property->url}}</a>
                    </td>
                    <td>{{$property->customers->name ?? 'N/A'}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

@endsection

@section('list_child_script_include')
    <script>
        $(document).ready(function ($) {
            $(".url-link").click(function () {
                window.location = $(this).data("href");
            });
        });
    </script>
@endsection


