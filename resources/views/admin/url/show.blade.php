@extends('admin.list')

@section('css')
    <style>
        .url-link {
            cursor: pointer;
        }

        .url-link:hover {
            background-color: #cccccc;
        }

        .tooltip {
            position: relative;
            display: inline-block;
        }

        .tooltip .tooltiptext {
            visibility: hidden;
            width: 140px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px;
            position: absolute;
            z-index: 1;
            bottom: 150%;
            left: 50%;
            margin-left: -75px;
            opacity: 0;
            transition: opacity 0.3s;
        }

        .tooltip .tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
            opacity: 1;
        }
    </style>
@endsection

@section('content')

    <div class="messages"></div>
    <div class="cat-messages"></div>

    <h1 class="font-weight-bold pb-3 ml-2">URL's</h1>

    <div class="card">
        <div class="card-header">
            <h2>AMP Cache Demo Environment</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">URL</th>
                </tr>
                </thead>
                <tbody>
                @if(count($amp_cache_demo_env) > 0)
                    @foreach($amp_cache_demo_env as $url)

                        <tr>
                            <td class="d-flex justify-content-between">
                                <textarea class="amp_cache_demo_env" hidden>{{$url}}</textarea>
                                <a href="{{$url}}" target="_blank">{{$url}}</a>
                                <button data-clipboard-text="{{$url}}" type="button" class="btn btn-secondary" data-container="body"
                                        data-toggle="popover" data-placement="left" data-content="Copied!">
                                    <i class="ti-clipboard"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2>AMP Demo Environment</h2>
        </div>
        <div class="card-body">

            <table class="table">
                <thead>
                <tr>
                    <th scope="col">URL</th>
                </tr>
                </thead>
                <tbody>
                @if(count($urls) > 0)
                    @foreach($urls as $url)
                        <tr>
                            <td class="d-flex justify-content-between">
                                <textarea class="urls" hidden>{{$url}}</textarea>
                                <a href="{{$url}}" target="_blank">{{$url}}</a>
                                <button data-clipboard-text="{{$url}}" type="button" class="btn btn-secondary" data-container="body"
                                        data-toggle="popover" data-placement="left" data-content="Copied!">
                                    <i class="ti-clipboard"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2>Insight AMP Original</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">URL</th>
                </tr>
                </thead>
                <tbody>
                @if(count($insight_amp_original) > 0)

                    @foreach($insight_amp_original as $url)

                        <tr>
                            <td class="d-flex justify-content-between">
                                <textarea class="insight_amp_original" hidden>{{$url}}</textarea>
                                <a href="{{$url}}" target="_blank">{{$url}}</a>
                                <button data-clipboard-text="{{$url}}" type="button" class="btn btn-secondary" data-container="body"
                                        data-toggle="popover" data-placement="left" data-content="Copied!">
                                    <i class="ti-clipboard"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h2>Insight AMP Demo Version</h2>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">URL</th>
                </tr>
                </thead>
                <tbody>

                @if(count($insight_amp_version) > 0)
                    @foreach($insight_amp_version as $url)
                        <tr>
                            <td class="d-flex justify-content-between">
                                <textarea class="insight_amp_version" hidden>{{$url}}</textarea>
                                <a href="{{$url}}" target="_blank">{{$url}}</a>
                                <button data-clipboard-text="{{$url}}" type="button" class="btn btn-secondary" data-container="body"
                                        data-toggle="popover" data-placement="left" data-content="Copied!">
                                    <i class="ti-clipboard"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('list_child_script_include')
    <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.6/dist/clipboard.min.js"></script>
    <script>
        var clipboard = new ClipboardJS('.btn');

        $(function () {
            $(document).on('shown.bs.popover', function (e) {
                setTimeout(function () {
                    $(e.target).popover('hide');
                }, 1500);
            });
        });
    </script>
@endsection



