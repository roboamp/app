@extends('admin.forms')

@section('css_form')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.11.0/sweetalert2.css"/>
@endsection


@section('form_fields')
    @csrf
    @method('patch')
    <div class="cat-messages"></div>
    <h1>Edit Timeline</h1>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" required value="{{$timeline->name}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description"
                      required>{{$timeline->description}}</textarea>
            @error('name')
            <p class="text-danger">The Description is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Description is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>

    <div id="multiple_timeline_statuses" class="row">
        <div class="col-sm-12">
            {{--            edit existing input--}}
            @if(count($timeline->statuses()) > 0)
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="card-title">Edit Timeline Status</h4>
                        @foreach($timeline->statuses() as $status)
                            <div class="card deleted-{{$status->pivot->id}}">
                                <div class="card-body">
                                    @error('statuses')
                                    <p class="text-danger">{{$errors->first('statuses')}}</p>
                                    @enderror
                                    <div id="education_fields"></div>
                                    <div class="row">
                                        <div class="col-sm-6 nopadding">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <select class="form-control" id="dd_status_id"
                                                            name="dd_status_id[]">
                                                        @foreach($statuses as $s)
                                                            <option
                                                                value="{{$s->id}}" {{$s->id == $status->id ? 'selected' : ''}}>{{$s->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 nopadding">
                                            <div class="input-group">
                                                <input id="pivot_status_id" class="form-control"
                                                       name="pivot_status_id[]"
                                                       value="{{$status->pivot->id}}" type="number" hidden>
                                                <input type="text" class="form-control" id="dd_status_description"
                                                       name="dd_status_description[]"
                                                       value="{{$status->pivot->description}}"
                                                       required>
                                                <div class="input-group-append delete-btn"
                                                     data-id="{{$status->pivot->id}}">
                                                    <label for="status" class="sr-only">Status</label>
                                                    <button type="button" class="btn btn-danger " id="btn-delete"><i
                                                            class="fa fa-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif


            <h4 class="card-title">Add Timeline Status</h4>

            {{--                 old if errors input--}}
            @if(old('dd_status_description_add'))
                @for($i=0; $i<count(old('dd_status_description_add')); $i++)
                    <div class="card remove{{$i}}">
                        @error('statuses')
                        <p class="text-danger">{{$errors->first('statuses')}}</p>
                        @enderror
                        <div class="card-body">
                            <div id="education_fields"></div>
                            <div class="row">
                                <div class="col-sm-6 nopadding">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select class="form-control" id="dd_status_id" name="dd_status_id_add[]">
                                                @foreach($statuses as $s)
                                                    <option value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('message')
                                        <p class="text-danger m-1">{{$errors->first('message')}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6 nopadding">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="dd_status_description"
                                               value="{{old('dd_status_description_add.'.$i)}}"
                                               name="dd_status_description_add[]" placeholder="Description">
                                        <div class="input-group-append">
                                            @if($i==0)
                                                <button class="btn btn-success" onclick="education_fields();"
                                                        type="button">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            @else
                                                <button class="btn btn-danger" onclick="remove_fields({{$i}});"
                                                        type="button">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor
            @else
                {{--                  add new input--}}
                <div class="card">
                    @error('statuses')
                    <p class="text-danger">{{$errors->first('statuses')}}</p>
                    @enderror
                    <div class="card-body">
                        <div id="education_fields"></div>
                        <div class="row">
                            <div class="col-sm-6 nopadding">
                                <div class="form-group">
                                    <div class="input-group test">
                                        <select class="form-control dd_status_id" id="dd_status_id"
                                                name="dd_status_id_add[]">
                                            @foreach($statuses as $s)
                                                <option value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                            <option value="">Add a Status</option>
                                        </select>
                                    </div>
                                    @error('message')
                                    <p class="text-danger m-1">{{$errors->first('message')}}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6 nopadding">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="dd_status_description"
                                           name="dd_status_description_add[]"
                                           placeholder="Description">
                                    <div class="input-group-append">
                                        <button class="btn btn-success" type="button" onclick="education_fields();">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

@endsection

@section('new_row')

@endsection

@section ('form_scripts_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{asset('dashboard_assets/js/pages/timeline/update.js')}}"></script>

    <script>
        $('#exampleModalCenter').on('shown.bs.modal', function () {
            $('#status').trigger('focus');
        });

        function remove_fields(rid) {
            $('.remove' + rid).remove();
        }

        $('.dd_status_id').on("change", function (e) {

            if ($(this).children("option:selected").val() === '') {
                Swal.fire({
                    title: 'Add Status',
                    input: 'text',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Save'
                }).then((result) => {
                    if (result.isConfirmed) {

                        $token = $('input[name="_token"]').val();
                        $name = result.value;

                        $.post("/admin/statuses/status", {
                            _token: $token,
                            name: $name
                        })
                            .done(function (data) {
                                var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
                                var $option = $(`<option value="${data.id}" selected>${data.name}</option>`);
                                $('.cat-messages').append($success);
                                $('.dd_status_id').append($option)
                                $("#status").val("");

                                setTimeout(function () {
                                    $success.remove();
                                }, 1800)
                            })
                            .fail(function (data) {
                                var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.errors.name}</div>`);

                                $('.cat-messages').append($fail);
                                setTimeout(function () {
                                    $fail.remove();
                                }, 1800)
                            });
                    }
                })
            }
        });

        $('.delete-btn').on("click", function (e) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $id = $(this).attr('data-id');
                    $token = $('input[name="_token"]').val();

                    $.post("/admin/timeline/delete/" + $id, {
                        _token: $token,
                        id: $id
                    }).done(function (data) {
                        var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
                        $('.cat-messages').append($success);
                        setTimeout(function () {
                            $success.remove();
                        }, 1800)
                        $('.deleted-' + $id).remove();
                    })
                        .fail(function (data) {
                            var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
                            $('.cat-messages').append($fail);
                            setTimeout(function () {
                                $fail.remove();
                            }, 1800)
                        })
                }
            })

        });
    </script>
@endsection


