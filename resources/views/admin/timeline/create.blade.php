@extends('admin.forms')

@section('css_form')
@endsection


@section('form_fields')
    <div class="cat-messages"></div>
    <h1>Create Timeline</h1>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" required value="{{old("name")}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>

    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description"
                      required>{{old("description")}}</textarea>
            @error('name')
            <p class="text-danger">The Description is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Description is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>
    </div>

    <div id="multiple_timeline_statuses" class="row">
        <div class="col-md-12">
            <h4 class="card-title">Timeline Status</h4>
            @if(old('dd_status_description'))
                @for($i=0; $i<count(old('dd_status_description')); $i++)
                    <div class="card remove{{$i}}">
                        <div class="card-body">
                            @error('statuses')
                            <p class="text-danger">{{$errors->first('statuses')}}</p>
                            @enderror
                            <div id="education_fields"></div>
                            <div class="row">
                                <div class="col-sm-6 nopadding">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select class="form-control" id="dd_status_id" name="dd_status_id[]">
                                                @foreach($statuses as $status)
                                                    @if(old('dd_status_id'))
                                                        <option
                                                            value="{{$status->id}}" {{old('dd_status_id.'.$i) == $status->id ? 'selected' : ''}}>{{$status->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @error('message')
                                    <p class="text-danger">{{$errors->first('message')}}</p>
                                    @enderror
                                </div>
                                <div class="col-sm-6 nopadding">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="Degree" id="dd_status_description"
                                               name="dd_status_description[]"
                                               value="{{old('dd_status_description.'.$i)}}"
                                               placeholder="Description" required>
                                        <div class="input-group-append">
                                            @if($i==0)
                                                <button class="btn btn-success" onclick="education_fields();"
                                                        type="button">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            @else
                                                <button class="btn btn-danger" onclick="remove_fields({{$i}});"
                                                        type="button">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endfor

                        @else

                            <div class="card">
                                <div class="card-body">
                                    @error('statuses')
                                    <p class="text-danger">{{$errors->first('statuses')}}</p>
                                    @enderror
                                    <div id="education_fields"></div>
                                    <div class="row">
                                        <div class="col-sm-6 nopadding">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <select class="form-control dd_status_id" id="dd_status_id"
                                                            name="dd_status_id[]">
                                                        @foreach($statuses as $status)
                                                            <option value="{{$status->id}}">{{$status->name}}</option>
                                                        @endforeach
                                                            <option value="">Add a Status</option>
                                                    </select>
                                                </div>
                                            </div>
                                            @error('message')
                                            <p class="text-danger">{{$errors->first('message')}}</p>
                                            @enderror
                                        </div>
                                        <div class="col-sm-6 nopadding">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="Degree"
                                                       id="dd_status_description"
                                                       name="dd_status_description[]"
                                                       placeholder="Description" required>
                                                <div class="input-group-append">
                                                    <button class="btn btn-success" type="button"
                                                            onclick="education_fields();">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
        </div>

        @endsection

        @section('new_row')

        @endsection

        @section ('form_scripts_include')
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

            <script src="{{asset('dashboard_assets/js/pages/timeline/create.js')}}"></script>

            <script>
                function remove_fields(rid) {
                    $('.remove' + rid).remove();
                }


                $('.dd_status_id').on("change", function (e) {
                    if ($(this).children("option:selected").val() === '') {

                        Swal.fire({
                            title: 'Add Status',
                            input: 'text',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Save',
                        }).then((result) => {
                            if (result.isConfirmed) {

                                $token = $('input[name="_token"]').val();
                                $name = result.value;

                                $.post("/admin/statuses/status", {
                                    _token: $token,
                                    name: $name
                                })
                                    .done(function (data) {
                                        var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
                                        var $option = $(`<option value="${data.id}" selected>${data.name}</option>`);
                                        $('.cat-messages').append($success);
                                        $('.dd_status_id').append($option)
                                        $("#status").val("");

                                        setTimeout(function () {
                                            $success.remove();
                                        }, 1800)
                                    })
                                    .fail(function (data) {
                                        var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.errors.name}</div>`);

                                        $('.cat-messages').append($fail);
                                        setTimeout(function () {
                                            $fail.remove();
                                        }, 1800)
                                    });
                            }
                        })
                    }
                });
            </script>
@endsection

@section('form_script')

@endsection
