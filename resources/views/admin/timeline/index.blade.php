@extends('admin.list')

@section('css_list')
    <link href="{{asset('dashboard_assets')}}/css/pages/timeline-vertical-horizontal.css" rel="stylesheet">
@endsection

@section('list')
    <h1 class="text-center">{{$timeline->name}}</h1>
    <h4 class="text-center">{{$timeline->description}}</h4>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <ul class="timeline">
                        <?php $i = 0 ?>
                        @foreach($timeline->statuses() as $status)
                            <li class="{{$i % 2 == 0 ? '' : 'timeline-inverted'}}">

                                <div class="timeline-badge info">
                                    <i class="{{$i % 2 == 0 ? 'far fa-arrow-alt-circle-left' : 'far fa-arrow-alt-circle-right'}}"></i>
                                </div>
                                <div class="timeline-panel">
                                    <div class="timeline-body d-flex justify-content-between m-2">
                                        <div class="mx-auto">
                                            <h2 class="text-center">{{$status->name}}</h2>
                                            <p>{{$status->pivot->description}}</p>
                                        </div>
                                        @if($active_checkpoint_id ?? '')
                                            @if($status->id==$active_checkpoint_id)
                                                <div>
                                                    <span class="label label-success">Active</span>
                                                </div>
                                            @endif
                                        @endif
                                        <?php $i++ ?>
                                    </div>
                                    @endforeach
                                </div>
                            </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
