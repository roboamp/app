@extends('admin.list')

@section('css')
    <style>
        #btn-delete, #btn-update {
            padding: 6px 32px;
        }
    </style>
@endsection

@section('list')

    @include('admin.faq.includes.messages')

    <div class="cat-messages"></div>

    <div class="d-flex justify-content-between">
        <h1 class="font-weight-bold pb-3 ml-2">Statuses</h1>
        <button type="button" class="btn btn-info mb-3 ml-2" data-toggle="modal" data-target="#exampleModalCenter">
            Add Status
        </button>
    </div>

    <br>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Status</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @if(count($statuses) > 0)
            @foreach($statuses as $s)
                <tr class="deleted-{{$s->id}}">
                    <td id="update-{{$s->id}}">{{$s->name}}</td>
                    <td>
                        <div class="d-flex form-group deleted-{{$s->id}}">
                            <label for="status" class="sr-only">Status</label>
                            <input type="text" id="old_name_{{$s->id}}" value="{{$s->name}}" hidden>
                            <input type="text" class="form-control mr-1" id="new_name_{{$s->id}}" value="{{$s->name}}"
                                   required>
                            <div class="edit-btn ml-2" data-id="{{$s->id}}">
                                <button class="btn btn-info edit" id="btn-update"><i class="ti-pencil-alt"></i></button>
                            </div>
                        </div>
                    </td>
                    <td>
                        @if(!$s->timelines_count > 0)
                            <label for="status" class="sr-only">Status</label>
                            <div class="delete-btn" data-id="{{$s->id}}">
                                <button class="btn btn-danger" id="btn-delete"><i class="ti-trash"></i>
                                </button>
                            </div>
                        @endif
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>

    <!-- Add A New Status Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" data-id="{{$s->id}}">
                    <label for="status" class="sr-only">Status</label>
                        <input type="text" class="form-control" name="name" id="status" placeholder="Add Status"
                               required>
                        <button type="button" class="store-btn btn btn-info mt-3" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('list_child_script_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection

@section('list_child_script')

    $('#exampleModalCenter').on('shown.bs.modal', function() {
    $('#status').trigger('focus');
    });

    $('.alert').delay(1800).fadeOut('slow');

    $('.store-btn').on("click", function (e) {

    $id = $(this).attr('data-id');
    $token = $('input[name="_token"]').val();

    $name = $("#status").val();

    $.post("/admin/statuses/status", {
    _token: $token,
    name: $name
    })
    .done(function (data) {
    var $addCategory = $(`<tr class="deleted-${data.id}"><td id="update-${data.id}">${$name}</td><td><div class="d-flex form-group deleted-${data.id}"><label for="status" class="sr-only">Status</label><input id="old_name_${data.id}" value="${data.name}" hidden><input type="text" class="form-control mr-1" id="new_name_${data.id}" value="${data.name}" required><div class="edit-btn ml-2" data-id="${data.id}"><button class="btn btn-info edit" id="btn-update"><i class="ti-pencil-alt"></i></button></div></div></td><td><label for="status" class="sr-only">Status</label><div class="delete-btn" data-id="${data.id}"><button class="btn btn-danger" id="btn-delete"><i class="ti-trash"></i></button></div></td></tr>`);

    var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);

    $('.cat-messages').append($success);
    $('table tbody').append($addCategory);
    $("#status").val("");

    setTimeout(function () {
    $success.remove();
    }, 1800)
    })
    .fail(function (data) {
    var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.errors.name}</div>`);

    $('.cat-messages').append($fail);
    setTimeout(function () {
    $fail.remove();
    }, 1800)
    });
    });

    {{--  Edit Ajax Request  --}}
    $(document).on('click', '.edit-btn', function (e) {

    $id = $(this).attr('data-id');
    $token = $('input[name="_token"]').val();

    $name = $("#new_name_" + $id).val();
    $oldName = $("#old_name_" + $id).val();

    if($name !== $oldName){
    $.post("/admin/statuses/status/" + $id, {
    _token: $token,
    name: $name
    })
    .done(function (data) {
    var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
    $('#update-' + $id).text($name);
    $('.cat-messages').append($success);
    setTimeout(function() {
    $success.remove();
    }, 1800)
    })
    .fail(function (data) {
    var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.errors.name}</div>`);
    $('.cat-messages').append($fail);
    setTimeout(function() {
    $fail.remove();
    }, 1800)
    });
    }
    });

        $(document).on('click', '.delete-btn', function (e) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $id = $(this).attr('data-id');
                    $token = $('input[name="_token"]').val();

                    $.post("/admin/statuses/status/delete/" + $id, {
                        _token: $token,
                        id: $id
                    }).done(function (data) {
                        var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
                        $('.cat-messages').append($success);
                        setTimeout(function () {
                        $success.remove();
                        }, 1800)
                        $('.deleted-' + $id).remove();
                        })
                        .fail(function (data) {
                        var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
                        $('.cat-messages').append($fail);
                        setTimeout(function () {
                        $fail.remove();
                        }, 1800)
                        })
                }
            })
    });
@endsection


























