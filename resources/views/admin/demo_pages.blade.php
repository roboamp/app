@extends('admin.tables')

@section('css_table')
    @isset($hide_table_label)
        <style>div.footable-pagination-wrapper>span.label{display: none;}</style>
    @endisset
@endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{$card_name ?? ""}}</h4>
                    <h6 class="card-subtitle"></h6>
                    @isset($add_new_button)
                    <button type="button" class="btn btn-info btn-rounded m-t-10 float-right" data-toggle="modal" data-target="#add-contact">Add New Contact</button>
                    <!-- Add Contact Popup Model -->
                    <div id="add-contact" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Add New Contact</h4> </div>
                                <div class="modal-body">
                                    <from class="form-horizontal form-material">
                                        <div class="form-group">
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Type name"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Email"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Phone"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Designation"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Age"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Date of joining"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <input type="text" class="form-control" placeholder="Salary"> </div>
                                            <div class="col-md-12 m-b-20">
                                                <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>Upload Contact Image</span>
                                                    <input type="file" class="upload"> </div>
                                            </div>
                                        </div>
                                    </from>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Save</button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    @endisset
                        <div class="table-responsive">
                        <table id="demo-foo-addrow" class="table table-bordered m-t-30 table-hover contact-list"  data-paging="{{$data_paging ?? "true"}}" data-paging-size="{{ $data_paging_size ?? 5 }}">
                            <thead>
                            <tr>
                                <th>Page Name</th>
                                <th>Local URL</th>
                                <th>Testing URL</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <a href="javascript:void(0)"><img src="../assets/images/users/4.jpg" alt="user" width="40" class="img-circle" /> Genelia Deshmukh</a>
                                </td>
                                <td>genelia@gmail.com</td>
                                <td>+123 456 789</td>
                                <td><span class="label label-danger">Designer</span> </td>
                                <td>23</td>
                                <td>12-10-2014</td>
                                <td>$1200</td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


