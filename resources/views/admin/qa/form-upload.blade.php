@extends('admin.dashboard')
@section('css')
    <link rel="stylesheet" href="{{asset('Axton/assets/node_modules/dropify/dist/css/dropify.min.css')}}">

@endsection
@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Upload</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Uploads</li>
            </ol>
            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">File Upload</h4>
                                <label for="input-file-now">Upload a ZIP file with all the HTMLs files inside of it</label>
                                <form method="POST" action="{{route('qa.upload.post')}}" enctype="multipart/form-data">
                                    @csrf

                                    <div class="form-group">
                                        <label for="select2" class="col-sm-2 control-label">Native select</label>
                                        <div class="col-sm-12">
                                            <select id="select2" class="form-control" required>
                                                <option selected disabled hidden value="">aa</option>
                                                <option value="1">Mustard</option>
                                                <option value="2">Ketchup</option>
                                                <option value="3">Barbecue</option>
                                            </select>




                                        </div>
                                    </div>

                                    <input type="file" id="dev" name="dev" class="dropify" data-height="500" data-allowed-file-extensions="zip" data-max-file-size="10M" required />
                                    <button type="submit">SUBMIT FORM</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>


@endsection

    @section('scripts')
    <!-- ============================================================== -->
    <!-- Plugins for this page -->
    <!-- ============================================================== -->
    <!-- jQuery file upload -->
    <script src="{{asset('Axton/assets/node_modules/dropify/dist/js/dropify.min.js')}}"></script>
    <script>
    $(document).ready(function() {

        // Used events
        var drEvent = $('#dev').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#dev').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
    @endsection