@extends('admin.forms')

@section('form_fields')
    @csrf
    @method('patch')
    <div class="cat-messages"></div>
    <h1>Edit Project</h1>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Project Name</label>
            <input type="text" class="form-control" id="name" name="name" required value="{{$dev_project->name}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>

    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="description">Assign Contractor</label>
            <select class="form-control" id="dev_contractor_id"
                    name="dev_contractor_id">
                <option value="" selected>No Contractor</option>
                @foreach($dev_contractors as $contractor)
                    <option
                        value="{{$contractor->id}}" {{$contractor->id == $dev_project->dev_contractor_id ? 'selected' : ''}}>{{$contractor->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @if(count($dev_pages) > 0)
                <h4 class="card-title">Edit Pages</h4>
                @foreach($dev_pages as $page)
                    <div class="card deleted-{{$page->id}}">
                        <div class="card-body">
                            @error('name')
                            <p class="text-danger">{{$errors->first('name')}}</p>
                            @enderror
                            <div id="education_fields"></div>
                            <div class="row">
                                <div class="col-sm-6 nopadding">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input id="page_id" class="form-control" name="page_id[]"
                                                   value="{{$page->id}}" type="number" hidden>
                                            <input type="text" class="form-control" id="page_name_update"
                                                   name="page_name_update[]" value="{{$page->name}}"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 nopadding">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="page_url"
                                               name="page_url_update[]" value="{{$page->url}}"
                                               required>
                                        <div class="input-group-append delete-btn" data-id="{{$page->id}}">
                                            <label for="status" class="sr-only">Pages</label>
                                            <button type="button" class="btn btn-danger " id="btn-delete"><i
                                                    class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>

    <div id="multiple_timeline_statuses" class="row">
        <div class="col-md-12">
            <h4 class="card-title">Add Pages</h4>

            <div class="card">
                <div class="card-body">
                    <div id="education_fields"></div>
                    <div class="row">
                        <div class="col-sm-6 nopadding">
                            <div class="input-group">
                                <input type="text" class="form-control" id="Degree"
                                       id="page_name"
                                       name="page_name[]"
                                       placeholder="name">
                            </div>
                        </div>
                        <div class="col-sm-6 nopadding">
                            <div class="input-group">
                                <input type="text" class="form-control" id="Degree"
                                       id="page_url"
                                       name="page_url[]"
                                       placeholder="URL">
                                <div class="input-group-append">
                                    <button class="btn btn-success" type="button"
                                            onclick="education_fields();">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section ('form_scripts_include')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{asset('dashboard_assets/js/pages/devQA/create.js')}}"></script>

    <script>
        function remove_fields(rid) {
            $('.remove' + rid).remove();
        }

        $('.delete-btn').on("click", function (e) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $id = $(this).attr('data-id');
                    $token = $('input[name="_token"]').val();

                    $.post("/admin/devQA/delete/" + $id, {
                        _token: $token,
                        id: $id
                    }).done(function (data) {
                        var $success = $(`<div class='alert alert-success alert-dismissible fade show text-center' role='alert'>${data.success}</div>`);
                        $('.cat-messages').append($success);
                        setTimeout(function () {
                            $success.remove();
                        }, 1800)
                        $('.deleted-' + $id).remove();
                    })
                        .fail(function (data) {
                            var $fail = $(`<div class='alert alert-danger alert-dismissible fade show text-center' role='alert'>${data.responseJSON.error}</div>`);
                            $('.cat-messages').append($fail);
                            setTimeout(function () {
                                $fail.remove();
                            }, 1800)
                        })
                }
            })

        });
    </script>
@endsection

