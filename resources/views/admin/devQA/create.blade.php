@extends('admin.forms')

@section('form_fields')
    <h1>Create Project</h1>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="name">Project Name</label>
            <input type="text" class="form-control" id="name" name="name" required value="{{old("name")}}">
            @error('name')
            <p class="text-danger">The Name is required{{$errors->first('name')}}</p>
            @enderror
            <div class="invalid-tooltip">The Name is required</div>
            <div class="valid-tooltip">Looks good!</div>
        </div>

    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <label for="description">Assign Contractor</label>
            <select class="form-control" id="dev_contractor_id"
                    name="dev_contractor_id">
                <option value="" selected>No Contractor</option>
            @foreach($dev_contractors as $contractor)
                    <option value="{{$contractor->id}}">{{$contractor->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div id="multiple_timeline_statuses" class="row">
        <div class="col-md-12">
            <h4 class="card-title">Project Pages</h4>
            @if(old('dd_status_description'))
                @for($i=0; $i<count(old('dd_status_description')); $i++)
                    <div class="card remove{{$i}}">
                        <div class="card-body">
                            @error('statuses')
                            <p class="text-danger">{{$errors->first('statuses')}}</p>
                            @enderror
                            <div id="education_fields"></div>
                            <div class="row">
                                <div class="col-sm-6 nopadding">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select class="form-control" id="dd_status_id" name="dd_status_id[]">
                                                @foreach($dev_contractors as $contractor)
                                                    @if(old('dd_status_id'))
                                                        <option value="{{$contractor->id}}" {{old('dd_status_id.'.$i) == $contractor->id ? 'selected' : ''}}>{{$contractor->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @error('message')
                                    <p class="text-danger">{{$errors->first('message')}}</p>
                                    @enderror
                                </div>
                                <div class="col-sm-6 nopadding">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="Degree" id="dd_status_description"
                                               name="dd_status_description[]"
                                               value="{{old('dd_status_description.'.$i)}}"
                                               placeholder="Description" required>
                                        <div class="input-group-append">
                                            @if($i==0)
                                                <button class="btn btn-success" onclick="education_fields();"
                                                        type="button">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            @else
                                                <button class="btn btn-danger" onclick="remove_fields({{$i}});"
                                                        type="button">
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endfor

                        @else

                            <div class="card">
                                <div class="card-body">
                                    {{--                                    @error('page_name')--}}
                                    {{--                                    <p class="text-danger">{{$errors->first('page_name')}}</p>--}}
                                    {{--                                    @enderror--}}
                                    <div id="education_fields"></div>
                                    <div class="row">
                                        <div class="col-sm-6 nopadding">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="Degree"
                                                       id="page_name"
                                                       name="page_name[]"
                                                       placeholder="name" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 nopadding">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="Degree"
                                                       id="page_url"
                                                       name="page_url[]"
                                                       placeholder="URL" required>
                                                <div class="input-group-append">
                                                    <button class="btn btn-success" type="button"
                                                            onclick="education_fields();">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
        </div>

        @endsection


        @section ('form_scripts_include')
            <script src="{{asset('dashboard_assets/js/pages/devQA/create.js')}}"></script>

            <script>
                function remove_fields(rid) {
                    $('.remove' + rid).remove();
                }
            </script>
@endsection
