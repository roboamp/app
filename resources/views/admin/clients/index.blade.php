@extends('admin.list')


@section('list')
<div class="table-responsive mt-3">
    <table class="table table-striped clickable-table" id="clients-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Client Of</th>
                <th>Properties</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($clients as $client)
            <tr data-target="{{ $client->id }}">
                <td>{{ $client->name }}</td>
                <td>{{ $client->user->name }}</td>
                <td>
                    @foreach($client->properties as $property)
                    <p class="mb-0">{{$property->domain}}</p>
                    @endforeach
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>


@endsection


@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script>

    @if(count($clients) <= 10 )
    $('#clients-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#clients-table').DataTable();
    @endif

    $(document).on("click", "#clients-table > tbody > tr", function(e){
        window.location = '/admin/clients/show/'+$(this).data('target');
    });
    </script>
@endsection