@extends('admin.list')

@section('list')
<p class="h4">Customer Name: {{$client->name}}</p>
<p class="h5">Email: {{$client->email}}</p>
<p class="h5">Client Of: {{$client->user->name}}</p>

<p class="h4"><b>Properties</b></p>
<div class="table-responsive mt-3">
    <table class="table table-hover clickable-table" id="properties-table">
        <thead>
            <tr>
                <th>Property Name</th>
                <th>Domain</th>
                <th>Bucket</th>
                <th>Sections</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($client->properties as $property)
            @php $pages = 0; @endphp
            <tr data-target="{{$property->id}}" class="property">
                <td>{{ $property->name }}</td>
                <td>{{ $property->domain }}</td>
                <td>{{ $property->bucket->name }}</td>
                <td>
                    @foreach($property->sections as $section)
                        <p class="p-section-{{$section->id}}"><a href="{{$section->url}}" target="_blank">{{$section->url}}</a> ({{count($section->pages)}} pages)</p>
                        @php $pages += count($section->pages); @endphp
                    @endforeach
                </td>
                <td>
                    <span class="label label-property-{{$property->id}} 
                    @if($property->bucket->max > $property->total_pages())
                    label-info">
                    @else
                    label-danger">
                    @endif
                    {{$pages }}/{{ ($property->bucket->max)}} Pages</span>
                </td>
            </tr>
            <tr id="property-details-{{$property->id}}" class="d-none">
                <td colspan="5" class="p-4">
                    
                    <div class="alert alert-danger fade @if($property->total_pages() > $property->bucket->max) show @endif" role="alert" id="alert-bucket-{{$property->id}}">
                        The number of pages generated exceed the limit for your current plan. Upgrade your plan or additional charges will be applied.
                    </div>
                    <label>Price Bucket</label>
                    <select id="property-bucket-{{$property->id}}" class="form-control">
                    @foreach($buckets as $bucket)
                        <option value="{{$bucket->id}}" @if($bucket->id == $property->bucket->id) selected @endif>{{$bucket->name}} - Price: {{$bucket->pricing}} - Limit: {{$bucket->max}} Pages</option>
                    @endforeach
                    </select>
                    <input type="hidden" id="total-limit-{{$property->id}}" value="{{$property->bucket->max}}">
                    <input type="hidden" id="total-pages-{{$property->id}}" value="{{$property->total_pages()}}">
                    <p class="text-center my-4 save-bucket-{{$property->id}}"><button class="btn btn-primary change-bucket" data-target="{{$property->id}}">Apply Changes</button></p>
                    <h5 class="mt-4">Sections</h5>
                    <div id="sections-{{$property->id}}">
                    @foreach($property->sections as $section)
                        <div class="alert alert-info" role="alert" id="alert-section-{{$section->id}}">
                            <div class="clearfix">
                                <div class="float-right upper-buttons-{{$section->id}}">
                                    <button type="button" class="btn btn-primary section-edit" data-target="{{$section->id}}">Edit</button>
                                    <button type="button" class="btn btn-danger destroy" data-target="{{$section->id}}" data-action="pagesections">delete</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label><b>Section Name</b></label>
                                        <input type="text" class="form-control editable-{{$section->id}}" value="{{$section->name}}" name="name" id="name-{{$section->id}}" data-old="{{$section->name}}" disabled>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label><b>URL</b></label>
                                        <input type="text" class="form-control editable-{{$section->id}}" value="{{$section->url}}" name="url" id="url-{{$section->id}}" data-old="{{$section->url}}" disabled>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label><b>Description</b></label>
                                        <textarea class="form-control editable-{{$section->id}}" name="description" id="description-{{$section->id}}" data-old="{{$section->description}}" disabled>{{$section->description}}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label><b>Limit</b></label>
                                        <div class="editable-{{$section->id}}">
                                            <input type="range" class="form-control-range limit-{{$property->id}} editable-{{$section->id}}" data-target="{{$section->id}}" data-property="{{$property->id}}" name="limit" id="limit-{{$section->id}}" value="{{$section->limit}}" min="1" max="{{$property->bucket->max}}" data-old="{{$section->limit}}" disabled>
                                            <br><span class="range-span-{{$section->id}}" data-old="{{$section->limit}}">{{$section->limit}}</span>                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix d-none actions-buttons-{{$section->id}}">
                                <button type="button" class="btn btn-primary float-left section-cancel " data-target="{{$section->id}}">Cancel</button>
                                <button type="button" class="btn btn-primary float-right section-save" data-target="{{$section->id}}" data-property="{{$property->id}}">Save</button>
                            </div>
                      </div>
                    @endforeach
                    </div>
                    <p class="text-center"><button class="btn btn-primary add-section-btn" data-target="{{$property->id}}">Add Section</button></p>
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>


<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            @csrf
            @method("DELETE")
            <input type="hidden" id="deleteItem" name="item" value="">
            <input type="hidden" id="deleteAction" name="action" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="confirm-destroy">Understood, continue.</button>
        </div>
      </div>
    </div>
</div>
@endsection


@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    

    $(document).on("click", "#properties-table > tbody > tr", function(e){
        let target = $(this).data("target");
        if($("#property-details-"+target).hasClass("d-none")){
            $("#property-details-"+target).removeClass("d-none");
        }else{
            $("#property-details-"+target).addClass("d-none");
        }
    });

    $(document).on("click", ".add-section-btn", function(){
        let target = $(this).data("target");
        let id = "temp-"+((parseInt(("#sections-"+target+" div").length))+1);
        let max = $("#total-limit-"+target).val();
        let section = '<div class="alert alert-info" role="alert" id="alert-section-'+id+'">';
        section += '<div class="clearfix">';
        section += '<div class="float-right upper-buttons-'+id+' d-none">';
        section += '<button type="button" class="btn btn-primary section-edit" data-target="'+id+'">Edit</button>';
        section += '<button type="button" class="btn btn-danger destroy" data-target="'+id+'" data-action="pagesections">delete</button>';
        section += '</div>';
        section += '</div>';
        section += '<div class="row">';
        section += '<div class="col-xs-12 col-lg-6">';
        section += '<div class="form-group">';
        section += '<label><b>Section Name</b></label>';
        section += '<input type="text" class="form-control editable-'+id+'" value="" name="name" id="name-'+id+'" data-old="">';
        section += '</div>';
        section += '</div>';
        section += '<div class="col-xs-12 col-lg-6">';
        section += '<div class="form-group">';
        section += '<label><b>Description</b></label>';
        section += '<textarea class="form-control editable-'+id+'" name="description" id="description-'+id+'" data-old=""></textarea>';
        section += '</div>';
        section += '</div>';
        section += '<div class="col-xs-12 col-lg-6">';
        section += '<div class="form-group">';
        section += '<label><b>URL</b></label>';
        section += '<input type="text" class="form-control editable-'+id+'" value="" name="url" id="url-'+id+'" data-old="">';
        section += '</div>';
        section += '</div>';
        section += '<div class="col-xs-12 col-lg-6">';
        section += '<div class="form-group">';
        section += '<label><b>Limit</b></label>';
        section += '<div class="editable-'+id+'">';
        section += '<input type="range" class="form-control-range limit-'+target+' editable-'+id+'" data-target="'+id+'" data-property="'+target+'" name="limit" id="limit-'+id+'" value="" min="1" max="'+max+'" data-old="">';
        section += '<br><span class="range-span-'+id+'" data-old=""></span>';
        section += '</div></div></div></div>';
        section += '<div class="clearfix actions-buttons-'+id+'">';
        section += '<button type="button" class="btn btn-primary float-left section-cancel" data-target="'+id+'">Cancel</button>';
        section += '<button type="button" class="btn btn-primary float-right section-save" data-target="'+id+'" data-property="'+target+'">Save</button>';
        section += '</div></div>';

        $("#sections-"+target).append(section);
    });

    $(document).on("click", ".section-edit", function(){
        $(".editable-" + $(this).data("target")).removeClass("d-none");
        $(".editable-" + $(this).data("target")).attr("disabled", false);
        $(".upper-buttons-"+ $(this).data("target")).addClass("d-none");
        $(".actions-buttons-"+$(this).data("target")).removeClass("d-none");
    });

    $(document).on("click", ".section-cancel", function(){
        let target = $(this).data("target");
        dismiss_section_editable_fields($(this).data("target"));
        $("#name-"+target).val($("#name-"+target).data("old"));
        $("#description-"+target).val($("#description-"+target).data("old"));
        $("#url-"+target).val($("#url-"+target).data("old"));
        $("#limit-"+target).val($("#limit-"+target).data("old"));
        $(".range-span-"+target).text($(".range-span-"+target).data("old"));
    });

    $(document).on("change", ".form-control-range", function(){
        let target = $(this).data("target");
        $(".range-span-"+target).text($(this).val());
    });

    $(document).on("click", ".section-save", function(){
        let property = $(this).data("property");
        let target = $(this).data("target");
        let name = $("#name-" + target).val();
        let description = $("#description-" + target).val();
        let url = $("#url-" + target).val();
        let limit = $("#limit-" + target).val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        let method = 'PATCH';
        $.post("{{route('admin.pagesections.update')}}", { property: property, section: target, name: name, description: description, url: url, limit: limit, _token: csrf_token, _method : method }, function(data){
            if(data.success){
                $.toast({
                    heading: 'Section Updated',
                    text: 'The Section '+data.name+' was succesfully updated',
                    hideAfter: 10000,
                    position: 'bottom-right',
                    loaderBg:'#00c292',
                    icon: 'success',
                    hideAfter: 7000,
                    stack: false, 
                });
                $(".label-name-" + data.section).text(data.name);
                $(".label-description-" + data.section).text(data.description);
                $(".label-url-" + data.section).text(data.url);
                $(".label-limit-" + data.section).text(data.limit);
                $(".range-span-" + data.section).text(data.limit);
                $("#name-" + data.section).data("old", data.name);
                $("#description-" + data.section).data("old", data.description);
                $("#url-" + data.section).data("old", data.url);
                $("#limit-" + data.section).data("old", data.limit);
                $(".range-span-" + data.section).data("old", data.limit);
            }
        });
        dismiss_section_editable_fields(target);
    });

    $(document).on("click", ".destroy", function(){
        let target = $(this).data("target");
        let action = $(this).data("action");
        $("#deleteItem").val(target);
        $("#deleteAction").val(action);
        $('#destroyModal').modal({
            keyboard: false
        })
    });

    $(document).on("click", ".change-bucket", function(){
        let property = $(this).data("target");
        let bucket = $("#property-bucket-"+property).val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        let method = 'PATCH';
        $.post("{{route('admin.tmpproperties.update')}}", {property: property, bucket: bucket, _token: csrf_token, _method: method}, function(data){
            $("#total-limit-"+data.property).val(data.max);
            let tpages = $("#total-pages-"+data.property).val();
            let tlimit = $("#total-limit-"+data.property).val();
            $(".limit-"+data.property).attr("max", data.max);
            $(".label-property-"+data.property).text(tpages+"/"+tlimit+" Pages");
            if(parseInt(tlimit) >= parseInt(tpages)){
                $("#alert-bucket-"+data.property).removeClass('show');
                $(".label-property-"+data.property).removeClass("label-danger");
                $(".label-property-"+data.property).addClass("label-info");
            }else{
                $("#alert-bucket-"+data.property).addClass('show');
                $(".label-property-"+data.property).addClass("label-danger");
                $(".label-property-"+data.property).removeClass("label-info");
            }
            $.toast({
                heading: 'Property Updated',
                text: 'Price Bucket Succesfully Updated for the Property '+data.name,
                hideAfter: 10000,
                position: 'bottom-right',
                loaderBg:'#00c292',
                icon: 'success',
                hideAfter: 7000,
                stack: false, 
            });
        });
    });

    $(document).on("click", "#confirm-destroy", destroy_item);

    function destroy_item(){
        let item = $("#deleteItem").val();
        let action = $("#deleteAction").val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        let method = 'DELETE';
        $('#destroyModal').modal("hide");
        $.post( "/admin/"+action+"/destroy", { item: item, _token: csrf_token, _method: method }, function( data ) {
            if(data.success){
                $('#alert-'+data.action+"-"+data.target).remove();
                $('.p-'+data.action+"-"+data.target).remove();
                $.toast({
                    heading: 'Section Deleted',
                    text: 'The Seccion was succesfully deleted.',
                    hideAfter: 10000,
                    position: 'bottom-right',
                    loaderBg:'#00c292',
                    icon: 'success',
                    hideAfter: 7000,
                    stack: false, 
                });
            }
        });
    }

    function dismiss_section_editable_fields(target){
        $(".editable-" + target).attr("disabled", true);
        $(".upper-buttons-" + target).removeClass("d-none");
        $(".actions-buttons-" + target).addClass("d-none");

    }

    $(document).on("change", '.form-control-range', function(){
        let total = 0;
        let property = $(this).data("property");
        let max = $("#total-limit-"+property).val();
        $(".limit-"+property).each(function(){
            total += parseInt($(this).val());
        });
        if(total > max){
            $.toast({
                heading: 'Limit Generated Pages Setup',
                text: 'Your limit of pages generated exceed the total amount for your current plan. Chenge your setup or upgrade your plan.',
                hideAfter: 10000,
                position: 'bottom-right',
                loaderBg:'#e46a76',
                icon: 'error',
                hideAfter: false,
                stack: false, 
            });
        }else if(total < max){
            $.toast({
                heading: 'Limit Generated Pages Setup',
                text: 'Your Plan still have room.',
                hideAfter: 10000,
                position: 'bottom-right',
                loaderBg:'#00c292',
                icon: 'success',
                hideAfter: false,
                stack: false, 
            });
        }
    });

    @if(count($client->properties) <= 10 )
    $('#properties-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#properties-table').DataTable();
    @endif
    </script>
@endsection