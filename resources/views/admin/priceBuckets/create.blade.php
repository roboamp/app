@extends('admin.forms')

@section('css_form')
<link href="{{asset('Axton/assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
@endsection


@section('form_fields')
<div class="form-row">
    <div class="col-12">
        <label>Name</label>
        <input type="text" class="form-control @error('txt_name')alert-danger @enderror" name="name" id="name">
        @error('name')
        <p class="text-danger">{{$errors->first('name')}}</p>
        @enderror
    </div>                                                                                                                        
</div>
<div class="form-row">
    <div class="col-6">
        <label>Price</label>
        <input type="text" class="form-control" name="pricing" id="pricing">
        @error('pricing')
        <p class="text-danger">{{$errors->first('pricing')}}</p>
        @enderror
    </div>  
    <div class="col-6">
        <label>Max Generated Pages</label>
        <input id="max" type="text" value="" name="max" data-bts-button-down-class="btn btn-secondary btn-outline" data-bts-button-up-class="btn btn-secondary btn-outline">
        @error('max')
        <p class="text-danger">{{$errors->first('max')}}</p>
        @enderror
    </div>                                                                                                                          
</div>
@endsection

@section('new_row')

@endsection


@section ('form_scripts_include')
<script src="{{asset('Axton/assets/node_modules/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}"></script>
@endsection

@section('form_script')
$("input[name='max']").TouchSpin({
    initVal: 2,
    min: 0,
    max: 10000
});
@endsection