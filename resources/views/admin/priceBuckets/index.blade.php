@extends('admin.list')


@section('list')
<div class="clearfix">
    <a href="{{ route('admin.buckets.create') }}" class="btn btn-primary float-right">Add a Bucket</a>
</div>
<div class="table-responsive mt-3">
    <table class="table table-striped" id="buckets-table">
        <thead>
            <tr>
                <th width="35">Name</th>
                <th width="15">Pricing</th>
                <th width="15">Max</th>
                <th width="20">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($buckets as $bucket)
            <tr id="bucket-row-{{ $bucket->id }}">
                <td>
                    <span class="name-{{ $bucket->id }}">{{ $bucket->name }}</span>
                    <input type="text" class="form-control d-none" value="{{ $bucket->name }}" name="name-{{ $bucket->id }}" id="name-{{ $bucket->id }}" data-old="{{ $bucket->name }}">
                </td>
                <td>
                    <span class="pricing-{{ $bucket->id }}">{{ $bucket->pricing }}</span>
                    <input type="text" class="form-control d-none" value="{{ $bucket->pricing }}" name="pricing-{{ $bucket->id }}" id="pricing-{{ $bucket->id }}" data-old="{{ $bucket->pricing }}">
                </td>
                <td>
                    <span class="max-{{ $bucket->id }}">{{ $bucket->max }}</span>
                    <input type="text" class="form-control d-none" value="{{ $bucket->max }}" name="max-{{ $bucket->id }}" id="max-{{ $bucket->id }}" data-old="{{ $bucket->max }}">
                </td>
                <td>
                    <div class="actions-{{ $bucket->id }}">
                        <button type="button" class="btn btn-info btn-sm edit" data-target="{{ $bucket->id }}"><i class="ti-pencil-alt"></i></button>
                        <button type="button" class="btn btn-danger btn-sm destroy" data-target="{{ $bucket->id }}"><i class="ti-trash"></i></button>
                    </div>
                    <div class="form-actions-{{ $bucket->id }} d-none">
                        <button type="button" class="btn btn-success btn-sm success" data-target="{{ $bucket->id }}"><i class="ti-check"></i></button>
                        <button type="button" class="btn btn-danger btn-sm cancel" data-target="{{ $bucket->id }}"><i class="ti-na"></i></button>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>


<div class="modal fade" id="destroyModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Delete Price Bucket</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            This action cannot be undone. Are you sure you want to continue?
            @csrf
            @method("DELETE")
            <input type="hidden" id="deleteBucketId" name="bucket" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="confirm-destroy">Understood, continue.</button>
        </div>
      </div>
    </div>
  </div>
@endsection


@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('dashboard_assets/node_modules/toast-master/css/jquery.toast.css')}}">
    <script src="{{asset('dashboard_assets/node_modules/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/datatables-responsive/js/dataTables.responsive.js')}}"></script>
    <script src="{{asset('dashboard_assets/node_modules/toast-master/js/jquery.toast.js')}}"></script>
    <script>

    @if(count($buckets) <= 10 )
    $('#buckets-table').DataTable({
        searching: false,
        ordering:  false,
        paging: false
    });
    @else
    $('#buckets-table').DataTable();
    @endif
    
    $(document).on("click", ".destroy", function(){
        let target = $(this).data("target");
        $("#deleteBucketId").val(target);
        $('#destroyModal').modal({
            keyboard: false
        })
    });

    $(document).on("click", "#confirm-destroy", destroy_component);

    function destroy_component(){
        let bucket = $("#deleteBucketId").val();
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        let method = $('input[name="_method"]').val();
        $('#destroyModal').modal("hide");
        $.post( "{{route('admin.buckets.destroy')}}", { bucket: bucket, _token: csrf_token, _method: method }, function( data ) {
            if(data.success){
                $('#buckets-table').DataTable().row("#bucket-row-"+data.target).remove().draw("false");
                $.toast({
                    heading: 'Bucket Info Updated',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#00c292',
                    icon: 'success',
                    hideAfter: 3000,
                    stack: false, 
                });
            }
        });
    }


    $(document).on("click", ".edit", function(){
        let target = $(this).data("target");
        prepare_edit_form(target);
    });
    $(document).on("click", ".success", function(){
        let target = $(this).data("target");
        send_edit_form(target);
    });
    $(document).on("click", ".cancel", function(){
        let target = $(this).data("target");
        return_old_values(target);
        dismiss_form(target);
    });

    function prepare_edit_form(target){
        $(".actions-"+target).addClass("d-none");
        $(".form-actions-"+target).removeClass("d-none");
        $(".name-"+target).addClass("d-none");
        $("#name-"+target).removeClass("d-none");
        $(".pricing-"+target).addClass("d-none");
        $("#pricing-"+target).removeClass("d-none");
        $(".max-"+target).addClass("d-none");
        $("#max-"+target).removeClass("d-none");
    }

    function send_edit_form(target){
        let name = $("#name-"+target).val();
        let pricing = $("#pricing-"+target).val();
        let max = $("#max-"+target).val();
        let method = "PATCH";
        let csrf_token = $('meta[name="csrf-token"]').attr('content');
        $.post( "{{route('admin.buckets.update')}}", { id: target, name: name, pricing: pricing, max: max, _token: csrf_token, _method : method }, function( data ) {
            if(data.success){
                $(".name-"+data.target).text(data.name);
                $(".pricing-"+data.target).text(data.pricing);
                $(".max-"+data.target).text(data.max);
                $("#name-"+target).data("old", data.name);
                $("#pricing-"+target).data("old", data.pricing);
                $("#max-"+target).data("old", data.max);
                $.toast({
                    heading: 'Bucket Info Updated',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#00c292',
                    icon: 'success',
                    hideAfter: 3000,
                    stack: false, 
                });
                dismiss_form(data.target);
            }else{
                $.toast({
                    heading: 'Bucket Info Updated',
                    text: data.message,
                    position: 'top-right',
                    loaderBg:'#e46a76',
                    icon: 'error',
                    hideAfter: 3000,
                    stack: false, 
                });
                return_old_values(data.target);   
            }
        });
    }

    function dismiss_form(target){
        $(".actions-"+target).removeClass("d-none");
        $(".form-actions-"+target).addClass("d-none");
        $(".name-"+target).removeClass("d-none");
        $("#name-"+target).addClass("d-none");
        $(".pricing-"+target).removeClass("d-none");
        $("#pricing-"+target).addClass("d-none");
        $(".max-"+target).removeClass("d-none");
        $("#max-"+target).addClass("d-none");
    }

    function return_old_values(target){
        let name = $("#name-"+target).data("old");
        let pricing = $("#pricing-"+target).data("old");
        let max = $("#max-"+target).data("old");
        $(".name-"+target).text(name);
        $("#name-"+target).val(name);
        $(".pricing-"+target).text(pricing);
        $("#pricing-"+target).val(pricing);
        $(".max-"+target).text(max);
        $("#max-"+target).val(max);
    }
    </script>
@endsection