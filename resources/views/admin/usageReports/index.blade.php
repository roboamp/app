@extends('admin.list')

@section('list')
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <label>Client</label>
            <select class="form-control" id="client">
                @foreach($clients as $client)
                    <option value="{{$client->id}}">{{$client->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-lg-4">
        <div class="card shadow p-3 mb-5 bg-white rounded">
            <div class="d-flex flex-row">
                <div class="p-10 bg-primary">
                    <h3 class="text-white box m-b-0"><i class="ti-world"></i></h3></div>
                <div class="align-self-center m-l-20">
                    <h3 class="m-b-0 text-info properties"></h3>
                    <h5 class="text-muted m-b-0">Properties</h5></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-4">
        <div class="card shadow p-3 mb-5 bg-white rounded">
            <div class="d-flex flex-row">
                <div class="p-10 bg-dark">
                    <h3 class="text-white box m-b-0"><i class="ti-layout-menu-v "></i></h3></div>
                <div class="align-self-center m-l-20">
                    <h3 class="m-b-0 text-info sections"></h3>
                    <h5 class="text-muted m-b-0">Sections</h5></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-4">
        <div class="card shadow p-3 mb-5 bg-white rounded">
            <div class="d-flex flex-row">
                <div class="p-10 bg-info">
                    <h3 class="text-white box m-b-0"><i class="ti-files"></i></h3></div>
                <div class="align-self-center m-l-20">
                    <h3 class="m-b-0 text-info pages"></h3>
                    <h5 class="text-muted m-b-0">Pages Generated</h5></div>
            </div>
        </div>
    </div>

    <div class="col-12 my-4 shadow p-3 mb-5 bg-white rounded p-10">
        <h4>Properties</h4>
        <div class="row" id="properties-charts">
            
        </div>
    </div>
</div>
@endsection


@section('scripts')
<link rel="stylesheet" href="{{asset('chart-js/Chart.min.css')}}" />
<script src="{{asset('chart-js/Chart.min.js')}}"></script>

<script>
update_report();
$(document).on("change", "#client", update_report);

function update_report(){
    $("#properties-charts").empty();
    let client = $("#client").val();
    let csrf = $('meta[name="csrf-token"]').attr('content');
    $.post("{{route('admin.usage_reports.show')}}", { client: client, _token: csrf }, function(data){
        $(".properties").text(data.properties_count);
        $(".sections").text(data.sections_count);
        $(".pages").text(data.pages_count);
        data.properties.forEach(element => render_property_charts(element));

    });
}

function render_property_charts(property){
    let csrf = $('meta[name="csrf-token"]').attr('content');
    let chart = '<div class="col-xs-12 col-lg-6">';
    chart += '<div class="card shadow p-3 mb-5 bg-white rounded">';
    chart += '<h4 class="card-title">'+property.name+'</h4>';
    chart += '<div class="card-body">';
    chart += '<canvas id="chart-'+property.id+'" class="chart-'+property.id+'" height: "400px" width="100%"></canvas>';
    chart += '</div></div></div>';
    $("#properties-charts").append(chart);
    $.post("{{route('admin.usage_reports.getSections')}}", {property: property.id, _token:csrf}, function(data){
        var ctx = document.getElementById('chart-'+data.property);
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: data.sections.names,
                datasets: [{
                    label: '# of Pages Generated',
                    data: data.sections.pages,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        
    });
}
</script>
@endsection