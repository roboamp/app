@extends('admin.dashboard')
@section ('css')
    <style></style>
    @yield('css_list')

@endsection

@section('content')
<div class="row">
    @center_form($form_centered)
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{$form_title??''}}</h4>
                @yield('list')
            </div>
        </div>
    @endcenter_form
</div>


@endsection


@section ('scripts')
    @yield ('list_child_script_include')
    <script>

        (function(){
            @yield('list_child_script')

        })();


    </script>
@endsection



