<!doctype html>
<html ⚡ amp lang="en">
<head>
	<meta charset="utf-8">
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>

	<meta name="google-site-verification" content="0z-StCgxeE6Zc-wns2ZeXMKWV7G003lYKLzSEctV7yE" />
	<title>ROBOAMP</title>
	<link rel="canonical" href="https://roboamp.com/">
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "NewsArticle",
        "headline": "Open-source framework for publishing content",
        "datePublished": "2015-10-07T12:02:41Z",
        "image": [
          "logo.jpg"
        ]
      }

    </script>
	<style amp-custom>
		form.amp-form-submit-success > input {
			display: none
		}
		form.amp-form-submit-success [submit-success],
		form.amp-form-submit-error [submit-error]{
			margin-top: 16px;
		}
		form.amp-form-submit-success [submit-success] {
			color: green;
		}
		form.amp-form-submit-error [submit-error] {
			color: red;
		}
		form.amp-form-submit-success.hide-inputs > input {
			display: none;
		}
		@include('amp.style')

	</style>
	<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

</head>
<body>



<!-- First Screen -->
<section class="screen-section section-1">
	<div class="section-inner">
		@include('countdown.partials.header',['type'=>'amp'])


		<div id="countdown_block">
			<!--
                Don't be afraid to edit code ;)
                Here you can edit site tagline
             -->
			<div id="count__down"></div>
			<h2 id="site-tagline">Our website is launching soon</h2>


			<!-- Mailchimp-generated subscribe form -->
			<div id="subscribe-form">
				<div id="mc_embed_signup">
					<!--

					-->



					<form method="post"
						  class="p2"
						  action-xhr="//127.0.0.1:8000/welcome"
						  target="_top">
						<div class="ampstart-input inline-block relative m0 p0 mb3">
							<input type="text"
								   class="block border-none p0 m0"
								   name="name"
								   placeholder="Name..."
								   required>
							<input type="email"
								   class="block border-none p0 m0"
								   name="email"
								   placeholder="Email..."
								   required>
						</div>
						<input type="hidden" name="_token" id="token" value="<?php echo csrf_token() ?>">

						<input type="submit"
							   value="Subscribe"
							   class="ampstart-btn caps">
						<div submit-success>
							<template type="amp-mustache">
								Success! Thanks @{{name}} for trying the

							</template>
						</div>
						<div submit-error>
							<template type="amp-mustache">
								Error! Thanks @{{name}} for trying the
								<code>amp-form</code> demo with an error response.
							</template>
						</div>
					</form>




				</div>
			</div>
		</div>

		<footer id="site-footer">
			@include('countdown.partials.social')
			<div class="scrolldown">
					<span class="mousescroll">
						<span class="scrolldown-title">Scroll down</span>
						<i class="scroll-icon"></i>
					</span>

			</div>
		</footer>

	</div>
</section>

<!-- Section 2 -->
<section class="screen-section section-2">
	<div class="section-inner">
		<div class="left-half-block section-half">
			<div class="section-content">
				<h2 class="section-title">About<!-- Here you can change section title --></h2>
				<p>We love internet, but we love <b>FAST</b> internet even more!</p>
				<p>RoboAMP transforms your regular slow web pages into super fast and mobile friendly AMP compatible websites.
				</p>

				<h4>7 ways AMP makes your Pages Fast</h4>

				<!--		<iframe width="560" height="500" src="https://www.youtube.com/embed/9Cfxm7cikMY" frameborder="0" allowfullscreen></iframe>	-->
				<amp-youtube data-videoid="9Cfxm7cikMY" layout="responsive" width="560" height="500"></amp-youtube>
			</div>

		</div>
		<div class="right-half-block section-half"></div>
	</div>
</section>

<!-- Section 3 -->
<section class="screen-section section-3">
	<div class="section-inner">
		<div class="left-half-block section-half">
			@include('countdown.partials.why')
		</div>
		<div class="right-half-block section-half"></div>
	</div>
</section>

<!-- Section 4 -->
<section class="screen-section section-4">
	<div class="section-inner">
		<div class="left-half-block section-half">
			<div class="section-content">
				<h2 class="section-title">Get In Touch<!-- Here you can change section title --></h2>

				<!-- Here You can edit contacts info -->
				@include('countdown.partials.contact')


			</div>
		</div>
		<div class="right-half-block section-half"></div>
	</div>
</section>


@include('countdown.partials.analytics',['type'=>'amp'])
</body>
</html>
