<form method="post"
      class="p2"
      action-xhr="//127.0.0.1:8000/welcome"
      target="_top">
    <div class="ampstart-input inline-block relative m0 p0 mb3">
        <input type="text"
               class="block border-none p0 m0"
               name="name"
               placeholder="Name..."
               required>
        <input type="email"
               class="block border-none p0 m0"
               name="email"
               placeholder="Email..."
               required>
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

    </div>
    <input type="submit"
           value="Subscribe"
           class="ampstart-btn caps">
    <div submit-success>
        <template type="amp-mustache">
            Success! Thanks nana @{{name}} popo for your email @{{email}}
        </template>
    </div>
    <div submit-error>
        <template type="amp-mustache">
            some error
        </template>
    </div>
</form>