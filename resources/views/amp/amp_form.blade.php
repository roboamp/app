<!doctype html>
<html ⚡>
<head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <link rel="canonical" href=".">

</head>
<body>
<form method="post"
      action-xhr="https://parser.roboamp.com/amp/email">
    <p>Form Submission with Page Update</p>
    <div>
        <input type="text"
               name="name"
               placeholder="Name..."
               required value="Some Name">
        <input type="email"
               name="email"
               placeholder="Email..." value="some18@email.com"
               required>
    </div>
    <input type="submit"
           value="Subscribe">
    <div submit-success>
        <template type="amp-mustache">
            Success! Thanks @{{name}} for trying the
            <code>amp-form</code> demo! Try to insert the word "error" as a name input in the form to see how
            <code>amp-form</code> handles errors.
        </template>
    </div>
    <div submit-error>
        <template type="amp-mustache">
            Error! Thanks @{{name}} for trying the
            <code>amp-form</code> demo with an error response.
        </template>
    </div>
</form>
</body>
</html>