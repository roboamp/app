<!--
  Steps:
    1. Create a clone of https://codepen.io/iubenda/pen/550303cd252f27722080d1dd1bf5f142?editors=1000 and host it on your site (edit the CS configuration to reflect your own)
    2. Copy the entire <amp-consent> section below into your AMP template
    3. Add to your template the resources in the Setup section in the head below
-->


<!DOCTYPE html>
<html ⚡>
<head>
    <meta charset="utf-8">
    <link rel="canonical" href="https://amp.dev/documentation/examples/user-consent/external_user_consent_flow/index.html">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <title>External User Consent Flow</title>

    <!-- ## Setup -->
    <!-- We need to import the `amp-consent` ... -->
    <script async custom-element="amp-consent" src="https://cdn.ampproject.org/v0/amp-consent-latest.js"></script>
    <!-- ... and the `amp-iframe` extension. -->
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-latest.js"></script>
    <!-- ... the `amp-geo` to only ask consent in EU ... -->
    <!-- <script async custom-element="amp-geo" src="https://cdn.ampproject.org/v0/amp-geo-latest.js"></script>-->
    <style amp-custom>
        .popupOverlay {
            position:fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
        }
        amp-iframe {
            margin: 0;
        }
        amp-consent.amp-active {
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            position: fixed;
        }
    </style>
    <!-- /Setup -->

    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

</head>
<body>
<!--
  Define the geo groups for which consent is required (if desired)

  <amp-geo layout="nodisplay">
    <script type="application/json">
      {
        "ISOCountryGroups": {
          "eu": ["preset-eea"]
        }
      }
    </script>
  </amp-geo>
-->

<!-- ## Defining the Consent Flow -->
<!--
  We define a basic consent flow, similar to [this sample](/user_consent/basic_user_consent_flow/). The consent dialog is from a CORS iframe loaded with the `amp-iframe` component
-->
<amp-consent id="powpow" layout="nodisplay">
    <!--
      It is preferred to set the consent ID as "consent" + site ID
      If you want to request consent only to EU users then replace "consentRequired": true with
      "promptIfUnknownForGeoGroup": "eu" -> allows to ask consent only to EU users
    -->
    <script type="application/json">
      {
        "consentInstanceId": "consent896537",
        "consentRequired": true,
        "promptUI": "myConsentFlow"
      }
    </script>
    <div id="myConsentFlow" class="popupOverlay">
        <!--
          Set src attribute to your webpage with the CS for the AMP pages.
          Note: it must be served over HTTPS
          See https://cdn.iubenda.com/cs/test/cs-for-amp.html for an example on how to set
          a page to embed CS
        -->
        <amp-iframe
                layout="fill"
                sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox"
                src="https://ampy.roboamp.com/consent">
            <div placeholder>Loading</div>
        </amp-iframe>
    </div>
</amp-consent>

<p>Here below we show an image only after the consent has been given</p>
<!--
  Use `data-block-on-consent` attribute to block AMP components until consent is given.
  Individual AMP components can override  blocking behavior and implement blocking logic themselves.

  Here is an image which is blocked until consent is given:
-->
<p>
    <amp-img data-block-on-consent src="https://demo.roboamp.com/Roboamp.png" width="500" height="500"></amp-img>
</p>
</body></html>