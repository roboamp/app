<!--{/*
@info
Generated on: 2020-06-03 20:23:58
Initiator: ROBOAMP AMP Generator
Generator version: 0.9.6
*/}-->

<!doctype html>
<html amp lang="en">
  <head>
        <title>
</title>
<link href="https://fonts.googleapis.com/css?family=Hind:400,500" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500" rel="stylesheet">


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">

<link rel="canonical" href="https://www.mansfield-dentalcare.com">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>

<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <style amp-custom>
      @font-face{font-family: 'Hind', sans-serif ;src: url( https://fonts.googleapis.com/css?family=Hind:400,500);}


      *{-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;padding:0;margin:0;transition: all 0s ease-in-out;transition-timing-function: cubic-bezier(0.22, .25, .27, .3);}
      li{list-style:none}input{outline: none;}
      *:focus {outline: none;}
      html{font-size:62.5%}
      body{font-size:14px;font-weight: normal;font-family: 'Hind', sans-serif;color: #797878;line-height: 1.65;overflow-x: hidden;}
      p{text-align: justify; margin-bottom: 20px;}
      img{object-fit:cover}.objcontain img{object-fit:contain;}
      .bodyc {color: #797878;}.cw{color:#fff;}
      .gbg {
        background: #F5F5F5;
      }

      a,a:hover,button{color: #03A9F4;-webkit-transition: all 0.6s ease-in-out;-moz-transition: all 0.6s ease-in-out;-o-transition: all 0.6s ease-in-out;transition: all 0.6s ease-in-out;text-decoration: none;background:transparent;border:0;outline:0;text-decoration:none;cursor:pointer;}
      .left-img img {object-fit: contain;object-position: left;}
      .mb1{margin-bottom: 1rem;}.mr1{margin-right: 1rem;}.ml1{margin-left: 1rem;}.mt1{margin-top: 1rem;}

      .m0{margin:0;}.m2{margin:2rem;}.m1{margin: 1rem}.m20{margin: 2rem 0}.mhalf{margin: 0.5rem}.m1a{margin: 1rem auto;}.m10{margin: 1rem 0;}.mh0{margin: 0.5rem 0;}.m0h{margin: 0 0.5rem;}.m12{margin: 1rem 2rem;}
      .fs1{font-size: 1rem}
      .fs11{font-size: 1.1rem;}
      .fs12{font-size: 1.2rem;}
      .fs13{font-size: 1.3rem;}
      .fs14{font-size: 1.4rem;}
      .fs16{font-size: 1.6rem;}
      .fs2{font-size: 2rem;}
      .fs26{font-size: 2.6rem;}
      .fs3{font-size: 3rem;}

      .fw3{font-weight:300;}.fw4{font-weight: 400;}.fw5{font-weight: 500;}.bold{font-weight: bold;}
      .ucase{text-transform: uppercase;}
      .br3r{border-radius: 0.3rem;-webkit-border-radius: 0.3rem;}

      .p1{padding: 1rem;}
      .p2{padding: 2rem;}

      .p10{padding: 1rem 0;}
      .p20{padding: 2rem 0;}
      .p30{padding: 3rem 0;}
      .p40{padding: 4rem 0;}

      .p02{padding: 0 2rem;}
      .m02{margin: 0 2rem;}

      .pl1{padding-left: 1rem; }
      .pr1{padding-right: 1rem; }
      .pt1{padding-top: 1rem; }
      .pb1{padding-bottom: 1rem; }

      .rbtn{color: #fff;background-color: #dc3545;border-color: #dc3545;}
      .dfx{display: flex;align-items: center;}.center{text-align:center}
      .jcsb{justify-content: space-between;}
      .flxwrp{flex-wrap: wrap;}
      .jcc{justify-content: center;}
      .baseline{align-items: baseline;}
      .flstart{align-items: flex-start;}
      .ftcnt{width: fit-content;}
      .fullw {width: 100%;}
      .wrapper{
        width: 96%;
        margin:auto;
      }
      .logo{width: 30rem;height:5rem;}.logo img{object-fit:contain;}
      .mobile-toggler {
        display: none;
        position: absolute;
        top: 3rem;
        right: 3rem;
      }
      h1 {font-size: 56px;}
      h2 {font-size: 46px;}
      h3 {font-size: 32px;}
      h4 {font-size: 25px;}
      h5 {font-size: 20px;}
      h6 {font-size: 18px;}
      h1, h2, h3, h4, h5, h6 {
        clear: both;
        font-family: 'Montserrat', sans-serif;
        color: #222;
        font-weight: 500;
        margin: 1rem 0;
      }

      .row{display: -ms-flexbox;display: flex;-ms-flex-wrap: wrap;flex-wrap: wrap;margin-right: -15px;margin-left: -15px}
      .lg1,.lg10,.lg11,.lg12,.lg2,.lg3,.lg4,.lg5,.lg6,.lg7,.lg8,.lg9,.md1,.md10,.md11,.md12,.md2,.md3,.md4,.md5,.md6,.md7,.md8,.md9,.sm1,.sm10,.sm11,.sm12,.sm2,.sm3,.sm4,.sm5,.sm6,.sm7,.sm8,.sm9,.xs1,.xs10,.xs11,.xs12,.xs2,.xs3,.xs4,.xs5,.xs6,.xs7,.xs8,.xs9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.xs1,.xs10,.xs11,.xs12,.xs2,.xs3,.xs4,.xs5,.xs6,.xs7,.xs8,.xs9{float:left}.xs12{width:100%}.xs11{width:91.66666667%}.xs10{width:83.33333333%}.xs9{width:75%}.xs8{width:66.66666667%}.xs7{width:58.33333333%}.xs6{width:50%}.xs5{width:41.66666667%}.xs4{width:33.33333333%}.xs3{width:25%}.xs2{width:16.66666667%}.xs1{width:8.33333333%}@media(min-width:768px){.sm1,.sm10,.sm11,.sm12,.sm2,.sm3,.sm4,.sm5,.sm6,.sm7,.sm8,.sm9{float:left}.sm12{width:100%}.sm11{width:91.66666667%}.sm10{width:83.33333333%}.sm9{width:75%}.sm8{width:66.66666667%}.sm7{width:58.33333333%}.sm6{width:50%}.sm5{width:41.66666667%}.sm4{width:33.33333333%}.sm3{width:25%}.sm2{width:16.66666667%}.sm1{width:8.33333333%}}@media(min-width:992px){.md1,.md10,.md11,.md12,.md2,.md3,.md4,.md5,.md6,.md7,.md8,.md9{float:left}.md12{width:100%}.md11{width:91.66666667%}.md10{width:83.33333333%}.md9{width:75%}.md8{width:66.66666667%}.md7{width:58.33333333%}.md6{width:50%}.md5{width:41.66666667%}.md4{width:33.33333333%}.md3{width:25%}.md2{width:16.66666667%}.md1{width:8.33333333%}}@media(min-width:1200px){.lg1,.lg10,.lg11,.lg12,.lg2,.lg3,.lg4,.lg5,.lg6,.lg7,.lg8,.lg9{float:left}.lg12{width:100%}.lg11{width:91.66666667%}.lg10{width:83.33333333%}.lg9{width:75%}.lg8{width:66.66666667%}.lg7{width:58.33333333%}.lg6{width:50%}.lg5{width:41.66666667%}.lg4{width:33.33333333%}.lg3{width:25%}.lg2{width:16.66666667%}.lg1{width:8.33333333%}}.container{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media(max-width:336px){.container{width:90%}}@media(min-width:768px){.container{width:750px}}@media(min-width:992px){.container{width:970px}}@media(min-width:1200px){.container{width:1170px}}
      .branded {
        color: #1faaf1;
      }

      #p0 {
        padding: 0;
      }
      .bbot1 {
        border-bottom: 1px solid #f3f3f3;
      }

      .header-top {
        color: #7D7D7D;
        font-size: 12px;
      }
      .top-header-nav-menu li {
        display: inline-block;
        margin-right: 5px;
      }
      .top-header-nav-menu li::before {
        content: "|";
        color: #7D7D7D;
        padding-right: 8px;
        color: #1faaf1;
      }

      .header-top a {
        color: #1faaf1;
      }
      .header-top .header-btn {
        background: #1faaf1;
        padding: 10px;

      }
      .header-top .header-btn a {
        color: #fff;
        padding: 10px;
      }
      .header-top .top-header-nav-menu {
        text-align: right;
      }
      .mast-head .icon-wrapper i {
        font-size: 35px;
      }
      .header-contact-info {
        padding-left: 1rem;
      }
      .menu-items {
        overflow-y: hidden;
      }
      .menu-items>li a {
        padding: 15px;
        color: #444;
        font-weight: 500;
        text-transform: uppercase;
        -webkit-backface-visibility: hidden;
      }
      li.menu-item:hover>a {
        color: #309653;
      }
      .menu-item .menu-item {
        padding: 1.5rem 0;
      }
      .sub-menu {
        position: absolute;
        z-index: 99999;
        display: none;
        padding: 0;
        background: #fff;
        list-style: none;
        font-weight: 400;
        min-width: 300px;
        -webkit-box-shadow: 0 0 10px rgba(149,149,149,0.31);
        -moz-box-shadow: 0 0 10px rgba(149,149,149,0.31);
        box-shadow: 0 0 10px rgba(149,149,149,0.31);
        border-radius: 3px;
      }
      .menu-item:hover .sub-menu {
        display: block;
      }
      .sub-menu li a {
        text-transform: none;
      }
      .sidebar .menu-item:hover .sub-menu, .sidebar .sub-menu {
        position: relative;
        display: block;
      }
      .sidebar .sub-menu {
        background: transparent;
        box-shadow: none;
      }
      .sidebar .sub-menu .menu-item {
        padding: 0;
        padding-left: 2rem;
      }
      .sidebar ul li a {
        font-size: 12px;
      }
      .sidebar ul li {
        display: block;
        margin: 0;
        line-height: 48px;
        border-top: 1px solid #fff;
        border-bottom: 1px solid #dfdfdf;
      }
      .header-mobile .contact-header-area {
        background: #fafafa;
        padding: 12px 0;
      }
      .header-mobile .contact-item {
        padding: 10px 0;
        display: flex;
        align-items: baseline;
      }
      .header-mobile .icon-wrapper {
        float: left;
        padding: 12px 10px;
        text-align: center;
      }
      .header-mobile .header-area-contact-info h6,.header-mobile .header-area-contact-info a {
        color: #003448;
      }
      .footer-area-inner {
        padding-top: 40px;
      }
      .footer-area {
        min-height: 300px;
        color: #898888;
        background: #F5F5F5;
      }
      .strhld-breadcrumb .breadcrumb-trail a,.footer-area a {
        color: #797878;
      }
      .footer-area li {
        padding: 5px 0;
      }
      .widget_nav_menu li:before {
        font-family: 'fontawesome';
        content: "\f054";
        color: #03A9F4;
        padding-right: 8px;
        font-size: 12px;
        color: #5488a4;
      }
      .footer-area li i{
        padding-right: 8px;
        font-size: 20px;
        color: #5488a4;
      }
      .footer-widget-title {
        font-weight: 400;
        text-transform: uppercase;
        margin-right: 40px;
      }
      .widget .widget-title-underline-footer {
        display: block;
        border-bottom: 1px solid #727272;
        padding-top: 15px;
        margin: -10px 0 20px 0;
      }
      .social-widget-item a {
        border-radius: 2px;
        display: inline-block;
        line-height: 40px;
        width: 40px;
        text-align: center;
        font-size: 24px;
        -webkit-transition-duration: 0.6s;
        -moz-transition-duration: 0.6s;
        -o-transition-duration: 0.6s;
        transition-duration: 0.6s;
        -webkit-transition-property: -webkit-transform;
        -moz-transition-property: -moz-transform;
        -o-transition-property: -o-transform;
        transition-property: transform;
        overflow: hidden;
        background: #5488a4;
      }
      .social-widget-item a i {color: #fff;padding-right: 0;}
      .opening-hours-wid li span {float: right;}
      .site-info-wrapper {background: #eee;padding: 20px 0px 15px;
      }
      .site-info {padding: 8px 0;font-size: 13px;}
      .footer-nav-menu li{display: inline-block;margin-right: 5px;margin-top: 10px;}
      .site-footer li a:hover{color: #1faaf1;}
      .md-right {float: right;}
      .footer-nav-menu li::before {content: "|";color: #9F9F9F;padding-right: 8px;}
      .top-header-nav-menu li:first-child:before,
      .footer-nav-menu li:first-child::before {content: "";}

      .page-title-wrapper {
        background: #f7f7f7;
        padding-top: 35px;
        padding-bottom: 35px;
      }
      .strhld-breadcrumb .breadcrumb-trail {
        -ms-word-wrap: break-word;
        word-wrap: break-word;
      }
      .strhld-breadcrumb .breadcrumb-trail .trail-begin:before {
        font-family: "fontawesome";
        content: "\f015";
        padding-right: 3px;
      }
      .breadcrumb-trail .trail-end {
        font-weight: 600;
      }
      .breadcrumb-trail .sep {color: #E6E6E6;font-size: 12px;padding: 0 1px;}

      .slide {
        min-width: 100%;
        min-height: 100%;
        background-repeat: no-repeat;
        background-size: cover;
      }
      .slide-detail {
        position: absolute;
        top: 14%;
        left: 6%;
        min-width: 42vw;
        min-height: 24vw;
        background: #00000090;
      }
      .sld-title,
      .sld-text,
      .sld-btn,
      .sld-head {
        max-height: 20%;
        max-width: 100%;
      }
      .slide-cont {
        position: relative;
      }
      .sld-head {
        font-family: Montserrat;
      }
      .sld-btn {
        position: relative;
      }
      .sld-btn .lmb {
        font-family: Hind;
        background-color: #1faaf1;
        border-color: #000;
        padding: 5px 10px;
        color: #fff;
        min-height: 14px;
        position: absolute;
        top: 0;
        display:block; height:100%;
        max-height: 2rem;
      }
      .sld-btn >.omh>div>div {
        min-height: 14px;
        max-height: 14px;
      }

      .sld-text {
        padding: 1rem 0;
      }
      .main-content {
        padding: 8rem 0 4rem 0;
        max-width: calc(95% - 30px);
      }
      .member-img {
        border-radius: 50%;
        overflow: hidden;
        margin-bottom: 1rem;
      }
      .member-name p {
        text-align: center;
      }
      .members a, .service-main-name a {
        color: #222;
        font-size: 16px;
      }
      .service-main-name a:hover {
        color: #1faaf1;
      }
      .dcs-wrap {
        background: #fcfcfc;
      }
      .service-main-detail {
        padding: 0px 20px 10px;
        border: 1px solid #F2F2F2;
        border-bottom: 3px solid #F2F2F2;
        text-align: center;
        background: #fff;
      }
      .service-block{margin: 1rem;}

      .upb_row_bg{background: #25a9f4;}
      .bg_a {
        color: #fff;
        border: 2px solid #fff;
        background-color: transparent;
        padding: 13px 19px;
      }

      .entry-content ul {
        margin-left: 1rem;
      }
      .entry-content ul li {
        list-style: disc;
        margin-left: 2rem;
      }
      input[type="submit"] {
        background: #03A9F4;
        padding: 12px 30px;
        color: #FFF;
        font-size: 20px;
      }
      .imgshadc:before, .imgshadc:after {
        content: "";
        position: absolute;
        -webkit-box-shadow: 0 15px 10px rgba(0,0,0,.6);
        box-shadow: 0 15px 10px rgba(0,0,0,.6);
        left: 25px;
        right: 50%;
        bottom: 28px;
        height: 30%;
        z-index: -1;
        border-radius: inherit;
        transform-origin: 0 0;
        -webkit-transform: skewY(-6deg);
        -ms-transform: skewY(-6deg);
        transform: skewY(-6deg);
      }
      .imgshadc:after {
        left: 50%;
        right: 25px;
        -webkit-transform: skewY(6deg);
        -ms-transform: skewY(6deg);
        transform: skewY(6deg);
        -webkit-transform-origin: 100% 0;
        -moz-transform-origin: 100% 0;
        -ms-transform-origin: 100% 0;
        transform-origin: 100% 0;
      }
      .lm-btn {
        font-size: 14px;
        padding: 14px 20px;
        border-radius: 5px;
      }
      .bluebanner {
        background: #003448;
        min-height: 2rem;
        min-width: 100%;
        padding: 5rem 0;
      }
      .whyus-cont {
        position: relative;
      }
      .upper-round, .lower-round {
        position: absolute;
        width: 100%;
      }
      .lower-round {
        bottom: -1rem;
        transform: rotate(180deg);
      }
      .gline {
        height: 1px;
        border-top: 3px solid #EBEBEB;
        display: block;
        position: relative;
        top: 1px;
        width: 10rem;
        margin: auto;
      }
      .testimonial-section {
        background: url(https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/bg-testimonials-1.jpg) center center no-repeat fixed;
        background-size: cover;
      }
      .testimonials {
        background-color: #21212178;
      }
      .testimonial-item {
        font-size: 24px;
        text-align: center;
        font-family: Hind;
        font-weight: 500;
        font-style: normal;
        margin-bottom: 35px;
      }
      .testimonial-author {
        font-size: 20px;
      }
      .addr-icon {
        width: 5rem;
        height: 5rem;
        min-width: 5rem;
        min-height: 5rem;
        max-width: 5rem;
        max-height: 5rem;
        background: #003448;
        border-color: #003448;
        border-radius: 50%;
        position: relative;
      }
      .addr-head {
        font-family: 'Montserrat', sans-serif;
        color: #003448;
        font-weight: 700;
        width: 100%;
      }
      .icon-connector {
        left: 49.5%;
        border-right-width: 1px;
        border-right-style: dashed;
        height: 100%;
        position: absolute;
        top: 70%;
        z-index: 1;
      }
      .noborder {
        border: none;
      }
      .addr-info p {
        font-size: 13px;
        line-height: 18px;
        margin: 0;
      }
      .break-all {
        word-break: break-all;
      }
      .cg {
        color: #309653;
        font-weight: 600;
      }
      .team-text {
        max-width:90%;
        margin: auto;
      }
      .container.m0 {
        margin: 0;
        width: 100%;
      }
      @media screen and (max-width: 991px) {
        .mobile-toggler {
          display: block;
        }
      }
      .visible-lg,.visible-md,.visible-sm,.visible-xs {
        display: none;
      }

      /* lg */
      @media screen and (min-width: 1200px) {
        .hidden-lg {
          display: none;
        }
        .visible-lg {
          display: block;
        }
      }
      /* md */
      @media screen and (max-width: 1199px) {
        .hidden-md {
          display: none;
        }
        .visible-md {
          display: block;
        }
      }
      /* sm */
      @media screen and (max-width: 991px) {
        .hidden-sm {
          display: none;
        }
        .visible-sm {
          display: block;
        }
        .sm-center {
          text-align: center;
        }
      }

      /* xs */
      @media only screen and (max-width: 767px) {
        .hidden-xs {
          display: none;
        }
        .visible-xs {
          display: block;
        }
        .site-info {
          text-align: center;
        }
        .md-right {
          float: none;
        }
        .footer-area-inner>div {margin: 2rem}
        #home-slider .amp-carousel-button {
          display: none;
        }
        .sld-btn .lmb {

        }
      }
    </style>
</head>
<body>
  <div class="hidden-sm">
  <div class="top-bar header-top bbot1 fullw">
    <div class="container">
      <div class="row">
        <div class="md1 sm12 xs12 header-top-right">
        </div>
        <div class="md11 sm12 xs12 header-top-right">
          <div class="top-header-menu-wrapper">
            <ul id="menu-top-header" class="top-header-nav-menu">
              <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/patient-information/online-patient-forms/">Patient Forms</a></li>
              <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/patient-information/patient-education/">Education</a></li>
              <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/patient-information/membership/">Membership</a></li>
              <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/patient-information/financing/">Financing</a></li>
              <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/patient-information/online-payment/">Payment</a></li>
              <li class="header-btn menu-item"><a target="_blank" href="https://rwlogin.com/PatientConnectAuthentication/SignIn?practiceId=12731">Patient Portal</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="mast-head bbot1 fullw">
    <div class="container">
      <div class="row p20">
        <div class="md3" id="p0">
          <div class="p20">
            <a href="">
              <amp-img src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2017/08/mansfield-logo.png" width="3" height="1" layout="responsive"></amp-img>
            </a>
          </div>
        </div>
        <div class="md3 m20" id="p0">
          <div class="header-contact-info dfx flstart">
            <div class="icon-wrapper m1">
              <i class="fa fa-clock-o"></i>
            </div>
            <div class="hcontact-info">
              <h6>Opening Hours</h6>
              <p>Mon, Wed, Fri: 1pm-7pm<br>Tu, Thu, Sat: 7am-7pm<br>Sun: 11am - 5pm</p>
            </div>
          </div>
        </div>
        <div class="md3 m20" id="p0">
          <div class="header-contact-info dfx flstart">
            <div class="icon-wrapper m1">
              <i class="fa fa-phone-square"></i>
            </div>
            <div class="hcontact-info">
              <h6>Call Us</h6>
              <p>817-522-0345</p>
            </div>

          </div>
        </div>
        <div class="md3 m20" id="p0">
          <div class="header-contact-info dfx flstart" id="#p0" style="padding-right: 2rem;">
            <div class="icon-wrapper m1">
              <i class="fa fa-calendar"></i>
            </div>
            <div class="hcontact-info">
              <h6><a class="bodyc" href="https://www.mansfield-dentalcare.com/appointments/">Request an Appointment</a></h6>
              <p>Your perfect smile is a click away!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <?php
  $amp_links=0;
  $testing=1;

  $server=new \App\MyClasses\Server();
  if($server->testing_server() && $testing)$testing=0;

  $property_id='8fc33172-1b47-4eb3-95b9-92e3d4007912';
  $root_path='https://www.mansfield-dentalcare.com';
  $full_path=$property_id.$root_path;

  if($testing){
      $url="http://127.0.0.1:8000/".$property_id."/".$root_path;

  }else{
      if($amp_links){
          $url="the real AMP cached version";
      }else{
          $url="https://amp.mansfield-dentalcare.com/?id=".$property_id."&page=".$root_path;

      }
  }
  $url=html_entity_decode($url);
  ?>

  <amp-sidebar id="sidebar-right"
               class="sidebar"
               layout="nodisplay"
               side="right">
    <nav toolbar="(min-width: 992px)"
         toolbar-target="target-element-right">
      <ul class="menu-items" style="overflow-y: hidden;">
        <li class="menu-item"><a href="{{$url}}">Home</a></li>
        <li class="menu-item"><a href="{{$url}}/team">Team</a></li>
        <li class="menu-item"><a href="{{$url}}/services">Services</a></li>
        <li class="menu-item menu-item-has-children"><a href="{{$url}}/patient-information">Patient Information</a>
          <ul class="sub-menu">
            <li class="menu-item"><a href="{{$url}}/patient-education/online-patient-forms">Online Patient Forms</a></li>
            <li class="menu-item"><a href="{{$url}}/patient-information/patient-education">Patient Education</a></li>
            <li class="menu-item"><a href="{{$url}}/membership">Membership</a></li>
            <li class="menu-item"><a href="{{$url}}/financing">Financing</a></li>
            <li class="menu-item"><a href="{{$url}}/online-payment">Online Payment</a></li>
          </ul>
        </li>
        <li class="menu-item"><a href="{{$url}}/appointments">Request Appointment</a></li>
        <li class="menu-item"><a target="_blank" href="https://rwlogin.com/PatientConnectAuthentication/SignIn?practiceId=12731">Patient Portal Login</a></li>
      </ul>
    </nav>
  </amp-sidebar>
<div class="menubar-wrap">
  <div class="menubar dfx jcsb">
    <div id="target-element-right" class="container" style="padding:1.5rem;">
    </div>
    <div class="fullw visible-sm">
      <div class="container">
        <button on="tap:sidebar-right.toggle" class="mobile-toggler"><i class="fa fa-bars bodyc" style="font-size: 4.5rem;"></i></button>
        <div class="row jcsb">
          <div class="xs6 sm3">
            <div class="p2">
              <a href="">
                <amp-img src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2017/08/mansfield-logo.png" width="3" height="1" layout="responsive"></amp-img>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="header-mobile">
  <div class="contact-header-area">
    <div class="container">
      <div class="row">
        <div class="p2">
          <div class="contact-item md12 sm12 xs12 call-contact">
              <div class="icon-wrapper branded">
                  <i class="fa fa-phone-square fs2"></i>
              </div>
              <div class="header-area-contact-info">
                  <h6>Call Us</h6>
                  <p><a href="tel:817-522-0345">817-522-0345</a></p>
              </div>
          </div>
          <div class="contact-item md12 sm12 xs12 opening-contact">
              <div class="icon-wrapper branded">
                  <i class="fa fa-clock-o fs2"></i>
              </div>
              <div class="header-area-contact-info">
                  <h6>Opening Hours</h6>
                  <p>Mon, Wed, Fri: 1pm-7pm<br>Tu, Thu, Sat: 7am-7pm<br>Sun: 11am - 5pm</p>
              </div>
          </div>
          <div class="contact-item md12 sm12 xs12 booking-contact">
              <div class="icon-wrapper branded">
                  <i class="fa fa-calendar fs2"></i>
              </div>
              <div class="header-area-contact-info">
                  <h6><a href="https://www.mansfield-dentalcare.com/appointments/">Request an Appointment</a></h6>
                  <p>Your perfect smile is a click away!</p>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    </div>
  </div>
</div>
<amp-carousel width="1265"
  height="700"
  layout="responsive"
  loop
  autoplay
  delay="201100"
  id="home-slider"
  type="slides">

      <div class="slide" style="background-image: url(https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2017/08/mansfield-1-1.jpg)">
      <div class="slide-detail ">
        <div class="slide-cont p2 cw">
          <div class="sld-title ucase">
            <amp-fit-text
              height="20"
              layout="fixed-height"
              max-font-size="18"
              >
              Dental care: Changing lives one smile at a time            </amp-fit-text>
          </div>
          <div class="sld-head bold">
            <amp-fit-text
              height="2"
              width="10"
              layout="responsive"
              max-font-size="32"
            >
              Routine Dental Exams & Check Ups $118            </amp-fit-text>
          </div>

          <div class="sld-text fw5">
            <amp-fit-text
              height="2"
              width="10"
              layout="responsive"
              max-font-size="16"
            >
              We’re here for you every six months to make sure your smile stays at its best.            </amp-fit-text>
          </div>

          <div class="sld-btn">
            <amp-fit-text
              height="1"
              width="8"
              layout="responsive"
              max-font-size="12"
              class='omh'
              id="overflow-unset"
            >
            <div style="display: inline-block;">
              <a class="lmb" href="services.php"><span style="padding: 5px 0px;">Learn more</span></a>
            </div>
            </amp-fit-text>
          </div>

        </div>
      </div>
    </div>
      <div class="slide" style="background-image: url(https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2017/08/slide-002-na.jpg)">
      <div class="slide-detail ">
        <div class="slide-cont p2 cw">
          <div class="sld-title ucase">
            <amp-fit-text
              height="20"
              layout="fixed-height"
              max-font-size="18"
              >
              Dental care: Changing lives one smile at a time            </amp-fit-text>
          </div>
          <div class="sld-head bold">
            <amp-fit-text
              height="2"
              width="10"
              layout="responsive"
              max-font-size="32"
            >
              Professional & Highly Trained Dental Staff            </amp-fit-text>
          </div>

          <div class="sld-text fw5">
            <amp-fit-text
              height="2"
              width="10"
              layout="responsive"
              max-font-size="16"
            >
              We’re passionate about taking care of the dental and orthodontic needs of the Dallas/Fort Worth metroplex            </amp-fit-text>
          </div>

          <div class="sld-btn">
            <amp-fit-text
              height="1"
              width="8"
              layout="responsive"
              max-font-size="12"
              class='omh'
              id="overflow-unset"
            >
            <div style="display: inline-block;">
              <a class="lmb" href="team.php"><span style="padding: 5px 0px;">Learn more</span></a>
            </div>
            </amp-fit-text>
          </div>

        </div>
      </div>
    </div>
      <div class="slide" style="background-image: url(https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2017/08/slide-003-na.jpg)">
      <div class="slide-detail ">
        <div class="slide-cont p2 cw">
          <div class="sld-title ucase">
            <amp-fit-text
              height="20"
              layout="fixed-height"
              max-font-size="18"
              >
              Dental care: Changing lives one smile at a time            </amp-fit-text>
          </div>
          <div class="sld-head bold">
            <amp-fit-text
              height="2"
              width="10"
              layout="responsive"
              max-font-size="32"
            >
              Dental Services for the <br> Entire Family            </amp-fit-text>
          </div>

          <div class="sld-text fw5">
            <amp-fit-text
              height="2"
              width="10"
              layout="responsive"
              max-font-size="16"
            >
              Bring everyone in at once to take of all your family’s dental and orthodontic needs.            </amp-fit-text>
          </div>

          <div class="sld-btn">
            <amp-fit-text
              height="1"
              width="8"
              layout="responsive"
              max-font-size="12"
              class='omh'
              id="overflow-unset"
            >
            <div style="display: inline-block;">
              <a class="lmb" href="services.php"><span style="padding: 5px 0px;">Learn more</span></a>
            </div>
            </amp-fit-text>
          </div>

        </div>
      </div>
    </div>
  </amp-carousel>
<div class="home-welcome main-content">
  <div class="container">
    <div class="row">
      <div class="xs12">
        <h1 style="letter-spacing: 1px;margin:0;font-size: 32px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading vc_custom_1502909386466">Welcome to Mansfield Dental Care</h1>
        <p style="font-size: 16px;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal" class="vc_custom_heading">Bringing the Best In Dental Care to Mansfield One Smile at a Time!</p>
      </div>
    </div>
    <div class="row p20">
      <div class="sm6 xs12" id="p0">
        <div class="p2">
          <div class="imgshadc">
            <amp-img src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/mansfield-home.jpg" width="3" height="2" layout="responsive"></amp-img>
          </div>
        </div>
      </div>
      <div class="sm6 xs12" id="p0">
        <div class="m2 pl1">

        <div class="fs16 fw5">
          <p>We are a community of practitioners dedicated to delivering the best dental care to Mansfield and the surrounding communities of South Arlington, Midlothian, Burleson, and Waxahachie.</p>
          <p>We are a Monday through Saturday office, offering appointments from 1 p.m. to 7 p.m. during the weekdays along with Saturday and Sunday hours. You’ll never have to worry about taking time away from school, work, or your family to experience the best in dental care.</p>
        </div>

        <div class="p30"><a style="background-color:#309653; color:#ffffff;" class="lm-btn" href="http://www.mansfield-dentalcare.com/services/" title="">Learn More</a></div>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="main-content">
  <div class="whyus-cont">

  <div class="upper-round">
    <svg class="uvc-x-large-circle" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="#fff" width="100%" height="60" viewBox="0 0 4.66666 0.333331" preserveAspectRatio="none" style="height: 60px;"><path class="fil1" d="M4.66666 0l0 7.87402e-006 -3.93701e-006 0c0,0.0920315 -1.04489,0.166665 -2.33333,0.166665 -1.28844,0 -2.33333,-0.0746339 -2.33333,-0.166665l-3.93701e-006 0 0 -7.87402e-006 4.66666 0z"></path></svg>
  </div>
  <div class="bluebanner cw">
    <div class="container">
      <div class="row">
        <div class="xs12">
          <h3 style="color:#fff;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal">Why Mansfield Dental Care</h3>
        </div>

      </div>
      <div class="row">
        <div class="sm3 xs12" id="p0">
          <div class="p2">
            <div class="yimgc mb1">
              <amp-img class="objcontain" src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/icon-dentist.png" height="52" layout="fixed-height"></amp-img>
            </div>
            <div class="ytext">
              <p style="font-size: 16px;color: #ffffff;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal" class="">Highly Trained Dental Team</p>
              <p style="font-size: 14px;color: #ffffff;text-align: center;font-family:Hind;font-weight:400;font-style:normal" class="">We only hire team members with a special combination of personality traits and specialty skills. Everyone in our office is held to the highest degrees of professionalism, self-motivation, and education.</p>
            </div>

          </div>
        </div>

        <div class="sm3 xs12" id="p0">
          <div class="p2">
            <div class="yimgc mb1">
              <amp-img class="objcontain" src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/icon-services.png" height="52" layout="fixed-height"></amp-img>
            </div>
            <div class="ytext">
              <p style="font-size: 16px;color: #ffffff;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal" class="">Extensive line of dental services</p>
              <p style="font-size: 14px;color: #ffffff;text-align: center;font-family:Hind;font-weight:400;font-style:normal" class="">Ten different dental and orthodontic services are offered to all patients.</p>
            </div>

          </div>
        </div>

        <div class="sm3 xs12" id="p0">
          <div class="p2">
            <div class="yimgc mb1">
              <amp-img class="objcontain" src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/icon-chair.png" height="52" layout="fixed-height"></amp-img>
            </div>
            <div class="ytext">
              <p style="font-size: 16px;color: #ffffff;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal" class="">Advanced Dental Treatment Facilities and Equipment</p>
              <p style="font-size: 14px;color: #ffffff;text-align: center;font-family:Hind;font-weight:400;font-style:normal" class="">We’re here to deliver personalized dental care solutions and make you as comfortable as possible during the process.</p>
            </div>

          </div>
        </div>

        <div class="sm3 xs12" id="p0">
          <div class="p2">
            <div class="yimgc mb1">
              <amp-img class="objcontain" src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/icon-dentist.png" height="52" layout="fixed-height"></amp-img>
            </div>
            <div class="ytext">
              <p style="font-size: 16px;color: #ffffff;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal" class="">Guaranteed Results and Smiles</p>
              <p style="font-size: 14px;color: #ffffff;text-align: center;font-family:Hind;font-weight:400;font-style:normal" class="">We’re friendly and outgoing in all that we do, and that combined with our expertise in the field is sure to give you the smile you’ve always dreamed of.</p>
            </div>

          </div>
        </div>

      </div>
    </div>


  </div>
  <div class="lower-round">
    <svg class="uvc-x-large-circle" xmlns="http://www.w3.org/2000/svg" version="1.1" fill="#fff" width="100%" height="60" viewBox="0 0 4.66666 0.333331" preserveAspectRatio="none" style="height: 60px;"><path class="fil1" d="M4.66666 0l0 7.87402e-006 -3.93701e-006 0c0,0.0920315 -1.04489,0.166665 -2.33333,0.166665 -1.28844,0 -2.33333,-0.0746339 -2.33333,-0.166665l-3.93701e-006 0 0 -7.87402e-006 4.66666 0z"></path></svg>
  </div>
  </div>

</div>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="xs12">
        <h1 style="font-size: 32px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="">Mansfield Dental Care</h1>
        <p style="font-size: 16px;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="">Professional and highly trained</p>
        <p>
          <div class="gline" style="border-color:#309653;"></div>
        <p>
      </div>
    </div>

    <div class="row">
      <div class="sm4 xs12" id="p0">
        <div class="m2">
          <div class="feature-container">
            <a href="https://www.mansfield-dentalcare.com/team">
              <amp-img src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/Screen-Shot-2017-09-11-at-5.01.16-PM.png" width="36" height="25" layout="responsive"></amp-img>
            </a>
          </div>

        </div>
      </div>

      <div class="sm4 xs12" id="p0">
        <div class="m2">
          <div class="feature-container">
            <a href="https://www.mansfield-dentalcare.com/services">
              <amp-img src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/Screen-Shot-2017-09-11-at-5.04.14-PM.png" width="36" height="25" layout="responsive"></amp-img>
            </a>
          </div>

        </div>
      </div>

      <div class="sm4 xs12" id="p0">
        <div class="m2">
          <div class="feature-container">
            <a href="https://www.mansfield-dentalcare.com/patient-information">
              <amp-img src="https://www.mansfield-dentalcare.com/wp-content/uploads/sites/3/2016/01/Screen-Shot-2017-09-11-at-5.04.32-PM.png" width="36" height="25" layout="responsive"></amp-img>
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="testimonial-section cw">
  <div class="testimonials">
    <div class="container p40">
      <h3 style="font-size: 32px;color: #ffffff;text-align: center;font-family:Montserrat;font-weight:700;font-style:normal" class="vc_custom_heading vc_custom_1502745383317">Testimonials</h3>

      <div class="testimonials-container">
        <div class="testimonial-item">
          <div class="testimonial-text">
            <div>
              This is one of the best experiences we've had. The facility is very clean and welcoming. [The doctor] and the rest of her staff are very knowledgeable and sensitive to our needs. I take my whole family there and I would not have it any other way. I will definitely recommend this place to all my friends. <div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div>            </div>
          </div>
          <div class="testimonial-author">
            Naileah G.
          </div>
        </div>

        <div class="testimonial-item">
          <div class="testimonial-text">
            Friendly and informative staff, I felt well taken care of. This is the best dental experience I've ever had as far as having to get dental work done. I felt almost no pain from start to finish, pretty amazing. [The doctor] is pleasant, informative, thorough, skilled, and kind. I chose this place because they were open on the weekend (my regular one wasn't, and not returning calls) and I had an emergency, but I am going to make this my new dental office going forward. <div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div>          </div>
          <div class="testimonial-author">
            Michelle F.
          </div>
        </div>

        <div class="testimonial-item">
          <div class="testimonial-text">
            Their skills and professionalism are clearly evident in everything that they do. We've been to a few dental offices since moving to Texas, but since finding Mansfield Dental Care & Orthodontics, out search is over. <div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div><div style="display:inline-block;width:20px;height:20px"><amp-img height="1" width="1" layout="responsive" src="https://s.w.org/images/core/emoji/2.3/svg/2b50.svg"></amp-img></div>          </div>
          <div class="testimonial-author">
            Danny S.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="upb_row_bg" style="background: rgb(48, 150, 83);">
  <div class="container">
    <div class="p40">
      <h3 style="font-size: 25px;color: #ffffff;text-align: center;font-family:Montserrat;font-weight:400;font-style:normal">Make your dream smile a reality!</h3>
      <div class="center p20">
        <a style="background-color:#003448; color:#ffffff;" class="lm-btn" href="/appointments" title="">Request an Appointment</a>
      </div>


    </div>
  </div>

</div>
<div class="row">
  <div class="xs12 sm6">
      <amp-iframe width="5" height="3" title="" layout="responsive" sandbox="allow-scripts allow-same-origin" frameborder="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6723.733691105218!2d-97.130299!3d32.583075!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e61a203423d25%3A0x4216ed70eeb9b218!2s1050+Country+Club+Dr+%23112%2C+Mansfield%2C+TX+76063!5e0!3m2!1sen!2sus!4v1552106320704">
      </amp-iframe>
  </div>
  <div class="xs12 sm6 addr-cont p20">
    <div class="row m20">
      <div class="container m0">
        <div class="xs2 dfx jcc" id="p0">
          <div class="addr-icon dfx jcc">
            <i class="fa fa-map-marker fs2 cw"></i>
            <div class="icon-connector" style="border-color:#333333;"></div>
          </div>
        </div>
        <div class="xs10 dfx flxwrp" style="align-content: center;">
          <h5 class="fs16 addr-head m0">Address</h5>
          <p class="addr-info m0">1050 Country Club Road, Suite 112 Mansfield, TX 76063</p>
        </div>
      </div>
    </div>

    <div class="row m20">
      <div class="container m0">
        <div class="xs2 dfx jcc" id="p0">
          <div class="addr-icon dfx jcc">
            <i class="fa fa-phone fs2 cw"></i>
            <div class="icon-connector" style="border-color:#333333;"></div>
          </div>
        </div>
        <div class="xs10 dfx flxwrp" style="align-content: center;">
          <h5 class="fs16 addr-head m0">Phone</h5>
          <p class="addr-info m0">817-522-0345</p>
        </div>
      </div>
    </div>

    <div class="row m20">
      <div class="container m0">
        <div class="xs2 dfx jcc" id="p0">
          <div class="addr-icon dfx jcc">
            <i class="fa fa-map-marker fs2 cw"></i>
            <div class="icon-connector" style="border-color:#333333;"></div>
          </div>

        </div>
        <div class="xs10 dfx flxwrp" style="align-content: center;">
          <h5 class="fs16 addr-head m0">Email</h5>
          <p class="addr-info m0 break-all">MansfieldCustomerService@TexasDentalResources.com</p>
        </div>
      </div>
    </div>

    <div class="row m20">
      <div class="container m0">
        <div class="xs2 dfx jcc" id="p0">
          <div class="addr-icon dfx jcc">
            <i class="fa fa-map-marker fs2 cw"></i>
            <div class="icon-connector noborder" style="border-color:#333333;"></div>
          </div>
        </div>
        <div class="xs10 dfx flxwrp" style="align-content: center;">
          <h5 class="fs16 addr-head m0">Opening Hours</h5>
            <div class="addr-info m0">
              <p><span class="cg">Mon – Fri:</span> 1 p.m. to 7 p.m.</p>
              <p><span class="cg">Sat:</span> 7 a.m. to 7 p.m.</p>
              <p><span class="cg">Sun:</span> 11 a.m to 5 p.m.</p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <footer id="colophon" class="site-footer container-fluid">
    <div id="dental-care-footer-sidebar" class="secondary footer-area row">
      <div class="container footer-area-inner">
        <div id="dental-care-footer-sidebar1" class="footer-widgets widget-area clear md4" role="complementary">
          <aside id="dental_care_company-1" class="widget dental_care_company">
            <h5 class="footer-widget-title">About Us</h5><span class="widget-title-underline-footer"></span>
            <ul class="company-info-wid">
              <li><i class="fa fa-map-marker"></i> 1050 Country Club Road, Suite 112 Mansfield, TX 76063</li>
              <li><i class="fa fa-phone"></i><a href="tel:817-522-0345"> 817-522-0345</a></li>
              <li><i class="fa fa-envelope-o"></i><a class="break-all" href="mailto:{{'@'}}TexasDentalResources.com?subject=Contact Us" target="_blank"> MansfieldCustomerService@TexasDentalResources.com </a></li>
            </ul>
          </aside>
        </div>
        <div id="dental-care-footer-sidebar2" class="footer-widgets widget-area clear md4" role="complementary">
          <aside id="nav_menu-1" class="widget widget_nav_menu">
            <h5 class="footer-widget-title">Services</h5><span class="widget-title-underline-footer"></span>
            <div class="menu-service-menu-container">
              <ul id="menu-service-menu" class="menu">
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/cosmetic-dentistry/">Cosmetic Dentistry</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/dental-implants/">Dental Implants</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/all-on-4-dental-implants/">All-on-4 Dental Implants</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/general-dentistry/">General Dentistry</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/porcelain-veneers/">Porcelain Veneers</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/root-canals/">Root Canals</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/sedation-dentistry/">Sedation Dentistry</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/teeth-whitening/">Teeth Whitening</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/orthodontics-clear-braces/">Orthodontics &amp; Clear Braces</a></li>
                <li class="menu-item"><a href="https://www.mansfield-dentalcare.com/service/sleep-apnea/">Sleep Apnea</a></li>
              </ul>
            </div>
          </aside>
        </div>
        <div id="dental-care-footer-sidebar3" class="footer-widgets widget-area clear md4" role="complementary">
          <aside id="dental_care_opening-1" class="widget dental_care_opening">
            <h5 class="footer-widget-title">Opening Hours</h5><span class="widget-title-underline-footer"></span>
            <ul class="opening-hours-wid">
              <li><i class="fa fa-clock-o"></i>Monday<span>1:00 PM - 7:00 PM</span></li>
              <li><i class="fa fa-clock-o"></i>Tuesday<span>1:00 PM - 7:00 PM</span></li>
              <li><i class="fa fa-clock-o"></i>Wednesday<span>1:00 PM - 7:00 PM</span></li>
              <li><i class="fa fa-clock-o"></i>Thursday<span>1:00 PM - 7:00 PM</span></li>
              <li><i class="fa fa-clock-o"></i>Friday<span>1:00 PM - 7:00 PM</span></li>
              <li><i class="fa fa-clock-o"></i>Saturday<span>7:00 AM - 7:00 PM</span></li>
              <li><i class="fa fa-clock-o"></i>Sunday<span>11:00 AM - 5 PM</span></li>
            </ul>
          </aside>
          <aside id="dental_care_social-1" class="widget dental_care_social">
            <h5 class="footer-widget-title">Follow Us</h5><span class="widget-title-underline-footer"></span>
            <div class="social-widget">
              <ul class="">
                <li class="social-widget-item social-fb"><a target="_blank" title="Facebook" href="https://www.facebook.com/Mansfield-Dental-Care-Orthodontics-1563226890643005/"><i class="fa fa-facebook"></i></a></li>
              </ul>
            </div>
          </aside>
        </div>
        <div id="dental-care-footer-sidebar4" class="footer-widgets widget-area clear md4" role="complementary">
        </div>
      </div>
    </div>
    <div class="site-info-wrapper row">
      <div class="container site-info">
        <div class="site-info-inner md6 sm12 xs12">
          <span class="copyright">Copyright © 2017 Texas Dental Resources</span>
          <a href="" id="to-top" title="Back to top" style="display: inline;"></a>
        </div>
        <div class="footer-menu md6 sm12 xs12">
          <div class="footer-menu-wrapper md-right">
            <ul id="menu-footer-menu" class="footer-nav-menu">
              <li class="menu-item"><a class="bodyc" href="https://www.mansfield-dentalcare.com/team/">Team</a></li>
              <li class="menu-item"><a class="bodyc" href="https://www.mansfield-dentalcare.com/services/">Services</a></li>
              <li class="menu-item"><a class="bodyc" href="https://www.mansfield-dentalcare.com/patient-information/">Patient Information</a></li>
              <li class="menu-item"><a class="bodyc" href="https://www.mansfield-dentalcare.com/appointments/">Appointments</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
</body>
</html>
