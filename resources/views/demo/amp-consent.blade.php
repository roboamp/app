<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CS for AMP</title>
</head>
<body>
<script type="text/javascript">
    var _iub = _iub || [];
    _iub.csConfiguration = {
        lang: 'en',
        siteId: 896537, // your own site ID
        cookiePolicyId: 8207462, // your own cookie policy ID

        enableCMP: true,
        askConsentIfCMPNotFound: true,
        isTCFConsentGlobal: true,
        googleAdsPreferenceManagement: true,

        banner: {
            position: 'float-bottom-center',
            acceptButtonDisplay: true,
            customizeButtonDisplay: true,
            rejectButtonDisplay: true,

            // setting an overlay for UX reasons
            // because it's not possible to click-through in iframes
            backgroundOverlay: true
        },
        callback: {
            onPreferenceExpressed: function(preference) {
                var consentAction = 'reject';
                if (preference && preference.consent) {
                    consentAction = 'accept';
                }
                console.log('send consent-response', consentAction);
                window.parent.postMessage({
                    type: 'consent-response',
                    action: consentAction
                }, '*');
            }
        }
    };
</script>
<script async src="https://ampy.iubenda.com/cs/beta/iubenda_cs.js"></script>
<!--<script async src="https://ampy.roboamp.com/robocookie.js"></script>-->
</body>
</html>