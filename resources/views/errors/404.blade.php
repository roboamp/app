<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="{{asset('img/mono-48px.png')}}">
        <title>404</title>
        <!-- Bootstrap core CSS -->
        @include ('customers/dashboard/includes.css')
    </head>
    <body class="body-404">
        <div class="container">
            <section class="error-wrapper">
                <img src="{{asset('img/logo-2.png')}}">
                <h1>404</h1>
                <h2>page not found</h2>
                <p class="page-404">
                <?php

                if( $exception->getMessage()!=""){
                    echo "<h3>Error: ".$exception->getMessage()."</h3>";
                }else{
                    echo "Something went wrong or that page doesn’t exist yet.";
                }

                ?>
                </p>
                <p class="page-404"><a href="/home">Return Home</a></p>
            </section>
        </div>
    </body>
</html>