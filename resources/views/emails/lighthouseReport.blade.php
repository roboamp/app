@component('mail::message')
# Report about {{ $report->url }}

@foreach ($report->flags as $flag)
## {{ $flag->name }}
@if($flag->pivot->score_normal > 0)
@php $more = ((((float)$flag->pivot->score * 100)/(float)$flag->pivot->score_normal)-100); @endphp
@endif
<table border="0">
    <thead>
        <tr>
            <th>Score without ROBOAMP</th>
            <th>Score with ROBOAMP</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><img src="{{ asset('knobs/generated/knob-normal-'.($flag->pivot->score_normal * 100).'.png') }}"/></td>
            <td><img src="{{ asset('knobs/generated/knob-amp-'.($flag->pivot->score * 100).'.png') }}"/></td>
        </tr>
        @if($flag->pivot->score_normal > 0)
        <tr>
            <td colspan="2"><p style="text-align: center;"><span style="color: #01c0c8"><em>{{ round($more, 2)}}% faster</em></span></p></td>
        </tr>
        @endif
    </tbody>
</table>
<p>{{ $flag->message }}</p>

@endforeach

@component('mail::button', ['url' => $url ])
Contact us at ROBOAMP
@endcomponent
@endcomponent