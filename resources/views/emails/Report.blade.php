<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('dashboard_assets')}}/images/favicon.png">
  <title>ROBOAMP - Control Panel</title>
  <!-- This page CSS -->
  <!-- This page CSS -->
  <!--<link href="{{--asset('dashboard_assets') --}}/node_modules/morrisjs/morris.css" rel="stylesheet"> -->
  <!-- Custom CSS -->
  <link href="{{asset('dashboard_assets')}}/css/style.min.css" rel="stylesheet">
  <!-- Dashboard 1 Page CSS -->
  <link href="{{asset('dashboard_assets')}}/css/pages/dashboard4.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  @yield('header_scrips')
  @yield('css')
</head>
<body>
  <input data-plugin="knob" data-width="220" data-height="220" data-linecap="round" data-fgcolor="#01c0c8" value=".90" data-skin="tron" data-angleoffset="180" data-readonly="true" data-thickness=".2" readonly>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset('Axton/assets/node_modules/jquery/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('Axton/assets/node_modules/popper/popper.min.js')}}"></script>
<script src="{{asset('Axton/assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Axton/assets/node_modules/knob/jquery.knob.min.js')}}"></script>
<script type="text/javascript">

$(function() {
      $('[data-plugin="knob"]').knob();
  });

</script>
</body>
</html>