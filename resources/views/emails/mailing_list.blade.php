<!doctype html>
<html ⚡4email>
<head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <style amp4email-boilerplate>body{visibility:hidden}</style>
    <style amp-custom>
        body {
            padding-bottom: 40px;
        }
        h1 {
            margin: 1rem;
        }
    </style>
</head>
<body>
<form method="post" action-xhr="https://dev.amp4email.roboamp.com/amp/email">
    @csrf
      <!--action-xhr="https://parser.roboamp.com/amp/email">-->
    <p>ROBOAMP  - MAILING LIST SUBMISSION DEMO</p>
    <div>
        <input type="text"
               name="name"
               placeholder="Name..."
               required value="ROBERTO">
        <input type="email"
               name="email"
               placeholder="Email..."
               required value="copertus@gmail.com">
    </div>
    <input type="submit"
           value="Subscribe">
    <div submit-success>
        <template type="amp-mustache">
            <b>Success!</b><br> Thanks @{{name}} for trying the
            <b> ROBOAMP</b> demo!🚀🚀🚀
        </template>
    </div>
    <div submit-error>
        <template type="amp-mustache">
            Error! <br>Thanks for trying the
            <b>ROBOAMP</b> demo with an error response. 💀😵💀
        </template>
    </div>
</form>
</body>
</html>