@extends('spark::layouts.app')
<!-- Main Content -->
@section('content')
<link href="{{asset('css/dashboard/style.css')}}" rel="stylesheet">
<body class="login-body">
    <div class="container">
        <form class="form-signin" action="{{ route('customers.password.email') }}" method="POST">
            {{ csrf_field() }}
            <h2 class="form-signin-heading">Customer Reset Password</h2>
            <div class="login-wrap">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                <!-- E-Mail Address -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" placeholder="E-Mail Address" value="{{ old('email') }}" autofocus>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
                    @endif
                </div>
                <!-- Send Password Reset Link Button -->
                
                <button class="btn btn-lg btn-login btn-block" type="submit">
                Send Password Reset Link
                </button>
            </div>
        </form>
    </div>
</body>
@endsection