@extends('spark::layouts.app')
@section('content')
<link href="{{asset('css/dashboard/style.css')}}" rel="stylesheet">
<body class="login-body">
    <div class="container">
        <form class="form-signin" action="{{ route('customers.password.reset.submit') }}" method="POST">
            {{ csrf_field() }}
            <h2 class="form-signin-heading">Customer Reset Password</h2>
            <div class="login-wrap">
                <!-- E-Mail Address -->
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" placeholder="E-Mail Address" value="{{ old('email') }}" autofocus>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        {{ $errors->first('email') }}
                    </span>
                    @endif
                </div>
                <!-- Password -->
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" name="password" placeholder="Password" value="{{ old('password') }}" autofocus>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        {{ $errors->first('password') }}
                    </span>
                    @endif
                </div>
                <!-- Password Confirmation -->
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        {{ $errors->first('password_confirmation') }}
                    </span>
                    @endif
                </div>
                <input type="hidden" value="{{$token}}" name="token">
                <!-- Reset Button -->
                <button class="btn btn-lg btn-login btn-block" type="submit">
                Reset Password
                </button>
            </div>
        </form>
    </div>
</body>
@endsection